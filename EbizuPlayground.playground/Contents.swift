import Foundation

func solution(_ N:Int) -> Int
{
    var arrAnswer = [String]()
    
    let completeStr = "\(N)"
    let arrInteger = completeStr.characters.flatMap({Int(String($0))})
    arrAnswer = arrInteger.sorted(by: >).flatMap({String(Int($0))})
    
    
    return Int(arrAnswer.joined()) ?? 0
}

solution(553)

