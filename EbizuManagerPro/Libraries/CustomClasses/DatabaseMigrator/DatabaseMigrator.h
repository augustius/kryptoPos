#import <Foundation/Foundation.h>

@import FMDB;

@interface DatabaseMigrator : NSObject
{
	NSString *_filename;
	BOOL overwriteDatabase;
	FMDatabase *database;
}

@property (nonatomic, copy) NSString *filename;
@property (nonatomic) BOOL overwriteDatabase;

@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSString *databasePath;
-(instancetype)initWithDatabaseFile:(NSString *)filename NS_DESIGNATED_INITIALIZER;

@property (NS_NONATOMIC_IOSONLY, readonly) BOOL moveDatabaseToUserDirectoryIfNeeded;
-(void)migrateToVersion:(int)version;

@property (NS_NONATOMIC_IOSONLY) int version;

@end
