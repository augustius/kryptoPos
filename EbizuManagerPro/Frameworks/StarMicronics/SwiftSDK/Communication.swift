//
//  Communication.swift
//  Swift SDK
//
//  Created by Yuji on 2015/**/**.
//  Copyright © 2015年 Star Micronics. All rights reserved.
//

import Foundation

enum LanguageIndex: Int {
    case english = 0
    case japanese
    case french
    case portuguese
    case spanish
    case russian
    case simplifiedChinese
    case traditionalChinese
}

enum PaperSizeIndex: Int {
    case twoInch = 384
    case threeInch = 576
    case fourInch = 832
    case escPosThreeInch = 512
    case dotImpactThreeInch = 210
}

class Communication {
    static func sendCommands(_ commands: Data!, port: SMPort!) -> Bool {
        var result: Bool = false
        
//        var title:   String = ""
//        var message: String = ""
        
        var error: NSError?
        
        let length: UInt32 = UInt32(commands.count)
        
        var array: [UInt8] = [UInt8](repeating: 0, count: commands.count)
        
        commands.copyBytes(to: &array, count: commands.count)
        
        while true {
            if port == nil {
//                title   = "Fail to Open Port"
//                message = ""
                break
            }
            
            var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
            
            port.beginCheckedBlock(&printerStatus, 2, &error)
            
            if error != nil {
                break
            }
            
            if printerStatus.offline == sm_true {
//                title   = "Printer Error"
//                message = "Printer is offline (BeginCheckedBlock)"
                break
            }
            
            let startDate: Date = Date()
            
            var total: UInt32 = 0
            
            while total < length {
                let written: UInt32 = port.write(array, total, length - total, &error)
                
                if error != nil {
                    break
                }
                
                total += written
                
                if Date().timeIntervalSince(startDate) >= 30.0 {     // 30000mS!!!
//                    title   = "Printer Error"
//                    message = "Write port timed out"
                    break
                }
            }
            
            if total < length {
                break
            }
            
            port.endCheckedBlockTimeoutMillis = 30000     // 30000mS!!!
            
            port.endCheckedBlock(&printerStatus, 2, &error)
            
            if error != nil {
                break
            }
            
            if printerStatus.offline == sm_true {
//                title   = "Printer Error"
//                message = "Printer is offline (endCheckedBlock)"
                break
            }
            
//            title   = "Send Commands"
//            message = "Success"
            
            result = true
            break
        }
        
        if error != nil {
//            title   = "Printer Error"
//            message = error!.description
        }
        
//        DispatchQueue.main.async {
//            let alertView: UIAlertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
//            
//            alertView.show()
//        }
        
        return result
    }
    
    static func sendCommandsDoNotCheckCondition(_ commands: Data!, port: SMPort!) -> Bool {
        var result: Bool = false
        
//        var title:   String = ""
//        var message: String = ""
        
        var error: NSError?
        
        let length: UInt32 = UInt32(commands.count)
        
        var array: [UInt8] = [UInt8](repeating: 0, count: commands.count)
        
        commands.copyBytes(to: &array, count: commands.count)
        
        while true {
            if port == nil {
//                title   = "Fail to Open Port"
//                message = ""
                break
            }
            
            var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
            
            port.getParsedStatus(&printerStatus, 2, &error)
            
            if error != nil {
                break
            }
            
//          if printerStatus.offline == sm_true {     // Do not check condition.
//              title   = "Printer Error"
//              message = "Printer is offline (getParsedStatus)"
//              break
//          }
            
            let startDate: Date = Date()
            
            var total: UInt32 = 0
            
            while total < length {
                let written: UInt32 = port.write(array, total, length - total, &error)
                
                if error != nil {
                    break
                }
                
                total += written
                
                if Date().timeIntervalSince(startDate) >= 30.0 {     // 30000mS!!!
//                    title   = "Printer Error"
//                    message = "Write port timed out"
                    break
                }
            }
            
            if total < length {
                break
            }
            
            port.getParsedStatus(&printerStatus, 2, &error)
            
            if error != nil {
                break
            }
            
//          if printerStatus.offline == sm_true {     // Do not check condition.
//              title   = "Printer Error"
//              message = "Printer is offline (getParsedStatus)"
//              break
//          }
            
//            title   = "Send Commands"
//            message = "Success"
            
            result = true
            break
        }
        
        if error != nil {
//            title   = "Printer Error"
//            message = error!.description
        }
        
//        DispatchQueue.main.async {
//            let alertView: UIAlertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: nil, otherButtonTitles: "OK")
//            
//            alertView.show()
//        }
        
        return result
    }
    
    static func sendCommands(_ commands: Data!, portName: String!, portSettings: String!, timeout: UInt32) -> Bool! {
        var result: Bool = false
        
        var title:   String = ""
        var message: String = ""
        
        var error: NSError?
        
        let length: UInt32 = UInt32(commands.count)
        
        var array: [UInt8] = [UInt8](repeating: 0, count: commands.count)
        
        commands.copyBytes(to: &array, count: commands.count)
        
        var port: SMPort?
        
        while true {
            port = SMPort.getPort(portName, portSettings, timeout)
            
            if port == nil {
                title   = "Fail to Open Port"
                message = ""
                break
            }
            
            defer {
                SMPort.release(port)
            }
            
            var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
            
            port!.beginCheckedBlock(&printerStatus, 2, &error)
            
            if error != nil {
                break
            }
            
            if printerStatus.offline == sm_true {
                title   = "Printer Error"
                message = "Printer is offline (BeginCheckedBlock)"
                break
            }
            
            let startDate: Date = Date()
            
            var total: UInt32 = 0
            
            while total < length {
                let written: UInt32 = port!.write(array, total, length - total, &error)
                
                if error != nil {
                    break
                }
                
                total += written
                
                if Date().timeIntervalSince(startDate) >= 30.0 {     // 30000mS!!!
                    title   = "Printer Error"
                    message = "Write port timed out"
                    break
                }
            }
            
            if total < length {
                break
            }
            
            port!.endCheckedBlockTimeoutMillis = 30000     // 30000mS!!!
            
            port!.endCheckedBlock(&printerStatus, 2, &error)
            
            if error != nil {
                break
            }
            
            if printerStatus.offline == sm_true {
                title   = "Printer Error"
                message = "Printer is offline (endCheckedBlock)"
                break
            }
            
            title   = "Send Commands"
            message = "Success"
            
            result = true
            break
        }
        
        if error != nil {
            title   = "Printer Error"
            message = error!.description
        }
        
//        DispatchQueue.main.async {
//            let alertView: UIAlertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
//            
//            alertView.show()
//        }
        
        return result
    }
    
    static func sendCommandsDoNotCheckCondition(_ commands: Data!, portName: String!, portSettings: String!, timeout: UInt32) -> Bool! {
        var result: Bool = false
        
        var title:   String = ""
        var message: String = ""
        
        var error: NSError?
        
        let length: UInt32 = UInt32(commands.count)
        
        var array: [UInt8] = [UInt8](repeating: 0, count: commands.count)
        
        commands.copyBytes(to: &array, count: commands.count)
        
        var port: SMPort?
        
        while true {
            port = SMPort.getPort(portName, portSettings, timeout)
            
            if port == nil {
                title   = "Fail to Open Port"
                message = ""
                break
            }
            
            defer {
                SMPort.release(port)
            }
            
            var printerStatus: StarPrinterStatus_2 = StarPrinterStatus_2()
            
            port!.getParsedStatus(&printerStatus, 2, &error)
            
            if error != nil {
                break
            }
            
//          if printerStatus.offline == sm_true {     // Do not check condition.
//              title   = "Printer Error"
//              message = "Printer is offline (getParsedStatus)"
//              break
//          }
            
            let startDate: Date = Date()
            
            var total: UInt32 = 0
            
            while total < length {
                let written: UInt32 = port!.write(array, total, length - total, &error)
                
                if error != nil {
                    break
                }
                
                total += written
                
                if Date().timeIntervalSince(startDate) >= 30.0 {     // 30000mS!!!
                    title   = "Printer Error"
                    message = "Write port timed out"
                    break
                }
            }
            
            if total < length {
                break
            }
            
            port!.getParsedStatus(&printerStatus, 2, &error)
            
            if error != nil {
                break
            }
            
//          if printerStatus.offline == sm_true {     // Do not check condition.
//              title   = "Printer Error"
//              message = "Printer is offline (getParsedStatus)"
//              break
//          }
            
            title   = "Send Commands"
            message = "Success"
            
            result = true
            break
        }
        
        if error != nil {
            title   = "Printer Error"
            message = error!.description
        }
        
//        DispatchQueue.main.async {
//            let alertView: UIAlertView = UIAlertView(title: title, message: message, delegate: nil, cancelButtonTitle: "OK")
//            
//            alertView.show()
//        }
        
        return result
    }
}
