//
//  AppDelegate.h
//  Ebizu Manager Pro
//
//  Created by Jamal on 6/20/15.
//  Copyright (c) 2015 Ebizu Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SessionData.h"

#import "DatabaseMigrator.h"
#import "AbstractRepository.h"
#import "MBProgressHUD.h"
#import "UIUtils.h"
#import "FileUtils.h"
//#import "Analytics.h" // Google Analaytics
#import "shareddb.h"
#import "HoldOrderForm.h"
#import <UserNotifications/UserNotifications.h>

@class CocoaMQTT;

@protocol CocoaMQTTDelegate;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>

@property (strong, nonatomic ) UIWindow                  *window;
@property (strong            ) NSTimer                   *timer;
@property (strong , nonatomic) NSString                  *push_device_token;
@property (strong , nonatomic) NSBundle                  *language_bundle;

@property (nonatomic, assign ) BOOL                      pin_validated;
@property (nonatomic, assign ) BOOL                      is_multi_device;
@property (nonatomic, assign ) BOOL                      is_order_terminal;
@property (nonatomic, assign ) BOOL                      is_hub_connection_lost;
@property (strong , nonatomic) NSString                  *hub_base_url;

@property (nonatomic, assign) BOOL     is_currently_printing;
//@property (nonatomic, assign) BOOL     is_currently_syncing;
@property (nonatomic, assign ) int                        user_role_id;
@property (readonly, nonatomic) NSCalendar               *current_calender;


- (void)    show_indicator;

- (void)    hide_indicator;
- (void)    publish_message:(NSString *)data;
- (void)    notify_with_right_toast:(NSString*)process_name title:(NSString*)title_name;
- (void)    notify_with_right_toast:(NSString*)process_name title:(NSString*)title_name image:(NSString*)image_name;
- (void)    notify_with_right_toast:(NSString*)process_name title:(NSString*)title_name duration:(double)duration;


- (void) handle_exception:(NSString *)exception_error withmodule:(NSString *)module withfunc:(NSString *)func;

#pragma mark - Mqtt configurations

- (void)start_mqtt:(BOOL)is_connection_test;
- (void)listen_mqtt_incoming;


- (void)setup_db;
- (void)hub_connection_lost:(NSNotification *)data;

@end


