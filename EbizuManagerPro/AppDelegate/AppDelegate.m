     //
//  AppDelegate.m
//  Ebizu Manager Pro
//
//  Created by Jamal on 6/20/15.
//  Copyright (c) 2015 Ebizu Sdn Bhd. All rights reserved.
//

#import "AppDelegate.h"
#import <Intercom/Intercom.h>
#import "UIView+Toast.h"  
#import "data_sync.h"
// For Fabric w/ Crashlytics
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>
#import "EBNDefaults.h"
#import "KryptoPOS-Swift.h"

@import CoreLocation;
@import CoreBluetooth;
@import Firebase;
@import FirebaseCrashlytics;

CBPeripheralManager *peripheral_manager = nil;
CLBeaconRegion      *region = nil;
NSNumber            *power = nil;

@interface AppDelegate ()<CBPeripheralManagerDelegate,GCDAsyncSocketDelegate, GCDAsyncUdpSocketDelegate>

@property(strong , nonatomic) CocoaMQTT *cocoa_mqtt;

@end

// Constants for INTERCOM
#define INTERCOM_APP_ID  @"snrtdyzn"
#define INTERCOM_API_KEY @"ios_sdk-23e580ba207087161bef4e00b7578251de3e02ca"

@implementation AppDelegate

#pragma mark - UIApplication Specific Methods
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // set app never idle to true
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    // customize navigation bar
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTintColor:UIColorFromHEX(0x478BCA)];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIColor blackColor], NSForegroundColorAttributeName,
                                                          [UIFont fontWithName:@"Poppins-Light" size:18.0], NSFontAttributeName,nil]];
    
    // set all navigation bar to one color
    
    /* Write the LOGS into text file & Disable from xcode console for app performence - Please enable while submitting to appstore*/
    
//        #if defined (DEBUG) && (DEBUG==1)
//    
//            [self write_logs_into_file];
//    
//            NSString	*ourVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleVersion"];
//    
//            NSLog(@"********************************************");
//            NSLog(@"  This is the Ebizu Manager app version %@", ourVersion);
//            NSLog(@"********************************************");
//    
//        #endif
    
    //Set Local time zone
    [NSTimeZone setDefaultTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"MYT"]];
    _current_calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    //Start of Google Analytics initialisation
//    [GAI sharedInstance].trackUncaughtExceptions = YES;
    APP_DELEGATE.push_device_token               = @"";
    
    //Check device is order terminal
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"is_order_terminal"] isEqualToString:@"enabled"])
    {
        APP_DELEGATE.is_order_terminal = YES;
    }
    else
    {
        APP_DELEGATE.is_order_terminal = NO;
    }

    // Google Analytics
    // FIXME: Check what is the current use of Google Analytics in the Application
//    [GAI sharedInstance].dispatchInterval        = 20;
//    [[GAI sharedInstance] trackerWithTrackingId:@"UA-113365224-1"];

    // Database setup
    [self setup_db];
    [self setup_images_directory];
    self.push_device_token  = @"";
    [self get_user_defaults_values];
    
    
    // For UDID
    NSString *udid = [NSUUID UUID].UUIDString;
    [[NSUserDefaults standardUserDefaults] setObject:udid forKey:@"UDID"];
    
    // For Intercom
    [Intercom setApiKey:INTERCOM_API_KEY forAppId:INTERCOM_APP_ID];
    
    #ifdef DEBUG
        [Intercom enableLogging];
    #endif
    
    NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    
    if (email.length > 0) // Checking if there is a user logged in
    {
        [Intercom registerUserWithEmail:email];
    }
    
    // For Fabric w/ Crashlytics
//    [Fabric with:@[[Crashlytics class]]];
    [FIRApp configure];
    
    // Register for push notifications
    [self register_push_notification];
    
    peripheral_manager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];
    

    switch ([UIApplication sharedApplication].backgroundRefreshStatus) {
        case UIBackgroundRefreshStatusDenied:
            NSLog(@"App won't detect Beacon in Background without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh");
            break;
        case UIBackgroundRefreshStatusRestricted:
            NSLog(@"The functions of this app are limited because the Background App Refresh is disable.");
            break;
        case UIBackgroundRefreshStatusAvailable:
            NSLog(@"background refresh status available");
        }
    
    /*Push Notification Handler */
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (notification)
    {
//        [self sync_all_using_push_data:[notification objectForKey:@"data"]];
        [self clear_all_notifactions_from_noticaftioncenter];
    }

    self.is_hub_connection_lost = NO;
    
    
// enable back when know where to remove observer
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                                 selector:@selector(hub_connection_lost:)
//                                                     name:@"hub_connection_lost"
//                                                   object:nil];
    [self start_mqtt:YES];
    return YES;
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [self activate_pos_as_beacon];
    completionHandler(UIBackgroundFetchResultNewData);
    // We will add content here soon.
}

-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    return true;
}
// Calls Manage Pay connect URL
-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [SESSION_DATA handleURL:url];
}

#pragma mark - Below iOS-10 version Notification delegate methods

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // For Intercom - Send device token to Intercom third party
    [Intercom setDeviceToken:deviceToken];
    
    //For POS push notification
    self.push_device_token = [[deviceToken.description stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] mutableCopy];
    self.push_device_token = [[[self.push_device_token copy] stringByReplacingOccurrencesOfString:@" " withString:@""] mutableCopy];
    NSLog(@"PUSH ID : %@", self.push_device_token);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"PUSH ERROR : %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"user %@", userInfo);
    
    if(userInfo != nil)
    {
        UIApplicationState state    = application.applicationState;
        
        switch (state) {
            case UIApplicationStateActive:
                [self.window makeToast:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] duration:2.0 position:CSToastPositionTopRight title:nil image:[UIImage imageNamed:@"AppIcon76x76"] style:nil completion:nil];
                break;
            default: break;
        }
        
//        [self sync_all_using_push_data:userInfo[@"data"]];
        [self clear_all_notifactions_from_noticaftioncenter];
    }
}

#pragma mark - POS system beacon methods

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    switch (peripheral.state) {
        case CBPeripheralManagerStatePoweredOn:
            [self activate_pos_as_beacon];
            break;
        case CBPeripheralManagerStatePoweredOff:
            NSLog(@"Beacon Signal is Turned Off! \n Enable your Device Bluetooth");
            break;
        case CBPeripheralManagerStateUnsupported:
            NSLog(@"Device Does Not support bluetooth");
            break;
        default:
            break;
    }
}
- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error
{
    NSLog(@"Started advertising");
}

-(void)activate_pos_as_beacon
{
    NSDictionary *peripheral_data = nil;
    region                        = [[CLBeaconRegion alloc] initWithProximityUUID:[EBNDefaults sharedDefaults].defaultProximityUUID major:([EBNDefaults sharedDefaults].defaultProximityUUIDMajorValue).shortValue minor:([EBNDefaults sharedDefaults].defaultProximityUUIDMinorValue).shortValue identifier:beacon_identifier];
    peripheral_data = [region peripheralDataWithMeasuredPower:[EBNDefaults sharedDefaults].defaultPower];
    
    if(peripheral_data)
    {
        [peripheral_manager startAdvertising:peripheral_data];
        NSLog(@"Broadcasting Beacon Signal from POS app!");
    }
}

-(void)get_user_defaults_values //TODO: check
{
    SESSION_DATA.mqtt_ip_address=[[NSUserDefaults standardUserDefaults] objectForKey:@"mqtt_server_ip"];
    NSLog(@"%@",SESSION_DATA.mqtt_ip_address);
    
    if([SESSION_DATA.mqtt_ip_address isEqualToString:@""] || SESSION_DATA.mqtt_ip_address == nil || SESSION_DATA.mqtt_ip_address == (id)[NSNull null])
    {
        SESSION_DATA.mqtt_ip_address = mqtt_server_ip;
    }
    else
    {
        APP_DELEGATE.hub_base_url = [NSString stringWithFormat:@"http://%@:8181/api/v1",SESSION_DATA.mqtt_ip_address];
    }
}

-(void)notify_with_right_toast:(NSString*)process_name title:(NSString*)title_name;
{
    dispatch_async(dispatch_get_main_queue(),^{

        [self notify_with_right_toast:[StringUtils get_language_label:process_name] title:[StringUtils get_language_label:title_name] image:@"AppIcon76x76"];
    
    });
}

-(void)notify_with_right_toast:(NSString*)process_name title:(NSString*)title_name image:(NSString*)image_name;
{
    dispatch_async(dispatch_get_main_queue(),^{

        if([image_name isEqualToString:@"activity_indicator"])
        {
            [self.window makeToast:[StringUtils get_language_label:process_name] duration:2.0 position:CSToastPositionTopRight title:[StringUtils get_language_label:title_name] image:[UIImage animatedImageNamed:@"spinner-" duration:1.0f] style:nil completion:nil];
        }
        else
        {
            [self.window makeToast:[StringUtils get_language_label:process_name] duration:2.0 position:CSToastPositionTopRight title:[StringUtils get_language_label:title_name] image:[UIImage imageNamed:image_name] style:nil completion:nil];
        }
    });
}

-(void)notify_with_right_toast:(NSString*)process_name title:(NSString*)title_name duration:(double) duration;
{
    dispatch_async(dispatch_get_main_queue(),^{

        [self.window makeToast:[StringUtils get_language_label:process_name] duration:duration position:CSToastPositionTopRight title:[StringUtils get_language_label:title_name] image:[UIImage imageNamed:@"AppIcon76x76"] style:nil completion:nil];
    });
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    //Capture background enter time
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
    NSString *current_time = [dateFormatter stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults]setObject:current_time forKey:@"background_time"];
    
    [self activate_pos_as_beacon];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /* Checking whether merchant data syncing upto-date - Also purpose of checking subscription validation during login with internet connection
     1. Check whether app in background mode for more than 2-min
     2. If YES -  Check merchant data synced to cloud upto date - If YES no need to do anything
     3. If NO  -  Check how many days it's not been syned to cloud
     4. If more than SEVEN days - POPTO loginview and ask user to login again with internet connection.
     */
        
//    NSString *last_bacground_time = [[NSUserDefaults standardUserDefaults] objectForKey:@"background_time"];
//    
//    if(![last_bacground_time isEqualToString:@""] && last_bacground_time != nil && last_bacground_time != (id)[NSNull null])
//    {
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
//        NSDate *last_detect_time = [dateFormatter dateFromString:last_bacground_time];
//        
//        NSArray *day_mins = [[UIUtils get_days_minutes_different:last_detect_time] componentsSeparatedByString:@"-"];
//        
//        if([[day_mins objectAtIndex:1] intValue] >= 2) // if more than 2 min
//        {
//            if([UIUtils is_data_not_syned_seven_days])
//            {
//                UIAlertController *dataNotSyncedAlertController = [UIAlertController alertControllerWithTitle:@"Data not synced" message:@"Your data not synced for last seven days.Please login again with internet connection" preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *dismissAlertAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault handler:nil];
//                [dataNotSyncedAlertController addAction:dismissAlertAction];
//                [self.window.rootViewController presentViewController:dataNotSyncedAlertController animated:YES completion:nil];
//                [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
//            }
//            else
//            {
//                //sync batch here
//            }
//        }
//    }
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    [self activate_pos_as_beacon];
}

#pragma - Register Push Notification

- (void)register_push_notification
{
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0"))
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound + UNAuthorizationOptionAlert + UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if( !error )
             {
                 dispatch_async(dispatch_get_main_queue(),^{
                     [[UIApplication sharedApplication] registerForRemoteNotifications];
                 });
             }
             if (!granted)
             {
                 
             }
         }];
    }
    else
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

#pragma mark - iOS-10 User Notification delegate methods

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    //Called when a notification is delivered to a foreground app.
    
    NSLog(@"Userinfo %@",notification.request.content.userInfo);
    
    if(notification.request.content.userInfo != nil)
    {
        // iOS-10 version showing default top notification while app in active mode - So no need this code
        
        /*  UIApplicationState state    = [[UIApplication sharedApplication] applicationState];
         if (state == UIApplicationStateActive)
         {
            [self.window makeToast:@"Push Notification" duration:2.0 position:CSToastPositionTopRight title:@"Notification" image:[UIImage imageNamed:@"AppIcon76x76"] style:nil completion:nil];
         } */
        //[self sync_all_using_push_data:[notification.request.content.userInfo objectForKey:@"data"]];
        [self clear_all_notifactions_from_noticaftioncenter];
    }
    completionHandler(UNNotificationPresentationOptionAlert);
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
    //Called to let your app know which action was selected by the user for a given notification.
    
    NSLog(@"Userinfo %@",response.notification.request.content.userInfo);
    if(response.notification.request.content.userInfo != nil)
    {
        // iOS-10 version showing default top notification while app in active mode - So no need this code
        
        /*  UIApplicationState state    = [[UIApplication sharedApplication] applicationState];
         if (state == UIApplicationStateActive)
         {
         [self.window makeToast:@"Push Notification" duration:2.0 position:CSToastPositionTopRight title:@"Notification" image:[UIImage imageNamed:@"AppIcon76x76"] style:nil completion:nil];
         } */
        //[self sync_all_using_push_data:[response.notification.request.content.userInfo objectForKey:@"data"]];
        [self clear_all_notifactions_from_noticaftioncenter];
    }
}

-(void)clear_all_notifactions_from_noticaftioncenter
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

#pragma mark - Call sync function based on Push notification data


#pragma mark - Activity Indicator

- (void)show_indicator
{
    [MBProgressHUD showHUDAddedTo:self.window animated:YES];
}

- (void)hide_indicator
{
    [MBProgressHUD hideHUDForView:self.window animated:YES];
}

- (void)setup_db
{    
    DatabaseMigrator *migrator  = [[DatabaseMigrator alloc] initWithDatabaseFile:[AbstractRepository databaseFilename]];
    [migrator moveDatabaseToUserDirectoryIfNeeded];
    [migrator migrateToVersion:CURRENT_DB_VERSION];
}

- (void)setup_images_directory
{
    NSString *str   = [FileUtils documentsDirectoryPath];
    BOOL isSuccess  = [FileUtils createNewDirectoryAtPath:str andName:POSImages];
    if (isSuccess)
    {
        [FileUtils createNewDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",str,POSImages] andName:IMG_BUSINESS];
        [FileUtils createNewDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",str,POSImages] andName:IMG_MODULES];
        [FileUtils createNewDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",str,POSImages] andName:IMG_CUSTOMERS];
        [FileUtils createNewDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",str,POSImages] andName:IMG_CATEGORIES];
        [FileUtils createNewDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",str,POSImages] andName:IMG_ITEMS];
        [FileUtils createNewDirectoryAtPath:[NSString stringWithFormat:@"%@/%@",str,POSImages] andName:IMG_USERS];
    }
}

- (void) handle_exception:(NSString *)exception_error withmodule:(NSString *)module withfunc:(NSString *)func
{
//    NSArray *error_data =
//    @[
//        @{
//          @"error_desc"            : exception_error ? exception_error : @"",
//          @"error_module"          : module ? module : @"",
//          @"error_function_name"   : func ? func : @"",
//          }
//      ];
//    [Shareddb insert_error_details:error_data[0]]; 
//
//    [Sharedservices send_error_report];
//
//    NSLog(@"%@ %@",func,exception_error);
}

#pragma mark - Write logs into text file

- (void)write_logs_into_file
{
    NSString *docDirectory = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *logPath = [docDirectory stringByAppendingPathComponent:@"console.txt"];
    freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding], "a+", stderr);
}

#pragma - Cocoa MQTT fucntion

-(void)start_mqtt:(BOOL)is_connection_test
{
    NSString *client_id    = [UIDevice currentDevice].identifierForVendor.UUIDString;
    self.cocoa_mqtt = [[ CocoaMQTT alloc] initWithClientID:client_id host:mqtt_server_ip port:11883];
    self.cocoa_mqtt.username = @"Ebizu";
    self.cocoa_mqtt.username = @"public";
    self.cocoa_mqtt.keepAlive = 90;
    self.cocoa_mqtt.delegate = self;
    if(is_connection_test)
        [self.cocoa_mqtt connect];
}

- (void)listen_mqtt_incoming;
{
    
}

-(void)publish_message:(NSString *)data
{
//    if(self.mqtt_connected == YES)
//    {
//        //Publish message from here
//        // [self.cocoa_mqtt.willMessage initWithTopic:mqtt_topic   message:data];
//    }
//    
//    else
//    {
//        DebugLog(@"MQTT Not Connected")
//    }
}

#pragma mark - Cocoa MQTT Delegate methods


-(void)mqttDidDisconnect:(CocoaMQTT *)mqtt withError:(NSError *)err
{
    //Error handler
}
-(void)mqtt:(CocoaMQTT *)mqtt didConnect:(NSString *)host port:(NSInteger)port
{
    //Check MQTT connected  
    NSLog(@"%@-%ld",host,(long)port);
}
-(void)mqttDidPing:(CocoaMQTT *)mqtt
{
    
}

- (void)mqtt:(CocoaMQTT * _Nonnull)mqtt didReceiveMessage:(CocoaMQTTMessage * _Nonnull)message id:(uint16_t)id
{
    //Handle receiver messages here
}

-(void)mqtt:(CocoaMQTT *)mqtt didSubscribeTopic:(NSString *)topic
{
    
}

- (void)mqtt:(CocoaMQTT * _Nonnull)mqtt didPublishMessage:(CocoaMQTTMessage * _Nonnull)message id:(uint16_t)id;
{
    
}

-(void)mqtt:(CocoaMQTT *)mqtt didPublishAck:(uint16_t)id
{
    
}

-(void)mqttDidReceivePong:(CocoaMQTT *)mqtt
{
    
}

- (void)mqtt:(CocoaMQTT * _Nonnull)mqtt didUnsubscribeTopic:(NSString * _Nonnull)topic;
{
    
}


@end
