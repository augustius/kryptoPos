-- MIGRATION SCRIPT FOR 1.7

UPDATE TRN_ITEMS SET sync_id = 'migration_sync_id' WHERE TRN_ITEMS.trx_id IN (SELECT trx_id FROM TRANSACTIONS WHERE (sync_id != '' AND sync_id IS NOT NULL));

UPDATE TRN_MODIFIERS SET sync_id = 'migration_sync_id' WHERE TRN_MODIFIERS.trx_item_id IN (SELECT trx_item_id FROM TRN_ITEMS WHERE (sync_id != '' AND sync_id IS NOT NULL));

UPDATE REFUND_TRN_ITEMS SET sync_id = 'migration_sync_id' WHERE REFUND_TRN_ITEMS.trx_id IN (SELECT trx_id FROM REFUND WHERE ( REFUND.create_time == REFUND_TRN_ITEMS.created_date AND sync_id != '' AND sync_id IS NOT NULL));

UPDATE REFUND_TRN_MODIFIERS SET sync_id = 'migration_sync_id' WHERE REFUND_TRN_MODIFIERS.trx_item_id IN (SELECT trx_item_id FROM REFUND_TRN_ITEMS WHERE ( REFUND_TRN_ITEMS.created_date == REFUND_TRN_MODIFIERS.created_date AND sync_id != '' AND sync_id IS NOT NULL));

--

CREATE TABLE tmp_TRANSACTIONS AS SELECT * FROM TRANSACTIONS;

DROP TABLE TRANSACTIONS;

CREATE TABLE `TRANSACTIONS` (
`id`                   		VARCHAR PRIMARY KEY,
`poh_total`                 VARCHAR,
`poh_lpt_id`                VARCHAR,
`poh_mem_id`                VARCHAR,
`poh_transaction_type`      VARCHAR,
`poh_pyt_name`              VARCHAR,
`poh_close_datetime`        VARCHAR,
`poh_mem_screen_name`       VARCHAR,
`poh_offer_sn`              VARCHAR,
`poh_subtotal`              VARCHAR,
`poh_cus_id`      	        VARCHAR,
`poh_discount_amount`       VARCHAR,
`poh_currency`              VARCHAR,
`poh_tax_amount`            VARCHAR,
`poh_create_datetime`       VARCHAR,
`poh_discount_percentage`   VARCHAR,
`tran_sync_id`              VARCHAR,
`poh_usr_id`                VARCHAR,
`poh_order_id`              VARCHAR DEFAULT '(NULL)',
`poh_table_number`          VARCHAR,
`poh_notes`                 VARCHAR,
`poh_transaction_status`    VARCHAR,
`poh_payment_method_id`     VARCHAR,
`poh_discount_id`     	    VARCHAR,
`poh_voucher_id`            VARCHAR,
`poh_voucher_name`          VARCHAR,
`poh_voucher_amount`        DECIMAL(13, 4),
`poh_voucher_type_id`       VARCHAR,
`poh_voucher_type_name`     VARCHAR,
`poh_voucher_member_number` VARCHAR,
`poh_voucher_ref_number`    VARCHAR,
`poh_counter_number`        INT,
`poh_rounding_adj`          DECIMAL(13, 4),
`poh_customer_gstid`        VARCHAR,
`poh_customer_email`        VARCHAR,
`poh_customer_phone`        VARCHAR,
`poh_customer_pincode`      VARCHAR,
`poh_customer_city`         VARCHAR,
`poh_customer_address1`     VARCHAR,
`poh_customer_address2`     VARCHAR,
`poh_gst_mode`              VARCHAR,
`poh_total_gst_value`       DECIMAL(13, 4),
`poh_trans_number`          INTEGER,
`poh_pvd_id` 			    VARCHAR,
`poh_custom_field`          VARCHAR,
`poh_com_id`                INTEGER DEFAULT NULL,
`sync_error_message`        VARCHAR,
`sync_id`                   VARCHAR,
`last_sync_time`            TIMESTAMP DEFAULT (strftime('%s','now')),
`poh_created_by`            INT,
`poh_created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`poh_updated_by`            INT,
`poh_updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`poh_active_status`         INT DEFAULT '1',
`poh_sales_person_id`       VARCHAR,
`poh_sales_person_name`     VARCHAR,
`poh_no_of_pax`             INT DEFAULT 0,
`poh_customer_review`       INT DEFAULT 0
);

INSERT INTO TRANSACTIONS SELECT * FROM tmp_TRANSACTIONS;

DROP TABLE tmp_TRANSACTIONS;

ALTER TABLE TRANSACTIONS ADD poh_tax_percentage DECIMAL(13, 4) DEFAULT 0;
ALTER TABLE TRANSACTIONS ADD poh_discount_description VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_usr_name VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_payment_amount DECIMAL(13, 4) DEFAULT 0;
ALTER TABLE TRANSACTIONS ADD poh_payment_changes DECIMAL(13, 4) DEFAULT 0;
ALTER TABLE TRANSACTIONS ADD poh_payment_method_action VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_payment_type_id VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_payment_gateway_id VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_approval_code VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_card_number VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_table_area VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_table_shape VARCHAR;
ALTER TABLE TRANSACTIONS ADD poh_table_chair VARCHAR;

--

CREATE TABLE tmp_TRN_ITEMS AS SELECT * FROM TRN_ITEMS;

DROP TABLE TRN_ITEMS;

CREATE TABLE `TRN_ITEMS` (
`id`           				VARCHAR PRIMARY KEY,
`pod_poh_sn`            	VARCHAR,
`pod_item_id`           	VARCHAR,
`pod_item`        		 	VARCHAR,
`pod_qty`               	VARCHAR,
`pod_price`             	VARCHAR,
`pod_price_goods`       	DECIMAL(13, 4),
`pod_selling_price`     	DECIMAL(13, 4),
`pod_remarks`          	 	VARCHAR,
`pod_discount_amount`      	DECIMAL(13, 4),
`pod_discount_id`   		VARCHAR,
`pod_is_edited_item_price`  VARCHAR,
`pod_type_of_price`         VARCHAR,
`pod_type_of_unit`          VARCHAR,
`pod_stock_monitoring`      INTEGER,
`pod_gst_code`              VARCHAR,
`pod_gst_rate`              INTEGER,
`pod_gst_value`             DECIMAL(13, 4),
`pod_uom_qty`               DECIMAL(13, 4),
`pod_com_id`            	INTEGER DEFAULT NULL,
`sync_id`               	VARCHAR,
`last_sync_time`        	TIMESTAMP DEFAULT (strftime('%s','now')),
`pod_created_by`            INT,
`pod_created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`pod_updated_by`            INT,
`pod_updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`pod_active_status`         INT DEFAULT '1'
);

INSERT INTO TRN_ITEMS SELECT * FROM tmp_TRN_ITEMS;

DROP TABLE tmp_TRN_ITEMS;

ALTER TABLE TRN_ITEMS ADD pod_discount_description VARCHAR;

--

CREATE TABLE tmp_TRN_MODIFIERS AS SELECT * FROM TRN_MODIFIERS;

DROP TABLE TRN_MODIFIERS;

CREATE TABLE `TRN_MODIFIERS`
(
`id`               		VARCHAR,
`pom_pod_sn`       		VARCHAR,
`pom_name`          	VARCHAR,
`pom_price`         	DECIMAL(13, 4),
`pom_type`          	VARCHAR,
`pom_mod_id`            VARCHAR,
`pom_mod_parent_id`     VARCHAR,
`pom_mod_parent_name`	VARCHAR,
`pom_mod_amount`        INTEGER,
`pom_mod_qty`           INTEGER,
`pom_mod_item_id`       INTEGER,
`pom_com_id`        	INTEGER DEFAULT NULL,
`sync_id`           	VARCHAR,
`last_sync_time`		TIMESTAMP DEFAULT (strftime('%s','now')),
`pom_created_by`        INT,
`pom_created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`pom_updated_by`        INT,
`pom_updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`pom_active_status`     INT DEFAULT '1'
);

INSERT INTO TRN_MODIFIERS SELECT * FROM tmp_TRN_MODIFIERS;

DROP TABLE tmp_TRN_MODIFIERS;

ALTER TABLE TRN_MODIFIERS ADD pom_poh_sn VARCHAR;


--

CREATE TABLE tmp_REFUND AS SELECT * FROM REFUND;

DROP TABLE REFUND;

CREATE TABLE `REFUND`
(
`id`          		    	VARCHAR PRIMARY KEY,
`phr_amount`        		DECIMAL(13, 4),
`phr_total_amount`      	DECIMAL(13, 4),
`phr_rounding_amount`  	 	DECIMAL(13, 4),
`phr_tax_amount`        	DECIMAL(13, 4),
`phr_gst_amount`        	DECIMAL(13, 4),
`phr_discount_amount`   	DECIMAL(13, 4),
`phr_custom_discount_id`    VARCHAR,
`phr_voucher_amount`        DECIMAL(13, 4),
`phr_voucher_id`            VARCHAR,
`phr_poh_sn`                VARCHAR,
`phr_order_id`              VARCHAR,
`phr_create_datetime`      	VARCHAR,
`tran_sync_id`          	VARCHAR,
`phr_remarks`               VARCHAR,
`phr_com_id`           	    INTEGER DEFAULT NULL,
`sync_id`              	 	VARCHAR,
`last_sync_time`        	TIMESTAMP DEFAULT (strftime('%s','now')),
`phr_created_by`           	VARCHAR,
`phr_create_date`         	TIMESTAMP DEFAULT (strftime('%s','now')),
`phr_updated_by`          	VARCHAR,
`phr_updated_date`        	TIMESTAMP DEFAULT (strftime('%s','now')),
`phr_user_id`               VARCHAR,
`phr_active_status`         INT DEFAULT '1',
`phr_sales_person_id`       VARCHAR,
`phr_sales_person_name`     VARCHAR,
`phr_no_of_pax`             INT DEFAULT 0
);

INSERT INTO REFUND SELECT * FROM tmp_REFUND;

DROP TABLE tmp_REFUND;

--

CREATE TABLE tmp_REFUND_TRN_ITEMS AS SELECT * FROM REFUND_TRN_ITEMS;

DROP TABLE REFUND_TRN_ITEMS;

CREATE TABLE `REFUND_TRN_ITEMS`
(
`id`                   		VARCHAR PRIMARY KEY,
`phd_poh_sn`                VARCHAR,
`phd_item_id`               VARCHAR,
`phd_pod_sn`          	    VARCHAR,
`phd_item_name`             VARCHAR,
`phd_item_qty`              VARCHAR,
`phd_refund_item_qty`       INTEGER,
`phd_item_price`            VARCHAR,
`phd_cost_price`            DECIMAL(13, 4),
`phd_selling_price`         DECIMAL(13, 4),
`phd_refund_item_price`     DECIMAL(13, 4),
`phd_gst_value`             DECIMAL(13, 4),
`phd_refund_gst_value`      DECIMAL(13, 4),
`phd_gst_code`              VARCHAR,
`phd_gst_rate`              INTEGER,
`phd_item_remarks`          VARCHAR,
`phd_item_discount`         DECIMAL(13, 4),
`phd_custom_discount_id`	VARCHAR,
`phd_stock_monitoring`      INTEGER,
`phd_item_checked`          INTEGER DEFAULT 0,
`phd_com_id`                INTEGER DEFAULT NULL,
`sync_id`               	VARCHAR,
`last_sync_time`        	TIMESTAMP DEFAULT (strftime('%s','now')),
`phd_created_by`            INT,
`phd_created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`phd_updated_by`            INT,
`phd_updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`phd_active_status`         INT DEFAULT '1'
);

INSERT INTO REFUND_TRN_ITEMS SELECT * FROM tmp_REFUND_TRN_ITEMS;

DROP TABLE tmp_REFUND_TRN_ITEMS;

ALTER TABLE REFUND_TRN_ITEMS ADD phd_phr_id VARCHAR;

--

CREATE TABLE tmp_REFUND_TRN_MODIFIERS AS SELECT * FROM REFUND_TRN_MODIFIERS;

DROP TABLE REFUND_TRN_MODIFIERS;

CREATE TABLE `REFUND_TRN_MODIFIERS`
(
`id`                	VARCHAR PRIMARY KEY,
`prm_pod_sn`       		VARCHAR,
`prm_mod_name`      	VARCHAR,
`prm_mod_price`         DECIMAL(13, 4),
`prm_mod_type`          VARCHAR,
`prm_mod_id`            VARCHAR,
`prm_mod_parent_id`     VARCHAR,
`prm_parent_name`		VARCHAR,
`prm_mod_amount`        INTEGER,
`prm_mod_qty`           INTEGER,
`prm_mod_item_id`      	INTEGER,
`prm_com_id`        	INTEGER DEFAULT NULL,
`sync_id`           	VARCHAR,
`last_sync_time`		TIMESTAMP DEFAULT (strftime('%s','now')),
`prm_created_by`        INT,
`prm_created_datetime`  TIMESTAMP DEFAULT (strftime('%s','now')),
`prm_updated_by`        INT,
`prm_updated_datetime`  TIMESTAMP DEFAULT (strftime('%s','now')),
`prm_active_status`     INT DEFAULT '1'
);

INSERT INTO REFUND_TRN_MODIFIERS SELECT * FROM tmp_REFUND_TRN_MODIFIERS;

DROP TABLE tmp_REFUND_TRN_MODIFIERS;

ALTER TABLE REFUND_TRN_MODIFIERS ADD prm_phr_id VARCHAR;
ALTER TABLE REFUND_TRN_MODIFIERS ADD prm_phd_id VARCHAR;
ALTER TABLE REFUND_TRN_MODIFIERS ADD prm_poh_sn VARCHAR;

--

CREATE TABLE tmp_TRANSACTION_MULTIPLE_PAYMENTS AS SELECT * FROM TRANSACTION_MULTIPLE_PAYMENTS;

DROP TABLE TRANSACTION_MULTIPLE_PAYMENTS;

CREATE TABLE `TRANSACTION_MULTIPLE_PAYMENTS`
(
`id`                   	 	VARCHAR PRIMARY KEY,
`phm_poh_sn`                VARCHAR,
`phm_current_paid_amount`	DOUBLE,
`phm_total_amount`          DOUBLE,
`phm_pay_name`              VARCHAR,
`phm_payment_type_id`       VARCHAR,
`phm_payment_type_name`     VARCHAR,
`phm_com_id`           		INTEGER DEFAULT NULL,
`sync_id`               	VARCHAR,
`last_sync_time`        	TIMESTAMP DEFAULT (strftime('%s','now')),
`phm_created_by`            INT,
`phm_created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`phm_updated_by`            INT,
`phm_updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`phm_active_status`         INT DEFAULT '1',
`phm_pay_method_id`         INT
);

INSERT INTO TRANSACTION_MULTIPLE_PAYMENTS SELECT * FROM tmp_TRANSACTION_MULTIPLE_PAYMENTS;

DROP TABLE tmp_TRANSACTION_MULTIPLE_PAYMENTS;

