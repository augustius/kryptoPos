-- MIGRATION SCRIPT FOR 2.0

CREATE TABLE tmp_TRN_MODIFIERS AS SELECT * FROM TRN_MODIFIERS GROUP BY id;

DROP TABLE TRN_MODIFIERS;

CREATE TABLE `TRN_MODIFIERS`
(
`id`                    VARCHAR PRIMARY KEY,
`pom_pod_sn`            VARCHAR,
`pom_name`              VARCHAR,
`pom_price`             DECIMAL(13, 4),
`pom_type`              VARCHAR,
`pom_mod_id`            VARCHAR,
`pom_mod_parent_id`     VARCHAR,
`pom_mod_parent_name`   VARCHAR,
`pom_mod_amount`        INTEGER,
`pom_mod_qty`           INTEGER,
`pom_mod_item_id`       INTEGER,
`pom_com_id`            INTEGER DEFAULT NULL,
`sync_id`               VARCHAR,
`last_sync_time`        TIMESTAMP DEFAULT (strftime('%s','now')),
`pom_created_by`        INT,
`pom_created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`pom_updated_by`        INT,
`pom_updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`pom_active_status`     INT DEFAULT '1',
`pom_poh_sn`            VARCHAR
);

INSERT INTO TRN_MODIFIERS SELECT * FROM tmp_TRN_MODIFIERS;

DROP TABLE tmp_TRN_MODIFIERS;

--

ALTER TABLE TABLES ADD tbl_sequence_no INT DEFAULT '1';

UPDATE TABLES SET area_id = (SELECT AREA.id FROM AREA WHERE AREA.area_name = TABLES.table_area LIMIT 1);

--

ALTER TABLE AREA ADD tba_sequence_no INT DEFAULT '1';

--

ALTER TABLE TRANSACTIONS ADD poh_order_type INT DEFAULT '0';

--

ALTER TABLE TRANSACTION_MULTIPLE_PAYMENTS ADD phm_ref_no VARCHAR;

--

ALTER TABLE MEMBERS ADD cust_image_changed INT DEFAULT 0;








