-- MIGRATION SCRIPT FOR 1.5.5

-- // CODE FOR FULL COLUMN RENAMING, USE WITH CAUTION. //

-- Code for column changes

-- CREATE TABLE tmp_ NAME_OF_TABLE_STATEMENT AS SELECT * FROM NAME_OF_TABLE_STATEMENT;

-- DROP TABLE NAME_OF_TABLE_STATEMENT;

-- CREATE TABLE NAME_OF_TABLE_STATEMENT;

-- INSERT INTO NAME_OF_TABLE_STATEMENT SELECT * FROM tmp_ NAME_OF_TABLE_STATEMENT;

-- DROP TABLE tmp_ NAME_OF_TABLE_STATEMENT;

CREATE TABLE tmp_AREA AS SELECT * FROM AREA;

DROP TABLE AREA;

CREATE TABLE IF NOT EXISTS `AREA`
(
`id`                    VARCHAR,
`area_name`             VARCHAR(50),
`sync_id`               VARCHAR,
`last_sync_time`        DATETIME,
`created_by`            INT,
`created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`            INT,
`updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`         INT DEFAULT '1',
PRIMARY KEY(id)
);

INSERT INTO AREA SELECT * FROM tmp_AREA;

DROP TABLE tmp_AREA;

-----

CREATE TABLE tmp_TABLES AS SELECT * FROM TABLES;

DROP TABLE TABLES;

CREATE TABLE IF NOT EXISTS `TABLES`
(
`id`                VARCHAR PRIMARY KEY,
`table_no`          VARCHAR(50),
`total_chair`       INTEGER,
`table_area`        VARCHAR(50),
`table_x`           DOUBLE,
`table_y`           DOUBLE,
`table_color`       VARCHAR DEFAULT '0xFEFA29',
`table_shape`       VARCHAR(50),
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`	TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1',
`order_id`          VARCHAR,
`total_amount`      VARCHAR
);

INSERT INTO TABLES SELECT * FROM tmp_TABLES;

DROP TABLE tmp_TABLES;

--

CREATE TABLE tmp_CANCELLED_ORDER_ITEMS AS SELECT * FROM CANCELLED_ORDER_ITEMS;

DROP TABLE CANCELLED_ORDER_ITEMS;

CREATE TABLE IF NOT EXISTS `CANCELLED_ORDER_ITEMS` (
`id` VARCHAR,
`order_id`          VARCHAR,
`item_name`         VARCHAR,
`item_id`           VARCHAR,
`item_qty`          INTEGER,
`item_price`        DECIMAL(13, 4),
`item_remarks`      VARCHAR,
`item_totalPrice`   DECIMAL(13, 4),
`item_discount`     DECIMAL(13, 4),
`item_checked`      INTEGER DEFAULT '0',
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`    TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

INSERT INTO CANCELLED_ORDER_ITEMS SELECT * FROM tmp_CANCELLED_ORDER_ITEMS;

DROP TABLE tmp_CANCELLED_ORDER_ITEMS;

--

CREATE TABLE tmp_CANCELLED_ORDER_MODIFIERS AS SELECT * FROM CANCELLED_ORDER_MODIFIERS;

DROP TABLE CANCELLED_ORDER_MODIFIERS;

CREATE TABLE IF NOT EXISTS `CANCELLED_ORDER_MODIFIERS`
(
`id`                INTEGER PRIMARY KEY AUTOINCREMENT,
`item_id`           INTEGER,
`mod_id`            VARCHAR,
`mod_name`          VARCHAR,
`mod_price`         DECIMAL(13, 4),
`mod_type`          VARCHAR NOT NULL,
`mod_parent_name`	VARCHAR,
`mod_selected`      VARCHAR,
`mod_parent_id`     VARCHAR,
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`	TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

INSERT INTO CANCELLED_ORDER_MODIFIERS SELECT * FROM tmp_CANCELLED_ORDER_MODIFIERS;

DROP TABLE tmp_CANCELLED_ORDER_MODIFIERS;


--

CREATE TABLE tmp_HOLD_ORDER_ITEMS AS SELECT * FROM HOLD_ORDER_ITEMS;

DROP TABLE HOLD_ORDER_ITEMS;

CREATE TABLE IF NOT EXISTS `HOLD_ORDER_ITEMS`
(
`id`                        VARCHAR,
`order_id`                  VARCHAR,
`item_name`                 VARCHAR,
`item_id`                   VARCHAR,
`item_qty`                  VARCHAR,
`split_item_qty`            VARCHAR,
`item_price`                DECIMAL(13, 4),
`cost_price`                DECIMAL(13, 4),
`selling_price`             DECIMAL(13, 4),
`item_remarks`              VARCHAR,
`item_totalPrice`           DECIMAL(13, 4),
`item_discount`             DECIMAL(13, 4),
`uom_qty`                   DECIMAL(13, 4),
`item_checked`              INTEGER DEFAULT '0',
`kitchen_printer_checked`	INTEGER DEFAULT '0',
`active_split_qty`          INTEGER DEFAULT '0',
`custom_discount_id`        VARCHAR,
`stock_monitoring`          INTEGER,
`item_category_id`          INTEGER DEFAULT 0,
`gst_code`                  VARCHAR,
`gst_rate`                  INTEGER,
`gst_value`                 DECIMAL(13, 4),
`is_edited_item_price`      VARCHAR,
`type_of_price`             VARCHAR,
`type_of_unit`              VARCHAR,
`company_id`                INTEGER DEFAULT NULL,
`sync_id`                   VARCHAR,
`last_sync_time`            TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`                INT,
`created_date`              TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`                INT,
`updated_date`              TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`             INT DEFAULT '1'
);

INSERT INTO HOLD_ORDER_ITEMS SELECT * FROM tmp_HOLD_ORDER_ITEMS;

DROP TABLE tmp_HOLD_ORDER_ITEMS;

--

CREATE TABLE tmp_HOLD_ORDER_MODIFIERS AS SELECT * FROM HOLD_ORDER_MODIFIERS;

DROP TABLE HOLD_ORDER_MODIFIERS;

CREATE TABLE IF NOT EXISTS `HOLD_ORDER_MODIFIERS`
(
`id`                    VARCHAR,
`hold_order_item_id`    INTEGER,
`mod_id`                VARCHAR,
`mod_name`              VARCHAR,
`mod_price`             DECIMAL(13, 4),
`mod_type`              VARCHAR NOT NULL,
`mod_parent_name`       VARCHAR,
`mod_selected`          VARCHAR,
`mod_parent_id`         VARCHAR,
`mod_item_id`           INTEGER,
`mod_qty`               INTEGER,
`selected_index`        INTEGER,
`company_id`            INTEGER DEFAULT NULL,
`sync_id`               VARCHAR,
`last_sync_time`        TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`            INT,
`created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`            INT,
`updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`         INT DEFAULT '1'
);

INSERT INTO HOLD_ORDER_MODIFIERS SELECT * FROM tmp_HOLD_ORDER_MODIFIERS;

DROP TABLE tmp_HOLD_ORDER_MODIFIERS;

--

CREATE TABLE tmp_REFUND_TRN_ITEMS AS SELECT * FROM REFUND_TRN_ITEMS;

DROP TABLE REFUND_TRN_ITEMS;

CREATE TABLE IF NOT EXISTS `REFUND_TRN_ITEMS`
(
`id`                    VARCHAR,
`trx_id`                VARCHAR,
`item_id`               VARCHAR,
`trx_item_id`           VARCHAR,
`item_name`             VARCHAR,
`item_qty`              VARCHAR,
`refund_item_qty`       INTEGER,
`item_price`            VARCHAR,
`cost_price`            DECIMAL(13, 4),
`selling_price`         DECIMAL(13, 4),
`refund_item_price`     DECIMAL(13, 4),
`gst_value`             DECIMAL(13, 4),
`refund_gst_value`      DECIMAL(13, 4),
`gst_code`              VARCHAR,
`gst_rate`              INTEGER,
`item_remarks`          VARCHAR,
`item_discount`         DECIMAL(13, 4),
`custom_discount_id`	VARCHAR,
`stock_monitoring`      INTEGER,
`item_checked`          INTEGER DEFAULT 0,
`company_id`            INTEGER DEFAULT NULL,
`sync_id`               VARCHAR,
`last_sync_time`        TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`            INT,
`created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`            INT,
`updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`         INT DEFAULT '1'
);

INSERT INTO REFUND_TRN_ITEMS SELECT * FROM tmp_REFUND_TRN_ITEMS;

DROP TABLE tmp_REFUND_TRN_ITEMS;

--

CREATE TABLE tmp_REFUND_TRN_MODIFIERS AS SELECT * FROM REFUND_TRN_MODIFIERS;

DROP TABLE REFUND_TRN_MODIFIERS;

CREATE TABLE IF NOT EXISTS `REFUND_TRN_MODIFIERS`
(
`id`                VARCHAR,
`trx_item_id`       VARCHAR,
`mod_name`          VARCHAR,
`mod_price`         DECIMAL(13, 4),
`mod_type`          VARCHAR,
`mod_id`            VARCHAR,
`mod_parent_id`     VARCHAR,
`mod_parent_name`	VARCHAR,
`mod_amount`        INTEGER,
`mod_qty`           INTEGER,
`mod_item_id`       INTEGER,
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`	TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

INSERT INTO REFUND_TRN_MODIFIERS SELECT * FROM tmp_REFUND_TRN_MODIFIERS;

DROP TABLE tmp_REFUND_TRN_MODIFIERS;

--

CREATE TABLE tmp_TAXES AS SELECT * FROM TAXES;

DROP TABLE TAXES;

CREATE TABLE IF NOT EXISTS `TAXES`
(
`id`                VARCHAR,
`tax_name`          VARCHAR,
`tax_percentage`	VARCHAR,
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`	TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

INSERT INTO TAXES SELECT * FROM tmp_TAXES;

DROP TABLE tmp_TAXES;

--

CREATE TABLE tmp_TRN_MODIFIERS AS SELECT * FROM TRN_MODIFIERS;

DROP TABLE TRN_MODIFIERS;

CREATE TABLE IF NOT EXISTS `TRN_MODIFIERS`
(
`id`                VARCHAR,
`trx_item_id`       VARCHAR,
`mod_name`          VARCHAR,
`mod_price`         DECIMAL(13, 4),
`mod_type`          VARCHAR,
`mod_id`            VARCHAR,
`mod_parent_id`     VARCHAR,
`mod_parent_name`	VARCHAR,
`mod_amount`        INTEGER,
`mod_qty`           INTEGER,
`mod_item_id`       INTEGER,
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`	TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

INSERT INTO TRN_MODIFIERS SELECT * FROM tmp_TRN_MODIFIERS;

DROP TABLE tmp_TRN_MODIFIERS;

--

CREATE TABLE tmp_USERRIGHTSMODULES AS SELECT * FROM USERRIGHTSMODULES;

DROP TABLE USERRIGHTSMODULES;

CREATE TABLE IF NOT EXISTS `USERRIGHTSMODULES`
(
`item_id`           INTEGER PRIMARY KEY AUTOINCREMENT,
`user_id`           INTEGER,
`module_id`         INTEGER,
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`	TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

INSERT INTO USERRIGHTSMODULES SELECT * FROM tmp_USERRIGHTSMODULES;

DROP TABLE tmp_USERRIGHTSMODULES;

ALTER TABLE USERRIGHTS ADD is_sales_person INT;

ALTER TABLE HOLD_ORDER ADD sales_person_id VARCHAR;
ALTER TABLE HOLD_ORDER ADD sales_person_name VARCHAR;
ALTER TABLE HOLD_ORDER ADD no_of_pax INT DEFAULT 0;
ALTER TABLE HOLD_ORDER ADD is_order_merged INT DEFAULT 0;
ALTER TABLE HOLD_ORDER ADD tbl_primary_id VARCHAR;
ALTER TABLE HOLD_ORDER ADD is_locked INTEGER DEFAULT 0;

ALTER TABLE HOLD_ORDER_ITEMS ADD printed_qty INT DEFAULT 0;

ALTER TABLE REFUND ADD sales_person_id VARCHAR;
ALTER TABLE REFUND ADD sales_person_name VARCHAR;
ALTER TABLE REFUND ADD no_of_pax INT DEFAULT 0;

ALTER TABLE SETTING ADD mqtt_model_name VARCHAR;
ALTER TABLE SETTING ADD mqtt_server_ip VARCHAR;
ALTER TABLE SETTING ADD is_multi_device INT DEFAULT 0;
ALTER TABLE SETTING ADD is_mqtt_enabled INT DEFAULT 0;
ALTER TABLE SETTING ADD currency VARCHAR;
ALTER TABLE SETTING ADD currency_symbol VARCHAR;
ALTER TABLE SETTING ADD gst_id VARCHAR;
ALTER TABLE SETTING ADD service_charge DECIMAL(13, 4);
ALTER TABLE SETTING ADD gst_percentage DECIMAL(13, 4);

ALTER TABLE TABLES ADD area_id VARCHAR;
ALTER TABLE TABLES ADD merged_into VARCHAR;

ALTER TABLE TRANSACTIONS ADD sales_person_id VARCHAR;
ALTER TABLE TRANSACTIONS ADD sales_person_name VARCHAR;
ALTER TABLE TRANSACTIONS ADD no_of_pax INT DEFAULT 0;
ALTER TABLE TRANSACTIONS ADD customer_review INT DEFAULT 0;


ALTER TABLE USERRIGHTSMODULES ADD role_id INT DEFAULT '0';
ALTER TABLE USERRIGHTSMODULES ADD role_name VARCHAR;

CREATE TABLE IF NOT EXISTS `EMPLOYEE_TIMECARD`
(
`id` 				VARCHAR PRIMARY KEY,
`user_id`           VARCHAR,
`clock_in_time`     INT,
`clock_out_time`    INT DEFAULT '0',
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`    TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

CREATE TABLE IF NOT EXISTS `SHIFT_DETAILS`
(
`id` 					VARCHAR PRIMARY KEY,
`user_id`       		VARCHAR,
`shift_status`      	INT DEFAULT '0',
`shift_open_time`   	INT,
`shift_closed_time` 	INT,
`open_drawer_amount`	DECIMAL(13, 4),
`closed_drawer_amount`	DECIMAL(13, 4),
`company_id`        	INTEGER DEFAULT NULL,
`sync_id`           	VARCHAR,
`last_sync_time`    	TIMESTAMP DEFAULT(strftime('%s','now')),
`created_by`        	INT,
`created_date`      	TIMESTAMP DEFAULT(strftime('%s','now')),
`updated_by`        	INT,
`updated_date`      	TIMESTAMP DEFAULT(strftime('%s','now')),
`active_status`     	INT DEFAULT '1'
);

ALTER TABLE AREA ADD company_id INT DEFAULT NULL;
ALTER TABLE BUSINESS ADD company_id INT DEFAULT NULL;

CREATE TABLE tmp_SYNC_MASTER AS SELECT * FROM SYNC_MASTER;

DROP TABLE SYNC_MASTER;

CREATE TABLE IF NOT EXISTS `SYNC_MASTER`
( `master_id`       VARCHAR PRIMARY KEY,
`table_name`        VARCHAR,
`record_id`         VARCHAR,
`company_id`        INT DEFAULT NULL,
`last_sync_time`    TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

INSERT INTO SYNC_MASTER(master_id, table_name, record_id) SELECT master_id, table_name, record_id FROM tmp_SYNC_MASTER;

DROP TABLE tmp_SYNC_MASTER;

---

CREATE TABLE tmp_DB_VERSION AS SELECT * FROM DB_VERSION;

DROP TABLE DB_VERSION;

CREATE TABLE IF NOT EXISTS `DB_VERSION`
( `created_date`    TIMESTAMP DEFAULT 'strftime(''%s'',''now'')',
`last_version`      VARCHAR PRIMARY KEY,
`created_by`        INT,
`company_id`        INT DEFAULT NULL,
`last_sync_time`    TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1'
);

INSERT INTO DB_VERSION(created_date, last_version) SELECT created_date, last_version FROM tmp_DB_VERSION;

DROP TABLE tmp_DB_VERSION;

---

/* Decimal Number */

CREATE TABLE tmp_TRANSACTIONS AS SELECT * FROM TRANSACTIONS;

DROP TABLE TRANSACTIONS;

CREATE TABLE IF NOT EXISTS `TRANSACTIONS` (
`trx_id`                    VARCHAR,
`total`                     VARCHAR,
`checkin_id`                VARCHAR,
`member_id`                 VARCHAR,
`payment`                   VARCHAR,
`pay_type_name`             VARCHAR,
`close_time`                VARCHAR,
`customer_name`             VARCHAR,
`offer_sn`                  VARCHAR,
`subtotal`                  VARCHAR,
`customer_id`               VARCHAR,
`disc_amt`                  VARCHAR,
`currency`                  VARCHAR,
`tax_amt`                   VARCHAR,
`create_time`               VARCHAR,
`disc_percentage`           VARCHAR,
`tran_sync_id`              VARCHAR,
`user_id`                   VARCHAR,
`trx_order_id`              VARCHAR DEFAULT '(NULL)',
`table_no`                  VARCHAR,
`notes`                     VARCHAR,
`status`                    VARCHAR,
`payment_method_id`         VARCHAR,
`custom_discount_id`        VARCHAR,
`voucher_id`                VARCHAR,
`voucher_name`              VARCHAR,
`voucher_amount`            DECIMAL(13, 4),
`voucher_type_id`           VARCHAR,
`voucher_type_name`         VARCHAR,
`voucher_member_number`     VARCHAR,
`voucher_ref_number`        VARCHAR,
`counter_number`            INT,
`rounding_amount`           DECIMAL(13, 4),
`customer_gstid`            VARCHAR,
`customer_email`            VARCHAR,
`customer_phone`            VARCHAR,
`customer_pincode`          VARCHAR,
`customer_city`             VARCHAR,
`customer_address1`         VARCHAR,
`customer_address2`         VARCHAR,
`trx_gst_mode`              VARCHAR,
`total_gst_value`           DECIMAL(13, 4),
`trans_number`              INTEGER,
`voucher_bought_detail_id`  VARCHAR,
`custom_field_name`         VARCHAR,
`company_id`                INTEGER DEFAULT NULL,
`sync_error_message`        VARCHAR,
`sync_id`                   VARCHAR,
`last_sync_time`            TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`                INT,
`created_date`              TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`                INT,
`updated_date`              TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`             INT DEFAULT '1',
`sales_person_id`           VARCHAR,
`sales_person_name`         VARCHAR,
`no_of_pax`                 INT DEFAULT 0,
`customer_review`           INT DEFAULT 0,
PRIMARY KEY(trx_id)
);

INSERT INTO TRANSACTIONS SELECT * FROM tmp_TRANSACTIONS;

DROP TABLE tmp_TRANSACTIONS;

--

CREATE TABLE tmp_TRN_ITEMS AS SELECT * FROM TRN_ITEMS;

DROP TABLE TRN_ITEMS;

CREATE TABLE IF NOT EXISTS `TRN_ITEMS` (
`trx_item_id`           VARCHAR PRIMARY KEY,
`trx_id`                VARCHAR,
`item_id`               VARCHAR,
`item_name`             VARCHAR,
`item_qty`              VARCHAR,
`item_price`            VARCHAR,
`cost_price`            DECIMAL(13, 4),
`selling_price`         DECIMAL(13, 4),
`item_remarks`          VARCHAR,
`item_discount`         DECIMAL(13, 4),
`custom_discount_id`    VARCHAR,
`is_edited_item_price`  VARCHAR,
`type_of_price`         VARCHAR,
`type_of_unit`          VARCHAR,
`stock_monitoring`      INTEGER,
`gst_code`              VARCHAR,
`gst_rate`              INTEGER,
`gst_value`             DECIMAL(13, 4),
`uom_qty`               DECIMAL(13, 4),
`company_id`            INTEGER DEFAULT NULL,
`sync_id`               VARCHAR,
`last_sync_time`        TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`            INT,
`created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`            INT,
`updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`         INT DEFAULT '1'
);

INSERT INTO TRN_ITEMS SELECT * FROM tmp_TRN_ITEMS;

DROP TABLE tmp_TRN_ITEMS;

--

CREATE TABLE tmp_REFUND AS SELECT * FROM REFUND;

DROP TABLE REFUND;

CREATE TABLE IF NOT EXISTS `REFUND` (
`refund_id`             VARCHAR,
`refund_amount`         DECIMAL(13, 4),
`total_amount`          DECIMAL(13, 4),
`rounding_amount`       DECIMAL(13, 4),
`tax_amount`            DECIMAL(13, 4),
`gst_amount`            DECIMAL(13, 4),
`discount_amount`       DECIMAL(13, 4),
`custom_discount_id`    VARCHAR,
`voucher_amount`        DECIMAL(13, 4),
`voucher_id`            VARCHAR,
`trx_id`                VARCHAR,
`order_id`              VARCHAR,
`create_time`           VARCHAR,
`tran_sync_id`          VARCHAR,
`remarks`               VARCHAR,
`company_id`            INTEGER DEFAULT NULL,
`sync_id`               VARCHAR,
`last_sync_time`        TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`            VARCHAR,
`created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`            VARCHAR,
`updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`user_id`               VARCHAR,
`active_status`         INT DEFAULT '1',
`sales_person_id`       VARCHAR,
`sales_person_name`     VARCHAR,
`no_of_pax`             INT DEFAULT 0,
PRIMARY KEY(refund_id)
);

INSERT INTO REFUND SELECT * FROM tmp_REFUND;

DROP TABLE tmp_REFUND;

--

CREATE TABLE tmp_HOLD_ORDER_ITEMS AS SELECT * FROM HOLD_ORDER_ITEMS;

DROP TABLE HOLD_ORDER_ITEMS;

CREATE TABLE IF NOT EXISTS `HOLD_ORDER_ITEMS` (
`id`                        VARCHAR PRIMARY KEY,
`order_id`                  VARCHAR,
`item_name`                 VARCHAR,
`item_id`                   VARCHAR,
`item_qty`                  VARCHAR,
`split_item_qty`            VARCHAR,
`item_price`                DECIMAL(13, 4),
`cost_price`                DECIMAL(13, 4),
`selling_price`             DECIMAL(13, 4),
`item_remarks`              VARCHAR,
`item_totalPrice`           DECIMAL(13, 4),
`item_discount`             DECIMAL(13, 4),
`uom_qty`                   DECIMAL(13, 4),
`item_checked`              INTEGER DEFAULT '0',
`kitchen_printer_checked`   INTEGER DEFAULT '0',
`active_split_qty`          INTEGER DEFAULT '0',
`custom_discount_id`        VARCHAR,
`stock_monitoring`          INTEGER,
`item_category_id`          INTEGER DEFAULT 0,
`gst_code`                  VARCHAR,
`gst_rate`                  INTEGER,
`gst_value`                 DECIMAL(13, 4),
`is_edited_item_price`      VARCHAR,
`type_of_price`             VARCHAR,
`type_of_unit`              VARCHAR,
`company_id`                INTEGER DEFAULT NULL,
`sync_id`                   VARCHAR,
`last_sync_time`            TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`                INT,
`created_date`              TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`                INT,
`updated_date`              TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`             INT DEFAULT '1' ,
printed_qty                 INT DEFAULT 0
);

INSERT INTO HOLD_ORDER_ITEMS SELECT * FROM tmp_HOLD_ORDER_ITEMS;

DROP TABLE tmp_HOLD_ORDER_ITEMS;

--

CREATE TABLE tmp_CUSTOMERS AS SELECT * FROM CUSTOMERS;

DROP TABLE CUSTOMERS;

CREATE TABLE IF NOT EXISTS `MEMBERS` (
`user_id`           INTEGER,
`cust_id`           INTEGER,
`cust_firstName`    VARCHAR,
`cust_lastName`     VARCHAR,
`cust_image`        VARCHAR,
`cust_checkin_id`   VARCHAR,
`cust_member_id`    VARCHAR,
`cust_level`        VARCHAR,
`cust_last_visit`   VARCHAR,
`cust_manis_pts`    INTEGER,
`cust_fav_cat`      VARCHAR,
`cust_fav_item`     VARCHAR,
`cust_total_spent`  INTEGER,
`cust_email`        VARCHAR,
`cust_mobile`       VARCHAR,
`cust_address`      VARCHAR,
`cust_birthdate`    VARCHAR,
`cust_gender`       VARCHAR,
`cust_city`         VARCHAR,
`cust_postcode`     VARCHAR,
`company_id`        INTEGER DEFAULT NULL,
`sync_id`           VARCHAR,
`last_sync_time`    TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`        INT,
`created_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`        INT,
`updated_date`      TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`     INT DEFAULT '1',
PRIMARY KEY(user_id)
);

INSERT INTO MEMBERS SELECT * FROM tmp_CUSTOMERS;

DROP TABLE tmp_CUSTOMERS;

--

CREATE TABLE IF NOT EXISTS `CUSTOMERS` (
`cus_id`                INTEGER,
`cus_mem_id`            INTEGER,
`cus_com_id`            INTEGER,
`cus_datetime`          INTEGER,
`cus_group`             INTEGER,
`cus_spent_credit`      INTEGER,
`cus_earn_point`        INTEGER,
`cus_redeem_point`      INTEGER,
`cus_last_check_in`     INTEGER,
`cus_last_earn_point`   INTEGER,
`cus_last_redeem_point` INTEGER,
`cus_last_bought`       INTEGER,
`cus_datetime_confirm`  INTEGER,
`cus_status`            INTEGER DEFAULT NULL,
`cus_key`               VARCHAR DEFAULT NULL,
`cus_cuf_id`            INTEGER DEFAULT NULL,
`company_id`            INTEGER DEFAULT NULL,
`sync_id`               INTEGER DEFAULT NULL,
`last_sync_time`        TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by`            INTEGER DEFAULT NULL,
`created_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by`            INTEGER DEFAULT NULL,
`updated_date`          TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status`         INT DEFAULT '1',
`cus_prime_id`           VARCHAR,
PRIMARY KEY(cus_prime_id)
);
