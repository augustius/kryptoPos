-- BASE SCRIPT BASED ON 1.5
-- IF CHANGED, MAKE SURE TO DO THE SAME TO VERSION NUMBER.

CREATE TABLE `AREA`
(
`id` 									VARCHAR,
`area_name` 							VARCHAR(50),
`sync_id` 								VARCHAR,
`last_sync_time` 						DATETIME,
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
`company_id` 							INT DEFAULT NULL,
PRIMARY KEY(id)
);

CREATE TABLE `BUSINESS`
(
`business_id` 							INTEGER,
`business_name` 						VARCHAR,
`business_token` 						VARCHAR,
`business_email` 						VARCHAR,
`business_userid` 						VARCHAR,
`business_descrip` 						VARCHAR,
`business_username` 					VARCHAR,
`business_type_id` 						INTEGER,
`business_type` 						VARCHAR,
`business_banner` 						VARCHAR,
`business_category` 					VARCHAR,
`business_address` 						VARCHAR,
`business_photo` 						VARCHAR,
`business_pswd` 						VARCHAR,
`subs_expiring_date` 					VARCHAR,
`subs_valid_status` 					INT DEFAULT '1',
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
`company_id` 							INT DEFAULT NULL,
PRIMARY KEY(business_id)
);

CREATE TABLE `CANCELLED_ORDER`
(
`trx_id` 								VARCHAR,
`customer_id` 							INTEGER,
`member` 								VARCHAR,
`customer` 								VARCHAR,
`customer_name` 						VARCHAR,
`checkin_id` 							VARCHAR NOT NULL,
`create_time` 							VARCHAR NOT NULL,
`sub_total` 							DOUBLE NOT NULL,
`total` 								DOUBLE NOT NULL,
`currency` 								VARCHAR NOT NULL,
`discount_prcnt` 						INTEGER NOT NULL,
`discount_amt` 							DOUBLE NOT NULL,
`tax_amt` 								DOUBLE NOT NULL,
`customer_profile_pic` 					VARCHAR,
`user_id` 								VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`table_no` 								VARCHAR,
`order_type` 							INTEGER,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(trx_id)
);

CREATE TABLE `CANCELLED_ORDER_ITEMS`
(
`id` 									VARCHAR,
`order_id` 								VARCHAR,
`item_name` 							VARCHAR,
`item_id` 								VARCHAR,
`item_qty` 								INTEGER,
`item_price` 							DECIMAL(13, 4),
`item_remarks` 							VARCHAR,
`item_totalPrice` 						DECIMAL(13, 4),
`item_discount` 						DECIMAL(13, 4),
`item_checked` 							INTEGER DEFAULT '0',
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `CANCELLED_ORDER_MODIFIERS`
(
`id` 									INTEGER PRIMARY KEY AUTOINCREMENT,
`item_id` 								INTEGER,
`mod_id` 								VARCHAR,
`mod_name` 								VARCHAR,
`mod_price` 							DECIMAL(13, 4),
`mod_type` 								VARCHAR NOT NULL,
`mod_parent_name` 						VARCHAR,
`mod_selected` 							VARCHAR,
`mod_parent_id` 						VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `CASH_REGISTER`
(
`cash_reg_id` 							VARCHAR,
`cash_type` 							INTEGER,
`amount` 								DOUBLE,
`member_id` 							VARCHAR,
`create_time` 							VARCHAR,
`tran_sync_id` 							VARCHAR,
`user_id` 								VARCHAR,
`trx_id` 								VARCHAR DEFAULT NULL,
`notes` 								VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(cash_reg_id)
);

CREATE TABLE `CATEGORIES`
(
`category_id` 							INTEGER,
`category_name` 						VARCHAR,
`category_image` 						VARCHAR,
`datetime` 								VARCHAR,
`sort_index` 							INT DEFAULT 0,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1', PRIMARY KEY(category_id) );

CREATE TABLE `CUSTOMERS`
(
`cus_id` 								INTEGER,
`cus_mem_id` 							INTEGER,
`cus_com_id` 							INTEGER,
`cus_datetime` 							INTEGER,
`cus_group` 							INTEGER,
`cus_spent_credit` 						INTEGER,
`cus_earn_point` 						INTEGER,
`cus_redeem_point` 						INTEGER,
`cus_last_check_in` 					INTEGER,
`cus_last_earn_point` 					INTEGER,
`cus_last_redeem_point` 				INTEGER,
`cus_last_bought` 						INTEGER,
`cus_datetime_confirm` 					INTEGER,
`cus_status` 							INTEGER DEFAULT NULL,
`cus_key` 								VARCHAR DEFAULT NULL,
`cus_cuf_id` 							INTEGER DEFAULT NULL,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								INTEGER DEFAULT NULL,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INTEGER DEFAULT NULL,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INTEGER DEFAULT NULL,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
`cus_prime_id` 							VARCHAR,
PRIMARY KEY(cus_prime_id)
);

CREATE TABLE `CUSTOM_INPUT`
(
`custom_field_name` 					VARCHAR PRIMARY KEY,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `DB_VERSION`
(
`created_date` 							TIMESTAMP DEFAULT 'strftime(''%s'',''now'')',
`last_version` 							VARCHAR PRIMARY KEY,
`created_by` 							INT,
`company_id` 							INT DEFAULT NULL,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `DISCOUNT`
(
`id` 									VARCHAR,
`discount_desc` 						VARCHAR,
`amount` 								DOUBLE,
`percentage` 							DOUBLE,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(id)
);

CREATE TABLE `EMPLOYEE_TIMECARD`
(
`id` 									VARCHAR PRIMARY KEY,
`user_id` 								VARCHAR,
`clock_in_time` 						INT,
`clock_out_time` 						INT DEFAULT '0',
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `ERRORS`
(
`error_id` 								VARCHAR,
`error_desc` 							VARCHAR,
`error_module` 							VARCHAR,
`error_function_name` 					VARCHAR,
`notified` 								INT DEFAULT 0,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(error_id)
);

CREATE TABLE `HOLD_ORDER`
(
`trx_id` 								VARCHAR PRIMARY KEY,
`member` 								VARCHAR,
`customer` 								VARCHAR,
`customer_name` 						VARCHAR,
`checkin_id` 							VARCHAR NOT NULL,
`create_time` 							VARCHAR NOT NULL,
`sub_total` 							DOUBLE NOT NULL,
`total` 								DOUBLE NOT NULL,
`total_gst_value` 						DOUBLE,
`currency` 								VARCHAR NOT NULL,
`discount_prcnt` 						INTEGER NOT NULL,
`discount_amt` 							DOUBLE NOT NULL,
`tax_amt` 								DOUBLE NOT NULL,
`customer_profile_pic` 					VARCHAR,
`user_id` 								VARCHAR,
`table_no` 								VARCHAR,
`order_type` 							INTEGER,
`custom_discount_id` 					VARCHAR,
`counter_number` 						INT,
`voucher_id` 							VARCHAR,
`voucher_name` 							VARCHAR,
`voucher_amount` 						DOUBLE,
`voucher_type_id` 						VARCHAR,
`voucher_type_name` 					VARCHAR,
`voucher_member_number` 				VARCHAR,
`voucher_ref_number` 					VARCHAR,
`rounding_amount` 						DOUBLE DEFAULT 0,
`customer_gstid` 						VARCHAR,
`customer_email` 						VARCHAR,
`customer_phone` 						VARCHAR,
`customer_pincode` 						VARCHAR,
`customer_city` 						VARCHAR,
`customer_address1` 					VARCHAR,
`customer_address2` 					VARCHAR,
`voucher_bought_detail_id` 				VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1' ,
`sales_person_id` 						VARCHAR,
`sales_person_name` 					VARCHAR,
`no_of_pax` 							INT DEFAULT 0,
`is_order_merged` 						INT DEFAULT 0,
`tbl_primary_id` 						VARCHAR,
`is_locked` 							INTEGER DEFAULT 0
);

CREATE TABLE `HOLD_ORDER_ITEMS`
(
`id` 									VARCHAR PRIMARY KEY,
`order_id` 								VARCHAR,
`item_name` 							VARCHAR,
`item_id` 								VARCHAR,
`item_qty` 								VARCHAR,
`split_item_qty` 						VARCHAR,
`item_price` 							DECIMAL(13, 4),
`cost_price` 							DECIMAL(13, 4),
`selling_price` 						DECIMAL(13, 4),
`item_remarks` 							VARCHAR,
`item_totalPrice` 						DECIMAL(13, 4),
`item_discount` 						DECIMAL(13, 4),
`uom_qty` 								DECIMAL(13, 4),
`item_checked` 							INTEGER DEFAULT '0',
`kitchen_printer_checked` 				INTEGER DEFAULT '0',
`active_split_qty` 						INTEGER DEFAULT '0',
`custom_discount_id` 					VARCHAR,
`stock_monitoring` 						INTEGER,
`item_category_id` 						INTEGER DEFAULT 0,
`gst_code` 								VARCHAR,
`gst_rate` 								INTEGER,
`gst_value` 							DECIMAL(13, 4),
`is_edited_item_price` 					VARCHAR,
`type_of_price` 						VARCHAR,
`type_of_unit` 							VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
`printed_qty` 							INT DEFAULT 0
);

CREATE TABLE `HOLD_ORDER_MODIFIERS`
(
`id` 									VARCHAR,
`hold_order_item_id` 					INTEGER,
`mod_id` 								VARCHAR,
`mod_name` 								VARCHAR,
`mod_price` 							DECIMAL(13, 4),
`mod_type` 								VARCHAR NOT NULL,
`mod_parent_name` 						VARCHAR,
`mod_selected` 							VARCHAR,
`mod_parent_id` 						VARCHAR,
`mod_item_id` 							INTEGER,
`mod_qty` 								INTEGER,
`selected_index` 						INTEGER,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `HOLD_ORDER_NUMBER`
(
`hold_order_number` 					INTEGER DEFAULT '0',
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(hold_order_number)
);

CREATE TABLE `ITEMS`
(
`item_id` 								INTEGER,
`item_name` 							VARCHAR,
`description` 							VARCHAR,
`online_order` 							INTEGER,
`item_image` 							VARCHAR,
`image_last_updated` 					INTEGER,
`inclusive_price` 						DOUBLE,
`price` 								DOUBLE,
`cost_price` 							DOUBLE,
`selling_price` 						DOUBLE,
`gst_code` 								VARCHAR,
`gst_rate` 								INTEGER,
`stock_monitoring` 						INTEGER,
`stock_left` 							DOUBLE,
`stock_min` 							DOUBLE,
`stock_ideal` 							DOUBLE,
`timestamp` 							VARCHAR,
`sort_index` 							INT DEFAULT 0,
`customfield1` 							VARCHAR,
`customfield2` 							VARCHAR,
`sku` 									VARCHAR,
`barcode` 								VARCHAR,
`parent` 								INTEGER DEFAULT 0,
`source` 								VARCHAR,
`brand` 								VARCHAR,
`supplier_id` 							INTEGER,
`term` 									VARCHAR,
`margin` 								DOUBLE,
`item_code` 							VARCHAR,
`size` 									VARCHAR,
`color` 								VARCHAR,
`season` 								VARCHAR,
`remarks1` 								VARCHAR,
`remarks2` 								VARCHAR,
`currency_type` 						VARCHAR,
`category_tags` 						VARCHAR,
`type_of_price` 						VARCHAR,
`type_of_unit` 							VARCHAR,
`item_cat_id` 							INTEGER,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(item_id)
);

CREATE TABLE `MEMBERS`
(
`user_id` 								INTEGER,
`cust_id` 								INTEGER,
`cust_firstName` 						VARCHAR,
`cust_lastName` 						VARCHAR,
`cust_image` 							VARCHAR,
`cust_checkin_id` 						VARCHAR,
`cust_member_id` 						VARCHAR,
`cust_level` 							VARCHAR,
`cust_last_visit` 						VARCHAR,
`cust_manis_pts` 						INTEGER,
`cust_fav_cat` 							VARCHAR,
`cust_fav_item` 						VARCHAR,
`cust_total_spent` 						INTEGER,
`cust_email` 							VARCHAR,
`cust_mobile` 							VARCHAR,
`cust_address` 							VARCHAR,
`cust_birthdate` 						VARCHAR,
`cust_gender` 							VARCHAR,
`cust_city` 							VARCHAR,
`cust_postcode` 						VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(user_id)
);

CREATE TABLE `MODIFIERS`
(
`mod_id` 								INTEGER,
`mod_type` 								VARCHAR,
`mod_name` 								VARCHAR,
`mod_price` 							DOUBLE,
`mod_parent_id` 						INTEGER,
`mod_parent_name` 						VARCHAR,
`mod_item_id` 							INTEGER,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(mod_id)
);

CREATE TABLE `MODULES`
(
`module_id` 							INTEGER,
`module_name` 							VARCHAR,
`module_desc` 							VARCHAR,
`module_icon` 							VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(module_id)
);

CREATE TABLE `ORDER_COUNT`
(
`counter_number` 						INTEGER DEFAULT '(1)',
`company_id` 							INTEGER DEFAULT NULL,
`last_reset_date` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1', PRIMARY KEY(counter_number) );

CREATE TABLE `PAYMENT_METHOD`
(
`id` 									INTEGER,
`pay_name` 								VARCHAR,
`action` 								INTEGER,
`type_id` 								VARCHAR,
`type_name` 							VARCHAR,
`gateway_id` 							VARCHAR,
`gateway_name` 							VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(id) );

CREATE TABLE `PENDING_EMAILS`
(
`email_add` 							VARCHAR,
`trx_id` 								VARCHAR,
`last_try` 								VARCHAR DEFAULT NULL,
`remarks` 								VARCHAR,
`email_type` 							INT DEFAULT 1,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(trx_id)
);

CREATE TABLE `PRINTERS`
(
`id` 									VARCHAR,
`ip_address` 							VARCHAR,
`printer_name` 							VARCHAR,
`printer_type` 							VARCHAR,
`category_id` 							VARCHAR,
`default_printer` 						INT DEFAULT 0,
`print_copies` 							INT DEFAULT 1,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INTEGER,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INTEGER,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(id)
);

CREATE TABLE `PURCHASE_ORDER`
(
`po_id` 								VARCHAR,
`po_no` 								VARCHAR,
`supplier_code` 						VARCHAR,
`supplier_name` 						VARCHAR,
`outright_consignment` 					INT,
`status_id` 							INT DEFAULT 0,
`company_id`							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(po_id)
);

CREATE TABLE `PURCHASE_ORDER_ITEM`
(
`po_item_no` 							VARCHAR,
`po_id` 								VARCHAR,
`quantity` 								INTEGER,
`item_id` 								INTEGER,
`remarks` 								VARCHAR,
`is_temp` 								INT DEFAULT 0,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1', PRIMARY KEY(po_item_no) );

CREATE TABLE `REFUND`
(
`refund_id` 							VARCHAR,
`refund_amount` 						DECIMAL(13, 4),
`total_amount` 							DECIMAL(13, 4),
`rounding_amount` 						DECIMAL(13, 4),
`tax_amount` 							DECIMAL(13, 4),
`gst_amount` 							DECIMAL(13, 4),
`discount_amount` 						DECIMAL(13, 4),
`custom_discount_id` 					VARCHAR,
`voucher_amount` 						DECIMAL(13, 4),
`voucher_id` 							VARCHAR,
`trx_id` 								VARCHAR,
`order_id` 								VARCHAR,
`create_time` 							VARCHAR,
`tran_sync_id` 							VARCHAR,
`remarks` 								VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							VARCHAR,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							VARCHAR,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`user_id` 								VARCHAR,
`active_status` 						INT DEFAULT '1',
`sales_person_id` 						VARCHAR,
`sales_person_name` 					VARCHAR,
`no_of_pax` 							INT DEFAULT 0,
PRIMARY KEY(refund_id) );

CREATE TABLE `REFUND_TRN_ITEMS`
(
`id` 									VARCHAR,
`trx_id` 								VARCHAR,
`item_id` 								VARCHAR,
`trx_item_id` 							VARCHAR,
`item_name` 							VARCHAR,
`item_qty` 								VARCHAR,
`refund_item_qty` 						INTEGER,
`item_price` 							VARCHAR,
`cost_price` 							DECIMAL(13, 4),
`selling_price` 						DECIMAL(13, 4),
`refund_item_price` 					DECIMAL(13, 4),
`gst_value` 							DECIMAL(13, 4),
`refund_gst_value` 						DECIMAL(13, 4),
`gst_code` 								VARCHAR,
`gst_rate` 								INTEGER,
`item_remarks` 							VARCHAR,
`item_discount` 						DECIMAL(13, 4),
`custom_discount_id` 					VARCHAR,
`stock_monitoring` 						INTEGER,
`item_checked` 							INTEGER DEFAULT 0,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `REFUND_TRN_MODIFIERS`
(
`id` 									VARCHAR,
`trx_item_id` 							VARCHAR,
`mod_name` 								VARCHAR,
`mod_price` 							DECIMAL(13, 4),
`mod_type` 								VARCHAR,
`mod_id` 								VARCHAR,
`mod_parent_id` 						VARCHAR,
`mod_parent_name` 						VARCHAR,
`mod_amount` 							INTEGER,
`mod_qty` 								INTEGER,
`mod_item_id` 							INTEGER,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `SETTING`
(
`hardware_code` 						VARCHAR,
`enable_barcode` 						INT DEFAULT 0,
`enable_qr_validation` 					INT DEFAULT 0,
`enable_dine_type` 						INT DEFAULT '(1)',
`printer_mode` 							INT DEFAULT 0,
`image_OnOff_type` 						INT DEFAULT 0,
`custom_sales` 							INT DEFAULT 0,
`multi_sync` 							INT DEFAULT 0,
`enable_gst` 							INT DEFAULT 0,
`merchant_name` 						VARCHAR,
`merchant_address` 						VARCHAR,
`merchant_phone` 						VARCHAR,
`footer_message` 						VARCHAR,
`wifi_name` 							VARCHAR,
`wifi_password` 						VARCHAR,
`enable_merchant_name` 					INT DEFAULT 1,
`enable_merchant_address` 				INT DEFAULT 1,
`enable_merchant_phone` 				INT DEFAULT 1,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
`mqtt_model_name` 						VARCHAR,
`mqtt_server_ip` 						VARCHAR,
`is_multi_device` 						INT DEFAULT 0,
`is_mqtt_enabled` 						INT DEFAULT 0,
`currency` 								VARCHAR,
`currency_symbol` 						VARCHAR,
`gst_id` 								VARCHAR,
`service_charge` 						DECIMAL(13, 4),
`gst_percentage` 						DECIMAL(13, 4),
PRIMARY KEY(hardware_code)
);

CREATE TABLE `SHIFT_DETAILS`
(
`id` 									VARCHAR PRIMARY KEY,
`user_id` 								VARCHAR,
`shift_status` 							INT DEFAULT '0',
`shift_open_time` 						INT,
`shift_closed_time` 					INT,
`open_drawer_amount` 					DECIMAL(13, 4),
`closed_drawer_amount` 					DECIMAL(13, 4),
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT(strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT(strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT(strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `SYNC_MASTER`
(
`master_id` 							VARCHAR PRIMARY KEY,
`table_name` 							VARCHAR,
`record_id` 							VARCHAR,
`company_id` 							INT DEFAULT NULL,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `TABLES`
(
`id` 									VARCHAR PRIMARY KEY,
`table_no` 								VARCHAR(50),
`total_chair` 							INTEGER,
`table_area` 							VARCHAR(50),
`table_x` 								DOUBLE,
`table_y` 								DOUBLE,
`table_color` 							VARCHAR DEFAULT '0xFEFA29',
`table_shape` 							VARCHAR(50),
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
`order_id` 								VARCHAR,
`total_amount` 							VARCHAR ,
`area_id` 								VARCHAR,
`merged_into` 							VARCHAR
);

CREATE TABLE `TAXES`
(
`id` 									VARCHAR,
`tax_name` 								VARCHAR,
`tax_percentage` 						VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `TRANSACTIONS`
(
`trx_id` 								VARCHAR,
`total` 								VARCHAR,
`checkin_id` 							VARCHAR,
`member_id` 							VARCHAR,
`payment` 								VARCHAR,
`pay_type_name` 						VARCHAR,
`close_time` 							VARCHAR,
`customer_name` 						VARCHAR,
`offer_sn` 								VARCHAR,
`subtotal` 								VARCHAR,
`customer_id` 							VARCHAR,
`disc_amt` 								VARCHAR,
`currency` 								VARCHAR,
`tax_amt` 								VARCHAR,
`create_time` 							VARCHAR,
`disc_percentage` 						VARCHAR,
`tran_sync_id` 							VARCHAR,
`user_id` 								VARCHAR,
`trx_order_id` 							VARCHAR DEFAULT '(NULL)',
`table_no` 								VARCHAR,
`notes` 								VARCHAR,
`status` 								VARCHAR,
`payment_method_id` 					VARCHAR,
`custom_discount_id` 					VARCHAR,
`voucher_id` 							VARCHAR,
`voucher_name` 							VARCHAR,
`voucher_amount` 						DECIMAL(13, 4),
`voucher_type_id` 						VARCHAR,
`voucher_type_name` 					VARCHAR,
`voucher_member_number` 				VARCHAR,
`voucher_ref_number` 					VARCHAR,
`counter_number` 						INT,
`rounding_amount` 						DECIMAL(13, 4),
`customer_gstid` 						VARCHAR,
`customer_email` 						VARCHAR,
`customer_phone` 						VARCHAR,
`customer_pincode` 						VARCHAR,
`customer_city` 						VARCHAR,
`customer_address1` 					VARCHAR,
`customer_address2` 					VARCHAR,
`trx_gst_mode` 							VARCHAR,
`total_gst_value` 						DECIMAL(13, 4),
`trans_number` 							INTEGER,
`voucher_bought_detail_id` 				VARCHAR,
`custom_field_name` 					VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_error_message` 					VARCHAR,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
`sales_person_id` 						VARCHAR,
`sales_person_name` 					VARCHAR,
`no_of_pax` 							INT DEFAULT 0,
`customer_review` 						INT DEFAULT 0,
PRIMARY KEY(trx_id)
);

CREATE TABLE `TRANSACTIONS_NUMBER`
(
`trans_number` 							INTEGER,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(trans_number)
);

CREATE TABLE `TRANSACTION_MULTIPLE_PAYMENTS`
(
`id` 									VARCHAR PRIMARY KEY,
`trx_id` 								VARCHAR,
`current_paid_amount` 					DOUBLE,
`total_amount` 							DOUBLE,
`pay_name` 								VARCHAR,
`payment_type_id` 						VARCHAR,
`payment_type_name` 					VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `TRN_ITEMS`
(
`trx_item_id` 							VARCHAR PRIMARY KEY,
`trx_id` 								VARCHAR,
`item_id` 								VARCHAR,
`item_name` 							VARCHAR,
`item_qty` 								VARCHAR,
`item_price` 							VARCHAR,
`cost_price` 							DECIMAL(13, 4),
`selling_price` 						DECIMAL(13, 4),
`item_remarks` 							VARCHAR,
`item_discount` 						DECIMAL(13, 4),
`custom_discount_id` 					VARCHAR,
`is_edited_item_price` 					VARCHAR,
`type_of_price` 						VARCHAR,
`type_of_unit` 							VARCHAR,
`stock_monitoring` 						INTEGER,
`gst_code` 								VARCHAR,
`gst_rate` 								INTEGER,
`gst_value` 							DECIMAL(13, 4),
`uom_qty` 								DECIMAL(13, 4),
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `TRN_MODIFIERS`
(
`id` 									VARCHAR,
`trx_item_id` 							VARCHAR,
`mod_name` 								VARCHAR,
`mod_price` 							DECIMAL(13, 4),
`mod_type` 								VARCHAR,
`mod_id` 								VARCHAR,
`mod_parent_id` 						VARCHAR,
`mod_parent_name` 						VARCHAR,
`mod_amount` 							INTEGER,
`mod_qty` 								INTEGER,
`mod_item_id` 							INTEGER,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `USERRIGHTS`
(
`user_id` 								INTEGER,
`email` 								VARCHAR,
`password` 								VARCHAR,
`last_login` 							VARCHAR,
`name` 									VARCHAR,
`super_user` 							INT DEFAULT '0',
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1', is_sales_person INT,
PRIMARY KEY(user_id)
);

CREATE TABLE `USERRIGHTSMODULES`
(
`item_id` 								INTEGER PRIMARY KEY AUTOINCREMENT,
`user_id` 								INTEGER,
`module_id` 							INTEGER,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1' , role_id INT DEFAULT '0', role_name VARCHAR
);

CREATE TABLE `USER_LOGIN`
(
`username` 								VARCHAR,
`user_password` 						VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1',
PRIMARY KEY(username)
);

CREATE TABLE `VOUCHERS`
(
`voucher_id` 							VARCHAR PRIMARY KEY,
`voucher_name` 							VARCHAR,
`voucher_type_id` 						VARCHAR,
`voucher_amount` 						DOUBLE,
`voucher_qrcode` 						VARCHAR,
`description` 							VARCHAR,
`type` 									VARCHAR,
`picture` 								VARCHAR,
`amount` 								VARCHAR,
`valid_start` 							TIMESTAMP,
`valid_end` 							TIMESTAMP,
`stock_left` 							INTEGER,
`stock_minimum` 						INTEGER,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `VOUCHERS_BOUGHT`
(
`id` 									VARCHAR PRIMARY KEY,
`voucher_id` 							VARCHAR,
`qty` 									INTEGER,
`price` 								INTEGER,
`bought_time` 							TIMESTAMP,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `VOUCHERS_BOUGHT_DETAIL`
(
`id` 									VARCHAR PRIMARY KEY,
`voucher_name` 							VARCHAR,
`voucher_id` 							VARCHAR,
`voucher_bought_id` 					VARCHAR,
`sn` 									VARCHAR,
`code` 									VARCHAR,
`redeemed` 								INT DEFAULT '0',
`expired` 								TIMESTAMP,
`member_id` 							VARCHAR,
`is_manis` 								INT DEFAULT '0',
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);

CREATE TABLE `VOUCHER_TYPE`
(
`voucher_type_id` 						VARCHAR PRIMARY KEY,
`voucher_type_name` 					VARCHAR,
`company_id` 							INTEGER DEFAULT NULL,
`sync_id` 								VARCHAR,
`last_sync_time` 						TIMESTAMP DEFAULT (strftime('%s','now')),
`created_by` 							INT,
`created_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`updated_by` 							INT,
`updated_date` 							TIMESTAMP DEFAULT (strftime('%s','now')),
`active_status` 						INT DEFAULT '1'
);
