//
//  Manager-Bridging-Header.h
//  EbizuManagerPro
//
//  Created by Dev Sharma on 12/11/2015.
//  Copyright © 2015 Ebizu Sdn Bhd. All rights reserved.

//  Use this file to import your target's public headers that you would like to expose to Swift.

#import "Constants.h"
#import "AppDelegate.h"
#import "shareddb.h"
#import "SessionData.h"
#import "StringUtils.h"
#import "Macros.h"
#import "ModelController.h"
#import "google_analytics.h"
#import <Crashlytics/Crashlytics.h>

// 3rd Party
#import "GCDAsyncSocket.h"
#import "GCDAsyncUdpSocket.h"
#import "MSWeakTimer.h"

// Printers

#import "MWIFIManager.h"
#import "PosCommand.h"

//#import "ePOS-Print.h"
#import "ePOS2.h"

// StarMicronics
#ifndef StarIO_Extension_Bridging_Header_h
#define StarIO_Extension_Bridging_Header_h

#import <StarIO_Extension/StarIoExt.h>
#import <StarIO_Extension/StarIoExtManager.h>

#import <SMCloudServices/SMCloudServices.h>
#import <SMCloudServices/SMCSAllReceipts.h>

#endif /* StarIO_Extension_Bridging_Header_h */

#import "NSString+EBExtension.h"
#import "NSDictionary+EBExtension.h"
#import "NSDate+EBExtension.h"
#import "UIViewController+EBExtension.h"

// Shareddb extension
#import "shareddb+refund.h"
#import "shareddb+transaction.h"
#import "shareddb+transactionMultiplePayment.h"
#import "shareddb+Business.h"
#import "shareddb+setting.h"
#import "shareddb+transactionNumber.h"
#import "shareddb+paymentMethod.h"
#import "shareddb+cashRegister.h"
#import "shareddb+discount.h"
#import "shareddb+voucher.h"
#import "shareddb+printer.h"
#import "shareddb+category.h"
#import "shareddb+item.h"
#import "shareddb+modifier.h"
#import "shareddb+pendingEmail.h"
#import "shareddb+error.h"
#import "shareddb+shiftDetail.h"
#import "shareddb+employeeTimecard.h"
#import "shareddb+customInput.h"
#import "shareddb+userrights.h"
#import "shareddb+member.h"
#import "shareddb+area.h"
#import "shareddb+table.h"
#import "shareddb+holdOrder.h"

// RHB Related Libraries
#import "EADSessionController.h"
#import "StaticFunctions.h"

// Model
#import "Product.h"
#import "Categories.h"
#import "Voucher.h"
#import "Discount.h"
#import "Customer.h"

