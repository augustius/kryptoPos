//
//  Mpay.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 30/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

@objc class Mpay: NSObject,ExternalPaymentProtocol
{
    let MpayUrlSchemeRequest = "x-callback-url/payment/1.0?x-source=%@&api-key=&amount=%@&amountOther=0.0&transactionType=0200&subTranType=00&traceNo=&image=%@&description=%@&reference=&sendReceipt=0&sendNotification=1&testMode=1&sendSign=0&x-success=%@&x-failure=%@&x-cancel=%@"
    
    // MARK: Singleton Declaration
    // swiftSharedExPayment is not accessible from ObjC
    class var swiftSharedExPayment: Mpay
    {
        struct Singleton
        {
            static let SharedExPayment = Mpay()
        }
        return Singleton.SharedExPayment
    }
    
    // the sharedInstance class method can be reached from ObjC
    class func SharedExPayment() -> Mpay
    {
        return Mpay.swiftSharedExPayment
    }
    
    func sendPaymentAmount(amount: String?) -> Bool
    {
        //Image as NSData
        let imageData = NSData()
        let imageBase64 = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        let requestParam = String(format:MpayUrlSchemeRequest,
                                  "mpayconnect",                                                                // source
                                  amount ?? "0.00",                                                             // amount
                                  imageBase64,                                                                  // image
                                  UIUtils.add_encoded_value(StringUtils.get_business_detail("business_name"))!,  // description
                                  SUCCESSURL,                                                                   // success url
                                  FAILUREURL,                                                                   // fail url
                                  CANCELURL)                                                                    // cancel url
        
        // Set up the full URL used to call the MPAY Connect App
        let urlRequest = "mpayconnect://\(requestParam)"
        print("mpay request : \(urlRequest)")
        
        if let url = URL(string: urlRequest)
        {
            print("success mpay request : \(url)")
            return UIApplication.shared.openURL(url)
        }
        
        return false
    }
    
    func checkPaymentResponse(response: AnyObject) -> [String:String]
    {
        var mpayResponse = [String:String]()

        if let url = response as? NSURL
        {
            if let response = url.query?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            {
                print("mpay original response : \(response)")
                
                guard let dictResponse = response.getResponseDict() else
                {
                    print("ERROR : mpay reponse error")
                    return mpayResponse;
                }
                
                print("mpay original dict response : \(dictResponse)")
                
                mpayResponse[RESPONSECARDCODE] = dictResponse["responseCode"]
                mpayResponse[RESPONSECARDMSG]  = dictResponse["responseMessage"]
                mpayResponse[RESPONSECARDTYPE] = dictResponse["appLabel"]
                mpayResponse[RESPONSECARDNO]   = dictResponse["maskedCardNo"]
                mpayResponse[RESPONSECARDAPPROVALCODE] = dictResponse["mTrxId"]
                
                print("mpay stored response: \(mpayResponse)")
                
                // check did we have this status ?
                
                if let mpayCardCode = mpayResponse[RESPONSECARDCODE]
                {
                    guard let status = MpayStatus(rawValue: (mpayCardCode)) else
                    {
                        print ("ERROR : mpay unknown status response")
                        mpayResponse[RESPONSECARDSTAT] = UNKNOWNSCHEME
                        return mpayResponse;
                    }
                    
                    var responseStatus = "";
                    switch status
                    {
                    case .successStatus:
                        responseStatus = SUCCESSSCHEME
                    case .cancelStatus:
                        responseStatus = CANCELSCHEME
                    case .failStatus:
                        responseStatus = FAILURESCHEME
                    }
                    mpayResponse[RESPONSECARDSTAT] = responseStatus
                }
            }
        }
        return mpayResponse
    }
}
