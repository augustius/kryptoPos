//
//  3rdPaymentProtocol.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 30/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

@objc protocol ExternalPaymentProtocol
{
    func checkPaymentResponse(response: AnyObject) -> [String:String]
    
    func sendPaymentAmount(amount: String?) -> Bool
}

extension String
{
    func getResponseDict() -> [String : String]?
    {
        var dictResponse = [String : String]()
        
        let arrayResponse = self.components(separatedBy: "&")
        
        for stringResponse in arrayResponse
        {
            let seperatedArray = stringResponse.components(separatedBy: "=") as [String]
            let key     = seperatedArray.first ?? ""
            let value   = seperatedArray.last ?? ""
            
            dictResponse["\(key)"] = "\(value)"
        }
        
        return dictResponse
    }
}

enum MaybankMPOSStatus: String
{
    case successStatus  = "100"
    case cancelStatus   = ""
    case failStatus     = "291"
}

enum MpayStatus: String
{
    case successStatus  = "00"
    case cancelStatus   = "18"
    case failStatus     = "03"
}

enum RHBStatus: String
{
    case successStatus  = "APPROVAL"
    case cancelStatus   = "USER ABORT"
    case failStatus     = "TRANSACTION NOT SUCCESS"
}

