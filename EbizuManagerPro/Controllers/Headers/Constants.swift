//
//  Constants.swift
//  EbizuManagerPro
//
//  Created by Daliso Ngoma on 14/02/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

let userDefaults = UserDefaults.standard
let application = UIApplication.shared
let notification = NotificationCenter.default
let appDelegate: AppDelegate? = application.delegate as? AppDelegate

