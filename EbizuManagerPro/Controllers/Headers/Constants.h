//
//  Constants.h
//  
//
//  Copyright (c) 2012. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "data_sync.h"

#pragma mark DB Version Control

static int       const CURRENT_DB_VERSION   = 5;

#pragma mark Application Related Constants

static NSString *const APP_NAME = @"Ebizu Manager Pro";

    
//-------- For Single Terminal BASE_URL ---------------

//static NSString *const BASE_URL           = @"http://pos-api.ebizu.com/v1.5.4";   // Production
//static NSString *const BASE_URL           = @"http://new-apis.ebizu.com/api";     // Staging
//static NSString *const BASE_URL           = @"http://new-apiss.ebizu.com";        // Testing API Staging
//static NSString *const BASE_URL           = @"http://new-apisv17.ebizu.com";      // Testing 1.7 API Staging
//static NSString *const BASE_URL           = @"http://pos-apiv17.ebizu.com";       // New 1.7 API Production
static NSString *const BASE_URL           = @"http://api.kryptopos.com";       // KryptPOS Production

//static NSString *const BASE_URL           = @"http://pos-apiv16.ebizu.com";       // New 1.6 API Production
//static NSString *const BASE_URL           = @"http://new-apix.ebizu.com";         // Development
//static NSString *const BASE_URL           = @"http://192.168.1.109/manager-pro-backend-new/services_1.7/api/services.php"; // justin local server

//-------- For Multi Terminal BASE_URL ---------------

static NSString *const HUB_BASE_URL         = @"http://mnsolutions.ddns.net:8181/api/v1";     // Hub URL - Jamal

static NSString *const SERVICE_POINT        = @"/pointofsale/";

static NSString *const mqtt_server_ip       = @"192.168.0.200";

static NSString *const mqtt_topic           = @"MQTT/test";

//--------For Testing Customer Display---------------
static NSString *const mqtt_cd_topic        = @"MQTT/customer_display";

static NSString *const GST_PERCENTAGE       = @"GST_PERCENTAGE";
static NSString *const CURRENCY_CODE        = @"CURRENCY_CODE"; // TODO: This should be placed in constants.
//static NSString *const mqtt_server_ip    = @"ebizumanagerhub03.ddns.net";
//static NSString *const mqtt_topic        = @"MQTT/customers_display";
//----------------------------------------------------


static int       const REQUEST_TIMEOUT_SECONDS  = 250;             // it is needed to keep like/around this big

static NSString *const CURRENTLY_SYNCING    = @"CURRENTLY_SYNCING";
static NSString *const UDID                 = @"UDID";
static NSString *const APP_TOKEN            = @"APP_TOKEN";
static NSString *const COMPANY_ID           = @"COMPANY_ID";
static NSString *const CURRENT_LOGIN_DATE   = @"CURRENT_LOGIN_DATE";
static NSString *const CURRENT_USERID       = @"CURRENT_USERID";
static NSString *const TAXES                = @"TAXES";
static NSString *const CURRENCY             = @"CURRENCY";
static NSString *const GST_ID               = @"GST_ID"; //GST version
static NSString *const HARDWARE_CODE        = @"HARDWARE_CODE";
static NSString *const BUSINESS_DATA        = @"BUSINESS_DATA";
static NSString *const CURRENT_BALANCE      = @"CURRENT_BALANCE";
static NSString *const SHIFT_OPENED         = @"SHIFT_OPENED";
static NSString *const ACC_STATUS           = @"ACC_STATUS";
static NSString *const LAST_ACC_STATUS_CHECK = @"LAST_ACC_STATUS_CHECK";

static NSString *const CUSTOMER_DISPLAY      = @"CUSTOMER_DISPLAY";
static NSString *const CUSTOMER_DISPLAY_NAME = @"CUSTOMER_DISPLAY_NAME";

static NSString *const POSImages            = @"POSImages";
static NSString *const IMG_BUSINESS         = @"IMG_BUSINESS";
static NSString *const IMG_MODULES          = @"IMG_MODULES";
static NSString *const IMG_CUSTOMERS        = @"IMG_CUSTOMERS";
static NSString *const IMG_CATEGORIES       = @"IMG_CATEGORIES";
static NSString *const IMG_ITEMS            = @"IMG_ITEMS";
static NSString *const IMG_USERS            = @"IMG_USERS";

static NSString *const KeyIndex             = @"Key";
static NSString *const KeyBounds            = @"Bounds";
static NSString *const KeyImage             = @"Key_Image";
static NSString *const KeyHighlightImage    = @"key_HImage";

static NSString *const PlistName            = @"KeyboardFrame";
static NSString *const NumberKeyboardName   = @"NumberKeyBoard";
static NSString *const PinNumberKeyboard    = @"PinNumberKeyBoard";
static NSString *const KITCHEN              = @"KITCHEN";
static NSString *const RECEIPT              = @"RECEIPT";
static NSString *const CASHINOUT            = @"CASHINOUT";
//Constants related to AppVersion Helper

static NSString *const DATABASE_STATUS          = @"DATABASE_STATUS";
static NSString *const DATABASE_STATUS_SUCCESS  = @"DATABASE_STATUS_SUCCESS";
static NSString *const DATABASE_STATUS_FAIL     = @"DATABASE_STATUS_FAIL";

static NSString *const CHOICE               = @"CHOICE";
static NSString *const ADDITION             = @"ADDITION";

static NSString *const CASH                 = @"CASH";
static NSString *const CARD                 = @"CREDIT CARD";
static NSString *const MPAY                 = @"MPAY";
static NSString *const MAYBANK              = @"MPOS MAYBANK";
static NSString *const RHBCardBiz           = @"CARDBIZ RHB";
static NSString *const CHEQUE               = @"CHEQUE";
static NSString *const MAYBANKQR            = @"QRPAY";

// POS authorization level
static int const SUPERVISOR                 = 1; // - Can access everything
static int const CASHIER                    = 2; // - Can access everything with out report
static int const EMPLOYEE                   = 3; // - Can access only clock-in & clock-out

/// POSPay is defined in info.plist
static NSString *const SUCCESSURL           = @"POSPay://success";
static NSString *const FAILUREURL           = @"POSPay://failure";
static NSString *const CANCELURL            = @"POSPay://cancel";

static NSString *const SUCCESSSCHEME        = @"success";
static NSString *const FAILURESCHEME        = @"failure";
static NSString *const CANCELSCHEME         = @"cancel";
static NSString *const UNKNOWNSCHEME        = @"unknown";

// Dictionary key access for any 3rd party payment
static NSString *const RESPONSECARDCODE         = @"EbizuCardResponseCode";
static NSString *const RESPONSECARDMSG          = @"EbizuCardResponseMsg";
static NSString *const RESPONSECARDTYPE         = @"EbizuCardResponseCardType";
static NSString *const RESPONSECARDNO           = @"EbizuCardResponseCardNo";
static NSString *const RESPONSECARDSTAT         = @"EbizuCardResponseStatus";
static NSString *const RESPONSECARDAPPROVALCODE = @"EbizuCardResponseApprovalCode";

// Shift open and closed
static NSString *const OPENSHIFT            = @"open_shift";
static NSString *const CLOSESHIFT           = @"close_shift";
///////////////////////////////////////////////////////////////////////////////////

// TEMPORARY KEY TO STORE IMPORTANT DATA
static NSString *const ALL_PAYMENT_METHOD   = @"all_payment_method";
static NSString *const HOLD_ORDER_NO        = @"hold_order_no";
static NSString *const TRANSACTION_NO       = @"transaction_no";
static NSString *const ORDER_COUNT          = @"order_count";

static NSString *const HEADER_FOOTER_DATA   = @"header_footer_data";
static NSString *const WIFI_NAME            = @"wifi_name";
static NSString *const WIFI_PASS            = @"wifi_password";
static NSString *const MERCHANT_NAME        = @"merchant_name";
static NSString *const MERCHANT_ADDRESS     = @"merchant_address";
static NSString *const MERCHANT_PHONE       = @"merchant_phone";
static NSString *const FOOTER_MESSAGE       = @"footer_message";
static NSString *const PRINTING_OPTION      = @"printing_option";

static NSString *const APP_RUNNING_BACKGROUND   = @"Background";
static NSString *const APP_RUNNING_FOREGROUND   = @"Foreground";
static NSString *const APP_LAUNCH               = @"Launching";

static NSString *const RESPONCE_TYPE            = @"type";
static NSString *const RESPONCE_MESSAGE         = @"message";

static NSString *const APNS_DEVICE_TOKEN        = @"APNS_DEVICE_TOKEN";

static NSString *const CUSTOMER_DEFAULT_PIC     = @"default_people";

///////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////

#define SESSION_DATA            [SessionData sharedSessionData]
#define SharedModel             [ModelController sharedModelController]
#define Shareddb                [shareddb sharedShareddb]
#define SharedDataSync          [data_sync sharedDataSync]
#define Sharedservices          [SharedServices sharedSharedServices]
#define SharedHubServices       [HubServices sharedHubServices]

#define CURRENT_TIMESTAMP       [NSString stringWithFormat:@"%.0f",[[NSDate date] timeIntervalSince1970]]
#define CALCULATION_BEHAVIOR    [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundPlain scale:2 raiseOnExactness:NO raiseOnOverflow:NO raiseOnUnderflow:NO raiseOnDivideByZero:NO]

