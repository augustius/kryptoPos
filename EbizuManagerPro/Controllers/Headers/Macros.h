//
//  Macros.h
//
//  Copyright (c) 2012 All rights reserved.
//
//  Include this file in the .pch file of your project. That way these macros will be available
//  in all of your classes without importing this file again and again.
//

#import "AppDelegate.h"
#import "FileUtils.h"
#ifndef iOSLibrary_Macros_h
#define iOSLibrary_Macros_h

#pragma mark - Convenience Constants

/**
 Easily access the Projects AppDelegate object from anywhere
 */
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#pragma mark - Logging

/**
 If defined this constant enables logging output for//DebugLog Macro
 */
#define PRINT_LOGS

/**
 Used to print debugging output
 */
#ifdef PRINT_LOGS
#   define DebugLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DebugLog(...)
#endif

#endif //ifndef iOSLibrary_Macros_h



