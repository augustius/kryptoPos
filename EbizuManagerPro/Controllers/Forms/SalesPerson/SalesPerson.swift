//
//  SalesPerson.swift
//  Manager
//
//  Created by augustius cokroe on 08/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct SalesPersonData
{
    var id = ""
    var name = ""
    var image = UIImage()
}

class SalesPersonCell : UITableViewCell
{
    @IBOutlet private var salesPersonLogo: UIButton!
    @IBOutlet private var salesPersonName: UILabel!
    
    var salesPersonData = SalesPersonData() {
        didSet{
            DispatchQueue.main.async {
                self.salesPersonLogo.setBackgroundImage(self.salesPersonData.image, for: .normal)
                self.salesPersonName.text = self.salesPersonData.name
            }
        }
    }
}

class SalesPerson: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    var salesPersonArr = [[String:AnyObject]]()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        if let data = shareddb.shared().get_all_sp_data() as? [[String:AnyObject]]
        {
            salesPersonArr = data
        }
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction Method
    @IBAction func clearSalesPerson(_ sender: UIBarButtonItem)
    {
        ModelController.shared().currentCompleteOrder.sales_person_id = ""
        ModelController.shared().currentCompleteOrder.sales_person_name = ""
        performSegue(withIdentifier: "salesPersonSelected", sender: self)
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem)
    {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return salesPersonArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let data = salesPersonArr[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SalesPersonCell", for: indexPath)
        
        if let salesPersonCell = cell as? SalesPersonCell
        {
            guard
                let id = data["sales_person_id"] as? String,
                let name = data["sales_person_name"] as? String,
                let image = UIUtils.get_employee_image(data["sales_person_id"] as? String)
            else { print("error on \(#function), \(#line) "); return salesPersonCell}
            
            let currentSalesPersonId = (ModelController.shared().currentCompleteOrder.sales_person_id != nil) ? ModelController.shared().currentCompleteOrder.sales_person_id : ""
            let salesPersonData = SalesPersonData(id: id, name: name, image: image)
            salesPersonCell.salesPersonData = salesPersonData
            salesPersonCell.accessoryType = (id == currentSalesPersonId) ? .checkmark : .none

            return salesPersonCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let selectedCell = tableView.cellForRow(at: indexPath) as? SalesPersonCell
        {
            ModelController.shared().currentCompleteOrder.sales_person_id = selectedCell.salesPersonData.id
            ModelController.shared().currentCompleteOrder.sales_person_name = selectedCell.salesPersonData.name
            performSegue(withIdentifier: "salesPersonSelected", sender: self)
        }
    }
    
    
}
