//
//  AccountSuspended.swift
//  Manager
//
//  Created by augustius cokroe on 11/10/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class AccountSuspended : UIViewController
{
    @IBOutlet var messageTitle: UILabel!
    @IBOutlet var messageSubTitle: UILabel!
    @IBOutlet var messageDetail: UILabel!
    @IBOutlet var activeButton: UIButton!
    
    override func viewDidLoad()
    {
        notification.addObserver(self, selector: #selector(setupViewFromNotification), name: networkNotification.AccountStatusUpdated.notification, object: nil)
        Networking.shared().getData(forReading: .readingAccountSubscriptionStatus)
        setupView()
    }
    
    func setupView()
    {
        let accountSubsriptionStatus = userDefaults.string(forKey: ACC_STATUS) ?? ""
        if accountSubsriptionStatus == "1"
        {
            DispatchQueue.main.async {
                self.activeButton.isHidden = false
                self.messageTitle.text = "Welcome back to Ebizu POS!"
                self.messageSubTitle.isHidden = false
                self.messageSubTitle.text = "Just one more step to re-activate your account…"
                self.messageDetail.isHidden = false
                self.messageDetail.text = "Click the big button below to re-activate your Ebizu POS."
            }
        }
        else //2 or 3
        {
            let isTerminated = accountSubsriptionStatus == "3"
            
            DispatchQueue.main.async {
                self.activeButton.isHidden = true
                self.messageTitle.text = isTerminated ? "Your account has been terminated" : "Your account has been temporarily suspended"
                self.messageSubTitle.isHidden = isTerminated
                self.messageDetail.isHidden = isTerminated
            }
        }
    }
    
    @objc func setupViewFromNotification()
    {
        notification.removeObserver(self, name: networkNotification.AccountStatusUpdated.notification, object: nil)
        setupView()
    }
    
}
