//
//  OptionController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 31/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension Option
{
    func setupView()
    {
        view.addSubview(blurView)
        view.sendSubviewToBack(blurView)
        
        setupViewForShift()
    }
    
    func setupViewForShift()
    {
        guard
            let shiftData = shareddb.shared().get_last_shift_time_n_date() as? [String:AnyObject],
            let shiftTime = shiftData["last_shift"] as? String
            else { print("error on \(#function), \(#line) "); return}
        
        BigShiftStatus.text = userDefaults.bool(forKey: SHIFT_OPENED) ? "OPEN" : "CLOSED"
        shiftStatus.text = userDefaults.bool(forKey: SHIFT_OPENED) ? "OPENED AT" : "CLOSED AT"
        lastShiftTime.text = shiftTime
    }
    
    // MARK: - alert method
    func performShiftAlert()
    {
        present_sclalert("Info", message: "Please 'open shift' before performing action", alert_style: .info)
    }
    
    func performAccessAlert()
    {
        present_sclalert("Error", message: "Access Denied", alert_style: .error)
    }
    
    // MARK: - segue method
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool
    {
        switch identifier
        {
        case "openHoldOrder","openCashRegister","changeShift":
            
            if !userDefaults.bool(forKey: SHIFT_OPENED)
            {
                performShiftAlert()
                return false
            }
        case "openReport":
            
            if appDelegate?.user_role_id == CASHIER || appDelegate?.user_role_id == EMPLOYEE
            {
                performAccessAlert()
                return false
            }
        default: break
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "changeShift" || segue.identifier == "openCashRegister"
        {
            guard
                let dvc = segue.destination as? NewCashRegister
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.shiftAction = segue.identifier == "openCashRegister" ? "" : (userDefaults.bool(forKey: SHIFT_OPENED) ? CLOSESHIFT : OPENSHIFT)
        }
    }
}
