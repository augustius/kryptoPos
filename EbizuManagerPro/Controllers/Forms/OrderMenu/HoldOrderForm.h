//
//  HoldOrderForm.h
//  EbizuManagerPro
//
//  Created by Vinoth on 15/06/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "shareddb.h"
#import "HoldOrderCell.h"
@interface HoldOrderForm : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>
{
    IBOutlet UIView         *tutorial_view;
    IBOutlet UIButton       *btn_clear , *btn_select;
    IBOutlet UIImageView    *hand_animate_image;
    
    BOOL                is_edit_mode;
    NSIndexPath         *primary_indexpath;
    NSMutableDictionary *draging_tables_data;
    NSMutableArray      *selected_index_array;
    NSMutableArray      *orders_array;
    UIImageView         *movingCell;

    HoldOrderCell       *cell;
}

@property (strong, nonatomic) IBOutlet  UICollectionView    *collection_view;

@property (strong,nonatomic) NSString                       *selected_order_id;

@property BOOL from_split_bill;

- (IBAction)btn_select:(id)sender;

#pragma mark - Mqtt
- (void)receive_mqtt_message:(NSArray*)data;
@end
