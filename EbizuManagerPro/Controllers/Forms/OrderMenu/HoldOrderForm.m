//
//  HoldOrderForm.m
//  EbizuManagerPro
//
//  Created by Vinoth on 15/06/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "HoldOrderForm.h"
#import "HoldOrderCell.h"
#import "UIUtils.h"
#import "SplitBillVC.h"
#import "CompleteOrder.h"
#import "KryptoPOS-Swift.h"

static NSString *const TUTORIAL = @"hold_order_tutorial";

static NSString *const PRIMARY_ORDER_TABLE_ID   = @"primary_order_tbl_primary_id";
static NSString *const PRIMARY_ORDER_ID         = @"primary_order_id";
static NSString *const PRIMARY_ORDER_AMOUNT     = @"primary_order_total_amt";
static NSString *const PRIMARY_ORDER_TABLE      = @"primary_order_tbl_name";
static NSString *const PRIMARY_ORDER_DISC       = @"primary_order_discount";
static NSString *const PRIMARY_ORDER_VOUC       = @"primary_order_voucher";

static NSString *const SECOND_ORDER_TABLE_ID    = @"secondary_order_tbl_primary_id";
static NSString *const SECOND_ORDER_ID          = @"secondary_order_id";
static NSString *const SECOND_ORDER_AMOUNT      = @"secondary_order_total_amt";
static NSString *const SECOND_ORDER_TABLE       = @"secondary_order_tbl_name";
static NSString *const SECOND_ORDER_DISC        = @"secondary_order_discount";
static NSString *const SECOND_ORDER_VOUC        = @"secondary_order_voucher";

@implementation HoldOrderForm

#pragma mark - view cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setup_data];
    [self setup_view];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)setup_data
{    
    btn_select.hidden       = APP_DELEGATE.is_order_terminal;
    is_edit_mode            = NO;
    draging_tables_data     = [[NSMutableDictionary alloc]init];
    selected_index_array    = [[NSMutableArray alloc]init];
    orders_array            = [[NSMutableArray alloc]initWithArray:[Shareddb get_all_on_hold_orders]];
}

- (void)setup_view
{    
    if(orders_array.count > 1 && ![[NSUserDefaults standardUserDefaults] boolForKey:TUTORIAL])
    {
        [tutorial_view setHidden:NO];
        [self do_animate:CGRectMake(349, 213, hand_animate_image.frame.size.width, hand_animate_image.frame.size.height)];
    }
    else
    {
        [tutorial_view setHidden:YES];
    }
    
    self.collection_view.contentSize = CGSizeMake(_collection_view.frame.size.width, _collection_view.frame.size.height);
    UIEdgeInsets insets = UIEdgeInsetsMake(0, 0, 65, 0);
    self.collection_view.contentInset = insets;
    self.collection_view.scrollIndicatorInsets = insets;
}


- (IBAction)handle_pan:(UIPanGestureRecognizer *)pan_recognizer
{
    if(is_edit_mode == NO)
    {
        CGPoint location_point = [pan_recognizer locationInView:self.collection_view];
        NSIndexPath     *index_path_moving_cell   = [self.collection_view indexPathForItemAtPoint:location_point];
        HoldOrderCell   *cell_view                = (HoldOrderCell*) [self.collection_view cellForItemAtIndexPath:index_path_moving_cell];
        
        if (pan_recognizer.state == UIGestureRecognizerStateBegan)
        {
            primary_indexpath = index_path_moving_cell;
            self.collection_view.scrollEnabled = NO;
            
            draging_tables_data[SECOND_ORDER_TABLE_ID]  = cell_view.tbl_id;
            draging_tables_data[SECOND_ORDER_ID]        = cell_view.lbl_hold_id.text;
            draging_tables_data[SECOND_ORDER_AMOUNT]    = [cell_view.lbl_amount.text stringByReplacingOccurrencesOfString:@"RM " withString:@""];
            draging_tables_data[SECOND_ORDER_TABLE]     = cell_view.lbl_table_no.text;
            draging_tables_data[SECOND_ORDER_DISC]      = cell_view.lbl_discount.text;
            draging_tables_data[SECOND_ORDER_VOUC]      = cell_view.lbl_voucher.text;
            
            UIGraphicsBeginImageContext(cell_view.bounds.size);
            [cell_view.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *cellImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            movingCell = [[UIImageView alloc] initWithImage:cellImage];
            movingCell.center = location_point;
            movingCell.alpha = 0.75f;
            [self.collection_view addSubview:movingCell];
        }
        
        if (pan_recognizer.state == UIGestureRecognizerStateChanged)
        {
            movingCell.center = location_point;
        }
        
        if (pan_recognizer.state == UIGestureRecognizerStateEnded)
        {
            self.collection_view.scrollEnabled = YES;
            
            if(cell_view != nil)
            {
                draging_tables_data[PRIMARY_ORDER_TABLE_ID] = cell_view.tbl_id;
                draging_tables_data[PRIMARY_ORDER_ID]       = cell_view.lbl_hold_id.text;
                draging_tables_data[PRIMARY_ORDER_AMOUNT]   = [cell_view.lbl_amount.text stringByReplacingOccurrencesOfString:@"RM " withString:@""];
                draging_tables_data[PRIMARY_ORDER_TABLE]    = cell_view.lbl_table_no.text;
                draging_tables_data[PRIMARY_ORDER_DISC]     = cell_view.lbl_discount.text;
                draging_tables_data[PRIMARY_ORDER_VOUC]     = cell_view.lbl_voucher.text;
                
                if (![draging_tables_data[SECOND_ORDER_VOUC] isEqualToString:@""] || ![draging_tables_data[SECOND_ORDER_DISC] isEqualToString:@""] ||
                    ![draging_tables_data[PRIMARY_ORDER_VOUC] isEqualToString:@""] || ![draging_tables_data[PRIMARY_ORDER_DISC] isEqualToString:@""])
                {
                    [movingCell removeFromSuperview];
                    [self present_alert:@"Error" message:@"You can't merge into vouchers/discount applied orders"];
                    return;
                }
                
                if(![draging_tables_data[PRIMARY_ORDER_ID] isEqualToString:@""] && ![draging_tables_data[SECOND_ORDER_ID] isEqualToString:@""]
                   && ![draging_tables_data[SECOND_ORDER_ID] isEqualToString:draging_tables_data[PRIMARY_ORDER_ID]])
                {
                    [self present_alert:@"Merge Order"
                               message:[NSString stringWithFormat:@"You want to merge %@ into %@ order ?",
                                        draging_tables_data[SECOND_ORDER_ID],
                                        draging_tables_data[PRIMARY_ORDER_ID]]
               yesAlertActionCompletionHandler:^(UIAlertAction * _Nonnull action)
                     {
                         NSArray *second_table_data     = [self get_no_pax_and_table_details:draging_tables_data[SECOND_ORDER_TABLE]];
                         NSString *second_no_of_pax     = second_table_data.firstObject;
                         NSString *second_table_no      = second_table_data.lastObject;
                         NSString *second_table_id      = draging_tables_data[SECOND_ORDER_TABLE_ID];
                         
                         NSArray *primary_table_data    = [self get_no_pax_and_table_details:draging_tables_data[PRIMARY_ORDER_TABLE]];
                         NSString *primary_no_of_pax    = primary_table_data.firstObject;
                         NSString *primary_table_no     = primary_table_data.lastObject;
                         NSString *primary_table_id     = draging_tables_data[PRIMARY_ORDER_TABLE_ID];
                         
                         primary_no_of_pax = [NSString stringWithFormat:@"%d",primary_no_of_pax.intValue + second_no_of_pax.intValue];
                         draging_tables_data[PRIMARY_ORDER_AMOUNT] = [NSString stringWithFormat:@"%f",
                                                                      [draging_tables_data[PRIMARY_ORDER_AMOUNT] doubleValue] + [draging_tables_data[SECOND_ORDER_AMOUNT] doubleValue]];
                         
                         if (![second_table_no isEqualToString:@""] && ![primary_table_no isEqualToString:@""])
                         {
                             // if Two orders have a table
                             [Shareddb merge_into:primary_table_id from:second_table_id];
                         }
                         else if (![second_table_no isEqualToString:@""] && [primary_table_no isEqualToString:@""])
                         {
                             // if second order has a table
                             primary_table_no = second_table_no;
                             primary_table_id = draging_tables_data[SECOND_ORDER_TABLE_ID];
                         }
                         // else if Primary order has a table, then do nothing as primary_table_id already equals to primary_table_id itself

                         NSDictionary *hold_order_data =
                         @{
                           @"is_order_merged"    : @"1",
                           @"total_amount"       : draging_tables_data[PRIMARY_ORDER_AMOUNT],
                           @"no_of_pax"          : primary_no_of_pax,
                           @"table_no"           : primary_table_no,
                           @"table_primary_id"   : primary_table_id,
                           @"order_id"           : draging_tables_data[PRIMARY_ORDER_ID],
                           @"second_order_id"    : draging_tables_data[SECOND_ORDER_ID]
                           };
                         
                         NSMutableDictionary *primary_dict = [orders_array objectAtIndex:primary_indexpath.row];
                         primary_dict[@"trx_id"]           = hold_order_data[@"order_id"];
                         primary_dict[@"tbl_primary_id"]   = hold_order_data[@"table_primary_id"];
                         primary_dict[@"table_no"]         = hold_order_data[@"table_no"];
                         primary_dict[@"total"]            = hold_order_data[@"total_amount"];
                         primary_dict[@"no_of_pax"]        = hold_order_data[@"no_of_pax"];
                         primary_dict[@"is_order_merged"]  = hold_order_data[@"is_order_merged"];
                         
                         if(![primary_table_id isEqualToString:@""])
                             [Shareddb update_table_amount_orderid:hold_order_data];
                         
                         [Shareddb update_hold_order_merged_status:hold_order_data];
                         [orders_array removeObjectAtIndex:index_path_moving_cell.row];
                         [self.collection_view reloadData];
                     }
            noAlertActionCompletionHandler:^(UIAlertAction * _Nonnull action)
                     {
                         draging_tables_data = [[NSMutableDictionary alloc]init];
                     }
                     ];
                }
                else
                {
                    draging_tables_data = [[NSMutableDictionary alloc]init];
                }
            }
            [movingCell removeFromSuperview];
        }
    }
}

#pragma mark - Tutorial View methods

-(IBAction)btn_tutorial_closed:(id)sender
{
    [tutorial_view setHidden:YES];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:TUTORIAL];
}

-(void)do_animate:(CGRect)rect
{
    double delay_in_seconds = 0;
    dispatch_time_t pop_time = dispatch_time(DISPATCH_TIME_NOW, delay_in_seconds * NSEC_PER_SEC);
    dispatch_after(pop_time, dispatch_get_main_queue(), ^(void)
    {
        [UIView animateWithDuration:1.5
                              delay:0.0
                            options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat | UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             
                             hand_animate_image.frame = rect;
                         }
                         completion:^(BOOL finished) {
                             
                             NSLog(@"Animation completed ");
                         }];
    });
    
}

-(IBAction)btn_tutorial_info:(UIButton*)sender
{
    NSString *type_of_info;
    
    if(sender.tag == 13)
    {
        [tutorial_view setHidden:NO];
        [self do_animate:CGRectMake(349, 213, hand_animate_image.frame.size.width, hand_animate_image.frame.size.height)];
        return;
    }
    else if (sender.tag == 11)
    {
        type_of_info = @"Walk_in";
    }
    else
    {
        type_of_info = @"Dine_in";
    }
    
    UIView *btn                       = (UIView *)sender;
    UIStoryboard *mainStoryBoard      = [UIStoryboard storyboardWithName:@"CashRegister" bundle:nil];
    ShiftNameForm *sp_popup_vc        = [mainStoryBoard instantiateViewControllerWithIdentifier:@"ShiftNameForm"];
    sp_popup_vc.type_of_popup         = type_of_info;
    sp_popup_vc.preferredContentSize  = CGSizeMake(300, 130);
    sp_popup_vc.modalInPopover        = NO;
    UIPopoverController *pop          = [[UIPopoverController alloc] initWithContentViewController:sp_popup_vc];
    [pop presentPopoverFromRect:btn.bounds  inView:btn permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}

#pragma mark - UIButton action methods

-(IBAction)btn_close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)btn_reset:(id)sender
{
    if(is_edit_mode)
    {
        [self change_btn_appereance:NO];
        is_edit_mode = NO;
        [selected_index_array removeAllObjects];
        [btn_clear setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
        [btn_select setTitle:NSLocalizedString(@"Split bill", nil) forState:UIControlStateNormal];
        [self.collection_view reloadData];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(IBAction)btn_select:(id)sender
{
    if(is_edit_mode == NO)
    {
        is_edit_mode = YES;
        [btn_select setTitle:NSLocalizedString(@"Select items & split orders", nil) forState:UIControlStateNormal];
        [btn_clear setTitle :NSLocalizedString(@"Clear", nil) forState:UIControlStateNormal];
        [self.collection_view reloadData];
    }
    else
    {
        if(selected_index_array.count > 0)
        {
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(split_bill_done)
                                                         name:@"split_bill_done"
                                                       object:nil];
            
            UIStoryboard *mainStoryBoard      = [UIStoryboard storyboardWithName:@"HoldOrder" bundle:nil];
            SplitBillVC *split_bill           = [mainStoryBoard instantiateViewControllerWithIdentifier:@"SplitBillVC"];
            self.selected_order_id            = (orders_array)[[selected_index_array[0]integerValue]][@"trx_id"];
            split_bill.order_id               = self.selected_order_id;
            [self presentViewController:split_bill animated:YES completion:nil];
        }
        else
        {
            [self present_alert:@"Attention" message:@"Please select orders to split the bill"];
        }
    }
    
}


-(void)split_bill_done
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"split_bill_done" object:nil];
    
    [self performSelector:@selector(open_payment_selection) withObject:nil afterDelay:1.0];
    
}
-(void)open_payment_selection
{
    self.from_split_bill = YES;
//    [self performSegueWithIdentifier:@"hold_order_selected" sender:self];
    [self performSegueWithIdentifier:@"holdOrderSelected" sender:self];
}

#pragma mark - UICollectionView delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    NSInteger numOfSections = 0;
    
    if (collectionView == self.collection_view)
    {
        if(orders_array.count > 0)
        {
            numOfSections = 1;
            _collection_view.backgroundView = nil;
        }
        else
        {
            [UIUtils show_message_to:_collection_view tbl:nil message:@"Right now, No orders in hold"];
        }
    }
    
    return numOfSections;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (orders_array).count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Setup cell identifier
    static NSString *cellIdentifier = @"HoldOrderCell";
    
    cell = (HoldOrderCell *)[self.collection_view dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.layer.cornerRadius     =   10.0;
    cell.layer.masksToBounds    =   YES;
    
    if((orders_array).count > 0)
    {
        (cell.lbl_voucher).text = @"";
        (cell.lbl_discount).text = @"";
        
        if([(orders_array)[indexPath.row][@"is_locked"] isEqualToString:@"0"])
        {
            [cell.view_order_progress setHidden:YES];
        }
        else
        {
            [cell.view_order_progress setHidden:NO];
        }
        
        
        if([(orders_array)[indexPath.row][@"is_order_merged"] isEqualToString:@"0"])
        {
            [cell.merged_icon setHidden:YES];
            (cell.merged_icon).image = [UIImage imageNamed:@"icon_merged"];
        }
        else
        {
            [cell.merged_icon setHidden:NO];
        }
        
        if([(orders_array)[indexPath.row][@"voucher_amount"] floatValue] > 0.0 )
        {
            [cell.lbl_voucher setHidden:NO];
            (cell.lbl_voucher).text = [NSString stringWithFormat:@"voucher@rm%.2f",[(orders_array)[indexPath.row][@"voucher_amount"] doubleValue]];
        }
        
        if([(orders_array)[indexPath.row][@"discount_amt"] floatValue] > 0.0)
        {
            [cell.lbl_discount setHidden:NO];
            (cell.lbl_discount).text = [NSString stringWithFormat:@"discount@rm%.2f",[(orders_array)[indexPath.row][@"discount_amt"] doubleValue]];
        }
        
        if(is_edit_mode)
        {
            (cell.selected_icon).image = [UIImage imageNamed:@"select_hold_order"];
            [cell.view_selected_cell setHidden:NO];
        }
        else
        {
            [cell.view_selected_cell setHidden:YES];
        }
        
        if(is_edit_mode && [selected_index_array containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
        {
            (cell.selected_icon).image = [UIImage imageNamed:@"tick_hold_order"];
        }
        else
        {
            cell.layer.borderWidth      =   1.0;
            cell.layer.borderColor      =   [UIColor lightGrayColor].CGColor;
        }
        
        
        NSString *customer_name     = (orders_array)[indexPath.row][@"customer_name"];
        NSString *table_no          = (orders_array)[indexPath.row][@"table_no"];
        cell.lbl_amount.text        = [NSString stringWithFormat:@"RM %.2f",[(orders_array)[indexPath.row][@"total"] floatValue]];
        cell.lbl_hold_id.text       = (orders_array)[indexPath.row][@"trx_id"];
        cell.tbl_id                 = (orders_array)[indexPath.row][@"tbl_primary_id"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[(orders_array)[indexPath.row][@"created_date"] integerValue]];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"h:mm a";
        NSString *start_time_string = [formatter stringFromDate:date];
        
        NSDate *current_date = [NSDate date];
        NSDateFormatter *date_formatter = [[NSDateFormatter alloc] init];
        date_formatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";
        NSString *start_date = [date_formatter stringFromDate:date];
        NSString *current_date_str = [date_formatter stringFromDate:current_date];
        
        cell.lbl_waiting_time.text   = [UIUtils different_bw_two_dates_start:start_date end:current_date_str];
        
        if(![customer_name isEqualToString:@""] && ![customer_name isKindOfClass:[NSNull class]])
            cell.lbl_customer_name.text = [NSString stringWithFormat:@"%@ @ %@",customer_name,start_time_string];
        else
            cell.lbl_customer_name.text = [NSString stringWithFormat:@"Walk in @ %@",start_time_string];
        
        if(![table_no isEqualToString:@""] && ![table_no isKindOfClass:[NSNull class]])
        {
            cell.order_type_icon.image       = [UIImage imageNamed:@"tables_group"];
            cell.lbl_table_no.text           = [NSString stringWithFormat:@"%@ @ %@",(orders_array)[indexPath.row][@"no_of_pax"],table_no];
            cell.lbl_hold_id.backgroundColor = [UIColor colorWithRed:116.0/255.0 green:185.0/255.0 blue:129.0/255.0 alpha:1.0];
        }
        else
        {
			NSString *orderType = (orders_array)[indexPath.row][@"order_type"];
            cell.order_type_icon.image       = [UIImage imageNamed:@"walk_in"];
            cell.lbl_table_no.text           = ([orderType isEqualToString:@"0"]) ? @"Dine In" : @"Take Away";
            cell.lbl_hold_id.backgroundColor = [UIColor colorWithRed:99.0/255.0 green:149.0/255.0 blue:203.0/255.0 alpha:1.0];
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if(is_edit_mode == NO)
    {
        if([(orders_array)[indexPath.row][@"is_locked"] isEqualToString:@"0"])
        {
            self.selected_order_id = orders_array[indexPath.row][@"trx_id"];
            self.from_split_bill   = NO;
//            [self performSegueWithIdentifier:@"hold_order_selected" sender:self];
            [self performSegueWithIdentifier:@"holdOrderSelected" sender:self];
            
            if(APP_DELEGATE.is_multi_device)
                [self performSelectorInBackground:@selector(lock_selected_order:) withObject:self.selected_order_id];
        }
        else
        {
            [self present_alert:@"Attention" message:@"Order is progress in other terminal"];
        }
    }
    else
    {
        if([(orders_array)[indexPath.row][@"voucher_amount"] floatValue] > 0.0 || [(orders_array)[indexPath.row][@"discount_amt"] floatValue] > 0.0)
        {
            [self present_alert:@"Attention" message:@"Unable to split order with voucher & discount"];
        }
        else
        {
            selected_index_array = [[NSMutableArray alloc]initWithObjects:[NSString stringWithFormat:@"%ld",(long)indexPath.row], nil];
            
            if(selected_index_array.count > 0)
            {
                [btn_select setTitle:NSLocalizedString(@"Split now", nil) forState:UIControlStateNormal];
                [self change_btn_appereance:YES];
            }
            else
            {
                [self change_btn_appereance:NO];
                [btn_select setTitle:NSLocalizedString(@"Select items & split orders", nil) forState:UIControlStateNormal];
            }
            
            [self.collection_view reloadData];
            
        }
    }
}

-(void)change_btn_appereance:(BOOL)split_mode
{
    if(split_mode)
    {
        [btn_select setBackgroundImage:[UIImage imageNamed:@"green_outline-1"] forState:UIControlStateNormal];
        [btn_select setTitleColor:[UIColor colorWithRed:73.0/255.0 green:88.0/255.0 blue:23.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
    else
    {
        [btn_select setBackgroundImage:[UIImage imageNamed:@"orange_outline-1"] forState:UIControlStateNormal];
        [btn_select setTitleColor:[UIColor colorWithRed:219.0/255.0 green:117.0/255.0 blue:45.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    }
}

#pragma mark - Mqtt

-(void)lock_selected_order:(NSString*)trans_id
{
    [Shareddb set_hold_order_locked:1 trx_id:trans_id];
}

-(void)receive_mqtt_message:(NSArray*)data
{
    if([[data[0] valueForKey:@"sync_to"] isEqualToString:@"hold_order_locked"])
    {
        [self performSelectorOnMainThread:@selector(update_hold_order_list) withObject:nil waitUntilDone:YES];
    }
}

-(void)update_hold_order_list
{
    orders_array       =  [[NSMutableArray alloc]initWithArray:[Shareddb get_all_on_hold_orders]];
    [self.collection_view reloadData];
}


- (NSArray *)get_no_pax_and_table_details:(NSString *)tbl_no
{
    NSArray *dine_data =@[@"0",@""];
    
    if(![tbl_no isEqualToString:@"Take away"] && ![tbl_no isEqualToString:@""])
    {
        dine_data  = [tbl_no componentsSeparatedByString:@" @ "];
    }
    return dine_data;
}

@end
