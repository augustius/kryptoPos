//
//  HoldOrderCell.h
//  EbizuManagerPro
//
//  Created by Vinoth on 15/06/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HoldOrderCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel        *lbl_customer_name;
@property (weak, nonatomic) IBOutlet UILabel        *lbl_amount;
@property (weak, nonatomic) IBOutlet UILabel        *lbl_table_no;
@property (weak, nonatomic) IBOutlet UILabel        *lbl_waiting_time;
@property (weak, nonatomic) IBOutlet UILabel        *lbl_hold_id;
@property (weak, nonatomic) IBOutlet UILabel        *lbl_voucher;
@property (weak, nonatomic) IBOutlet UILabel        *lbl_discount;
@property (weak, nonatomic) IBOutlet UIImageView    *order_type_icon;
@property (weak, nonatomic) IBOutlet UIImageView    *merged_icon;
@property (weak, nonatomic) IBOutlet UIImageView    *selected_icon;

@property (weak, nonatomic) IBOutlet UIView         *view_new_order;
@property (weak, nonatomic) IBOutlet UIView         *view_hold_order;
@property (weak, nonatomic) IBOutlet UIView         *view_selected_cell;
@property (weak, nonatomic) IBOutlet UIView         *view_order_progress;
@property (weak, nonatomic) NSString                *tbl_id;

@end
