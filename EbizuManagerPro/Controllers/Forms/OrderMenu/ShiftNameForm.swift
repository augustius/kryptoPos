//
//  ShiftNameForm.swift
//  EbizuManagerPro
//
//  Created by Vinoth on 19/07/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

import UIKit

class ShiftNameForm: UIViewController, UITextFieldDelegate
{
    var shift_name: String? = ""
    
    var type_of_popup: String?

    @IBOutlet var txt_shift_name    : UITextField!
    
    //Created for translation purpose
    @IBOutlet var lbl_header_name   : UILabel!
    @IBOutlet var view_create_shift : UIView!
    @IBOutlet var view_tutorial     : UIView!
    @IBOutlet var cancel_btn        : UIButton!
    @IBOutlet var done_btn          : UIButton!
    
    @IBOutlet var lbl_tutorial_header   : UILabel!
    @IBOutlet var lbl_tutorial_content  : UILabel!


    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.set_translation()
        
        if(type_of_popup == "create_shift")
        {
            view_tutorial.isHidden        = true
            view_create_shift.isHidden    = false
        }
        else
        {
            lbl_tutorial_header.text = type_of_popup
            
            if(type_of_popup == "Walk_in")
            {
                lbl_tutorial_content.text = StringUtils.get_language_label("Take-away customers orders top color displayed as a blue one")
            }
            else
            {
                lbl_tutorial_content.text = StringUtils.get_language_label("Dine-in customers orders top color displayed as a green one")
            }
            
            view_tutorial.isHidden        = false
            view_create_shift.isHidden    = true
        }
        // Do any additional setup after loading the view.
    }
    
    func set_translation()
    {
        lbl_header_name.text = StringUtils.get_language_label("Shift Name")
        txt_shift_name.placeholder = StringUtils.get_language_label("Create Shift Name")
        cancel_btn.setTitle("Cancel", for: UIControlState())
        done_btn.setTitle("Done", for: UIControlState())
    }


    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cmd_cancel(_ sender: AnyObject)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func cmd_done(_ sender: AnyObject)
    {
        if(txt_shift_name.text == "")
        {
            let alert = UIAlertController(title: nil, message: "Please enter shift name", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            self.dismiss(animated: false, completion: nil)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "create_shift_name"), object: txt_shift_name.text)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        shift_name = textField.text!
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
