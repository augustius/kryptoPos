//
//  SplitBillVC.m
//  EbizuManagerPro
//
//  Created by Vinoth on 30/06/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "SplitBillVC.h"
#import "KryptoPOS-Swift.h"

@implementation SplitBillVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    is_item_selected       = NO;
    self.items_array       =  [[NSMutableArray alloc]initWithArray:[Shareddb get_orders_item_for_split_bill:self.order_id]];
    self.original_list_items = [[NSMutableArray alloc]initWithArray:self.items_array];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(IBAction)btn_split:(id)sender
{
    if(is_item_selected)
    {
        [Shareddb update_order_item:self.items_array ofOrderID:_order_id.intValue];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"split_bill_done"
                                                            object:_order_id
                                                          userInfo:nil];
    }
    else
    {
        UIAlertController *splitBillAlertController = [UIAlertController alertControllerWithTitle:@"No items selecdted" message:@"Please select the items you want to split." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAlertAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [splitBillAlertController addAction:okAlertAction];
        [self presentViewController:splitBillAlertController animated:YES completion:nil];
    }
    
    
}
-(IBAction)btn_back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark - UICollectionView delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (self.items_array).count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Setup cell identifier
    static NSString *cellIdentifier = @"SplitBillCollectionCell";
    
    SplitBillCollectionCell *cell = (SplitBillCollectionCell *)[self.collection_view dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.layer.cornerRadius     =   10.0;
    cell.layer.masksToBounds    =   YES;

    if((self.items_array).count > 0)
    {
        if ([(self.items_array[indexPath.row])[@"item_checked"] intValue] == 1)
        {
            is_item_selected = YES;
            [cell.view_selected_cell setHidden:NO];
            (cell.selected_icon).image = [UIImage imageNamed:@"tick_hold_order"];
        }
        else
        {
            [cell.view_selected_cell setHidden:YES];
            cell.layer.borderColor      =   [UIColor lightGrayColor].CGColor;
            cell.layer.borderWidth      =   1.0;
        }

        
        cell.lbl_quantity.text   = [NSString stringWithFormat:@"%d",[(self.items_array[indexPath.row])[@"item_qty"] intValue]];
        
        if([(self.items_array[indexPath.row])[@"type_of_price"] isEqualToString:@"Unit Price"])
        {
            cell.lbl_item_name.text     = [NSString stringWithFormat:@"%@ @ %.2f-%@",(self.items_array[indexPath.row])[@"item_name"] ,
                                           [(self.items_array[indexPath.row])[@"uom_qty"] floatValue],
                                           (self.items_array[indexPath.row])[@"type_of_unit"]];
        }
        else
            cell.lbl_item_name.text     = [self.items_array[indexPath.row] valueForKey:@"item_name"] ;
        
        cell.lbl_amount.text   = [NSString stringWithFormat:@"%@ %.2f",    [[NSUserDefaults standardUserDefaults] objectForKey:CURRENCY],[(self.items_array[indexPath.row])[@"item_totalPrice"] floatValue]];
        
        [cell.btn_add_qty addTarget        : self action:@selector(customcell_btn_add:) forControlEvents:UIControlEventTouchUpInside];
        (cell.btn_add_qty).tag = indexPath.row;
        
        [cell.btn_minus_qty addTarget      : self action:@selector(customcell_btn_minus:) forControlEvents:UIControlEventTouchUpInside];
        (cell.btn_minus_qty).tag = indexPath.row;
        
        
        NSString *image_name        = [Shareddb get_item_image_name:(self.items_array[indexPath.row])[@"item_id"]];
        NSString *item_name         = (self.items_array[indexPath.row])[@"item_name"];
        
        NSArray *array_value        = [item_name componentsSeparatedByString:@" "];
        
        if(array_value.count > 1)
        {
            NSString *spaceStr_1        = array_value[0];
            spaceStr_1                  = [spaceStr_1 substringToIndex:1];
            NSString *spaceStr_2        = array_value[1];
            /* Check the String contains Numeric value */
            BOOL valid;
            NSCharacterSet *alphaNums   = [NSCharacterSet decimalDigitCharacterSet];
            NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:spaceStr_2];
            valid = [alphaNums isSupersetOfSet:inStringSet];
            if (!valid)
                spaceStr_2 = [spaceStr_2 substringToIndex:1];
            
            item_name                   = [NSString stringWithFormat:@"%@%@",spaceStr_1,spaceStr_2];
        }
        else
        {
            item_name   = [item_name substringToIndex:2];
        }
        
        
        if (image_name && ![image_name isEqualToString:@""])
        {
            NSRange nameRange = [image_name rangeOfString:@"/" options:NSBackwardsSearch];
            
            if (nameRange.length > 0 || [image_name isEqualToString:@"default_product.png"])
            {
                [cell.btn_item_image setTitle:item_name forState:UIControlStateNormal];
                (cell.btn_item_image).backgroundColor = [UIColor colorWithRed:130.0/255.0 green:130.0/255.0 blue:130.0/255.0 alpha:1.0];
                [cell.btn_item_image setBackgroundImage:nil forState:UIControlStateNormal];
            }
            else
            {
                nameRange = [image_name rangeOfString:@"."];
                
                NSData *imgData = [FileUtils getDataFromDocDirWithPath:[NSString stringWithFormat:@"%@/%@",POSImages,IMG_ITEMS] fileName:[image_name substringToIndex:nameRange.location] andExtn:@"jpg"];
                
                if (imgData)
                {
                    [cell.btn_item_image setTitle:nil forState:UIControlStateNormal];
                    (cell.btn_item_image).backgroundColor = [UIColor clearColor];
                    [cell.btn_item_image setBackgroundImage:[UIImage imageWithData:imgData] forState:UIControlStateNormal];
                }
                else
                {
                    [cell.btn_item_image setTitle:item_name forState:UIControlStateNormal];
                    (cell.btn_item_image).backgroundColor = [UIColor colorWithRed:130.0/255.0 green:130.0/255.0 blue:130.0/255.0 alpha:1.0];
                    [cell.btn_item_image setBackgroundImage:nil forState:UIControlStateNormal];
                }
            }
        }

    }
        
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    [self collection_view_clicked:indexPath.row];
}

#pragma mark - add button on the grid
- (void)customcell_btn_add:(id)sender
{
    UIButton *senderButton  = (UIButton *)sender;
    [self recalculate_price:(int)senderButton.tag withPlusMinus:YES];
}

#pragma mark - minus button on the grid

- (void)customcell_btn_minus:(id)sender
{
    UIButton *senderButton  = (UIButton *)sender;
    [self recalculate_price:(int)senderButton.tag withPlusMinus:NO];
}

-(void)recalculate_price:(int)row withPlusMinus:(BOOL)is_plus
{
    NSArray *order_item         = (self.items_array)[row];
    NSMutableDictionary *item   = [[NSMutableDictionary alloc] init];
    
    item[@"id"] = [order_item valueForKey:@"id"];
    item[@"item_id"] = [order_item valueForKey:@"item_id"];
    item[@"item_name"] = [order_item valueForKey:@"item_name"];
    item[@"item_price"] = [order_item valueForKey:@"item_price"];
    item[@"item_checked"] = [order_item valueForKey:@"item_checked"];
    item[@"type_of_price"] = [order_item valueForKey:@"type_of_price"];
    item[@"type_of_unit"] = [order_item valueForKey:@"type_of_unit"];
    item[@"item_discount"] = [order_item valueForKey:@"item_discount"];
    item[@"uom_qty"] = [order_item valueForKey:@"uom_qty"];
    
    
    int org_qty             = [[order_item valueForKey:@"item_qty"]  intValue];
    CGFloat item_price      = [[order_item valueForKey:@"item_price"]floatValue];
    
    if(is_plus == YES)
    {
        if (org_qty < [[self.original_list_items valueForKey:@"item_qty"][row] intValue])
        {
            org_qty = org_qty + 1;
            item_price = item_price * org_qty;
            item[@"item_totalPrice"] = [NSString stringWithFormat:@"%f",item_price];
            item[@"item_qty"] = [NSString stringWithFormat:@"%d",org_qty];
        }
        else
        {
            item[@"item_totalPrice"] = [order_item valueForKey:@"item_totalPrice"];
            item[@"item_qty"] = [order_item valueForKey:@"item_qty"];
        }
    }
    else
    {
        if (org_qty > 1)
        {
            org_qty = org_qty - 1;
            item_price = item_price * org_qty;
            item[@"item_totalPrice"] = [NSString stringWithFormat:@"%f",item_price];
            item[@"item_qty"] = [NSString stringWithFormat:@"%d",org_qty];
        }
        else
        {
            item[@"item_totalPrice"] = [order_item valueForKey:@"item_totalPrice"];
            item[@"item_qty"] = [order_item valueForKey:@"item_qty"];
        }
    }
    (self.items_array)[row] = item;
    [self.collection_view reloadData];
}

-(void)collection_view_clicked:(NSInteger)index_path
{
    is_item_selected = NO;

    NSArray *order_item         = (self.items_array)[index_path];
    NSMutableDictionary *item   = [[NSMutableDictionary alloc] init];
    
    item[@"id"] = [order_item valueForKey:@"id"];
    item[@"item_id"] = [order_item valueForKey:@"item_id"];
    item[@"item_name"] = [order_item valueForKey:@"item_name"];
    item[@"item_price"] = [order_item valueForKey:@"item_price"];
    item[@"type_of_price"] = [order_item valueForKey:@"type_of_price"];
    item[@"type_of_unit"] = [order_item valueForKey:@"type_of_unit"];
    item[@"item_discount"] = [order_item valueForKey:@"item_discount"];
    item[@"uom_qty"] = [order_item valueForKey:@"uom_qty"];
    
    
    if([[order_item valueForKey:@"item_checked"] isEqualToString:@"0"])
    {
        item[@"item_checked"] = [NSString stringWithFormat:@"1"];
        item[@"item_totalPrice"] = [order_item valueForKey:@"item_totalPrice"];
        item[@"item_qty"] = [order_item valueForKey:@"item_qty"];
        (self.items_array)[index_path] = item;
    }
    else
    {
        item[@"item_checked"] = [NSString stringWithFormat:@"0"];
        item[@"item_totalPrice"] = [self.original_list_items valueForKey:@"item_totalPrice"][index_path];
        item[@"item_qty"] = [self.original_list_items valueForKey:@"item_qty"][index_path];
        (self.items_array)[index_path] = item;
    }
    [self.collection_view reloadData];
}




@end
