//
//  SplitBillVC.h
//  EbizuManagerPro
//
//  Created by Vinoth on 30/06/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "shareddb.h"
#import "SplitBillCollectionCell.h"
@interface SplitBillVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    BOOL                is_item_selected;
}
@property (strong, nonatomic) IBOutlet  UICollectionView    *collection_view;
@property (nonatomic,strong) NSMutableArray *original_list_items;

@property (strong,nonatomic) NSMutableArray      *items_array;
@property (strong,nonatomic) NSString            *order_id;

-(IBAction)btn_split:(id)sender;
-(IBAction)btn_back:(id)sender;

@end
