//
//  SplitBillCollectionCell.h
//  EbizuManagerPro
//
//  Created by Vinoth on 30/06/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplitBillCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel        *lbl_item_name;
@property (weak, nonatomic) IBOutlet UILabel        *lbl_quantity;
@property (weak, nonatomic) IBOutlet UILabel        *lbl_amount;

@property (weak, nonatomic) IBOutlet UIButton       *btn_item_image;
@property (weak, nonatomic) IBOutlet UIButton       *btn_add_qty;
@property (weak, nonatomic) IBOutlet UIButton       *btn_minus_qty;

@property (weak, nonatomic) IBOutlet UIView         *view_selected_cell;
@property (weak, nonatomic) IBOutlet UIView         *view_quantity;
@property (weak, nonatomic) IBOutlet UIImageView    *selected_icon;


@end
