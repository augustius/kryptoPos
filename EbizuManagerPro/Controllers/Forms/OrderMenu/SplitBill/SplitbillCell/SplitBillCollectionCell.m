//
//  SplitBillCollectionCell.m
//  EbizuManagerPro
//
//  Created by Vinoth on 30/06/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "SplitBillCollectionCell.h"

@implementation SplitBillCollectionCell

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    
    _btn_item_image.layer.cornerRadius =_btn_item_image.frame.size.width/2;
    _btn_item_image.layer.borderWidth=1.0;
    _btn_item_image.layer.borderColor = [UIColor clearColor].CGColor;
    _btn_item_image.layer.masksToBounds=YES;
    
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.btn_minus_qty.bounds byRoundingCorners:(UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.btn_minus_qty.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.btn_minus_qty.layer.mask = maskLayer;
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.btn_add_qty.bounds byRoundingCorners:(UIRectCornerTopLeft) cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.btn_add_qty.bounds;
    maskLayer1.path  = maskPath1.CGPath;
    self.btn_add_qty.layer.mask = maskLayer1;
    
    
}
@end
