//
//  NewCashRegisterController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 30/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension NewCashRegister
{
    func setupView()
    {
        switch shiftAction
        {
        case OPENSHIFT:
            cashOption.selectedSegmentIndex = 0
            cashOption.isUserInteractionEnabled = false
            break
        case CLOSESHIFT:
            cashOption.selectedSegmentIndex = 1
            cashOption.isUserInteractionEnabled = false
            break
        default:
            break
        }
    }
    
    func dataValid() -> Bool
    {
        if inputAmount.text == ""
        {
            present_sclalert("Fail", message: "Please provide an amount to cash in/out", alert_style: .error)
            return false
        }
        else if cashOption.selectedSegmentIndex == 1 //cash out
        {
            let amount = NSDecimalNumber.init(string:inputAmount.text)
            let balance = NSDecimalNumber.init(string:currentBalance.text)

            if amount != NSDecimalNumber.notANumber && balance != NSDecimalNumber.notANumber
            {
                if amount.doubleValue > balance.doubleValue
                {
                    present_sclalert("Fail", message: "The current balance is lesser than amount to be taken out, please try a smaller amount", alert_style: .error)
                    return false
                }
            }
            else
            {
                present_sclalert("Fail", message: "Provided amount is not a number", alert_style: .error)
                return false
            }
        }
        return true
    }
    
    func handleDone()
    {
        if dataValid()
        {
            processData()
        }
    }
    
    func processData()
    {
        cashRegisterData["cash_type"] = "\(cashOption.selectedSegmentIndex)"
        cashRegisterData["amount"] = inputAmount.text
        cashRegisterData["member_id"] = ""
        cashRegisterData["trx_id"] = ""
        cashRegisterData["notes"] = notes.text
        cashRegisterData["shift_action"] = shiftAction
        
        let arr = [cashRegisterData]
        
        shareddb.shared().add_cash_(in: arr)
        showLoadingScreen()
        notification.addObserver(self, selector: #selector(enableUICashRegister), name: NSNotification.Name(DATABASE_STATUS), object: nil)
    }

    func setCurrentBalance()
    {
        let amount = NSDecimalNumber.init(string:self.cashRegisterData["amount"] ?? "0.00")
        shareddb.shared().update_current_balance(amount.doubleValue, with_plus: self.cashRegisterData["cash_type"] == "0")
    }

    @objc func enableUICashRegister(notif:NSNotification)
    {
        notification.removeObserver(self, name: NSNotification.Name(DATABASE_STATUS), object: nil)
        
        guard
            let notifData = notif.object as? [AnyObject],
            let notifMessage = notifData.last as? String,
            let databaseStatus = notifData.first as? String
            else
        {
            hideLoadingScreen()
            present_sclalert("DATA CONVERSION ERROR", message: "Fail to convert to type safe", alert_style: .info)
            print("error on \(#function), \(#line) "); return
        }
        
        if databaseStatus == DATABASE_STATUS_SUCCESS
        {
            // handle Cash Register Data
            setCurrentBalance()
            Networking.shared().putData(forWriting: .writingCashRegister)
            
            if let defaultPrinter = shareddb.shared().get_all_default_printers_(from_bill_only: true) as? [[String:String]]
            {
                if defaultPrinter.count > 0
                {
                    let data = cashRegiterReceiptData(cashType: self.cashRegisterData["cash_type"] ?? "",
                                                      amount: NSDecimalNumber.init(string: self.cashRegisterData["amount"] ?? "0.00").doubleValue,
                                                      notes: self.cashRegisterData["notes"] ?? "")
                    EbizuPrinter.sharedPrinter.printCashRegister(data: data)
                }
            }
            
            // handle Shift Data
            if shiftAction != ""
            {
                Networking.shared().putData(forWriting: .writingShift)
                userDefaults.set(!userDefaults.bool(forKey: SHIFT_OPENED), forKey: SHIFT_OPENED)
                
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "openShiftReturn", sender: self)
                }
            }
            else
            {
							DispatchQueue.main.async { [weak self] in
								self?.cancelPressed()
							}
            }
        }
        else
        {
            present_sclalert("DATABASE ERROR", message: notifMessage, alert_style: .error)
        }

        hideLoadingScreen()
    }

    //MARK: - CustomKeyboardDelegate method
    func pinStatus(success: Bool, textField: UITextField) -> Bool
    {
        if success
        {
            handleDone()
        }
        else
        {
            present_sclalert("Error", message: "Invalid PIN Entered, please try again", alert_style: .error)
        }
        
        return true
    }

}
