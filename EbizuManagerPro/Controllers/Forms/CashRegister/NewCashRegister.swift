//
//  NewCashRegister.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 30/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation


class NewCashRegister: UITableViewController,UITextFieldDelegate,customKeyboardViewDelegate
{
    @IBOutlet var currentBalance: UILabel!
    @IBOutlet var cashOption: UISegmentedControl!
    @IBOutlet var inputAmount: UITextField!
    @IBOutlet var notes: UITextField!
    @IBOutlet var pinTextfield: UITextField!
    
    var cashRegisterData = [String:String]()
    
    var shiftAction = ""
    
    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        currentBalance.text = String(format: "%.2f", userDefaults.double(forKey: CURRENT_BALANCE))
        customKeyboard?.delegate = self
        sendFormToGoogleAnalytic()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setupView()
    }
    
    //MARK: - IBAction method
    @IBAction func donePressed()
    {
        if shiftAction == ""
        {
            handleDone()
        }
        else
        {
            pinTextfield.becomeFirstResponder()
        }
    }
    
    @IBAction func cancelPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextfield delegate method
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        switch textField
        {
        case inputAmount:
            customKeyboard?.textfield = textField
            customKeyboard?.customKeyboardType = .amount
            break
        case pinTextfield:
            switch shiftAction
            {
            case OPENSHIFT:
                handleDone()
                break
            case CLOSESHIFT:
                customKeyboard?.textfield = textField
                customKeyboard?.customKeyboardType = .pin
                break
            default:
                break
            }
            break
        default:
            break
        }
    }
}











