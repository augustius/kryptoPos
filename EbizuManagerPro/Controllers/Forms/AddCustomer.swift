//
//  AddCustomer.swift
//  Manager
//
//  Created by augustius cokroe on 15/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum addCustomerFormUsage {
    case toEdit, toAdd
}

class AddCustomer:UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, AddCustomerGenderDelegate
{
    @IBOutlet var profilePic: UIButton!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var mobile: UITextField!
    @IBOutlet var address: UITextField!
    @IBOutlet var city: UITextField!
    @IBOutlet var zipCode: UITextField!
    @IBOutlet var birthdate: UITextField!
    @IBOutlet var gender: UIButton!
    @IBOutlet var addOrSaveButton: UIButton!
    
    let imagePicker = NoAutorotateUIImagePickerController()
    var currentCustomer = Customer()
    var formUsage:addCustomerFormUsage = .toAdd
    let customCalendarKeyboard = Bundle.main.loadNibNamed("customCalendarKeyboard", owner: self, options: nil)?.first as? customCalendarKeyboardView
    var dateFormatter = DateFormatter()
    
    // MARK: - View cycle
    override func viewDidLoad()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = true

        setupView()
    }
    
    // MARK: - IBAction method
    @IBAction func profilePicTapped()
    {
        handleUserImage()
    }
    
    @IBAction func cancelPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    func setupView()
    {
        dateFormatter.dateFormat = "yyyy-MM-dd"

        switch formUsage
        {
        case .toAdd:
            break
        case .toEdit:
            DispatchQueue.main.async {
                self.profilePic.setBackgroundImage(self.currentCustomer.storedProfilePicUIImage, for: .normal)
                self.firstName.text = self.currentCustomer.first_name
                self.lastName.text = self.currentCustomer.last_name
                self.email.text = self.currentCustomer.email
                self.mobile.text = self.currentCustomer.mobile
                self.address.text = self.currentCustomer.address
                self.city.text = self.currentCustomer.city
                self.zipCode.text = self.currentCustomer.postCode
                self.birthdate.text = self.currentCustomer.birthDate
                self.gender.setTitleColor(UIColor.black, for: .normal)
                self.gender.setTitle(self.currentCustomer.gender, for: .normal)
                
                self.addOrSaveButton.setTitle("Save", for: .normal)
                
                if self.currentCustomer.user_id == "0" // for walk in
                {
                    self.profilePic.isUserInteractionEnabled = false
                    self.firstName.isUserInteractionEnabled = false
                    self.lastName.isUserInteractionEnabled = false
                    self.email.isUserInteractionEnabled = false
                    self.mobile.isUserInteractionEnabled = false
                    self.address.isUserInteractionEnabled = false
                    self.city.isUserInteractionEnabled = false
                    self.zipCode.isUserInteractionEnabled = false
                    self.birthdate.isUserInteractionEnabled = false
                    self.gender.isUserInteractionEnabled = false
                    self.addOrSaveButton.isUserInteractionEnabled = false
                    self.addOrSaveButton.backgroundColor = UIColor.gray
                }
            }
        }
    }
    
    func handleUserImage()
    {
        let defaultPic = UIImage.init(named: CUSTOMER_DEFAULT_PIC)
        let currentPic = profilePic.backgroundImage(for: .normal)
        
        if currentPic == defaultPic
        {
            present_alert("Change Profile Picture",
                          message: "Where would you like to get your profile picture from?",
                          yesTitle: "Gallery",
                          noTitle: "Camera",
                          yesAlertActionCompletionHandler:
                { (UIAlertAction) in
                    
                    self.imagePicker.sourceType = .savedPhotosAlbum
                    self.present(self.imagePicker, animated: true, completion: nil)
            },
                          noAlertActionCompletionHandler:
                { (UIAlertAction) in
                    
                    self.imagePicker.sourceType = .camera
                    self.present(self.imagePicker, animated: true, completion: nil)
            })
        }
        else
        {
            present_alert("Delete Profile Picture",
                          message: "Would you like to delete your current profile picture?",
                          yesAlertActionCompletionHandler:
                { (UIAlertAction) in
                    
                    self.storeNewCustomerImage(defaultPic)
            },
                          noAlertActionCompletionHandler:nil)
        }
    }
    
    func mandatoryFilled() -> Bool
    {
        if firstName.text == "" || mobile.text == ""
        {
            present_sclalert("Error", message: "Please fill in all the mandatory field", alert_style: .error)
            return false
        }
        else if email.text != ""
        {
            if !(email.text ?? "" ).isValidEmail()
            {
                email.text = ""
                present_sclalert("Error", message: "Invalid email format", alert_style: .error)
                return false
            }
        }
        
        return true
    }
    
    func storeNewCustomerImage(_ image:UIImage?)
    {
        if var chosenImage = image
        {
            if chosenImage != UIImage.init(named: CUSTOMER_DEFAULT_PIC)
            {
                chosenImage = chosenImage.ConvertToStandardSize(100)
            }
            profilePic.setBackgroundImage(chosenImage, for: .normal)
            currentCustomer.justAddedProfilePicUIImage = chosenImage
        }
    }
    
    //MARK: - UITextfield delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        switch textField
        {
        case mobile,zipCode:
            let charSet:CharacterSet  = CharacterSet.init(charactersIn:"0123456789+").inverted
            let filteredString:String   = string.components(separatedBy: charSet).joined(separator: "")
            return (string == filteredString)
        default:
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        switch textField
        {
        case birthdate:
            customCalendarKeyboard?.textfield = textField
            customCalendarKeyboard?.dateFormatter = dateFormatter
            customCalendarKeyboard?.datePicker.maximumDate = Date()
            customCalendarKeyboard?.datePicker.datePickerMode = .date
            
            if let safeDate = dateFormatter.date(from: textField.text ?? "")
            {
                customCalendarKeyboard?.datePicker.setDate(safeDate, animated: true)
            }
        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        
        switch textField
        {
        case firstName: lastName.becomeFirstResponder()
        case lastName: email.becomeFirstResponder()
        case email: mobile.becomeFirstResponder()
        case mobile: address.becomeFirstResponder()
        case address: city.becomeFirstResponder()
        case city: zipCode.becomeFirstResponder()
        case zipCode: birthdate.becomeFirstResponder()
        case birthdate: gender.becomeFirstResponder()
        default: break
        }
        
        return true
    }
    
    //MARK: - UIImagePickerController delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        storeNewCustomerImage(info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage)
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - AddCustomerGender delegate
    func predefinedGenderSelected(G: String)
    {
        gender.setTitleColor(UIColor.black, for: .normal)
        gender.setTitle(G, for: .normal)
    }
    
    //MARK: - segue method
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool
    {
        if identifier == "customerDetailDone"
        {
            return mandatoryFilled()
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "customerDetailDone"
        {
            currentCustomer.first_name = firstName.text
            currentCustomer.last_name = lastName.text
            currentCustomer.email = email.text
            currentCustomer.mobile = mobile.text
            currentCustomer.address = address.text
            currentCustomer.city = city.text
            currentCustomer.postCode = zipCode.text
            currentCustomer.birthDate = birthdate.text
            currentCustomer.gender = (gender.title(for: .normal) == "Male" || gender.title(for: .normal) == "Female") ? gender.title(for: .normal) : ""
        }
        else if segue.identifier == "showGender"
        {
            guard
                let button = sender as? UIButton,
                let dvc = segue.destination as? AddCustomerGenderTableview,
                let possiblePopover = dvc.popoverPresentationController
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = button.bounds
            possiblePopover.sourceView = button
            dvc.delegate = self
            dvc.selectedPredefinedGender = gender.title(for: .normal) ?? ""
        }
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
