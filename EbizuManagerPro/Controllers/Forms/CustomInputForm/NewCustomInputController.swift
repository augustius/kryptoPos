//
//  NewCustomInputController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 29/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension NewCustomInput
{
    func complietCustomInputData()
    {
        // Example JSON: {"title":"value_from_field","2ndtitle:"2ndvalue_from_field"}
        
        var serialized = ""
        
        serialized.append("{")
        
        for (row,str) in fieldList.enumerated()
        {
            let indexPath = IndexPath(row: row, section: 0)
            guard
                let cell = customFieldTable.cellForRow(at: indexPath) as? CustomInputCell,
                let value = cell.textfield.text
                else { print("error on \(#function), \(#line) "); return}
            
            let data = "\"\(str)\":\"\(value)\""
            serialized.append(data)
            serialized.append(",")
        }
        
        if serialized.last == ","
        {
            serialized.remove(at: serialized.endIndex)
        }
        
        serialized.append("}")
        
        ModelController.shared().currentCompleteOrder.custom_field_name = serialized
    }
}
