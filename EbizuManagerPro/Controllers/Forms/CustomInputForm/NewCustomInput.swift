//
//  NewCustomInput.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 29/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class NewCustomInput: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var customFieldTable: UITableView!
    
    var fieldList = [String]()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        if let safeFieldList = shareddb.shared().get_custom_field_name() as? [String]
        {
            fieldList = safeFieldList
        }
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func cancelPressed()
    {
        performSegue(withIdentifier: "customInputReturn", sender: self)
    }
    
    @IBAction func donePressed()
    {
        complietCustomInputData()
        
        cancelPressed()
    }
    
    //MARK: - UITableViewDelegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return fieldList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customInputCell", for: indexPath)
        
        if let customInputCell = cell as? CustomInputCell
        {
            customInputCell.fieldName.text = ""
            
            return customInputCell
        }
        
        return cell
    }
    
}
