//
//  CustomInputCell.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 29/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class CustomInputCell: UITableViewCell
{
    @IBOutlet var fieldName: UILabel!
    @IBOutlet var textfield: UITextField!
    
}
