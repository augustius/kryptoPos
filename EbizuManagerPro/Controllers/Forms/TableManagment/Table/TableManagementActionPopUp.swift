//
//  TableManagementActionPopUp.swift
//  Manager
//
//  Created by augustius cokroe on 30/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct TableManagementActionPopUpData {
    var id = ""
    var desc = ""
}

class TableManagementActionCell:UITableViewCell
{
    var data = TableManagementActionPopUpData() {
        didSet{
            self.textLabel?.font = UIFont(name: "Poppins-Light", size: 18)
            self.textLabel?.text = data.desc
        }
    }
}

protocol TableManagementActionPopUpDelegate: class {
    func dataSelected(data: TableManagementActionPopUpData)
}

class TableManagementActionPopUp:UITableViewController
{
    var arrData = [TableManagementActionPopUpData]()
    var selectedData = TableManagementActionPopUpData()
    weak var delegate: TableManagementActionPopUpDelegate?
    
    //MARK: - UITableView delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableManagementActionCell", for: indexPath)
        
        if let tableManagementActionCell = cell as? TableManagementActionCell
        {
            let data = arrData[indexPath.row]
            tableManagementActionCell.data = data
            tableManagementActionCell.accessoryType = data.id == selectedData.id ? .checkmark : .none
            
            return tableManagementActionCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrData.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let selectedCell = tableView.cellForRow(at: indexPath) as? TableManagementActionCell
        {
            delegate?.dataSelected(data: selectedCell.data)
            dismiss(animated: true, completion: nil)
        }
    }
    
}
