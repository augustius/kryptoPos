//
//  TableManagement.swift
//  Manager
//
//  Created by augustius cokroe on 27/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum tableManagementUsage {
    case toView, toSelect
}

struct TableData {
    var id = ""
    var name = ""
    var areaData = TableAreaData()
    var orderNo = ""
    var noOfPax = 0
    var total = 0.0
    var tableMerged = false
    var tableMergedInto = ""
    var voucherAmount = 0.0
    var discountAmount = 0.0
    var sequenceNo = 1
    
    func dictRepresentation() -> [String:String]
    { // follow excat same dict structure on shareddb.shared().getAllTablesWithAreaId
        return
            [
                "table_id" : id,
                "table_no" : name,
                "table_area_id" : areaData.areaId,
                "table_area" : areaData.areaName,
                "order_id" : orderNo,
                "no_of_pax" : noOfPax.description,
                "total_amount" : total.description,
                "merged_into" : tableMergedInto,
                "tbl_sequence_no" : sequenceNo.description,
            ]
    }
}

class TableManagementCell:UICollectionViewCell
{
    @IBOutlet private var back: UIButton!
    @IBOutlet private var tableName: UILabel!
    
    @IBOutlet private var orderNoView: UIView!
    @IBOutlet private var orderNo: UIButton!
    
    @IBOutlet private var noOfPaxView: UIView!
    @IBOutlet private var noOfPax: UILabel!
    
    @IBOutlet private var totalAmountView: UIView!
    @IBOutlet private var totalAmount: UIButton!
    
    @IBOutlet var voucher: UILabel!
    @IBOutlet var discount: UILabel!
    
    var data = TableData() {
        didSet{
            back.isHidden = !data.tableMerged
            tableName.text = data.name
            orderNoView.isHidden = data.orderNo == ""
            if orderNoView.isHidden
            {
                noOfPaxView.isHidden = true
                totalAmountView.isHidden = true
            }
            else
            {
                noOfPaxView.isHidden = false
                totalAmountView.isHidden = false
                
                orderNo.setTitle(data.orderNo, for: .normal)
                noOfPax.text = data.noOfPax.description + " Pax"
                totalAmount.setTitle(String(format:"%.2f",data.total), for: .normal)
                voucher.text = data.voucherAmount > 0 ? " V " : ""
                discount.text = data.discountAmount > 0 ? " D " : ""
            }
        }
    }
}

class TableManagement:UICollectionViewController,TableManagementActionPopUpDelegate
{
    @IBOutlet var longPressGestureRecognizer: UILongPressGestureRecognizer!
    private var arrShownTableData = [TableData]()
    private var arrTableData = [TableData]()
    var selectedOrderId = ""
    var selectedArea = TableAreaData()
    var formUsage:tableManagementUsage = .toView
    var cellMoved = false
    let arrAction = [TableManagementActionPopUpData.init(id: "1", desc: "Add Table"),
                     TableManagementActionPopUpData.init(id: "2", desc: "Merge Table"),
                     TableManagementActionPopUpData.init(id: "3", desc: "Unmerge Table")]
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        if let safeData = shareddb.shared().getAllTables(withAreaId: selectedArea.areaId) as? [[String:String]]
        {
            for table in safeData
            {
                guard
                    let tableId = table["table_id"],
                    let tableName = table["table_no"],
                    let orderId = table["order_id"],
                    let noOfPax = Int(table["no_of_pax"] ?? "0") ?? Int?(0),
                    let mergeInto = table["merged_into"],
                    let total = Double(table["total_amount"] ?? "0.00") ?? Double?(0.0),
                    let voucherAmt = Double(table["voucher_amount"] ?? "0.00") ?? Double?(0.0),
                    let discountAmt = Double(table["discount_amount"] ?? "0.00") ?? Double?(0.0),
                    let seqNo = Int(table["tbl_sequence_no"] ?? "0") ?? Int?(0)
                    else { print("error on \(#function), \(#line) "); return}
                
                let mergedTable = safeData.filter{($0["merged_into"] == tableId)}
                let data = TableData.init(id: tableId,
                                          name: tableName,
                                          areaData: selectedArea,
                                          orderNo: orderId,
                                          noOfPax: noOfPax,
                                          total: total,
                                          tableMerged: mergedTable.count > 0,
                                          tableMergedInto: mergeInto,
                                          voucherAmount: voucherAmt,
                                          discountAmount: discountAmt,
                                          sequenceNo: seqNo)
                arrTableData.append(data)
            }
            reloadView()
        }
        formUsage = ModelController.shared().currentCompleteOrder.hold_order_id == nil ? .toView : .toSelect
        collectionView?.addGestureRecognizer(longPressGestureRecognizer)
        
        title = selectedArea.areaName
        if formUsage == .toView
        {
            let actionButton:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(actionPressed(_:)))
            navigationItem.rightBarButtonItem = actionButton
        }
    }
    
    func reloadView()
    {
        arrShownTableData = arrTableData.filter({$0.tableMergedInto == ""})
        collectionView?.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        if cellMoved
        {
            var tempArr = [[String:String]]()
            for (index,table) in arrTableData.reversed().enumerated()
            {
                var temptable = table
                temptable.sequenceNo = (index+2)
                tempArr.append(temptable.dictRepresentation())
            }
            shareddb.shared().updateTableDetails(tempArr)
            Networking.shared().putData(forWriting: .writingTable)
        }
    }
    
    //MARK: - IBAction method
    @IBAction func handleLongPressed(_ sender: UILongPressGestureRecognizer)
    {
        switch sender.state
        {
        case .began:
            guard let selectedIndexPath = collectionView?.indexPathForItem(at: sender.location(in: collectionView)) else {
                break
            }
            collectionView?.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            self.collectionView?.updateInteractiveMovementTargetPosition(sender.location(in: sender.view!))
        case .ended:
            self.collectionView?.endInteractiveMovement()
        default:
            self.collectionView?.cancelInteractiveMovement()
        }
    }
    
    @objc func actionPressed(_ sender: UIBarButtonItem)
    {
        performSegue(withIdentifier: "showAction", sender: sender)
    }

    //MARK: - UICollectionViewController delegate
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrShownTableData.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TableManagementCell", for: indexPath)
        
        if let tableManagementCell = cell as? TableManagementCell
        {
            let data = arrShownTableData[indexPath.row]
            tableManagementCell.data = data
            
            return tableManagementCell
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    {
        let temp = arrTableData.remove(at: sourceIndexPath.item)
        arrTableData.insert(temp, at: destinationIndexPath.item)
        reloadView()

        if !cellMoved
        {
            cellMoved = (sourceIndexPath != destinationIndexPath)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool
    {
        return formUsage == .toView
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if let selectedCell = collectionView.cellForItem(at: indexPath) as? TableManagementCell
        {
            switch formUsage
            {
            case .toView:
                if selectedCell.data.orderNo == ""
                {
                    if !selectedCell.data.tableMerged
                    {
                        performSegue(withIdentifier: "editTable", sender: indexPath)
                    }
                }
                else
                {
                    selectedOrderId = selectedCell.data.orderNo
                    performSegue(withIdentifier: "tableOrderSelected", sender: self)
                }
            case .toSelect:
                if selectedCell.data.orderNo == ""
                {
                    performSegue(withIdentifier: "inputNoOfPeople", sender: selectedCell)
                }
            }
        }
    }

    //MARK: - TableManagementActionPopUp delegate method
    func dataSelected(data: TableManagementActionPopUpData)
    {
        switch data.id
        {
        case "1":
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "addTable", sender: self)
            }
        case "2":
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "mergeTable", sender: self)
            }
            break
        case "3":
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "unmergeTable", sender: self)
            }
            break
        default:
            break
        }
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showAction"
        {
            guard
                let dvc = segue.destination as? TableManagementActionPopUp,
                let possiblePopover = dvc.popoverPresentationController,
                let barItem = sender as? UIBarButtonItem
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.barButtonItem = barItem
            dvc.arrData = arrAction
            dvc.delegate = self
        }
        else if segue.identifier == "addTable"
        {
            guard
                let dvc = segue.destination as? AddTable
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.formUsage = .toAdd
            dvc.currentTable = TableData(id: "",
                                         name: "",
                                         areaData: selectedArea,
                                         orderNo: "",
                                         noOfPax: 0,
                                         total: 0.0,
                                         tableMerged: false,
                                         tableMergedInto: "",
                                         voucherAmount: 0.0,
                                         discountAmount: 0.0,
                                         sequenceNo: 1)
        }
        else if segue.identifier == "editTable"
        {
            guard
                let dvc = segue.destination as? AddTable,
                let indexPath = sender as? IndexPath,
                let selectedCell = collectionView?.cellForItem(at: indexPath) as? TableManagementCell
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.formUsage = .toEdit
            dvc.currentTable = selectedCell.data
        }
        else if segue.identifier == "mergeTable" || segue.identifier == "unmergeTable"
        {
            guard
                let dvc = segue.destination as? MergeOrUnMergeTable
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.formUsage = segue.identifier == "mergeTable" ? .toMerge : .toUnmerge
            dvc.currentAllTable = arrTableData
        }
        else if segue.identifier == "inputNoOfPeople"
        {
            guard
                let dvc = segue.destination as? NoOfPax,
                let possiblePopover = dvc.popoverPresentationController,
                let selectedCell = sender as? TableManagementCell
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = CGRect.init(x: selectedCell.frame.midX, y: selectedCell.frame.midY, width: 0, height: 0)
            dvc.selectedTable = selectedCell.data
        }
    }
    
    @IBAction func AddEditTableDone(segue: UIStoryboardSegue)
    {
        if let svc = segue.source as? AddTable
        {
            if segue.identifier == "addTableSegue"
            {
                guard
                    let addTableId = shareddb.shared().addNewTable(svc.currentTable.dictRepresentation())
                    else { return }
                
                if addTableId == ""
                {
                    present_sclalert("Error", message: "Table name already exist. \nPlease choose another name for the new table. \nThank You.", alert_style: .error)
                    return
                }
                else if svc.currentTable.areaData.areaId != selectedArea.areaId // not same area
                {
                    return
                }
                
                svc.currentTable.id = addTableId
                arrTableData.append(svc.currentTable)
            }
            else if segue.identifier == "editTableSegue"
            {
                if let index = arrTableData.index(where: {$0.id == svc.currentTable.id})
                {
                    if svc.currentTable.areaData.areaId != selectedArea.areaId // not same area
                    {
                        svc.currentTable.sequenceNo = 1
                        arrTableData.remove(at: index)
                    }
                    else
                    {
                        arrTableData[index] = svc.currentTable
                    }
                    shareddb.shared().updateTableDetails([svc.currentTable.dictRepresentation()])
                }
            }
            else if segue.identifier == "deleteTableSegue"
            {
                if let index = arrTableData.index(where: {$0.id == svc.currentTable.id})
                {
                    shareddb.shared().deleteTable(withId: svc.currentTable.id)
                    arrTableData.remove(at: index)
                }
            }
            reloadView()
        }
    }
    
    @IBAction func MergeUnmergeTableDone(segue: UIStoryboardSegue)
    {
        if let svc = segue.source as? MergeOrUnMergeTable
        {
            arrTableData = svc.currentAllTable
            reloadView()
        }
    }
    
    @IBAction func noOfPaxDone(segue: UIStoryboardSegue)
    {
        if let svc = segue.source as? NoOfPax
        {
            ModelController.shared().currentCompleteOrder.table_no = svc.selectedTable.name
            ModelController.shared().currentCompleteOrder.tbl_primary_id = svc.selectedTable.id
            ModelController.shared().currentCompleteOrder.no_of_pax = Int32(svc.noOfPeople)
            
            dismiss(animated: true, completion: nil)
        }
    }
}
