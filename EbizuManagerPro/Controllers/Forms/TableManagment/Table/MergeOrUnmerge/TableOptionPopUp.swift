//
//  TableOptionPopUp.swift
//  Manager
//
//  Created by augustius cokroe on 30/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum TableOptionPopUpFormUsage {
    case getOne, getMultiple
}

class TableOptionPopUpCell:UITableViewCell
{
    var data = TableData() {
        didSet{
            self.textLabel?.font = UIFont(name: "Poppins-Light", size: 18)
            self.textLabel?.text = data.name
        }
    }
}

protocol TableOptionPopUpDelegate: class {
    func tableSelected(view: TableOptionPopUp)
}

class TableOptionPopUp:UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var customToolBar: UIToolbar!
    @IBOutlet var customToolBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    weak var delegate: TableOptionPopUpDelegate?
    var formUsage:TableOptionPopUpFormUsage = .getOne
    var selectedTable = [TableData]()
    var arrTable = [TableData]()
    
    //MARK: - view cycle
    override func viewDidLoad()
    {
        // setup view
        switch formUsage
        {
        case .getOne:
            customToolBar.isHidden = true
            customToolBarHeightConstraint.constant = 0.0
        case .getMultiple:
            break
        }
    }
    
    //MARK: - IBAction method
    @IBAction func donePressed(_ sender: UIBarButtonItem)
    {
        handleDone()
    }
    
    func handleDone()
    {
        delegate?.tableSelected(view: self)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectAllPressed(_ sender: UIBarButtonItem)
    {
        selectedTable = arrTable
        tableView.reloadData()
    }
    
    @IBAction func clearPressed(_ sender: UIBarButtonItem)
    {
        selectedTable.removeAll()
        tableView.reloadData()
    }
    
    //MARK: - UITableviewDelegate method
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableOptionPopUpCell", for: indexPath)
        
        if let tableOptionPopUpCell = cell as? TableOptionPopUpCell
        {
            let data = arrTable[indexPath.row]
            tableOptionPopUpCell.data = data
            tableOptionPopUpCell.accessoryType = selectedTable.contains(where: { $0.id == data.id }) ? .checkmark : .none
            
            return tableOptionPopUpCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrTable.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let selectedCell = tableView.cellForRow(at: indexPath) as? TableOptionPopUpCell
        {
            switch formUsage
            {
            case .getMultiple:
                if selectedCell.accessoryType == .checkmark
                {
                    if let index = selectedTable.index(where: {$0.id == selectedCell.data.id})
                    {
                        selectedTable.remove(at: index)
                    }
                }
                else
                {
                    selectedTable.append(selectedCell.data)
                }
                tableView.reloadData()
            case .getOne:
                selectedTable = [selectedCell.data]
                handleDone()
            }
        }
    }
    
}
