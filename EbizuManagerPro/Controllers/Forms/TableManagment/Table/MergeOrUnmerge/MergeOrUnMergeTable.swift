//
//  MergeOrUnMergeTable.swift
//  Manager
//
//  Created by augustius cokroe on 30/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum MergeOrUnMergeTableUsage{
    case toMerge, toUnmerge
}

class MergeOrUnMergeTable:UITableViewController, TableOptionPopUpDelegate
{
    @IBOutlet private var headerTitle: UILabel!
    @IBOutlet private var multipleTableTitle: UIButton!
    @IBOutlet private var oneTableTitle: UIButton!
    
    var currentAllTable = [TableData]()
    var formUsage:MergeOrUnMergeTableUsage = .toMerge
    private var multipleTable = [TableData](){
        didSet{
            multipleTableTitle.setTitle(multipleTable.map({ $0.name }).joined(separator: ","), for: .normal)
        }
    }
    private var oneTable = TableData(){
        didSet{
            oneTableTitle.setTitle(oneTable.name, for: .normal)
        }
    }
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        switch formUsage
        {
        case .toUnmerge:
            headerTitle.text = "Unmerge Table"
        case .toMerge:
            break
        }
    }
    
    //MARK: - IBAction method
    @IBAction func cancelPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func donePressed()
    {
        if mandatoryFilled()
        {
            // update the multiple table
            switch formUsage
            {
            case .toUnmerge:
                handleUnmergeDone()
            case .toMerge:
                handleMergeDone()
            }
            
            // update the one table
            if let index = currentAllTable.index(where: {$0.id == oneTable.id})
            {
                currentAllTable[index] = oneTable
            }
            
            performSegue(withIdentifier: "mergeUnmergeDone", sender: self)
        }
    }
    
    private func mandatoryFilled() -> Bool
    {
        if oneTable.id == "" ||
            (multipleTable.first?.id ?? "") == ""
        {
            present_sclalert("Error", message: "Please select all the mandatory information to proceed.\nThank You.", alert_style: .error)
            return false
        }
        
        return true
    }
    
    private func handleMergeDone()
    {
        oneTable.tableMerged = true
        var tempTableArr = [[String:String]]() // for TABLES
        var tempHoldArr = [[String:String]]() // for HOLD_ORDER
        for table in multipleTable
        {
            var tempTable = table
            tempTable.tableMergedInto = oneTable.id
            oneTable.noOfPax += tempTable.noOfPax
            oneTable.total += tempTable.total
            oneTable.orderNo = oneTable.orderNo != "" ? oneTable.orderNo : tempTable.orderNo
            
            // pass into arr to be updated on shareddb if table has order no
            if tempTable.orderNo != ""
            {
                tempHoldArr.append(tempTable.dictRepresentation())
            }
            
            // reset joined table value
            tempTable.orderNo = ""
            tempTable.total = 0.0
            tempTable.noOfPax = 0
            tempTable.tableMerged = false
            
            if let index = currentAllTable.index(where: {$0.id == tempTable.id})
            {
                currentAllTable[index] = tempTable
                // pass into arr to be updated on shareddb for TABLE
                tempTableArr.append(tempTable.dictRepresentation())
            }
        }
        tempTableArr.append(oneTable.dictRepresentation())
        shareddb.shared().updateTableDetails(tempTableArr)
        
        if oneTable.orderNo != ""
        {
            shareddb.shared().mergeHoldOrder(from: tempHoldArr, into: oneTable.dictRepresentation())
        }
    }
    
    func handleUnmergeDone()
    {
        var tempTableArr = [[String:String]]() // for TABLES
        for table in multipleTable
        {
            var tempTable = table
            tempTable.tableMergedInto = ""
            
            if let index = currentAllTable.index(where: {$0.id == tempTable.id})
            {
                currentAllTable[index] = tempTable
                // pass into arr to be updated on shareddb for TABLE
                tempTableArr.append(tempTable.dictRepresentation())
            }
        }
        oneTable.tableMerged = currentAllTable.filter({$0.tableMergedInto == oneTable.id}).count > 0
        tempTableArr.append(oneTable.dictRepresentation())
        shareddb.shared().updateTableDetails(tempTableArr)
    }
    
    // MARK: - UITableView Delegate
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        switch formUsage
        {
        case .toMerge:
            switch section
            {
            case 1: return "MERGE INTO"
            case 2: return "SELECT TABLE(s) TO MERGE"
            default: return ""
            }
        case .toUnmerge:
            switch section
            {
            case 1: return "UNMERGE FROM"
            case 2: return "SELECT TABLE(s) TO UNMERGE"
            default: return ""
            }
        }
    }
    
    //MARK: - TableOptionPopUpDelegate method
    func tableSelected(view: TableOptionPopUp)
    {
        switch view.formUsage
        {
        case .getMultiple:
            switch formUsage
            {
            case .toMerge:
                // get all selected table id
                let allSelectedTableId = view.selectedTable.map({$0.id})
                // get all table that merged to the selected table
                let mergedTable = currentAllTable.filter({ allSelectedTableId.contains($0.tableMergedInto) })
                // joined selected table and all table that merged to the selected table, so that they can be merge to one table
                multipleTable = (view.selectedTable + mergedTable)
            case .toUnmerge:
                multipleTable = view.selectedTable
            }
        case .getOne:
            oneTable = view.selectedTable.first ?? TableData()
            multipleTable = [TableData]()
        }
    }
    
    //MARK: - segue method
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool
    {
        if identifier == "getMultiple"
        {
            if oneTable.id == ""
            {
                switch formUsage
                {
                case .toMerge:
                    present_sclalert("Info", message: "Please select 'merge into' before select table to merge", alert_style: .info)
                case .toUnmerge:
                    present_sclalert("Info", message: "Please select 'from' before select table to unmerge", alert_style: .info)
                }
                return false
            }
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let dvc = segue.destination as? TableOptionPopUp
        {
            guard
                let possiblePopover = dvc.popoverPresentationController,
                let button = sender as? UIButton
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = CGRect.init(x: button.frame.midX, y: button.frame.midY, width: 0, height: 0)
            dvc.delegate = self
            
            let allowedTable = currentAllTable.filter({$0.voucherAmount == 0.0 && $0.discountAmount == 0.0})
            
            if segue.identifier == "getOne"
            {
                dvc.selectedTable = [oneTable]
                dvc.formUsage = .getOne
                switch formUsage
                {
                case .toMerge:
                    dvc.arrTable = allowedTable.filter({$0.tableMergedInto == ""})
                case .toUnmerge:
                    dvc.arrTable = allowedTable.filter({$0.tableMerged})
                }
            }
            else if segue.identifier == "getMultiple"
            {
                dvc.formUsage = .getMultiple
                switch formUsage
                {
                case .toMerge:
                    let arrTable = allowedTable.filter({$0.id != oneTable.id && $0.tableMergedInto == ""})
                    dvc.arrTable = arrTable
                    dvc.selectedTable = multipleTable.filter({ arrTable.map({$0.id}).contains($0.id) })
                case .toUnmerge:
                    dvc.arrTable = allowedTable.filter({$0.tableMergedInto == oneTable.id})
                    dvc.selectedTable = multipleTable
                }
            }
        }
        

    }
    
}
