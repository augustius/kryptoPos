//
//  NoOfPax.swift
//  Manager
//
//  Created by augustius cokroe on 05/12/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class NoOfPax:UIViewController, UITextFieldDelegate
{
    @IBOutlet private var noOfPeopleTitle: UITextField!
    
    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    
    var selectedTable = TableData()
    var noOfPeople = 1 {
        didSet{
            noOfPeopleTitle.text = noOfPeople.description
        }
    }
    
    //MARK: - IBAction method
    @IBAction private func plusPressed()
    {
        noOfPeople += 1
    }
    
    @IBAction private func minusPressed()
    {
        if noOfPeople > 1
        {
            noOfPeople -= 1
        }
    }
    
    @IBAction private func cancelPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func donePressed()
    {
        performSegue(withIdentifier: "noOfPaxDone", sender: self)
    }
    
    //MARK: - UITextfield delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        customKeyboard?.textfield = textField
        customKeyboard?.customKeyboardType = .intNumber
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        noOfPeople = Int(textField.text ?? "0") ?? 1
        if noOfPeople < 1
        {
            noOfPeople = 1
        }
    }
    
}
