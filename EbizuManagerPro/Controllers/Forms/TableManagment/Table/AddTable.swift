//
//  AddTable.swift
//  Manager
//
//  Created by augustius cokroe on 30/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum AddTableFormUsage {
    case toAdd, toEdit
}

class AddTable:UITableViewController,TableManagementActionPopUpDelegate
{
    @IBOutlet private var headerTitle: UILabel!
    @IBOutlet private var addEditButton: UIButton!
    @IBOutlet private var deleteButton: UIButton!
    @IBOutlet private var tableName: UITextField!
    @IBOutlet private var areaName: UIButton!
    var currentTable = TableData()
    var formUsage:AddTableFormUsage = .toEdit
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        switch formUsage
        {
        case .toAdd:
            headerTitle.text = "Add Table"
            deleteButton.isUserInteractionEnabled = false
            deleteButton.backgroundColor = UIColor.lightGray
        case .toEdit:
            break
        }
        setupView()
    }
    
    func setupView()
    {
        tableName.text = self.currentTable.name
        areaName.setTitle(self.currentTable.areaData.areaName, for: .normal)
    }
    
    //MARK: - IBAction method
    @IBAction private func cancelPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func addEditPressed()
    {
        if mandatoryFilled()
        {
            currentTable.name = tableName.text ?? ""

            switch formUsage
            {
            case .toAdd:
                performSegue(withIdentifier: "addTableSegue", sender: self)
            case .toEdit:
                performSegue(withIdentifier: "editTableSegue", sender: self)
            }
        }
    }
    
    private func mandatoryFilled() -> Bool
    {
        if tableName.text == "" || areaName.title(for: .normal) == ""
        {
            present_sclalert("Error", message: "Please fill in all the mandatory field", alert_style: .error)
            return false
        }
        return true
    }
    
    @IBAction private func deletePressed()
    {
        let alertConroller = UIAlertController(title: "Confirmation", message: "Are you sure you want to delete table \(currentTable.name)", preferredStyle: .alert)

        let okAction = UIAlertAction(title: "Yes", style: .default) { [weak self] (action) in
            self?.performSegue(withIdentifier: "deleteTableSegue", sender: self)
        }
        alertConroller.addAction(okAction)

        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alertConroller.addAction(cancelAction)

        present(alertConroller, animated: true, completion: nil)
    }
    
    //MARK: - TableManagementActionPopUpDelegate method
    func dataSelected(data: TableManagementActionPopUpData)
    {
        currentTable.name = tableName.text ?? ""
        currentTable.areaData = TableAreaData(areaId: data.id , areaName: data.desc)
        setupView()
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showAreaOption"
        {
            guard
                let dvc = segue.destination as? TableManagementActionPopUp,
                let possiblePopover = dvc.popoverPresentationController,
                let button = sender as? UIButton,
                let arrAreaDataRaw = shareddb.shared().get_list_of_areas() as? [[String:String]]
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = CGRect.init(x: button.frame.midX, y: button.frame.midY, width: 0, height: 0)
            
            let arrAreaData = arrAreaDataRaw.map({TableManagementActionPopUpData.init(id: $0["id"] ?? "", desc: $0["area_name"] ?? "")})
            dvc.arrData = arrAreaData
            dvc.selectedData = TableManagementActionPopUpData.init(id: currentTable.areaData.areaId, desc: currentTable.areaData.areaName)
            dvc.delegate = self
        }
    }
    
    
}
