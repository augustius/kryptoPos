//
//  TableArea.swift
//  Manager
//
//  Created by augustius cokroe on 22/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct TableAreaData {
    var areaId = ""
    var areaName = ""
}

class TableAreaCell:UICollectionViewCell
{
    @IBOutlet private var areaName: UILabel!
    var areaData = TableAreaData() {
        didSet{
            areaName.text = areaData.areaName
        }
    }
}

class TableArea:UICollectionViewController
{
    @IBOutlet var longPressGestureRecognizer: UILongPressGestureRecognizer!
    private var areaData = [[String:String]]()
    private var selectedArea = TableAreaData()
    private var cellMoved = false

    //MARK: - View cycle
    override func viewDidLoad()
    {
        if let safeData = shareddb.shared().get_list_of_areas() as? [[String:String]]
        {
            areaData = safeData
        }
        collectionView?.addGestureRecognizer(longPressGestureRecognizer)
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        if cellMoved
        {
            var tempArr = [[String:String]]()
            for (index,dict) in areaData.reversed().enumerated()
            {
                var tempDict = dict
                tempDict["tba_sequence_no"] = (index+1).description
                tempArr.append(tempDict)
            }
            shareddb.shared().updateArea(tempArr)
            Networking.shared().putData(forWriting: .writingArea)
        }
    }

    //MARK: - IBAction method
    @IBAction func menuButtonPressed(_ sender: UIBarButtonItem)
    {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func handleLongPressed(_ sender: UILongPressGestureRecognizer)
    {
        switch sender.state
        {
        case .began:
            guard let selectedIndexPath = collectionView?.indexPathForItem(at: sender.location(in: collectionView)) else {
                break
            }
            collectionView?.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            self.collectionView?.updateInteractiveMovementTargetPosition(sender.location(in: sender.view!))
        case .ended:
            self.collectionView?.endInteractiveMovement()
        default:
            self.collectionView?.cancelInteractiveMovement()
        }
    }

    //MARK: - UICollectionView delegate
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return areaData.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TableAreaCell", for: indexPath)

        if let tableAreaCell = cell as? TableAreaCell
        {
            let area = areaData[indexPath.row]

            guard
                let id = area["id"],
                let areaName = area["area_name"]
                else { print("error on \(#function), \(#line) "); return tableAreaCell}

            let data = TableAreaData.init(areaId: id, areaName: areaName)
            tableAreaCell.areaData = data

            return tableAreaCell
        }

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath)
    {
        let temp = areaData.remove(at: sourceIndexPath.item)
        areaData.insert(temp, at: destinationIndexPath.item)
        
        if !cellMoved
        {
            cellMoved = (sourceIndexPath != destinationIndexPath)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool
    {
        return ModelController.shared().currentCompleteOrder.hold_order_id == nil
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if let selectedCell = collectionView.cellForItem(at: indexPath) as? TableAreaCell
        {
            selectedArea = selectedCell.areaData
            performSegue(withIdentifier: "showTableOnArea", sender: self)
        }
    }
    
    // MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showTableOnArea"
        {
            guard
                let dvc = segue.destination as? TableManagement
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.selectedArea = selectedArea
        }
    }
    
    
}










