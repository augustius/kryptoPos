//
//  Option.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 31/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class Option:UIViewController
{
    @IBOutlet var BigShiftStatus: UILabel!
    @IBOutlet var shiftStatus: UILabel!
    @IBOutlet var lastShiftTime: UILabel!
    @IBOutlet var blurView: UIView!

    //MARK: - View Cycle
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        setupView()
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func resetCounter()
    {
        shareddb.shared().reset_order_count_to_one()
        present_sclalert("Success", message: "Counter has been reset", alert_style: .success)
    }

    
}
