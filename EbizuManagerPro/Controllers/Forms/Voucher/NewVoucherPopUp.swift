//
//  NewVoucherPopUp.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 29/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

protocol NewVoucherPopUpDelegate: class {
    func predefinedVoucherSelected(vouc:Voucher)
    func predefinedVoucherCleared()
}

class NewVoucherPopUp: UIViewController,UITableViewDataSource,UITableViewDelegate
{

    @IBOutlet var predefinedTableView: UITableView!
    weak var delegate: NewVoucherPopUpDelegate?
    var selectedPredefinedVoucherID = ""
    private var voucList = [Any]()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        voucList = shareddb.shared().get_all_voucher()
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func clearVoucher(_ sender: UIBarButtonItem)
    {
        delegate?.predefinedVoucherCleared()
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return voucList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "predefinedVoucherTableCell", for: indexPath)
        
        guard
            let voucData = voucList[indexPath.row] as? [AnyHashable : Any],
            let vouc = Voucher.init(dictionary: voucData)
            else { print("error on \(#function), \(#line) "); return cell}
        
        cell.textLabel?.font = UIFont(name: "Poppins-Light", size: 18)
        cell.textLabel?.text = vouc.voucher_name
        cell.accessoryType = selectedPredefinedVoucherID == vouc.voucher_id ? .checkmark : .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        guard
            let voucData = voucList[indexPath.row] as? [AnyHashable : Any],
            let vouc = Voucher.init(dictionary: voucData)
            else { print("error on \(#function), \(#line) "); return}
        
        delegate?.predefinedVoucherSelected(vouc: vouc)
        dismiss(animated: true, completion: nil)
    }

    
    
    
}
