//
//  NewVoucherController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 29/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension NewVoucher
{
    
    func adjustView()
    {
        predefinedVoucher.setTitle(currentCompleteOrderCopy.voucher_name != nil && currentCompleteOrderCopy.voucher_name != ""
            ? currentCompleteOrderCopy.voucher_name : "Choose Voucher", for: .normal)
        voucherAmount.text = String(format:"%.2f",currentCompleteOrderCopy.voucher_amount != NSDecimalNumber.notANumber ? currentCompleteOrderCopy.voucher_amount.doubleValue : 0.00)
        
        memberNo.text = currentCompleteOrderCopy.voucher_member_number
        memberNo.isUserInteractionEnabled = currentCompleteOrderCopy.voucher_id != nil
        referenceNo.text = currentCompleteOrderCopy.voucher_ref_number
        referenceNo.isUserInteractionEnabled = currentCompleteOrderCopy.voucher_id != nil
    }
    
    //MARK: - VoucherPopUpDelegate method
    func predefinedVoucherSelected(vouc: Voucher)
    {
        currentCompleteOrderCopy.voucher_id = vouc.voucher_id
        currentCompleteOrderCopy.voucher_name = vouc.voucher_name
        currentCompleteOrderCopy.voucher_amount = NSDecimalNumber.init(string:vouc.voucher_amount ?? "0.00")
        currentCompleteOrderCopy.voucher_type_id = vouc.voucher_type_id
        currentCompleteOrderCopy.voucher_type_name = vouc.voucher_type_name
        currentCompleteOrderCopy.voucher_member_number = vouc.voucher_member_number ?? ""
        currentCompleteOrderCopy.voucher_ref_number = vouc.voucher_ref_number ?? ""
        currentCompleteOrderCopy.voucher_bought_detail_id = vouc.voucher_bought_detail_id ?? ""
        
        adjustView()
    }
    
    func predefinedVoucherCleared()
    {
        let emptyVoucher = Voucher()
        predefinedVoucherSelected(vouc: emptyVoucher)
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showPredefinedVoucher"
        {
            guard
                let button = sender as? UIButton,
                let dvc = segue.destination as? NewVoucherPopUp,
                let possiblePopover = dvc.popoverPresentationController
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = button.bounds
            possiblePopover.sourceView = button
            dvc.delegate = self
            dvc.selectedPredefinedVoucherID = currentCompleteOrderCopy.voucher_id ?? ""
        }
    }
}
