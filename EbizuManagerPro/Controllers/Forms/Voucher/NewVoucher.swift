//
//  NewVoucher.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 29/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class NewVoucher: UITableViewController,NewVoucherPopUpDelegate
{
    @IBOutlet var predefinedVoucher: UIButton!
    @IBOutlet var voucherAmount: UILabel!
    @IBOutlet var memberNo: UITextField!
    @IBOutlet var referenceNo: UITextField!

    var currentCompleteOrderCopy = CompleteOrder()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        if let safeCompleteOrder = ModelController.shared().currentCompleteOrder.copy() as? CompleteOrder
        {
            currentCompleteOrderCopy = safeCompleteOrder
        }
        sendFormToGoogleAnalytic()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        adjustView()
    }
    
    //MARK: - IBAction method
    @IBAction func donePressed()
    {
        currentCompleteOrderCopy.voucher_member_number = memberNo.text
        currentCompleteOrderCopy.voucher_ref_number = referenceNo.text
        
        ModelController.shared().currentCompleteOrder = currentCompleteOrderCopy
        performSegue(withIdentifier: "updateBillVoucher", sender: self)
    }
    
    @IBAction func cancelPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    
}
