//
//  NewPaymentComplete.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 18/08/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class NewPaymentComplete: UIViewController, UITextFieldDelegate
{
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var mostBottomConstraint: NSLayoutConstraint!
    @IBOutlet private var paymentType: UILabel!
    @IBOutlet private var totalAmountDue: UILabel!
    @IBOutlet private var amountTendered: UILabel!
    @IBOutlet private var changeDue: UILabel!
    @IBOutlet private var emailTextfield: UITextField!
    var completeMultiPayData = [[String:String]]()
    var dbErrorMsg = ""
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        paymentType.text = ModelController.shared().currentCompleteOrder.pay_type_name.capitalized
        totalAmountDue.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.total.doubleValue).addCurrency()
        amountTendered.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.payment_amount.doubleValue).addCurrency()
        changeDue.text = String(format:"%.2f", ModelController.shared().currentCompleteOrder.payment_changes.doubleValue).addCurrency()
        
        saveTransactionToDatabase()
        setupTransactionForPrinting() //this is to prepare the receipt before printing to improve printing speed
        sendPaymentDataToCustomerDisplay()
    }
    
    //MARK: - IBAction Method
    @IBAction func printReceipt()
    {
        if let printingOptionEnable = userDefaults.value(forKey: PRINTING_OPTION) as? Bool, printingOptionEnable {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "showPrinterOption", sender: nil)
            }
        } else {
            EbizuPrinter.sharedPrinter.printSales()
        }
    }
    
    @IBAction func donePressed()
    {
        let emailEntered = emailTextfield.text ?? ""
        
        if  emailEntered != ""
        {
            if !emailEntered.isValidEmail()
            {
                present_sclalert("Invalid Email", message: "Please enter a valid email to send email.", alert_style: .error)
                return
            }
            shareddb.shared().add_pending_email(emailEntered, withTrxId: ModelController.shared().currentCompleteOrder.trx_id)
        }
        
        performSegue(withIdentifier: "paymentCompleteReturn", sender: self)
    }

    //MARK: - UITextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        keyboardShowUI(true)
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        keyboardShowUI(false)
    }

    func keyboardShowUI(_ up: Bool)
    {
        mostBottomConstraint.constant = up ? 200 : 0
    }

    // MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPrinterOption" {
            guard let dvc = segue.destination as? PrinterOption else { return }
            dvc.forPrintBill = false
        }
    }

}
