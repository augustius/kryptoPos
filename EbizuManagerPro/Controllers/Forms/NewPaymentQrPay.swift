//
//  NewPaymentQrPay.swift
//  KryptoPOS
//
//  Created by augustius cokroe on 28/07/2018.
//  Copyright © 2018 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class NewPaymentQrPay: UIViewController {

    @IBOutlet weak var qrImageView: UIImageView!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var verifyButton: UIButton!

    private var countdownTimer: Timer?
    private var countdown: Int = 25
    private var manualStarted: Bool = false

    var paymentMethodId: String!
    var transRefId: String!
    var amountStr: String!
    var qrRefId: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        countdownLabel.isHidden = true
        enableManualButton(false)
        startObservingQrPayImage()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        endObservingQrPayImage()
        endObservingQrPayStatus()
    }

    private func enableManualButton(_ enable: Bool = true) {
        manualStarted = enable
        DispatchQueue.main.async {
            self.verifyButton.isEnabled = enable
            self.verifyButton.backgroundColor = enable ? UIColor(red:0.10, green:0.74, blue:0.61, alpha:1.0) : UIColor.lightGray
        }
    }

    private func presentManualCompleteAlert(_ title: String, message: String = "") {
        let alert = UIAlertController(title: title, message: "\(message)Do you still wish to proceed ?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { [weak self] (_) in
            self?.qrPayCompleted()
        }
        let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
        alert.addAction(yesAction)
        alert.addAction(noAction)
				DispatchQueue.main.async { [weak self] in
					self?.present(alert, animated: true, completion: nil)
				}
    }

    // MARK: - Network call for QRPAY image
    private func startObservingQrPayImage() {
        notification.addObserver(self, selector: #selector(receiveSuccessQrPay(notif:)), name: networkNotification.getMaybankQrPaySuccess.notification, object: nil)
        notification.addObserver(self, selector: #selector(receiveFailQrPay(notif:)), name: networkNotification.getMaybankQrPayFail.notification, object: nil)
        showLoadingScreen()
        Networking.shared().getMaybankQrCode(paymentMethodId: paymentMethodId, transRefId: transRefId, amountStr: amountStr)
    }

    private func endObservingQrPayImage() {
        notification.removeObserver(self, name: networkNotification.getMaybankQrPaySuccess.notification, object: nil)
        notification.removeObserver(self, name: networkNotification.getMaybankQrPayFail.notification, object: nil)
        hideLoadingScreen()
    }

    @objc private func receiveSuccessQrPay(notif:NSNotification)
    {
        guard
            let responseDict = notif.object as? [String : AnyObject],
            let qrData = responseDict["qr_data"] as? String,
            let qrRef = responseDict["qr_ref_id"] as? String
            else {
                present_sclalert("QRCODE ERROR", message: "Incomplete QrCode data", alert_style: .error)
                return }

        qrRefId = qrRef
        DispatchQueue.main.async {
            self.qrImageView.image = qrData.generateQRCode()
        }
        endObservingQrPayImage()
        startObservingQrPayStatus()
        startResendTimer()
    }

    @objc private func receiveFailQrPay(notif:NSNotification)
    {
        if let userDict = notif.userInfo as? [String:String]
        {
            present_sclalert(title, message: userDict["message"], alert_style: .error)
        }
        endObservingQrPayImage()
    }

    // MARK: - Network call for QRPAY status
    private func checkQrPayStatus() {
        Networking.shared().getQrCodeStatus(paymentMethodId: paymentMethodId, qrRefId: qrRefId)
    }

    private func startObservingManualQrPayStatus() {
        startObservingQrPayStatus()
        showLoadingScreen()
    }

    private func startObservingQrPayStatus() {
        notification.addObserver(self, selector: #selector(receiveSuccessQrStatus(notif:)), name: networkNotification.getQrPaySuccess.notification, object: nil)
        notification.addObserver(self, selector: #selector(receiveFailQrStatus(notif:)), name: networkNotification.getQrPayFail.notification, object: nil)
    }

    private func endObservingManualQrPayStatus() {
        endObservingQrPayStatus()
        hideLoadingScreen()
    }

    private func endObservingQrPayStatus() {
        stopTimer()
        notification.removeObserver(self, name: networkNotification.getQrPaySuccess.notification, object: nil)
        notification.removeObserver(self, name: networkNotification.getQrPayFail.notification, object: nil)
    }

    @objc private func receiveSuccessQrStatus(notif:NSNotification)
    {
        guard
            let responseDict = notif.object as? [String : AnyObject],
            let status = responseDict["status"] as? String
            else {
                present_sclalert("QRCODE STATUS ERROR", message: "unable to find status", alert_style: .error)
                return }

        if manualStarted {
            endObservingManualQrPayStatus()
            if status == "OK" {
                print("manual qr pay success")
                qrPayCompleted()
            } else if status == "PENDING" {
                presentManualCompleteAlert("Payment Status Pending")
            }
        } else {
            if status == "OK" {
                endObservingQrPayStatus()
                print("qr pay success")
                qrPayCompleted()
            } else if status == "PENDING" {
                if !(countdownTimer?.isValid ?? false) {
                    enableManualButton()
                }
            }
        }
    }

    @objc private func receiveFailQrStatus(notif:NSNotification)
    {
        guard
            let userDict = notif.userInfo as? [String:String],
            let message = userDict["message"]
            else { return }

        if manualStarted {
            endObservingManualQrPayStatus()
            presentManualCompleteAlert("Payment Status Fail", message: "\(message).\n")
        } else {
            present_sclalert("Payment Status Fail", message: message, alert_style: .error)
            endObservingQrPayStatus()
            enableManualButton()
        }
    }


    // MARK: - Timer function
    private func startResendTimer() {
        DispatchQueue.main.async {
            self.countdownLabel.isHidden = false
            self.countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateResendButton), userInfo: nil, repeats: true)
        }
    }

    private func stopTimer() {
        countdownTimer?.invalidate()
        DispatchQueue.main.async {
            self.countdownLabel.isHidden = true
        }
    }

    @objc private func updateResendButton() {
        countdownLabel.text = timeFormatted(countdown)
        switch countdown {
        case 15,10,0:
            checkQrPayStatus()
            if countdown == 0 {
                stopTimer()
            }
        default:
            break
        }
        countdown -= 1
    }

    private func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%01d:%02d", minutes, seconds)
    }

    // MARK: - IBAction method
    @IBAction private func manualVerify() {
        startObservingManualQrPayStatus()
        checkQrPayStatus()
    }

    @IBAction private func dismissView() {
        dismiss(animated: true, completion: nil)
    }

    // MARK: - segue method
    private func qrPayCompleted() {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "qrPayDone", sender: self)
        }
    }
}
