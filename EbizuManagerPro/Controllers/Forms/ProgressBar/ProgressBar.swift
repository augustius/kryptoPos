//
//  ProgressBar.swift
//  Manager
//
//  Created by augustius cokroe on 01/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class ProgressBar: UIViewController {
    
    @IBOutlet var progressView: UIProgressView!
    @IBOutlet var progressMessage: UILabel!
    @IBOutlet var continueButton: UIButton!
    var progress:Float = 0.0
    var readingResponseArr = [String]()
    var respondedResponseArr = [String]()
    
    override func viewDidLoad() {
        progressView.progress = 0.0
        continueButton.isHidden = true
        loadProgress()
        readingResponseArr = Networking.shared().getPartialReadingApiResponseName()
        notification.addObserver(self, selector: #selector(syncResponseNotification), name: networkNotification.transactionSyncForm.notification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        if let reachability = Reachability.networkReachabilityForInternetConnection()
        {
            if reachability.isReachable
            {
                showLoadingScreen()
                _ = Networking.shared().executePartialReadingApi()
            }
            else
            {
                notification.removeObserver(self, name: networkNotification.transactionSyncForm.notification, object: nil)
                present_sclalert("NO INTERNET", message: "Fail to retrieve complete content. \nPress 'Continue' button below to proceed. \nPlease go to Report->Transaction Sync, and click 'Sync All' to retrive the data manually. \nThank You.", alert_style: .error)
                continueButton.isHidden = false
            }
        }
    }
    
    @IBAction func continuePressed()
    {
        goToPosForm()
    }
    
    func loadProgress()
    {
        DispatchQueue.main.async {
            self.progressView.progress = self.progress
            self.progressMessage.text = "Receiving \(self.respondedResponseArr.count) out of \(self.readingResponseArr.count) contents"
        }
    }
    
    @objc func syncResponseNotification(notif:NSNotification)
    {
        guard
            let responseDict = notif.object as? syncResponse
            else
        {
            hideLoadingScreen()
            notification.removeObserver(self, name: networkNotification.transactionSyncForm.notification, object: nil)
            present_sclalert("CONVERSION ERROR", message: "Fail to retrieve complete content. \nPress 'Continue' button below to proceed. \nPlease go to Report->Transaction Sync, and click 'Sync All' to retrive the data manually. \nThank You.", alert_style: .info)
            print("error on \(#function), \(#line) ")
            continueButton.isHidden = false
            return
        }
        
        switch responseDict.status
        {
        case .onProgress:
            break
        case .noDataToSend:
            if !respondedResponseArr.contains(responseDict.name)
            {
                respondedResponseArr.append(responseDict.name)
            }
            break
        case .Success:
            if !respondedResponseArr.contains(responseDict.name)
            {
                respondedResponseArr.append(responseDict.name)
            }
            break
        case .fail:
            if !respondedResponseArr.contains(responseDict.name)
            {
                respondedResponseArr.append(responseDict.name)
            }
            break
        }
        
        if respondedResponseArr.count == readingResponseArr.count
        {
            while shareddb.shared().operation_still_running()
            {
                print("still waiting");
            }
            hideLoadingScreen()
            notification.removeObserver(self, name: networkNotification.transactionSyncForm.notification, object: nil)
            goToPosForm()
        }
        
        progress = Float(respondedResponseArr.count) / Float(readingResponseArr.count)
        loadProgress()
    }
    
    func goToPosForm()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "newPos", sender: self)
        }
    }
    
}
