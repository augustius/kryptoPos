//
//  newPINcontroller.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 01/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension newPIN
{
    func setupData()
    {
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        userPicWidthConstraint.constant = 0.0
    }
    
    func setupView()
    {
        pinSuccess = false
        shiftStatus.text = userDefaults.bool(forKey: SHIFT_OPENED) ? "Open" : "Closed"
        shiftStatus.textColor = userDefaults.bool(forKey: SHIFT_OPENED) ? UIColor.green : UIColor.red
        
        view.addSubview(blurView)
        view.sendSubviewToBack(blurView)
    }
    
    func validatePin()
    {
        if shareddb.shared().validate_user_pin(passCode.MD5())
        {
            userDefaults.set(StringUtils.get_user_detail("user_id"), forKey: CURRENT_USERID)
            if let roleId = Int(StringUtils.get_user_detail("super_user"))
            {
                appDelegate?.user_role_id = Int32(roleId)
            }
            userDefaults.set(NSDate(), forKey: CURRENT_LOGIN_DATE)
            pinSuccess = true
        }
        else
        {
            present_sclalert("Invalid Pin", message: "Please enter valid pin", alert_style: .error)
            resetPin()
        }
    }
    
    func resetPin()
    {
        passCode = ""
        
        firstPin.backgroundColor = UIColor.lightGray
        secondPin.backgroundColor = UIColor.lightGray
        thirdPin.backgroundColor = UIColor.lightGray
        fourthPin.backgroundColor = UIColor.lightGray
        
        pinSuccess = false
    }
    
    func handleUserImage()
    {
        let defaultPic = UIImage.init(named: "UserAvathar")
        let currentPic = userPic.backgroundImage(for: .normal)
        
        if currentPic == defaultPic
        {
            present_alert("Change Profile Picture",
                          message: "Where would you like to get your profile picture from?",
                          yesTitle: "Gallery",
                          noTitle: "Camera",
                          yesAlertActionCompletionHandler:
                            { (UIAlertAction) in
                                
                                self.imagePicker.sourceType = .savedPhotosAlbum
                                self.present(self.imagePicker, animated: true, completion: nil)
                            },
                          noAlertActionCompletionHandler:
                            { (UIAlertAction) in
                                
                                self.imagePicker.sourceType = .camera
                                self.present(self.imagePicker, animated: true, completion: nil)
                            })
        }
        else
        {
            present_alert("Delete Profile Picture",
                          message: "Would you like to delete your current profile picture?",
                          yesAlertActionCompletionHandler:
                            { (UIAlertAction) in
                            
                                self.userPic.setBackgroundImage(defaultPic, for: .normal)
                            },
                          noAlertActionCompletionHandler:nil)
        }
    }
    
    func downloadImage(chosenImage: UIImage)
    {
        let imageData = chosenImage.pngData()
        
        if let currentUserId = userDefaults.string(forKey: CURRENT_USERID)
        {
            FileUtils.writeToDocuemntsDirectory(atPath: POSImages+"/"+IMG_USERS,
                                                fileData: imageData,
                                                withPrefix: currentUserId+"_userimage",
                                                andExtension: "jpg")
        }
    }
}
