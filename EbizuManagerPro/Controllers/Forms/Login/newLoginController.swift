//
//  newLoginController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 02/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension NewLogin
{
    func setupView()
    {
        if let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        {
            appVersion.text = "version "+version
            tableView.backgroundView = self.backgroundView
        }
    }
    
    func checkFirstTimeLogin()
    {
        if userDefaults.bool(forKey: CUSTOMER_DISPLAY)
        {
            navigateToCustomerDisplay()
        }
        else
        {            
            if shareddb.shared().business_details_exist() // if not first time
            {
                navigateToPosform()
            }
        }
    }
    
    func handleLogin()
    {
        if username.text == "" || password.text == ""
        {
            present_sclalert("Attention", message: "Please enter a valid username or password to proceed.", alert_style: .info)
        }
        else
        {
            if let reachability = Reachability.networkReachabilityForInternetConnection()
            {
                if reachability.isReachable
                {
                    showLoadingScreen()
                    Networking.shared().verifyLogin(username: username.text ?? "",
                                                    password: password.text ?? "")
                }
                else
                {
                    present_sclalert("Alert", message: "Internet connectivity is required for first time login, please connect to the internet.", alert_style: .error)
                }
            }
        }
    }
    
    //MARK: - Navigation method
    func navigateToProgressForm()
    {
        DispatchQueue.main.async {
            self.username.isHidden = true
            self.password.isHidden = true
            self.login.isHidden = true
            self.appVersion.isHidden = true
            self.setAsCD.isHidden = true
            
            self.performSegue(withIdentifier: "progressBar", sender: self)
        }
    }
    
    func navigateToPosform()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "newPos", sender: self)
        }
    }
    
    func navigateToCustomerDisplay()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "customerDisplay", sender: self)
        }
    }
    
    //MARK: - Networking notification
    func loginSuccessful(notification:Notification)
    {
        hideLoadingScreen()
        navigateToProgressForm()
    }
    
    func loginFailed(notification:Notification)
    {
        hideLoadingScreen()
        
        if let userDict = notification.userInfo as? [String:String]
        {
            present_sclalert(title, message: userDict["message"], alert_style: .error)
        }
    }
}
