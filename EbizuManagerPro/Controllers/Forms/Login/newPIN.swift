//
//  newPIN.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 01/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class newPIN:UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    @IBOutlet var blurView: UIView!
    
    @IBOutlet var userPic: UIButton!
    @IBOutlet weak var userPicWidthConstraint: NSLayoutConstraint!
    @IBOutlet var shiftStatus: UILabel!
    
    @IBOutlet var firstPin: UIButton!
    @IBOutlet var secondPin: UIButton!
    @IBOutlet var thirdPin: UIButton!
    @IBOutlet var fourthPin: UIButton!
    
    @IBOutlet var allNumKey: [UIButton]!
    @IBOutlet var clockIn: UIButton!
    @IBOutlet var clockOut: UIButton!

    @IBOutlet var enter: UIButton!
    @IBOutlet var clear: UIButton!
    
    var passCode = ""
    let imagePicker = NoAutorotateUIImagePickerController()
    var pinSuccess = false
    {
        didSet
        {
            for numKey in self.allNumKey
            {
                numKey.isUserInteractionEnabled = !self.pinSuccess
                numKey.alpha = numKey.isUserInteractionEnabled ? 1.0 : 0.3
            }
            
            self.enter.isUserInteractionEnabled = self.pinSuccess
            self.enter.alpha = self.enter.isUserInteractionEnabled ? 1.0 : 0.3
            
            self.clear.isUserInteractionEnabled = self.pinSuccess
            self.clear.alpha = self.clear.isUserInteractionEnabled ? 1.0 : 0.3
            
            self.userPicWidthConstraint.constant = self.pinSuccess ? 100.0 : 0.0
            
            if self.pinSuccess
            {
                userPic.setBackgroundImage(UIUtils.get_employee_image(userDefaults.string(forKey: CURRENT_USERID)), for: .normal)
                
                let hadClockIn = shareddb.shared().employee_clocked_in()
                if appDelegate?.user_role_id != EMPLOYEE
                {
                    self.clockIn.isUserInteractionEnabled = !hadClockIn
                    self.clockIn.alpha = self.clockIn.isUserInteractionEnabled ? 1.0 : 0.3
                    
                    self.clockOut.isUserInteractionEnabled = hadClockIn
                    self.clockOut.alpha = self.clockOut.isUserInteractionEnabled ? 1.0 : 0.3
                }
            }
            else
            {
                self.clockIn.isUserInteractionEnabled = false
                self.clockIn.alpha = 0.3
                
                self.clockOut.isUserInteractionEnabled = false
                self.clockOut.alpha = 0.3
            }
            
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        setupData()
        setupView()
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    
    @IBAction func changeUserImage()
    {
        handleUserImage()
    }
    
    @IBAction func keyPressed(_ sender: UIButton)
    {
        if let safeKey = sender.titleLabel?.text
        {
            let pinColor = UIColor(netHex: "0x3FA1EE")
            passCode = passCode.appending(safeKey)
            
            //enable clear button everytime key pressed
            clear.isUserInteractionEnabled = true
            clear.alpha = clear.isUserInteractionEnabled ? 1.0 : 0.3
            
            if passCode.count <= 4
            {
                switch passCode.count
                {
                case 1:
                    firstPin.backgroundColor = pinColor
                    break
                case 2:
                    secondPin.backgroundColor = pinColor
                    break
                case 3:
                    thirdPin.backgroundColor = pinColor
                    break
                case 4:
                    fourthPin.backgroundColor = pinColor
                    validatePin()
                    break
                default:
                    break
                }
            }
        }
    }
    
    @IBAction func clockInPressed()
    {
        shareddb.shared().insert_new_employee_timecard()
        resetPin()
    }
    
    @IBAction func clockOutPressed()
    {
        shareddb.shared().update_employee_timecard()
        resetPin()
    }
    
    @IBAction func enterPin()
    {
        Networking.shared().putData(forWriting: .writingTimecard)
        Networking.shared().putData(forWriting: .writingUserright)
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearPin()
    {
        resetPin()
    }
    
    @IBAction func forgetPinPressed()
    {
        present_sclalert("Get Employee Pin", message: "Dear Store Manager,\n Can't remember your pin? No worries, you can reset it in the Ebizu Business Centre!\n\n Not a Store Manager?\n Please ask them for reset your pin!", alert_style: .info)
    }
    
    
    //MARK: - UIImagePickerController delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let chosenImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage
        {
            userPic.setBackgroundImage(chosenImage, for: .normal)
            downloadImage(chosenImage: chosenImage)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
