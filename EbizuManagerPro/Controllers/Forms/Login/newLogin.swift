//
//  NewLogin.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 02/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class NewLogin: UITableViewController,UITextFieldDelegate
{
    @IBOutlet var backgroundView: UIView!
    @IBOutlet var ebizuLogo: UIImageView!
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var login: UIButton!
    @IBOutlet var appVersion: UILabel!
    @IBOutlet var setAsCD: UIButton!
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        setupView()
        sendFormToGoogleAnalytic()
    }

    override func viewDidAppear(_ animated: Bool)
    {
        checkFirstTimeLogin()
        notification.addObserver(self, selector: #selector(loginSuccessful(notification:)), name: networkNotification.verifyLoginSuccess.notification, object: nil)
        notification.addObserver(self, selector: #selector(loginFailed(notification:)), name: networkNotification.verifyLoginFail.notification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        notification.removeObserver(self, name: networkNotification.verifyLoginSuccess.notification, object: nil)
        notification.removeObserver(self, name: networkNotification.verifyLoginFail.notification, object: nil)
    }

    //MARK: - IBAction Method
    @IBAction func loginPressed()
    {
        handleLogin()
    }
    
    //MARK: - UITextfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == username
        {
            password.becomeFirstResponder()
        }
        else if textField == password
        {
            loginPressed()
        }
        return true
    }
    
    
}
