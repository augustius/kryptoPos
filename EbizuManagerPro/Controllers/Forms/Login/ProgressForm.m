//
//  ProgressForm.m
//  EbizuManagerPro
//
//  Created by Vinoth on 23/12/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "ProgressForm.h"
#import "SharedServices.h"
@interface ProgressForm ()
{
    SharedServices *shared_service;
}

@end

@implementation ProgressForm

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shared_service = [[SharedServices alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(progress_notification:) name:@"SyncProgress"  object:nil];
    
    if ([NetworkUtils hasNetworkConnection])
    {
        [shared_service send_push_token];
        if(APP_DELEGATE.is_first_time_login)
        {
            [_lbl_header_message setText:@"Welcome to cloud based POS system."];
            [_lbl_message setText:@"Please wait for a while, We are doing basic setup for you to continue.. Thanks for signup with Ebizu:-)"];
//            progress_level = no_of_reading_api;
            progress_level = no_of_reading_api + no_of_writing_api;
            [SESSION_DATA sync_all_data];
//            [shared_service sync_all_shift_details_from_service_to_db];
        }
        else
        {
            [_lbl_header_message setText:[NSString stringWithFormat:@"Wecome back %@",[StringUtils get_business_detail:@"business_name"]]];
            [_lbl_message setText:@"Please wait for a while, We are downloading new data to make POS as upto date."];
            APP_DELEGATE.need_to_call_reading_api = YES;
//            progress_level = no_of_reading_api + no_of_writing_api;
            [SESSION_DATA sync_transactions];
        }
    }
    
    progress_level = 100 / progress_level;
    progress_level = progress_level / 10;
    NSLog(@"%lu",(unsigned long)progress_level);

    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(progress_notification:) name:@"SyncProgress"  object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SyncProgress" object:nil];
}


-(void)progress_notification:(NSNotification*)notification
{
    NSLog(@"%@",notification.object);
    
    [self increase_progress];
    
    if( [notification.object isEqualToString:@"CUSTOM_INPUT"]) // Please change the condition checking, If any new API added after custom_input
    {
        [_circleProgressBar setProgress:100.0f animated:NO];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SyncProgress" object:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
        [self performSegueWithIdentifier:@"Progress View" sender:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)increase_progress
{
    [_circleProgressBar setProgress:(_circleProgressBar.progress + progress_level) animated:YES];
}
-(IBAction)btn_proceed:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
    [self performSegueWithIdentifier:@"Progress View" sender:self];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
