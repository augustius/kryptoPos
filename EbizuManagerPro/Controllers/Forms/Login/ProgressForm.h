//
//  ProgressForm.h
//  EbizuManagerPro
//
//  Created by Vinoth on 23/12/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CircleProgressBar.h"

/** Change it based on NEW API functions - Now count from 1.5 version **/
NSInteger const no_of_reading_api = 21;
NSInteger const no_of_writing_api = 12;

@interface ProgressForm : UIViewController
{
    CGFloat progress_level;
}

@property (weak, nonatomic) IBOutlet CircleProgressBar *circleProgressBar;

@property (weak, nonatomic) IBOutlet UILabel *lbl_message;
@property (weak, nonatomic) IBOutlet UILabel *lbl_header_message;


@end
