//
//  CustomerDetailSales.swift
//  Manager
//
//  Created by augustius cokroe on 17/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class CustomerDetailSales: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var sampleReceiptTable: UITableView!
    var sampleReceiptData = [receiptData]()
    var selectedSales = CustomerLatestTransactionData()

    // MARK: - View cycle
    override func viewDidLoad()
    {
        sampleReceiptTable.register(UINib(nibName:"CustomReceiptCell", bundle: nil), forCellReuseIdentifier: "ReceiptPreviewCell")
        sampleReceiptTable.rowHeight = UITableView.automaticDimension
        sampleReceiptTable.estimatedRowHeight = 80.0
        setupPreview()
        sendFormToGoogleAnalytic()
    }
    
    // MARK: - IBAction method
    @IBAction func dismissFormTapped()
    {
        dismiss(animated: true, completion: nil)
    }
    
    func setupPreview()
    {
        EbizuPrinter.sharedPrinter.receiptDataArr = [receiptData]()
        if let sampleData = shareddb.shared().get_transaction_detail_(by_transaction_number: selectedSales.id)
        {
            sampleData.newCalculate()
            EbizuPrinter.sharedPrinter.getSampleBody(sampleCompleteOrder: sampleData)
        }
        sampleReceiptData = EbizuPrinter.sharedPrinter.receiptDataArr
    }
    
    //MARK: - UIViewTableViewDelegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptPreviewCell", for: indexPath)
        
        if let receiptPreviewCell = cell as? ReceiptPreviewCell
        {
            let sample = sampleReceiptData[indexPath.row]
            
            receiptPreviewCell.sample = sample
            
            return receiptPreviewCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sampleReceiptData.count
    }
}
