//
//  POSCell.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class CategoriesCell: UICollectionViewCell
{
    @IBOutlet var back: UIButton!
    @IBOutlet var initialName: UIButton!
    @IBOutlet var name: UILabel!
}

class ProductCell: UICollectionViewCell
{
    @IBOutlet var back: UIButton!
    @IBOutlet var initialName: UIButton!
    @IBOutlet var name: UILabel!
    @IBOutlet var price: UILabel!
}

class ProductWithItemCell: UICollectionViewCell
{
    @IBOutlet var back: UIButton!
    @IBOutlet var initialName: UIButton!
    @IBOutlet var name: UILabel!
    @IBOutlet var price: UILabel!
}

class SelectedTableCell: UICollectionViewCell
{
    @IBOutlet var shape: UIButton!
    @IBOutlet weak var name: UILabel!
}

class OrderCell: UITableViewCell
{
    @IBOutlet var itemName: UILabel!
    @IBOutlet var minus: UIButton!
    @IBOutlet var qty: UITextField!
    @IBOutlet var plus: UIButton!
    @IBOutlet var total: UILabel!
    @IBOutlet var remarks: UILabel!
    @IBOutlet var modifier: UILabel!
    @IBOutlet var discount: UILabel!
    @IBOutlet var gst: UILabel!
    
    @IBAction func minusPlusQty(_ sender: UIButton)
    {
        guard
            let safeItemQty = Int(qty.text!),
            let buttonLabel = sender.titleLabel?.text
            else { print("error on \(#function), \(#line) "); return}
        
        var itemQty = safeItemQty
        
        switch buttonLabel
        {
        case "+":
            if itemQty < 999
            {
                itemQty += 1
                qty.text = "\(itemQty)"
            }
            break
        case "-":
            if itemQty > 1
            {
                itemQty -= 1
                qty.text = "\(itemQty)"
            }
            break
        default:
            break
        }
    }
}
