//
//  ReportPaymentOption.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 13/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct paymentData
{
    var name = "All Payment Type"
    var id = ""
}

class ReportPaymentOptionCell: UITableViewCell
{
    @IBOutlet var name: UILabel!
    var id = ""
}

protocol ReportPaymentOptionDelegate: class {
    func paymentOptionSelected(payment:paymentData)
}

class ReportPaymentOption: UITableViewController
{
    weak var delegate: ReportPaymentOptionDelegate?
    var selectedPaymentData = paymentData()
    var allPaymentData = [[String:AnyObject]]()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        //additional data to show all
        var dict = [String:AnyObject]()
        dict["id"] = "" as AnyObject
        dict["pay_name"] = "All Payment Type" as AnyObject
        allPaymentData.append(dict)
        
        if let data = userDefaults.object(forKey: ALL_PAYMENT_METHOD) as? [[String:AnyObject]]
        {
            allPaymentData += data
        }
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - UITableViewDelegate method
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return allPaymentData.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportPaymentOptionCell", for: indexPath)
        
        if let reportPaymentOptionCell = cell as? ReportPaymentOptionCell
        {
            let data = allPaymentData[indexPath.row]
            
            guard
                let id = data["id"] as? String,
                let name = data["pay_name"] as? String
                else { print("error on \(#function), \(#line) "); return reportPaymentOptionCell }
            
            reportPaymentOptionCell.id = id
            reportPaymentOptionCell.name.text = name
            reportPaymentOptionCell.accessoryType = selectedPaymentData.id == id ? .checkmark : .none
            
            return reportPaymentOptionCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let data = allPaymentData[indexPath.row]
        
        guard
            let id = data["id"] as? String,
            let name = data["pay_name"] as? String
            else { print("error on \(#function), \(#line) "); return }
        
        let selectedPayment = paymentData.init(name: name, id: id)
        delegate?.paymentOptionSelected(payment: selectedPayment)
        dismiss(animated: true, completion: nil)
    }

}
