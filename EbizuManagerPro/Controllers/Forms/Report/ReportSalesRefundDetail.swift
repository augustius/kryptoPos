//
//  ReportSalesRefundDetail.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 18/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class ReportSalesRefundDetailCell: UITableViewCell
{
    @IBOutlet var itemName: UILabel!
    @IBOutlet var modifier: UILabel!
    @IBOutlet var itemGst: UILabel!
    @IBOutlet var itemPrice: UILabel!
    @IBOutlet var itemDisc: UILabel!
    @IBOutlet var itemQty: UILabel!
    @IBOutlet var itemTotal: UILabel!
    
    @IBOutlet var itemRemainQty: UILabel!
    @IBOutlet var itemRemainTotal: UILabel!
}

class ReportSalesRefundDetail: UIViewController, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate, customKeyboardViewDelegate
{
    
    @IBOutlet var transactionID: UILabel!
    @IBOutlet var syncStatus: UIButton!
    @IBOutlet var customerName: UILabel!
    @IBOutlet var cashierName: UILabel!
    @IBOutlet var paymentMethod: UILabel!
    @IBOutlet var transactionDateTime: UILabel!
    @IBOutlet weak var taxDetailHeader: UILabel!

    @IBOutlet var total: UILabel!
    @IBOutlet var totalGst: UILabel!
    @IBOutlet var serviceCharge: UILabel!
    @IBOutlet var discountAmount: UILabel!
    @IBOutlet var voucherAmount: UILabel!
    @IBOutlet var roundingAdj: UILabel!
    @IBOutlet var refundAmount: UILabel!
    @IBOutlet var grandTotal: UILabel!
    
    @IBOutlet var refundButton: UIButton!
    @IBOutlet var pinTextField: UITextField!
    
    @IBOutlet var transactionDetailTable: UITableView!
    
//    @IBOutlet weak var subDetailTotalLabel: UILabel!
    @IBOutlet weak var subDetailTaxLabel: UILabel!


    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    var selectedReportSalesRefundData = reportSalesRefundData()
    var reportDetailData = CompleteOrder()

    //MARK: - View Cycle
    override func viewDidLoad()
    {
        transactionDetailTable.rowHeight = UITableView.automaticDimension
        transactionDetailTable.estimatedRowHeight = 99.0
        customKeyboard?.delegate = self
        setupData()
        
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func refundOrder()
    {
        pinTextField.becomeFirstResponder()
    }
    
    @IBAction func reprintOrder()
    {
        EbizuPrinter.sharedPrinter.completeOrder = reportDetailData
        EbizuPrinter.sharedPrinter.reprintSales()
    }
    
    @IBAction func cancel()
    {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITableViewDelegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return reportDetailData.orderItemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportSalesRefundDetailCell", for: indexPath)
        
        if let reportSalesRefundDetailCell = cell as? ReportSalesRefundDetailCell
        {
            if let item = reportDetailData.orderItemsArray[indexPath.row] as? OrderItem
            {
                var taxCode = item.gst_code != "" ? item.gst_code.description : "ZR"
                taxCode = taxCode == "SV" ? "" : taxCode

                reportSalesRefundDetailCell.itemName.text = item.itemName
                reportSalesRefundDetailCell.modifier.text = item.modifierLabel
                reportSalesRefundDetailCell.itemGst.text = taxCode
                reportSalesRefundDetailCell.itemPrice.text = String(format:"%.2f",item.itemPrice_n_uomQty_n_modifier_perItem.doubleValue)
                reportSalesRefundDetailCell.itemDisc.text = String(format:"%.2f",item.discount.doubleValue)
                
                if item.refund_item_qty > 0
                {
                    let remainQty = item.itemQuantity.subtracting(item.refund_item_qty).description
                    let remainPrice = String(format:"%.2f",item.totalPrice_bfr_bill_disc.subtracting(item.refund_item_price).doubleValue)
                    
                    var attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: item.itemQuantity.description)
                    attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                    reportSalesRefundDetailCell.itemQty.attributedText = attributeString
                    
                    attributeString = NSMutableAttributedString(string: String(format:"%.2f",item.totalPrice_bfr_bill_disc.doubleValue))
                    attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                    reportSalesRefundDetailCell.itemTotal.attributedText = attributeString
                    
                    reportSalesRefundDetailCell.itemRemainQty.text = remainQty
                    reportSalesRefundDetailCell.itemRemainTotal.text = remainPrice
                }
                else
                {
                    reportSalesRefundDetailCell.itemQty.text = item.itemQuantity.description
                    reportSalesRefundDetailCell.itemTotal.text = String(format:"%.2f",item.totalPrice_bfr_bill_disc.doubleValue)
                    reportSalesRefundDetailCell.itemRemainQty.text = ""
                    reportSalesRefundDetailCell.itemRemainTotal.text = ""
                }
            }
            return reportSalesRefundDetailCell
        }
        
        return cell
    }
    
    //MARK: - UITextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) 
    {
        switch textField
        {
        case pinTextField:
            customKeyboard?.textfield = textField
            customKeyboard?.customKeyboardType = .pin
        default:
            break
        }
    }
}
