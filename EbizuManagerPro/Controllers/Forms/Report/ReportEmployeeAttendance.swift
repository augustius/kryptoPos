//
//  ReportEmployeeAttendance.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 04/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class ReportEmployeeAttendanceCell: UITableViewCell
{
    @IBOutlet var name: UILabel!
    @IBOutlet var clockinTime: UILabel!
    @IBOutlet var clockoutTime: UILabel!
    @IBOutlet var duration: UILabel!
}

class ReportEmployeeAttendance: UIViewController,UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, ReportEmployeeOptionDelegate
{
    @IBOutlet var selectedStartDate: UITextField!
    @IBOutlet var selectedEndDate: UITextField!
    @IBOutlet var selectedEmployee: UIButton!
    @IBOutlet var attendanceTable: UITableView!
    @IBOutlet var numberOfCurrentActiveEmployee: UILabel!
    
    let customCalendarKeyboard = Bundle.main.loadNibNamed("customCalendarKeyboard", owner: self, options: nil)?.first as? customCalendarKeyboardView
    var acticeAttendanceData = [[String:AnyObject]]()
    var inacticeAttendanceData = [[String:AnyObject]]()
    var dateFormatter = DateFormatter()
    
    var currentEmployee = employeeData() {
        didSet{
            DispatchQueue.main.async {
                self.selectedEmployee.setTitle(self.currentEmployee.name, for: .normal)
            }
        }
    }
    
    var startDate = Date() {
        didSet{
            DispatchQueue.main.async {
                self.dateFormatter.dateFormat = "dd-MMM-yyyy"
                self.selectedStartDate.text = self.dateFormatter.string(from: self.startDate)
            }
        }
    }
    
    var endDate = Date() {
        didSet{
            DispatchQueue.main.async {
                self.dateFormatter.dateFormat = "dd-MMM-yyyy"
                self.selectedEndDate.text = self.dateFormatter.string(from: self.endDate)
            }
        }
    }
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        attendanceTable.rowHeight = UITableView.automaticDimension
        attendanceTable.estimatedRowHeight = 48.0
        sendFormToGoogleAnalytic()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        setDefaultDateRange()
    }
    
    //MARK: - UITableViewDelegate method
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return section == 0 ? "Current Active Employee" : "Attendance History"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return section == 0 ? acticeAttendanceData.count : inacticeAttendanceData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportEmployeeAttendanceCell", for: indexPath)
        
        if let reportEmployeeAttendanceCell = cell as? ReportEmployeeAttendanceCell
        {
            dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss"
            let data = indexPath.section == 0 ? acticeAttendanceData[indexPath.row] : inacticeAttendanceData[indexPath.row]

            guard
                let name = data["name"] as? String,
                let clockinTime = data["clock_in_time"] as? String,
                let clockinTimeinterval = TimeInterval(clockinTime),
                let clockoutTime = data["clock_out_time"] as? String,
                let clockoutTimeinterval = clockoutTime == "Active" ? TimeInterval(0) : TimeInterval(clockoutTime)
                else { print("error on \(#function), \(#line) "); return reportEmployeeAttendanceCell }
            
            reportEmployeeAttendanceCell.name.text = name
            reportEmployeeAttendanceCell.clockinTime.text = dateFormatter.string(from:Date.init(timeIntervalSince1970: clockinTimeinterval))
            reportEmployeeAttendanceCell.clockoutTime.text = clockoutTime == "Active" ? "Active" :dateFormatter.string(from:Date.init(timeIntervalSince1970: clockoutTimeinterval))
            reportEmployeeAttendanceCell.clockoutTime.textColor = clockoutTime == "Active" ? UIColor.init(color: .ebizuGreen) : UIColor.black
            
            if clockoutTime == "Active"
            {
                reportEmployeeAttendanceCell.duration.text = "-"
            }
            else
            {
                let componentFormatter = DateComponentsFormatter()
                componentFormatter.allowedUnits = [.hour, .minute]
                componentFormatter.unitsStyle = .abbreviated
                
                reportEmployeeAttendanceCell.duration.text = componentFormatter.string(from: Date.init(timeIntervalSince1970: clockinTimeinterval),
                                                                                       to: Date.init(timeIntervalSince1970: clockoutTimeinterval))
            }
        
            return reportEmployeeAttendanceCell
        }
        return cell
    }
    
    //MARK: - UITextFieldDelegate method
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        customCalendarKeyboard?.textfield = textField
        customCalendarKeyboard?.dateFormatter = dateFormatter
        if let safeDate = dateFormatter.date(from: textField.text!)
        {
            customCalendarKeyboard?.datePicker.setDate(safeDate, animated: true)
            customCalendarKeyboard?.datePicker.minimumDate = textField == selectedEndDate ? startDate : nil
            customCalendarKeyboard?.datePicker.maximumDate = textField == selectedEndDate ? Date().endOfToday() : endDate
            customCalendarKeyboard?.datePicker.datePickerMode = .date
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        guard
            let safeStartDate = dateFormatter.date(from: selectedStartDate.text!),
            let safeEndDate = dateFormatter.date(from: selectedEndDate.text!)
            else
        {
            setDefaultDateRange(); return
        }
        
        startDate = safeStartDate.startOfToday()
        endDate = safeEndDate.endOfToday()
        setupData()
    }

    

}
