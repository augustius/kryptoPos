//
//  ReportSalesRefundController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 13/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension ReportSalesRefund
{
    func setDefaultDateRange()
    {
//        if let data = shareddb.shared().getLastShiftRecord() as? [String:AnyObject]
//        {
//            guard
//                let id = data["id"] as? String,
//                let openTime = data["open_time_epoch"] as? String,
//                let openTimeinterval = TimeInterval(openTime),
//                let closeTime = data["close_time_epoch"] as? String,
//                let closeTimeinterval = closeTime == "0" ? NSDate().timeIntervalSince1970 : TimeInterval(closeTime)
//                else { print("error on \(#function), \(#line) "); return }
//
//            let openTimeDate = Date.init(timeIntervalSince1970: openTimeinterval)
//            let closeTimeDate = Date.init(timeIntervalSince1970: closeTimeinterval)
//
//            currentShift = shiftData(startDate: openTimeDate, endDate: closeTimeDate, id: id)
//        }

        startDate = Date().startOfToday()
        endDate = Date().endOfToday()
        reloadData()
    }
    
    func reloadData()
    {
        switch type
        {
        case .sales:
            if let data = shareddb.shared().getSalesReport(from: startDate, to: endDate, withPaymentId: currentPaymentMethod.id) as? [[String:AnyObject]]
            {
                reportData = data
            }
            break
        case .refund:
            if let data = shareddb.shared().getRefundReport(from: startDate, to: endDate, withPaymentId: currentPaymentMethod.id) as? [[String:AnyObject]]
            {
                reportData = data
            }
            break
        }
        
        let totalSalesRefund = reportData.compactMap({ Double(($0["total"] as? String) ?? "0.0")}).reduce(0, +)
        self.totalAmount.text = String(format: "%.2f", totalSalesRefund)
        
        reportTable.reloadData()
    }
    
    //MARK: - ReportShiftOptionDelegate method
    func shiftOptionSelected(shift: shiftData)
    {
        currentShift = shift
    }
    
    //MARK: - ReportPaymentOptionDelegate method
    func paymentOptionSelected(payment: paymentData)
    {
        currentPaymentMethod = payment
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showPaymentOption"
        {
            guard
                let button = sender as? UIButton,
                let dvc = segue.destination as? ReportPaymentOption,
                let possiblePopover = dvc.popoverPresentationController
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = button.bounds
            possiblePopover.sourceView = button
            dvc.delegate = self
            dvc.selectedPaymentData = currentPaymentMethod
        }
        else if segue.identifier == "showShiftOption"
        {
            guard
                let button = sender as? UIButton,
                let dvc = segue.destination as? ReportShiftOption,
                let possiblePopover = dvc.popoverPresentationController
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = button.bounds
            possiblePopover.sourceView = button
            dvc.delegate = self
            dvc.selectedShiftData = currentShift
        }
        else if segue.identifier == "showSalesDetail"
        {
            if let dvc = segue.destination as? ReportSalesRefundDetail
            {
                dvc.selectedReportSalesRefundData = selectedReportSalesRefundData
            }
        }
    }
    
    @IBAction func updateReport(segue: UIStoryboardSegue)
    {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}
