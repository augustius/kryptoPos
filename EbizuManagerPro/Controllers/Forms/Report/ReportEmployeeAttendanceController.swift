//
//  ReportEmployeeAttendanceController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 04/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension ReportEmployeeAttendance
{
    func setupData()
    {
        if let data = shareddb.shared().get_employee_clock_(in_out_record: startDate, to: endDate, user_id: currentEmployee.id) as? [[String:AnyObject]]
        {
            acticeAttendanceData = data.filter { ($0["clock_out_time"] as! String) == "Active" }
            inacticeAttendanceData = data.filter { ($0["clock_out_time"] as! String) != "Active" }

            attendanceTable.reloadData()
            DispatchQueue.main.async {
                self.numberOfCurrentActiveEmployee.text = self.acticeAttendanceData.count.description
            }
        }
    }
    
    func setDefaultDateRange()
    {
        startDate = Date().startOfToday()
        endDate = startDate.endOfToday()
        setupData()
    }
    
    //MARK: - ReportEmployeeDelegate method
    func employeeSelected(employee: employeeData)
    {
        currentEmployee = employee
        setupData()
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showEmployeeOption"
        {
            guard
                let button = sender as? UIButton,
                let dvc = segue.destination as? ReportEmployeeOption,
                let possiblePopover = dvc.popoverPresentationController
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = button.bounds
            possiblePopover.sourceView = button
            dvc.delegate = self
            dvc.selectedEmployeeData = currentEmployee
        }
    }
}
