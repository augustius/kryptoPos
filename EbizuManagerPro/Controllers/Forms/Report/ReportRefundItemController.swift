//
//  ReportRefundItemController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 25/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension ReportRefundItem
{
    func setupData()
    {
        refundTableView.rowHeight = UITableView.automaticDimension
        refundTableView.estimatedRowHeight = 88.0
        transactionId.text = refundHeaderData.transactionId
        taxLabel.text = (refundData.isSSTmode() || refundData.isNewSSTmode()) ? "Service Tax" : "GST"
        
        //readjust the data for refund
        if let itemArray = refundData.orderItemsArray as? [OrderItem]
        {
            var tempArray = [OrderItem]()
            for item in itemArray
            {
                item.remain_item_qty = item.itemQuantity.subtracting(item.refund_item_qty)
                item.itemQuantity = NSDecimalNumber.init(string: "0.00")
                
                tempArray.append(item)
            }
            refundData.orderItemsArray = tempArray
            setRefundChargeDetails()
        }
    }
    
    func adjustDataForCalculation()
    {
        //put back reference data for discount, voucher etc
        guard
            let itemReferenceArray = referenceData.orderItemsArray as? [OrderItem],
            let itemRefundArray = refundData.orderItemsArray as? [OrderItem]
            else { print("error on \(#function), \(#line) "); return}
        
        var tempArray = [OrderItem]()
        for (index,itemReference) in itemReferenceArray.enumerated()
        {
            let refundItem = itemRefundArray[index]
            itemReference.remain_item_qty = refundItem.remain_item_qty
            itemReference.itemQuantity = refundItem.itemQuantity
            
            tempArray.append(itemReference)
        }
        referenceData.orderItemsArray = tempArray
        refundData = referenceData
    }

    func presentSelectAllOption() {
        let alert = UIAlertController(title: "Refund", message: "Do you wish to refund all remain item ?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { (action) in
            self.autoSelectAll()
        })
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func autoSelectAll() {
        if let safeOrderItemArray = refundData.orderItemsArray as? [OrderItem] {
            for (index,existOrder) in safeOrderItemArray.enumerated() {
                let defaultQty = NSDecimalNumber.init(string:"0")
                let itemQty = existOrder.remain_item_qty ?? defaultQty

                replace(existOrder: existOrder, atIndex: index, withNewQty: itemQty)
            }
            isSelectRefundAll = true
            recalculate()
        }
    }

    func replace(existOrder: OrderItem, atIndex index: Int, withNewQty itemQty: NSDecimalNumber) {
        existOrder.remain_item_qty = existOrder.remain_item_qty.adding(existOrder.itemQuantity) // get total qty first
        existOrder.itemQuantity = (existOrder.remain_item_qty < itemQty) ? existOrder.remain_item_qty : itemQty // replace refund qty with new refund qty
        existOrder.remain_item_qty = existOrder.remain_item_qty.subtracting(existOrder.itemQuantity) // replace remain qty with new remain qty
        refundData.orderItemsArray[index] = existOrder
    }

    func recalculate() {
        adjustDataForCalculation()
        setRefundChargeDetails()
    }

    func setRefundChargeDetails()
    {
        DispatchQueue.main.async{

            if self.refundData.isNewSSTmode() {
                self.refundData.newCalculate()
            } else {
                self.refundData.calculate()
            }

            self.serviceCharge.text = String(format:"%.2f",self.refundData.tax_amount.doubleValue)
            self.discount.text = String(format:"%.2f",self.refundData.discount_amount.doubleValue)
            self.voucher.text = String(format:"%.2f",self.refundData.voucher_amount.doubleValue)
            self.gst.text = String(format:"%.2f",self.refundData.total_gst_value.doubleValue)
            self.roundingAmount.text = String(format:"%.2f",self.refundData.rounding_amount.doubleValue)
            self.pay.setTitle("REFUND " + String(format:"%.2f",self.refundData.total.doubleValue).addCurrency(), for: .normal)
            let enoughAmount = self.refundData.total.doubleValue > 0
            self.pay.isUserInteractionEnabled = enoughAmount
            self.pay.backgroundColor = enoughAmount ? UIColor.init(color: .ebizuGreen) : UIColor.gray
            
            self.refundTableView.reloadData()
        }
    }
    
    func enableUIRefundItem(notif:NSNotification)
    {
        notification.removeObserver(self, name: NSNotification.Name(DATABASE_STATUS), object: nil)
        
        guard
            let notifData = notif.object as? [AnyObject],
            let notifMessage = notifData.last as? String,
            let databaseStatus = notifData.first as? String
            else
        {
            hideLoadingScreen()
            present_sclalert("DATA CONVERSION ERROR", message: "Fail to convert to type safe", alert_style: .info)
            print("error on \(#function), \(#line) "); return
        }
        
        if databaseStatus == DATABASE_STATUS_SUCCESS
        {
            EbizuPrinter.sharedPrinter.receiptType = .refund
            EbizuPrinter.sharedPrinter.completeOrder = self.refundData
            EbizuPrinter.sharedPrinter.printRefund()

            if self.refundData.pay_type_name.uppercased() == CASH {
                shareddb.shared().update_current_balance(self.refundData.total.doubleValue, with_plus: false)
            }
            
            Networking.shared().putData(forWriting: .writingRefund)
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "refundComplete", sender: self)
            }
        }
        else
        {
            present_sclalert("DATABASE ERROR", message: notifMessage, alert_style: .error)
        }
        
        hideLoadingScreen()
    }
}
