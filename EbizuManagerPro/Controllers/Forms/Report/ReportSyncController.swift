//
//  ReportSyncController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 30/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct syncData {
    var time = ""
    var message = ""
    var messageColor = UIColor.black
}

struct syncResponse{
    var name = ""
    var message = ""
    var status = responseStatus.onProgress
}

extension ReportSync
{
    func setupData()
    {
        respondedResponseArr = [String]()
        readingResponseArr = Networking.shared().getAllReadingApiResponseName()
        writingResponseArr = Networking.shared().getAllWritingApiResponseName()
        
        // reset developer logs data
        arrSyncData = [syncData]()
            
        DispatchQueue.main.async {
            self.syncProgressBar.setProgress(0, animated: false)
        }
    }
    
    func setupMerchantSyncRotating()
    {
        // reset merchant logs data
        allMerchantSyncImage = [storeSyncStatus,itemSyncStatus,
                                employeeSyncStatus,customerSyncStatus,
                                transactionSyncStatus,settingSyncStatus]
        allExpectedMerchantResponse = [expectedStoreResponseArr,
                                       expectedItemResponseArr,
                                       expectedEmployeeResponseArr,
                                       expectedCustomerResponseArr,
                                       expectedTransactionResponseArr,
                                       expectedSettingResponseArr]
        allRespondedMerchantResponse = [[syncResponse]]()
        for _ in 1...allExpectedMerchantResponse.count
        {
            allRespondedMerchantResponse.append([])
        }
        
        for (index,_) in allMerchantSyncImage.enumerated()
        {
            allMerchantSyncImage[index].image = UIImage(named:"transaction_sync") //progress image
            allMerchantSyncImage[index].startRotating(duration: 2.0)
        }
    }
    
    func reloadSyncTable(withResponse str:String = "")
    {
        if str != ""
        {
            shownArrSyncData = arrSyncData.filter({$0.message.contains(str)})
        }
        else
        {
            shownArrSyncData = arrSyncData
        }
        syncTable.reloadData()
    }
    
    func syncResponseNotification(notif:NSNotification)
    {
        guard
            let responseDict = notif.object as? syncResponse
            else
        {
            hideLoadingScreen()
            notification.removeObserver(self, name: networkNotification.transactionSyncForm.notification, object: nil)
            present_sclalert("CONVERSION ERROR", message: "Fail to convert to type safe", alert_style: .info)
            print("error on \(#function), \(#line) "); return
        }
        
        DispatchQueue.main.sync
            {
                // collect sync data for developer mode log
                self.handleResponseDeveloperView(responseDict)
                // collect data for merchant view
                self.handleResponseMerchantView(responseDict)
                
                // handle progress circle data
                self.handleProgressCircle(responseDict)
            }
    }
    
    func handleResponseDeveloperView(_ responseDict:syncResponse)
    {
        var tempData = syncData()
        tempData.time = dateFormatter.string(from: Date())
        tempData.message = responseDict.status.rawValue + " on " + responseDict.name + " , message : " + responseDict.message
        
        switch responseDict.status
        {
        case .onProgress:
            tempData.messageColor = UIColor.init(color: .ebizuBlue)
        case .noDataToSend:
            tempData.messageColor = UIColor.lightGray
        case .Success:
            tempData.messageColor = UIColor.init(color: .ebizuGreen)
        case .fail:
            tempData.messageColor = UIColor.init(color: .ebizuRed)
        }
        arrSyncData.append(tempData)
        
        if developerMode
        {
            reloadSyncTable()
        }
    }
    
    func handleResponseMerchantView(_ responseDict:syncResponse)
    {
        for (index,expectedResponseArr) in allExpectedMerchantResponse.enumerated()
        {
            if expectedResponseArr.contains(responseDict.name)
            {
                if (allRespondedMerchantResponse[index].contains(where: {$0.name == responseDict.name}))
                {
                    return
                }
                
                switch responseDict.status
                {
                case .onProgress:
                    break
                case .noDataToSend,.Success:
                    allRespondedMerchantResponse[index].append(responseDict)
                    if allExpectedMerchantResponse[index].count == allRespondedMerchantResponse[index].count
                    {
                        allMerchantSyncImage[index].image = UIImage(named:"transaction_synced") //success image
                        allMerchantSyncImage[index].stopRotating()
                        allExpectedMerchantResponse[index] = [String]()
                    }
                case .fail:
                    allMerchantSyncImage[index].image = UIImage(named:"transaction_synced_fail") //fail image
                    allMerchantSyncImage[index].stopRotating()
                    allExpectedMerchantResponse[index] = [String]()
                }
                return
            }
        }
    }
    
    func handleProgressCircle(_ responseDict:syncResponse)
    {
        switch responseDict.status
        {
        case .onProgress:
            break
        case .noDataToSend,.Success,.fail:
            if !respondedResponseArr.contains(responseDict.name)
            {
                respondedResponseArr.append(responseDict.name)
            }
        }
        
        let allResponseArr = writingResponseArr + readingResponseArr
        let addingProgress = (CGFloat(respondedResponseArr.count) / CGFloat(allResponseArr.count))
        if !executeReadingApi
        {
            print(String(format:"responded : %d",respondedResponseArr.count))
            print(String(format:"writing : %d",writingResponseArr.count))
            executeReadingApi = (respondedResponseArr.count == writingResponseArr.count)
        }
        else
        {
            if respondedResponseArr.count == allResponseArr.count
            {
                while shareddb.shared().operation_still_running()
                {
                    print("still waiting");
                }
                print("allRespondedMerchantResponse :\(allRespondedMerchantResponse)")
                print("allExpectedMerchantResponse :\(allExpectedMerchantResponse)")

                hideLoadingScreen()
                notification.removeObserver(self, name: networkNotification.transactionSyncForm.notification, object: nil)
            }
        }
        
        // update ui
        syncProgressBar.setProgress(addingProgress, animated: true)
    }
    
    func handleSendLocalDB()
    {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let documentDirectory = paths.first
        {
            notification.addObserver(self, selector: #selector(sendDbSuccess(notif:)), name: networkNotification.sendLocalDbSuccess.notification, object: nil)
            notification.addObserver(self, selector: #selector(sendDbFail(notif:)), name: networkNotification.sendLocalDbFail.notification, object: nil)
            
            let fileName = documentDirectory + "/posapp_db.sqlite"
            if let fileData = NSData.init(contentsOfFile: fileName)
            {
                let data:[String:String] =
                    [
                        "filename" : StringUtils.get_business_detail("business_id") + "-" + shareddb.shared().get_unique_syncid() + ".sqlite",
                        "company_name" : StringUtils.get_business_detail("business_name"),
                        "file" : fileData.base64EncodedString(options: .lineLength64Characters)
                    ]
//                let data: Data = NSKeyedArchiver.archivedData(withRootObject: dict)
                showLoadingScreen()

//                UploadAPI.uploadAttachment(data, fileName: fileName, progessHandler: { (progress) in
//                    print("here august: \(progress)")
//                }) { (result) in
//                    self.hideLoadingScreen()
//                    switch result {
//                    case .success:
//                        self.present_sclalert("Database File Status", message: "Database has been sent successfully.", alert_style: .success)
//                    case .failure:
//                        self.present_sclalert("Database File Status", message: "Database failed to sent, please try again later.", alert_style: .notice)
//                    }
//                }

                Networking.shared().putLocalDb(localDbData: data)
            }
            else
            {
                present_sclalert("Error", message: "Fail to sent database", alert_style: .error)
            }
        }
        else
        {
            present_sclalert("Error", message: "Can't find database path", alert_style: .error)
        }
    }
    
    @objc func sendDbSuccess(notif:NSNotification)
    {
        notification.removeObserver(self, name: networkNotification.sendLocalDbSuccess.notification, object: nil)
        notification.removeObserver(self, name: networkNotification.sendLocalDbFail.notification, object: nil)
        hideLoadingScreen()
        
        present_sclalert("Database File Status", message: "Database has been sent successfully.", alert_style: .success)
    }
    
    @objc func sendDbFail(notif:NSNotification)
    {
        notification.removeObserver(self, name: networkNotification.sendLocalDbSuccess.notification, object: nil)
        notification.removeObserver(self, name: networkNotification.sendLocalDbFail.notification, object: nil)
        hideLoadingScreen()

        present_sclalert("Database File Status", message: "Database failed to sent, please try again later.", alert_style: .notice)
    }
    
}

extension UIView
{
    func startRotating(duration: Double = 1)
    {
        let kAnimationKey = "rotation"
        
        if self.layer.animation(forKey: kAnimationKey) == nil
        {
            let animate = CABasicAnimation(keyPath: "transform.rotation")
            animate.duration = duration
            animate.repeatCount = Float.infinity
            animate.fromValue = 0.0
            animate.toValue = Float(.pi * 2.0)
            self.layer.add(animate, forKey: kAnimationKey)
        }
    }
    func stopRotating() {
        let kAnimationKey = "rotation"
        
        if self.layer.animation(forKey: kAnimationKey) != nil
        {
            self.layer.removeAnimation(forKey: kAnimationKey)
        }
    }
}
















