//
//  ReportRefundItemCell.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 24/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class ReportRefundItemCell:UITableViewCell
{
    @IBOutlet var selectAllButton: UIButton!
    @IBOutlet var itemName: UILabel!
    @IBOutlet var remainingQty: UITextField!
    @IBOutlet var minus: UIButton!
    @IBOutlet var refundQty: UITextField!
    @IBOutlet var plus: UIButton!
    @IBOutlet var totalRefund: UILabel!
    @IBOutlet var itemRemarks: UILabel!
    @IBOutlet var modifier: UILabel!
    @IBOutlet var itemDiscount: UILabel!
    @IBOutlet var itemGst: UILabel!
    
    @IBAction func plusMinusPressed(_ sender: UIButton)
    {
        guard
            var safeRemainQty = Int(remainingQty.text!),
            var safeRefundQty = Int(refundQty.text!),
            let buttonLabel = sender.titleLabel?.text
            else { print("error on \(#function), \(#line) "); return}
        
        
        switch buttonLabel
        {
        case "+":
            if safeRemainQty > 0
            {
                safeRemainQty -= 1
                safeRefundQty += 1
                
                remainingQty.text = "\(safeRemainQty)"
                refundQty.text = "\(safeRefundQty)"
            }
            break
        case "-":
            if safeRefundQty > 0
            {
                safeRemainQty += 1
                safeRefundQty -= 1
                
                remainingQty.text = "\(safeRemainQty)"
                refundQty.text = "\(safeRefundQty)"
            }
            break
        default:
            break
        }
    }

    @IBAction func didTapSelectAll(_ sender: UIButton) {
        guard
            var safeRemainQty = Int(remainingQty.text!),
            var safeRefundQty = Int(refundQty.text!)
            else { print("error on \(#function), \(#line) "); return}

        if sender.backgroundImage(for: .normal) == nil {
            sender.setBackgroundImage(UIImage(named: "tick_hold_order"), for: .normal)
            safeRefundQty += safeRemainQty
            safeRemainQty = 0
        } else {
            sender.setBackgroundImage(nil, for: .normal)
            safeRemainQty += safeRefundQty
            safeRefundQty = 0
        }

        remainingQty.text = "\(safeRemainQty)"
        refundQty.text = "\(safeRefundQty)"
    }

    func setSelectAllButton() {
        selectAllButton.setBackgroundImage(UIImage(named: "tick_hold_order"), for: .normal)
    }
}
