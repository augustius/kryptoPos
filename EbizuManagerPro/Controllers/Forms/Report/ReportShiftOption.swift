//
//  ReportShiftOption.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 13/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct shiftData
{
    var startDate = Date()
    var endDate = Date()
    var id = ""
}

class ReportShiftOptionCell: UITableViewCell
{
    @IBOutlet var name: UILabel!
    var id = ""
}

protocol ReportShiftOptionDelegate: class {
    func shiftOptionSelected(shift:shiftData)
}

class ReportShiftOption: UITableViewController
{
    weak var delegate: ReportShiftOptionDelegate?
    var selectedShiftData = shiftData()
    var allShiftData = [[String:AnyObject]]()
    var dateFormatter = DateFormatter()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        if let data = shareddb.shared().getAllShiftRecord() as? [[String:AnyObject]]
        {
            // re-adjust data structure to display as section as desired
            let allMonthYear = [String](Set(data.map{($0["month_year"] as? String) ?? "" })).sorted(by: >)
            for monthYear in allMonthYear
            {
                let tempArrDict = data.filter { (($0["month_year"] as? String) ?? "") == monthYear }
                guard
                    let firstDict = tempArrDict.first,
                    let month = firstDict["month"] as? String,
                    let year = firstDict["year"] as? String
                    else { print("error on \(#function), \(#line) "); return}
                
                var tempDict = [String:AnyObject]()
                tempDict["monthYear"] = (month + " " + year) as AnyObject
                tempDict["shiftData"] = tempArrDict as AnyObject
                
                allShiftData.append(tempDict)
            }
        }
        
        dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss"
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - UITableViewDelegate method
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return allShiftData.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        let data = allShiftData[section]
        guard
            let storedMonthYear = data["monthYear"] as? String
            else { return "" }
        
        return storedMonthYear
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let data = allShiftData[section]
        guard
            let storedShiftData = data["shiftData"] as? [[String:AnyObject]]
            else { return 0 }
        
        return storedShiftData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportShiftOptionCell", for: indexPath)
        
        if let reportShiftOptionCell = cell as? ReportShiftOptionCell
        {
            let data = allShiftData[indexPath.section]
        
            if let storedShiftDatas = data["shiftData"] as? [[String:AnyObject]]
            {
                let storeData = storedShiftDatas[indexPath.row]
            
                guard
                    let id = storeData["id"] as? String,
                    let openTime = storeData["open_time_epoch"] as? String,
                    let openTimeinterval = TimeInterval(openTime),
                    let closeTime = storeData["close_time_epoch"] as? String,
                    let closeTimeinterval = closeTime == "0" ? NSDate().timeIntervalSince1970 : TimeInterval(closeTime)
                    else { print("error on \(#function), \(#line) "); return reportShiftOptionCell }
                
                let openTimeStr = dateFormatter.string(from:Date.init(timeIntervalSince1970: openTimeinterval))
                let closeTimeStr = closeTime == "0" ? "now" : dateFormatter.string(from:Date.init(timeIntervalSince1970: closeTimeinterval))
                
                reportShiftOptionCell.id = id
                reportShiftOptionCell.name.text = openTimeStr + " - " + closeTimeStr
                reportShiftOptionCell.accessoryType = selectedShiftData.id == id ? .checkmark : .none
            }
            return reportShiftOptionCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let data = allShiftData[indexPath.section]
        
        if let storedShiftDatas = data["shiftData"] as? [[String:AnyObject]]
        {
            let storeData = storedShiftDatas[indexPath.row]
        
            guard
                let id = storeData["id"] as? String,
                let openTime = storeData["open_time_epoch"] as? String,
                let openTimeinterval = TimeInterval(openTime),
                let closeTime = storeData["close_time_epoch"] as? String,
                let closeTimeinterval = closeTime == "0" ? NSDate().timeIntervalSince1970 : TimeInterval(closeTime)
                else { print("error on \(#function), \(#line) "); return }
            
            let openTimeDate = Date.init(timeIntervalSince1970: openTimeinterval)
            let closeTimeDate = Date.init(timeIntervalSince1970: closeTimeinterval)
            
            let selectedShift = shiftData.init(startDate: openTimeDate, endDate: closeTimeDate, id: id)
            delegate?.shiftOptionSelected(shift: selectedShift)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
}
