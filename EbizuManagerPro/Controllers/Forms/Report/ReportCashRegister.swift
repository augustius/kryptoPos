//
//  ReportCashRegister.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 04/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class ReportCashRegisterCell: UITableViewCell
{
    @IBOutlet var createdDate: UILabel!
    @IBOutlet var amount: UILabel!
    @IBOutlet var type: UILabel!
    @IBOutlet var createdBy: UILabel!
    @IBOutlet var remarks: UILabel!
}

class ReportCashRegister: UIViewController,UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate
{
    @IBOutlet var selectedStartDate: UITextField!
    @IBOutlet var selectedEndDate: UITextField!
    @IBOutlet var cashRegisterTable: UITableView!
    var cashRegisterData = [[String:AnyObject]]()
    
    let customCalendarKeyboard = Bundle.main.loadNibNamed("customCalendarKeyboard", owner: self, options: nil)?.first as? customCalendarKeyboardView
    var dateFormatter = DateFormatter()
    var startDate = Date() {
        didSet{
            DispatchQueue.main.async {
                self.selectedStartDate.text = self.dateFormatter.string(from: self.startDate)
            }
        }
    }
    
    var endDate = Date() {
        didSet{
            DispatchQueue.main.async {
                self.selectedEndDate.text = self.dateFormatter.string(from: self.endDate)
            }
        }
    }
    
    //MARK: - View Cylce
    override func viewDidLoad()
    {
        cashRegisterTable.rowHeight = UITableView.automaticDimension
        cashRegisterTable.estimatedRowHeight = 47.0
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        sendFormToGoogleAnalytic()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        jumpToToday()
    }
    
    //MARK: IBAction method
    @IBAction func jumpToToday()
    {
        startDate = Date().startOfToday()
        endDate = startDate.endOfToday()
        setupData()
    }
    
    func setupData()
    {
        if let data = shareddb.shared().get_cash_register_history_(from: startDate, to: endDate) as? [[String:AnyObject]]
        {
            cashRegisterData = data
            cashRegisterTable.reloadData()
        }
    }
    
    //MARK: - UITableViewDelegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return cashRegisterData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportCashRegisterCell", for: indexPath)
        
        if let reportCashRegisterCell = cell as? ReportCashRegisterCell
        {
            let data = cashRegisterData[indexPath.row]
            
            guard
                let createdDate = data["createtime"] as? String,
                let amountStr = data["amount"] as? String,
                let amount = Double(amountStr),
                let type = data["cash_type_name"] as? String,
                let createdBy = data["userright_name"] as? String,
                let remarks = data["notes"] as? String
                else { print("error on \(#function), \(#line) "); return reportCashRegisterCell}
            
            reportCashRegisterCell.createdDate.text = createdDate
            reportCashRegisterCell.amount.text = String(format: "%.2f", amount)
            reportCashRegisterCell.type.text = type
            reportCashRegisterCell.createdBy.text = createdBy
            reportCashRegisterCell.remarks.text = remarks
            
            return reportCashRegisterCell
        }
        
        return cell
    }
    
    //MARK: - UITextfieldDelegate method
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        customCalendarKeyboard?.textfield = textField
        customCalendarKeyboard?.dateFormatter = dateFormatter
        if let safeDate = dateFormatter.date(from: textField.text!)
        {
            customCalendarKeyboard?.datePicker.setDate(safeDate, animated: true)
            customCalendarKeyboard?.datePicker.minimumDate = textField == selectedEndDate ? startDate : nil
            customCalendarKeyboard?.datePicker.maximumDate = textField == selectedEndDate ? Date().endOfToday() : endDate
            customCalendarKeyboard?.datePicker.datePickerMode = .date
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        guard
            let safeStartDate = dateFormatter.date(from: selectedStartDate.text!),
            let safeEndDate = dateFormatter.date(from: selectedEndDate.text!)
            else
        {
            jumpToToday(); return
        }
        
        startDate = safeStartDate.startOfToday()
        endDate = safeEndDate.endOfToday()
        setupData()
    }
    
}
