//
//  ReportDailySales.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 03/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation
import FSCalendar

class ReportDailySales: UIViewController, FSCalendarDataSource, FSCalendarDelegate, UITextFieldDelegate
{
    @IBOutlet var selectedStartDate: UITextField!
    @IBOutlet var selectedEndDate: UITextField!
    @IBOutlet var datePicker: FSCalendar!
    
    let customCalendarKeyboard = Bundle.main.loadNibNamed("customCalendarKeyboard", owner: self, options: nil)?.first as? customCalendarKeyboardView
    var dateFormatter = DateFormatter()
    var startDate = Date() {
        didSet{
            DispatchQueue.main.async {
                self.selectedStartDate.text = self.dateFormatter.string(from: self.startDate)
            }
        }
    }
    
    var endDate = Date() {
        didSet{
            DispatchQueue.main.async {
                self.selectedEndDate.text = self.dateFormatter.string(from: self.endDate)
            }
        }
    }
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        dateFormatter.dateFormat = "dd-MMM-yyyy  hh:mm a"
        sendFormToGoogleAnalytic()        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        jumpToToday()
    }
    
    //MARK: IBAction method
    @IBAction func jumpToToday()
    {
        datePicker.select(Date(), scrollToDate: true)
        startDate = Date().startOfToday()
        endDate = startDate.endOfToday()
    }
    
    @IBAction func printDailySalesReport()
    {
        EbizuPrinter.sharedPrinter.printDailySalesReport(startDate: startDate, endDate: endDate)
    }
    
    //MARK: - FSCalendarDelegate method
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition)
    {
        startDate = date
        endDate = date.endOfToday()
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date
    {
        return Date()
    }
    
    //MARK: - UITextfieldDelegate method
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        customCalendarKeyboard?.textfield = textField
        customCalendarKeyboard?.dateFormatter = dateFormatter
        if let safeDate = dateFormatter.date(from: textField.text!)
        {
            customCalendarKeyboard?.datePicker.setDate(safeDate, animated: true)
            customCalendarKeyboard?.datePicker.minimumDate = textField == selectedEndDate ? startDate : nil
            customCalendarKeyboard?.datePicker.maximumDate = textField == selectedEndDate ? Date().endOfToday() : endDate
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        guard
            let safeStartDate = dateFormatter.date(from: selectedStartDate.text!),
            let safeEndDate = dateFormatter.date(from: selectedEndDate.text!)
            else
        {
            jumpToToday(); return
        }
        
        startDate = safeStartDate
        endDate = safeEndDate
    }
}
