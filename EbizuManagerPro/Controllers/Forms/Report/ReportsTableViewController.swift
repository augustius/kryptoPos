//
//  ReportsTableViewController.swift
//  EbizuManagerPro
//
//  Created by Daliso Ngoma on 17/02/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import UIKit

class ReportsTableViewController: UITableViewController {
    
    // TODO: Make this into a protocol
    @IBAction func menuButtonTouchUp(_ sender: UIBarButtonItem)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        // NOTE: Add any additional identifiers as new cases
        print(segue.identifier ?? "No identifier")
        switch segue.identifier ?? ""
        {
        case "showSales" :
            let salesRefundReport = (segue.destination as? UINavigationController)?.viewControllers.first as? ReportSalesRefund
            salesRefundReport?.type = .sales
        case "showRefund" :
            let salesRefundReport = (segue.destination as? UINavigationController)?.viewControllers.first as? ReportSalesRefund
            salesRefundReport?.type = .refund
        default: break
        }
    }
    
}
