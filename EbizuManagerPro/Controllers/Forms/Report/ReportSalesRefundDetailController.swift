//
//  ReportSalesRefundDetailController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 18/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension ReportSalesRefundDetail
{
    func setupData()
    {
        reportDetailData = shareddb.shared().get_transaction_detail_(by_transaction_number: selectedReportSalesRefundData.pohSn)
        if reportDetailData.isNewSSTmode() {
            reportDetailData.newCalculate()
        } else {
            reportDetailData.calculate()
        }

        if let arrayItems = reportDetailData.orderItemsArray as? [OrderItem]
        {
            let totalRefundQty = arrayItems.compactMap( {$0.refund_item_qty.intValue} ).reduce(0, +)
            let totalQty = arrayItems.compactMap( {$0.itemQuantity.intValue} ).reduce(0, +)
            
            refundButton.isUserInteractionEnabled = totalRefundQty < totalQty
            refundButton.backgroundColor = refundButton.isUserInteractionEnabled ? UIColor.init(color: .ebizuGreen) : UIColor.lightGray
        }

        let currentTaxLabel = (self.reportDetailData.isSSTmode() || self.reportDetailData.isNewSSTmode()) ? "   " : "GST"
        
        DispatchQueue.main.async{
            
            self.transactionID.text = self.selectedReportSalesRefundData.transactionId
            self.customerName.text = self.reportDetailData.customer_name
            self.cashierName.text = self.selectedReportSalesRefundData.createdBy
            self.paymentMethod.text = self.selectedReportSalesRefundData.payment
            self.transactionDateTime.text = self.selectedReportSalesRefundData.createdDate
            self.taxDetailHeader.text = currentTaxLabel

            self.total.text = String(format:"%.2f",self.reportDetailData.subtotal_bfr_bill_disc.doubleValue + self.reportDetailData.total_gst_value_bfr_bill_disc.doubleValue)
            self.totalGst.text = String(format:"%.2f",self.reportDetailData.total_gst_value_bfr_bill_disc.doubleValue)
            self.serviceCharge.text = String(format:"%.2f",self.reportDetailData.tax_amount.doubleValue)
            self.discountAmount.text = String(format:"-%.2f",self.reportDetailData.discount_amount.doubleValue)
            self.voucherAmount.text = String(format:"-%.2f",self.reportDetailData.voucher_amount.doubleValue)
            self.roundingAdj.text = String(format:"%.2f",self.reportDetailData.rounding_amount.doubleValue)
            self.refundAmount.text = String(format:"-%.2f",self.reportDetailData.refund_total_amount.doubleValue)
            self.grandTotal.text = String(format:"%.2f",self.reportDetailData.total.subtracting(self.reportDetailData.refund_total_amount).doubleValue)

//            self.subDetailTotalLabel.text = String(format:"Total (Incl.%@)", currentTaxLabel)
            self.subDetailTaxLabel.text = (self.reportDetailData.isSSTmode() || self.reportDetailData.isNewSSTmode()) ? "Service Tax (6%)" : "GST (6%)"
        }
    }
    
    //MARK: - CustomKeyboardDelegate method
    func pinStatus(success: Bool, textField: UITextField) -> Bool
    {
        if success
        {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "showRefund", sender: self)
            }
        }
        else
        {
            present_sclalert("Error", message: "Invalid PIN Entered, please try again", alert_style: .error)
        }
        
        return true
    }
    
    //MARK: - Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showRefund"
        {
            if let dvc = segue.destination as? ReportRefundItem
            {
                dvc.refundHeaderData = selectedReportSalesRefundData
                dvc.refundData = reportDetailData.copy() as! CompleteOrder
            }
        }
    }

}
