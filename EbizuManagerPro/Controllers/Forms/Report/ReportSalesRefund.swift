//
//  ReportSalesRefund.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 13/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct reportSalesRefundData
{
    var pohSn = ""
    var transactionId = ""
    var createdBy = ""
    var createdDate = ""
    var payment = ""
    var amount = ""
    var synced = false
}

enum reportType {
    case sales, refund
}

class ReportSalesRefundCell: UITableViewCell
{
    @IBOutlet private var syncStatus: UIButton!
    @IBOutlet private var transactionId: UILabel!
    @IBOutlet private var createdBy: UILabel!
    @IBOutlet private var createdDate: UILabel!
    @IBOutlet private var payment: UILabel!
    @IBOutlet private var amount: UILabel!
    
    var data = reportSalesRefundData() {
        didSet{
            syncStatus.backgroundColor = data.synced ? UIColor.init(color: .ebizuGreen) : UIColor.init(color: .ebizuYellow)
            transactionId.text = data.transactionId
            createdBy.text = data.createdBy
            createdDate.text = data.createdDate
            payment.text = data.payment
            amount.text = data.amount
        }
    }
}

class ReportSalesRefund: UIViewController,UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate,ReportShiftOptionDelegate,ReportPaymentOptionDelegate
{
    @IBOutlet var totalAmount: UILabel!
    @IBOutlet var selectedStartDate: UITextField!
    @IBOutlet var selectedEndDate: UITextField!
    @IBOutlet var selectedPaymentMethod: UIButton!
    @IBOutlet var reportTable: UITableView!
    
    let customCalendarKeyboard = Bundle.main.loadNibNamed("customCalendarKeyboard", owner: self, options: nil)?.first as? customCalendarKeyboardView
    let hardwareCode = userDefaults.string(forKey: HARDWARE_CODE) ?? ""
    var type:reportType = .sales
    var reportData = [[String:AnyObject]]()
    var dateFormatter = DateFormatter()
    var selectedReportSalesRefundData = reportSalesRefundData()
    
    var currentShift = shiftData() {
        didSet{
            DispatchQueue.main.async {
                self.startDate = self.currentShift.startDate
                self.endDate = self.currentShift.endDate
                self.reloadData()
            }
        }
    }
    
    var startDate = Date() {
        didSet{
            DispatchQueue.main.async {
                self.selectedStartDate.text = self.dateFormatter.string(from: self.startDate)
            }
        }
    }
    
    var endDate = Date() {
        didSet{
            DispatchQueue.main.async {
                self.selectedEndDate.text = self.dateFormatter.string(from: self.endDate)
            }
        }
    }
    
    var currentPaymentMethod = paymentData() {
        didSet{
            DispatchQueue.main.async {
                self.selectedPaymentMethod.setTitle(self.currentPaymentMethod.name, for: .normal)
                self.reloadData()
            }
        }
    }
    
    //MARK: - View Cylce
    override func viewDidLoad()
    {
        dateFormatter.dateFormat = "dd-MMM-yyyy  hh:mm a"
        setDefaultDateRange()
        sendFormToGoogleAnalytic()
        
        switch type
        {
        case .sales:
            title = "Sales"
        case.refund:
            title = "Refund"
        }
    }
    
    //MARK: - IBActin method
    @IBAction func jumpToTodayPressed()
    {
        startDate = Date().startOfToday()
        endDate = Date().endOfToday()
        reloadData()
    }
    
    @IBAction func printCurrentReport()
    {
        EbizuPrinter.sharedPrinter.printDailySalesReport(startDate: startDate, endDate: endDate)
    }
    
    
    //MARK: - UITableView Delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return reportData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportSalesRefundCell", for: indexPath)
        
        if let reportSalesRefundCell = cell as? ReportSalesRefundCell
        {
            let data = reportData[indexPath.row]
            guard
                let pohSn = data["poh_sn"] as? String,
                let transNo = data["trans_no"] as? String,
                let totalStr = data["total"] as? String,
                let total = Double(totalStr),
                let paymentMethod = data["payment_method"] as? String,
                let createdDate = data["created_datetime"] as? String,
                let createdBy = data["created_by"] as? String,
                let syncStatus = data["sync_status"] as? String
                else { print("error on \(#function), \(#line) "); return reportSalesRefundCell}
            
            reportSalesRefundCell.data = reportSalesRefundData(pohSn: pohSn,
                                                               transactionId: hardwareCode + "-" + transNo,
                                                               createdBy: createdBy,
                                                               createdDate: createdDate,
                                                               payment: paymentMethod,
                                                               amount: String(format: "%.2f", total),
                                                               synced: syncStatus == "synced")
            return reportSalesRefundCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let currentCell = tableView.cellForRow(at: indexPath) as? ReportSalesRefundCell
        {
            selectedReportSalesRefundData = currentCell.data
            if selectedReportSalesRefundData.pohSn == ""
            {
                present_sclalert("Info", message: "Unable to get original sales information from local storage. \nTo view more details please go to POS BackOffice.", alert_style: .info)
            }
            else
            {
                performSegue(withIdentifier: "showSalesDetail", sender: self)
            }
        }
    }
    
    //MARK: - UITextFieldDelegate method
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        customCalendarKeyboard?.textfield = textField
        customCalendarKeyboard?.dateFormatter = dateFormatter
        if let safeDate = dateFormatter.date(from: textField.text!)
        {
            customCalendarKeyboard?.datePicker.setDate(safeDate, animated: true)
            customCalendarKeyboard?.datePicker.minimumDate = textField == selectedEndDate ? startDate : nil
            customCalendarKeyboard?.datePicker.maximumDate = textField == selectedEndDate ? Date().endOfToday() : endDate
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        guard
            let safeStartDate = dateFormatter.date(from: selectedStartDate.text!),
            let safeEndDate = dateFormatter.date(from: selectedEndDate.text!)
            else
        {
            reloadData(); return
        }
        
        startDate = safeStartDate
        endDate = safeEndDate
        reloadData()
    }
}

