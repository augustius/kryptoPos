//
//  ReportEmployeeOption.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 05/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct employeeData
{
    var name = "All Employee"
    var id = ""
}

class ReportEmployeeOptionCell: UITableViewCell
{
    @IBOutlet var name: UILabel!
    var id = ""
}


protocol ReportEmployeeOptionDelegate: class {
    func employeeSelected(employee:employeeData)
}

class ReportEmployeeOption: UITableViewController
{
    weak var delegate: ReportEmployeeOptionDelegate?
    var selectedEmployeeData = employeeData()
    var allEmployeeData = [[String:AnyObject]]()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        //additional data to show all
        var dict = [String:AnyObject]()
        dict["user_id"] = "" as AnyObject
        dict["name"] = "All Employee" as AnyObject
        allEmployeeData.append(dict)
        
        if let data = shareddb.shared().get_all_employee() as? [[String:AnyObject]]
        {
            allEmployeeData += data
        }
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - UITableViewDelegate method
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return allEmployeeData.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportEmployeeOptionCell", for: indexPath)
        
        if let reportEmployeeOptionCell = cell as? ReportEmployeeOptionCell
        {
            let data = allEmployeeData[indexPath.row]
            
            guard
                let userId = data["user_id"] as? String,
                let name = data["name"] as? String
                else { print("error on \(#function), \(#line) "); return reportEmployeeOptionCell }
            
            reportEmployeeOptionCell.id = userId
            reportEmployeeOptionCell.name.text = name
            reportEmployeeOptionCell.accessoryType = selectedEmployeeData.id == userId ? .checkmark : .none
            
            return reportEmployeeOptionCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let data = allEmployeeData[indexPath.row]
        
        guard
            let userId = data["user_id"] as? String,
            let name = data["name"] as? String
            else { print("error on \(#function), \(#line) "); return }
        
        let selectedEmployee = employeeData.init(name: name, id: userId)
        delegate?.employeeSelected(employee: selectedEmployee)
        dismiss(animated: true, completion: nil)
    }
}
