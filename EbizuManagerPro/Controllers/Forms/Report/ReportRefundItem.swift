//
//  ReportRefundItem.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 24/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class ReportRefundItem: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet var transactionId: UILabel!
    @IBOutlet var syncStatus: UIButton!
    @IBOutlet var refundTableView: UITableView!
    @IBOutlet var serviceCharge: UILabel!
    @IBOutlet var discount: UILabel!
    @IBOutlet var voucher: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet var gst: UILabel!
    @IBOutlet var roundingAmount: UILabel!
    @IBOutlet var pay: UIButton!
    
    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    var refundHeaderData = reportSalesRefundData()
    internal var referenceData = CompleteOrder()
    var refundData = CompleteOrder()
    {
        didSet{
            self.referenceData = refundData.copy() as! CompleteOrder
        }
    }
    var isSelectRefundAll = false
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        setupData()
        sendFormToGoogleAnalytic()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presentSelectAllOption()
    }

    //MARK: - IBAction method
    @IBAction func refundPressed()
    {
        shareddb.shared().refund_transaction(refundData)
        showLoadingScreen()
        notification.addObserver(self, selector: #selector(enableUIRefundItem), name: NSNotification.Name(DATABASE_STATUS), object: nil)
    }
    
    @IBAction func cancelPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return refundData.orderItemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportRefundItemCell", for: indexPath)
        
        if let reportRefundItemCell = cell as? ReportRefundItemCell
        {
            if let itemArray = refundData.orderItemsArray as? [OrderItem]
            {
                let item = itemArray[indexPath.row]
                
                reportRefundItemCell.itemName.text = item.itemName.capitalized
                if item.type_of_price == "Unit Price"
                {
                    reportRefundItemCell.itemName.text = String(format:"%@ @ %.2f%@", item.itemName.capitalized, item.uom_qty.doubleValue, item.type_of_unit)
                }
                
                reportRefundItemCell.remainingQty.text = String(format:"%d",item.remain_item_qty.intValue)

                reportRefundItemCell.refundQty.delegate = self
                reportRefundItemCell.refundQty.tag = indexPath.row
                reportRefundItemCell.refundQty.text = String(format:"%d",item.itemQuantity.intValue)
                customKeyboard?.textfield = reportRefundItemCell.refundQty
                customKeyboard?.customKeyboardType = .number

                reportRefundItemCell.totalRefund.text = String(format: "%.2f", item.totalPrice_bfr_bill_disc.doubleValue)
                reportRefundItemCell.itemRemarks.text = item.remarks
                reportRefundItemCell.modifier.text = item.modifierLabel

                reportRefundItemCell.itemDiscount.text = ""
                if item.discount.doubleValue > 0.0 && item.is_edited_item_price != "price_edited"
                {
                    reportRefundItemCell.itemDiscount.text = "Discount @ " + String(format:"%.2f",item.discount.multiplying(by: item.itemQuantity).doubleValue).addCurrency()
                }

                reportRefundItemCell.itemGst.text = ""

                if item.gst_rate.doubleValue > 0.0 && !refundData.isNewSSTmode() {
                    reportRefundItemCell.itemGst.text = String(format:"%@ %@ %@%% @ ", refundData.isSSTmode() ? "SST" : "GST", item.gst_code == "SV" ? "" : item.gst_code, item.gst_rate) + String(format : "%.2f",item.gst_value_bfr_bill_disc.doubleValue).addCurrency()

                }

                reportRefundItemCell.plus.addTarget(self, action: #selector(changeOrderQty(sender:)), for: .touchUpInside)
                reportRefundItemCell.minus.addTarget(self, action: #selector(changeOrderQty(sender:)), for: .touchUpInside)
                reportRefundItemCell.selectAllButton.addTarget(self, action: #selector(changeOrderQty(sender:)), for: .touchUpInside)

                if isSelectRefundAll {
                    reportRefundItemCell.isUserInteractionEnabled = false
                    reportRefundItemCell.setSelectAllButton()
                }
            }
            return reportRefundItemCell
        }
        return cell
    }
    
    @objc func changeOrderQty(sender: UIButton)
    {
        if let cell = sender.superview?.superview as? ReportRefundItemCell
        {
            textFieldDidEndEditing(cell.refundQty)
        }
    }
    
    //MARK : - Textfield delegate
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        // for qty textfield on ReportRefundItemCell only
        if let safeOrderItemArray = refundData.orderItemsArray as? [OrderItem]
        {
            let existOrder = safeOrderItemArray[textField.tag]
            let safeInt = Int(textField.text ?? "0") ?? 0
            var itemQty = NSDecimalNumber.init(value: safeInt)
            let defaultQty = NSDecimalNumber.init(string:"0")
            itemQty = itemQty != NSDecimalNumber.notANumber ? itemQty : defaultQty
            
            replace(existOrder: existOrder, atIndex: textField.tag, withNewQty: itemQty)
            recalculate()
        }
    }
}
