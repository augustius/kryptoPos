//
//  ReportSync.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 30/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation
import CircleProgressBar

class ReportSyncCell: UITableViewCell{
    
    @IBOutlet var syncTime: UILabel!
    @IBOutlet var syncMessage: UILabel!
}

class ReportSync: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate
{
    @IBOutlet var syncProgressBar: CircleProgressBar!
    @IBOutlet var syncTable: UITableView! //this table will only load when developerMode is true
    @IBOutlet var overallView: UIView!
    @IBOutlet var searhResponseTextfield: UITextField!
    @IBOutlet var blurView: UIView!
    @IBOutlet var merchantSyncView: UIView!
    @IBOutlet var storeSyncStatus: UIImageView!
    @IBOutlet var itemSyncStatus: UIImageView!
    @IBOutlet var employeeSyncStatus: UIImageView!
    @IBOutlet var customerSyncStatus: UIImageView!
    @IBOutlet var transactionSyncStatus: UIImageView!
    @IBOutlet var settingSyncStatus: UIImageView!

    @IBOutlet var tapGesture: UITapGestureRecognizer!
    @IBOutlet var developerSyncView: UIView!
    
    var arrSyncData = [syncData]()
    var shownArrSyncData = [syncData]()
    let dateFormatter = DateFormatter()
    var respondedResponseArr = [String]()
    var writingResponseArr = [String]()
    var readingResponseArr = [String]()
    var executeReadingApi = false{
        didSet{
            if executeReadingApi
            {
                _ = Networking.shared().executeAllReadingApi()
            }
        }
    }
    var developerMode = false
    let expectedStoreResponseArr =
        Networking.shared().getStoreInformationReadingApiResponseName() +
            Networking.shared().getStoreInformationWritingApiResponseName()
    let expectedItemResponseArr =
        Networking.shared().getItemInformationReadingApiResponseName() +
            Networking.shared().getItemInformationWritingApiResponseName()
    let expectedEmployeeResponseArr =
        Networking.shared().getEmployeeInformationReadingApiResponseName() +
            Networking.shared().getEmployeeInformationWritingApiResponseName()
    let expectedCustomerResponseArr =
        Networking.shared().getCustomerInformationReadingApiResponseName() +
            Networking.shared().getCustomerInformationWritingApiResponseName()
    let expectedTransactionResponseArr =
        Networking.shared().getTransactionInformationReadingApiResponseName() +
            Networking.shared().getTransactionInformationWritingApiResponseName()
    let expectedSettingResponseArr =
        Networking.shared().getSettingInformationReadingApiResponseName() +
            Networking.shared().getSettingInformationWritingApiResponseName()

    var allExpectedMerchantResponse = [[String]]()
    var allRespondedMerchantResponse = [[syncResponse]]()
    var allMerchantSyncImage = [UIImageView]()
    var numberOfTap = 0
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        setupData()
        
        view.addSubview(blurView)
        view.sendSubviewToBack(blurView)
        developerSyncView.addSubview(merchantSyncView)
        overallView.addGestureRecognizer(tapGesture)
        syncTable.rowHeight = UITableView.automaticDimension
        syncTable.estimatedRowHeight = 30.0
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func screenTapped(_ sender: UITapGestureRecognizer)
    {
        if arrSyncData.count > 0
        {
            if numberOfTap > 10
            {
                searhResponseTextfield.isHidden = false
                developerMode = true
                overallView.removeGestureRecognizer(sender)
                merchantSyncView.removeFromSuperview()
                reloadSyncTable()
                present_alert("Developer Mode On", message: "Developer mode is now on.\nBelow are the full response message from server")
            }
            else
            {
                numberOfTap += 1
            }
        }
        else
        {
            print("no data to show, must click 'sync now' first")
        }
    }
    
    @IBAction func dismissForm()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func syncNow()
    {
        if executeReadingApi
        {
            executeReadingApi = false
            setupData()
        }
        
        if !developerMode
        {
            setupMerchantSyncRotating()
        }
        
        notification.addObserver(self, selector: #selector(syncResponseNotification), name: networkNotification.transactionSyncForm.notification, object: nil)

        _ = Networking.shared().executeAllWritingApi()
        showLoadingScreen()
    }
    
    @IBAction func sendLocalDB()
    {
        self.handleSendLocalDB()
    }
    
    //MARK: - UITableViewDelegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return shownArrSyncData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReportSyncCell", for: indexPath)
        
        if let syncCell = cell as? ReportSyncCell
        {
            let data = shownArrSyncData[indexPath.row]
            syncCell.syncTime.text = data.time + " |"
            syncCell.syncTime.textColor = data.messageColor
            syncCell.syncMessage.text = data.message
            syncCell.syncMessage.textColor = data.messageColor
            
            return syncCell
        }
        
        return cell
    }
    
    //MARK: - UITextfield delegate method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        reloadSyncTable(withResponse: textField.text ?? "")
        textField.resignFirstResponder()
        return true
    }

}
