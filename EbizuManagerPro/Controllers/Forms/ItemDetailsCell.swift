//
//  ItemDetailsCell.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 04/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class ChoiceCollectionCell: UICollectionViewCell
{
    @IBOutlet var back: UIButton!
    @IBOutlet var price: UILabel!
    @IBOutlet var initialName: UILabel!
    @IBOutlet var name: UILabel!
    
    var choiceID = ""
    var parentID = ""
    
    var choosed = false{
        didSet
        {
            if choosed
            {
                back.backgroundColor = UIColor(netHex: "0x3D8AE9")
                price.textColor = UIColor.white
                initialName.textColor = UIColor.white
                name.textColor = UIColor.white
            }
            else
            {
                back.backgroundColor = UIColor(netHex: "0xEBF3FD")
                price.textColor = UIColor.gray
                initialName.textColor = UIColor.gray
                name.textColor = UIColor.gray
            }
        }
    }
    
}

class AdditionCollectionCell: UICollectionViewCell
{
    @IBOutlet var back: UIButton!
    @IBOutlet var initialName: UILabel!
    @IBOutlet var blurView: UIView!
    @IBOutlet var name: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var add: UIButton!
    @IBOutlet var minus: UIButton!
    
    var additionID = ""
    
    var qty = 0 {
        didSet
        {
            qty = (qty < 0) ? 0 : qty
            
            add.setTitle("\(qty)", for: .normal)
            add.isHidden = (qty == 0)
            minus.isHidden = (qty == 0)
        }
    }
    
    @IBAction func addQty()
    {
        qty += 1
    }
    
    @IBAction func minusQty()
    {
        qty -= 1
    }
}

class ChoiceAdditionTableCell: UITableViewCell
{
    @IBOutlet var modifierName: UILabel!
    @IBOutlet private weak var modifierCollection: UICollectionView!
    
    var collectionViewOffset: CGFloat
    {
        get
        {
            return modifierCollection.contentOffset.x
        }
        set
        {
            modifierCollection.contentOffset.x = newValue
        }
    }
    
    // Notes: special thanks to https://ashfurrow.com/blog/putting-a-uicollectionview-in-a-uitableviewcell-in-swift/
    func setCollectionViewDataSourceDelegate <D: UICollectionViewDataSource & UICollectionViewDelegate> (dataSourceDelegate: D, forRow row: Int)
    {
        modifierCollection.delegate = dataSourceDelegate
        modifierCollection.dataSource = dataSourceDelegate
        modifierCollection.tag = row
        modifierCollection.reloadData()
    }
}
