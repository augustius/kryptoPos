//
//  NewPaymentCompleteController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 21/08/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension NewPaymentComplete
{
    func setupTransactionForPrinting()
    {
        let custGstId = (ModelController.shared().currentCompleteOrder.customer_gstid != nil) ? ModelController.shared().currentCompleteOrder.customer_gstid : ""
        EbizuPrinter.sharedPrinter.receiptType = (custGstId == "") ? .normal : .gstFullTax
        EbizuPrinter.sharedPrinter.completeOrder = (ModelController.shared().currentCompleteOrder.copy() as? CompleteOrder) ?? CompleteOrder()

        EbizuPrinter.sharedPrinter.setupSalesReceipt()

        if SessionData.shared().force_print_mode_enable
        {
            self.printReceipt()
        }
    }
    
    func getAdjustedTransactionDict() -> [String:AnyObject]
    {
        var returnedData =  [String:AnyObject]()
        
        // adjust item and modifier data
        var itemArray = [AnyObject]()
        if let orderItemArray = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
        {
            for item in orderItemArray
            {
                if var tempItem = item.dictionaryRepresentation() as? [String:AnyObject]
                {
                    var tempChoice = [[String:AnyObject]]()
                    var tempAddition = [[String:AnyObject]]()
                    
                    //adjust modifier(choice) data
                    if let modifier = item.allModifiersDict as? [String:AnyObject]
                    {
                        if let choiceArr = modifier[CHOICE] as? [[String:AnyObject]]
                        {
                            for choice in choiceArr
                            {
                                if let selectedChoice = choice["selected_choice"] as? [String:AnyObject]
                                {
                                    tempChoice.append(selectedChoice)
                                }
                            }
                        }
                        
                        //adjust modifier(addition) data
                        if let additionArr = modifier[ADDITION] as? [[String:AnyObject]]
                        {
                            for addition in additionArr
                            {
                                let additionQtyStr = (addition["mod_qty"] as? String) ?? "0"
                                let additionQty = Int(additionQtyStr) ?? 0
                                
                                if additionQty > 0
                                {
                                    tempAddition.append(addition)
                                }
                            }
                        }
                    }
                    
                    //adjust item data
                    var modifierDict = [String:AnyObject]()
                    modifierDict["addition"] = tempAddition as AnyObject
                    modifierDict["choice"] = tempChoice as AnyObject
                    let modifierArr = [modifierDict]
                    
                    tempItem["modifiers"] = modifierArr as AnyObject
                    itemArray.append(tempItem as AnyObject)
                }
            }
        }
        
        // adjust transaction data
        ModelController.shared().currentCompleteOrder.trx_id = String(format:"%@-%.0f",userDefaults.string(forKey: UDID) ?? "",NSDate().timeIntervalSince1970)
        if var transactionDict = ModelController.shared().currentCompleteOrder.dictionaryRepresentation() as? [String:AnyObject]
        {
            transactionDict["items"]      = itemArray as AnyObject
            transactionDict["user"]       = StringUtils.get_user_detail("user_id") as AnyObject
            transactionDict["customer"]   = (ModelController.shared().currentCustomer.customer_id ?? "0") as AnyObject
            transactionDict["currency"]   = ModelController.shared().currentCompleteOrder.currency as AnyObject
            transactionDict["company"]    = StringUtils.get_business_detail("business_id") as AnyObject
            transactionDict["notes"]      = "" as AnyObject
            transactionDict["status"]     = "1" as AnyObject
            transactionDict["close_time"] = String(format:"%.0f",NSDate().timeIntervalSince1970) as AnyObject
            
            returnedData = transactionDict
        }
        
        print("your data sir : ")
        print(returnedData)
        
        return returnedData
    }
    
    func saveTransactionToDatabase()
    {
        showLoadingScreen()
        notification.addObserver(self, selector: #selector(enableUIPaymentComplete), name: NSNotification.Name(DATABASE_STATUS), object: nil)
        
        shareddb.shared().insert_trx_details(getAdjustedTransactionDict())
    }
    
    @objc func enableUIPaymentComplete(notif:NSNotification)
    {
        notification.removeObserver(self, name: NSNotification.Name(DATABASE_STATUS), object: nil)
        
        guard
            let notifData = notif.object as? [AnyObject],
            let notifMessage = notifData.last as? String,
            let databaseStatus = notifData.first as? String
            else
        {
            hideLoadingScreen()
            present_sclalert("DATA CONVERSION ERROR", message: "Fail to convert to type safe", alert_style: .info)
            print("error on \(#function), \(#line) "); return
        }
        
        if databaseStatus == DATABASE_STATUS_SUCCESS
        {
            shareddb.shared().increase_transaction_number()
            shareddb.shared().increase_order_counter_number()
            
            // adjust split bill data
            if ModelController.shared().currentCompleteOrder.from_split_bill
            {
                shareddb.shared().reupdate_confirmed_order_item()
            }
            
            // adjust item stock
            if let itemArr = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
            {
                var stockData = [[String:String]]()
                
                for item in itemArr
                {
                    let currentStockQty = NSDecimalNumber.init(value:shareddb.shared().get_stock_left(item.itemID))
                    let remainStock = currentStockQty.intValue - item.itemQuantity.intValue
                    
                    var tempDict = [String:String]()
                    tempDict["item_id"] = item.itemID
                    tempDict["stock_left"] = String(format: "%d", remainStock)
                    stockData.append(tempDict)
                }
                
                if stockData.count > 0
                {
                    shareddb.shared().update_stock_quantity(stockData)
                }
            }
            
            // adjust multiple payment data 
            insertMultiPayData()
            
            // adjust current balance
            if ModelController.shared().currentCompleteOrder.pay_type_name.uppercased() == CASH
            {
                shareddb.shared().update_current_balance(ModelController.shared().currentCompleteOrder.total.doubleValue, with_plus: true)
            }
            
        }
        else
        {
            DispatchQueue.main.async{
                self.dbErrorMsg = notifMessage
                self.performSegue(withIdentifier: "TransactionFailToRecordReturn", sender: self)
            }
        }
        
        hideLoadingScreen()
    }
    
    func insertMultiPayData()
    {
        if completeMultiPayData.count > 0
        {
            var tempArr = [[String:String]]()
            var changes = ModelController.shared().currentCompleteOrder.payment_changes ?? NSDecimalNumber.zero
            for dict in completeMultiPayData
            {
                var tempDict = dict
                tempDict["trx_id"] = ModelController.shared().currentCompleteOrder.trx_id
                
                // adjust current balance data AND deduct changes on cash if any
                let paymenType = (tempDict["type_name"] ?? "").uppercased()
                if paymenType == CASH
                {
                    var paidAmount = NSDecimalNumber.init(string: tempDict["paid_amount"])
                    if changes.doubleValue > 0
                    {
                        paidAmount = (paidAmount == NSDecimalNumber.notANumber) ? NSDecimalNumber.zero : paidAmount
                        if changes.doubleValue > paidAmount.doubleValue
                        {
                            changes = changes.subtracting(paidAmount)
                            paidAmount = NSDecimalNumber.zero
                        }
                        else
                        {
                            paidAmount = paidAmount.subtracting(changes)
                            changes = NSDecimalNumber.zero
                        }
                        tempDict["paid_amount"] = String(format: "%.2f", paidAmount.doubleValue)
                    }
                    shareddb.shared().update_current_balance(paidAmount.doubleValue, with_plus: true)
                }
                
                tempArr.append(tempDict)
            }
            shareddb.shared().insert_multiple_payment_details(tempArr)
        }
    }
    
    //MARK: - P2P communication Method
    func sendPaymentDataToCustomerDisplay()
    {
        var data = [String:AnyObject]()
        data["CompleteOrder"] = ModelController.shared().currentCompleteOrder.dictionaryRepresentation() as AnyObject
        data[P2PConnection.P2PCommand] = P2PConnection.P2PCommandList.PAYMENT_CD.rawValue as AnyObject
        
        P2PConnection.sharedP2PConnection().sendDataToPeers(data: data as AnyObject)
    }
}
