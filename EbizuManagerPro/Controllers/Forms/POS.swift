//
//  POS.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class Pos: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, ItemDetailsDelegate, UITextFieldDelegate, customKeyboardViewDelegate
{
    @IBOutlet var pinTextField: UITextField!

    @IBOutlet var noConnectivityStatus: UIButton!
    
    @IBOutlet var lastShiftLabel: UILabel!
    @IBOutlet var closeShiftView: UIView!
    
    @IBOutlet weak var spaceBetweenLeftAndRightConstraint: NSLayoutConstraint!
    
    @IBOutlet var currentHoldOrder: UIButton!
    @IBOutlet weak var wholeRightOrderView: UIView!
    @IBOutlet weak var wholeRightOrderViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var categoryCollectionView: UICollectionView!
    @IBOutlet var itemCollectionView: UICollectionView!
    @IBOutlet var tableCollectionView: UICollectionView!
    @IBOutlet var orderTableView: UITableView!
    
    @IBOutlet var categoryName: UILabel!
    @IBOutlet var orderLabel: UILabel!
    
    @IBOutlet var topOrderView: UIView!
    @IBOutlet weak var topOrderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var selectTableView: UIView!
    @IBOutlet var tableDetailView: UIView!
    @IBOutlet weak var tableDetailViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var bottomOrderView: UIView!
    
    @IBOutlet weak var barcodeScannerWidthConstraint: NSLayoutConstraint!
    @IBOutlet var barcodeScanner: UIButton!
    @IBOutlet var barcodeScannerTextfield: UITextField!
    
    @IBOutlet var searchItemWidthConstraint: NSLayoutConstraint!
    @IBOutlet var searchItem: UIButton!
    @IBOutlet var searchItemTextfield: UITextField!
    
    @IBOutlet var noOfPax: UIButton!
    @IBOutlet var customerImage: UIButton!
    @IBOutlet var customerName: UITextField!
    @IBOutlet var orderType: UIButton!
    @IBOutlet var currentSalesPerson: UILabel!
    
    @IBOutlet var orderDetailsView: UIStackView!
    @IBOutlet weak var bottomOrderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var serviceCharge: UILabel!
    @IBOutlet var discount: UILabel!
    @IBOutlet var voucher: UILabel!
    @IBOutlet var gst: UILabel!
    @IBOutlet var roundingAmount: UILabel!
    
    @IBOutlet var detailsIcon: UIButton!
    @IBOutlet var detailsLabel: UILabel!
    
    @IBOutlet var pay: UIButton!
    @IBOutlet var longPressGesture: UILongPressGestureRecognizer!
    
    var tableSelected = false {
        didSet{
            
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        self.topOrderViewHeightConstraint.constant = self.tableSelected ? 128 : 50
                        self.tableDetailViewHeightConstraint.constant = self.tableSelected ? 85 : 0
                        self.view.updateConstraints()
                    }, completion: nil)
            }
        }
    }
    
    var detailsShown = false {
        didSet{
            
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        self.detailsLabel.text = self.detailsShown ? "hide" : "details"
                        self.bottomOrderViewHeightConstraint.constant = self.detailsShown ? 270 : 135
                        self.view.updateConstraints()
                    }, completion: nil)
             }
        }
    }
    
    var barcodeScannerActive = false {
        didSet{
         
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        if self.barcodeScannerActive
                        {
                            self.barcodeScannerWidthConstraint.constant = 250
                            self.barcodeScannerTextfield.becomeFirstResponder()
                        }
                        else
                        {
                            self.barcodeScannerWidthConstraint.constant = 40
                            self.barcodeScannerTextfield.resignFirstResponder()
                        }
                    }, completion: nil)
            }
        }
    }
    
    var searchItemActive = false {
        didSet{
            
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        if self.searchItemActive
                        {
                            self.searchItemWidthConstraint.constant = 250
                            self.searchItemTextfield.becomeFirstResponder()
                        }
                        else
                        {
                            self.searchItemWidthConstraint.constant = 40
                            self.searchItemTextfield.resignFirstResponder()
                        }
                        self.view.updateConstraints()
                    }, completion: nil)
            }
        }
    }
    
    var dineTypeActive = false {
        didSet{
            
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        self.selectTableView.isHidden = !self.dineTypeActive
                        self.orderType.isHidden = !self.dineTypeActive
                }, completion: nil)
            }
        }
    }
    
    var selectedCategoriesIndexPath: IndexPath? = nil
    var categoriesArray = [[String:AnyObject]]()
    var productArray = [[String:AnyObject]]()
    var selectedTableArray = [String]()
    var selectedOrderItem = OrderItem()
    var addNewLineItem = false
    var accountSubscriptionStatus = ""
    var reachability: Reachability? = Reachability.networkReachabilityForInternetConnection()
    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    
    // MARK: - View Cycle
    override func viewDidLoad()
    {
        customKeyboard?.delegate = self
        orderTableView.rowHeight = UITableView.automaticDimension
        orderTableView.estimatedRowHeight = 80.0
        orderTableView.register(UINib(nibName:"CustomOrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        tableCollectionView.register(UINib(nibName:"CustomSelectedTableCell", bundle: nil), forCellWithReuseIdentifier: "SelectedTableCell")
        itemCollectionView.addGestureRecognizer(longPressGesture)
        
        // set device as master
        P2PConnection.sharedP2PConnection().setupMasterDevice()
        notification.addObserver(self, selector: #selector(didReceiveDataWithNotification(notif:)), name: NSNotification.Name("MCDidReceiveDataNotification"), object: nil)
        
        // set observer to observe internet statusdidReceiveDataWithNotification
        notification.addObserver(self, selector: #selector(checkReachabilityStatus), name: NSNotification.Name(rawValue: ReachabilityDidChangeNotificationName), object: nil)
        _ = reachability?.startNotifier()
        checkReachabilityStatus()

        setAccountSuspendedOrTerminatedView()
        registerUserForCrashlytic()
        setupData()
        setupView()
        Networking.shared().getData(forReading: .readingAccountSubscriptionStatus)
        Networking.shared().putData(forWriting: .writingPushToken)
        sendFormToGoogleAnalytic()
    }
    
    deinit
    {
        NotificationCenter.default.removeObserver(self)
        reachability?.stopNotifier()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        refreshPOSView()
    }
    
    // MARK: - IBOutlet method
    @IBAction func refreshMenu()
    {
        DispatchQueue.main.async{
            self.setupData()
            if let safeIndex = self.selectedCategoriesIndexPath {
                self.collectionView(self.categoryCollectionView, didSelectItemAt: safeIndex)
            }
        }
    }
    
    @IBAction func barcodeScannerPressed()
    {
        barcodeScannerActive = !barcodeScannerActive
    }
    
    @IBAction func itemSearchPressed()
    {
        searchItemActive = !searchItemActive
    }
    
    @IBAction func longPressedItem(_ sender: UILongPressGestureRecognizer)
    {
        if sender.state != .ended { return }
        
        let p:CGPoint = sender.location(in: itemCollectionView)
        let indexPath = itemCollectionView.indexPathForItem(at: p)
        if let safeIndexPath = indexPath
        {
            if let prod = Product.init(dictionary: productArray[safeIndexPath.row])
            {
                addNewLineItem = true
                selectedProduct(product: prod)
            }
        }
    }
    
    @IBAction func selectCustomers(_ sender: UIButton)
    {
        DispatchQueue.main.async{
            self.performSegue(withIdentifier: "showCustomer", sender: sender)
        }
    }
    
    @IBAction func selectTable(_ sender: UIButton)
    {
        DispatchQueue.main.async{
            self.performSegue(withIdentifier: "showTables", sender: sender)
        }
    }

    @IBAction func selectOrderType(_ sender: UIButton)
    {
        sender.setTitle(sender.titleLabel?.text == "Dine In" ? "Take Away" : "Dine In",for: .normal)
    }
    
    @IBAction func holdOrder()
    {
        holdOrderFunction()
    }
    
    @IBAction func cancelOrder()
    {
        cancelOrderAndItemAlert()
    }
    
    @IBAction func showPaymentDetails()
    {
        detailsShown = !detailsShown
    }
    
    @IBAction func payNow()
    {
        finalizeSelectedDataToCurrentCompleteOrder()
        payNowFunction()
    }
    
    // MARK: - UITableview Delegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath)
        
        if let orderCell = cell as? OrderCell
        {
            if let itemArray = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
            {
                let item = itemArray[indexPath.row]
                
                orderCell.itemName.text = item.itemName.capitalized
                if item.type_of_price == "Unit Price"
                {
                    orderCell.itemName.text = String(format:"%@ @ %.2f%@", item.itemName.capitalized, item.uom_qty.doubleValue, item.type_of_unit)
                }
                
                orderCell.qty.delegate = self
                orderCell.qty.tag = indexPath.row
                orderCell.qty.text = String(format:"%d",item.itemQuantity.intValue)

                orderCell.total.text = String(format: "%.2f", item.totalPrice_bfr_bill_disc.doubleValue)
                orderCell.remarks.text = item.remarks
                orderCell.modifier.text = item.modifierLabel
                
                orderCell.discount.text = ""
                if item.discount.doubleValue > 0.0 && item.is_edited_item_price != "price_edited"
                {
                    orderCell.discount.text = "Discount @ " + String(format:"%.2f",item.discount.multiplying(by: item.itemQuantity).doubleValue).addCurrency()
                }
                
                orderCell.gst.text = ""
//                if item.gst_rate.doubleValue > 0.0 && SessionData.shared().gst_enable
//                {
//                    orderCell.gst.text = String(format:"SST %@ %@%% @ ", item.gst_code == "SV" ? "" : item.gst_code, item.gst_rate) + String(format : "%.2f",item.gst_value_bfr_bill_disc.doubleValue).addCurrency()
//                }
                
                orderCell.plus.addTarget(self, action: #selector(changeOrderQty(sender:)), for: .touchUpInside)
                orderCell.minus.addTarget(self, action: #selector(changeOrderQty(sender:)), for: .touchUpInside)
            }
        }

        return cell
    }
    
    @objc func changeOrderQty(sender: UIButton)
    {
        if let cell = sender.superview?.superview as? OrderCell
        {
            textFieldDidEndEditing(cell.qty)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if let itemArray = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
        {
            return itemArray.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let safeSelectedOrderItem = ModelController.shared().currentCompleteOrder.orderItemsArray[indexPath.row] as? OrderItem
        {
            selectedOrderItem = safeSelectedOrderItem
            DispatchQueue.main.async{
                self.performSegue(withIdentifier: "showItemDetails", sender: indexPath)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle
    {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath)
    {
        switch editingStyle
        {
        case .delete:
            self.deleteOrderItemAt(Index:indexPath.row)
            break
        default:
            break
        }
    }
    
    func scrollOrderTableRowToBottom()
    {
        orderTableView.scrollToRow(at: IndexPath.init(row: ModelController.shared().currentCompleteOrder.orderItemsArray.count - 1, section: 0), at: .bottom, animated: true)
    }

    func scrollOrderToIndex(_ index:Int) {
        orderTableView.scrollToRow(at: IndexPath.init(row: index, section: 0), at: .middle, animated: true)
    }
    
    // MARK: - UICollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        switch collectionView
        {
        case categoryCollectionView:
            
            let cat = Categories.init(dictionary:categoriesArray[indexPath.row])
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath)
            
            if let categoriesCell = cell as? CategoriesCell
            {
                categoriesCell.initialName.setTitle(cat?.title.initialChar().capitalized, for: .normal)
                categoriesCell.name.text = cat?.title.capitalized
                
                return categoriesCell
            }
            
            return cell
            
        case itemCollectionView:
            
            let prod = Product.init(dictionary:productArray[indexPath.row])
            
            if SessionData.shared().image_on_off_enable
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductWithItemCell", for: indexPath)
                
                if let productWithItemCell = cell as? ProductWithItemCell
                {
                    productWithItemCell.name.text = prod?.item_name.capitalized
                    productWithItemCell.price.text = String(format:"%.2f",NSDecimalNumber.init(string: prod?.price).doubleValue).addCurrency()
                    productWithItemCell.back.setBackgroundImage(prod?.itemUIImage, for: .normal)

                    if prod?.itemUIImage != nil
                    {
                        productWithItemCell.initialName.setTitle("", for: .normal)
                    } else {
                        productWithItemCell.initialName.setTitle(prod?.item_name.initialChar().capitalized, for: .normal)
                    }
                    
                    return productWithItemCell
                }
                
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath)
                
                if let productCell = cell as? ProductCell
                {
                    productCell.initialName.setTitle(prod?.item_name.initialChar().capitalized, for: .normal)
                    productCell.name.text = prod?.item_name.capitalized
                    productCell.price.text = String(format:"%.2f",NSDecimalNumber.init(string: prod?.price).doubleValue).addCurrency()
                    
                    return productCell
                }
            
                return cell
            }
            
        default: //tableCollectionView
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedTableCell", for: indexPath)
            
            if let selectedTableCell = cell as? SelectedTableCell
            {
                selectedTableCell.name.text = selectedTableArray[indexPath.row]
                
                return selectedTableCell
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        switch collectionView
        {
        case categoryCollectionView:
            return categoriesArray.count
        case itemCollectionView:
            return productArray.count
        default: // tableCollectionView
            return selectedTableArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        switch collectionView
        {
        case categoryCollectionView:

            selectedCategoriesIndexPath = indexPath
            let cat = Categories.init(dictionary: categoriesArray[indexPath.row])
            if let safeProductArray = shareddb.shared().get_product_categories_items(cat?.ctg_id, with: .cat_id) as? [[String:AnyObject]]
            {
                productArray = safeProductArray
                categoryName.text = cat?.title
                itemCollectionView.reloadData()
            }
            break
            
        case itemCollectionView:
            
            if let prod = Product.init(dictionary: productArray[indexPath.row])
            {
                selectedProduct(product: prod)
            }
            break
            
        default: // tableCollectionView
                    
            break
        }
    }
    
    // MARK: - UITextField delegate method
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if textField != searchItemTextfield && textField != barcodeScannerTextfield && textField != pinTextField
        {
            let pointInTable:CGPoint = textField.superview!.convert(textField.frame.origin, to:orderTableView)
            var contentOffset:CGPoint = orderTableView.contentOffset
            contentOffset.y  = pointInTable.y
            if let accessoryView = textField.inputAccessoryView
            {
                contentOffset.y -= accessoryView.frame.size.height
            }
            orderTableView.contentOffset = contentOffset
        }
        
        if searchItemTextfield.isFirstResponder
        {
            itemSearchPressed()
        }
        else if barcodeScannerTextfield.isFirstResponder
        {
            barcodeScannerPressed()
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        switch textField
        {
        case barcodeScannerTextfield:
            if string == "\n" && textField.text != ""
            {
                if let itemData = shareddb.shared().get_product_categories_items(textField.text, with: .barcode) as? [[String:AnyObject]]
                {
                    if itemData.count > 0
                    {
                        if let prod = Product.init(dictionary: itemData.first)
                        {
                            selectedProduct(product: prod)
                        }
                    }
                    else
                    {
                        present_sclalert("Fail", message: "No Record found for '\(textField.text!)' barcode.", alert_style: .error)
                    }
                }
                textField.text = ""
            }
            break
        case searchItemTextfield:
            if string == "\n" && textField.text != ""
            {
                if let itemData = shareddb.shared().get_product_categories_items(textField.text, with: .item_name) as? [[String:AnyObject]]
                {
                    if itemData.count > 0
                    {
                        categoryName.text = ""
                        productArray = itemData
                        itemCollectionView.reloadData()
                    }
                    else
                    {
                        present_sclalert("Fail", message: "No item found with name '\(textField.text!)'.", alert_style: .error)
                    }
                }
                textField.text = ""
            }
            break
        default:
            break
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {

        if textField != searchItemTextfield && textField != barcodeScannerTextfield
        {
            // for qty textfield on ordercell && pinTextField only
            customKeyboard?.textfield = textField
            customKeyboard?.customKeyboardType = textField == pinTextField ? .pin : .intNumber
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField != searchItemTextfield && textField != barcodeScannerTextfield
        {
            // for qty textfield on ordercell && pinTextField only
            if  textField != pinTextField, let safeOrderItemArray = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
            {
                let existOrder = safeOrderItemArray[textField.tag]
                let defaultQty = NSDecimalNumber.init(string:"1.00")
                var itemQty = NSDecimalNumber.init(string: textField.text)
                itemQty = itemQty != NSDecimalNumber.notANumber ? itemQty : defaultQty
                existOrder.itemQuantity = itemQty
                
                ModelController.shared().currentCompleteOrder.orderItemsArray[textField.tag] = existOrder
                setOrderChargeDetails(false)
            }
        }
    }

}

