//
//  CustomerList.swift
//  Manager
//
//  Created by augustius cokroe on 14/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum customerListUsage {
    case toView, toSelect
}

class CustomerListCell : UICollectionViewCell
{
    @IBOutlet private var profilePic: UIButton!
    @IBOutlet private var name: UILabel!
    var customerData = Customer() {
        didSet{
            self.name.text = self.customerData.customerFullName
            self.profilePic.setBackgroundImage(self.customerData.storedProfilePicUIImage, for: .normal)
        }
    }
}

class CustomerList : UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate
{
    @IBOutlet var menuBtn: UIBarButtonItem!
    @IBOutlet var customerCollectionView: UICollectionView!
    @IBOutlet var enteredCustomerName: UITextField!
    @IBOutlet var SearchCustomerWidthConstraint: NSLayoutConstraint!
    
    var allCustomerArr = [Customer]()
    var shownCustomerArr = [Customer]()
    var currentSelectedCustomer = Customer()
    var formUsage:customerListUsage = .toView
    var SearchCustomerActive = false {
        didSet{
            SearchCustomerWidthConstraint.constant = self.SearchCustomerActive ? 500 : 40
            if self.SearchCustomerActive
            {
                enteredCustomerName.becomeFirstResponder()
            }
            else
            {
                enteredCustomerName.resignFirstResponder()
            }
        }
    }
    
    // MARK: - View Cycle
    override func viewDidLoad()
    {
        if let safeCust = shareddb.shared().get_all_cust() as? [Customer]
        {
            allCustomerArr = safeCust
            searchCustomer(with: "")
        }
        formUsage = ModelController.shared().currentCompleteOrder.hold_order_id == nil ? .toView : .toSelect
        switch formUsage
        {
        case .toView:
            break
        case .toSelect:
            menuBtn.title = "Cancel"
            menuBtn.image = nil
        }
    }
    
    // MARK: - IBAction method
    @IBAction func topRightBarItemTapped(_ sender: UIBarButtonItem)
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SearchCustomerTapped()
    {
        SearchCustomerActive = !SearchCustomerActive
    }
    
    func searchCustomer(with name:String)
    {
        shownCustomerArr = allCustomerArr.filter({$0.customerFullName.lowercased().contains(name.lowercased())})
        shownCustomerArr = shownCustomerArr.count == 0 ? allCustomerArr : shownCustomerArr
        DispatchQueue.main.async {
            self.customerCollectionView.reloadData()
        }
    }
    
    // MARK: - UICollectionView method
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomerListCell", for: indexPath)
        
        if let customerListCell = cell as? CustomerListCell
        {
            let data = shownCustomerArr[indexPath.row]
            customerListCell.customerData = data
                        
            return customerListCell
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return shownCustomerArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        currentSelectedCustomer = shownCustomerArr[indexPath.row]
        switch formUsage
        {
        case .toView:
            performSegue(withIdentifier: "showCustomerDetail", sender: self)
        case .toSelect:
            ModelController.shared().currentCustomer = currentSelectedCustomer
            dismiss(animated: true, completion: nil)
        }
    }
    
    // MARK: - UITextfield delegate method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let textEntered = ((textField.text as NSString?) ?? "").replacingCharacters(in: range, with: string)
        searchCustomer(with: textEntered)
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        SearchCustomerActive = false
        return true
    }
    
    // MARK: - Segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "addNewCustomer"
        {
            guard
                let dvc = segue.destination as? AddCustomer
                else { print("error on \(#function), \(#line) "); return}

            dvc.formUsage = .toAdd
        }
        else if segue.identifier == "showCustomerDetail"
        {
            guard
                let dvc = segue.destination as? CustomerDetail
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.currentCustomer = currentSelectedCustomer
        }
    }
    
    @IBAction func customerListReturnSegue(segue: UIStoryboardSegue)
    {
        if segue.identifier == "customerDetailDone"
        {
            guard
                let svc = segue.source as? AddCustomer
                else { print("error on \(#function), \(#line) "); return}
            
            switch svc.formUsage
            {
            case .toAdd:
                allCustomerArr.append(shareddb.shared().insert_new_customer_details(svc.currentCustomer))
            case .toEdit:
                shareddb.shared().updateCutomerDetail(svc.currentCustomer)
                if let index = allCustomerArr.index(where: {$0.customer_id == svc.currentCustomer.customer_id})
                {
                    allCustomerArr[index] = svc.currentCustomer
                }
            }
            Networking.shared().putData(forWriting: .writingCustomer)
            
            DispatchQueue.main.async{
                self.searchCustomer(with: "")
            }
        }
        
    }
    
}
