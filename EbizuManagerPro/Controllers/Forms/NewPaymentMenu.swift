//
//  NewPaymentMenu.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 14/08/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct paymentMethodData {
    var id = ""
    var name = ""
    var creditCardNo = ""
    var type = ""
    var typeName = ""
    var amountTendered = NSDecimalNumber.init(string:"0.0")
}

class NewPaymentCollectionCell: UICollectionViewCell
{
    @IBOutlet private var paymentLogo: UIButton!
    @IBOutlet private var paymentNameView: UIView!
    @IBOutlet private var paymentName: UIButton!
    @IBOutlet var clearButton: UIButton!
    
    var data = paymentMethodData()
    {
        didSet{
            var choosenImage = UIImage.init(named: "paymentCash")
            
            switch self.data.typeName
            {
            case CARD:
                choosenImage = UIImage.init(named: "paymentCreditcard")
            case CHEQUE:
                choosenImage = UIImage.init(named: "paymentCheque")
            case MPAY:
                choosenImage = UIImage.init(named: "mpay-1")
            case MAYBANK, MAYBANKQR:
                choosenImage = UIImage.init(named: "maybank")
            case RHBCardBiz:
                choosenImage = UIImage.init(named: "rhbLogo")
            default: //CASH
                break
            }
            self.paymentLogo.setImage(choosenImage, for: .normal)
            
            let safeAmount = (self.data.amountTendered == NSDecimalNumber.notANumber) ? NSDecimalNumber.init(string:"0.0") : self.data.amountTendered
            let hasAmount = safeAmount.doubleValue > 0
            let selectedBackColor = hasAmount ? UIColor.init(color: .ebizuDarkGreen) : UIColor.groupTableViewBackground
            let selectedTextColor = hasAmount ? UIColor.white : UIColor.darkGray
            
            self.paymentNameView.backgroundColor = selectedBackColor
            self.paymentName.backgroundColor = selectedBackColor
            self.clearButton.isHidden = !hasAmount
            self.paymentName.setTitle(!hasAmount ? self.data.name : String(format:"%.2f",safeAmount.doubleValue).addCurrency() , for: .normal)
            self.paymentName.setTitleColor(selectedTextColor, for: .normal)
        }
    }
    
    func getPaymentLogo() -> UIImage
    {
        return paymentLogo.image(for: .normal) ?? UIImage.init()
    }
}

class NewPaymentMenu: UIViewController,UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate
{
    @IBOutlet var paymentOptionCollectionView: UICollectionView!
    @IBOutlet var paymentOptionView: UIView!
    @IBOutlet var paymentView: UIView!

    @IBOutlet var orderNo: UILabel!
    @IBOutlet var selectedTableView: UIView!
    @IBOutlet var selectedTableHeightConstraint: NSLayoutConstraint!
    @IBOutlet var selectedTableCollectionView: UICollectionView!
    @IBOutlet var noOfPax: UIButton!
    
    @IBOutlet var customerPic: UIButton!
    @IBOutlet var customerName: UILabel!
    @IBOutlet var OrderType: UIButton!
    
    @IBOutlet var orderTableView: UITableView!
    
    @IBOutlet var detailHeightConstraint: NSLayoutConstraint!
    @IBOutlet var detailView: UIView!
    @IBOutlet var serviceCharge: UILabel!
    @IBOutlet var discount: UILabel!
    @IBOutlet var voucher: UILabel!
    @IBOutlet var gst: UILabel!
    @IBOutlet var roundingAdj: UILabel!
    @IBOutlet var total: UILabel!
    
    @IBOutlet var payMethodName: UILabel!
    @IBOutlet var creditCardView: UIView!
    @IBOutlet var creditCardTextField: UITextField!
    @IBOutlet var amountTenderedView: UIView!
    @IBOutlet var paymentIcon: UIButton!
    @IBOutlet var amountKeypadView: UIView!
    @IBOutlet var amountShortcutView: UIView!
    
    @IBOutlet var paymentTendered: UILabel!
    
    @IBOutlet var shortcut1: UIButton!
    @IBOutlet var shortcut2: UIButton!
    @IBOutlet var shortcut3: UIButton!
    
    @IBOutlet var multiPaySwitchView: UIView!
    @IBOutlet var nonMultiPayView: UIView!
    @IBOutlet var nonMultiPay: UIButton!
    @IBOutlet var nonMultiPayViewTopConstraint: NSLayoutConstraint!
    @IBOutlet var multiPay: UIButton!
    @IBOutlet var selectedAccessory:EAAccessory!
    
    
    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    var selectedTableArray = [String]()
    var paymentMethodArray = [[String:String]]()
    var ExPayment:ExternalPaymentProtocol = Mpay.SharedExPayment()
    var completeMultiPaymentData = [[String:String]]()
    var currentMultiPaymentMethod = paymentMethodData()
    var currentSelectedPaymentMethod = paymentMethodData()
    
    // START RHB RELATED CODE
//    var accessoryList = [EAAccessory]()
//    var eaSessionController = EADSessionController.shared()
//    var totalBytesRead:UInt = 0
    // END RHB RELATED CODE
    
    var multiPayEnabled = false
    {
        didSet{
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        self.multiPay.isHidden = !self.multiPayEnabled
                    }, completion: nil)
            }
        }
    }

    var detailShown = false
    {
        didSet{
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        self.detailView.isHidden = !self.detailShown
                        self.detailHeightConstraint.constant = self.detailShown ? 240 : 100
                    }, completion: nil)
            }
        }
    }
    
    var tableExist = false
    {
        didSet{
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        self.selectedTableHeightConstraint.constant = self.tableExist ? 128 : 50
                        self.selectedTableView.isHidden = !self.tableExist
                    }, completion: nil)
            }
        }
    }
    
    // whatever value entered set it to numberEntered
    var numberEntered = ""
    {
        didSet{
            
            var finalStr = self.numberEntered.replace(".", replacement: "")
            
            switch self.numberEntered.length
            {
            case 0:
                break
            case 1:
                finalStr = String(format: "0.0%@", finalStr)
                break
            case 2:
                finalStr = String(format: "0.%@", finalStr)
                break
            default:
                let lastTwoString = finalStr.substring(from: finalStr.index(finalStr.endIndex, offsetBy: -2))
                let theRestOfFrontStr = finalStr.substring(to: finalStr.index(finalStr.endIndex, offsetBy: -2))
                finalStr = String(format: "%@.%@",theRestOfFrontStr,lastTwoString)
                break
            }
            self.finalAmountTendered = NSDecimalNumber.init(string:finalStr)
        }
    }
    
    // finalAmountTendered is being used as final amount for non-multiple payment total
    // finalAmountTendered being used as temporary storage for each payment method when multiPayEnabled = true
    var finalAmountTendered = NSDecimalNumber.init(string:"0.00")
    {
        didSet{
            // just to be safe
            self.finalAmountTendered = (self.finalAmountTendered == NSDecimalNumber.notANumber) ? NSDecimalNumber.init(string:"0.00") : self.finalAmountTendered

            let enoughAmount = !(ModelController.shared().currentCompleteOrder.total.doubleValue > self.finalAmountTendered.doubleValue)
            let amountDue = enoughAmount ? NSDecimalNumber.init(string:"0.00") : ModelController.shared().currentCompleteOrder.total.subtracting(self.finalAmountTendered)
            
            let amountStr = String(format: "%.2f", self.finalAmountTendered.doubleValue).addCurrency()
            let amountDueStr = String(format: "%.2f", amountDue.doubleValue).addCurrency()
            
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        let selectedBackColor = enoughAmount || self.multiPayEnabled ? UIColor.init(color: .ebizuDarkGreen) : UIColor.groupTableViewBackground
                        let selectedTextColor = enoughAmount || self.multiPayEnabled ? UIColor.white : UIColor.red
                        
                        self.paymentTendered.text = amountStr
                        self.nonMultiPayView.backgroundColor = selectedBackColor
                        self.nonMultiPay.backgroundColor = selectedBackColor
                        self.nonMultiPay.setTitleColor(selectedTextColor, for: .normal)
                        self.nonMultiPay.isUserInteractionEnabled = enoughAmount || self.multiPayEnabled
                        
                        if self.multiPayEnabled
                        {
                            self.nonMultiPay.setTitle("ADD NOW - " + amountStr , for: .normal)
                        }
                        else
                        {
                            let selectedText = enoughAmount ? "PAY NOW - " + amountStr : "AMOUNT DUE - " + amountDueStr
                            self.nonMultiPay.setTitle(selectedText, for: .normal)
                        }
                    }, completion: nil)
            }
        }
    }
    
    // finalAmountTenderedMultiPay is being used as final amount for multiple payment total
    var finalAmountTenderedMultiPay = NSDecimalNumber.init(string:"0.00")
    {
        didSet{
            // just to be safe
            self.finalAmountTenderedMultiPay = (self.finalAmountTenderedMultiPay == NSDecimalNumber.notANumber) ? NSDecimalNumber.init(string:"0.00") : self.finalAmountTenderedMultiPay
            
            let enoughAmount = !(ModelController.shared().currentCompleteOrder.total.doubleValue > self.finalAmountTenderedMultiPay.doubleValue)
            let amountDue = enoughAmount ? NSDecimalNumber.init(string:"0.00") : ModelController.shared().currentCompleteOrder.total.subtracting(self.finalAmountTenderedMultiPay)
            
            let amountStr = String(format: "%.2f", self.finalAmountTenderedMultiPay.doubleValue).addCurrency()
            let amountDueStr = String(format: "%.2f", amountDue.doubleValue).addCurrency()
            
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        let selectedBackColor = enoughAmount ? UIColor.init(color: .ebizuDarkGreen) : UIColor.groupTableViewBackground
                        let selectedTextColor = enoughAmount ? UIColor.white : UIColor.red
                        let selectedText = enoughAmount ? "PAY NOW - " + amountStr : "AMOUNT DUE - " + amountDueStr
                        
                        self.multiPay.backgroundColor = selectedBackColor
                        self.multiPay.isUserInteractionEnabled = enoughAmount
                        self.multiPay.setTitleColor(selectedTextColor, for: .normal)
                        self.multiPay.setTitle(selectedText, for: .normal)
                        
                    }, completion: nil)
            }
        }
    }
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        orderTableView.rowHeight = UITableView.automaticDimension
        orderTableView.estimatedRowHeight = 80.0
        orderTableView.register(UINib(nibName:"CustomOrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        selectedTableCollectionView.register(UINib(nibName:"CustomSelectedTableCell", bundle: nil), forCellWithReuseIdentifier: "SelectedTableCell")
        setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
//        EADSessionController.shared().closeSession()
//        notification.removeObserver(self, name: NSNotification.Name("EAAccessoryDidConnectNotification"), object: nil)
//        notification.removeObserver(self, name: NSNotification.Name("EAAccessoryDidDisconnectNotification"), object: nil)
//        notification.removeObserver(self, name: NSNotification.Name("EADSessionDataReceivedNotification"), object: nil)
    }
    
    //MARK: - IBAction method
    @IBAction func detailPressed()
    {
        detailShown = !detailShown
    }
    
    @IBAction func shortcutPressed(_ sender: UIButton)
    {
        if let safeText = sender.titleLabel?.text
        {
            numberEntered = safeText
        }
    }
    
    @IBAction func numberPressed(_ sender: UIButton)
    {
        if let safeText = sender.titleLabel?.text
        {
            numberEntered.append(safeText)
        }
    }
    
    @IBAction func clearPressed()
    {
        numberEntered = "0"
    }
    
    @IBAction func deletePressed()
    {
        if numberEntered.count > 0
        {
            let removedLastChar = numberEntered.substring(to: numberEntered.index(before: numberEntered.endIndex))
            numberEntered = removedLastChar
        }
        else
        {
            clearPressed()
        }
    }
    
    @IBAction func nonMultiPayPressed()
    {
        // checking credit card for both multipay and non-multipay. P.S. Disable this gavin suggested
//        if currentSelectedPaymentMethod.typeName == CARD
//        {
//            if creditCardTextField.text == ""
//            {
//                present_sclalert("Info", message: "Please enter credit card number to proceed", alert_style: .info)
//                return
//            }
//        }
        currentSelectedPaymentMethod.creditCardNo = creditCardTextField.text ?? ""
        
        if multiPayEnabled
        {
            if currentSelectedPaymentMethod.typeName != CASH
            {
                let afterAddAmount = finalAmountTenderedMultiPay.adding(finalAmountTendered)
                if afterAddAmount.doubleValue > ModelController.shared().currentCompleteOrder.total.doubleValue
                {
                    present_sclalert("Info", message: "No Change allowed for non-cash payment method", alert_style: .info)
                    return
                }
            }
            
            currentSelectedPaymentMethod.amountTendered = currentSelectedPaymentMethod.amountTendered.adding(finalAmountTendered)
            
            switch currentSelectedPaymentMethod.typeName
            {
            case CASH,CARD,CHEQUE:
                addAmountComplete()
            case MAYBANKQR:
                sendAmountToQrPage()
            default:
                sendAmountToExternalPayment()
            }
        }
        else
        {
            paymentComplete()
        }
    }
    
    @IBAction func multiPayPressed()
    {
        paymentComplete()
    }
    
    @IBAction func multipaymentSwitched(_ sender: UISwitch)
    {
        multiPayEnabled = sender.isOn
        setupPaymentData()
        paymentOptionCollectionView.reloadData()
    }
    
    @IBAction func cancelPayment()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelSelectedPaymentMethod()
    {
        setupPaymentView()
    }
    
    
    //MARK: - UICollection Delegate Method
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        switch collectionView
        {
        case selectedTableCollectionView:
            return selectedTableArray.count
        default: //payment method cell
            return paymentMethodArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        switch collectionView
        {
            
        case selectedTableCollectionView:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectedTableCell", for: indexPath)
            
            if let selectedTableCell = cell as? SelectedTableCell
            {
                selectedTableCell.name.text = selectedTableArray[indexPath.row]
                
                return selectedTableCell
            }
            
            return cell
            
        default: //payment method cell
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewPaymentCollectionCell", for: indexPath)
            
            if let newPaymentCollectionCell = cell as? NewPaymentCollectionCell
            {
                let data = paymentMethodArray[indexPath.row]
                
                guard
                    let paymentId = data["id"],
                    let paidAmountStr = data["paid_amount"],
                    let paymentName = data["pay_name"],
                    let paymentType = data["type_id"],
                    let paymentTypeName = data["type_name"],
                    let creditCardNumber = data["credit_card_no"]
                    else { print("error on \(#function), \(#line) "); return newPaymentCollectionCell}
                
                let paidAmount = NSDecimalNumber.init(string: paidAmountStr)
                
                newPaymentCollectionCell.data = paymentMethodData.init(id: paymentId, name: paymentName, creditCardNo: creditCardNumber, type: paymentType, typeName: paymentTypeName.uppercased(), amountTendered: paidAmount)
                newPaymentCollectionCell.clearButton.tag = indexPath.row
                newPaymentCollectionCell.clearButton.addTarget(self, action: #selector(clearSelectedPaymentMethodTenderedAmount(sender:)), for: .touchUpInside)
                
                return newPaymentCollectionCell
            }
            return cell
        }
    }
    
    @objc func clearSelectedPaymentMethodTenderedAmount(sender: UIButton)
    {
        var tempDict = paymentMethodArray[sender.tag]
        tempDict["paid_amount"]     = "0.00"
        tempDict["credit_card_no"]  = ""

        paymentMethodArray[sender.tag] = tempDict
        finalAmountTenderedMultiPay = NSDecimalNumber.init(value:paymentMethodArray.compactMap({ Double($0["paid_amount"] ?? "0.0")}).reduce(0, +))
        paymentOptionCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == paymentOptionCollectionView
        {
            if let newPaymentCollectionCell = collectionView.cellForItem(at: indexPath) as? NewPaymentCollectionCell
            {
                currentSelectedPaymentMethod = newPaymentCollectionCell.data
                paymentIcon.setImage(newPaymentCollectionCell.getPaymentLogo(), for: .normal)
                
                if multiPayEnabled || currentSelectedPaymentMethod.typeName == CASH || currentSelectedPaymentMethod.typeName == CARD || currentSelectedPaymentMethod.typeName == CHEQUE
                {
                    if multiPayEnabled
                    {
                        print(currentSelectedPaymentMethod.typeName)
                        if currentSelectedPaymentMethod.typeName != CASH && currentSelectedPaymentMethod.typeName != CARD && currentSelectedPaymentMethod.typeName != CHEQUE
                        {
                            if currentSelectedPaymentMethod.amountTendered.doubleValue > 0 //aldy tendered
                            {
                                present_sclalert("Info", message: "Unable to modified tendered amount for external payment method", alert_style: .info)
                                return
                            }
                        }
                    }
                    
                    clearPressed()
                    payMethodName.text = currentSelectedPaymentMethod.name
                    creditCardView.isHidden = currentSelectedPaymentMethod.typeName != CARD && currentSelectedPaymentMethod.typeName != CHEQUE
                    if !creditCardView.isHidden
                    {
                        if currentSelectedPaymentMethod.typeName == CARD
                        {
                            creditCardTextField.placeholder = "Card Number / Approval Code"
                        }
                        else if currentSelectedPaymentMethod.typeName == CHEQUE
                        {
                            creditCardTextField.placeholder = "Cheque Number"
                        }
                    }
                    creditCardTextField.text = currentSelectedPaymentMethod.creditCardNo
                    multiPaySwitchView.isHidden = true
                    
                    let cardAndNonMulti = (currentSelectedPaymentMethod.typeName == CARD || currentSelectedPaymentMethod.typeName == CHEQUE)
                        && !multiPayEnabled
                    nonMultiPayViewTopConstraint.constant = cardAndNonMulti ? -10 : 0
                    amountShortcutView.isHidden = cardAndNonMulti
                    amountKeypadView.isHidden = cardAndNonMulti
                    if cardAndNonMulti
                    {
                        finalAmountTendered = ModelController.shared().currentCompleteOrder.total
                    }
                    
                    paymentOptionView.removeFromSuperview()
                }
                else if currentSelectedPaymentMethod.typeName == MAYBANKQR
                {
                    finalAmountTendered = ModelController.shared().currentCompleteOrder.total
                    sendAmountToQrPage()
                }
                else //External type AND not multiple payment
                {
                    finalAmountTendered = ModelController.shared().currentCompleteOrder.total
                    sendAmountToExternalPayment()
                }
            }
        }
    }
    
    //MARK: - UITableView Delegate Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if let itemArray = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
        {
            return itemArray.count
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath)
        
        if let orderCell = cell as? OrderCell
        {
            if let itemArray = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
            {
                let item = itemArray[indexPath.row]
                
                orderCell.backgroundColor = UIColor.clear
                
                orderCell.itemName.text = item.itemName.capitalized
                if item.type_of_price == "Unit Price"
                {
                    orderCell.itemName.text = String(format:"%@ @ %.2f%@", item.itemName.capitalized, item.uom_qty.doubleValue, item.type_of_unit)
                }
                
                orderCell.qty.text = String(format:"%d",item.itemQuantity.intValue)
                orderCell.qty.isUserInteractionEnabled = false
                
                orderCell.total.text = String(format: "%.2f", item.totalPrice_bfr_bill_disc.doubleValue)
                orderCell.remarks.text = item.remarks
                orderCell.modifier.text = item.modifierLabel
                
                orderCell.discount.text = ""
                if item.discount.doubleValue > 0.0 && item.is_edited_item_price != "price_edited"
                {
                    orderCell.discount.text = "Discount @ " + String(format:"%.2f",item.discount.multiplying(by: item.itemQuantity).doubleValue).addCurrency()
                }
                
                orderCell.gst.text = ""
//                if item.gst_rate.doubleValue > 0.0 && SessionData.shared().gst_enable
//                {
//                    orderCell.gst.text = String(format:"SST %@ %@%% @ ", item.gst_code == "SV" ? "" : item.gst_code, item.gst_rate) + String(format : "%.2f",item.gst_value_bfr_bill_disc.doubleValue).addCurrency()
//                }
                
                orderCell.plus.isHidden = true
                orderCell.minus.isHidden = true
            }
        }
        return cell
    }
    
    //MARK: - UITextField Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if currentSelectedPaymentMethod.typeName == CARD
        {
            customKeyboard?.textfield = textField
            customKeyboard?.customKeyboardType = .intNumber
        }
        else
        {
            textField.inputView = nil
            textField.reloadInputViews()
        }
    }
}
