//
//  POSController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//


import Foundation

extension Pos
{
    //MARK: - Get Basic Value Method
    func getOrderNo() -> String
    {
        guard
            let safeHardwareCode = userDefaults.string(forKey: HARDWARE_CODE),
            let safeHoldOrderNo = userDefaults.string(forKey: HOLD_ORDER_NO)
            else { print("error on \(#function), \(#line) "); return ""}

        return safeHardwareCode+"-"+safeHoldOrderNo
    }
    
    //MARK: - Initiate Method
    func setupData()
    {
        setupCategoryData()
        refreshUserDefaultData()
    }
    
    func setupCategoryData()
    {
        if let safeCategoriesArray = shareddb.shared().get_product_categories() as? [[String:AnyObject]]
        {
            categoriesArray = safeCategoriesArray
            categoryCollectionView.reloadData()
        }
    }

    func setupView()
    {
        setupPinForm()
        setupShiftClosedView()
    }
    
    func setupPinForm()
    {
        if let pinKeyedIn = appDelegate?.pin_validated
        {
            if !pinKeyedIn && accountSubscriptionStatus != "2" && accountSubscriptionStatus != "3"
            {
                navigateToPinform()
            }
        }
    }

    func setupShiftClosedView()
    {
        view.bringSubviewToFront(closeShiftView)

        refreshCloseShiftView()
    }

    func registerUserForCrashlytic()
    {
        Crashlytics.sharedInstance().setUserIdentifier(StringUtils.get_business_detail("business_id"))
        Crashlytics.sharedInstance().setUserName(StringUtils.get_business_detail("business_name"))
        Crashlytics.sharedInstance().setUserEmail(StringUtils.get_business_detail("business_email"))
    }
    
    //MARK: - Re-Initiate Method
    func refreshPOSView()
    {
        wholeRightOrderView.isHidden = ModelController.shared().currentCompleteOrder.hold_order_id == nil
        
        if !wholeRightOrderView.isHidden
        {
            orderLabel.text =  "Order # " + (ModelController.shared().currentCompleteOrder.hold_order_id ?? "")
            orderType.setTitle(ModelController.shared().currentCompleteOrder.order_type == 0 ? "Dine In" : "Take Away", for: .normal)
        }
        else
        {
            let holdOrderArr = shareddb.shared().get_all_on_hold_orders()
            currentHoldOrder.setTitle("Hold order : \(holdOrderArr?.count ?? 0)", for: .normal)
            clearCustomerDisplay()
        }
    
        detailsShown = false
        
        setTableView()
        setCustomerIconName()
        setSalesPerson()
    }
    
    func refreshPOSData()
    {
        ModelController.shared().currentCompleteOrder = CompleteOrder()
        ModelController.shared().currentCustomer = Customer()
        refreshPOSView()
    }
    
    func refreshCloseShiftView()
    {
        closeShiftView.isHidden = userDefaults.bool(forKey: SHIFT_OPENED)
        if !closeShiftView.isHidden
        {
            guard
                let shiftData = shareddb.shared().get_last_shift_time_n_date() as? [String:AnyObject],
                let lastShiftTime = shiftData["last_shift"] as? String
                else { print("error on \(#function), \(#line) "); return}
            
            lastShiftLabel.text = lastShiftTime == "" ? "" : "Last Shift Closed at - \(lastShiftTime)"
        }
    }
    
    func refreshUserDefaultData()
    {
        shareddb.shared().get_all_paymethod()
        shareddb.shared().get_hold_order_number_n_order_count()
        shareddb.shared().get_transaction_number()
        shareddb.shared().get_current_balance()
        shareddb.shared().get_header_footer()
    }
    
    //MARK: - Navigation method
    
    func navigateToCustomInput()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "showCustomInput", sender: self)
        }
    }
    
    func navigateToPaymentOption()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "showNewPaymentOption", sender: self)
        }
    }
    
    func navigateToPinform()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "showPin", sender: self)
        }
    }

    func navigateToCustomerDisplay()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "showCustomerDisplay", sender: self)
        }
    }
    
    func navigateToCustomerGstDetails()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "showCustomerGSTDetail", sender: self)
        }
    }
    
    func navigateToAccountSuspended()
    {
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "AccountSuspended", sender: self)
        }
    }
    
    //MARK: - reacability Method
    @objc func checkReachabilityStatus()
    {
        guard
            let r = reachability
            else { return }
        
        noConnectivityStatus.isHidden = r.isReachable
    }
    
    //MARK: - P2P communication Method
    @objc func didReceiveDataWithNotification(notif:Notification)
    {
        guard
            let receivedData = notif.userInfo?["data"] as? Data,
            let dict = NSKeyedUnarchiver.unarchiveObject(with: receivedData) as? [String:AnyObject],
            let commandRawValue = dict[P2PConnection.P2PCommand] as? Int,
            let command = P2PConnection.P2PCommandList(rawValue: commandRawValue),
            let rating = dict["review"] as? String,
            let trxId = dict["trxId"] as? String,
            let email = dict["email"] as? String
            else { print("error on \(#function), \(#line) "); return}

        if command == .CD_FEEDBACK
        {
            shareddb.shared().update_customer_feedback(trxId, rating: rating)
            if email != ""
            {
                shareddb.shared().add_pending_email(email, withTrxId: trxId)
            }
            print("database updated ヽ(　･∀･)ﾉ")
        }
    }
    
    func sendOrderToCustomerDislay()
    {
        if ModelController.shared().currentCompleteOrder.hold_order_id != nil
        {
            var data = [String:AnyObject]()
            data["CompleteOrder"] = ModelController.shared().currentCompleteOrder.dictionaryRepresentation() as AnyObject
            data[P2PConnection.P2PCommand] = P2PConnection.P2PCommandList.APPEND_CD.rawValue as AnyObject
            
            P2PConnection.sharedP2PConnection().sendDataToPeers(data: data as AnyObject)
        }
    }
    
    func clearCustomerDisplay()
    {
        var data = [String:AnyObject]()
        data[P2PConnection.P2PCommand] = P2PConnection.P2PCommandList.CLEAR_CD.rawValue as AnyObject
        
        P2PConnection.sharedP2PConnection().sendDataToPeers(data: data as AnyObject)
    }
    
    //MARK: - Set Account Suspended Method
    func setAccountSuspendedOrTerminatedView()
    {
        accountSubscriptionStatus = userDefaults.string(forKey: ACC_STATUS) ?? ""
        if accountSubscriptionStatus == "2" || accountSubscriptionStatus == "3"
        {
            navigateToAccountSuspended()
        }
    }
    
    func checkAccountSubscriptionWithTimeFrame()
    {
        DispatchQueue.global(qos: .background).async
            {
                let lastSubcriptionCheckDouble = userDefaults.double(forKey: LAST_ACC_STATUS_CHECK)
                let lastSubcriptionCheck = Date(timeIntervalSince1970: lastSubcriptionCheckDouble)
                let currentDate = Date()
                
                if currentDate.timeIntervalSince(lastSubcriptionCheck) > 86400.0 //If more than 1 days
                {
                    Networking.shared().getData(forReading: .readingAccountSubscriptionStatus)
                }
            }
    }
    
    //MARK: - Set Sales Person Method
    func setSalesPerson()
    {
        if ModelController.shared().currentCompleteOrder.sales_person_id == nil ||
            ModelController.shared().currentCompleteOrder.sales_person_id == "" ||
            ModelController.shared().currentCompleteOrder.sales_person_name == nil ||
            ModelController.shared().currentCompleteOrder.sales_person_name == ""
        {
            currentSalesPerson.text = ""
        }
        else
        {
            currentSalesPerson.text = "  Sales by : \(ModelController.shared().currentCompleteOrder.sales_person_name ?? "")  "
        }
    }
    
    //MARK: - Set Table Method
    func setTableView()
    {
        dineTypeActive = SessionData.shared().dine_in_enable
        setTableNo()
    }
    
    func setTableNo()
    {
        var tableExist = false
        if ModelController.shared().currentCompleteOrder.table_no != nil
        {
            if ModelController.shared().currentCompleteOrder.table_no != ""
            {
                noOfPax.setTitle(ModelController.shared().currentCompleteOrder.no_of_pax.description, for: .normal)
                selectedTableArray = [String]()
                selectedTableArray.append(ModelController.shared().currentCompleteOrder.table_no)
                if let allSelectedTable = shareddb.shared().getAllTableNameThatMerge(into: ModelController.shared().currentCompleteOrder.tbl_primary_id) as? [String]
                {
                    selectedTableArray += allSelectedTable
                }
                tableCollectionView.reloadData()
                tableExist = true
            }
        }
        
        tableSelected = tableExist
    }
    
    func insertSelectedTableNo()
    {
        if ModelController.shared().currentCompleteOrder.table_no != nil
        {
            if ModelController.shared().currentCompleteOrder.table_no != ""
            {
                let previousTable = shareddb.shared().getTableId(withOrderId: ModelController.shared().currentCompleteOrder.hold_order_id) ?? ""
                
                if ModelController.shared().currentCompleteOrder.tbl_primary_id != previousTable //if change table
                {
                    shareddb.shared().updateTableOrderId(ModelController.shared().currentCompleteOrder.hold_order_id,
                                                         fromTableId: previousTable,
                                                         intoTableId: ModelController.shared().currentCompleteOrder.tbl_primary_id)
                }
            }
        }
    }
    
    func removeOrderFromTable()
    {
        var tableDetails = [String:String]()
        tableDetails["total_amount"] = ""
        tableDetails["order_id"] = ""
        tableDetails["table_no"] = ModelController.shared().currentCompleteOrder.table_no
        
        shareddb.shared().update_table_amount_orderid(tableDetails)
        shareddb.shared().remove_merge_into_column_(with_id: ModelController.shared().currentCompleteOrder.tbl_primary_id)
    }
    
    //MARK: - Set Customer Method
    func customerDidExist() -> Bool
    {
        print("customerId : \(ModelController.shared().currentCustomer.customer_id)")

        return !(ModelController.shared().currentCustomer.customer_id == "0" || ModelController.shared().currentCustomer.customer_id == nil)
    }

    func setCustomerIconName()
    {
        let customerExist = customerDidExist()
        let nameExist = ModelController.shared().currentCustomer.customerFullName != nil

        customerName.isUserInteractionEnabled = !customerExist
        customerName.text = nameExist ? ModelController.shared().currentCustomer.customerFullName : "Walk In"
        customerImage.setBackgroundImage(customerExist ? ModelController.shared().currentCustomer.storedProfilePicUIImage : UIImage.init(named: CUSTOMER_DEFAULT_PIC), for: .normal)
    }
   
    //MARK: -  Set Item to OrderArray Method
    func selectedProduct(product: Product)
    {
        let hasModifier = shareddb.shared().is_modifier_exist(product.item_id)
        let modifiers = hasModifier ? shareddb.shared().get_all_modifiers("", withModItemID: product.item_id) : NSMutableDictionary()
        
        let order = OrderItem()
        order.itemName      = product.item_name
        order.itemImage     = product.item_image;
        order.itemQuantity  = NSDecimalNumber.init(string:"1")
        order.itemPrice     = NSDecimalNumber.init(string: product.price)
        order.itemID        = product.item_id
        order.itemCatID     = product.item_cat_id
        order.itemID        = product.item_id
        order.cost_price    = NSDecimalNumber.init(string:product.cost_price)
        order.selling_price = NSDecimalNumber.init(string:product.selling_price)
        order.type_of_price = product.type_of_price != nil ? product.type_of_price : ""
        order.type_of_unit  = product.type_of_unit != nil  ? product.type_of_unit : ""
        order.gst_rate      = NSDecimalNumber.init(string: product.gst_rate)
        order.gst_code      = product.gst_code;
        order.allModifiersDict  = modifiers as! [AnyHashable : Any]

        if let stock = Int(product.stock_left)
        {
            order.stock_left = NSNumber.init(value:stock)
        }
        if let stockMonitor = Int(product.stock_monitoring)
        {
            order.stock_monitoring = NSNumber.init(value:stockMonitor)
        }
        if let catId = Int(product.item_cat_id)
        {
            order.category_id = NSNumber.init(value: catId)
        }
        
        addItemToOrderArray(newItem: order)
    }
    
    func addItemToOrderArray(newItem:OrderItem)
    {
        // initiate for first item
        if ModelController.shared().currentCompleteOrder.hold_order_id == nil
        {
            ModelController.shared().currentCompleteOrder.hold_order_id = getOrderNo()
            ModelController.shared().currentCompleteOrder.currency = "".getCurrencyCode()
            ModelController.shared().currentCompleteOrder.user_id = StringUtils.get_user_detail("user_id")
            ModelController.shared().currentCompleteOrder.user_name = StringUtils.get_user_detail("name")
            ModelController.shared().currentCompleteOrder.order_type = 0
            ModelController.shared().currentCompleteOrder.isOrderOnHold = false
            ModelController.shared().currentCompleteOrder.create_time = String(format:"%.0f",NSDate().timeIntervalSince1970)
            ModelController.shared().currentCompleteOrder.voucher_amount = NSDecimalNumber.init(string:"0.00")
            ModelController.shared().currentCompleteOrder.discount_amount = NSDecimalNumber.init(string:"0.00")
            ModelController.shared().currentCompleteOrder.orderItemsArray = [OrderItem]()
            
            shareddb.shared().increase_hold_order_number()
            
            refreshPOSView()
        }
        
        if !billDiscountExist()
        {
            if !itemAldyExistOnOrderArray(newItem: newItem)
            {
                ModelController.shared().currentCompleteOrder.orderItemsArray.append(newItem)
                setOrderChargeDetails()
                
                var ShowItemDetails = newItem.type_of_price != "Fixed Price"
                
                if let modifier = newItem.allModifiersDict as? [String:AnyObject] // MODIFIER
                {
                    // CHOICE
                    if let _ = modifier[CHOICE] as? [[String:AnyObject]]
                    {
                        ShowItemDetails = true
                    }
                }
                
                if ShowItemDetails
                {
                    tableView(orderTableView, didSelectRowAt: IndexPath.init(row: ModelController.shared().currentCompleteOrder.orderItemsArray.count - 1, section: 0))
                }
            }
        }
    }
    
    func setOrderChargeDetails(_ scrollToBottom: Bool = true)
    {
        DispatchQueue.main.async{
    
            ModelController.shared().currentCompleteOrder.newCalculate()

            self.serviceCharge.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.tax_amount.doubleValue)
            self.discount.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.discount_amount.doubleValue)
            self.voucher.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.voucher_amount.doubleValue)
            self.gst.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.total_gst_value.doubleValue)
            self.roundingAmount.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.rounding_amount.doubleValue)
            self.pay.setTitle("PAY " + String(format:"%.2f",ModelController.shared().currentCompleteOrder.total.doubleValue).addCurrency(), for: .normal)
            
            self.sendOrderToCustomerDislay()
            self.orderTableView.reloadData()
            if scrollToBottom {
                self.scrollOrderTableRowToBottom()
            }
        }
    }

    func deleteOrderItemAt(Index: Int)
    {
        if let orderItemArr = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
        {
            let orderItem = orderItemArr[Index]
            
            if ModelController.shared().currentCompleteOrder.orderItemsArray.count == 1
            {
                cancelOrderAndItemAlert()
            }
            else
            {
                if orderItem.hold_item_id != nil
                {
                    shareddb.shared().deleteOrderItem(onHold: orderItem.hold_item_id)
                }
                ModelController.shared().currentCompleteOrder.orderItemsArray.remove(at: Index)
                
                setOrderChargeDetails(false)
            }
        }
    }
    
    func itemAldyExistOnOrderArray(newItem: OrderItem) -> Bool
    {
        if addNewLineItem // if add new line straightly add item no need to check for existing item
        {
            addNewLineItem = false // reset the value back 'false' by default
        }
        else if let safeOrderItemsArray = ModelController.shared().currentCompleteOrder.orderItemsArray as? [OrderItem]
        {
            if let index = safeOrderItemsArray.index(where: {$0.itemID == newItem.itemID})
            {
                let existItem = safeOrderItemsArray[index]
                
                if let modifier = newItem.allModifiersDict as? [String:AnyObject]
                {
                    if !(modifier.count > 0) // has no Modifier
                    {
                        if existItem.type_of_price == "Fixed Price" && existItem.itemQuantity.intValue < 999
                        {
                            existItem.itemQuantity = existItem.itemQuantity.adding(NSDecimalNumber.init(value: 1))
                            ModelController.shared().currentCompleteOrder.orderItemsArray[index] = existItem
                            setOrderChargeDetails(false)
                            scrollOrderToIndex(index)
                            return true
                        }
                    }
                }
            }
        }
        return false
    }
    
    func billDiscountExist() -> Bool
    {
        if ModelController.shared().currentCompleteOrder.discount_amount.doubleValue > 0
        {
            self.present_alert("Info", message: "Please clear 'Bill Discount' first before continue re-edit the order")
            return true
        }
        return false
    }
    
    func finalizeSelectedDataToCurrentCompleteOrder()
    {
        ModelController.shared().currentCompleteOrder.discount_percentage = 0
        ModelController.shared().currentCompleteOrder.offer_sn = ""
        ModelController.shared().currentCompleteOrder.order_type = orderType.titleLabel?.text == "Dine In" ? 0 : 1
        ModelController.shared().currentCompleteOrder.trans_number = userDefaults.string(forKey: TRANSACTION_NO)
        ModelController.shared().currentCompleteOrder.counter_number = Int32(Int(userDefaults.integer(forKey: ORDER_COUNT)))
        ModelController.shared().currentCompleteOrder.trx_gst_mode = SessionData.shared().gst_enable ? "en_sst" : "non_sst"

        // insert customer data to hold order
        ModelController.shared().currentCompleteOrder.checkin_id = ModelController.shared().currentCustomer.checkin_id ?? ""
        ModelController.shared().currentCompleteOrder.member_id = ModelController.shared().currentCustomer.member_id ?? "0"
        ModelController.shared().currentCompleteOrder.customer_id = ModelController.shared().currentCustomer.customer_id ?? "0"
        ModelController.shared().currentCompleteOrder.customer_name = customerDidExist() ? ModelController.shared().currentCustomer.customerFullName : (customerName.text ?? "")
        ModelController.shared().currentCompleteOrder.customer_profile_pic = ModelController.shared().currentCustomer.profilePic ?? ""
        ModelController.shared().currentCompleteOrder.customer_email = ModelController.shared().currentCustomer.email ?? ""
        ModelController.shared().currentCompleteOrder.customer_phone = ModelController.shared().currentCustomer.mobile ?? ""
        ModelController.shared().currentCompleteOrder.customer_city = ModelController.shared().currentCustomer.city ?? ""
        ModelController.shared().currentCompleteOrder.customer_pincode = ModelController.shared().currentCustomer.postCode ?? ""
        ModelController.shared().currentCompleteOrder.customer_address1 = ModelController.shared().currentCustomer.address ?? ""
        
        //left out 2 value below that current customer dont have
        //ModelController.shared().currentCompleteOrder.customer_gstid = ?
        //ModelController.shared().currentCompleteOrder.customer_address2 = ?
    }
    
    //MARK: - Hold Order Method
    func holdOrderFunction()
    {
        if ModelController.shared().currentCompleteOrder.hold_order_id != nil
        {
            putOrderOnHold()
            refreshPOSData()
        }
    }
    
    func putOrderOnHold()
    {
        finalizeSelectedDataToCurrentCompleteOrder()
        
        if ModelController.shared().currentCompleteOrder.isOrderOnHold
        {
            if !ModelController.shared().currentCompleteOrder.from_split_bill
            {
                shareddb.shared().updateOrder(onHold:ModelController.shared().currentCompleteOrder.copy() as! CompleteOrder)
            }
        }
        else
        {
            shareddb.shared().insertOrder(onHold: ModelController.shared().currentCompleteOrder.copy() as! CompleteOrder)
        }
        
        insertSelectedTableNo()
    }
    
    func selectedHoldOrderID(orderID: String, fromSplitBill: Bool)
    {
        holdOrderFunction()
        
        ModelController.shared().currentCompleteOrder = shareddb.shared().get_hold_order_(with_id: orderID, andSplitBill: fromSplitBill)
        
        //transfer customer details from hold order
        ModelController.shared().currentCustomer.checkin_id = ModelController.shared().currentCompleteOrder.checkin_id
        ModelController.shared().currentCustomer.member_id = ModelController.shared().currentCompleteOrder.member_id
        ModelController.shared().currentCustomer.customer_id = ModelController.shared().currentCompleteOrder.customer_id
        ModelController.shared().currentCustomer.first_name = ModelController.shared().currentCompleteOrder.customer_name
        ModelController.shared().currentCustomer.profilePic = ModelController.shared().currentCompleteOrder.customer_profile_pic
        ModelController.shared().currentCustomer.email = ModelController.shared().currentCompleteOrder.customer_email
        ModelController.shared().currentCustomer.mobile = ModelController.shared().currentCompleteOrder.customer_phone
        ModelController.shared().currentCustomer.city = ModelController.shared().currentCompleteOrder.customer_city
        ModelController.shared().currentCustomer.postCode = ModelController.shared().currentCompleteOrder.customer_pincode
        ModelController.shared().currentCustomer.address = ModelController.shared().currentCompleteOrder.customer_address1
   
        refreshPOSView()
        setOrderChargeDetails()
        
        if fromSplitBill
        {
            payNow()
        }
    }
    
    //MARK: - Cancel Order Method
    func cancelOrderAndItemAlert()
    {
        self.present_alert("Do you wish to cancel your current order",
                           message: "Press Ok to delete the item and order permanently",
                           yesTitle: "Ok", noTitle: "Cancel",
                           yesAlertActionCompletionHandler:
                            { (UIAlertAction) in
                                DispatchQueue.main.async {
                                    self.pinTextField.becomeFirstResponder()
                                }
                            },
                           noAlertActionCompletionHandler:nil)
    }

    private func reallyCancelOrder() {
        if ModelController.shared().currentCompleteOrder.hold_order_id != nil
        {
            shareddb.shared().deleteOrder(onHold: ModelController.shared().currentCompleteOrder, andSplitBill: ModelController.shared().currentCompleteOrder.from_split_bill)

            if ModelController.shared().currentCompleteOrder.table_no != nil && ModelController.shared().currentCompleteOrder.table_no != ""
            {
                self.removeOrderFromTable()
            }

            self.refreshPOSData()
        }
    }
    
    //MARK: - PayNow Method
    func payNowFunction()
    {
        if ModelController.shared().currentCompleteOrder.total.doubleValue > 500.0 && SessionData.shared().gst_enable
        {
            present_alert("Tax Invoice", message: "Would you like to include a full tax invoice?", yesTitle: "Yes", noTitle: "No",
                          yesAlertActionCompletionHandler:
                            { (UIAlertAction) in
                                
                                //[Printer_Form setCheck_receipt_type:@"gst_full_tax"];
                                self.navigateToCustomerGstDetails()
                            },
                          noAlertActionCompletionHandler:
                            {(UIAlertAction) in
                                
                                ModelController.shared().currentCompleteOrder.customer_gstid = ""
                                //[Printer_Form setCheck_receipt_type:@"gst_tax"];
                                self.navigateToPaymentOption()
                            })
        }
        else
        {
            ModelController.shared().currentCompleteOrder.customer_gstid = ""
            //[Printer_Form setCheck_receipt_type:@"non_gst"];

            guard
                let customField = shareddb.shared().get_custom_field_name() as? [String]
                else { print("error on \(#function), \(#line) "); return}

            if customField.first != ""
            {
                navigateToCustomInput()
            }
            else
            {
                navigateToPaymentOption()
            }
        }
    }
    
    func handlePaymentDone()
    {
        if ModelController.shared().currentCompleteOrder.hold_order_id != nil
        {
            shareddb.shared().deleteOrder(onHold: ModelController.shared().currentCompleteOrder, andSplitBill: ModelController.shared().currentCompleteOrder.from_split_bill)
            
            if ModelController.shared().currentCompleteOrder.table_no != nil && ModelController.shared().currentCompleteOrder.table_no != ""
            {
                removeOrderFromTable()
            }
            
            refreshPOSData()
            setAccountSuspendedOrTerminatedView()
            
            Networking.shared().putData(forWriting: .writingTransaction)
            Networking.shared().putData(forWriting: .writingEmail)
            Networking.shared().putData(forWriting: .writingSetting)
            
            checkAccountSubscriptionWithTimeFrame()
        }
    }
    
    //MARK: - ItemDetailsDelegate method
    func updateSelectedOrderItem()
    {
        setOrderChargeDetails(false)
    }

    //MARK: - CustomKeyboardDelegate method
    func pinStatus(success: Bool, textField: UITextField) -> Bool
    {
        if success
        {
            reallyCancelOrder()
        }
        else
        {
            present_sclalert("Error", message: "Invalid PIN Entered, please try again", alert_style: .error)
        }

        return true
    }
    
    //MARK: - segue method
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool
    {
        if identifier == "SelectSalesPerson"
        {
            if ModelController.shared().currentCompleteOrder.hold_order_id == nil
            {
                present_sclalert("No Order Detected", message: "Please create order before select any salesperson involved. \nThank you.", alert_style: .info)
                return false
            }
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showItemDetails"
        {
            guard
                let dvc = segue.destination as? ItemDetails,
                let possiblePopover = dvc.popoverPresentationController,
                let indexPath = sender as? IndexPath
                else { print("error on \(#function), \(#line) "); return}
            
            let selectedCellRect = orderTableView.rectForRow(at: indexPath)
            possiblePopover.sourceRect = CGRect.init(x: selectedCellRect.minX, y: selectedCellRect.midY, width: 0, height: 0)
            
            dvc.selectedOrderItem = selectedOrderItem
            dvc.selectedOrderIndex = indexPath.row
            dvc.delegate = self
        }
        else if segue.identifier == "openShift"
        {
            guard
                let dvc = segue.destination as? NewCashRegister
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.shiftAction = OPENSHIFT
        }
        else if segue.identifier == "showPrinterOption"
        {
            finalizeSelectedDataToCurrentCompleteOrder()
        }
    }
    
    @IBAction func posUpdateDataReturn(segue: UIStoryboardSegue)
    {
        if segue.identifier == "updateBillDiscount" || segue.identifier == "updateBillVoucher"
        {
            setOrderChargeDetails()
        }
        else if segue.identifier == "customInputReturn" || segue.identifier == "customerGstDetailReturn"
        {
            navigateToPaymentOption()
        }
        else if segue.identifier == "openShiftReturn"
        {
            guard
                let svc = segue.source as? NewCashRegister
                else { print("error on \(#function), \(#line) "); return}
            
            if svc.shiftAction == CLOSESHIFT
            {
                navigateToPinform()
            }
            
            setupShiftClosedView()
        }
        else if segue.identifier == "switchEmployee"
        {
            navigateToPinform()
        }
        else if segue.identifier == "paymentCompleteReturn"
        {
            handlePaymentDone()
        }
        else if segue.identifier == "salesPersonSelected"
        {
            setSalesPerson()
        }
        else if segue.identifier == "menuDismissed"
        {
            refreshPOSView()
        }
        else if segue.identifier == "TransactionFailToRecordReturn"
        {
            guard
                let svc = segue.source as? NewPaymentComplete
                else { print("error on \(#function), \(#line) "); return}

            DispatchQueue.main.async {
                    self.present_sclalert("DATABASE ERROR", message: svc.dbErrorMsg, alert_style: .error)
            }
        }
        else if segue.identifier == "holdOrderSelected"
        {
            guard
                let svc = segue.source as? HoldOrderForm
                else { print("error on \(#function), \(#line) "); return}

            selectedHoldOrderID(orderID: svc.selected_order_id, fromSplitBill: svc.from_split_bill)
        }
        else if segue.identifier == "tableOrderSelected"
        {
            guard
                let svc = segue.source as? TableManagement
                else { print("error on \(#function), \(#line) "); return}
            
            selectedHoldOrderID(orderID: svc.selectedOrderId, fromSplitBill: false)
        }
    }
}
