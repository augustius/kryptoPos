//
//  CustomerDisplayReview.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 27/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class CustomerDisplayReview: UITableViewController, UITextFieldDelegate
{
    @IBOutlet var payType: UILabel!
    @IBOutlet var amountDue: UILabel!
    @IBOutlet var amountTendered: UILabel!
    @IBOutlet var changeDue: UILabel!
    @IBOutlet var email: UITextField!
    @IBOutlet var badReview: UIButton!
    @IBOutlet var oklaReview: UIButton!
    @IBOutlet var goodReview: UIButton!
    
    var review = 0;
    
    //MARK: - view cycle
    override func viewDidLoad()
    {
        payType.text = ModelController.shared().currentCompleteOrder.pay_type_name
        amountDue.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.total.doubleValue)
        amountTendered.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.payment_amount.doubleValue)
        changeDue.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.payment_changes.doubleValue)
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - shared method
    func sendReview()
    {
        self.performSegue(withIdentifier: "sendReview", sender: self)
    }
    
    //MARK: - IBaction method
    @IBAction func reviewClicked(_ sender: UIButton)
    {
        switch sender
        {
        case badReview:
            review = 1
            break
        case oklaReview:
            review = 2
            break
        case goodReview:
            review = 3
            break
        default:
            break
        }
        
        sendReview()
    }
    
    @IBAction func noThanks()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextField delegate
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if textField == email
        {
            if !((textField.text?.isValidEmail())!)
            {
                textField.text = ""
                present_alert("Invalid Email", message: "Please enter a valid email, if you wish to recieve e-receipt. \nThank You.")
            }
        }
    }
    
    
    
    
}
