//
//  CustomerDisplaySetup.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 25/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class CustomerDisplaySetup: UIViewController,UITextFieldDelegate
{
    
    @IBOutlet var cdName: UITextField!
    @IBOutlet var setCD: UIButton!
    
    // MARK: - view cycle
    override func viewDidLoad()
    {
        setCD.setTitle( userDefaults.bool(forKey: CUSTOMER_DISPLAY) ? "Reset Customer Display".translate() : setCD.titleLabel?.text?.translate(), for: .normal)
        sendFormToGoogleAnalytic()
    }
    
    @IBAction func setCustomerDisplay()
    {
        if let name = cdName.text
        {
            if name != ""
            {
                userDefaults.set("[CD]" + name, forKey: CUSTOMER_DISPLAY_NAME)
                userDefaults.set(true, forKey: CUSTOMER_DISPLAY)
                self.performSegue(withIdentifier: "showCustomerDisplay", sender: self)
            }
            else
            {
                self.present_alert("Error", message: "Please fill-in the name that you wish to see upon searching for this Customer Display ")
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 5 // Bool
    }
}
