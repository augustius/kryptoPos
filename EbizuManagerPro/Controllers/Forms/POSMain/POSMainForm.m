//
//  POSMainForm.m
//  Ebizu Manager Pro
//
//  Created by Jamal on 6/28/15.
//  Copyright (c) 2015 Ebizu Sdn Bhd. All rights reserved.
//

#import "POSMainForm.h"

@interface POSMainForm ()

@end

@implementation POSMainForm

- (IBAction)cmd_sidemenu:(id)sender
{
    [[SlideNavigationController sharedInstance] toggleLeftMenu];
}

#pragma mark - SlideNavigationController Methods -
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return NO;
}

@end
