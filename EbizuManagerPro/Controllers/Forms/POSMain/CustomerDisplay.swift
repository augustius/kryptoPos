//
//  CustomerDisplay.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 25/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class CustomerDisplay: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    
    @IBOutlet var orderID: UILabel!
    @IBOutlet var subTotal: UILabel!
    @IBOutlet var serviceCharge: UILabel!
    @IBOutlet var discount: UILabel!
    @IBOutlet var voucher: UILabel!
    @IBOutlet var gst: UILabel!
    @IBOutlet var roundingAdjustment: UILabel!
    @IBOutlet var total: UILabel!
    @IBOutlet var orderView: UIView!
    @IBOutlet var orderTable: UITableView!
    
    var currency = ""
    let originalModifierHeight = 20.0
    let originalModifierY = 53.0
    let originalCellHeight = 79.0
    
    // MARK: - view cycle
    override func viewDidLoad()
    {
        orderTable.rowHeight = UITableView.automaticDimension
        orderTable.estimatedRowHeight = 80.0
        orderTable.register(UINib(nibName:"CustomOrderCell", bundle: nil), forCellReuseIdentifier: "OrderCell")
        setupP2P()
        sendFormToGoogleAnalytic()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        notification.addObserver(self, selector: #selector(didReceiveDataWithNotification), name: NSNotification.Name("MCDidReceiveDataNotification"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        notification.removeObserver(self, name: NSNotification.Name("MCDidReceiveDataNotification"), object: nil)
    }
    
    // MARK: - main shared method
    func setupP2P()
    {
        P2PConnection.sharedP2PConnection().setupCustomerDisplay()
    }
    
    func setupOrderDetails()
    {
        if ModelController.shared().currentCompleteOrder.hold_order_id != nil
        {
            orderID.text = "ORDER ID : \(ModelController.shared().currentCompleteOrder.hold_order_id.description)"
            subTotal.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.subtotal.doubleValue)
            serviceCharge.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.tax_amount.doubleValue)
            discount.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.discount_amount.doubleValue)
            voucher.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.voucher_amount.doubleValue)
            gst.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.total_gst_value.doubleValue)
            roundingAdjustment.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.rounding_amount.doubleValue)
            total.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.total.doubleValue)
            
            currency = ModelController.shared().currentCompleteOrder.currency
        }
        
        orderView.isHidden = ModelController.shared().currentCompleteOrder.hold_order_id == nil
    }
    
    // MARK: -IBAction method
    @IBAction func refreshP2Pconnection()
    {
        setupP2P()
    }
    
    // MARK: - tableview delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        setupOrderDetails()
        return ModelController.shared().currentCompleteOrder.hold_order_id != nil ? ModelController.shared().currentCompleteOrder.orderItemsArray.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath)
        
        if let customerDisplayCell = cell as? OrderCell
        {
            if ModelController.shared().currentCompleteOrder.orderItemsArray.count > 0
            {
                if let safeOrderItems = ModelController.shared().currentCompleteOrder.orderItemsArray[indexPath.row] as? [String : AnyObject]
                {
                    if let item = OrderItem.init(dictionary:safeOrderItems)
                    {
                        customerDisplayCell.qty.text        = String(format:"%d",item.itemQuantity.intValue)
                        customerDisplayCell.itemName.text   = (item.type_of_price == "Unit Price") ?
                                                                String(format:"%@ @ %.2f%@",item.itemName.capitalized,item.uom_qty.doubleValue,item.type_of_unit) :
                                                                item.itemName.capitalized
                        customerDisplayCell.total.text      = String(format:"%.2f",item.totalPrice_bfr_bill_disc.doubleValue)
                        customerDisplayCell.remarks.text    = item.remarks != "" ? String(format:"Remarks : %@",item.remarks) : ""
                        customerDisplayCell.discount.text   = (item.discount.doubleValue > 0.0 && !(item.is_edited_item_price == "price_edited")) ?
                                                                String(format:"Discount @ %@ %.2f",currency,item.discount.doubleValue) :
                                                                ""
                        customerDisplayCell.gst.text = ""
//                        customerDisplayCell.gst.text        = (item.gst_rate.doubleValue > 0.0) ?
//                                                                String(format:"SST %@ %@%% @ %@ %.2f",item.gst_code == "SV" ? "" : item.gst_code,item.gst_rate,currency,item.gst_value_bfr_bill_disc.doubleValue) :
//                                                                ""

                        customerDisplayCell.modifier.text   = item.modifierLabel
                        customerDisplayCell.plus.isHidden   = true
                        customerDisplayCell.minus.isHidden  = true
                    }
                }
            }
            
            return customerDisplayCell
        }
        
        return cell
    }
    
    // MARK: - Notification for P2P
    @objc func didReceiveDataWithNotification(notification: NSNotification)
    {
        // recieve completeOrder model here
        guard
            let receivedData = notification.userInfo?["data"] as? Data,
            let dict = NSKeyedUnarchiver.unarchiveObject(with: receivedData) as? [String : AnyObject],
            let commandRawValue = dict[P2PConnection.P2PCommand] as? Int,
            let command = P2PConnection.P2PCommandList(rawValue: commandRawValue)
            else { print("error on \(#function), \(#line) "); return}
        
        switch command
        {
        case .APPEND_CD:
            if let safeCompleteOrder = CompleteOrder.init(dictionary:dict["CompleteOrder"] as! [AnyHashable : Any])
            {
                 DispatchQueue.main.async
                    {
                        if (self.presentedViewController != nil)
                        {
                            self.dismiss(animated: true, completion: nil)
                        }
                        ModelController.shared().currentCompleteOrder = safeCompleteOrder
                        self.orderTable.reloadData()
                    }
            }
            break
        case .CLEAR_CD:
            DispatchQueue.main.async
                {
                    ModelController.shared().currentCompleteOrder = nil
                    self.orderTable.reloadData()
                }
            break
        case .PAYMENT_CD:
            if let safeCompleteOrder = CompleteOrder.init(dictionary:dict["CompleteOrder"] as! [AnyHashable : Any])
            {
                DispatchQueue.main.async
                    {
                        ModelController.shared().currentCompleteOrder = safeCompleteOrder
                        self.performSegue(withIdentifier: "showPaymentSummary", sender: self)
                    }
            }
            break
        default:
            break
        }
    }
    
    // MARK: - segue method
    @IBAction func customerDisplayReturn(segue: UIStoryboardSegue)
    {
        if segue.identifier == "resetCustomerDisplay"
        {
            setupP2P()
        }
        else if segue.identifier == "sendReview"
        {
            if let svc = segue.source as? CustomerDisplayReview
            {
                var data = [String:AnyObject]()
                data["trxId"] = ModelController.shared().currentCompleteOrder.trx_id as AnyObject
                data["review"] = svc.review.description as AnyObject
                data["email"] = svc.email.text as AnyObject
                data[P2PConnection.P2PCommand] = P2PConnection.P2PCommandList.CD_FEEDBACK.rawValue as AnyObject
                
                print("data here : \(data)")
                P2PConnection.sharedInstance.sendDataToPeers(data: data as AnyObject)

            }
        }
    }
    
}
