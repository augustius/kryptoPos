//
//  CustomerDetail.swift
//  Manager
//
//  Created by augustius cokroe on 16/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct CustomerLatestTransactionData{
    var id = ""
    var transID = ""
    var createdDate = ""
    var total = ""
}

class LatestTransactionCell:UITableViewCell
{
    @IBOutlet private var trxId: UILabel!
    @IBOutlet private var trxDate: UILabel!
    @IBOutlet private var trxTotal: UILabel!
    
    var data = CustomerLatestTransactionData() {
        didSet{
            trxId.text = data.transID
            trxDate.text = data.createdDate
            trxTotal.text = String(format:"%.2f",Double(data.total) ?? 0)
        }
    }
}

class CustomerDetail:UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var totalTransaction: UILabel!
    @IBOutlet var totalSales: UILabel!
    @IBOutlet var lastTransactionDate: UILabel!
    @IBOutlet var latestTransactionTableview: UITableView!
    
    var data = [[String:String]]()
    var currentCustomer = Customer()
    var hardwareCode = ""
    var selectedSales = CustomerLatestTransactionData()
    
    //MARK: - View cycle
    override func viewDidLoad()
    {
        latestTransactionTableview.rowHeight = UITableView.automaticDimension
        latestTransactionTableview.estimatedRowHeight = 38.0
        hardwareCode = userDefaults.string(forKey: HARDWARE_CODE) ?? ""
        
        if let safeData = shareddb.shared().getTransactionBased(onCustId: currentCustomer.customer_id) as? [[String:String]]
        {
            data = safeData
            totalTransaction.text = "\(data.count)"
            totalSales.text = data.count > 0 ? "\(data.compactMap({Double($0["total"]!)}).reduce(0, +))" :"0"
            lastTransactionDate.text = data.count > 0 ? data.first?["created_datetime"] ?? "" : "-"
        }
    }
    
    //MARK: - UITableViewDelegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LatestTransactionCell", for: indexPath)
        
        if let latestTransactionCell = cell as? LatestTransactionCell
        {
            let order = data[indexPath.row]

            guard
                let id = order["id"],
                let transNo = order["trans_no"],
                let createdDate = order["created_datetime"],
                let total = order["total"]
                else { print("error on \(#function), \(#line) "); return latestTransactionCell}

            let tempData = CustomerLatestTransactionData.init(id: id,
                                                              transID: hardwareCode + "-" + transNo,
                                                              createdDate: createdDate,
                                                              total: total)
            latestTransactionCell.data = tempData

            return latestTransactionCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let selectedCell = tableView.cellForRow(at: indexPath) as? LatestTransactionCell
        {
            selectedSales = selectedCell.data
            performSegue(withIdentifier: "showReceipt", sender: self)
        }
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showCustomerProfile"
        {
            guard
                let dvc = segue.destination as? AddCustomer
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.formUsage = .toEdit
            dvc.currentCustomer = currentCustomer
        }
        else if segue.identifier == "showReceipt"
        {
            guard
                let dvc = segue.destination as? CustomerDetailSales
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.selectedSales = selectedSales
        }
    }
    
}
