//
//  SettingsPrinter.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 05/09/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class SettingPrinterHeaderView: UICollectionReusableView
{
    @IBOutlet var title: UILabel!
}

class SettingPrinterOptionCell: UICollectionViewCell
{
    @IBOutlet private var type: UILabel!
    @IBOutlet private var name: UILabel!
    @IBOutlet private var tickedImage: UIButton!
    
    var ticked = false
    {
        didSet{
            self.tickedImage.isHidden = !self.ticked
        }
    }
    
    var printerDetailData:printerDetail = printerDetail()
    {
        didSet{
            if let receiptType = self.printerDetailData.receiptType.first
            {
                self.type.text = "\(receiptType)"
            }
            else
            {
                self.type.text = ""
            }
            self.name.text = self.printerDetailData.printerName
        }
    }
}

class SettingsPrinter:UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet var addPrinterButton: UIButton!
    @IBOutlet var printerCollectionView: UICollectionView!
    
    var billPrinter = [printerDetail]()
    var kitchenPrinter = [printerDetail]()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        if let getAllPrinters = shareddb.shared().get_list_of_printers() as? [[String : String]]
        {
            if getAllPrinters.count > 0
            {
                var tempPrinter = printerDetail()
                for dict in getAllPrinters
                {
                    var ipAddress = dict["ip_address"] ?? ""
                    var printerType = "Discovery Mode"
                    if ipAddress == "BT:mPOP" || ipAddress == "BT:Star Micronics"
                    {
                        printerType = "Bluetooth Printer"
                    } else if ipAddress.containIpAddress() {
                        printerType = "IP Printer"
                    } else if ipAddress.isXPrinter() {
                        printerType = "X Printer"
                        ipAddress.getXPrinterIp()
                    }

                    tempPrinter = printerDetail.init(printerId: dict["id"] ?? "",
                                                     printerType: printerType,
                                                     receiptType: dict["printer_type"] ?? "",
                                                     printerAddress: ipAddress,
                                                     printerName: dict["printer_name"] ?? "",
                                                     categoryToPrint: dict["category_id"] ?? "",
                                                     printerCopies: dict["print_copies"] ?? "",
                                                     defaultPrinter: dict["default_printer"] == "1" )
                    print(tempPrinter.receiptType)
                    if tempPrinter.receiptType == "Bill"
                    {
                        billPrinter.append(tempPrinter)
                    }
                    else if tempPrinter.receiptType == "Kitchen"
                    {
                        kitchenPrinter.append(tempPrinter)
                    }
                }
            }
        }
        sendFormToGoogleAnalytic()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        Networking.shared().putData(forWriting: .writingPrinter)
    }
    
    //MARK: - UICollectioView Delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return kitchenPrinter.count > 0 ? 2: 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return section == 0 ? billPrinter.count : kitchenPrinter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        let headerView = collectionView .dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "SettingPrinterHeaderView", for: indexPath)
        
        if kind == UICollectionView.elementKindSectionHeader
        {
            if let settingPrinterHeaderView = headerView as? SettingPrinterHeaderView
            {
                settingPrinterHeaderView.title.text = indexPath.section == 0 ? "Bill Printer" : "Kitchen Printer"
                
                return settingPrinterHeaderView
            }
        }
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SettingPrinterOptionCell", for: indexPath)
        
        if let settingPrinterOptionCell = cell as? SettingPrinterOptionCell
        {
            let printer = indexPath.section == 0 ? billPrinter[indexPath.row] : kitchenPrinter[indexPath.row]
            settingPrinterOptionCell.printerDetailData = printer
            settingPrinterOptionCell.ticked = printer.defaultPrinter
            
            return settingPrinterOptionCell
        }        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if let settingPrinterOptionCell = collectionView.cellForItem(at: indexPath) as? SettingPrinterOptionCell
        {
            performSegue(withIdentifier: "showPrinterDetail", sender: settingPrinterOptionCell)
        }
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showPrinterDetail"
        {
            guard
                let dvc = segue.destination as? SettingsPrinterDetails
                else { print("error on \(#function), \(#line) "); return}
            
            if let _ = sender as? UIButton
            {
                dvc.formUsageStatus = .add
                dvc.currentPrinterDetail = printerDetail.init(printerId: "",
                                                               printerType: "IP Printer",
                                                               receiptType: "",
                                                               printerAddress: "",
                                                               printerName: "",
                                                               categoryToPrint: "",
                                                               printerCopies: "",
                                                               defaultPrinter: true)
            }
            else if let cell = sender as? SettingPrinterOptionCell
            {
                dvc.formUsageStatus = .edit
                dvc.currentPrinterDetail = cell.printerDetailData
            }
        }
    }

    @IBAction func printerDetailDone(segue: UIStoryboardSegue)
    {
        guard
            let svc = segue.source as? SettingsPrinterDetails
            else { print("error on \(#function), \(#line) "); return}
        
        print(svc.currentPrinterDetail.receiptType)
        print(svc.currentPrinterDetail.printerName)
        let isBill = svc.currentPrinterDetail.receiptType == "Bill"
        
        if segue.identifier == "printerAdded"
        {
            var ipAddress = svc.currentPrinterDetail.printerAddress
            if svc.currentPrinterDetail.printerType == "X Printer" {
                ipAddress.markAsXPrinter()
            }

            let data = [
                        "ip_address" : ipAddress,
                        "printer_name" : svc.currentPrinterDetail.printerName,
                        "printer_type" : svc.currentPrinterDetail.receiptType,
                        "category_id" : svc.currentPrinterDetail.categoryToPrint,
                        "print_copies" : svc.currentPrinterDetail.printerCopies,
                        "default_printer" : svc.currentPrinterDetail.defaultPrinter ? "1" : "0"
                        ]
            
            svc.currentPrinterDetail.printerId = shareddb.shared().addPrinter(data)
            
            if isBill
            {
                billPrinter.append(svc.currentPrinterDetail)
            }
            else
            {
                kitchenPrinter.append(svc.currentPrinterDetail)
            }
        }
        else if segue.identifier == "printerEdited"
        {
            var ipAddress = svc.currentPrinterDetail.printerAddress
            if svc.currentPrinterDetail.printerType == "X Printer" {
                ipAddress.markAsXPrinter()
            }

            let data = [
                        "printer_id" : svc.currentPrinterDetail.printerId,
                        "ip_address" : ipAddress,
                        "printer_name" : svc.currentPrinterDetail.printerName,
                        "printer_type" : svc.currentPrinterDetail.receiptType,
                        "category_id" : svc.currentPrinterDetail.categoryToPrint,
                        "print_copies" : svc.currentPrinterDetail.printerCopies,
                        "default_printer" : svc.currentPrinterDetail.defaultPrinter ? "1" : "0"
                        ]
            
            shareddb.shared().editPrinter(data)
            
            if isBill
            {
                if let index = billPrinter.index(where: {$0.printerId == svc.currentPrinterDetail.printerId})
                {
                    billPrinter[index] = svc.currentPrinterDetail
                }
            }
            else
            {
                if let index = kitchenPrinter.index(where: {$0.printerId == svc.currentPrinterDetail.printerId})
                {
                    kitchenPrinter[index] = svc.currentPrinterDetail
                }
            }
        }
        else if segue.identifier == "printerDeleted"
        {
            shareddb.shared().delete_printer_(from_pos: svc.currentPrinterDetail.printerId)
            
            if isBill
            {
                if let index = billPrinter.index(where: {$0.printerId == svc.currentPrinterDetail.printerId})
                {
                    billPrinter.remove(at: index)
                }
            }
            else
            {
                if let index = kitchenPrinter.index(where: {$0.printerId == svc.currentPrinterDetail.printerId})
                {
                    kitchenPrinter.remove(at: index)
                }
            }
        }
        
        DispatchQueue.main.async {
            self.printerCollectionView.reloadData()
        }
    }
}
