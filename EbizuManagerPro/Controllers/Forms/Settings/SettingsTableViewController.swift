//
//  SettingsTableViewController.swift
//  EbizuManagerPro
//
//  Created by Daliso Ngoma on 14/02/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    @IBAction func menuButtonTouchUp(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
