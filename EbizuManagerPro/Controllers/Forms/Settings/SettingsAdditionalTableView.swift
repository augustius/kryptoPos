//
//  SettingsAdditionalTableView.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 05/09/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

protocol SettingsAdditionalTableViewDelegate: class {
    func updateReceiptPreview(view: SettingsAdditionalTableView)
}

class SettingsAdditionalTableView:UITableViewController,UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate
{
    @IBOutlet var printerLogo: UIButton!
    @IBOutlet var printerHeaderName: UITextField!
    @IBOutlet var showHeaderName: UISwitch!
    @IBOutlet var printerAddress: UITextView!
    @IBOutlet var showAddress: UISwitch!
    @IBOutlet var printerContactNumber: UITextField!
    @IBOutlet var showContactNumber: UISwitch!
    @IBOutlet var printerFooterMessage: UITextField!
    @IBOutlet var printerWifiName: UITextField!
    @IBOutlet var printerWifiPass: UITextField!
    @IBOutlet var forcePrintSwitch: UISwitch!
    @IBOutlet var printingOptionSwicth: UISwitch!
    
    weak var delegate: SettingsAdditionalTableViewDelegate?
    let imagePicker = NoAutorotateUIImagePickerController()

    //MARK: - View Cycle
    override func viewDidLoad()
    {
        // setup image picker
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        
        // get all userdefault to fill in value
        if let imageData = FileUtils.getDataFromDocDir(withPath: String(format: "%@/%@",POSImages,IMG_MODULES), fileName: "printer_logo", andExtn: "jpg")
        {
            let logo = UIImage(data: imageData)
            printerLogo.setTitle("", for: .normal)
            printerLogo.setBackgroundImage(logo, for: .normal)
        }
        
        if let headerFooterData = userDefaults.object(forKey: HEADER_FOOTER_DATA) as? [String : String]
        {
            printerHeaderName.text = (headerFooterData["merchant_name"] ?? "").filterSpecialChar()
            showHeaderName.isOn = SessionData.shared().enable_merchant_name
            printerAddress.text = (headerFooterData["merchant_address"] ?? "").filterSpecialChar()
            showAddress.isOn = SessionData.shared().enable_merchant_address
            printerContactNumber.text = (headerFooterData["merchant_phone"] ?? "").filterSpecialChar()
            showContactNumber.isOn = SessionData.shared().enable_merchant_phone
            
            printerFooterMessage.text = (headerFooterData["footer_message"] ?? "").filterSpecialChar()
            printerWifiName.text = (headerFooterData["wifi_name"] ?? "").filterSpecialChar()
            printerWifiPass.text = (headerFooterData["wifi_password"] ?? "").filterSpecialChar()
        }
        
        forcePrintSwitch.isOn = SessionData.shared().force_print_mode_enable
        printingOptionSwicth.isOn = userDefaults.object(forKey: PRINTING_OPTION) as? Bool ?? false
        
        delegate?.updateReceiptPreview(view: self)
    }
    
    //MARK: - IBAction method
    @IBAction func uploadLogo()
    {
        handlePrinterLogo()
    }
    
    @IBAction func switchToogled()
    {
        delegate?.updateReceiptPreview(view: self)
    }
    
    //MARK: - UITextfield delegate 
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        textField.text = textField.text?.filterSpecialChar()
        
        delegate?.updateReceiptPreview(view: self)
    }
    
    //MARK: - UITextview delegate
    func textViewDidEndEditing(_ textView: UITextView)
    {
        textView.text = textView.text.filterSpecialChar()
        
        delegate?.updateReceiptPreview(view: self)
    }
    
    //MARK: - UIImagePickerController delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let chosenImage = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        {
            printerLogo.setTitle("", for: .normal)
            printerLogo.setBackgroundImage(chosenImage, for: .normal)
            delegate?.updateReceiptPreview(view: self)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UIImage upload method
    func handlePrinterLogo()
    {
        if let _ = printerLogo.backgroundImage(for: .normal)
        {
            present_alert("Delete Profile Picture",
                          message: "Would you like to delete your current profile picture?",
                          yesAlertActionCompletionHandler:
                            { (UIAlertAction) in
                                
                                self.printerLogo.setTitle("Upload Logo", for: .normal)
                                self.printerLogo.setBackgroundImage(nil, for: .normal)
                                self.delegate?.updateReceiptPreview(view: self)
                            },
                        noAlertActionCompletionHandler:nil)

        }
        else
        {
            present_alert("Change Company Logo",
                          message: "Where would you like to get your company logo from?",
                          yesTitle: "Gallery",
                          noTitle: "Camera",
                          yesAlertActionCompletionHandler:
                            { (UIAlertAction) in
                    
                                self.imagePicker.sourceType = .savedPhotosAlbum
                                self.present(self.imagePicker, animated: true, completion: nil)
                            },
                          noAlertActionCompletionHandler:
                            { (UIAlertAction) in
                    
                            self.imagePicker.sourceType = .camera
                            self.present(self.imagePicker, animated: true, completion: nil)
                        })
        }
    }
    
    
//    private func selectGalleryImage() {
//        let pickerController = NoAutorotateUIImagePickerController()
//        pickerController.delegate = self
//        pickerController.allowsEditing = true
//        // DO NOT CHANGE THE SOURCE TYPE TILL PORTRAIT ORIENTATION IS SUPPORTED
//        pickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
//        present(pickerController, animated: true, completion: nil)
//    }
//    
//    fileprivate func saveImageLocally(_ image: UIImage)
//    {
//        let imageData = UIImageJPEGRepresentation(image, 0.80)
//        FileUtils.writeToDocuemntsDirectory(atPath: "\(POSImages)/\(IMG_MODULES)", fileData: imageData, withPrefix: "printer_logo", andExtension: "jpg")
//        // FIXME: Check if the line below is neccessary.
//        printerLogo.isEnabled = true
//    }
    
    

    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
