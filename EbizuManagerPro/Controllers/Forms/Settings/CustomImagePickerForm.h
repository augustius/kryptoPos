//
//  CustomImagePickerController.h
//  Ebizu Manager
//
//  Created by VinothV on 6/4/15.
//  Copyright (c) 2015 EBIZU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomImagePickerForm : UIImagePickerController

@property(strong , nonatomic)NSString *is_from;

@end
