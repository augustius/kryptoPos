//
//  IntercomForm.m
//  Ebizu Manager Pro
//
//  Created by Daliso Joshua Ngoma on 10/15/15.
//  Copyright © 2015 Ebizu Sdn Bhd. All rights reserved.
//

#import "IntercomForm.h"
#import <Intercom/Intercom.h>
#import "shareddb.h"
#import "StringUtils.h"

@interface IntercomForm () <UIAlertViewDelegate>

@property (nonatomic, assign) BOOL loggedIn;
@property (weak, nonatomic) IBOutlet UIButton *message_button;
@property (weak, nonatomic) IBOutlet UIButton *previous_messages_button;

@end

@implementation IntercomForm

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loggedIn = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"] != nil;
    
    NSString *email = [StringUtils get_business_detail:@"business_email"];
    if (email.length > 0)
    {
        //Start tracking the user with Intercom
        [Intercom registerUserWithEmail:email];

        //Save email so we know the user is logged in
        [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.loggedIn = YES;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self set_translation];
}

- (void)set_translation
{
    [self.message_button setTitle:[StringUtils get_language_label:@"Talk to Support"] forState:UIControlStateNormal];
    [self.previous_messages_button setTitle:[StringUtils get_language_label:@"Past Messages"] forState:UIControlStateNormal];
}

- (void)setLoggedIn:(BOOL)loggedIn
{
    _loggedIn = loggedIn;
    self.message_button.enabled = loggedIn;
    self.previous_messages_button.enabled = loggedIn;
}

- (IBAction)new_message
{
    [Intercom presentMessageComposer];

}

- (IBAction)previous_messages
{
    [Intercom presentConversationList];
}

// If an intercom logout button is to be provided, uncomment the code below. Do refer to the latest documentation if the code doesn't work.

//- (IBAction)logoutPressed:(id)sender {
//    //Logout was pressed, so calling [Intercom reset] will log remove all local user data and stop tracking them.
//    [Intercom reset];
//    
//    //Save email so we know the user is logged in
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//    self.loggedIn = NO;
//}

@end
