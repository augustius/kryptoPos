//
//  LanguageTableViewController.swift
//  EbizuManagerPro
//
//  Created by Daliso Ngoma on 16/02/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import UIKit

private let languageUserDefaultsKey: String = "AppleLanguages"

class LanguageTableViewController: UITableViewController {
    
    // MARK: - Class Variables
    // TODO: Probably change this to an enum to simply calls.
    let languageData: [String] = ["English", "Behasa Melayu", "Behasa Indonesia"]
    let languages: [String]? = UserDefaults.standard.object(forKey: languageUserDefaultsKey) as? [String]
    var currentLanguage: String? = (UserDefaults.standard.object(forKey: languageUserDefaultsKey) as? [String])?[0]
    var lastSelectedIndexPath: IndexPath? = nil
    var languagePath: String? = nil
    var bundle: Bundle = Bundle.main
    var lprojFileExt: String = "lproj"
    
    struct languagesStruct {
        struct english {
            static let abbrev = "en";
            static let fileAbbrev = "en"
        }
        struct malay {
            static let abbrev = "ms";
            static let fileAbbrev = "ms"
        }
        struct indonesian {
            static let abbrev = "id";
            static let fileAbbrev = "id"
        }
        struct chinese {
            static let abbrev = "zh";
            static let fileAbbrev = "zh"
        }
    }
    
    // MARK: - UIViewController Methods
    override func viewDidLoad() {
        if let currentLanguage = currentLanguage {
            switch currentLanguage {
            case "en":
                lastSelectedIndexPath = IndexPath(row: 0, section: 0)
            case "ms":
                lastSelectedIndexPath = IndexPath(row: 1, section: 0)
            case "id":
                lastSelectedIndexPath = IndexPath(row: 2, section: 0)
            default:
                lastSelectedIndexPath = IndexPath(row: 0, section: 0)
            }
        } else {
            lastSelectedIndexPath = IndexPath(row: 0, section: 0)
        }
        
    }
    
    // MARK: UITableViewController Methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languageData.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Please Select a Language" : nil
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LanguageTableViewCell", for: indexPath)
        cell.textLabel?.text = languageData[indexPath.row]
        
        if let lastSelectedIndexPath = lastSelectedIndexPath {
            if indexPath == lastSelectedIndexPath {
                cell.accessoryType = .checkmark
            }
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let safeLastSelectedIndexPath = lastSelectedIndexPath {
            tableView.cellForRow(at: safeLastSelectedIndexPath)?.accessoryType = .none
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            lastSelectedIndexPath = indexPath
            changeLanguage(withIndexPath: indexPath)
        } else {
            print("ERROR: Issue in changing the language")
        }
    }
    
    // MARK: - Class Methods
    func changeLanguage(withIndexPath indexPath: IndexPath) {
        
        // FIXME: Dumb this down to a generic method to allow for multiple languages support.
        // Also remove the need for the bundle name to be put into an array for the userDefaults.
        switch indexPath.row {
        case 0:
            userDefaults.set([languagesStruct.english.abbrev], forKey: languageUserDefaultsKey)
            languagePath = bundle.path(forResource: languagesStruct.english.fileAbbrev, ofType: lprojFileExt)
        case 1:
            userDefaults.set([languagesStruct.malay.abbrev], forKey: languageUserDefaultsKey)
            languagePath = bundle.path(forResource: languagesStruct.malay.fileAbbrev, ofType: lprojFileExt)
        case 2:
            userDefaults.set([languagesStruct.indonesian.abbrev], forKey: languageUserDefaultsKey)
            languagePath = bundle.path(forResource: languagesStruct.indonesian.fileAbbrev, ofType: lprojFileExt)
        case 4:
            userDefaults.set([languagesStruct.chinese.abbrev], forKey: languageUserDefaultsKey)
            languagePath = bundle.path(forResource: languagesStruct.chinese.fileAbbrev, ofType: lprojFileExt)
        default:
            userDefaults.set([languagesStruct.english.abbrev], forKey: languageUserDefaultsKey)
            languagePath = bundle.path(forResource: "Base", ofType: lprojFileExt)
        }
        
        let alertConroller: UIAlertController = UIAlertController(title: "Language Change Successful", message: "The Selected Language Has Been Applied to POS, you will have to log in again to see the applied changes.", preferredStyle: .alert)
        let okAlertAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { (action) in
            guard let languagePath = self.languagePath else {
                print("ERROR: Language was not able to be applied.")
                return
            }
            appDelegate?.language_bundle = Bundle(path: languagePath)
            userDefaults.synchronize()
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
        alertConroller.addAction(okAlertAction)
        present(alertConroller, animated: true, completion: nil)
    }
    
}
