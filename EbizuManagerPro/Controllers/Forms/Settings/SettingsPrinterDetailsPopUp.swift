//
//  SettingsPrinterDetailsPopUp.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 14/09/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum popUpUsage{
    case receiptType
    case printerType
    case bluetoothSearch
    case category
    case discoveryMode
}

protocol CustomPopUpDelegate: class {
    func customValueSelected(customValue:String, currentPopUpUsage:popUpUsage)
}

class SettingsPrinterDetailsPopUp:UIViewController, UITableViewDelegate, UITableViewDataSource, Epos2DiscoveryDelegate
{
    @IBOutlet var customToolBar: UIToolbar!
    @IBOutlet var customToolBarHeightConstraint: NSLayoutConstraint!
    @IBOutlet var customTableView: UITableView!
    weak var delegate: CustomPopUpDelegate?
    
    var currentData = [AnyObject]()
    var selectedCustomValue = ""
    private var selectedCustomValueArray = [String]()
    {
        didSet{
            self.selectedCustomValue = self.selectedCustomValueArray.joined(separator: ",")
        }
    }
    var currentPopUpUsage:popUpUsage = .receiptType
    var filterOption: Epos2FilterOption = Epos2FilterOption()

    //MARK: - View Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        let multiSelectValue = (currentPopUpUsage == .category)
        customToolBar.isHidden = !multiSelectValue
        customToolBarHeightConstraint.constant = customToolBar.isHidden ? 0 : 44
        
        switch currentPopUpUsage
        {
        case .discoveryMode:
            filterOption.deviceType = EPOS2_TYPE_ALL.rawValue
            currentData = []
        case .bluetoothSearch:
            if let allBluetoothPrinter = SMPort.searchPrinter("BT:") as? [PortInfo]
            {
                currentData = allBluetoothPrinter
            }
            else
            {
                currentData = []
            }
            selectedCustomValueArray = [selectedCustomValue]
        case .category:
            currentData = shareddb.shared().get_product_categories() as [AnyObject]
            selectedCustomValueArray = selectedCustomValue.components(separatedBy: ",")
        case .printerType:
            currentData = ["Bluetooth Printer" as AnyObject,
                           "IP Printer" as AnyObject,
                           "Discovery Mode" as AnyObject,
                           "X Printer" as AnyObject]
            selectedCustomValueArray = [selectedCustomValue]
        case .receiptType:
            currentData = ["Bill" as AnyObject,
                           "Kitchen" as AnyObject]
            selectedCustomValueArray = [selectedCustomValue]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch currentPopUpUsage {
        case .discoveryMode:
            let result = Epos2Discovery.start(filterOption, delegate: self)
            if result != EPOS2_SUCCESS.rawValue {
                //ShowMsg showErrorEpos(result, method: "start")
            }
        default:
            break
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        switch currentPopUpUsage {
        case .discoveryMode:
            while Epos2Discovery.stop() == EPOS2_ERR_PROCESSING.rawValue {
                // retry stop function
            }
        default:
            break
        }
    }

    //MARK: - IBAction Method
    @IBAction func doneSelectValue(_ sender: UIBarButtonItem)
    {
        doneSelecting()
    }
    
    @IBAction func selectAllValue(_ sender: UIBarButtonItem)
    {
        selectedCustomValueArray = ["*"]
        customTableView.reloadData()
    }
    
    @IBAction func clearSelectedValue(_ sender: UIBarButtonItem)
    {
        selectedCustomValueArray = [""]
        customTableView.reloadData()
    }
    
    func doneSelecting()
    {
        delegate?.customValueSelected(customValue: selectedCustomValue, currentPopUpUsage: currentPopUpUsage)
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITableView Delegate method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return currentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let data = currentData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "customTableCell", for: indexPath)
        var labelToBeShown = ""
        var detailLabelToBeShown = ""
        var selected = false
        
        switch currentPopUpUsage
        {
        case .discoveryMode:
            guard
                let deviceInfo = data as? Epos2DeviceInfo
                else { print("error on \(#function), \(#line) "); return cell }

            labelToBeShown = deviceInfo.deviceName
            detailLabelToBeShown = deviceInfo.target
            selected = (selectedCustomValue == deviceInfo.target)
        case .bluetoothSearch:
            guard
                let portInfo = data as? PortInfo
                else { print("error on \(#function), \(#line) "); return cell}
            
            labelToBeShown = portInfo.modelName
            detailLabelToBeShown = portInfo.portName
            selected = (selectedCustomValue == portInfo.portName)
        case .category:
            guard
                let dictData = data as? [String:String],
                let catId = dictData["ctg_id"],
                let title = dictData["title"]
            else { print("error on \(#function), \(#line) "); return cell}

            labelToBeShown = title
            selected = (selectedCustomValue == "*" && !(selectedCustomValueArray.count > 1)) || selectedCustomValueArray.contains(catId)
        case .printerType,.receiptType:
            labelToBeShown = (data as? String) ?? ""
            selected = (labelToBeShown == selectedCustomValue)
        }
        
        cell.textLabel?.font = UIFont(name: "Poppins-Light", size: 18)
        cell.textLabel?.text = labelToBeShown
        cell.detailTextLabel?.font = UIFont(name: "Poppins-Medium", size: 15)
        cell.detailTextLabel?.text = detailLabelToBeShown
        cell.accessoryType = selected ? .checkmark : .none

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let data = currentData[indexPath.row]
        
        switch currentPopUpUsage
        {
        case .discoveryMode:
            guard
                let deviceInfo = (data as? Epos2DeviceInfo)
                else { print("error on \(#function), \(#line) "); return}
            selectedCustomValueArray = [deviceInfo.target]
            doneSelecting()
        case .bluetoothSearch:
            guard
                let portName = (data as? PortInfo)?.portName
                else { print("error on \(#function), \(#line) "); return}
            
            selectedCustomValueArray = [portName]
            doneSelecting()
        case .category:
            guard
                let dictData = data as? [String:String],
                let catId = dictData["ctg_id"]
                else { print("error on \(#function), \(#line) "); return}
            
            if selectedCustomValue == "*" && !(selectedCustomValueArray.count > 1)
            {
                selectedCustomValueArray = [String](Set(currentData.lazy.map{$0["ctg_id"] as! String }))
            }
            
            if let index = selectedCustomValueArray.index(of: catId)
            {
                selectedCustomValueArray.remove(at: index)
            }
            else
            {
                selectedCustomValueArray.append(catId)
            }
            
            customTableView.reloadData()
        case .printerType,.receiptType:
            let selectedValue = (data as? String) ?? ""
            selectedCustomValueArray = [selectedValue]
            doneSelecting()
        }
    }

    // MARK: - Epos2DiscoveryDelegate
    func onDiscovery(_ deviceInfo: Epos2DeviceInfo!) {
        currentData.append(deviceInfo)
        customTableView.reloadData()
    }
}
