//
//  OtherSettingsTableViewController.swift
//  EbizuManagerPro
//
//  Created by Daliso Ngoma on 17/02/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import UIKit

class OtherSettingsTableViewController: UITableViewController {
    
    // MARK: - IBOutlet Variables
//    @IBOutlet weak var barcodeScannerSwitch: UISwitch! //disable in 2..0
    // FIXME: Find out what was the functionality of this needed for
//    @IBOutlet weak var qrCodeValidation: UISwitch! //disable in 2..0
    // FIXME: Clarify if this should be Dine Type or Dine In Type
    @IBOutlet weak var dineTypeSwitch: UISwitch!
    @IBOutlet weak var imageMode: UISwitch!
    // NOTE: DON'T CREATE AN ACTION FOR THIS SWITCH
//    @IBOutlet weak var gstActiveSwitch: UISwitch!
    // FIXME: customSales not being used
    // NOTE: Also think of a better name, it's not that descriptive
    // that it's used for adding custom food items
//    @IBOutlet weak var customSalesSwitch: UISwitch!
    @IBOutlet weak var serviceChargeSwitch: UISwitch!
    
    // MARK: - IBAction Methods
//    @IBAction func barcodeScannerValueChanged(_ sender: UISwitch) {
//        shareddb.shared().set_barcode(sender.isOn) //disable in 2..0
//    }
    
//    @IBAction func qrCodeValidationValueChanged(_ sender: UISwitch) {
//        shareddb.shared().set_qr_validation(sender.isOn) //disable in 2..0
//    }
    
    @IBAction func dineTypeValueChanged(_ sender: UISwitch) {
        shareddb.shared().set_dine_type(sender.isOn)
    }
    
    @IBAction func imageModeValueChanged(_ sender: UISwitch) {
        shareddb.shared().set_image_mode(sender.isOn)
    }
    
    @IBAction func serviceChargeValueChanged(_ sender: UISwitch)
    {
        //NOTE: For 1.6.6 store in UserDefault, will find better way to handle service charge in 1.7
        // you must be wondering why "SERVICE_CHARGE_DISABLE" why not "SERVICE_CHARGE_ENABLE" 
        // why store the opposite of the switch value
        // well because this is quick fix, so that we dont lose a merchant call 'katiestove'
        // and we dont want the rest of merchant lost their service charge and they have to go here to switch on the service charge when they update to 1.6.6
        userDefaults.set(!sender.isOn, forKey: "SERVICE_CHARGE_DISABLE")
    }
    
    
    // MARK: Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSwitches()
    }
    
    // MARK: - Class Methods
    // TODO: This is similar to the printer settings. This should be turned into a protocol method
    private func setupSwitches() {
//        barcodeScannerSwitch.isOn = SessionData.shared().barcode_enable
//        qrCodeValidation.isOn = SessionData.shared().qr_validation_enable
        // FIXME: Clarify if this should be Dine Type or Dine In Type
        imageMode.isOn = SessionData.shared().image_on_off_enable
        dineTypeSwitch.isOn = SessionData.shared().dine_in_enable
        serviceChargeSwitch.isOn = !userDefaults.bool(forKey: "SERVICE_CHARGE_DISABLE")
//        gstActiveSwitch.isOn = SessionData.shared().gst_enable
//        customSalesSwitch.isOn = SessionData.shared().custom_sales_enable
    }
    
}
