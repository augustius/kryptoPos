//
//  CustomImagePickerController.m
//  Ebizu Manager
//
//  Created by VinothV on 6/4/15.
//  Copyright (c) 2015 EBIZU. All rights reserved.
//

#import "CustomImagePickerForm.h"

@interface CustomImagePickerForm ()

@end

@implementation CustomImagePickerForm

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate
{
    return NO;
}
@end
