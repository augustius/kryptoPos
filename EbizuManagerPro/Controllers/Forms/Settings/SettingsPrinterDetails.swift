//
//  SettingsPrinterDetails.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 14/09/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum detailUsageStatus{
    case edit
    case add
}

struct printerDetail{
    var printerId = ""
    var printerType  = ""
    var receiptType = ""
    var printerAddress = ""
    var printerName = ""
    var categoryToPrint = ""
    var printerCopies = ""
    var defaultPrinter = false
}

class SettingsPrinterDetails:UITableViewController,UITextFieldDelegate,CustomPopUpDelegate,customKeyboardViewDelegate
{
    @IBOutlet var printerDetailHeader: UILabel!
    @IBOutlet var printerType: UIButton!
    @IBOutlet var receiptType: UIButton!
    @IBOutlet var bluetoothPrinter: UIButton!
    @IBOutlet var ip1: UITextField!
    @IBOutlet var ip2: UITextField!
    @IBOutlet var ip3: UITextField!
    @IBOutlet var ip4: UITextField!
    @IBOutlet var printerName: UITextField!
    @IBOutlet var noOfCopies: UITextField!
    @IBOutlet var printerDefaultStatus: UISwitch!
    @IBOutlet var addOrEditButton: UIButton!
    @IBOutlet var deleteButton: UIButton!

    var formUsageStatus:detailUsageStatus = .add
    {
        didSet{
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        self.printerDetailHeader.text = (self.formUsageStatus == .add) ? "ADD PRINTER" : "EDIT PRINTER"
                        if self.formUsageStatus == .add
                        {
                            self.deleteButton.isUserInteractionEnabled = false
                            self.deleteButton.backgroundColor = UIColor.lightGray
                        }
                    }, completion: nil)
            }
        }
        
    }
    
    var currentPrinterDetail = printerDetail()
    {
        didSet{
            DispatchQueue.main.async{
                UIView.animate(withDuration: 1, animations:
                    {
                        self.printerType.setTitle(self.currentPrinterDetail.printerType == "" ? "Click Here" : self.currentPrinterDetail.printerType, for: .normal)
                        self.receiptType.setTitle(self.currentPrinterDetail.receiptType == "" ? "Click Here" : self.currentPrinterDetail.receiptType, for: .normal)
                        self.printerName.text = self.currentPrinterDetail.printerName
                        self.noOfCopies.text = self.currentPrinterDetail.printerCopies
                        self.printerDefaultStatus.isOn = self.currentPrinterDetail.defaultPrinter
                        
                        if self.currentPrinterDetail.printerType == "Bluetooth Printer" || self.currentPrinterDetail.printerType == "Discovery Mode"
                        {
                            self.bluetoothPrinter.isHidden = false
                            self.bluetoothPrinter.setTitle(self.currentPrinterDetail.printerAddress == "" ? "Search" : self.currentPrinterDetail.printerAddress, for: .normal)
                        }
                        else
                        {
                            self.bluetoothPrinter.isHidden = true
                            
                            let arrayOfIP = self.currentPrinterDetail.printerAddress.components(separatedBy: ".")
                            if arrayOfIP.count > 0
                            {
                                for (index,str) in arrayOfIP.enumerated()
                                {
                                    switch index+1
                                    {
                                    case 1:
                                        self.ip1.text = ""
                                        self.ip1.text = str
                                        break
                                    case 2:
                                        self.ip2.text = ""
                                        self.ip2.text = str
                                        break
                                    case 3:
                                        self.ip3.text = ""
                                        self.ip3.text = str
                                        break
                                    default:
                                        self.ip4.text = ""
                                        self.ip4.text = str
                                        break
                                    }
                                }
                            }
                        }
                        
                    }, completion: nil)
            }
        }
    }
    
    private var currentPopUpUsage:popUpUsage = .receiptType
    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    
    //MARK: - IBAction method
    @IBAction func cancelPrinter()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectPrinterType(_ sender: UIButton)
    {
        currentPopUpUsage = .printerType
        showCustomPopUp(sender: sender)
    }
    
    @IBAction func selectReceiptType(_ sender: UIButton)
    {
        currentPopUpUsage = .receiptType
        showCustomPopUp(sender: sender)
    }
    
    @IBAction func searchBluetoothPrinter(_ sender: UIButton)
    {
        let isBluetooth = currentPrinterDetail.printerType == "Bluetooth Printer"
        currentPopUpUsage = isBluetooth ? .bluetoothSearch : .discoveryMode
        showCustomPopUp(sender: sender)
    }
    
    @IBAction func selectCategoryToPrint(_ sender: UIButton)
    {
        if currentPrinterDetail.receiptType == "Kitchen"
        {
            currentPopUpUsage = .category
            showCustomPopUp(sender: sender)
        }
        else
        {
            present_alert("Info", message: "Select 'Kitchen' receipt type to use this feature")
        }
    }
    
    @IBAction func setPrinterAsDefault(_ sender: UISwitch)
    {
        currentPrinterDetail.defaultPrinter = sender.isOn
    }
    
    @IBAction func testPrinter()
    {
        var printAddress = currentPrinterDetail.printerAddress

        if printAddress == ""
        {
            if currentPrinterDetail.printerType == "IP Printer" || currentPrinterDetail.printerType == "X Printer"
            {
                present_alert("", message: "No ip address entered")
            }
            else if currentPrinterDetail.printerType == "Bluetooth Printer"
            {
                present_alert("", message: "No bluetooth printer selected")
            }
            else if currentPrinterDetail.printerType == "Discovery Mode"
            {
                present_alert("", message: "No printer discovered")
            }
        }
        else
        {
            if currentPrinterDetail.printerType == "X Printer" {
                printAddress.markAsXPrinter()
            }
            EbizuPrinter.sharedPrinter.testPrinter(IpAddress: printAddress)
        }
    }
    
    @IBAction func addEditPrinter()
    {
        if inputtedValueIsValid()
        {
            switch formUsageStatus
            {
            case .add:
                performSegue(withIdentifier: "printerAdded", sender: self)
                break
            case .edit:
                performSegue(withIdentifier: "printerEdited", sender: self)
                break
            }
        }
    }
    
    @IBAction func deletePrinter()
    {
        performSegue(withIdentifier: "printerDeleted", sender: self)
    }
    
    //MARK: - Custom method
    func inputtedValueIsValid() -> Bool
    {
        // printer type checking && printer address
        if currentPrinterDetail.printerType  == ""
        {
            present_alert("", message: "Printer type not selected")
            return false
        }
        else if currentPrinterDetail.printerAddress == ""
        {
            if currentPrinterDetail.printerType == "IP Printer" || currentPrinterDetail.printerType == "X Printer"
            {
                present_alert("", message: "Ip address not entered")
            }
            else if currentPrinterDetail.printerType == "Bluetooth Printer"
            {
                present_alert("", message: "Bluetooth printer not selected")
            }
            else if currentPrinterDetail.printerType == "Discovery Mode"
            {
                present_alert("", message: "Printer not discovered")
            }
            return false
        }
        
        // receipt type checking
        if currentPrinterDetail.receiptType == ""
        {
            present_alert("", message: "Receipt type not selected")
            return false
        }
        
        if currentPrinterDetail.printerName == ""
        {
            present_alert("", message: "Printer name not entered")
            return false
        }
        
        if currentPrinterDetail.printerCopies == ""
        {
            present_alert("", message: "Printer copies not entered")
            return false
        }
        
        return true
    }
    
    func showCustomPopUp(sender: UIButton)
    {
        performSegue(withIdentifier: "showCustomPopUp", sender: sender)
    }
    
    //MARK: - UITextfield Delegate 
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        switch textField
        {
        case ip1,ip2,ip3,ip4:
            customKeyboard?.textfield = textField
            customKeyboard?.customKeyboardType = .ipAddress
            customKeyboard?.delegate = self
        case noOfCopies:
            customKeyboard?.textfield = textField
            customKeyboard?.customKeyboardType = .intNumber
            customKeyboard?.delegate = self
        default:
            break
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        switch textField
        {
        case ip1,ip2,ip3,ip4:
            let arrayOfIP = [ip1.text ?? "",ip2.text ?? "",ip3.text ?? "",ip4.text ?? ""]
            currentPrinterDetail.printerAddress = arrayOfIP.joined(separator: ".")
        case noOfCopies:
            currentPrinterDetail.printerCopies = textField.text ?? ""
        case printerName:
            currentPrinterDetail.printerName = textField.text ?? ""
        default:
            break
        }
    }
    
    //MARK: - customKeyboardViewDelegate method
    func keyPressed(key: String, textField: UITextField) -> Bool
    {
        switch textField
        {
        case ip1:
            if textField.text?.length == 3
            {
                ip2.becomeFirstResponder()
            }
        case ip2:
            if textField.text?.length == 3
            {
                ip3.becomeFirstResponder()
            }
        case ip3:
            if textField.text?.length == 1
            {
                ip4.becomeFirstResponder()
            }
        default:
            break
        }
        
        return true
    }
    
    //MARK: - CustomPopUpDelegate method
    func customValueSelected(customValue: String, currentPopUpUsage: popUpUsage)
    {
        switch currentPopUpUsage
        {
        case .bluetoothSearch, .discoveryMode:
            currentPrinterDetail.printerAddress = customValue
        case .category:
            currentPrinterDetail.categoryToPrint = customValue
        case .printerType:
            if currentPrinterDetail.printerType != customValue
            {
                currentPrinterDetail.printerType = customValue
                currentPrinterDetail.printerAddress = ""
            }
        case .receiptType:
            currentPrinterDetail.receiptType = customValue
        }
    }
    
    //MARK: - Segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showCustomPopUp"
        {
            guard
                let button = sender as? UIButton,
                let dvc = segue.destination as? SettingsPrinterDetailsPopUp,
                let possiblePopover = dvc.popoverPresentationController
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = button.bounds
            possiblePopover.sourceView = button
            dvc.delegate = self
            dvc.currentPopUpUsage = currentPopUpUsage
            
            switch currentPopUpUsage
            {
            case .bluetoothSearch, .discoveryMode:
                dvc.selectedCustomValue = currentPrinterDetail.printerAddress
            case .category:
                dvc.selectedCustomValue = currentPrinterDetail.categoryToPrint
            case .printerType:
                dvc.selectedCustomValue = currentPrinterDetail.printerType
            case .receiptType:
                dvc.selectedCustomValue = currentPrinterDetail.receiptType
            }
        }
    }
}
