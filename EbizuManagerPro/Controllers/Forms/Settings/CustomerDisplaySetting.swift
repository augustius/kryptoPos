//
//  CustomerDisplaySetting.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 18/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class CustomerDisplaySearchCell: UITableViewCell
{
    var peerID: MCPeerID = MCPeerID.init(displayName: "temporary-name")
}


class CustomerDisplayTableViewController: UITableViewController
{
    var shownDiscoveredPeers = [MCPeerID]()
    
    // MARK: - View Cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        P2PConnection.sharedP2PConnection().browser.startBrowsingForPeers()
        P2PConnection.sharedP2PConnection().advertiser.startAdvertisingPeer()
        self.filterCustomerDisplay()
        
        notification.addObserver(self, selector: #selector(reloadTable), name: NSNotification.Name("MCDidChangeStateNotification"), object: nil)
        notification.addObserver(self, selector: #selector(reloadTable), name: NSNotification.Name("DiscoveredNewPeersNotification"), object: nil)
    }

    // MARK: - UITableViewController Delegate
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return "Search For Customer Display"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return shownDiscoveredPeers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerDisplaySearchCell", for: indexPath)
        
        if let customerDisplaySearchCell = cell as? CustomerDisplaySearchCell
        {
            let discoveredCD = shownDiscoveredPeers[indexPath.row]
            
            customerDisplaySearchCell.peerID = discoveredCD
            customerDisplaySearchCell.textLabel?.text = discoveredCD.displayName.replace("[CD]", replacement: "")
            customerDisplaySearchCell.accessoryType = P2PConnection.sharedP2PConnection().connectedPeers.contains(discoveredCD) ? UITableViewCell.AccessoryType.checkmark : UITableViewCell.AccessoryType.none
            
            return customerDisplaySearchCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let cell = tableView.cellForRow(at: indexPath) as? CustomerDisplaySearchCell
        {
            P2PConnection.sharedP2PConnection().choose(peerID: cell.peerID,
                                                       toInvite: (cell.accessoryType != UITableViewCell.AccessoryType.checkmark))
        }
    }
    
    // MARK: - NSNotification method
    @objc func reloadTable(notification: NSNotification)
    {
        DispatchQueue.main.async
        {
            self.filterCustomerDisplay()
            self.tableView.reloadData()
        }
    }
    
    func filterCustomerDisplay()
    {
        let filteredList = P2PConnection.sharedP2PConnection().discoveredPeers.filter({$0.displayName.contains("[CD]")})
        shownDiscoveredPeers = filteredList
    }
}
