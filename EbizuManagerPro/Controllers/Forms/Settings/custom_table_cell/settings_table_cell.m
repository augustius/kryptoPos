//
//  settings_table_cell.m
//  Ebizu Manager
//
//  Created by Jamal on 11/23/14.
//  Copyright (c) 2014 EBIZU. All rights reserved.
//

#import "settings_table_cell.h"

@implementation settings_table_cell

@synthesize lbl_ipaddress           = _lbl_ipaddress;
@synthesize lbl_printer_type        = _lbl_printer_type;
@synthesize lbl_printername         = _lbl_printername;
@synthesize img_tick                = _img_tick;
@synthesize button_delete_printer   = _button_delete_printer;
@synthesize txt_print_copies        = _txt_print_copies;

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    [self.select_category_button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    self.select_category_button.layer.cornerRadius =6.0;
    self.select_category_button.layer.borderWidth=1.0;
    self.select_category_button.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.select_category_button.layer.masksToBounds=YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
