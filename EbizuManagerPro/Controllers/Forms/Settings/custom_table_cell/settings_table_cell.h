//
//  settings_table_cell.h
//  Ebizu Manager
//
//  Created by Jamal on 11/23/14.
//  Copyright (c) 2014 EBIZU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface settings_table_cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_ipaddress;
@property (weak, nonatomic) IBOutlet UILabel *lbl_printer_type;
@property (weak, nonatomic) IBOutlet UILabel *lbl_printername;
@property (nonatomic, weak) IBOutlet UIImageView *img_tick;
@property (strong, nonatomic) IBOutlet UIButton *button_delete_printer;

@property (weak, nonatomic) IBOutlet UITextField *txt_print_copies;

@property (weak, nonatomic) IBOutlet UIButton *select_category_button;
@end
