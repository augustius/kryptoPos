//
//  SettingsAdditionalPrinter.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 05/09/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class ReceiptPreviewCell: UITableViewCell
{
    @IBOutlet private var receiptImage: UIImageView!
    @IBOutlet private var receiptLabel: UILabel!
    @IBOutlet private var receiptImageWidthConstraint: NSLayoutConstraint!
    @IBOutlet private var receiptImageHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var receiptBottomConstraint: NSLayoutConstraint!
    
    var sample = receiptData() {
        didSet{
            switch sample.receiptDataType
            {
            case .image:
                receiptImageWidthConstraint.constant = sample.image.size.height
                receiptImageHeightConstraint.constant = sample.image.size.height
                receiptImage.image = sample.image.ConvertToGrayScale()
                receiptLabel.text = ""
            case .text, .line, .modifierText:
                receiptImageHeightConstraint.constant = 0
                let str = sample.receiptDataType == .line ? "".padding(toLength: 44, withPad: sample.text80mm, startingAt: 0) : sample.text80mm
                switch sample.textAlignment
                {
                case .left: receiptLabel.textAlignment = .left
                case .right: receiptLabel.textAlignment = .right
                case .center: receiptLabel.textAlignment = .center
                }
                
                var fontSize:CGFloat = 0.0
                switch sample.textFont
                {
                case .fontA: fontSize = 16.5
                case .fontB: fontSize = 12.0
                }
                fontSize += (CGFloat(sample.extraWitdh) * fontSize)
                let finalFont = sample.textBold ? UIFont(name: "FiraMono-Regular", size: fontSize) : UIFont(name: "FiraMono-Bold", size: fontSize)
                receiptLabel.font = finalFont
                receiptLabel.text = str
            case .cutPaper: break
            }
            
            receiptBottomConstraint.constant = CGFloat(10 * sample.extraLineSpace)
        }
    }
}

class SettingsAdditionalPrinter: UIViewController , UITableViewDataSource, UITableViewDelegate, SettingsAdditionalTableViewDelegate
{
    @IBOutlet var sampleReceipTableView: UITableView!
    var sampleReceiptData = [receiptData]()
    var sampleHeader = receiptHeaderData()
    var sampleFooter = receiptFooterData()
    var forcePrintEnable = false
    var printingOptionEnable = false

    //MARK: - View Cycle
    override func viewDidLoad()
    {
        sampleReceipTableView.register(UINib(nibName:"CustomReceiptCell", bundle: nil), forCellReuseIdentifier: "ReceiptPreviewCell")
        sampleReceipTableView.rowHeight = UITableView.automaticDimension
        sampleReceipTableView.estimatedRowHeight = 80.0
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func saveChanges()
    {
        //save changes on local db
        if let logo = sampleHeader.merchantLogo
        {
            downloadImage(chosenImage: logo)
        }
        else
        {
            deleteImage()
        }
        
        var updateHeaderFooterData = [String:String]()
        updateHeaderFooterData[MERCHANT_NAME]    = sampleHeader.merchantName
        updateHeaderFooterData[MERCHANT_ADDRESS] = sampleHeader.merchantAddress
        updateHeaderFooterData[MERCHANT_PHONE]   = sampleHeader.merchantPhone
        updateHeaderFooterData[FOOTER_MESSAGE]   = sampleFooter.footerMessage
        updateHeaderFooterData[WIFI_NAME]        = sampleFooter.wifiName
        updateHeaderFooterData[WIFI_PASS]        = sampleFooter.wifiPass
        shareddb.shared().update_header_footer(updateHeaderFooterData)
        
        shareddb.shared().set_merchant_address_mode(sampleHeader.showMerchantAddress,
                                                    name_mode: sampleHeader.showMerchantName,
                                                    phone_mode: sampleHeader.showMerchantPhone)
        
        shareddb.shared().set_printer_mode(forcePrintEnable)
        /// printing option is not stored in database
        userDefaults.set(printingOptionEnable, forKey: PRINTING_OPTION)
        
        Networking.shared().putData(forWriting: .writingSetting)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelChanges()
    {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UIViewTableViewDelegate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptPreviewCell", for: indexPath)
        
        if let receiptPreviewCell = cell as? ReceiptPreviewCell
        {
            let sample = sampleReceiptData[indexPath.row]
            
            receiptPreviewCell.sample = sample
            
            return receiptPreviewCell
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return sampleReceiptData.count
    }
    
    //MARK: - Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "embedReceiptSetting"
        {
            guard
                let dvc = segue.destination as? SettingsAdditionalTableView
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.delegate = self
        }
    }
    
    //MARK: - SettingsAdditionalTableViewDelegate method
    func updateReceiptPreview(view: SettingsAdditionalTableView)
    {
        sampleHeader = receiptHeaderData.init(merchantLogo: view.printerLogo.backgroundImage(for: .normal),
                                                 merchantName: view.printerHeaderName.text ?? "",
                                                 showMerchantName: view.showHeaderName.isOn,
                                                 merchantAddress: view.printerAddress.text,
                                                 showMerchantAddress: view.showAddress.isOn,
                                                 merchantPhone: view.printerContactNumber.text ?? "",
                                                 showMerchantPhone: view.showContactNumber.isOn)
        
        sampleFooter = receiptFooterData.init(wifiName: view.printerWifiName.text ?? "",
                                                 wifiPass: view.printerWifiPass.text ?? "",
                                                 footerMessage: view.printerFooterMessage.text ?? "")
        
        forcePrintEnable = view.forcePrintSwitch.isOn
        printingOptionEnable = view.printingOptionSwicth.isOn
        
        setupPreview()
    }
    
    //MARK: - setup preview method
    func setupPreview()
    {
        EbizuPrinter.sharedPrinter.receiptDataArr = [receiptData]()
        EbizuPrinter.sharedPrinter.getSampleHeader(sampleHeader: sampleHeader)
        EbizuPrinter.sharedPrinter.getSampleBody(sampleCompleteOrder: generateSampleBody())
        EbizuPrinter.sharedPrinter.getSampleFooter(sampleFooter: sampleFooter)
        sampleReceiptData = EbizuPrinter.sharedPrinter.receiptDataArr
        
        sampleReceipTableView.reloadData()
    }
    
    func generateSampleBody() -> CompleteOrder
    {
        let sampleCompleteOrder = CompleteOrder()
        sampleCompleteOrder.hold_order_id = (userDefaults.string(forKey: HARDWARE_CODE) ?? "") + "-" + (userDefaults.string(forKey: HOLD_ORDER_NO) ?? "")
        sampleCompleteOrder.currency = "".getCurrencyCode()
        sampleCompleteOrder.user_id = StringUtils.get_user_detail("user_id")
        sampleCompleteOrder.order_type = 0 //0 - Dine In || 1 - Walk in
        sampleCompleteOrder.isOrderOnHold = false
        sampleCompleteOrder.create_time = String(format:"%.0f",NSDate().timeIntervalSince1970)
        sampleCompleteOrder.voucher_amount = NSDecimalNumber.init(string:"0.00")
        sampleCompleteOrder.discount_amount = NSDecimalNumber.init(string:"0.00")
        sampleCompleteOrder.discount_percentage = 0
        sampleCompleteOrder.offer_sn = ""
        sampleCompleteOrder.trans_number = userDefaults.string(forKey: TRANSACTION_NO)
        sampleCompleteOrder.counter_number = Int32(Int(userDefaults.integer(forKey: ORDER_COUNT)))
        sampleCompleteOrder.trx_gst_mode = SessionData.shared().gst_enable ? "en_sst" : "non_sst"
        sampleCompleteOrder.customer_name = "Walk In"
        sampleCompleteOrder.orderItemsArray = [OrderItem]()
    
        let sampleOrderItem = OrderItem()
        sampleOrderItem.itemName      = "Cafe Latte"
        sampleOrderItem.itemQuantity  = NSDecimalNumber.init(string:"1")
        sampleOrderItem.itemPrice     = NSDecimalNumber.init(string:"10.6")
        sampleOrderItem.type_of_price = ""
        sampleOrderItem.type_of_unit  = ""
        sampleOrderItem.gst_rate      = NSDecimalNumber.init(string:"6")
        sampleOrderItem.gst_code      = "SR"
        sampleOrderItem.allModifiersDict  = nil

        sampleCompleteOrder.orderItemsArray.append(sampleOrderItem)
        sampleCompleteOrder.newCalculate()
        
        return sampleCompleteOrder
    }
    
    func downloadImage(chosenImage: UIImage)
    {
        let imageData = chosenImage.jpegData(compressionQuality: 0.80) //UIImagePNGRepresentation(chosenImage)
        
        FileUtils.writeToDocuemntsDirectory(atPath: POSImages+"/"+IMG_MODULES,
                                            fileData: imageData,
                                            withPrefix: "printer_logo",
                                            andExtension: "jpg")
    }
    
    func deleteImage()
    {
        FileUtils.deleteFile(fromDocumentsDirectory: "/\(POSImages)/\(IMG_MODULES)/printer_logo.jpg")
    }
}
