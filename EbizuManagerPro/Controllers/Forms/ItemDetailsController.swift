//
//  ItemDetailsController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 08/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension ItemDetails
{
    //MARK: - initiate method
    func setupData() // This is where we sort out the messed up data stucture
    {
        if let modifier = selectedOrderItem.allModifiersDict as? [String:AnyObject]
        {
            // CHOICE
            if let arr = modifier[CHOICE] as? [[String:AnyObject]]
            {
                print("choice here : \(arr)")
                parentChoiceArr = arr
            }
            
            // ADDITION
            if let arr = modifier[ADDITION] as? [[String:AnyObject]]
            {
                print("addition here : \(arr)")
                parentAddArr = arr
                
                readjustAdditionDataStructureToShowInCollectionView()
            }
        }        
    }
    
    func setupView()
    {
        itemName.text = selectedOrderItem.itemName.capitalized
        itemPrice.text = String(format:"%.2f",selectedOrderItem.itemPrice.doubleValue)
        itemStock.text = "" //selectedOrderItem.stock_monitoring.intValue == 1 ? "Stock Left : \(selectedOrderItem.stock_left == nil ? 0 : selectedOrderItem.stock_left.intValue)" : ""

        selectUomView.isHidden = selectedOrderItem.type_of_price != "Unit Price"
        selectCustomPrice.isHidden = selectedOrderItem.type_of_price == "Unit Price"
        if selectedOrderItem.type_of_price == "Unit Price"
        {
            showUnitOfMeasure()
        }
        else if selectedOrderItem.type_of_price == "Variable Price"
        {
            customizePrice()
        }
        
        updateDiscountAndCustomPriceView()
    }
    
    func updateDiscountAndCustomPriceView()
    {
        selectDiscountBtn.isUserInteractionEnabled = selectedOrderItem.is_edited_item_price != "price_edited"
        selectDiscountBtn.backgroundColor = selectDiscountBtn.isUserInteractionEnabled ? UIColor(netHex:"0x14406B") : UIColor.lightGray
        
        selectCustomBtn.isUserInteractionEnabled = (selectedOrderItem.discount.doubleValue == 0 || selectedOrderItem.is_edited_item_price == "price_edited")
        selectCustomBtn.backgroundColor = selectCustomBtn.isUserInteractionEnabled ? UIColor(netHex:"0x14406B") : UIColor.lightGray
    }
    
    //MARK: - Choice Addition Method
    func getParentChoiceOrAddition() -> [[String:AnyObject]]
    {
        var temp = [[String:AnyObject]]()
        
        switch option
        {
        case .addition:
            temp = showParentAddArr
            break
        case .choice:
            temp = parentChoiceArr
            break
        }
        
        return temp
    }
    
    func getChildChoiceOrAddition(index:Int) -> [[String:AnyObject]]
    {
        let parentArrDict = getParentChoiceOrAddition()
        var childArrDict = [[String:AnyObject]]()
        
        if parentArrDict.count > 0
        {
            if let safeArrDict = parentArrDict[index]["child_modifier"] as? Array< Dictionary< String, AnyObject >>
            {
                childArrDict = safeArrDict
            }
        }
        
        return childArrDict
    }
    
    func readjustAdditionDataStructureToShowInCollectionView()
    {
        // below code is to structure the data so that it can be represent like choice data
        // get all unique parent name
        let tempParentName = [String](Set(parentAddArr.lazy.map{$0["mod_parent_name"] as! String }))
        
        showParentAddArr = [[String:AnyObject]]()
        for str in tempParentName
        {
            // get all child that belongs to this parent
            let tempArr = parentAddArr.filter { ($0["mod_parent_name"] as! String) == str }
            let tempDict = ["mod_parent_name":str,"child_modifier":tempArr] as [String : AnyObject]
            
            showParentAddArr.append(tempDict)
        }
    }
    
    func executeOption(selectedOption : choiceOrAddition)
    {
        switch selectedOption
        {
        case .choice:
            choice.backgroundColor = UIColor.white
            choiceName.textColor = UIColor.gray
            choiceInitial.textColor = UIColor.gray
            addition.backgroundColor = UIColor(netHex: "0x14406B")
            additionName.textColor = UIColor.white
            additionInitial.textColor = UIColor.white
            break
        case .addition:
            choice.backgroundColor = UIColor(netHex: "0x14406B")
            choiceName.textColor = UIColor.white
            choiceInitial.textColor = UIColor.white
            addition.backgroundColor = UIColor.white
            additionName.textColor = UIColor.gray
            additionInitial.textColor = UIColor.gray
            break
        }

        option = selectedOption
        tableView.reloadData()
    }
    
    
    func UpdateAdditionData(sender: AnyObject)
    {
        if let view = sender.superview as? UIView
        {
            var cell = AdditionCollectionCell()
            var modId = ""
            var modQty = "0"
            
            if let additionCollectionCell = view.superview as? AdditionCollectionCell //add,minus
            {
                cell = additionCollectionCell
            }
            else if let additionCollectionCell = view.superview?.superview as? AdditionCollectionCell //back
            {
                cell = additionCollectionCell
            }
            
            modId = cell.additionID
            if let safeModQty = cell.add.titleLabel?.text
            {
                modQty = safeModQty
            }
            
            if modId != ""
            {
                // running for-loop to update addition qty , if can find a better way
                var tempParentAddArr = [[String:AnyObject]]()
                for dict in parentAddArr
                {
                    var tempDict = dict
                    
                    if tempDict["mod_id"]?.description == modId
                    {
                        tempDict["mod_qty"] = modQty as AnyObject
                    }
                    
                    tempParentAddArr.append(tempDict)
                }
                parentAddArr = tempParentAddArr
                
                compileItemData()
            }
        }
    }
    
    func UpdateChoiceData(sender: AnyObject)
    {
        if let view = sender.superview as? UIView
        {
            var modId = ""
            var modParentId = ""
            var selectedChoice = [[String:AnyObject]]()

            if let choiceCollectionCell = view.superview?.superview as? ChoiceCollectionCell //back
            {
                modId = choiceCollectionCell.choiceID
                modParentId = choiceCollectionCell.parentID
            }
            
            if modId != ""
            {
                var tempParentChoiceArr = [[String:AnyObject]]()
                for dict in parentChoiceArr
                {
                    var tempDict = dict
                    
                    if tempDict["mod_parent_id"]?.description == modParentId
                    {
                        if let safeArrDict = tempDict["child_modifier"] as? Array< Dictionary< String, AnyObject >>
                        {
                            selectedChoice = safeArrDict.filter { ($0["mod_id"] as! String) == modId }
                            
                            if selectedChoice.count > 0
                            {
                                tempDict["selected_choice"] = selectedChoice.first as AnyObject
                            }
                        }
                    }
                    tempParentChoiceArr.append(tempDict)
                }
                parentChoiceArr = tempParentChoiceArr
                tableView.reloadData()
                
                compileItemData()
            }
        }
    }
    
    //MARK: - All Done Button Method
    func handleDone(sender:UIButton)
    {
        if let view = sender.superview?.superview?.superview
        {
            switch view
            {
            case addDiscountView:
                textFieldDidEndEditing(discountAmount)
                break
            case customPriceView:
                textFieldDidEndEditing(customPrice)
                break
            case addNotesView:
                textFieldDidEndEditing(remarks)
                break
            case uomView:
                textFieldDidEndEditing(uomQty)
                break
            default:
                break
            }
            
            view.removeFromSuperview()
        }
    }
    
    //MARK: - Custom Price Method
    func handleCustomPrice(price: String)
    {
        let inputCustomPrice = NSDecimalNumber.init(string:price)
        if inputCustomPrice != NSDecimalNumber.notANumber
        {
            if selectedOrderItem.type_of_price == "Fixed Price"
            {
                if String(format:"%.2f",selectedOrderItem.totalPrice.doubleValue) != price
                {
                    if selectedOrderItem.totalPrice.doubleValue < inputCustomPrice.doubleValue
                    {
                        present_sclalert("Attention", message: "Please type in an item price that is less than the original price", alert_style: .info)
                        customPrice.text = String(format:"%.2f",selectedOrderItem.totalPrice.doubleValue)
                        
                    }
                    else
                    {
                        let itemDiscount = selectedOrderItem.totalPrice.subtracting(inputCustomPrice)
                        let itemDiscountPerQty = itemDiscount.dividing(by: selectedOrderItem.itemQuantity)
                        selectedOrderItem.discount = itemDiscountPerQty
                        selectedOrderItem.is_edited_item_price = (inputCustomPrice.doubleValue > 0) ? "price_edited" : ""
                        
                        updateDiscountAndCustomPriceView()
                    }
                }
            }
            else if selectedOrderItem.type_of_price == "Variable Price"
            {
                let inputCustomPricePerQty = inputCustomPrice.dividing(by: selectedOrderItem.itemQuantity)
                selectedOrderItem.itemPrice = inputCustomPricePerQty
                selectedOrderItem.selling_price = inputCustomPricePerQty
            }
        }
    }
    
    func handleClearCustomPrice()
    {
        if selectedOrderItem.type_of_price == "Fixed Price"
        {
            selectedOrderItem.discount = NSDecimalNumber.init(string:"0.00")
            selectedOrderItem.is_edited_item_price = ""
            selectedOrderItem.new_calculate_(with_bill_disc: NSDecimalNumber.init(string:"1.0"))
            
            customPrice.text = String(format:"%.2f",selectedOrderItem.totalPrice.doubleValue)
            updateDiscountAndCustomPriceView()
        }
        else if selectedOrderItem.type_of_price == "Variable Price"
        {
            customPrice.text = "0.00"
            textFieldDidEndEditing(customPrice)
        }
        
        compileItemData()
    }
    
    //MARK: - Discount method
    func handleDiscountCalculation(amount: String)
    {
        var inputDiscount = NSDecimalNumber.init(string:amount)
        if inputDiscount != NSDecimalNumber.notANumber && selectedOrderItem.itemPrice_n_uomQty_n_modifier_perItem.doubleValue > 0
        {
            if discountType.selectedSegmentIndex == 1 // percentage
            {
                let inputDiscountPerc = inputDiscount.dividing(by:NSDecimalNumber.init(string:"100.0"))
                inputDiscount = selectedOrderItem.itemPrice_n_uomQty_n_modifier_perItem.multiplying(by: inputDiscountPerc)
            }
            
            selectedOrderItem.is_edited_item_price = discountType.selectedSegmentIndex == 1 ?  "percentage" : "amount"
            selectedOrderItem.discount = inputDiscount
            
            updateDiscountAndCustomPriceView()
            adjustDiscountView()
        }
    }
    
    func adjustDiscountView()
    {
        selectedOrderItem.new_calculate_(with_bill_disc: NSDecimalNumber.init(string:"1.0"))
        
        predefinedDiscount.setTitle(selectedOrderItem.predefined_discount != nil ? shareddb.shared().get_predefined_discount_(by_id: selectedOrderItem.predefined_discount) : "Discount Type", for: .normal)
        discountType.isUserInteractionEnabled = selectedOrderItem.predefined_discount == nil
        discountAmount.isUserInteractionEnabled = selectedOrderItem.predefined_discount == nil
        
        if var storedDiscount = selectedOrderItem.discount // stored is fix amount
        {
            discountType.selectedSegmentIndex = selectedOrderItem.is_edited_item_price != nil ? (selectedOrderItem.is_edited_item_price == "percentage" ? 1 : 0) : 0
            if discountType.selectedSegmentIndex == 1
            {
                let oneHundred = NSDecimalNumber.init(string:"100.0")
                storedDiscount = storedDiscount.multiplying(by: oneHundred).dividing(by: selectedOrderItem.itemPrice_n_uomQty_n_modifier_perItem)
            }
            discountAmount.text = String(format:"%.2f",storedDiscount.doubleValue)
        }
    }
    
    //MARK: - Method to be called to send item back
    func compileItemData()
    {
        selectedOrderItem.allModifiersDict = [String:AnyObject]()
        selectedOrderItem.allModifiersDict[CHOICE] = parentChoiceArr
        selectedOrderItem.allModifiersDict[ADDITION] = parentAddArr
        
        ModelController.shared().currentCompleteOrder.orderItemsArray[selectedOrderIndex] = selectedOrderItem
        
        delegate?.updateSelectedOrderItem()
    }
    
    //MARK: - BillDiscountPopUp delegate method
    func predefinedDiscountSelected(disc: Discount)
    {
        selectedOrderItem.predefined_discount = disc.discount_id
        
        discountType.selectedSegmentIndex = disc.amount == "0" ? 1 : 0
        discountAmount.text = discountType.selectedSegmentIndex == 0 ? disc.amount : disc.percentage
        
        textFieldDidEndEditing(discountAmount)
    }
    
    func predefinedDiscountCleared()
    {
        clearDiscount()
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showDiscountPopUp"
        {
            guard
                let button = sender as? UIButton,
                let dvc = segue.destination as? BillDiscountPopUp,
                let possiblePopover = dvc.popoverPresentationController
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = button.bounds
            possiblePopover.sourceView = button
            dvc.delegate = self
            dvc.selectedPredefinedDiscountID = selectedOrderItem.predefined_discount ?? ""
        }
    }

}
