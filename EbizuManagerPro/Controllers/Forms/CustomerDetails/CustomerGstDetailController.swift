//
//  CustomerGstDetailController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 30/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension CustomerGstDetail
{
    func mandatoryFilled() -> Bool
    {
        if firstName.text == "" || address1.text == "" || email.text == "" || phoneNo.text == "" || postCode.text == ""
        {
            return false
        }
        return true
    }

    func setCustomer()
    {
        gstID.text = ModelController.shared().currentCompleteOrder.customer_gstid ?? ""
        firstName.text = ModelController.shared().currentCustomer.first_name ?? ""
        lastName.text = ModelController.shared().currentCustomer.last_name ?? ""
        email.text = ModelController.shared().currentCompleteOrder.customer_email ?? ""
        phoneNo.text = ModelController.shared().currentCompleteOrder.customer_phone ?? ""
        address1.text = ModelController.shared().currentCompleteOrder.customer_address1 ?? ""
        address2.text = ModelController.shared().currentCompleteOrder.customer_address2 ?? ""
        city.text = ModelController.shared().currentCompleteOrder.customer_city ?? ""
        postCode.text = ModelController.shared().currentCompleteOrder.customer_pincode ?? ""
    }
    
    func updateCustomerData()
    {
        ModelController.shared().currentCustomer.first_name = firstName.text
        ModelController.shared().currentCustomer.last_name = lastName.text
        
        ModelController.shared().currentCompleteOrder.customer_gstid = gstID.text
        ModelController.shared().currentCompleteOrder.customer_name = ModelController.shared().currentCustomer.full_name
        ModelController.shared().currentCompleteOrder.customer_email = email.text
        ModelController.shared().currentCompleteOrder.customer_phone = phoneNo.text
        ModelController.shared().currentCompleteOrder.customer_address1 = address1.text
        ModelController.shared().currentCompleteOrder.customer_address2 = address2.text
        ModelController.shared().currentCompleteOrder.customer_city = city.text
        ModelController.shared().currentCompleteOrder.customer_pincode = postCode.text
    }
}
