//
//  CustomerGstDetail.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 29/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class CustomerGstDetail: UITableViewController
{
    @IBOutlet var gstID: UITextField!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var phoneNo: UITextField!
    @IBOutlet var address1: UITextField!
    @IBOutlet var address2: UITextField!
    @IBOutlet var city: UITextField!
    @IBOutlet var postCode: UITextField!
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        setCustomer()
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func donePressed()
    {
        if mandatoryFilled()
        {
            guard
                let validEmail = email.text?.isValidEmail()
                else { print("error on \(#function), \(#line) "); return}

            if validEmail
            {
                updateCustomerData()
                cancelPressed()
            }
            else
            {
                present_sclalert("Notice", message: "Please enter valid email id", alert_style:.notice)
            }
        }
        else
        {
            present_sclalert("Attention", message: "Please fill in all the fields marked with *", alert_style: .error)
        }
    }
    
    @IBAction func cancelPressed()
    {
        performSegue(withIdentifier: "customerGstDetailReturn", sender: self)
    }
    
    
    
}
