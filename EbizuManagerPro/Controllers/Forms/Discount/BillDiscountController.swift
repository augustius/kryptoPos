//
//  BillDiscountController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 25/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension BillDiscount
{
    func calculateBillDiscount()
    {
        if discountValue.text != ""
        {
            var discountAmount = NSDecimalNumber.init(string:discountValue.text)
            
            if discountAmount != NSDecimalNumber.notANumber
            {
                if discountType.selectedSegmentIndex == 1 // percentage
                {
                    let oneHundred = NSDecimalNumber.init(string: "100.0")
                    let discPerc = discountAmount.dividing(by: oneHundred)
                    //let totalAmountBfrDiscount = currentCompleteOrderCopy.subtotal_bfr_bill_disc.adding(currentCompleteOrderCopy.total_gst_value_bfr_bill_disc)
                    discountAmount = currentCompleteOrderCopy.subtotal_bfr_bill_disc.multiplying(by: discPerc)
                }
                
                currentCompleteOrderCopy.discount_amount = discountAmount
                currentCompleteOrderCopy.discount_percentage = NSDecimalNumber.init(string: discountType.selectedSegmentIndex == 1 ? "1" : "0" )
                adjustView()
            }
        }
    }
    
    func adjustView()
    {
        currentCompleteOrderCopy.newCalculate()
        
        predefinedDiscount.setTitle(currentCompleteOrderCopy.predefined_discount != nil ? shareddb.shared().get_predefined_discount_(by_id: currentCompleteOrderCopy.predefined_discount) : "Discount Type" , for: .normal)
        discountValue.isUserInteractionEnabled = currentCompleteOrderCopy.predefined_discount == nil
        discountType.isUserInteractionEnabled = currentCompleteOrderCopy.predefined_discount == nil
        
        if var discountAmount = currentCompleteOrderCopy.discount_amount // fixed amount stored
        {
            let oneHundred = NSDecimalNumber.init(string: "100.0")
            guard
                let totalAmountBfrDiscount = currentCompleteOrderCopy.subtotal_bfr_bill_disc,
                let totalAmountAfrDiscount = currentCompleteOrderCopy.subtotal
                else { return }
            
            discountType.selectedSegmentIndex = currentCompleteOrderCopy.discount_percentage != nil ? currentCompleteOrderCopy.discount_percentage.intValue : 0
            if discountType.selectedSegmentIndex == 1 //percentage
            {
                discountAmount = discountAmount.dividing(by: totalAmountBfrDiscount).multiplying(by: oneHundred)
            }
            
            totalDue.text = String(format:"%.2f",totalAmountBfrDiscount.doubleValue)
            discountValue.text = String(format:"%.2f",discountAmount.doubleValue)
            discountedTotalDue.text = String(format: "%.2f",totalAmountAfrDiscount.doubleValue)
        }
    }
    
    //MARK: - BillDiscountPopUp delegate method
    func predefinedDiscountSelected(disc: Discount)
    {
        currentCompleteOrderCopy.predefined_discount = disc.discount_id
        
        discountType.selectedSegmentIndex = disc.amount == "0" ? 1 : 0
        discountValue.text = discountType.selectedSegmentIndex == 0 ? disc.amount : disc.percentage
        
        calculateBillDiscount()
    }
    
    func predefinedDiscountCleared()
    {
        currentCompleteOrderCopy.predefined_discount = nil
        
        discountValue.text = "0.00"
        
        calculateBillDiscount()
    }
    
    //MARK: - segue method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showPredefinedDiscount"
        {
            guard
                let button = sender as? UIButton,
                let dvc = segue.destination as? BillDiscountPopUp,
                let possiblePopover = dvc.popoverPresentationController
                else { print("error on \(#function), \(#line) "); return}
            
            possiblePopover.sourceRect = button.bounds
            possiblePopover.sourceView = button
            dvc.delegate = self
            dvc.selectedPredefinedDiscountID = currentCompleteOrderCopy.predefined_discount ?? ""
        }
    }
}
