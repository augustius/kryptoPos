//
//  BillDiscountPopUp.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 26/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

protocol BillDiscountPopUpDelegate: class {
    func predefinedDiscountSelected(disc:Discount)
    func predefinedDiscountCleared()
}

class BillDiscountPopUp: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet var predefinedTableView: UITableView!
    weak var delegate: BillDiscountPopUpDelegate?
    var selectedPredefinedDiscountID = ""
    private var discList = [Any]()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        discList = shareddb.shared().get_predefined_discount()
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func clearDiscount(_ sender: UIBarButtonItem)
    {
        delegate?.predefinedDiscountCleared()
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return discList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "predefinedTableCell", for: indexPath)
        
        guard
            let discData = discList[indexPath.row] as? [AnyHashable : Any],
            let disc = Discount.init(dictionary: discData)
            else { print("error on \(#function), \(#line) "); return cell}

        cell.textLabel?.font = UIFont(name: "Poppins-Light", size: 18)
        cell.textLabel?.text = disc.discount_desc
        cell.accessoryType = selectedPredefinedDiscountID == disc.discount_id ? .checkmark : .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        guard
            let discData = discList[indexPath.row] as? [AnyHashable : Any],
            let disc = Discount.init(dictionary: discData)
            else { print("error on \(#function), \(#line) "); return}
        
        delegate?.predefinedDiscountSelected(disc: disc)
        dismiss(animated: true, completion: nil)
    }
}
