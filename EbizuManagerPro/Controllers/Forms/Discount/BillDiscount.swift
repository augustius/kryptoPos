//
//  BillDiscount.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 25/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class BillDiscount: UITableViewController,UITextFieldDelegate,BillDiscountPopUpDelegate
{
    @IBOutlet var totalDue: UILabel!
    @IBOutlet var predefinedDiscount: UIButton!
    @IBOutlet var discountType: UISegmentedControl!
    @IBOutlet var discountValue: UITextField!
    @IBOutlet var discountedTotalDue: UILabel!
    
    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    var currentCompleteOrderCopy = CompleteOrder()
    
    //MARK: - View Cycle
    override func viewDidLoad()
    {
        if let safeCompleteOrder = ModelController.shared().currentCompleteOrder.copy() as? CompleteOrder
        {
            currentCompleteOrderCopy = safeCompleteOrder
        }
        sendFormToGoogleAnalytic()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        adjustView()
    }
    
    //MARK: - IBAction method
    @IBAction func discountTypeChanged()
    {
        calculateBillDiscount()
    }

    @IBAction func doneClicked()
    {
        textFieldDidEndEditing(discountValue)
        ModelController.shared().currentCompleteOrder = currentCompleteOrderCopy
        performSegue(withIdentifier: "updateBillDiscount", sender: self)    
    }
    
    @IBAction func cancelClicked()
    {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextFieldDelegate method
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        customKeyboard?.textfield = textField
        customKeyboard?.customKeyboardType = .amount

        /// scroll to current textfield cell
        guard
            let pointInTable = textField.superview?.convert(textField.frame.origin, to:tableView),
            let index = tableView.indexPathForRow(at: pointInTable)
            else { return }
        tableView.scrollToRow(at: index, at: .top, animated: true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        calculateBillDiscount()

        /// scroll to current most top cell
        let index = IndexPath(row: 0, section: 0)
        tableView.scrollToRow(at: index, at: .top, animated: true)
    }    
    
}
