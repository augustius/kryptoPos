//
//  ItemDetails.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 03/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum choiceOrAddition
{
    case choice,addition
}

protocol ItemDetailsDelegate: class {
    func updateSelectedOrderItem()
}

class ItemDetails: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,BillDiscountPopUpDelegate
{

    @IBOutlet var itemName: UILabel!
    @IBOutlet var itemPrice: UILabel!
    @IBOutlet var itemStock: UILabel!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var choiceInitial: UILabel!
    @IBOutlet var choiceName: UILabel!
    @IBOutlet var choice: UIButton!
    
    @IBOutlet var additionInitial: UILabel!
    @IBOutlet var additionName: UILabel!
    @IBOutlet var addition: UIButton!
    
    @IBOutlet var selectDiscountBtn: UIButton!
    @IBOutlet var selectCustomBtn: UIButton!
    @IBOutlet var selectCustomPrice: UIView!
    @IBOutlet var selectUomView: UIView!
    
    @IBOutlet var uomView: UIView!
    @IBOutlet weak var uomTopConstraint: NSLayoutConstraint!
    @IBOutlet var uomQty: UITextField!
    @IBOutlet var uomType: UILabel!
    
    @IBOutlet var addDiscountView: UIView!
    @IBOutlet weak var discountTopConstraint: NSLayoutConstraint!
    @IBOutlet var predefinedDiscount: UIButton!
    @IBOutlet var discountType: UISegmentedControl!
    @IBOutlet var discountAmount: UITextField!
    
    @IBOutlet var addNotesView: UIView!
    @IBOutlet weak var remarksTopConstraint: NSLayoutConstraint!
    @IBOutlet var remarks: UITextField!
    
    @IBOutlet var customPriceView: UIView!
    @IBOutlet weak var customPriceTopConstraint: NSLayoutConstraint!
    @IBOutlet var customPrice: UITextField!
    
    weak var delegate: ItemDetailsDelegate?
    
    let customKeyboard = Bundle.main.loadNibNamed("customKeyboard", owner: self, options: nil)?.first as? customKeyboardView
    var option:choiceOrAddition = .choice
    var storedOffsets = [Int: CGFloat]()
    var selectedOrderItem:OrderItem = OrderItem()
    var selectedOrderIndex:Int = 0
    var allModifier = [String:AnyObject]()
    var parentChoiceArr = [[String:AnyObject]]()
    var parentAddArr = [[String:AnyObject]]()
    var showParentAddArr = [[String:AnyObject]]()
    
    // MARK: - View Cycle
    override func viewDidLoad()
    {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 195.0

        setupData()
        setupView()
        sendFormToGoogleAnalytic()
    }
    
    // MARK: - IBAction method
    @IBAction func showChoiceOption()
    {
        executeOption(selectedOption: .choice)
    }
    
    @IBAction func showAdditionOption()
    {
        readjustAdditionDataStructureToShowInCollectionView()
        executeOption(selectedOption: .addition)
    }
    
    @IBAction func addDiscount()
    {
        adjustDiscountView()
    
        self.view.addSubview(addDiscountView)
    }
    
    @IBAction func customizePrice()
    {
        customPrice.text = String(format:"%.2f",selectedOrderItem.totalPrice.doubleValue)
        
        self.view.addSubview(customPriceView)
    }
    
    @IBAction func addNotes()
    {
        remarks.text = selectedOrderItem.remarks
        
        self.view.addSubview(addNotesView)
    }
    
    @IBAction func showUnitOfMeasure()
    {
        uomType.text = selectedOrderItem.type_of_unit
        uomQty.text = String(format:"%.2f",selectedOrderItem.uom_qty.doubleValue)
        
        self.view.addSubview(uomView)
    }
    
    //MARK: - clear button section
    @IBAction func dismissForm()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearUom()
    {
        uomQty.text = "1.00"
        
        textFieldDidEndEditing(uomQty)
    }
    
    @IBAction func clearDiscount()
    {
        selectedOrderItem.predefined_discount = nil
        discountAmount.text = "0.00"
        
        textFieldDidEndEditing(discountAmount)
    }
    
    @IBAction func clearNotes()
    {
        remarks.text = ""
        
        textFieldDidEndEditing(remarks)
    }
    
    @IBAction func clearCustomPrice()
    {
        handleClearCustomPrice()
    }
    
    @IBAction func Done(_ sender: UIButton)
    {
        handleDone(sender: sender)
    }
    
    @IBAction func changeDiscountType()
    {
        textFieldDidEndEditing(discountAmount)
    }
    
    // MARK: - UITableView Delegate Method    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        if let choiceAdditionTableCell = cell as? ChoiceAdditionTableCell
        {
            choiceAdditionTableCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, forRow: indexPath.row)
            choiceAdditionTableCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell,forRowAt indexPath: IndexPath)
    {
        if let choiceAdditionTableCell = cell as? ChoiceAdditionTableCell
        {
            storedOffsets[indexPath.row] = choiceAdditionTableCell.collectionViewOffset
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return getParentChoiceOrAddition().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChoiceAdditionTableCell", for: indexPath)
        
        if let choiceAdditionTableCell = cell as? ChoiceAdditionTableCell
        {
            let tempArrDict = getParentChoiceOrAddition()
            
            if let modifierName = tempArrDict[indexPath.row]["mod_parent_name"] as? String
            {
                choiceAdditionTableCell.modifierName.text = modifierName
            }
            
            return choiceAdditionTableCell
        }
        
        return cell
    }
    
    // MARK: - UICollectionView Delegate Method
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return getChildChoiceOrAddition(index: collectionView.tag).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        var childDict = getChildChoiceOrAddition(index: collectionView.tag)[indexPath.row]
    
        // start putting it on cell
        switch option
        {
        case .choice:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChoiceCollectionCell", for: indexPath)
            
            if let choiceCollectionCell = cell as? ChoiceCollectionCell
            {
                guard
                    let childId = childDict["mod_id"] as? String,
                    let parentID = childDict["mod_parent_id"] as? String,
                    let childName = childDict["mod_name"] as? String,
                    let childPrice = childDict["mod_price"] as? String
                    else{ print("error on \(#function), \(#line) "); return choiceCollectionCell}
                
                choiceCollectionCell.choiceID = childId
                choiceCollectionCell.parentID = parentID
                choiceCollectionCell.price.text = String(format:"%.2f", Double(childPrice) ?? 0.0).addCurrency()
                choiceCollectionCell.initialName.text = childName.initialChar().capitalized
                choiceCollectionCell.name.text = childName.capitalized
                
                choiceCollectionCell.back.addTarget(self, action:#selector(UpdateChoiceData(sender:)), for: .touchUpInside)
                
                let parentDict = getParentChoiceOrAddition()[collectionView.tag]
                if let selectedChoice = parentDict["selected_choice"] as? [String:AnyObject]
                {
                    if let selectedChildId = selectedChoice["mod_id"] as? String
                    {
                        choiceCollectionCell.choosed = childId == selectedChildId
                    }
                }                
                return choiceCollectionCell
            }
            
            return cell

        case .addition:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdditionCollectionCell", for: indexPath)
            
            if let additionCollectionCell = cell as? AdditionCollectionCell
            {                
                guard
                    let childId = childDict["mod_id"] as? String,
                    let childName = childDict["mod_name"] as? String,
                    let childPrice = childDict["mod_price"] as? String,
                    let childQty = childDict["mod_qty"] as? String,
                    let qty = Int(childQty)
                    else{ print("error on \(#function), \(#line) "); return additionCollectionCell}
                
                additionCollectionCell.additionID = childId
                additionCollectionCell.initialName.text = childName.initialChar().capitalized
                additionCollectionCell.name.text = childName
                additionCollectionCell.price.text = String(format:"%.2f", Double(childPrice) ?? 0.0).addCurrency()
                additionCollectionCell.qty = qty
                
                additionCollectionCell.back.addTarget(self, action:#selector(UpdateAdditionData(sender:)), for: .touchUpInside)
                additionCollectionCell.add.addTarget(self, action:#selector(UpdateAdditionData(sender:)), for: .touchUpInside)
                additionCollectionCell.minus.addTarget(self, action:#selector(UpdateAdditionData(sender:)), for: .touchUpInside)
                
                return additionCollectionCell
            }
            
            return cell
        }
    }
    
    //MARK: - UITextField Delegate Method
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return string != "\""
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if let view = textField.superview?.superview?.superview
        {
            switch view
            {
            case addDiscountView:
                discountTopConstraint.constant = discountTopConstraint.constant/4
            case customPriceView:
                customPriceTopConstraint.constant = customPriceTopConstraint.constant/2
            case addNotesView:
                remarksTopConstraint.constant = remarksTopConstraint.constant/2
            case uomView:
                uomTopConstraint.constant = uomTopConstraint.constant/2
            default:
                break
            }
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        if let view = textField.superview?.superview?.superview
        {
            switch view
            {
            case addDiscountView:
                discountTopConstraint.constant = discountTopConstraint.constant*4
            case customPriceView:
                customPriceTopConstraint.constant = customPriceTopConstraint.constant*2
            case addNotesView:
                remarksTopConstraint.constant = remarksTopConstraint.constant*2
            case uomView:
                uomTopConstraint.constant = uomTopConstraint.constant*2
            default:
                break
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField != remarks
        {
            customKeyboard?.textfield = textField
            customKeyboard?.customKeyboardType = .amount
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        switch textField
        {
        case uomQty:
            selectedOrderItem.uom_qty = NSDecimalNumber.init(string:textField.text)
        case discountAmount:
            handleDiscountCalculation(amount: textField.text!)
        case remarks:
            selectedOrderItem.remarks = textField.text
        case customPrice:
            handleCustomPrice(price: textField.text!)
        default:
            break
        }
        
        compileItemData()
    }
    
    
}
