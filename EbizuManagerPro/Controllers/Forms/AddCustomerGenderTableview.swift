//
//  AddCustomerGenderTableview.swift
//  Manager
//
//  Created by augustius cokroe on 16/11/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

protocol AddCustomerGenderDelegate: class {
    func predefinedGenderSelected(G:String)
}

class AddCustomerGenderTableview:UITableViewController
{
    let data = ["Male","Female"]
    var selectedPredefinedGender = ""
    weak var delegate: AddCustomerGenderDelegate?
    
    //UITableviewController delegate
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddCustomerGenderCell", for: indexPath)
        
        let str = data[indexPath.row]
        cell.textLabel?.font = UIFont(name: "Poppins-Light", size: 18)
        cell.textLabel?.text = str
        cell.accessoryType = selectedPredefinedGender == str ? .checkmark : .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return data.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        delegate?.predefinedGenderSelected(G: data[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
    
    
}
