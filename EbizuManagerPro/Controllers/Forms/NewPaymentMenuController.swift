//
//  NewPaymentMenuController.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 15/08/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension NewPaymentMenu
{
    func setupData()
    {
//        setupRHB()
        setupPaymentData()
        detailShown = false
        multiPayEnabled = false
        setOrderChargeDetails()
        setTableView()
        setCustomerView()
        setupPaymentView()
        setShortCutView()
    }
    
//    func setupRHB()
//    {
//        notification.addObserver(self, selector: #selector(accessoryDidConnect(notification:)), name: NSNotification.Name("EAAccessoryDidConnectNotification"), object: nil)
//        notification.addObserver(self, selector: #selector(accessoryDidDisconnect(notification:)), name: NSNotification.Name("EAAccessoryDidDisconnectNotification"), object: nil)
//        notification.addObserver(self, selector: #selector(sessionDataReceived(notification:)), name: NSNotification.Name("EADSessionDataReceivedNotification"), object: nil)
//        EAAccessoryManager.shared().registerForLocalNotifications()
//        accessoryList = EAAccessoryManager.shared().connectedAccessories
//
//        eaSessionController = EADSessionController.shared()
//    }

    func setupPaymentData()
    {
        paymentMethodArray = (userDefaults.array(forKey: ALL_PAYMENT_METHOD) as? [[String : String]]) ?? [[String:String]]()
        
        let tempArr = paymentMethodArray.filter { $0["type_id"] == "99" } // get only mutiple payment type
        print(tempArr)
        if tempArr.count > 0
        {
            if let firstArr = tempArr.first
            {
                guard
                    let paymentId = firstArr["id"],
                    let paidAmountStr = firstArr["paid_amount"],
                    let paymentName = firstArr["pay_name"],
                    let paymentType = firstArr["type_id"],
                    let paymentTypeName = firstArr["type_name"],
                    let creditCardNumber = firstArr["credit_card_no"]
                    else { print("error on \(#function), \(#line) "); return}
                
                let paidAmount = NSDecimalNumber.init(string: paidAmountStr)
                currentMultiPaymentMethod = paymentMethodData.init(id: paymentId, name: paymentName, creditCardNo: creditCardNumber, type: paymentType, typeName: paymentTypeName.uppercased(), amountTendered: paidAmount)
            }
        }
        
        paymentMethodArray = paymentMethodArray.filter { $0["type_id"] != "99" } // filter out the multiplePayment
        finalAmountTenderedMultiPay = NSDecimalNumber.init(value:paymentMethodArray.compactMap({ Double($0["paid_amount"] ?? "0.0")}).reduce(0, +))
    }
    
    func setupPaymentView()
    {
        payMethodName.text = ""
        multiPaySwitchView.isHidden = (currentMultiPaymentMethod.id == "") ? true : false
        paymentView.addSubview(paymentOptionView)
        paymentOptionCollectionView.reloadData()
    }
    
    func setOrderChargeDetails()
    {
        orderNo.text = "Order # " + (ModelController.shared().currentCompleteOrder.hold_order_id ?? "")
        OrderType.setTitle(ModelController.shared().currentCompleteOrder.order_type == 0 ? "Dine In" : "Take Away", for: .normal)
        
        serviceCharge.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.tax_amount.doubleValue)
        discount.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.discount_amount.doubleValue)
        voucher.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.voucher_amount.doubleValue)
        gst.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.total_gst_value.doubleValue)
        roundingAdj.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.rounding_amount.doubleValue)
        total.text = String(format:"%.2f",ModelController.shared().currentCompleteOrder.total.doubleValue).addCurrency()
    }
    
    func setTableView()
    {
        tableExist = false
        if ModelController.shared().currentCompleteOrder.table_no != nil
        {
            if ModelController.shared().currentCompleteOrder.table_no != ""
            {
                noOfPax.setTitle(ModelController.shared().currentCompleteOrder.no_of_pax.description, for: .normal)
                selectedTableArray.append(ModelController.shared().currentCompleteOrder.table_no)
                if let allSelectedTable = shareddb.shared().getAllTableNameThatMerge(into: ModelController.shared().currentCompleteOrder.tbl_primary_id) as? [String]
                {
                    selectedTableArray += allSelectedTable
                }
                tableExist = true
            }
        }
    }
    
    func setCustomerView()
    {
//        if ModelController.shared().currentCustomer.first_name != nil
//        {
            customerName.text = ModelController.shared().currentCompleteOrder.customer_name
            customerPic.setBackgroundImage(ModelController.shared().currentCustomer.storedProfilePicUIImage, for: .normal)
//        }
    }
    
    func setShortCutView()
    {
        let total = ModelController.shared().currentCompleteOrder.total.doubleValue
        let totalInt = ModelController.shared().currentCompleteOrder.total.intValue
        
        shortcut1.setTitle(String(format:"%.2f",total), for: .normal)
        
        var roundTotal = ceil(total)
        shortcut2.setTitle(String(format:"%.2f",roundTotal), for: .normal)

        let quotient = totalInt / 50
        roundTotal = Double(quotient * 50) + 50
        shortcut3.setTitle(String(format:"%.2f",roundTotal), for: .normal)
    }
    
    func addAmountComplete()
    {
        if let index = paymentMethodArray.index(where: {$0["id"] == currentSelectedPaymentMethod.id})
        {
            var tempDict = paymentMethodArray[index]
            tempDict["paid_amount"]     = String(format:"%.2f",currentSelectedPaymentMethod.amountTendered.doubleValue)
            tempDict["credit_card_no"]  = currentSelectedPaymentMethod.creditCardNo
            
            print(tempDict)
            paymentMethodArray[index] = tempDict
            finalAmountTenderedMultiPay = NSDecimalNumber.init(value:paymentMethodArray.compactMap({ Double($0["paid_amount"] ?? "0.0")}).reduce(0, +))
            setupPaymentView()
        }
    }
    
    func paymentComplete()
    {
        let paymentMethodId = multiPayEnabled ? currentMultiPaymentMethod.id : currentSelectedPaymentMethod.id
        let payment = multiPayEnabled ? currentMultiPaymentMethod.type : currentSelectedPaymentMethod.type
        let payTypeName = multiPayEnabled ? currentMultiPaymentMethod.typeName : currentSelectedPaymentMethod.typeName
        let payCreditCardNo = multiPayEnabled ? currentMultiPaymentMethod.creditCardNo : currentSelectedPaymentMethod.creditCardNo
        let payAmount = multiPayEnabled ? finalAmountTenderedMultiPay : finalAmountTendered
        var paymentChange =  payAmount.subtracting(ModelController.shared().currentCompleteOrder.total)
        paymentChange = (paymentChange == NSDecimalNumber.notANumber) ? NSDecimalNumber.init(string:"0.00") : paymentChange
        
        // get only mutiple payment type with amount in it
        completeMultiPaymentData =  paymentMethodArray.filter { (Double($0["paid_amount"] ?? "0.00") ?? 0.0) > 0.0 }
            
        if  !(ModelController.shared().currentCompleteOrder.total.doubleValue > payAmount.doubleValue)
        {
            ModelController.shared().currentCompleteOrder.payment_method_id = paymentMethodId
            ModelController.shared().currentCompleteOrder.payment = payment
            ModelController.shared().currentCompleteOrder.pay_type_name = payTypeName
            ModelController.shared().currentCompleteOrder.credit_card_no = payCreditCardNo
            ModelController.shared().currentCompleteOrder.payment_amount = payAmount
            ModelController.shared().currentCompleteOrder.payment_changes = paymentChange

            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "showPaymentComplete", sender: self)
            }
        }
        else //extra checking only although dont think that it can breach until here
        {
            present_sclalert("Error", message: "Amount tendered is not enough. Please key-in more amount.", alert_style: .error)
        }
    }
    
    //MARK: Notication method for RHB
//    func accessoryDidConnect(notification: NSNotification)
//    {
//        if let userInfoDict = notification.userInfo as? [String:AnyObject]
//        {
//            if let connectedAccessory = userInfoDict[EAAccessoryKey] as? EAAccessory
//            {
//                accessoryList.append(connectedAccessory)
//            }
//        }
//    }
    
//    func accessoryDidDisconnect(notification: NSNotification)
//    {
//        accessoryList = EAAccessoryManager.shared().connectedAccessories
//
//        if let userInfoDict = notification.userInfo as? [String:AnyObject]
//        {
//            if let disconnectedAccessory = userInfoDict[EAAccessoryKey] as? EAAccessory
//            {
//                if (disconnectedAccessory.connectionID == selectedAccessory.connectionID)
//                {
//                    hideLoadingScreen()
//                    present_sclalert("Disconnect", message: "RHB CardBiz Device disconnected.\n Please check the device connection.\n Thanks", alert_style: .error)
//                }
//            }
//        }
//    }

//    func sessionDataReceived(notification: NSNotification)
//    {
//        if let sessionController = notification.object as? EADSessionController
//        {
//            var bytesAvailable = sessionController.readBytesAvailable()
//
//            while bytesAvailable > 0
//            {
//                bytesAvailable = sessionController.readBytesAvailable()
//                if (sessionController.readData(bytesAvailable)) != nil
//                {
//                    totalBytesRead += bytesAvailable
//                }
//            }
//        }
//    }

    //MARK: - 3rdParty / External Payment method 
    func sendAmountToExternalPayment()
    {
        // decide which external payment 
        switch currentSelectedPaymentMethod.typeName
        {
        case MPAY:
            ExPayment = Mpay.SharedExPayment()
        case MAYBANK:
            ExPayment = MaybankMPOS.SharedExPayment()
//        case RHBCardBiz:
//            ExPayment = RHB.SharedExPayment()
//            if accessoryList.count > 0
//            {
//                for accessory in accessoryList
//                {
//                    if accessory.protocolStrings.count > 0
//                    {
//                        if accessory.protocolStrings.first == "com.castles.protocolCastles"
//                        {
//                            selectedAccessory = accessory
//                            break
//                        }
//                    }
//                }
//
//                if selectedAccessory.protocolStrings.first == "com.castles.protocolCastles"
//                {
//                    eaSessionController?.setupController(for: selectedAccessory, withProtocolString: selectedAccessory.protocolStrings.first ?? "")
//                    eaSessionController?.openSession()
//                    totalBytesRead = 0
//                }
//                else
//                {
//                    present_sclalert("RHB CardBiz Device Not Found", message: "Please make sure RHB CardBiz Device has been connected via bluetooth", alert_style: .error)
//                    return
//                }
//            }
//            else
//            {
//                present_sclalert("RHB CardBiz Device Not Found", message: "Please make sure RHB CardBiz Device has been connected via bluetooth", alert_style: .error)
//                return
//            }
//            break
        default:
            present_sclalert("ERROR", message: "Unrecognized external payment method", alert_style: .error)
            return
        }
        
        // send amount to decided external payment
        showLoadingScreen()
        notification.addObserver(self, selector: #selector(handleThirdPartyPayment), name: NSNotification.Name("RESPONSES_RECEIVE"), object: nil)
        
        let amountToBeSent = multiPayEnabled ? String(format:"%.2f",currentSelectedPaymentMethod.amountTendered.doubleValue) : String(format:"%.2f",finalAmountTendered.doubleValue)
        
        if !ExPayment.sendPaymentAmount(amount: amountToBeSent)
        {
            hideLoadingScreen()
            present_sclalert("Receiver Not Found", message: "The Receiver App is not installed. Receiver App must be installed to send payment.", alert_style: .error)
        }
    }
    
    @objc func handleThirdPartyPayment(notification: NSNotification)
    {
        hideLoadingScreen()
        
        // decide which external payment that responsed
        if let url = notification.object as? NSURL
        {
            if url.scheme == "POSPay" //Mpay
            {
                ExPayment = Mpay.SharedExPayment()
            }
            else if url.scheme == "com.ebizu.Ebizu-Manager" //maybank MPos
            {
                ExPayment = MaybankMPOS.SharedExPayment()
            }
        }
        else // RHB payment
        {
            ExPayment = RHB.SharedExPayment()
        }
        
        // extract data from notification
        let response = ExPayment.checkPaymentResponse(response: notification.object as AnyObject)
        
        let responseStatus = response[RESPONSECARDSTAT] ?? ""
        
        switch responseStatus
        {
        case SUCCESSSCHEME:
            let creditCardNo = response[RESPONSECARDNO] ?? ""
            currentSelectedPaymentMethod.creditCardNo = creditCardNo

            if multiPayEnabled {
                addAmountComplete()
            } else {
                paymentComplete()
            }
        case FAILURESCHEME, UNKNOWNSCHEME:
            let responseCode = response["RESPONSECARDCODE"] ?? ""
            let responseMsg = response["RESPONSECARDMSG"] ?? ""
            present_sclalert("Payment Failed", message: "Payment has failed with response code : " + responseCode + " " + responseMsg, alert_style: .error)
        case CANCELSCHEME:
            present_sclalert("Payment Canceled", message: "Payment has been canceled", alert_style: .notice)
        default:
            break
        }
    }

    //MARK: - Maybank Qr Method
    func sendAmountToQrPage() {
        performSegue(withIdentifier: "showQrPay", sender: nil)
    }
    
    //MARK: - Segue Method
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "showPaymentComplete"
        {
            guard
                let dvc = segue.destination as? NewPaymentComplete
                else { print("error on \(#function), \(#line) "); return}
            
            dvc.completeMultiPayData = completeMultiPaymentData            
        } else if segue.identifier == "showQrPay" {
            guard let dvc = segue.destination as? NewPaymentQrPay
                else { print("error on \(#function), \(#line) "); return}

            let amountToBeSent = multiPayEnabled ? String(format:"%.2f",currentSelectedPaymentMethod.amountTendered.doubleValue) : String(format:"%.2f",finalAmountTendered.doubleValue)

            dvc.amountStr = amountToBeSent
            dvc.paymentMethodId = currentSelectedPaymentMethod.id
            dvc.transRefId = (userDefaults.string(forKey: HARDWARE_CODE) ?? "") + String(format:"-%.0f",NSDate().timeIntervalSince1970)
        }
    }

    @IBAction func qrPayDoneReturnSegue(segue: UIStoryboardSegue) {
        if segue.identifier == "qrPayDone" {
            guard
                let svc = segue.source as? NewPaymentQrPay
                else { print("error on \(#function), \(#line) "); return}

            currentSelectedPaymentMethod.creditCardNo = svc.qrRefId
            if multiPayEnabled {
                addAmountComplete()
            } else {
                paymentComplete()
            }
        }

    }
}
