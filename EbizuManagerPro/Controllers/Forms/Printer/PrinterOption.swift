//
//  PrinterOption.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 19/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class PrinterHeaderView: UICollectionReusableView
{
    @IBOutlet var title: UILabel!
}

class BillPrinterOptionCell: UICollectionViewCell
{
    @IBOutlet var name: UILabel!
    @IBOutlet private var tickedImage: UIImageView!
    
    var ticked = false
    {
        didSet{
            self.tickedImage.isHidden = !self.ticked
        }
    }
}

class KitchenPrinterOptionCell: UICollectionViewCell
{
    @IBOutlet var name: UILabel!
    @IBOutlet private var tickedImage: UIImageView!
    @IBOutlet var remarks: UITextField!
    
    var ticked = false
    {
        didSet{
            self.tickedImage.isHidden = !self.ticked
        }
    }
}

class PrinterOption:UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate
{
    @IBOutlet var printerOptionCollectionView: UICollectionView!
    
    @IBOutlet var selectAllButton: UIButton!
    
    var allPrinter = [[String:String]]()
    var allSelectedPrinter = [[String:String]]()
    var billPrinter = [[String:String]]()
    var kitchenPrinter = [[String:String]]()
    let tickedImage = UIImage(named: "tick_hold_order")
    var originalCollectionViewOffset: CGPoint?
    var forPrintBill = true
    
    //MARK: - View Cylce
    override func viewDidLoad()
    {
        if let getAllPrinters = shareddb.shared().get_list_of_printers() as? [[String : String]]
        {
            allPrinter = getAllPrinters
            if allPrinter.count > 0
            {
                billPrinter = allPrinter.filter{ $0["printer_type"] == "Bill" }
                kitchenPrinter = allPrinter.filter{ $0["printer_type"] == "Kitchen" }
                decideToTickSelectAllOrNot()
            }
        }
        sendFormToGoogleAnalytic()
    }
    
    //MARK: - IBAction method
    @IBAction func cancelPressed()
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func donePressed()
    {
        if allSelectedPrinter.count > 0
        {
            if forPrintBill {
                EbizuPrinter.sharedPrinter.receiptType = .bill
            }
            EbizuPrinter.sharedPrinter.completeOrder = ModelController.shared().currentCompleteOrder
            EbizuPrinter.sharedPrinter.printBill(printerArr: allSelectedPrinter as [[String : AnyObject]])
            cancelPressed()
        }
        else
        {
            present_sclalert("Fail", message: "Please select at least one printer to continue printing", alert_style: .error)
        }
    }
    
    @IBAction func selectAllPrinter()
    {
        // bill
        var tempBillArr = [[String:String]]()
        for dict in billPrinter
        {
            var tempDict = dict
            tempDict["default_printer"] = selectAllButton.image(for: .normal) != nil ? "0" : "1"
            
            tempBillArr.append(tempDict)
        }
        billPrinter = tempBillArr
        
        // kitchen
        var tempKitchenArr = [[String:String]]()
        for dict in kitchenPrinter
        {
            var tempDict = dict
            tempDict["default_printer"] = selectAllButton.image(for: .normal) != nil ? "0" : "1"
            
            tempKitchenArr.append(tempDict)
        }
        kitchenPrinter = tempKitchenArr

        decideToTickSelectAllOrNot()
        printerOptionCollectionView.reloadData()
    }
    
    func decideToTickSelectAllOrNot()
    {
        // decide whether to check or not to check list button
        allPrinter = billPrinter + kitchenPrinter
        allSelectedPrinter = allPrinter.filter{ $0["default_printer"] == "1" }
        selectAllButton.setImage(allSelectedPrinter.count == allPrinter.count ? tickedImage : nil, for: .normal)
    }
    
    //MARK: - UICollectioViewDelegate method
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return kitchenPrinter.count > 0 ? 2 : 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return section == 0 ? billPrinter.count : kitchenPrinter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        let headerView = collectionView .dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "PrinterHeaderView", for: indexPath)
        
        if kind == UICollectionView.elementKindSectionHeader
        {
            if let printerHeaderView = headerView as? PrinterHeaderView
            {
                printerHeaderView.title.text = indexPath.section == 0 ? "Bill Printer" : "Kitchen Printer"
                
                return printerHeaderView
            }
        }
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return indexPath.section == 0 ? CGSize.init(width: 100, height: 100) : CGSize.init(width: 460, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if indexPath.section == 0
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BillPrinterOptionCell", for: indexPath)
            
            if let billPrinterOptionCell = cell as? BillPrinterOptionCell
            {
                let printer = billPrinter[indexPath.row]
                guard
                    let printerName = printer["printer_name"],
                    let defaultPrinter = printer["default_printer"]
                    else { print("error on \(#function), \(#line) "); return billPrinterOptionCell}
    
                let isDefaultPrinter = defaultPrinter == "1"
    
                billPrinterOptionCell.name.text = printerName
                billPrinterOptionCell.ticked = isDefaultPrinter
                
                return billPrinterOptionCell
            }
            
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "KitchenPrinterOptionCell", for: indexPath)
            
            if let kitchenPrinterOptionCell = cell as? KitchenPrinterOptionCell
            {
                let printer = kitchenPrinter[indexPath.row]
                let optionalString:String? = ""
                guard
                    let printerName = printer["printer_name"],
                    let printerRemarks = printer["printer_remark"] ?? optionalString,
                    let defaultPrinter = printer["default_printer"]
                    else { print("error on \(#function), \(#line) "); return kitchenPrinterOptionCell}
                
                let isDefaultPrinter = defaultPrinter == "1"
                
                kitchenPrinterOptionCell.name.text = printerName
                kitchenPrinterOptionCell.remarks.delegate = self
                kitchenPrinterOptionCell.remarks.tag = indexPath.row
                kitchenPrinterOptionCell.remarks.text = printerRemarks
                kitchenPrinterOptionCell.ticked = isDefaultPrinter
                
                return kitchenPrinterOptionCell
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath.section == 0
        {
            let printer = billPrinter[indexPath.row]
    
            guard
                let defaultPrinter = printer["default_printer"]
                else { print("error on \(#function), \(#line) "); return}
    
            billPrinter[indexPath.row]["default_printer"] = defaultPrinter == "1" ? "0" : "1"
        }
        else
        {
            let printer = kitchenPrinter[indexPath.row]
    
            guard
                let defaultPrinter = printer["default_printer"]
                else { print("error on \(#function), \(#line) "); return}
    
            kitchenPrinter[indexPath.row]["default_printer"] = defaultPrinter == "1" ? "0" : "1"
        }
        
        decideToTickSelectAllOrNot()
        printerOptionCollectionView.reloadData()
    }
    
    //MARK: - UITextfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // get offset value of selected textfield
        let pointInTable:CGPoint = textField.superview!.convert(textField.frame.origin, to:printerOptionCollectionView)
        var contentOffset:CGPoint = printerOptionCollectionView.contentOffset
        // keep actual original if user decide to switch texfield without triggering the textFieldShouldEndEditing
        if originalCollectionViewOffset == nil {
            originalCollectionViewOffset = contentOffset
        } else if let safeOriginalOffset = originalCollectionViewOffset {
            contentOffset = safeOriginalOffset
        }
        contentOffset.y  = pointInTable.y - 100
        if let accessoryView = textField.inputAccessoryView
        {
            contentOffset.y -= accessoryView.frame.size.height
        }
        printerOptionCollectionView.contentOffset = contentOffset
    }

    func textFieldDidEndEditing(_ textField: UITextField)
    {
        let index = textField.tag
        kitchenPrinter[index]["printer_remark"] = textField.text ?? ""
        decideToTickSelectAllOrNot()
        if let originalOffset = originalCollectionViewOffset {
            printerOptionCollectionView.contentOffset = originalOffset
            originalCollectionViewOffset = nil
        }
    }
}
