
//
//  Epson80mm.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 15/08/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

typealias CompileObj = (copies: Int, data: receiptData, fromCut: Bool)

@objc class Epson80mm: NSObject, PrinterProtocol, Epos2PtrReceiveDelegate {
    // MARK: Singleton Declaration
    // swiftSharedInstance is not accessible from ObjC
    class var swiftSharedPrinter: Epson80mm {
        struct Singleton {
            static let sharedPrinter = Epson80mm()
        }
        return Singleton.sharedPrinter
    }

    // the sharedInstance class method can be reached from ObjC
    class func sharedPrinter() -> Epson80mm {
        return Epson80mm.swiftSharedPrinter
    }

    fileprivate override init() { // Prevent use of initialisation of class
        ip_address = ""
    }

    private var valuePrinterSeries: Epos2PrinterSeries = EPOS2_TM_M10
    private var valuePrinterModel: Epos2ModelLang = EPOS2_MODEL_ANK

    var ip_address: String?
    private var currentPrinter: Epos2Printer!

    private func cleanPrinter() {
        currentPrinter.endTransaction()
        currentPrinter.disconnect()
        currentPrinter.clearCommandBuffer()
        currentPrinter.setReceiveEventDelegate(nil)
        currentPrinter = nil
    }

    private func printerConnected() -> Bool {
        currentPrinter = Epos2Printer(printerSeries: valuePrinterSeries.rawValue, lang: valuePrinterModel.rawValue)
        if currentPrinter == nil {
            return false
        } else {
            currentPrinter.setReceiveEventDelegate(self)
            return true
        }
    }

    private func printCopies() {
        /// initiate connection to printer
        guard let safeIpAddress = ip_address else { return }

        let isIpAddress = safeIpAddress.containIpAddress()
        let finalTarget = isIpAddress ? "TCP:\(safeIpAddress)" : safeIpAddress
        print("final Target : \(finalTarget)")

        var result = currentPrinter.connect(finalTarget, timeout:Int(EPOS2_PARAM_DEFAULT))
        if result != EPOS2_SUCCESS.rawValue {
            cleanPrinter()
            print("fail to connect printer")
            return
        }

        result = currentPrinter.beginTransaction()
        if result != EPOS2_SUCCESS.rawValue {
            cleanPrinter()
            print("fail to begin printer transaction")
            return
        }

        /// print no of copies
        result = currentPrinter.sendData(Int(EPOS2_PARAM_DEFAULT))
        if result != EPOS2_SUCCESS.rawValue {
            cleanPrinter()
            print("epson80mm fail to print")
        }
    }

    // MARK: - printer protocol method
    func openCashDrawer() {
        if printerConnected() {
            currentPrinter.addPulse(0, time: 0)
            printCopies()
        }
    }

    func printReciept(printerReceiptData:[receiptData], openCashRegister: Bool) {
        if printerConnected() {

            actualCompileData(printerReceiptData)

            if openCashRegister {
                currentPrinter.addPulse(0, time: 0)
            }
            printCopies()
        }
    }

    func actualCompileData(_ printerReceiptData:[receiptData]) {

        var tempReceiptData: [receiptData] = []

        for data in printerReceiptData {
            let compileObj = compileData(data)

            tempReceiptData.append(compileObj.data)

            if compileObj.fromCut {
                if compileObj.copies > 0 {
                    actualCompileData(tempReceiptData)
                } else {
                    tempReceiptData.removeAll()
                }
            }
        }
    }

    func compileData(_ data: receiptData) -> CompileObj {
        switch data.receiptDataType {
        case .image:
            currentPrinter.addTextAlign(1)
            currentPrinter.add(data.image, x:0, y:0,
                             width:Int(data.image.size.width),
                             height:Int(data.image.size.height),
                             color:EPOS2_PARAM_DEFAULT,
                             mode:EPOS2_PARAM_DEFAULT,
                             halftone:EPOS2_PARAM_DEFAULT,
                             brightness:3,
                             compress:EPOS2_PARAM_DEFAULT)
            currentPrinter.addFeedUnit(0)
        case .text, .line, .modifierText:
            var str = data.text80mm //modifierText
            if data.receiptDataType == .line {
                str = "".padding(toLength: 44, withPad: data.text80mm, startingAt: 0)
            } else if data.receiptDataType == .text {
                str = data.text80mm.inserting(separator: "\n", every: 44)
            }
            let extraSpace = data.extraWitdh == 0 ? "  " : " "
            switch data.textAlignment
            {
            case .left:
                currentPrinter.addTextAlign(0)
                str = extraSpace + str.replace("\n", replacement: "\n" + extraSpace);
            case .right:
                currentPrinter.addTextAlign(2)
                str = extraSpace + str.replace("\n", replacement: "\n" + extraSpace);
            case .center: currentPrinter.addTextAlign(1)
            }
            switch data.textFont
            {
            case .fontA: currentPrinter.addTextFont(0)
            case .fontB: currentPrinter.addTextFont(1)
            }

            currentPrinter.addTextLang(0)
            currentPrinter.addLineSpace(5+data.extraLineSpace)
            currentPrinter.addTextStyle(EPOS2_PARAM_DEFAULT, ul:EPOS2_PARAM_DEFAULT, em:data.textBold ? 1 : 0, color:EPOS2_PARAM_DEFAULT)
            currentPrinter.addTextSize(1+data.extraWitdh, height: 1+data.extraHeight)
            currentPrinter.addFeedUnit(10)
            currentPrinter.addText(str+"\n")

            /// august here to print out receipt sample
            print(str)
        case .cutPaper:
            print("**************************************")
            currentPrinter.addFeedUnit(100)
            currentPrinter.addCut(1)

            var finalData = data
            finalData.numberOfCopies -= 1
            let compileObj: CompileObj = (finalData.numberOfCopies, finalData, true)
            return compileObj
        }

        let compileObj: CompileObj = (0, data, false)
        return compileObj
    }

    func onPtrReceive(_ printerObj: Epos2Printer!, code: Int32, status: Epos2PrinterStatusInfo!, printJobId: String!) {
        self.cleanPrinter()
    }
}
