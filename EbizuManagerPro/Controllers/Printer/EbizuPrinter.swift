//
//  EbizuPrinter.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 16/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct receiptData {
    var receiptDataType:receiptDataType = .text
    var image = UIImage()
    var text80mm = "" //line length = 44
    var text58mm = "" //line length = 32
    var textBold = false
    var textAlignment:textAlignment = .left
    var textFont:textFont = .fontA
    var extraLineSpace:Int = 0
    var extraWitdh: Int = 0
    var extraHeight: Int = 0
    var numberOfCopies: Int = 0
}

extension receiptData {

    init(type: receiptDataType, image: UIImage, text80mm: String, text58mm: String, textBold: Bool, textAlignment: textAlignment, textFont: textFont, extraLineSpace: Int, extraWitdh: Int, extraHeight: Int, numberOfCopies: Int = 0) {
        self.receiptDataType = type
        self.image = image
        self.text80mm = text80mm
        self.text58mm = text58mm
        self.textBold = textBold
        self.textAlignment = textAlignment
        self.textFont = textFont
        self.extraLineSpace = extraLineSpace
        self.extraWitdh = extraWitdh
        self.extraHeight = extraHeight
        self.numberOfCopies = numberOfCopies
    }

    static func paperCut(_ copies: Int) -> receiptData {
        return receiptData(receiptDataType: .cutPaper, image: UIImage(), text80mm: "", text58mm: "", textBold: false, textAlignment: .center, textFont: .fontA, extraLineSpace: 0, extraWitdh: 0, extraHeight: 0, numberOfCopies: copies)
    }
}

struct receiptHeaderData {
    var merchantLogo:UIImage? = UIImage()
    var merchantName = ""
    var showMerchantName = false
    var merchantAddress = ""
    var showMerchantAddress = false
    var merchantPhone = ""
    var showMerchantPhone = false
}

struct receiptFooterData {
    var wifiName = ""
    var wifiPass = ""
    var footerMessage = ""
}

struct kitchenReceiptData {
    var receipt = [receiptData]()
    var numberOfCopies = 1
    var ipAddress = ""
}

struct cashRegiterReceiptData {
    var cashType = ""
    var amount = 0.00
    var notes = ""
}

enum textAlignment
{
    case center, left, right
}

enum textFont
{
    case fontA, fontB
}

enum receiptDataType
{
    case text, image, line, modifierText, cutPaper
}

enum receiptTypeTitle:String
{
    case refund         = "REFUND"
    case partialRefund  = "PARTIAL REFUND"
    case gstFullTax     = "SIMPLIFIED TAX INVOICE"
    case bill           = "BILL"
    case normal         = ""
}

@objc class EbizuPrinter:NSObject
{
    internal var receiptDataArr = [receiptData]()
    internal var selectedPrinter:PrinterProtocol = Epson80mm.sharedPrinter()
    internal var reprint = false
    var receiptType:receiptTypeTitle = .normal
    var completeOrder = CompleteOrder()
    private let dateFormatter = DateFormatter()
    private let timesExtraHeight:CGFloat = 0.9
    
    //MARK: - Initiate method
    class var sharedPrinter: EbizuPrinter
    {
        struct Singleton
        {
            static let sharedPrinter = EbizuPrinter()
        }
        return Singleton.sharedPrinter
    }
    
    //MARK: - Basic method
    private func addString(_ text80mm:String)
    {
        addString(text80mm, smallerText58mm: text80mm)
    }
    
    private func addString(_ text80mm:String, smallerText58mm text58mm:String)
    {
        addString(text80mm, smallerText58mm: text58mm, textAlignment: .left)
    }
    
    private func addString(_ text80mm:String, smallerText58mm text58mm:String, textAlignment alignment:textAlignment)
    {
        addString(text80mm, smallerText58mm: text58mm, textAlignment: alignment, textFont: .fontA)
    }
    
    private func addString(_ text80mm:String, smallerText58mm text58mm:String, textAlignment alignment:textAlignment, textFont font:textFont)
    {
        addString(text80mm, smallerText58mm: text58mm, textAlignment: alignment, textFont: font, extraLineSpace: 0)
    }
    
    private func addString(_ text80mm:String, smallerText58mm text58mm:String, textAlignment alignment:textAlignment, textFont font:textFont, extraLineSpace lineSpace:Int)
    {
        addString(text80mm, smallerText58mm: text58mm, textAlignment: alignment, textFont: font, extraLineSpace: lineSpace, withBold: false)
    }
    
    private func addString(_ text80mm:String, smallerText58mm text58mm:String, textAlignment alignment:textAlignment, textFont font:textFont, extraLineSpace lineSpace:Int, withBold bold:Bool)
    {
        addString(text80mm, smallerText58mm: text58mm, textAlignment: alignment, textFont: font, extraLineSpace: lineSpace, withBold: bold, extraWidth: 0, extraHeight: 0)
    }
    
    private func addString(_ text80mm:String, smallerText58mm text58mm:String, textAlignment alignment:textAlignment, textFont font:textFont, extraLineSpace lineSpace:Int, withBold bold:Bool, extraWidth width:Int, extraHeight height:Int)
    {
        let data = receiptData(type: .text, image: UIImage(), text80mm: text80mm, text58mm: text58mm, textBold: bold, textAlignment: alignment, textFont: font, extraLineSpace: lineSpace, extraWitdh: width, extraHeight: height)
        receiptDataArr.append(data)
    }

    private func addStringModifier(_ text80mm:String) {
        addStringModifier(text80mm, smallerText58mm: text80mm)
    }

    private func addStringModifier(_ text80mm:String, smallerText58mm text58mm:String, textAlignment alignment:textAlignment = .left, textFont font:textFont = .fontA, extraLineSpace lineSpace:Int = 0, withBold bold:Bool = false, extraWidth width:Int = 0, extraHeight height:Int = 0)
    {
        let data = receiptData(type: .modifierText, image: UIImage(), text80mm: text80mm, text58mm: text58mm, textBold: bold, textAlignment: alignment, textFont: font, extraLineSpace: lineSpace, extraWitdh: width, extraHeight: height)
        receiptDataArr.append(data)
    }
    
    private func addImage(_ image:UIImage)
    {
        let convertedImage = image.ConvertToStandardSize(300)
        let data = receiptData(type: .image, image: convertedImage, text80mm: "", text58mm: "", textBold: false, textAlignment: .center, textFont: .fontA, extraLineSpace: 0, extraWitdh: 0, extraHeight: 0)
        receiptDataArr.append(data)
    }
    
    private func addLineSpace(_ lineSpace:Int = 1)
    {
        for _ in 0...lineSpace
        {
            addString("")
        }
    }
    
    private func addLine()
    {
        let data = receiptData(type: .line, image: UIImage(), text80mm: "-", text58mm: "-", textBold: false, textAlignment: .center, textFont: .fontA, extraLineSpace: 0, extraWitdh: 0, extraHeight: 0)
        receiptDataArr.append(data)
    }
    
    private func add(text:String = "", givenSpace space:Int, alignment align:TextAlignment = .left) -> String
    {
        return text.with(givenSpace: space, alignment: align)
    }
    
    //MARK: - Receipt Data Method
    //MARK: Sample Receipt Only
    internal func getSampleHeader(sampleHeader: receiptHeaderData)
    {
        getHeaderMessage(header: sampleHeader)
    }
    
    internal func getSampleBody(sampleCompleteOrder: CompleteOrder)
    {
        completeOrder = sampleCompleteOrder
        receiptType = .normal
        reprint = false
        getSalesReciept()
    }
    
    internal func getSampleFooter(sampleFooter: receiptFooterData)
    {
        getFooterMessage(footer: sampleFooter)
    }
    
    //MARK: Actual Receipt
    internal func getTestPrintMessage()
    {
        if let business_name = StringUtils.get_business_detail("business_name")
        {
            self.addString(String(format:"Hello %@, if you see this text then your printer is ready",business_name))
        }
        else
        {
            self.addString("Hello.. , if you see this text then your printer is ready")
        }
    }
    
    internal func getHeader(logo:Bool)
    {
        var image:UIImage?
        
        if logo
        {
            if let image_data = FileUtils.getDataFromDocDir(withPath: String(format: "%@/%@",POSImages,IMG_MODULES), fileName: "printer_logo", andExtn: "jpg")
            {
                image = UIImage(data: image_data)
            }
        }
        
        if let headerFooterData = userDefaults.object(forKey: HEADER_FOOTER_DATA) as? [String : String]
        {
            guard
                let merchantName    = headerFooterData["merchant_name"] ?? PrinterDefaults.merchantName,
                let merchantAddress = headerFooterData["merchant_address"] ?? PrinterDefaults.merchantAddress,
                let merchantPhone   = headerFooterData["merchant_phone"] ?? PrinterDefaults.merchnatPhone
                else { print("error on \(#function), \(#line) "); return }
            
            let header = receiptHeaderData(merchantLogo: image,
                                           merchantName: merchantName,
                                           showMerchantName: SessionData.shared().enable_merchant_name,
                                           merchantAddress: merchantAddress,
                                           showMerchantAddress: SessionData.shared().enable_merchant_address,
                                           merchantPhone: merchantPhone,
                                           showMerchantPhone: SessionData.shared().enable_merchant_phone)
            getHeaderMessage(header: header)
        }
    }
    
    private func getHeaderMessage(header: receiptHeaderData)
    {
        if let logo = header.merchantLogo
        {
            addImage(logo)
        }
        
        if header.showMerchantName
        {
            addString(header.merchantName, smallerText58mm: header.merchantName, textAlignment: .center, textFont: .fontA, extraLineSpace: 0, withBold: true, extraWidth: 1, extraHeight: 1)
        }
        
        if header.showMerchantAddress
        {
            addString(header.merchantAddress, smallerText58mm: header.merchantAddress, textAlignment: .center)
        }
        
        if header.showMerchantPhone
        {
            addString("Phone : \(header.merchantPhone)", smallerText58mm: "Phone : \(header.merchantPhone)", textAlignment: .center)
        }
        
        if(SessionData.shared().gst_enable)
        {
            let gstId = userDefaults.string(forKey: GST_ID) ?? "No SST ID"
            addString("SST ID : \(gstId)", smallerText58mm: "SST ID : \(gstId)", textAlignment: .center)
        }
        
        addLineSpace()
    }
    
    internal func getFooter()
    {        
        if let headerFooterData = userDefaults.object(forKey: HEADER_FOOTER_DATA) as? [String : String]
        {
            guard
                let wifiName       = headerFooterData["wifi_name"] ?? PrinterDefaults.wifiName ,
                let wifiPassword   = headerFooterData["wifi_password"] ?? PrinterDefaults.wifiPassword ,
                let footerMessage  = headerFooterData["footer_message"] ?? PrinterDefaults.footerMessage
                else { print("error on \(#function), \(#line) "); return }
            
            let footer = receiptFooterData(wifiName: wifiName, wifiPass: wifiPassword, footerMessage: footerMessage)
            getFooterMessage(footer: footer)
        }
    }
    
    private func getFooterMessage(footer: receiptFooterData)
    {
        if (footer.wifiName != "" || footer.wifiPass != "")
        {
            addString("Wifi Name : \(footer.wifiName)", smallerText58mm: "Wifi Name : \(footer.wifiName)", textAlignment: .center)
            addString("Wifi Pass : \(footer.wifiPass)", smallerText58mm: "Wifi Pass : \(footer.wifiPass)", textAlignment: .center)
            addLine()
        }
        
        addString(footer.footerMessage, smallerText58mm: footer.footerMessage, textAlignment: .center, textFont: .fontA, extraLineSpace: 6, withBold: true)
        addString("Powered by KryptoPOS", smallerText58mm: "Powered by KryptoPOS", textAlignment: .center, textFont: .fontA, extraLineSpace: 6, withBold: true)
    }
    
    internal func getSalesReciept()
    {
        guard
            let hardwareCode  = userDefaults.string(forKey: HARDWARE_CODE) ?? PrinterDefaults.hardwareCode,
            let currency      = userDefaults.string(forKey: CURRENCY) ?? PrinterDefaults.currency
            else { print("error on \(#function), \(#line) "); return}


        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
        let gstEnable = completeOrder.trx_gst_mode == "trx_sst_en" ||  completeOrder.trx_gst_mode == "trx_gst_enable" || completeOrder.trx_gst_mode == "trx_gst_en" || completeOrder.trx_gst_mode == "en_sst"
        let serviceCharge: String   = String(format:"%.0f",userDefaults.double(forKey: TAXES))
        var isRefund = false
        var nonGstitemTotal:Double  = 0.0; // use for GST summary
        var gstItemTotal:Double     = 0.0; // use for GST summary
        var bfrGrandTotal:Double    = 0.0; // use to show total of all item price
        let gstPercentage: String   = String(format:"%.0f",userDefaults.double(forKey: GST_PERCENTAGE))
        
        if reprint
        {
            addString("reprint", smallerText58mm: "reprint", textAlignment: .center, textFont: .fontB, extraLineSpace: 0, withBold: true)
        }
        
        var headerText = receiptType.rawValue
        switch receiptType
        {
        case .refund,.partialRefund:
            isRefund = true
        case .normal:
            headerText = gstEnable ? ((completeOrder.isSSTmode() || completeOrder.isNewSSTmode()) ? "INVOICE" : "TAX INVOICE") : "RECEIPT"
        default:
            break
        }
        addString(headerText, smallerText58mm: headerText, textAlignment: .center, textFont: .fontA, extraLineSpace: 3, withBold: true, extraWidth: 1, extraHeight: 1)
        addString("", smallerText58mm: "", textAlignment: .center, textFont: .fontA, extraLineSpace: 3)
        
        switch receiptType
        {
        case .bill:
            addString("Order ID       : " + completeOrder.hold_order_id, smallerText58mm: "Order ID     : " + completeOrder.hold_order_id)
            break
        default:
            addString("Transaction No : " + hardwareCode + "-" + completeOrder.trans_number, smallerText58mm: "Trns No      : "  + hardwareCode + "-" + completeOrder.trans_number)
            break
        }
        
        if completeOrder.table_no != nil && completeOrder.table_no != "" && completeOrder.table_no != "(null)"
        {
            addString("Table No       : " + completeOrder.table_no, smallerText58mm: "Table No     : " + completeOrder.table_no)
            
            if completeOrder.no_of_pax > 0
            {
                addString("No Of Pax      : " + completeOrder.no_of_pax.description, smallerText58mm: "No Of Pax    : " + completeOrder.no_of_pax.description)
            }
        }
        
        addString("Order Count    : " + completeOrder.counter_number.description, smallerText58mm: "Order Count  : " + completeOrder.counter_number.description)
        
        if receiptType != .gstFullTax
        {
            addString("Customer Name  : " + completeOrder.customer_name, smallerText58mm: "Cust Name    : " + completeOrder.customer_name)
        }
        
        if SessionData.shared().dine_in_enable
        {
            let dineType = (completeOrder.order_type == 1 ? "Take Away" : "Dine In")
            addString("Dine Type      : " + dineType, smallerText58mm: "Dine Type    : " + dineType)
        }

        let cashierName = completeOrder.user_name ?? (StringUtils.get_user_detail("name") ?? "")
        addString("Cashier        : " + cashierName, smallerText58mm: "Cashier      : " + cashierName)
        
        if completeOrder.sales_person_id != nil && completeOrder.sales_person_id != "(null)" && !completeOrder.sales_person_id.isEmpty
        {
            addString("Sales Person   : " + completeOrder.sales_person_name, smallerText58mm: "Sales Person : " + completeOrder.sales_person_name)
        }
        
				let currentDate = reprint ? (completeOrder.create_time ?? "") : dateFormatter.string(from: Date())
        addString("Date           : " + currentDate, smallerText58mm: "Date         : " + currentDate)
        
        if receiptType == .gstFullTax
        {
            guard
                let custName    = (completeOrder.customer_name != nil) ? completeOrder.customer_name : "" ,
                let custGstId   = (completeOrder.customer_gstid != nil) ? completeOrder.customer_gstid : "" ,
                let custPhone   = (completeOrder.customer_phone != nil) ? completeOrder.customer_phone : "" ,
                let custAdd1    = (completeOrder.customer_address1 != nil) ? completeOrder.customer_address1 : "" ,
                let custAdd2    = (completeOrder.customer_address2 != nil) ? completeOrder.customer_address2 : "" ,
                let custCity    = (completeOrder.customer_city != nil) ? completeOrder.customer_city : "" ,
                let custPinCode = (completeOrder.customer_pincode != nil) ? completeOrder.customer_pincode : ""
                else{ print("error on \(#function), \(#line) "); return}
            
            addLine()
            addString("Customer Details", smallerText58mm: "Customer Details", textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
            addString("Customer Name     : " + custName, smallerText58mm:"Cust Name    : " + custName)
            addString("Customer SST ID   : " + custGstId, smallerText58mm:"Cust SST ID  : " + custGstId)
            addString("Customer Phone    : " + custPhone, smallerText58mm:"Cust Phone   : " + custPhone)
            addString("Customer Address  : " + custAdd1 + custAdd2 + custCity + custPinCode, smallerText58mm:"Cust Address : " + custAdd1 + custAdd2 + custCity + custPinCode)
        }
        addLine()
        
        let headerItem80mm = gstEnable ? "Item         Price    Disc    Qty      Total" : "Item         Price    Disc    Qty      Total"
        let headerItem58mm = "Item  Price   Disc   Qty   Total"
        addString(headerItem80mm, smallerText58mm: headerItem58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addLine()
        
        if let items = completeOrder.orderItemsArray as? [OrderItem]
        {
            for item in items
            {
                if item.itemQuantity.intValue > 0
                {
                
                    // item name
                    let desc = item.type_of_price == "Unit Price" ? String(format:"%@ @ %.2f%@",item.itemName,item.uom_qty.doubleValue,item.type_of_unit) : item.itemName
                    if let safeDesc = desc
                    {
                        addString(safeDesc, smallerText58mm: safeDesc, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
                    }
                    
                    // item modifier
                    if item.modifierLabel != ""
                    {
                        addStringModifier(item.modifierLabel)
                    }

                    // item remarks
                    if item.remarks != nil
                    {
                        if item.remarks != ""
                        {
                            addString(String(format:"Remarks: %@", item.remarks))
                        }
                    }
                    
                    // item detail (gst,price,disc,qty,subtotal)
                    var itemDisc = ""
                    if item.discount.doubleValue > 0.00 && item.is_edited_item_price != "price_edited"
                    {
                        itemDisc = String(format:"%.2f", item.discount.doubleValue)
                    }
                    let itemPrice   = String(format:"%.2f", item.itemPrice_n_uomQty_n_modifier_perItem.doubleValue)
                    let subTotal    = String(format:"%.2f", item.totalPrice_bfr_bill_disc.doubleValue)
                    let qty         = item.itemQuantity.description
//                    var gst         = item.gst_code != "" ? item.gst_code.description : "ZR"
//                    gst = gst == "SV" ? "" : gst
                    let gst = ""

                    var lineItem80mm = ""
                    var lineItem58mm = ""
                    if (gstEnable)
                    {
                        lineItem80mm =
                            add(givenSpace: 7) +
                            add(text: gst, givenSpace: 6) +
                            add(text: itemPrice, givenSpace: 9) +
                            add(text: itemDisc, givenSpace: 8) +
                            add(text: qty, givenSpace: 3, alignment: .right) +
                            add(text: subTotal, givenSpace: 11, alignment:.right)
                        
                        lineItem58mm =
                            add(text: gst, givenSpace: 6) +
                            add(text: itemPrice, givenSpace: 8) +
                            add(text: itemDisc, givenSpace: 7) +
                            add(text: qty, givenSpace: 3, alignment: .right) +
                            add(text: subTotal, givenSpace: 8, alignment:.right)
                    }
                    else
                    {
                        lineItem80mm =
                            add(givenSpace: 13) +
                            add(text: itemPrice, givenSpace: 9) +
                            add(text: itemDisc, givenSpace: 8) +
                            add(text: qty, givenSpace: 3, alignment: .right) +
                            add(text: subTotal, givenSpace: 11, alignment:.right)
                        
                        lineItem58mm =
                            add(givenSpace: 6) +
                            add(text: itemPrice, givenSpace: 8) +
                            add(text: itemDisc, givenSpace: 7) +
                            add(text: qty, givenSpace: 3, alignment: .right) +
                            add(text: subTotal, givenSpace: 8, alignment:.right)
                    }
                    addString(lineItem80mm, smallerText58mm: lineItem58mm)
                    
                    //for GST SUMMARY
                    if gst == "SR"
                    {
                        gstItemTotal += (item.totalPrice.doubleValue - item.gst_value.doubleValue)
                    }
                    else //ZRL or ZR
                    {
                        nonGstitemTotal += (item.totalPrice.doubleValue - item.gst_value.doubleValue)
                    }
                    
                    bfrGrandTotal += item.totalPrice_bfr_bill_disc_gst.doubleValue
                }
            }
            addLine()
        }
        
        //total
        let subTotal = String(format:"%.2f", bfrGrandTotal)
        let subStr80mm =
            add(text: (completeOrder.isSSTmode() || completeOrder.isNewSSTmode()) ? "Subtotal" : "Total", givenSpace: 37) +
                add(text: subTotal, givenSpace: 7, alignment: .right)
        let subStr58mm =
            add(text: (completeOrder.isSSTmode() || completeOrder.isNewSSTmode()) ? "Subtotal" : "Total", givenSpace: 25) +
                add(text: subTotal, givenSpace: 7, alignment: .right)
        addString(subStr80mm, smallerText58mm: subStr58mm)
        
        //bill discount
        if completeOrder.discount_amount.doubleValue > 0.00
        {
            let billDisc = String(format:"%.2f", completeOrder.discount_amount.doubleValue)
            let discount80mm =
                add(text: "Discount", givenSpace: 37) +
                    add(text: billDisc, givenSpace: 7, alignment: .right)
            let discount58mm =
                add(text: "Discount", givenSpace: 25) +
                    add(text: billDisc, givenSpace: 7, alignment: .right)
            addString(discount80mm, smallerText58mm: discount58mm)
        }
        
        //bill voucher
        if completeOrder.voucher_amount.doubleValue > 0.00
        {
            let voucherDesc = String(format:"Voucher ("+completeOrder.voucher_name+")")
            let billVoucher = String(format:"%.2f",completeOrder.voucher_amount.doubleValue)
            let voucher80mm =
                add(text: voucherDesc, givenSpace: 37) +
                    add(text: billVoucher, givenSpace: 7, alignment: .right)
            let voucher58mm =
                add(text: voucherDesc, givenSpace: 25) +
                    add(text: billVoucher, givenSpace: 7, alignment: .right)
            addString(voucher80mm, smallerText58mm: voucher58mm)
        }
        
        //service charge
        if completeOrder.tax_amount.doubleValue > 0.00
        {
            let scDesc = "Service Charge (\(serviceCharge)%)"
            let sc = String(format:"%.2f", completeOrder.tax_amount.doubleValue)
            let tax80mm =
                add(text: scDesc, givenSpace: 37) +
                    add(text: sc, givenSpace: 7, alignment: .right)
            let tax58mm =
                add(text: scDesc, givenSpace: 25) +
                    add(text: sc, givenSpace: 7, alignment: .right)
            addString(tax80mm, smallerText58mm: tax58mm)
        }

        //sst total
        if (completeOrder.isSSTmode() || completeOrder.isNewSSTmode()) && gstEnable {
            let sstTotal = String(format:"%.2f", completeOrder.total_gst_value.doubleValue)
            let sstStr80mm =
                add(text: "Service Tax (\(gstPercentage)%)", givenSpace: 37) +
                    add(text: sstTotal, givenSpace: 7, alignment: .right)
            let sstStr58mm =
                add(text: "Service Tax (\(gstPercentage)%)", givenSpace: 25) +
                    add(text: sstTotal, givenSpace: 7, alignment: .right)
            addString(sstStr80mm, smallerText58mm: sstStr58mm)
        }
        
        //total gst
        if gstEnable // check for gst enable before assume service charge is GST-ed
        {
            gstItemTotal += completeOrder.tax_amount.doubleValue
        }
        
        //rounding adjustment
        let roundingStr80mm =
            add(text: "Rounding Adjustment", givenSpace: 37) +
                add(text: String(format:"%.2f",completeOrder.rounding_amount.doubleValue), givenSpace: 7, alignment: .right)
        let roundingStr58mm =
            add(text: "Rounding Adjustment", givenSpace: 25) +
                add(text: String(format:"%.2f",completeOrder.rounding_amount.doubleValue), givenSpace: 7, alignment: .right)
        addString(roundingStr80mm, smallerText58mm: roundingStr58mm)

        addLine()
        
        //total incl gst
        let totalLbl80mm = isRefund ? "TOTAL REFUND" : ((completeOrder.isSSTmode() || completeOrder.isNewSSTmode()) ? "TOTAL" : "GRAND TOTAL")
        let totalLbl58mm = isRefund ? "T.REFUND" : ((completeOrder.isSSTmode() || completeOrder.isNewSSTmode()) ? "TOTAL" : "G.TOTAL")
        let total = String(format:"%.2f", completeOrder.total.doubleValue)
        let totalStr80mm =
            add(text: totalLbl80mm, givenSpace: 15) +
                add(text: total, givenSpace: 7, alignment: .right)
        let totalStr58mm =
            add(text: totalLbl58mm, givenSpace: 9) +
                add(text: total, givenSpace: 7, alignment: .right)
        addString(totalStr80mm, smallerText58mm: totalStr58mm, textAlignment: .center, textFont: .fontA, extraLineSpace: 0, withBold: true, extraWidth: 1, extraHeight: 0)
        
        //pay type & amount given
        if completeOrder.payment_amount != nil
        {
            if completeOrder.payment_amount != NSDecimalNumber.notANumber
            {
                if completeOrder.payment_amount.doubleValue > 0
                {
                    addLine()
                    let CardNumber = (completeOrder.credit_card_no ?? "").maskedCreditCardNo()
                    let payTypeName = (CardNumber == "" || completeOrder.pay_type_name == MAYBANKQR) ? completeOrder.pay_type_name.uppercased() : ( completeOrder.pay_type_name.uppercased() + "(" + CardNumber + ")" )

                    let amountGiven80mm =
                        add(text: payTypeName, givenSpace: 37) +
                            add(text: String(format:"%.2f",completeOrder.payment_amount.doubleValue), givenSpace: 7, alignment: .right)
                    let amountGiven58mm =
                        add(text: payTypeName, givenSpace: 25) +
                            add(text: String(format:"%.2f",completeOrder.payment_amount.doubleValue), givenSpace: 7, alignment: .right)
                    addString(amountGiven80mm, smallerText58mm: amountGiven58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)

                    if completeOrder.payment_changes.doubleValue > 0
                    {
                        let change80mm:String =
                            add(text: "CHANGE", givenSpace: 37) +
                                add(text: String(format:"%.2f",completeOrder.payment_changes.doubleValue) , givenSpace: 7, alignment: .right)
                        let change58mm:String =
                            add(text: "CHANGE", givenSpace: 25) +
                                add(text: String(format:"%.2f",completeOrder.payment_changes.doubleValue) , givenSpace: 7, alignment: .right)
                        self.addString(change80mm,smallerText58mm: change58mm)
                    }
                }
            }
        }
        
        if (gstEnable && !(completeOrder.isSSTmode() || completeOrder.isNewSSTmode()))
        {
            //gst summary
            addLine()
            let gstSummaryHeader80mm =
                add(text: "GST Summary", givenSpace: 22) +
                    add(text: String(format:"Amount(%@)",currency), givenSpace: 10, alignment: .right) +
                    add(text: String(format:"GST(%@)",currency), givenSpace: 12, alignment: .right)
            let gstSummaryHeader58mm =
                add(text: "GST Summary", givenSpace: 12) +
                    add(text: String(format:"Amount(%@)",currency), givenSpace: 10, alignment: .right) +
                    add(text: String(format:"GST(%@)",currency), givenSpace: 10, alignment: .right)
            addString(gstSummaryHeader80mm, smallerText58mm: gstSummaryHeader58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
            addLine()

            if gstItemTotal > 0.0
            {
                let gstTax = String(format:"%.2f", completeOrder.total_gst_value.doubleValue)
                let lineGst80mm =
                    add(text: "SR", givenSpace: 22) +
                        add(text: String(format:"%.2f",gstItemTotal), givenSpace: 10, alignment: .right) +
                        add(text: gstTax, givenSpace: 12, alignment: .right)
                let lineGst58mm =
                    add(text: "SR", givenSpace: 12) +
                        add(text: String(format:"%.2f",gstItemTotal), givenSpace: 10, alignment: .right) +
                        add(text: gstTax, givenSpace: 10, alignment: .right)
                addString(lineGst80mm, smallerText58mm: lineGst58mm)
            }
            if nonGstitemTotal > 0.0
            {
                let gstTax = "0"
                let lineGst80mm =
                    add(text: "ZRL", givenSpace: 22) +
                        add(text: String(format:"%.2f",nonGstitemTotal), givenSpace: 10, alignment: .right) +
                        add(text: gstTax, givenSpace: 12, alignment: .right)
                let lineGst58mm =
                    add(text: "ZRL", givenSpace: 12) +
                        add(text: String(format:"%.2f",nonGstitemTotal), givenSpace: 10, alignment: .right) +
                        add(text: gstTax, givenSpace: 10, alignment: .right)
                addString(lineGst80mm, smallerText58mm: lineGst58mm)
            }
            addLine()
        }
    }
    
    internal func getKitchenData(allowedCatId:[String],printerRemark:String) -> Bool
    {
        guard
            let hardwareCode  = userDefaults.string(forKey: HARDWARE_CODE) ?? PrinterDefaults.hardwareCode
            else { print("error on \(#function), \(#line) "); return false}
        
        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
        var itemAvailableToPrint = false
        var headerStr80mm = ""
        var headerStr58mm = ""
        switch receiptType
        {
        case .bill:
            headerStr80mm = String(format:"Order ID : %@", completeOrder.hold_order_id)
            headerStr58mm = String(format:"Ord ID: %@",completeOrder.hold_order_id)
            break
        default:
            headerStr80mm = String(format:"Trans No : %@-%@", hardwareCode, completeOrder.trans_number)
            headerStr58mm = String(format:"Trx No: %@-%@",hardwareCode, completeOrder.trans_number)
            break
        }
        addString(headerStr80mm, smallerText58mm: headerStr58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 5, withBold: true, extraWidth: 1, extraHeight: 1)

        if completeOrder.table_no != nil && completeOrder.table_no != ""
        {
            let tableStr = String(format:"Table No : %@",completeOrder.table_no)
            addString(tableStr, smallerText58mm: tableStr, textAlignment: .left, textFont: .fontA, extraLineSpace: 5, withBold: true, extraWidth: 1, extraHeight: 1)
        }

        addString(String(format:"Customer    : %@",completeOrder.customer_name))
        if SessionData.shared().dine_in_enable
        {
            addString(String(format:"Dine Type   : %@",(completeOrder.order_type == 1) ? "Take Away" : "Dine In"))
        }
        addString(String(format:"Order Count : %d",completeOrder.counter_number))
        addString(String(format:"Cashier     : %@",StringUtils.get_user_detail("name")))
        addString(String(format:"Date        : %@",dateFormatter.string(from: Date())))
        addLine()

        addString("Description                         Qty", smallerText58mm: "Description                  Qty")
        addLine()

        var ableToPrintAll = allowedCatId.count == 0
        if !ableToPrintAll
        {
            if let firstCatId = allowedCatId.first
            {
                ableToPrintAll = allowedCatId.joined(separator: ",") == "" || firstCatId == "*"
            }
        }

        // print new item only
        if let items = completeOrder.orderItemsArray as? [OrderItem]
        {
            for (index,item) in items.enumerated()
            {
//                let currentQty = item.itemQuantity.description
                // get new added quantity
//                let printedQty = (item.itemQuantityPrinted == nil || item.itemQuantityPrinted == NSDecimalNumber.notANumber)
//                                    ? ""
//                                    : item.itemQuantity.subtracting(item.itemQuantityPrinted).description
//                let divider = printedQty == "" ? " " : "|"

                let newAddedQty = (item.itemQuantityPrinted == nil || item.itemQuantityPrinted == NSDecimalNumber.notANumber) ? item.itemQuantity.intValue : item.itemQuantity.subtracting(item.itemQuantityPrinted).intValue

                if (allowedCatId.contains(item.category_id.stringValue) || ableToPrintAll) && newAddedQty > 0
                {
                    let desc = String(format:"-%@",item.itemName)
//                    let lineItem80mm =
//                        add(text: desc, givenSpace: 36) +
//                            add(text: "\(printedQty)\(divider)\(currentQty)", givenSpace: 8, alignment: .right)
//                    let lineItem58mm =
//                        add(text: desc, givenSpace: 28) +
//                            add(text: "\(printedQty)\(divider)\(currentQty)", givenSpace: 4, alignment: .right)
//                    addString(lineItem80mm, smallerText58mm: lineItem58mm)

                    let arrDesc80mm = item.itemName.stripStringToArray(17)
                    for (index,desc80mm) in arrDesc80mm.enumerated() {
                        if index == 0 { // first index
                            let lineItem80mm =
                                add(text: "-\(desc80mm)", givenSpace: 18) +
                                add(text: "\(newAddedQty)", givenSpace: 4, alignment: .right)
                            let lineItem58mm =
                                add(text: desc, givenSpace: 12) +
                                add(text: "\(newAddedQty)", givenSpace: 4, alignment: .right)
                            addString(lineItem80mm, smallerText58mm: lineItem58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 5, withBold: true, extraWidth: 1, extraHeight: 1)
                        } else {
                            let nextLineItem80mm =
                                add(text: " \(desc80mm)", givenSpace: 18) +
                                add(text: "", givenSpace: 4, alignment: .right)
                            addString(nextLineItem80mm, smallerText58mm: "", textAlignment: .left, textFont: .fontA, extraLineSpace: 5, withBold: true, extraWidth: 1, extraHeight: 1)
                        }
                    }

                    // item modifier
                    if item.modifierLabel != ""
                    {
                        let modifier80mm = " "+item.modifierLabel.replace("\n", replacement: "\n ")
                        addStringModifier(modifier80mm, smallerText58mm: modifier80mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 5, withBold: true, extraWidth: 0, extraHeight: 1)
                    }

                    // item remarks
                    if item.remarks != nil
                    {
                        if item.remarks != ""
                        {
                            let remark80mm = String(format:"Remarks: %@\n", item.remarks)
                            addString(remark80mm, smallerText58mm: remark80mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 5, withBold: true, extraWidth: 0, extraHeight: 1)
                        }
                    }

                    item.itemQuantityPrinted = item.itemQuantity
                    itemAvailableToPrint = true
                    
                    if ModelController.shared().currentCompleteOrder.orderItemsArray != nil
                    {
                        ModelController.shared().currentCompleteOrder.orderItemsArray[index] = item
                    }
                }
            }

            if printerRemark != ""
            {
                addLine()
                let printerRemark80mm = "Remark : " + printerRemark
                addString(printerRemark80mm, smallerText58mm: printerRemark80mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 5, withBold: true, extraWidth: 0, extraHeight: 1)
            }
        }

        print("itemAvailableToPrint : \(itemAvailableToPrint ? "true" : "false")")
        
        return itemAvailableToPrint
    }
    
    internal func getCashRegisterData(data:cashRegiterReceiptData)
    {
        guard
            let currency = userDefaults.string(forKey: CURRENCY) ?? PrinterDefaults.currency
            else { print("error on \(#function), \(#line) "); return}
        
        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"

        let cashierName = StringUtils.get_user_detail("name") ?? ""
        let currentDate = dateFormatter.string(from: Date())
        addString(String(format:"Cashier    : %@",cashierName), smallerText58mm:String(format:"Cashier : %@",cashierName))
        addString(String(format:"Date       : %@",currentDate), smallerText58mm:String(format:"Date    : %@",currentDate))
        
        var cashType:String = ""
        switch data.cashType
        {
        case "0":
            cashType = "CASH IN"
        case "1":
            cashType = "CASH OUT"
        default:
            break
        }
        
        let amountStr = String(format:"%@ %.2f",currency,data.amount)
        let str80mm =
            add(text: cashType, givenSpace: 34, alignment: .left) +
                add(text: amountStr, givenSpace: 10, alignment: .right)
        let str58mm =
            add(text: cashType, givenSpace: 22, alignment: .left) +
                add(text: amountStr, givenSpace: 10, alignment: .right)
        addString(str80mm, smallerText58mm: str58mm)
        addLine()
        
        addString("REMARKS :")
        addString(data.notes)
        addLine()
    }
    
    internal func getDailySalesReport(startDate:Date,endDate:Date)
    {
        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
        let currentCashRegister = String(format:"%.2f",userDefaults.double(forKey: CURRENT_BALANCE))

        guard
            let currency            = userDefaults.string(forKey: CURRENCY) ?? PrinterDefaults.currency,
            let billDiscountData    = (shareddb.shared().get_total_discount_today_(from: startDate, to: endDate) as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let totalBillDisc       = (Double(billDiscountData.first?["discount"] as? String ?? "")) ?? Double?(0.0),
            let totalBillDiscQty    = (Int(billDiscountData.first?["qty"] as? String ?? "")) ?? Int?(0),
            let ItemDiscountData    = (shareddb.shared().get_total_discount_item_today_(from: startDate, to: endDate) as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let totalItemDisc       = (Double(ItemDiscountData.first?["discount"] as? String ?? "")) ?? Double?(0.0),
            let totalItemDiscQty    = (Int(ItemDiscountData.first?["qty"] as? String ?? "")) ?? Int?(0),
            let discountGivenBill   = (shareddb.shared().get_discount_given_on_bill_today_(from: startDate, to: endDate) as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let discountGivenItem   = (shareddb.shared().get_discount_given_on_item_today_(from: startDate, to: endDate) as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let voucherData         = (shareddb.shared().get_total_voucher_today_(from: startDate, to: endDate) as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let voucherGivenArray   = (voucherData.first?["vouchers"] as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let totalVoucher        = (Double(voucherGivenArray.first?["total_amount"] as? String ?? "")) ?? Double?(0.0),
            let totalVoucherQty     = (Int(voucherGivenArray.first?["qty"] as? String ?? "")) ?? Int?(0),
            let categoryItemGroup   = (shareddb.shared().get_category_group_sales_today_(from: startDate, to: endDate) as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let cashIn              = Double(shareddb.shared().get_cash_in_(from: startDate, to: endDate)) ?? Double?(0.0),
            let cashOut             = Double(shareddb.shared().get_cash_out_(from: startDate, to: endDate)) ?? Double?(0.0),
            let paymentData         = (shareddb.shared().get_total_payment_today_(from: startDate, to: endDate) as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let totalPayment        = Double((paymentData.first?["total_amount"] as? String) ?? "") ?? Double?(0.0),
            let totalPaymentArray   = (paymentData.first?["payments"] as? [[String:AnyObject]]) ?? PrinterDefaults.emptyArrayDict,
            let totalRefund         = Double(shareddb.shared().get_total_refund_date_(from: startDate, to: endDate)) ?? Double?(0.0),
            let totalServiceCharge  = Double(shareddb.shared().get_total_tax_today_(from: startDate, to: endDate)) ?? Double?(0.0),
            let totalGstSst         = Double(shareddb.shared().get_total_gst_sst_today_(from: startDate, to: endDate)) ?? Double?(0.0),
//            let totalSst            = Double(shareddb.shared().get_total_sst_today_(from: startDate, to: endDate)) ?? Double?(0.0),
            let totalRefundGstSst   = Double(shareddb.shared().get_refund_gst_sst_today_(from: startDate, to: endDate)) ?? Double?(0.0)//,
//            let totalRefundSst      = Double(shareddb.shared().get_refund_sst_today_(from: startDate, to: endDate)) ?? Double?(0.0)
            else { print("error on \(#function), \(#line) "); return}

        // calculated value
        let grossSales            = totalPayment + totalBillDisc + totalItemDisc + totalVoucher
        let totalRevenue          = grossSales + totalRefund
        let totalGstSstInclRefund = totalGstSst - totalRefundGstSst
//        let totalSstInclRefund    = totalSst - totalRefundSst
//        let totalGstInclRefund    = totalGstSstInclRefund - totalSstInclRefund
        let netSales              = totalPayment - totalGstSstInclRefund

        // Title
        addString("Daily Sales Report", smallerText58mm: "Daily Sales \nReport", textAlignment: .center, textFont: .fontA, extraLineSpace: 0, withBold: true, extraWidth: 1, extraHeight: 1)

        // info detail
        addString(String(format:"Cashier    : %@",String(StringUtils.get_user_detail("name"))))
        if startDate == endDate
        {
            addString(String(format:"Date       : %@", dateFormatter.string(from: startDate)))
        }
        else
        {
            addString(String(format:"Shift From : %@",dateFormatter.string(from: startDate)))
            addString(String(format:"Shift To   : %@",dateFormatter.string(from: endDate)))
        }
        addString(String(format:"Currency   : %@",currency))
        addLine()

        // quick summary section
        addString(String(format:"Gross Sales (Incl.Tax): %.2f",grossSales), smallerText58mm: String(format:"G.Sales (Incl.Tax): %.2f",grossSales), textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addString(String(format:"Net Sales   (Excl.Tax): %.2f",netSales), smallerText58mm: String(format:"N.Sales (Excl.Tax): %.2f",netSales), textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addString(String(format:"Total Sales (Incl.Tax): %.2f",totalPayment), smallerText58mm: String(format:"T.Sales (Incl.Tax): %.2f",totalPayment), textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addString(String(format:"Total Discount (Bill) : %.2f",totalBillDisc), smallerText58mm:String(format:"Discount (Bill)   : %.2f",totalBillDisc))
        addString(String(format:"Total Discount (Item) : %.2f",totalItemDisc), smallerText58mm:String(format:"Discount (Item)   : %.2f",totalItemDisc))
        addString(String(format:"Total Voucher         : %.2f",totalVoucher), smallerText58mm:String(format:"Voucher           : %.2f",totalVoucher))
        addString(String(format:"Total Service Charge  : %.2f",totalServiceCharge), smallerText58mm:String(format:"Service Charge    : %.2f",totalServiceCharge))

        let totalTaxLbl = "Total Tax"
        let totalTaxStr80mm =
            add(text: totalTaxLbl, givenSpace: 22, alignment: .left) +
                ": " +
                String(format:"%.2f",totalGstSstInclRefund)
        let totalTaxStr58mm =
            add(text: totalTaxLbl, givenSpace: 18, alignment: .left) +
                ": " +
                String(format:"%.2f",totalGstSstInclRefund)
        addString(totalTaxStr80mm, smallerText58mm: totalTaxStr58mm)

        addString(String(format:"Total Refund (Sales)  : %.2f",totalRefund), smallerText58mm:String(format:"Refund (Sales)    : %.2f",totalRefund))
        addString(String(format:"Total Refund Tax      : %.2f",totalRefundGstSst), smallerText58mm:String(format:"Refund Tax        : %.2f",totalRefundGstSst))
        addLine()

        // Cash register part
        addString("CASH REGISTER :", smallerText58mm: "CASH REGISTER :", textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addString(String(format:"Total Cash In         : %.2f",cashIn), smallerText58mm: String(format:"Cash In           : %.2f",cashIn))
        addString(String(format:"Total Cash Out        : %.2f",cashOut), smallerText58mm: String(format:"Cash Out          : %.2f",cashOut))
        addString(String(format:"Current Balance       : %@",currentCashRegister), smallerText58mm: String(format:"Current Balance   : %@",currentCashRegister))
        addLine()

        // details of summary
        addString("SUMMARY :", smallerText58mm: "SUMMARY :", textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addString(String(format:"Total Revenue         : %.2f",totalRevenue), smallerText58mm: String(format:"Total Revenue     : %.2f",totalRevenue), textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addString(String(format:"Total Refund       (-): %.2f",totalRefund), smallerText58mm: String(format:"Total Refund   (-): %.2f",totalRefund))
        addString(String(format:"Gross Sales           : %.2f",grossSales), smallerText58mm: String(format:"Gross Sales       : %.2f",grossSales), textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addString(String(format:"Discount (Bill)    (-): %.2f",totalBillDisc), smallerText58mm: String(format:"Discount (Bill)(-): %.2f",totalBillDisc))
        addString(String(format:"Discount (Item)    (-): %.2f",totalItemDisc), smallerText58mm: String(format:"Discount (Item)(-): %.2f",totalItemDisc))
        addString(String(format:"Voucher            (-): %.2f",totalVoucher), smallerText58mm: String(format:"Voucher        (-): %.2f",totalVoucher))
        addString(String(format:"Total Sales           : %.2f",totalPayment), smallerText58mm: String(format:"Total Sales       : %.2f",totalPayment), textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addString(String(format:"Total Tax          (-): %.2f",totalGstSstInclRefund), smallerText58mm: String(format:"Tax            (-): %.2f",totalGstSstInclRefund))
        addString(String(format:"Net Sales             : %.2f",netSales), smallerText58mm: String(format:"Net Sales         : %.2f",netSales), textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
        addLine()

        // discount summary
        if totalBillDisc > 0.0 || totalItemDisc > 0.0
        {
            addString("DISCOUNT SUMMARY :", smallerText58mm: "DISCOUNT SUMMARY :", textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
            addString("Discount                 Qty     Amount", smallerText58mm: "Discount          Qty     Amount");
            addLine()

            var discDesc = ""
            var discAmt  = ""
            var discQty  = ""
            var discStr80mm  = ""
            var discStr58mm  = ""

            // bill discount
            if totalBillDisc > 0.0
            {
                discDesc   = "Total Bill Discount"
                discAmt    = String(format:"%.2f",totalBillDisc)
                discQty    = totalBillDiscQty.description
                discStr80mm =
                    add(text: discDesc, givenSpace: 28, alignment: .left) +
                    add(text: discQty, givenSpace: 5, alignment: .right) +
                    add(text: discAmt, givenSpace: 11, alignment: .right)
                discStr58mm =
                    add(text: discDesc, givenSpace: 16, alignment: .left) +
                    add(text: discQty, givenSpace: 5, alignment: .right) +
                    add(text: discAmt, givenSpace: 11, alignment: .right)
                addString(discStr80mm, smallerText58mm: discStr58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)

                //predefined bill discount
                for discGiven in discountGivenBill
                {
                    guard
                        let discDesc   = discGiven["discount_desc"] as? String ,
                        let discAmt    = Double(discGiven["total_discount"] as? String ?? "") ,
                        let discQty    = discGiven["discount_qty"] as? String
                        else { print("error on \(#function), \(#line) "); return}

                    discStr80mm =
                        "- " +
                        add(text: discDesc, givenSpace: 26, alignment: .left) +
                        add(text: discQty, givenSpace: 5, alignment: .right) +
                        add(text: String(format:"%.2f",discAmt), givenSpace: 11, alignment: .right)
                    discStr58mm =
                        "- " +
                        add(text: discDesc, givenSpace: 14, alignment: .left) +
                        add(text: discQty, givenSpace: 5, alignment: .right) +
                        add(text: String(format:"%.2f",discAmt), givenSpace: 11, alignment: .right)
                    addString(discStr80mm, smallerText58mm: discStr58mm)
                }
            }

            // item discount
            if totalItemDisc > 0.0
            {
                discDesc   = "Total Item Discount"
                discAmt    = String(format:"%.2f",totalItemDisc)
                discQty    = totalItemDiscQty.description
                discStr80mm    =
                    add(text: discDesc, givenSpace: 28, alignment: .left) +
                    add(text: discQty, givenSpace: 5, alignment: .right) +
                    add(text: discAmt, givenSpace: 11, alignment: .right)
                discStr58mm    =
                    add(text: discDesc, givenSpace: 16, alignment: .left) +
                    add(text: discQty, givenSpace: 5, alignment: .right) +
                    add(text: discAmt, givenSpace: 11, alignment: .right)
                addString(discStr80mm, smallerText58mm: discStr58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)

                //predefined item discount
                for discGiven in discountGivenItem
                {
                    guard
                        let discDesc   = discGiven["discount_desc"] as? String ,
                        let discAmt    = Double(discGiven["total_discount"] as? String ?? "") ,
                        let discQty    = discGiven["discount_qty"] as? String
                        else { print("error on \(#function), \(#line) "); return}

                    discStr80mm =
                        "- " +
                        add(text: discDesc, givenSpace: 26, alignment: .left) +
                        add(text: discQty, givenSpace: 5, alignment: .right) +
                        add(text: String(format:"%.2f",discAmt), givenSpace: 11, alignment: .right)
                    discStr58mm =
                        "- " +
                        add(text: discDesc, givenSpace: 14, alignment: .left) +
                        add(text: discQty, givenSpace: 5, alignment: .right) +
                        add(text: String(format:"%.2f",discAmt), givenSpace: 11, alignment: .right)
                    addString(discStr80mm, smallerText58mm: discStr58mm)
                }
            }
            addLine()
        }

        //voucher summary
        if totalVoucher > 0.0
        {
            addString("VOUCHER SUMMARY :", smallerText58mm: "VOUCHER SUMMARY :", textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
            addString("Voucher                  Qty     Amount", smallerText58mm:"Voucher           Qty     Amount")
            addLine()

            let voucDesc = "Total Voucher"
            let voucAmt  = String(format:"%.2f",totalVoucher)
            let voucQty  = totalVoucherQty.description
            var voucStr80mm  =
                add(text: voucDesc, givenSpace: 28, alignment: .left) +
                add(text: voucQty, givenSpace: 5, alignment: .right) +
                add(text: voucAmt, givenSpace: 11, alignment: .right)
            var voucStr58mm  =
                add(text: voucDesc, givenSpace: 16, alignment: .left) +
                add(text: voucQty, givenSpace: 5, alignment: .right) +
                add(text: voucAmt, givenSpace: 11, alignment: .right)
            addString(voucStr80mm, smallerText58mm: voucStr58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)

            //predefined voucher
            for voucherGiven in voucherGivenArray
            {
                guard
                    let voucDesc   = voucherGiven["voucher_name"] as? String ,
                    let voucAmt    = Double(voucherGiven["amount"] as? String ?? "") ,
                    let voucQty    = voucherGiven["qty"] as? String
                    else { print("error on \(#function), \(#line) "); return}

                voucStr80mm =
                    "- " +
                    add(text: voucDesc, givenSpace: 26, alignment: .left) +
                    add(text: voucQty, givenSpace: 5, alignment: .right) +
                    add(text: String(format:"%.2f",voucAmt), givenSpace: 11, alignment: .right)
                voucStr58mm =
                    "- " +
                    add(text: voucDesc, givenSpace: 14, alignment: .left) +
                    add(text: voucQty, givenSpace: 5, alignment: .right) +
                    add(text: String(format:"%.2f",voucAmt), givenSpace: 11, alignment: .right)
                addString(voucStr80mm, smallerText58mm: voucStr58mm)
            }
            addLine()
        }
        
        //payment summary
        if totalPayment > 0.0
        {
            addString("TOTAL SALES SUMMARY :", smallerText58mm: "TOTAL SALES SUMMARY :", textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)

            let payDesc:String = "Total Sales"
            let payAmt:String  = String(format:"%.2f",totalPayment)
            var payStr80mm:String  =
                add(text: payDesc, givenSpace: 30, alignment: .left) +
                    add(text: payAmt + "   ", givenSpace: 14, alignment: .right)
            var payStr58mm:String  =
                add(text: payDesc, givenSpace: 18, alignment: .left) +
                    add(text: payAmt + "   ", givenSpace: 14, alignment: .right)
            addString(payStr80mm, smallerText58mm: payStr58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)

            //detail payment
            for paymentGiven in totalPaymentArray
            {
                guard
                    let payDesc   = paymentGiven["pay_name"] as? String ,
                    let payAmt    = Double((paymentGiven["total"] as? String) ?? "") ?? Double?(0.0)
                    else { print("error on \(#function), \(#line) "); return}
                
                let payAmtStr = String(format:"%.2f",payAmt)
                payStr80mm =
                    "- " +
                    add(text: payDesc, givenSpace: 28, alignment: .left) +
                    add(text: payAmtStr + "(+)", givenSpace: 14, alignment: .right)
                payStr58mm =
                    "- " +
                    add(text: payDesc, givenSpace: 16, alignment: .left) +
                    add(text: payAmtStr + "(+)", givenSpace: 14, alignment: .right)
                self.addString(payStr80mm, smallerText58mm: payStr58mm)
            }

            //refund
            if totalRefund > 0.0
            {
                payStr80mm    =
                    "- " +
                    add(text: "Refund", givenSpace: 28, alignment: .left) +
                    add(text: String(format:"%.2f",totalRefund) + "(-)", givenSpace: 14, alignment: .right)
                payStr58mm    =
                    "- " +
                    add(text: "Refund", givenSpace: 16, alignment: .left) +
                    add(text: String(format:"%.2f",totalRefund) + "(-)", givenSpace: 14, alignment: .right)
                self.addString(payStr80mm, smallerText58mm: payStr58mm)
            }

            self.addLine()
        }

        //category item sales summary
        if (categoryItemGroup.count > 0)
        {
            addString("CATEGORY & ITEMS SALES :", smallerText58mm: "CATEGORY & ITEMS SALES :", textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)
            addString("Category         Qty  Refund  Amount  Refund", smallerText58mm:"Ctg      Qty  Rfd     Amt    Rfd")
            addString("- Item                 Qty            Amount", smallerText58mm:"-Item         Qty            Amt")
            addLine()

            var totalAmount    = 0.0
            var totalRefundAmt = 0.0
            var totalRefQty    = 0
            var totalQty       = 0

            //category summary
            for ctgGroup in categoryItemGroup
            {
                guard
                    let name        = ctgGroup["category_name"] as? String ,
                    let total       = ctgGroup["total"]?.doubleValue ,
                    let refundTotal = ctgGroup["refund_total"]?.doubleValue ,
                    let qty         = ctgGroup["qty"]?.integerValue
                    else { print("error on \(#function), \(#line) "); return}

                let totalAmt = total - refundTotal
                var refQty   = "-"
                var refAmt   = "-"

                if let refundQty = ctgGroup["refund_qty"]?.integerValue
                {
                    refQty = refundQty.description
                    refAmt = String(format:"%.2f",refundTotal)

                    totalRefundAmt += refundTotal
                    totalRefQty    += refundQty
                }

                var str80mm =
                    add(text: name, givenSpace: 17) +
                    add(text: qty.description, givenSpace: 6, alignment: .left) +
                    add(text: refQty, givenSpace: 6, alignment: .left) +
                    add(text: String(format:"%.2f",totalAmt), givenSpace: 7, alignment: .right) +
                    add(text: refAmt, givenSpace: 8, alignment: .right)
                var str58mm =
                    add(text: name, givenSpace: 9) +
                    add(text: qty.description, givenSpace: 5, alignment: .left) +
                    add(text: refQty, givenSpace: 4, alignment: .left) +
                    add(text: String(format:"%.2f",totalAmt), givenSpace: 7, alignment: .right) +
                    add(text: refAmt, givenSpace: 7, alignment: .right)
                addString(str80mm, smallerText58mm: str58mm, textAlignment: .left, textFont: .fontA, extraLineSpace: 0, withBold: true)

                totalAmount += totalAmt
                totalQty    += qty

                // item summary
                if let itemGroups = ctgGroup["items"] as? [[String:AnyObject]]
                {
                    for itemGroup in itemGroups
                    {
                        guard
                            let name        = itemGroup["item_name"] as? String ,
                            let total       = itemGroup["total"]?.doubleValue ,
                            let refundTotal = itemGroup["refund_total"]?.doubleValue ,
                            let qty         = itemGroup["qty"]?.integerValue
                            else { print("error on \(#function), \(#line) "); return}

                        let totalAmt = total - refundTotal
                        let nameStr  = String(format: "- %@",name)
                        refQty       = "-"
                        refAmt       = "-"

                        if let refundQty = itemGroup["refund_qty"]?.integerValue
                        {
                            refQty = refundQty.description
                            refAmt = String(format:"%.2f",refundTotal)
                        }

                        str80mm =
                            add(text: nameStr, givenSpace: 17) +
                            add(text: qty.description, givenSpace: 6, alignment: .left) +
                            add(text: refQty, givenSpace: 6, alignment: .left) +
                            add(text: String(format:"%.2f",totalAmt), givenSpace: 7, alignment: .right) +
                            add(text: refAmt, givenSpace: 8, alignment: .right)
                        str58mm =
                            add(text: nameStr, givenSpace: 9) +
                            add(text: qty.description, givenSpace: 5, alignment: .left) +
                            add(text: refQty, givenSpace: 4, alignment: .left) +
                            add(text: String(format:"%.2f",totalAmt), givenSpace: 7, alignment: .right) +
                            add(text: refAmt, givenSpace: 7, alignment: .right)
                        addString(str80mm, smallerText58mm: str58mm)

                        // item choice summary
                        if let choices = itemGroup["choice"] as? [[String:AnyObject]]
                        {
                            for choice in choices
                            {
                                guard
                                    let name        = choice["item_name"] as? String ,
                                    let total       = choice["total"]?.doubleValue ,
                                    let refundTotal = choice["refund_total"]?.doubleValue ,
                                    let qty         = choice["qty"]?.integerValue
                                    else { print("error on \(#function), \(#line) "); return}

                                let totalAmt = total - refundTotal
                                let nameStr  = String(format:"   + %@",name)
                                refQty       = "-"
                                refAmt       = "-"

                                if let refundQty = choice["refund_qty"]?.integerValue
                                {
                                    refQty = refundQty.description
                                    refAmt = String(format:"%.2f",refundTotal)
                                }

                                str80mm =
                                    add(text: nameStr, givenSpace: 24) +
                                    add(text: qty.description, givenSpace: 8, alignment: .left) +
                                    add(text: refQty, givenSpace: 8, alignment: .left) +
                                    //add(text: String(format:"%.2f",totalAmt), givenSpace: 10, alignment: .right) +
                                    add(text: "-", givenSpace: 10, alignment: .right) +
                                    //add(text: refAmt, givenSpace: 10, alignment: .right)
                                    add(text: "-", givenSpace: 10, alignment: .right)
                                str58mm =
                                    add(text: nameStr, givenSpace: 13) +
                                    add(text: qty.description, givenSpace: 6, alignment: .left) +
                                    add(text: refQty, givenSpace: 6, alignment: .left) +
                                    //add(text: String(format:"%.2f",totalAmt), givenSpace: 8, alignment: .right) +
                                    add(text: "-", givenSpace: 8, alignment: .right) +
                                    //add(text: refAmt, givenSpace: 9, alignment: .right)
                                    add(text: "-", givenSpace: 9, alignment: .right)
                                addString(str80mm, smallerText58mm: str58mm, textAlignment: .left, textFont: .fontB)
                            }
                        }
                    }
                }
                addLine()
            }
            let strSummary80mm:String  =
                add(text: "TOTAL", givenSpace: 17, alignment: .left) +
                add(text: totalQty.description, givenSpace: 6, alignment: .left) +
                add(text: totalRefQty.description, givenSpace: 6, alignment: .left) +
                add(text: String(format:"%.2f",totalAmount), givenSpace: 7, alignment: .right) +
                add(text: String(format:"%.2f",totalRefundAmt), givenSpace: 8, alignment: .right)
            let strSummary58mm:String  =
                add(text: "TOTAL", givenSpace: 9, alignment: .left) +
                    add(text: totalQty.description, givenSpace: 5, alignment: .left) +
                    add(text: totalRefQty.description, givenSpace: 4, alignment: .left) +
                    add(text: String(format:"%.2f",totalAmount), givenSpace: 7, alignment: .right) +
                    add(text: String(format:"%.2f",totalRefundAmt), givenSpace: 7, alignment: .right)
            addString(strSummary80mm, smallerText58mm: strSummary58mm)
        }
    }
}
