//
//  StarMicronics80mm.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 04/08/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//


import Foundation

struct StarMicronicsData {
    var command: ISCBBuilder?
    var copies: Int
}

@objc class StarMicronics80mm: NSObject, PrinterProtocol {

    // - MARK: Singleton Declaration
    // swiftSharedInstance is not accessible from ObjC
    class var swiftSharedPrinter: StarMicronics80mm {
        struct Singleton {
            static let sharedPrinter = StarMicronics80mm()
        }
        return Singleton.sharedPrinter
    }

    // the sharedInstance class method can be reached from ObjC
    class func sharedPrinter() -> StarMicronics80mm {
        return StarMicronics80mm.swiftSharedPrinter
    }

    fileprivate override init() {
        emulation     = ModelCapability.emulationAtModelIndex(ModelIndex.tsp650II)
        ip_address    = ""
        port_settings = ""
    }

    fileprivate var emulation: StarIoExtEmulation
    var ip_address: String?
    var port_settings: String?

    private func initializeData() -> ISCBBuilder? {
        let commands = StarIoExt.createCommandBuilder(emulation)
        commands?.beginDocument()
        return commands
    }

    private func printCopies(_ printerDatas: [StarMicronicsData]) {

        printerDatas.forEach({
            for _ in 1 ... $0.copies {
                _ = Communication.sendCommands($0.command?.commands.copy() as? Data, portName: ip_address, portSettings:port_settings, timeout: 10000)
            }
        })
    }

    // MARK: - printer protocol method
    func openCashDrawer() {
        let command = initializeData()
        command?.append(CashDrawerFunctions.createData(emulation, channel: SCBPeripheralChannel.no1))
        command?.endDocument()

        let data = StarMicronicsData(command: command, copies: 1)
        self.printCopies([data])
    }

    func printReciept(printerReceiptData:[receiptData], openCashRegister: Bool) {

        var openCashRegister = openCashRegister
        var allData: [StarMicronicsData] = []
        var command = initializeData()

        for data in printerReceiptData {
            switch data.receiptDataType {
            case .image:
                command?.appendAlignment(SCBAlignmentPosition.center)
                command?.appendBitmap(data.image, diffusion: true, width: Int(data.image.size.width), bothScale: true, rotation: .normal)
            case .text, .line, .modifierText:
                var str = data.text80mm //modifierText
                if data.receiptDataType == .line {
                    str = "".padding(toLength: 44, withPad: data.text80mm, startingAt: 0)
                } else if data.receiptDataType == .text {
                    str = data.text80mm.inserting(separator: "\n", every: 44)
                }
                let extraSpace = data.extraWitdh == 0 ? "  " : " "
                switch data.textAlignment {
                case .left:
                    command?.appendAlignment(SCBAlignmentPosition.left)
                    str = extraSpace + str.replace("\n", replacement: "\n" + extraSpace)
                case .right:
                    command?.appendAlignment(SCBAlignmentPosition.right)
                    str = extraSpace + str.replace("\n", replacement: "\n" + extraSpace)
                case .center:
                    command?.appendAlignment(SCBAlignmentPosition.center)
                }
                switch data.textFont {
                case .fontA:
                    command?.append(SCBFontStyleType.A)
                case .fontB:
                    command?.append(SCBFontStyleType.B)
                }

                command?.appendLineSpace(25+data.extraLineSpace)
                command?.appendEmphasis(data.textBold)

                command?.appendData(withMultiple: (str+"\n").data(using: String.Encoding.ascii), width: 1+data.extraWitdh, height: 1+data.extraHeight)
            case .cutPaper:
                command?.appendCutPaper(SCBCutPaperAction.partialCutWithFeed)
                if openCashRegister {
                    command?.append(CashDrawerFunctions.createData(emulation, channel: SCBPeripheralChannel.no1))
                    openCashRegister = false
                }
                command?.endDocument()

                let printerData = StarMicronicsData(command: command, copies: data.numberOfCopies)
                allData.append(printerData)
                command = initializeData()
            }
        }

        printCopies(allData)
    }
}



