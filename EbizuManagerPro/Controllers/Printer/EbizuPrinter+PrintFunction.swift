//
//  EbizuPrinter+PrintFunction.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 16/06/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension EbizuPrinter
{
    //MARK: - Shared Private Method
    private func decidePrinterTypeBasedOnIP(IpAddress:String) -> PrinterProtocol {
        var selectedPrinter: PrinterProtocol

        if IpAddress == "BT:mPOP" {
            selectedPrinter = Mpop58mm.sharedPrinter()
        } else if IpAddress == "BT:Star Micronics" {
            selectedPrinter = StarMicronics80mm.sharedPrinter()
        } else if IpAddress.isXPrinter() {
            selectedPrinter = XPrinter80mm.sharedPrinter()
        } else {
            selectedPrinter = Epson80mm.sharedPrinter()
        }
        selectedPrinter.ip_address = IpAddress

        return selectedPrinter
    }
    
    private func startPrinting()
    {
        appDelegate?.notify_(with_right_toast: "Printing In Progress", title: nil, image: "activity_indicator")
    }
    
    private func endPrinting()
    {
        appDelegate?.notify_(with_right_toast: "Printing Complete", title: nil)
    }
    
    //MARK: - Public Method
    func printSales()
    {
        if let defaultPrinters = shareddb.shared().get_all_default_printers_(from_bill_only: false) as? [[String:AnyObject]]
        {
            reprint = false
            printReceipt(printerArr: defaultPrinters)
        }
    }
    
    func reprintSales()
    {
        if let defaultPrinters = shareddb.shared().get_all_default_printers_(from_bill_only: true) as? [[String:AnyObject]]
        {
            if let firstDefaultPrinter = defaultPrinters.first
            {
                reprint = true
                printReceipt(printerArr: [firstDefaultPrinter])
            }
        }
    }
    
    func printRefund()
    {
        if let defaultPrinters = shareddb.shared().get_all_default_printers_(from_bill_only: true) as? [[String:AnyObject]]
        {
            if let firstDefaultPrinter = defaultPrinters.first
            {
                reprint = false
                printReceipt(printerArr: [firstDefaultPrinter])
            }
        }
    }
    
    func printBill(printerArr:[[String:AnyObject]])
    {
        reprint = false
        printReceipt(printerArr: printerArr)
    }
    
    func setupSalesReceipt()
    {
        receiptDataArr = [receiptData]()
        getHeader(logo: true)
        getSalesReciept()
        getFooter()
    }
    
    private func printReceipt(printerArr:[[String:AnyObject]])
    {
        DispatchQueue.global(qos: .userInitiated).async
            {
                //aldy setup receipt for transaction print
                if (self.receiptType != .gstFullTax && self.receiptType != .normal) || self.reprint
                {
                    self.setupSalesReceipt()
                }
                
                let allKitchenPrinter = printerArr.filter{($0["printer_type"] as! String) == "Kitchen"}
                let allBillPrinter = printerArr.filter{($0["printer_type"] as! String) == "Bill"}
                
                // get bill receipt
                var billReceipt = self.receiptDataArr
                
                // get kitchen receipt
                var kitchenReceiptArr = [kitchenReceiptData]()
                if allKitchenPrinter.count > 0
                {
                    for printerDict in allKitchenPrinter
                    {
                        guard
                            let ipAddress = printerDict["ip_address"] as? String,
                            let numberOfCopiesStr = printerDict["print_copies"] as? String,
                            let numberOfCopies = Int(numberOfCopiesStr),
                            let printerRemark = (printerDict["printer_remark"] ?? "" as AnyObject) as? String,
                            let catId = printerDict["category_id"] as? String
                            else { print("error on \(#function), \(#line) "); return}

                        let allCatId = catId.components(separatedBy: ",")

                        self.receiptDataArr = [receiptData]()
                        if self.getKitchenData(allowedCatId: allCatId, printerRemark: printerRemark)
                        {
                            let tempKitchenData = kitchenReceiptData.init(receipt: self.receiptDataArr, numberOfCopies: numberOfCopies, ipAddress: ipAddress)
                            kitchenReceiptArr.append(tempKitchenData)
                        }
                    }
                }
                
                // print bill & kitchen
                self.startPrinting()
        
                if allBillPrinter.count > 0
                {
                    for printerDict in allBillPrinter
                    {
                        DispatchQueue.main.sync {
                            guard
                                let ipAddress = printerDict["ip_address"] as? String,
                                let numberOfCopiesStr = printerDict["print_copies"] as? String,
                                let numberOfCopies = Int(numberOfCopiesStr)
                                else { print("error on \(#function), \(#line) "); return}

                            let selectedPrinter = self.decidePrinterTypeBasedOnIP(IpAddress: ipAddress)
                            billReceipt.append(receiptData.paperCut(numberOfCopies))

                            var finalKitchenReceiptArr: [kitchenReceiptData] = []
                            for kitchenReceipt in kitchenReceiptArr {
                                let kitchenIP = kitchenReceipt.ipAddress.replace("TCP:", replacement: "")
                                let billIP = ipAddress.replace("TCP:", replacement: "")
                                if kitchenIP == billIP {
                                    billReceipt += kitchenReceipt.receipt
                                    billReceipt.append(receiptData.paperCut(kitchenReceipt.numberOfCopies))
                                } else {
                                    finalKitchenReceiptArr.append(kitchenReceipt)
                                }
                            }
                            kitchenReceiptArr = finalKitchenReceiptArr

                            selectedPrinter.printReciept(printerReceiptData: billReceipt, openCashRegister: self.receiptType != .bill && !self.reprint)
                        }
                    }
                } 

                DispatchQueue.main.sync {

                    if kitchenReceiptArr.count > 0
                    {
                        var allFinalData: [kitchenReceiptData] = []
                        /// looping to find similar printer to be joined
                        for currentReceipt in kitchenReceiptArr
                        {
                            var modifiedReceipt = currentReceipt
                            var finalData = modifiedReceipt.receipt

                            if let index = allFinalData.firstIndex(where: {
                                let previousKitchen = $0.ipAddress.replace("TCP:", replacement: "")
                                let currentKitchen = modifiedReceipt.ipAddress.replace("TCP:", replacement: "")
                                return previousKitchen == currentKitchen
                            }) {
                                allFinalData[index].receipt += modifiedReceipt.receipt
                                allFinalData[index].receipt.append(receiptData.paperCut(modifiedReceipt.numberOfCopies))
                            } else {
                                finalData.append(receiptData.paperCut(modifiedReceipt.numberOfCopies))
                                modifiedReceipt.receipt = finalData
                                allFinalData.append(modifiedReceipt)
                            }
                        }
                        /// actual printing the merged receipt
                        allFinalData.forEach({
                            let selectedPrinter = self.decidePrinterTypeBasedOnIP(IpAddress: $0.ipAddress)
                            selectedPrinter.printReciept(printerReceiptData: $0.receipt, openCashRegister: false)
                        })
                    }
                }
        
                self.endPrinting()
        }
    }
    
    func printDailySalesReport(startDate:Date,endDate:Date)
    {
        receiptDataArr = [receiptData]()
        getHeader(logo: false)
        getDailySalesReport(startDate: startDate, endDate: endDate)
        
        self.startPrinting()
        if let defaultPrinters = shareddb.shared().get_all_default_printers_(from_bill_only: true) as? [[String:AnyObject]]
        {
            if let firstDefaultPrinter = defaultPrinters.first
            {
                guard
                    let ipAddress = firstDefaultPrinter["ip_address"] as? String
                    else { print("error on \(#function), \(#line) "); return}
                
                let selectedPrinter = decidePrinterTypeBasedOnIP(IpAddress: ipAddress)
                self.receiptDataArr.append(receiptData.paperCut(1))
                selectedPrinter.printReciept(printerReceiptData: self.receiptDataArr, openCashRegister: true)
            }
        }
        self.endPrinting()
    }
    
    func printCashRegister(data:cashRegiterReceiptData)
    {
        DispatchQueue.global(qos: .userInitiated).async
            {
                self.receiptDataArr = [receiptData]()
                self.getHeader(logo: false)
                self.getCashRegisterData(data: data)
                self.startPrinting()
                if let defaultPrinters = shareddb.shared().get_all_default_printers_(from_bill_only: true) as? [[String:AnyObject]]
                {
                    if let firstDefaultPrinter = defaultPrinters.first
                    {

                        DispatchQueue.main.async {
                            guard
                                let ipAddress = firstDefaultPrinter["ip_address"] as? String
                                else { print("error on \(#function), \(#line) "); return}

                            let selectedPrinter = self.decidePrinterTypeBasedOnIP(IpAddress: ipAddress)
                            self.receiptDataArr.append(receiptData.paperCut(1))
                            selectedPrinter.printReciept(printerReceiptData: self.receiptDataArr, openCashRegister: true)
                        }
                    }
                }
                self.endPrinting()
            }
    }
    
    func testPrinter(IpAddress:String)
    {
        receiptDataArr = [receiptData]()
        getTestPrintMessage()
        
        startPrinting()
        let selectedPrinter = decidePrinterTypeBasedOnIP(IpAddress: IpAddress)
        self.receiptDataArr.append(receiptData.paperCut(1))
        selectedPrinter.printReciept(printerReceiptData: self.receiptDataArr, openCashRegister: false)
        endPrinting()
    }
    
    
    // might not need below function, only requested by koffeholics
    func openPrinterCashDrawer(IpAddress:String)
    {
        DispatchQueue.global(qos: .userInitiated).async
            {
                let selectedPrinter = self.decidePrinterTypeBasedOnIP(IpAddress: IpAddress)
                selectedPrinter.openCashDrawer()
            }
    }
}
