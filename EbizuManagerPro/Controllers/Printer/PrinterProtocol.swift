//
//  PrinterProtocol.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 04/08/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

protocol PrinterProtocol {
    var ip_address:String?{get set}
    func openCashDrawer()
    func printReciept(printerReceiptData:[receiptData], openCashRegister:Bool)
}

// MARK: Printer constants
enum TextAlignment
{
    case right
    case left
}

struct PrinterDefaults
{
    static let merchantName:String? = "No Merchant Name"
    static let merchantAddress:String? = "No Address"
    static let merchnatPhone:String? = "No Phone"
    static let gstId:String? = "No GST ID"
    
    static let wifiName:String? = ""
    static let wifiPassword:String? = ""
    static let footerMessage:String? = ""
    
    static let payType:String? = "cash"
    static let printCopies:Int? = 1
    
    static let receiptType:String? = ""
    static let cardNumber:String? = ""
    static let hardwareCode:String? = ""
    static let currency:String? = ""
    static let taxes:Float? = 0.00// remove when use new way of printing
    static let serviceCharge:Double? = 0.00
    
    static let emptyArrayDict:[[String:AnyObject]]? = [[:]]
    static let emptyDict:[String:AnyObject]? = [:]
    
    static let zeroQty:Int? = 0
    
    // for cash register
    static let emptyAmount:String? = ""
    static let nonCashType:Int? = 99
    static let emptyNotes:String? = ""
    
}

extension UIImage
{
    func ConvertToStandardSize(_ size:CGFloat) -> UIImage
    {
        let size_guide:CGFloat   = (self.size.width > self.size.height) ? self.size.width : self.size.height
        let width_guide:CGFloat  = self.size.width / (size_guide/size)
        let height_guide:CGFloat = self.size.height / (size_guide/size)
        let img_size:CGSize      = CGSize(width: width_guide, height: height_guide)
        
        UIGraphicsBeginImageContext(img_size)
        self.draw(in: CGRect(x: 0, y: 0, width: img_size.width, height: img_size.height))
        let destImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return destImage
    }
    
    func ConvertToGrayScale() -> UIImage
    {
        let context = CIContext(options: nil)
        let currentFilter = CIFilter(name: "CIPhotoEffectNoir")
        currentFilter!.setValue(CIImage(image: self), forKey: kCIInputImageKey)
        let output = currentFilter!.outputImage
        let cgimg = context.createCGImage(output!,from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        
        return processedImage
    }
    
}

extension String
{
    func StripStringToLength(_ length:Int) -> String
    {
        var subString = self
        if subString.count > length
        {
            subString = subString.substring(to: self.index(self.startIndex, offsetBy: length - 2))
            subString = subString + ".."
        }
        return subString
    }
    
    func with(givenSpace space:Int, alignment align:TextAlignment = .left) -> String
    {
        var editText:String = self
        if editText.count > space
        {
            editText = editText.StripStringToLength(space)
        }
        switch align
        {
        case .left:  return String(format:"%@%@", editText, giveSpace(numberOfSpace: space - editText.count))
        case .right: return String(format:"%@%@", giveSpace(numberOfSpace: space - editText.count), editText)
        }
    }
    
    func giveSpace(numberOfSpace space:Int) -> String
    {
        let emptySpace:Character = " "
        return String(repeating: String(emptySpace), count: space)
    }

    func inserting(separator: String, every n: Int) -> String {
        var result: String = ""
        let characters = Array(self)
        stride(from: 0, to: count, by: n).forEach {
            result += String(characters[$0..<min($0+n, count)])
            if $0+n < count {
                result += separator
            }
        }
        return result
    }

    func containIpAddress() -> Bool {
        let address = self.components(separatedBy: ".")
        if address.count == 4 {
            for add in address {
                guard let _ = Int(add) else { return false }
            }
            return true
        }
        return false
    }

    func stripStringToArray(_ length:Int) -> [String] {
        var modStr = self
        var arrResult:[String] = []

        while modStr.count > length {
            let stripStr = modStr.substring(to: modStr.index(modStr.startIndex, offsetBy: length))
            arrResult.append(stripStr)
            modStr = modStr.substring(from: modStr.index(modStr.endIndex, offsetBy: -(modStr.count - stripStr.count))).trimmingCharacters(in: CharacterSet.whitespaces)
        }
        arrResult.append(modStr)

        return arrResult
    }

    mutating func markAsXPrinter() {
        self.getXPrinterIp()
        self = "XPrinter:\(self)"
    }

    func isXPrinter() -> Bool {
        return self.contains("XPrinter:")
    }

    mutating func getXPrinterIp() {
        self = self.replace("XPrinter:", replacement: "")
    }
}

