//
//  XPrinter80mm.swift
//  KryptoPOS
//
//  Created by augustius cokroe on 17/06/2019.
//  Copyright © 2019 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct XPrinterData {
    var data: Data
    var copies: Int
}

@objc class XPrinter80mm: NSObject, PrinterProtocol {

    // MARK: Singleton Declaration
    // swiftSharedInstance is not accessible from ObjC
    class var swiftSharedPrinter: XPrinter80mm {
        struct Singleton {
            static let sharedPrinter = XPrinter80mm()
        }
        return Singleton.sharedPrinter
    }

    // the sharedInstance class method can be reached from ObjC
    class func sharedPrinter() -> XPrinter80mm {
        return XPrinter80mm.swiftSharedPrinter
    }

    fileprivate override init() { // Prevent use of initialisation of class
        ip_address = ""
        wifi = MWIFIManager.share()
    }

    var ip_address: String?
    var printerData: Data!

    private var wifi: MWIFIManager! {
        didSet {
            wifi.delegate = self
        }
    }

    private func printerConnected(_ completion: @escaping (_ connected: Bool) -> Void) {
        ip_address?.getXPrinterIp()
        wifi.mDisConnect()
        wifi.mConnect(withHost: ip_address ?? "", port: 9100, completion: { (connected) in
            completion(connected)
        })
    }

    private func initializeData() -> NSMutableData {
        let width: Int32 = 58
        let tempData = NSMutableData(data: MCommand.initializePrinter())
        tempData.append(MCommand.setLableWidth(width))
        tempData.append(MCommand.setPrintAreaWidthWithnL(width*8%256, andnH: width*8/256))
        return tempData
    }

    private func printCopies(_ printerDatas: [XPrinterData]) {
        printerConnected { (connected) in
            if connected {
                /// print no of copies
                printerDatas.forEach({
                    for _ in 1 ... $0.copies {
                        self.wifi.mWriteCommand(with: $0.data)
                    }
                })
            } else {
                print("china fail to connect")
            }
        }
    }

    // MARK: - printer protocol method
    func openCashDrawer() {
        printerConnected { (connected) in
            if connected {
                let tempData = NSMutableData(data: MCommand.initializePrinter())
								let cashdrawBytes: [UInt8] = [0x1B, 0x70, 0x00, 0x1E, 0xFF]
								let cashdrawData = Data(bytes: cashdrawBytes)
								tempData.append(cashdrawData)
								tempData.append(MCommand.openCashBoxRealTime(withM: 0x00, andT: 0x01))
                let data = XPrinterData(data: tempData as Data, copies: 1)
                self.printCopies([data])
            }
        }
    }

    func printReciept(printerReceiptData:[receiptData], openCashRegister:Bool) {
        let cfEnc = CFStringEncodings.GB_18030_2000 //kCFStringEncodingGB_18030_2000
        let gbkEncoding = CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(cfEnc.rawValue))

        var allData: [XPrinterData] = []
        var tempData = initializeData()

        for data in printerReceiptData {
            switch data.receiptDataType {
            case .image: break
                //                safePrinter.addTextAlign(1)
                //                safePrinter.add(data.image, x:0, y:0,
                //                                width:Int(data.image.size.width),
                //                                height:Int(data.image.size.height),
                //                                color:EPOS2_PARAM_DEFAULT,
                //                                mode:EPOS2_PARAM_DEFAULT,
                //                                halftone:EPOS2_PARAM_DEFAULT,
                //                                brightness:3,
                //                                compress:EPOS2_PARAM_DEFAULT)
            //                safePrinter.addFeedUnit(0)
            case .text, .line, .modifierText:
                var str = data.text80mm //modifierText
                if data.receiptDataType == .line {
                    str = "".padding(toLength: 44, withPad: data.text80mm, startingAt: 0)
                } else if data.receiptDataType == .text {
                    str = data.text80mm.inserting(separator: "\n", every: 44)
                }
                let extraSpace = data.extraWitdh == 0 ? "  " : " "
                switch data.textAlignment {
                case .left:
                    tempData.append(MCommand .selectAlignment(0))
                    str = extraSpace + str.replace("\n", replacement: "\n" + extraSpace);
                case .right:
                    tempData.append(MCommand.selectAlignment(2))
                    str = extraSpace + str.replace("\n", replacement: "\n" + extraSpace);
                case .center:
                    tempData.append(MCommand.selectAlignment(1))
                }
                switch data.textFont
                {
                case .fontA: tempData.append(MCommand.selectFont(0))
                case .fontB: tempData.append(MCommand.selectFont(1))
                }

                tempData.append(MCommand.selectOrCancleBoldModel(data.textBold ? 1 : 0))
                tempData.append(MCommand.selectCharacterSize(Int32(data.extraHeight + data.extraWitdh)))

                if let textData = (str+"\n").data(using: String.Encoding(rawValue: gbkEncoding)) {
                    tempData.append(textData)
                } else {
                    print("textData conversion failed")
                }

            case .cutPaper:

                if let textData = "\n\n\n".data(using: String.Encoding(rawValue: gbkEncoding)) {
                    tempData.append(textData)
                } else {
                    print("textData conversion failed")
                }
                tempData.append(MCommand.printAndFeedLine())
                tempData.append(MCommand.selectCutPageModelAndCutpage(1))

                let printerData = XPrinterData(data: tempData as Data, copies: data.numberOfCopies)
                allData.append(printerData)
                tempData = initializeData()
            }
        }

        if openCashRegister {
						let cashdrawBytes: [UInt8] = [0x1B, 0x70, 0x00, 0x1E, 0xFF]
						let cashdrawData = Data(bytes: cashdrawBytes)
						tempData.append(cashdrawData)
						tempData.append(MCommand.openCashBoxRealTime(withM: 0x00, andT: 0x01))
        }

        printCopies(allData)
    }
}

extension XPrinter80mm: MWIFIManagerDelegate {

    func mwifiManager(_ manager: MWIFIManager!, didConnectedToHost host: String!, port: UInt16) {
        print("connect to \(host)")
    }

    func mwifiManager(_ manager: MWIFIManager!, willDisconnectWithError error: Error!) {
        print("disconnect to \(error.localizedDescription)")
    }

    func mwifiManager(_ manager: MWIFIManager!, didWriteDataWithTag tag: Int) {

    }

    func mwifiManager(_ manager: MWIFIManager!, didRead data: Data!, tag: Int) {

    }

    func mwifiManagerDidDisconnected(_ manager: MWIFIManager!) {
        manager.mDisConnect()
    }
}
