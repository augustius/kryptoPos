//
//  Order.m
//  Ebizu Manager
//
//  Created by Swati on 11/22/13.
//  Copyright (c) 2013 EBIZU. All rights reserved.
//  Modified By Jamal on Aug-14
//

#import "OrderItem.h"
#import <objc/runtime.h>

@interface OrderItem()
//- (instancetype)init;
@end


@implementation OrderItem

@synthesize hold_item_id        = _hold_item_id;
@synthesize itemID              = _itemID;
@synthesize itemCatID           = _itemCatID;
@synthesize itemImage           = _itemImage;
@synthesize itemName            = _itemName;
@synthesize is_edited_item_price = _is_edited_item_price;
@synthesize itemQuantity        = _itemQuantity;
@synthesize itemQuantityPrinted   = _itemQuantityPrinted;
@synthesize split_item_qty      = _split_item_qty;

@synthesize itemPrice           = _itemPrice;
@synthesize totalPrice          = _totalPrice;
@synthesize totalPrice_bfr_bill_disc = _totalPrice_bfr_bill_disc;
@synthesize totalPrice_bfr_bill_disc_gst = _totalPrice_bfr_bill_disc_gst;
@synthesize stock_left          = _stock_left;
@synthesize cost_price          = _cost_price;
@synthesize selling_price       = _selling_price;

@synthesize selectedChoices     = _selectedChoices;
@synthesize selectedAdditions   = _selectedAdditions;
@synthesize remarks             = _remarks;
@synthesize discount            = _discount;
@synthesize allModifiersDict    = _allModifiersDict;
@synthesize item_checked        = _item_checked;
@synthesize predefined_discount = _predefined_discount;
@synthesize stock_monitoring    = _stock_monitoring;
@synthesize category_id         = _category_id;

@synthesize gst_code            = _gst_code;
@synthesize gst_rate            = _gst_rate;
@synthesize gst_value           = _gst_value;
@synthesize gst_value_bfr_bill_disc = _gst_value_bfr_bill_disc;
@synthesize refund_item_qty     = _refund_item_qty;
@synthesize remain_item_qty     = _remain_item_qty;
@synthesize refund_item_price   = _refund_item_price;

@synthesize type_of_price       = _type_of_price;
@synthesize type_of_unit        = _type_of_unit;
@synthesize uom_qty             = _uom_qty;

@synthesize itemPrice_n_uomQty_n_modifier_perItem = _itemPrice_n_uomQty_n_modifier_perItem;


-(id) copyWithZone: (NSZone *) zone
{
    OrderItem *copy = [[OrderItem allocWithZone: zone] init];
    
    copy.hold_item_id       = self.hold_item_id;
    copy.itemID             = self.itemID;
    copy.itemCatID          = self.itemCatID;
    copy.itemImage          = self.itemImage;
    copy.itemName           = self.itemName;
    copy.is_edited_item_price = self.is_edited_item_price;
    copy.itemQuantity       = self.itemQuantity;
    copy.itemQuantityPrinted       = self.itemQuantityPrinted;
    copy.split_item_qty     = self.split_item_qty;
    
    copy.itemPrice          = self.itemPrice;
    copy.totalPrice         = self.totalPrice;
    copy.totalPrice_bfr_bill_disc = self.totalPrice_bfr_bill_disc;
    copy.totalPrice_bfr_bill_disc_gst = self.totalPrice_bfr_bill_disc_gst;
    copy.stock_left         = self.stock_left;
    copy.cost_price         = self.cost_price;
    copy.selling_price      = self.selling_price;
    
    copy.selectedChoices    = self.selectedChoices;
    copy.selectedAdditions  = self.selectedAdditions;
    copy.remarks            = self.remarks;
    copy.discount           = self.discount;
    copy.allModifiersDict   = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:self.allModifiersDict]];
    copy.item_checked       = self.item_checked;
    copy.predefined_discount = self.predefined_discount;
    copy.stock_monitoring   = self.stock_monitoring;
    copy.category_id        = self.category_id;
    
    copy.gst_code           = self.gst_code;
    copy.gst_rate           = self.gst_rate;
    copy.gst_value          = self.gst_value;
    copy.gst_value_bfr_bill_disc = self.gst_value_bfr_bill_disc;
    copy.refund_item_qty    = self.refund_item_qty;
    copy.remain_item_qty    = self.remain_item_qty;
    copy.refund_item_price  = self.refund_item_price;
    
    copy.type_of_price      = self.type_of_price;
    copy.type_of_unit       = self.type_of_unit;
    copy.uom_qty            = self.uom_qty;
    
    copy.itemPrice_n_uomQty_n_modifier_perItem = self.itemPrice_n_uomQty_n_modifier_perItem;
    
    return copy;
}
- (NSDictionary *)dictionaryRepresentation
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++)
    {
        NSString *key = @(property_getName(properties[i]));
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
        {
            dictionary[key] = value;
        }
        else
        {
            dictionary[key] = @"";
        }
    }
    
    free(properties);
    
    return dictionary;
}

// FIXME: Remove this call.
//- (instancetype)init {
//    self = [self init];
//    return self;
//}


- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    
    if (self)
    {
        
        self.hold_item_id               = [NSString stringWithFormat:@"%@",dictionary[@"hold_item_id"]];
        self.itemID                     = [NSString stringWithFormat:@"%@",dictionary[@"itemID"]];
        self.itemCatID                  = [NSString stringWithFormat:@"%@",dictionary[@"itemCatID"]];
        self.itemImage                  = [NSString stringWithFormat:@"%@",dictionary[@"itemImage"]];
        self.itemName                   = [NSString stringWithFormat:@"%@",dictionary[@"itemName"]];
        self.is_edited_item_price       = [NSString stringWithFormat:@"%@",dictionary[@"is_edited_item_price"]];
        self.itemQuantity               = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"itemQuantity"]]];
        self.split_item_qty             = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"split_item_qty"]]];
        
        self.itemPrice                  = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"itemPrice"]]];
        self.totalPrice                 = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"totalPrice"]]];
        self.totalPrice_bfr_bill_disc   = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"totalPrice_bfr_bill_disc"]]];
        self.totalPrice_bfr_bill_disc_gst = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"totalPrice_bfr_bill_disc_gst"]]];
        self.stock_left                 = [NSNumber numberWithInt:[dictionary[@"stock_left"] intValue]];
        self.cost_price                 = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"cost_price"]]];
        self.selling_price              = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"selling_price"]]];
        
        self.selectedChoices            = dictionary[@"selectedChoices"];
        self.selectedAdditions          = dictionary[@"selectedAdditions"];
        self.remarks                    = [NSString stringWithFormat:@"%@",dictionary[@"remarks"]];
        self.discount                   = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"discount"]]];
        self.allModifiersDict           = dictionary[@"allModifiersDict"];
        self.item_checked               = [NSNumber numberWithInt:[dictionary[@"item_checked"] intValue]];
        self.predefined_discount        = [NSString stringWithFormat:@"%@",dictionary[@"predefined_discount"]];
        self.stock_monitoring           = [NSNumber numberWithInt:[dictionary[@"stock_monitoring"] intValue]];
        self.category_id                = [NSNumber numberWithInt:[dictionary[@"category_id"] intValue]];
        
        self.gst_code                   = [NSString stringWithFormat:@"%@",dictionary[@"gst_code"]];
        self.gst_rate                   = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"gst_rate"]]];
        self.gst_value                  = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"gst_value"]]];
        self.gst_value_bfr_bill_disc    = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"gst_value_bfr_bill_disc"]]];
        self.refund_item_qty            = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"refund_item_qty"]]];
        self.remain_item_qty            = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"remain_item_qty"]]];
        self.refund_item_price          = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"refund_item_price"]]];

        
        self.type_of_price              = [NSString stringWithFormat:@"%@",dictionary[@"type_of_price"]];
        self.type_of_unit               = [NSString stringWithFormat:@"%@",dictionary[@"type_of_unit"]];
        self.uom_qty                    = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"uom_qty"]]];
        
        self.itemPrice_n_uomQty_n_modifier_perItem = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"itemPrice_n_uomQty_n_modifier_perItem"]]];
    }
    
    NSLog(@"all modifier : %@", self.allModifiersDict);
    
    return self;
}

-(OrderItem *)calculate_with_bill_disc:(NSDecimalNumber *)bill_disc_perc_item
{
    [self filter_unwanted_nil_or_zero_value];
    
    NSDecimalNumber *item_mod_price = [self get_modifier_price];
    NSDecimalNumber *one_hundred    = [NSDecimalNumber decimalNumberWithString:@"100.0"];

    self.itemPrice_n_uomQty_n_modifier_perItem = [[self.itemPrice
                                                   decimalNumberByAdding:item_mod_price]
                                                  decimalNumberByMultiplyingBy:self.uom_qty];

    self.totalPrice                 = [[[self.itemPrice_n_uomQty_n_modifier_perItem
                                         decimalNumberBySubtracting:self.discount]
                                        decimalNumberByMultiplyingBy:bill_disc_perc_item]
                                       decimalNumberByMultiplyingBy:self.itemQuantity];

    self.totalPrice_bfr_bill_disc   = [[self.itemPrice_n_uomQty_n_modifier_perItem
                                        decimalNumberBySubtracting:self.discount]
                                       decimalNumberByMultiplyingBy:self.itemQuantity];

    self.gst_value                  = [self.totalPrice decimalNumberByMultiplyingBy: // 100 * (6 / 106) 
                                       [self.gst_rate decimalNumberByDividingBy:[self.gst_rate decimalNumberByAdding:one_hundred]] withBehavior:CALCULATION_BEHAVIOR];

    self.gst_value_bfr_bill_disc    = [self.totalPrice_bfr_bill_disc decimalNumberByMultiplyingBy:
                                       [self.gst_rate decimalNumberByDividingBy:[self.gst_rate decimalNumberByAdding:one_hundred]] withBehavior:CALCULATION_BEHAVIOR];

    self.totalPrice_bfr_bill_disc_gst = [self.totalPrice_bfr_bill_disc decimalNumberBySubtracting:self.gst_value_bfr_bill_disc withBehavior:CALCULATION_BEHAVIOR];
    
    return self;
}

-(OrderItem *)new_calculate_with_bill_disc:(NSDecimalNumber *)bill_disc_perc_item
{
    [self filter_unwanted_nil_or_zero_value];

    NSDecimalNumber *item_mod_price = [self get_modifier_price];

    self.itemPrice_n_uomQty_n_modifier_perItem = [[self.itemPrice
                                                   decimalNumberByAdding:item_mod_price]
                                                  decimalNumberByMultiplyingBy:self.uom_qty];

    self.totalPrice                 = [[[self.itemPrice_n_uomQty_n_modifier_perItem
                                         decimalNumberBySubtracting:self.discount]
                                        decimalNumberByMultiplyingBy:bill_disc_perc_item]
                                       decimalNumberByMultiplyingBy:self.itemQuantity];

    self.totalPrice_bfr_bill_disc   = [[self.itemPrice_n_uomQty_n_modifier_perItem
                                        decimalNumberBySubtracting:self.discount]
                                       decimalNumberByMultiplyingBy:self.itemQuantity];

    self.gst_value                  = [NSDecimalNumber decimalNumberWithString:@"0.0"];

    self.gst_value_bfr_bill_disc    = [NSDecimalNumber decimalNumberWithString:@"0.0"];

    self.totalPrice_bfr_bill_disc_gst = self.totalPrice_bfr_bill_disc;

    return self;
}

-(OrderItem *)filter_unwanted_nil_or_zero_value
{
    if (!(self.itemPrice) || self.itemPrice.doubleValue < 0 )
    {
        self.itemPrice = [NSDecimalNumber decimalNumberWithString:@"0.0"];
    }
    
    if (!(self.uom_qty) || (self.uom_qty.doubleValue < 0))
    {
        self.uom_qty = [NSDecimalNumber decimalNumberWithString:@"1.0"];
    }
    
    if (!self.discount)
    {
        self.discount = [NSDecimalNumber decimalNumberWithString:@"0.0"];
    }
    
    if (self.gst_rate == NSDecimalNumber.notANumber)
    {
        self.gst_rate = [NSDecimalNumber decimalNumberWithString:@"0.0"];
    }
    
    return self;
}

-(NSDecimalNumber *)get_modifier_price
{
    NSDecimalNumber *item_mod_price = [NSDecimalNumber decimalNumberWithString:@"0.00"];

    if ([self.allModifiersDict isKindOfClass:[NSDictionary class]])
    {
        NSLog(@"i have modifier...");
        
        NSDictionary *item_modifier = self.allModifiersDict;
        
        for (NSDictionary *item_choice_modifier in [[item_modifier valueForKey:@"CHOICE"] valueForKey:@"selected_choice"]) // get CHOICE modifier price if any
        {
            if (![item_choice_modifier isKindOfClass:[NSNull class]])
            {
                NSDecimalNumber *mod_price = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_choice_modifier[@"mod_price"]]];
                item_mod_price = [item_mod_price decimalNumberByAdding:mod_price];
            }
        }
        
        for (NSDictionary *item_addition_modifier in item_modifier[@"ADDITION"])  // get ADDTION modifier price if any
        {
            NSDecimalNumber *mod_price = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_addition_modifier[@"mod_price"]]];
            NSDecimalNumber *mod_qty = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_addition_modifier[@"mod_qty"]]];
            
            if (mod_qty.intValue > 0)
            {
                NSDecimalNumber *mod_total = [mod_price decimalNumberByMultiplyingBy:mod_qty];
                
                item_mod_price = [item_mod_price decimalNumberByAdding:mod_total];
            }
        }
    }
    
    return item_mod_price;
}

- (NSDictionary *)getModifierLabelAndNumberOfLine
{
    int modifierNumberOfLines   = 0;
    NSMutableString *lbl_modifier = [NSMutableString stringWithString:@""];
    NSMutableDictionary *result = [[NSMutableDictionary alloc] init];
    
    if ([self.allModifiersDict isKindOfClass:[NSDictionary class]])
    {
        NSArray *addition_array     = [self.allModifiersDict objectForKey:ADDITION];
        NSArray *choice_array       = [self.allModifiersDict objectForKey:CHOICE];
        
        for (id choice_array_item in choice_array)
        {
            if([choice_array_item objectForKey:@"selected_choice"] > 0)
            {
                [lbl_modifier appendFormat:@"- %@ : %@ \n",
                 [[choice_array_item objectForKey:@"selected_choice"] objectForKey:@"mod_parent_name"],
                 [[choice_array_item objectForKey:@"selected_choice"] objectForKey:@"mod_name"]
                 ];
                modifierNumberOfLines++;
            }
        }
        
        for (id addition_array_item in addition_array)
        {
            if ([[addition_array_item objectForKey:@"mod_qty"] intValue] > 0)
            {
                [lbl_modifier appendFormat:@"  + %@ : %@ (%@) \n",
                 [addition_array_item objectForKey:@"mod_parent_name"],
                 [addition_array_item objectForKey:@"mod_name"],
                 [addition_array_item objectForKey:@"mod_qty"]
                 ];
                modifierNumberOfLines++;
            }
        }
    }
    
    result[@"numberOfLines"] = [NSString stringWithFormat:@"%d",modifierNumberOfLines];
    result[@"modifierLabel"] = lbl_modifier;
    
    return result;
}

- (NSString *)getModifierLabel
{
    NSMutableString *lbl_modifier = [NSMutableString stringWithString:@""];
    
    if ([self.allModifiersDict isKindOfClass:[NSDictionary class]])
    {
        NSArray *addition_array     = [self.allModifiersDict objectForKey:ADDITION];
        NSArray *choice_array       = [self.allModifiersDict objectForKey:CHOICE];
        
        for (id choice_array_item in choice_array)
        {
            if([choice_array_item objectForKey:@"selected_choice"] > 0)
            {
                [lbl_modifier appendFormat:@"- %@ : %@ \n",
                 [[choice_array_item objectForKey:@"selected_choice"] objectForKey:@"mod_parent_name"],
                 [[choice_array_item objectForKey:@"selected_choice"] objectForKey:@"mod_name"]
                 ];
            }
        }
        
        for (id addition_array_item in addition_array)
        {
            if ([[addition_array_item objectForKey:@"mod_qty"] intValue] > 0)
            {
                [lbl_modifier appendFormat:@"  + %@ : %@ (%@) \n",
                 [addition_array_item objectForKey:@"mod_parent_name"],
                 [addition_array_item objectForKey:@"mod_name"],
                 [addition_array_item objectForKey:@"mod_qty"]
                 ];
            }
        }
    }
    
    return lbl_modifier;
}

@end
