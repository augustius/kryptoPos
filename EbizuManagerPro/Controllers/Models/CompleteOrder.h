//
//  CompleteOrder.h
//  Ebizu Manager
//
//  Created by Swati on 1/6/14.
//  Copyright (c) 2014 EBIZU. All rights reserved.
//  Modified by Jamal on Aug-14
//

#import <Foundation/Foundation.h>

@interface CompleteOrder : NSObject <NSCopying>
@property(nonatomic,strong) NSArray *orderItemsArray;

@property(nonatomic,strong) NSString *hold_order_id;
@property(nonatomic,strong) NSString *trx_order_id;
@property(nonatomic,strong) NSString *trx_id;

@property(nonatomic,strong) NSString *create_time;
@property(nonatomic,strong) NSString *currency;
@property(nonatomic,strong) NSString *offer_sn;

@property(nonatomic,strong) NSString *member_id;
@property(nonatomic,strong) NSString *customer_id;
@property(nonatomic,strong) NSString *customer_name;
@property(nonatomic,strong) NSString *customer_profile_pic;
@property(nonatomic,strong) NSString *checkin_id;
@property(nonatomic,strong) NSString *customer_gstid;
@property(nonatomic,strong) NSString *customer_email;
@property(nonatomic,strong) NSString *customer_phone;
@property(nonatomic,strong) NSString *customer_address1;
@property(nonatomic,strong) NSString *customer_address2;
@property(nonatomic,strong) NSString *customer_city;
@property(nonatomic,strong) NSString *customer_pincode;
@property(nonatomic,strong) NSString *trx_gst_mode;

@property(nonatomic,assign) BOOL isOrderOnHold;
@property(nonatomic,assign) BOOL is_order_merged;

@property(nonatomic,strong) NSDecimalNumber *total;
@property(nonatomic,strong) NSDecimalNumber *subtotal_bfr_bill_disc;
@property(nonatomic,strong) NSDecimalNumber *subtotal;
@property(nonatomic,strong) NSDecimalNumber *tax_amount;
@property(nonatomic,strong) NSDecimalNumber *discount_amount;

@property(nonatomic,strong) NSDecimalNumber *discount_percentage;
@property(nonatomic,strong) NSDecimalNumber *tax_percent;
@property(nonatomic,strong) NSString *table_no;

@property(nonatomic,strong) NSString *payment; // payment type
@property(nonatomic,strong) NSString *payment_method_id; // payment method
@property(nonatomic,strong) NSString *pay_type_name;
@property(nonatomic,strong) NSDecimalNumber *payment_amount;
@property(nonatomic,strong) NSDecimalNumber *payment_changes;
@property(nonatomic,strong) NSString *credit_card_no;

@property(nonatomic,assign) int      order_type;
@property(nonatomic,strong) NSString *predefined_discount;
@property(nonatomic,strong) NSDecimalNumber *total_gst_value_afr_bill_disc;
@property(nonatomic,strong) NSDecimalNumber *total_gst_value_bfr_bill_disc;
@property(nonatomic,strong) NSDecimalNumber *total_gst_value;

@property(nonatomic,strong) NSString *voucher_id;
@property(nonatomic,strong) NSString *voucher_name;
@property(nonatomic,strong) NSDecimalNumber *voucher_amount;
@property(nonatomic,strong) NSString *voucher_type_id;
@property(nonatomic,strong) NSString *voucher_type_name;
@property(nonatomic,strong) NSString *voucher_member_number;
@property(nonatomic,strong) NSString *voucher_ref_number;

@property(nonatomic,assign) int      counter_number;
@property(nonatomic,strong) NSDecimalNumber *rounding_amount;
@property(nonatomic,strong) NSDecimalNumber *refund_total_amount;
@property(nonatomic,strong) NSString *trans_number;
@property(nonatomic,strong) NSString *is_multiple_payment;

@property(nonatomic,strong) NSString *voucher_bought_detail_id;

@property(nonatomic,strong) NSString *custom_field_name;

@property(nonatomic,strong) NSString *user_id;
@property(nonatomic,strong) NSString *user_name;
@property(nonatomic,strong) NSString *sales_person_name;
@property(nonatomic,strong) NSString *sales_person_id;
@property(nonatomic,assign) int      no_of_pax;
@property(nonatomic,strong) NSString *tbl_primary_id;

@property(nonatomic,assign) BOOL from_split_bill;

@property(nonatomic,assign) BOOL from_sales_report;


- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;
- (CompleteOrder *)calculate;
- (BOOL)isSSTmode;
- (BOOL)isNewSSTmode;
- (CompleteOrder *)newCalculate;
@end
