//
//  Categories.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 27/10/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Categories : NSObject<NSCopying>

@property(nonatomic,strong) NSString *ctg_id;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,strong) NSString *title;


- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;


@end
