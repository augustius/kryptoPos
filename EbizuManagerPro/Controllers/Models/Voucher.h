//
//  Voucher.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 10/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Voucher : NSObject<NSCopying>

@property(nonatomic,strong) NSString *voucher_id;
@property(nonatomic,strong) NSString *voucher_name;
@property(nonatomic,strong) NSString *voucher_amount;
@property(nonatomic,strong) NSString *voucher_type_id;
@property(nonatomic,strong) NSString *voucher_type_name;
@property(nonatomic,strong) NSString *voucher_member_number;
@property(nonatomic,strong) NSString *voucher_ref_number;
@property(nonatomic,strong) NSString *voucher_bought_detail_id;
@property(nonatomic,strong) NSString *redeemed;


- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
