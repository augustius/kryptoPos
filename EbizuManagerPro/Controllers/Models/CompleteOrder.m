//
//  CompleteOrder.m
//  Ebizu Manager
//
//  Created by Swati on 1/6/14.
//  Copyright (c) 2014 EBIZU. All rights reserved.
//  Modified by Jamal on Aug-14
//

#import "CompleteOrder.h"
#import <objc/runtime.h>


@implementation CompleteOrder

@synthesize trx_id                  = _trx_id;
@synthesize orderItemsArray         = _orderItemsArray;
@synthesize member_id               = _member_id;
@synthesize customer_id             = _customer_id;
@synthesize customer_name           = _customer_name;
@synthesize customer_profile_pic    = _customer_profile_pic;
@synthesize checkin_id              = _checkin_id;
@synthesize create_time             = _create_time;
@synthesize subtotal                = _subtotal;
@synthesize subtotal_bfr_bill_disc  = _subtotal_bfr_bill_disc;
@synthesize total                   = _total;
@synthesize currency                = _currency;
@synthesize discount_percentage     = _discount_percentage;
@synthesize discount_amount         = _discount_amount;
@synthesize tax_amount              = _tax_amount;
@synthesize offer_sn                = _offer_sn;
@synthesize hold_order_id           = _hold_order_id;
@synthesize isOrderOnHold           = _isOrderOnHold;
@synthesize tax_percent             = _tax_percent;
@synthesize payment                 = _payment;
@synthesize payment_method_id       = _payment_method_id;
@synthesize order_type              = _order_type;
@synthesize predefined_discount     = _predefined_discount;
@synthesize total_gst_value         = _total_gst_value;
@synthesize total_gst_value_afr_bill_disc = _total_gst_value_afr_bill_disc;
@synthesize total_gst_value_bfr_bill_disc = _total_gst_value_bfr_bill_disc;
@synthesize payment_amount          = _payment_amount;
@synthesize payment_changes         = _payment_changes;
@synthesize credit_card_no          = _credit_card_no;


@synthesize voucher_id              = _voucher_id;
@synthesize voucher_name            = _voucher_name;
@synthesize voucher_amount          = _voucher_amount;
@synthesize voucher_member_number   = _voucher_member_number;
@synthesize voucher_ref_number      = _voucher_ref_number;
@synthesize voucher_type_id         = _voucher_type_id;
@synthesize voucher_type_name       = _voucher_type_name;

@synthesize counter_number          = _counter_number;
@synthesize pay_type_name           = _pay_type_name;

@synthesize customer_gstid          = _customer_gstid;
@synthesize customer_email          = _customer_email;
@synthesize customer_phone          = _customer_phone;
@synthesize customer_address1       = _customer_address1;
@synthesize customer_address2       = _customer_address2;
@synthesize customer_city           = _customer_city;
@synthesize customer_pincode        = _customer_pincode;
@synthesize trx_gst_mode            = _trx_gst_mode;
@synthesize trx_order_id            = _trx_order_id;
@synthesize refund_total_amount     = _refund_total_amount;
@synthesize trans_number            = _trans_number;

@synthesize is_multiple_payment     = _is_multiple_payment;
@synthesize voucher_bought_detail_id = _voucher_bought_detail_id;
@synthesize custom_field_name       = _custom_field_name;

@synthesize user_id                 = _user_id;
@synthesize user_name               = _user_name;
@synthesize sales_person_id         = _sales_person_id;
@synthesize sales_person_name       = _sales_person_name;
@synthesize no_of_pax               = _no_of_pax;

@synthesize tbl_primary_id          = _tbl_primary_id;
@synthesize from_split_bill         = _from_split_bill;
@synthesize is_order_merged         = _is_order_merged;

@synthesize from_sales_report       = _from_sales_report;


-(id) copyWithZone: (NSZone *) zone
{
    CompleteOrder *copy = [[CompleteOrder allocWithZone: zone] init];
    
    copy.trx_id                 = self.trx_id;
    copy.orderItemsArray        = [[NSArray alloc] initWithArray: self.orderItemsArray copyItems:YES];
    copy.member_id              = self.member_id;
    copy.customer_id            = self.customer_id;
    copy.customer_name          = self.customer_name;
    copy.customer_profile_pic   = self.customer_profile_pic;
    copy.checkin_id             = self.checkin_id;
    copy.create_time            = self.create_time;
    copy.subtotal               = self.subtotal;
    copy.subtotal_bfr_bill_disc = self.subtotal_bfr_bill_disc;
    copy.total                  = self.total;
    copy.currency               = self.currency;
    copy.discount_percentage    = self.discount_percentage;
    copy.discount_amount        = self.discount_amount;
    copy.tax_amount             = self.tax_amount;
    copy.offer_sn               = self.offer_sn;
    copy.hold_order_id          = self.hold_order_id;
    copy.isOrderOnHold          = self.isOrderOnHold;
    copy.tax_percent            = self.tax_percent;
    copy.table_no               = self.table_no;
    copy.payment                = self.payment;
    copy.payment_method_id      = self.payment_method_id;
    copy.order_type             = self.order_type;
    copy.predefined_discount    = self.predefined_discount;
    copy.total_gst_value        = self.total_gst_value;
    copy.total_gst_value_afr_bill_disc = self.total_gst_value_afr_bill_disc;
    copy.total_gst_value_bfr_bill_disc = self.total_gst_value_bfr_bill_disc;
    copy.payment_amount         = self.payment_amount;
    copy.payment_changes        = self.payment_changes;
    copy.credit_card_no         = self.credit_card_no;
    copy.rounding_amount        = self.rounding_amount;
    
    copy.voucher_id             = self.voucher_id;
    copy.voucher_name           = self.voucher_name;
    copy.voucher_amount         = self.voucher_amount;
    copy.voucher_member_number  = self.voucher_member_number;
    copy.voucher_ref_number     = self.voucher_ref_number;
    copy.voucher_type_id        = self.voucher_type_id;
    copy.voucher_type_name      = self.voucher_type_name;
    
    copy.counter_number         = self.counter_number;
    copy.pay_type_name          = self.pay_type_name;
    
    copy.customer_gstid         = self.customer_gstid;
    copy.customer_email         = self.customer_email;
    copy.customer_phone         = self.customer_phone;
    copy.customer_address1      = self.customer_address1;
    copy.customer_address2      = self.customer_address2;
    copy.customer_city          = self.customer_city;
    copy.customer_pincode       = self.customer_pincode;
    copy.trx_gst_mode           = self.trx_gst_mode;
    copy.trx_order_id           = self.trx_order_id;
    copy.refund_total_amount    = self.refund_total_amount;
    copy.trans_number           = self.trans_number;
    
    copy.is_multiple_payment        = self.is_multiple_payment;
    copy.voucher_bought_detail_id   = self.voucher_bought_detail_id;
    copy.custom_field_name          = self.custom_field_name;
    
    copy.user_id                = self.user_id;
    copy.user_name              = self.user_name;
    copy.sales_person_id        = self.sales_person_id;
    copy.sales_person_name      = self.sales_person_name;
    copy.no_of_pax              = self.no_of_pax;
    copy.tbl_primary_id         = self.tbl_primary_id;
    copy.from_split_bill        = self.from_split_bill;
    copy.is_order_merged        = self.is_order_merged;
    
    copy.from_sales_report      = self.from_sales_report;
    
    return copy;
}

- (NSDictionary *)dictionaryRepresentation
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++)
    {
        NSString *key = @(property_getName(properties[i]));
        NSString *value = [self valueForKey:key];
        
        // if it is orderitem, need to convert to dict again
        if ([key isEqual: @"orderItemsArray"])
        {
            NSMutableArray *itemArrayDict = [[NSMutableArray alloc]init];
            for (OrderItem *item in  [self valueForKey:key])
            {
                NSDictionary *itemDict = [[item copy] dictionaryRepresentation];
                [itemArrayDict addObject: itemDict];
            }
            dictionary[key] = itemArrayDict;
        }
        else
        {
            // Only add to the NSDictionary if it's not nil.
            if (value || ![value isEqualToString:@""])
            {
                dictionary[key] = value;
            }
            else
            {
                if ([self needZeroDecimal:dictionary[key]])
                {
                    dictionary[key] = @"0.00";
                }
                else if ([self needZeroInt:dictionary[key]])
                {
                    dictionary[key] = @"0";
                }
                else if ([dictionary[key] isEqualToString:@"member_id"]) //special case
                {
                    dictionary[key] = @"0000";
                }
                else
                {
                    dictionary[key] = @"";
                }
            }
        }
    }
    
    free(properties);
    
    return dictionary;
}

-(BOOL)needZeroDecimal:(NSString *)key_name
{
    if ([key_name isEqualToString:@"subtotal"] ||
        [key_name isEqualToString:@"discount_amount"] ||
        [key_name isEqualToString:@"tax_amount"] ||
        [key_name isEqualToString:@"voucher_amount"]
        )
    {
        return YES;
    }
    
    return NO;
}

-(BOOL)needZeroInt:(NSString *)key_name
{
    if ([key_name isEqualToString:@"counter_number"] ||
        [key_name isEqualToString:@"discount_percentage"] ||
        [key_name isEqualToString:@"no_of_pax"]
        )
    {
        return YES;
    }
    
    return NO;
}

//-(instancetype)init {
//    self = [self init];
//    return self;
//}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    NSLog(@"dict : %@",dictionary);
    NSLog(@"hold_order_id : %@",dictionary[@"hold_order_id"]);
    
    if (self = [super init])
    {
        self.trx_id                  = [NSString stringWithFormat:@"%@",dictionary[@"trx_id"]];
        self.orderItemsArray         = dictionary[@"orderItemsArray"];
        self.member_id               = [NSString stringWithFormat:@"%@",dictionary[@"member_id"]];
        self.customer_id             = [NSString stringWithFormat:@"%@",dictionary[@"customer_id"]];
        self.customer_name           = [NSString stringWithFormat:@"%@",dictionary[@"customer_name"]];
        self.customer_profile_pic    = [NSString stringWithFormat:@"%@",dictionary[@"customer_profile_pic"]];
        self.checkin_id              = [NSString stringWithFormat:@"%@",dictionary[@"checkin_id"]];
        self.create_time             = [NSString stringWithFormat:@"%@",dictionary[@"create_time"]];
        self.subtotal                = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"subtotal"]]];
        self.subtotal_bfr_bill_disc  = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"subtotal_bfr_bill_disc"]]];
        self.total                   = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"total"]]];
        self.currency                = [NSString stringWithFormat:@"%@",dictionary[@"currency"]];
        self.discount_percentage     = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"discount_percentage"]]];
        self.discount_amount         = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"discount_amount"]]];
        self.tax_amount              = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"tax_amount"]]];
        self.offer_sn                = [NSString stringWithFormat:@"%@",dictionary[@"offer_sn"]];
        self.hold_order_id           = [NSString stringWithFormat:@"%@",dictionary[@"hold_order_id"]];
        self.isOrderOnHold           = [dictionary[@"isOrderOnHold"] boolValue];
        self.tax_percent             = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"tax_percent"]]];
        self.table_no                = [NSString stringWithFormat:@"%@",dictionary[@"table_no"]];
        self.payment                 = [NSString stringWithFormat:@"%@",dictionary[@"payment"]];
        self.payment_method_id       = [NSString stringWithFormat:@"%@",dictionary[@"payment_method_id"]];
        self.order_type              = [dictionary[@"order_type"] intValue];
        self.predefined_discount     = [NSString stringWithFormat:@"%@",dictionary[@"predefined_discount"]];
        self.total_gst_value         = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"total_gst_value"]]];
        self.total_gst_value_bfr_bill_disc = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"total_gst_value_bfr_bill_disc"]]];
        self.total_gst_value_afr_bill_disc = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"total_gst_value_afr_bill_disc"]]];
        self.payment_amount          = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"payment_amount"]]];
        self.payment_changes         = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"payment_changes"]]];
        self.credit_card_no          = [NSString stringWithFormat:@"%@",dictionary[@"credit_card_no"]];
        self.rounding_amount         = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"rounding_amount"]]];
        
        self.voucher_id              = [NSString stringWithFormat:@"%@",dictionary[@"voucher_id"]];
        self.voucher_name            = [NSString stringWithFormat:@"%@",dictionary[@"voucher_name"]];
        self.voucher_amount          = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"voucher_amount"]]];
        self.voucher_member_number   = [NSString stringWithFormat:@"%@",dictionary[@"voucher_member_number"]];
        self.voucher_ref_number      = [NSString stringWithFormat:@"%@",dictionary[@"voucher_ref_number"]];
        self.voucher_type_id         = [NSString stringWithFormat:@"%@",dictionary[@"voucher_type_id"]];
        self.voucher_type_name       = [NSString stringWithFormat:@"%@",dictionary[@"voucher_type_name"]];
        
        self.counter_number          = [dictionary[@"counter_number"] intValue];
        self.pay_type_name           = [NSString stringWithFormat:@"%@",dictionary[@"pay_type_name"]];
        
        self.customer_gstid          = [NSString stringWithFormat:@"%@",dictionary[@"customer_gstid"]];
        self.customer_email          = [NSString stringWithFormat:@"%@",dictionary[@"customer_email"]];
        self.customer_phone          = [NSString stringWithFormat:@"%@",dictionary[@"customer_phone"]];
        self.customer_address1       = [NSString stringWithFormat:@"%@",dictionary[@"customer_address1"]];
        self.customer_address2       = [NSString stringWithFormat:@"%@",dictionary[@"customer_address2"]];
        self.customer_city           = [NSString stringWithFormat:@"%@",dictionary[@"customer_city"]];
        self.customer_pincode        = [NSString stringWithFormat:@"%@",dictionary[@"customer_pincode"]];
        self.trx_gst_mode            = [NSString stringWithFormat:@"%@",dictionary[@"trx_gst_mode"]];
        self.trx_order_id            = [NSString stringWithFormat:@"%@",dictionary[@"trx_order_id"]];
        self.refund_total_amount     = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",dictionary[@"refund_total_amount"]]];
        self.trans_number            = [NSString stringWithFormat:@"%@",dictionary[@"trans_number"]];
        
        self.is_multiple_payment     = [NSString stringWithFormat:@"%@",dictionary[@"is_multiple_payment"]];
        self.voucher_bought_detail_id= [NSString stringWithFormat:@"%@",dictionary[@"voucher_bought_detail_id"]];
        self.custom_field_name       = [NSString stringWithFormat:@"%@",dictionary[@"custom_field_name"]];
        
        self.user_id                 = [NSString stringWithFormat:@"%@",dictionary[@"user_id"]];
        self.user_name               = [NSString stringWithFormat:@"%@",dictionary[@"user_name"]];
        self.sales_person_id         = [NSString stringWithFormat:@"%@",dictionary[@"sales_person_id"]];
        self.sales_person_name       = [NSString stringWithFormat:@"%@",dictionary[@"sales_person_name"]];
        self.no_of_pax               = [dictionary[@"no_of_pax"] intValue];
        self.tbl_primary_id          = [NSString stringWithFormat:@"%@",dictionary[@"tbl_primary_id"]];
        self.from_split_bill         = [dictionary[@"from_split_bill"] boolValue];
        self.is_order_merged         = [dictionary[@"is_order_merged"] boolValue];
        
        self.from_sales_report       = [dictionary[@"from_sales_report"] boolValue];
        
        NSLog(@"hold_order_id : %@",self.hold_order_id);
    }
    return self;
}


-(CompleteOrder *)calculate
{
    NSMutableArray *calculated_order_item_array = [[NSMutableArray alloc] init];
    NSDecimalNumber *one_hundred                = [NSDecimalNumber decimalNumberWithString:@"100.0"];
    NSDecimalNumber *taxes                      = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    
    if (self.from_sales_report)
    {
        if (self.tax_amount.doubleValue > 0.0)
        {
            taxes = [self.subtotal decimalNumberByDividingBy:self.tax_amount];
        }
    }
    else
    {
        taxes = [NSDecimalNumber decimalNumberWithString:
                 [[NSUserDefaults standardUserDefaults] boolForKey:@"SERVICE_CHARGE_DISABLE"] ? @"0.00" : [NSString stringWithFormat:@"%.2f",[[NSUserDefaults standardUserDefaults] doubleForKey:TAXES]]];
    }
    
    NSDecimalNumber *taxes_perc                 = [taxes decimalNumberByDividingBy:one_hundred withBehavior:CALCULATION_BEHAVIOR];
    
    NSLog(@"taxes percetnage : %@",taxes_perc);
    
    // reinit value
    self.subtotal         = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.total            = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.total_gst_value  = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.tax_amount       = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    
    NSDecimalNumber *discount_amount_percentage  = [NSDecimalNumber decimalNumberWithString:@"1.0"];
    
    //    ( (Selling price with GST + Modifier with GST)  * Quantity) - (GST Amount Rounded to 2 Decimal)
    //    * Example:
    //    =((1.00 + 0.99) * 10,000) - (6/106)
    //    =(19,900.00) - (1,126.4150943396)
    //    =19,900.00 - 1126.42
    //    =18,773.58
    
    if (self.subtotal_bfr_bill_disc.doubleValue > 0.0)
    {
        discount_amount_percentage = [discount_amount_percentage decimalNumberBySubtracting:
                                      [self.discount_amount decimalNumberByDividingBy:
                                       [self.subtotal_bfr_bill_disc decimalNumberByAdding:
                                        self.total_gst_value_bfr_bill_disc]]];
    }
    
    self.subtotal_bfr_bill_disc         = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.total_gst_value_bfr_bill_disc  = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    
    for (OrderItem *order_item in self.orderItemsArray)
    {
        OrderItem *item             = [order_item calculate_with_bill_disc:discount_amount_percentage];
        
        self.subtotal               = [self.subtotal decimalNumberByAdding:
                                       [item.totalPrice decimalNumberBySubtracting:item.gst_value]];
        
        self.subtotal_bfr_bill_disc = [self.subtotal_bfr_bill_disc decimalNumberByAdding:
                                       [item.totalPrice_bfr_bill_disc decimalNumberBySubtracting:item.gst_value_bfr_bill_disc]];
        
        self.total_gst_value        = [self.total_gst_value decimalNumberByAdding:item.gst_value];
        
        self.total_gst_value_bfr_bill_disc = [self.total_gst_value_bfr_bill_disc decimalNumberByAdding:item.gst_value_bfr_bill_disc];
        
        [calculated_order_item_array addObject:item];
    }
    
    self.total_gst_value_afr_bill_disc = self.total_gst_value; // this is to store gst without tax gst // for now only use in discount form for display purpose only
    
    self.discount_amount            = [[self.subtotal_bfr_bill_disc decimalNumberByAdding: self.total_gst_value_bfr_bill_disc]
                                       decimalNumberByMultiplyingBy: [[NSDecimalNumber decimalNumberWithString:@"1.0"]  decimalNumberBySubtracting:discount_amount_percentage]]; // need to recalculate for split bill
    
    self.tax_amount                 = [self.subtotal decimalNumberByMultiplyingBy:taxes_perc withBehavior:CALCULATION_BEHAVIOR];
    
    if (SESSION_DATA.gst_enable || self.from_sales_report)
    {
        double gstPercentage = [[NSUserDefaults standardUserDefaults] doubleForKey:GST_PERCENTAGE];
        
        if (self.from_sales_report)
        {
            // this is to determine previous transaction is gstEnable or not
            BOOL gstEnable = self.total_gst_value.doubleValue > 0;
            gstPercentage = gstEnable ? 6.00 : 0.00;
        }
        
        NSDecimalNumber *gst_percentage = [[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",gstPercentage]] decimalNumberByDividingBy:one_hundred withBehavior:CALCULATION_BEHAVIOR];
        
        self.total_gst_value            = [self.total_gst_value decimalNumberByAdding:
                                           [self.tax_amount decimalNumberByMultiplyingBy:gst_percentage withBehavior:CALCULATION_BEHAVIOR]];
    }
    
    self.total    = [[self.subtotal decimalNumberByAdding:self.tax_amount] decimalNumberByAdding:self.total_gst_value];
    self.total    = [self.total decimalNumberBySubtracting:self.voucher_amount];
    self.total    = self.total.doubleValue > 0.0 ? self.total : [NSDecimalNumber decimalNumberWithString:@"0.00"];

    
    NSDecimalNumber *total_rounded_Value    = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",round(self.total.doubleValue/0.05)*0.05]];
    self.rounding_amount                    = [total_rounded_Value decimalNumberBySubtracting:self.total];
    self.total                              = total_rounded_Value;
    
    self.orderItemsArray = [NSArray arrayWithArray:calculated_order_item_array];
    
    return self;
}

- (BOOL)isSSTmode {
    return ([self.trx_gst_mode isEqualToString:@"trx_sst_en"] || [self.trx_gst_mode isEqualToString:@"trx_non"]);
}

- (BOOL)isNewSSTmode {
    return ([self.trx_gst_mode isEqualToString:@"en_sst"] || [self.trx_gst_mode isEqualToString:@"non_sst"]);
}

-(CompleteOrder *)newCalculate
{
    NSMutableArray *calculated_order_item_array = [[NSMutableArray alloc] init];
    NSDecimalNumber *one_hundred                = [NSDecimalNumber decimalNumberWithString:@"100.0"];
    NSDecimalNumber *taxes                      = [NSDecimalNumber decimalNumberWithString:@"0.00"];

    if (self.from_sales_report)
    {
        if (self.tax_amount.doubleValue > 0.0)
        {
            taxes = [self.subtotal decimalNumberByDividingBy:self.tax_amount];
        }
    }
    else
    {
        taxes = [NSDecimalNumber decimalNumberWithString:
                 [[NSUserDefaults standardUserDefaults] boolForKey:@"SERVICE_CHARGE_DISABLE"] ? @"0.00" : [NSString stringWithFormat:@"%.2f",[[NSUserDefaults standardUserDefaults] doubleForKey:TAXES]]];
    }

    NSDecimalNumber *taxes_perc                 = [taxes decimalNumberByDividingBy:one_hundred withBehavior:CALCULATION_BEHAVIOR];

    NSLog(@"taxes percetnage : %@",taxes_perc);

    // reinit value
    self.subtotal         = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.total            = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.total_gst_value  = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.tax_amount       = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.total_gst_value_bfr_bill_disc = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.total_gst_value_afr_bill_disc = [NSDecimalNumber decimalNumberWithString:@"0.00"];

    NSDecimalNumber *discount_amount_percentage  = [NSDecimalNumber decimalNumberWithString:@"1.0"];

    if (self.subtotal_bfr_bill_disc.doubleValue > 0.0)
    {
        discount_amount_percentage = [discount_amount_percentage decimalNumberBySubtracting:
                                      [self.discount_amount decimalNumberByDividingBy:
                                       self.subtotal_bfr_bill_disc]];
    }

    self.subtotal_bfr_bill_disc         = [NSDecimalNumber decimalNumberWithString:@"0.00"];
    self.total_gst_value_bfr_bill_disc  = [NSDecimalNumber decimalNumberWithString:@"0.00"];

    for (OrderItem *order_item in self.orderItemsArray)
    {
        OrderItem *item             = [order_item new_calculate_with_bill_disc:discount_amount_percentage];

        self.subtotal               = [self.subtotal decimalNumberByAdding: item.totalPrice];

        self.subtotal_bfr_bill_disc = [self.subtotal_bfr_bill_disc decimalNumberByAdding: item.totalPrice_bfr_bill_disc];

        [calculated_order_item_array addObject:item];
    }

    self.discount_amount            = [self.subtotal_bfr_bill_disc
                                       decimalNumberByMultiplyingBy: [[NSDecimalNumber decimalNumberWithString:@"1.0"]  decimalNumberBySubtracting:discount_amount_percentage]]; // need to recalculate for split bill

    self.tax_amount                 = [self.subtotal decimalNumberByMultiplyingBy:taxes_perc withBehavior:CALCULATION_BEHAVIOR];

    if (SESSION_DATA.gst_enable || self.from_sales_report)
    {
        double gstPercentage = [[NSUserDefaults standardUserDefaults] doubleForKey:GST_PERCENTAGE];

        if (self.from_sales_report)
        {
            // this is to determine previous transaction is gstEnable or not
            BOOL gstEnable = [self.trx_gst_mode isEqualToString:@"en_sst"];
            gstPercentage = gstEnable ? 6.00 : 0.00;
        }

        NSDecimalNumber *gst_percentage = [[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",gstPercentage]] decimalNumberByDividingBy:one_hundred withBehavior:CALCULATION_BEHAVIOR];

        self.total_gst_value        = [self.subtotal decimalNumberByMultiplyingBy:gst_percentage withBehavior:CALCULATION_BEHAVIOR];

        self.total_gst_value_bfr_bill_disc = [self.subtotal_bfr_bill_disc decimalNumberByMultiplyingBy:gst_percentage withBehavior:CALCULATION_BEHAVIOR];

        self.total_gst_value_afr_bill_disc = self.total_gst_value; // this is to store gst without tax gst // for now only use in discount form for display purpose only
    }

    self.total    = [[self.subtotal decimalNumberByAdding:self.tax_amount] decimalNumberByAdding:self.total_gst_value];
    self.total    = [self.total decimalNumberBySubtracting:self.voucher_amount];
    self.total    = self.total.doubleValue > 0.0 ? self.total : [NSDecimalNumber decimalNumberWithString:@"0.00"];


    NSDecimalNumber *total_rounded_Value    = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",round(self.total.doubleValue/0.05)*0.05]];
    self.rounding_amount                    = [total_rounded_Value decimalNumberBySubtracting:self.total];
    self.total                              = total_rounded_Value;

    self.orderItemsArray = [NSArray arrayWithArray:calculated_order_item_array];

    return self;
}

@end
