//
//  Table.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 14/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Table : NSObject<NSCopying>

@property(nonatomic,strong) NSString *table_id;
@property(nonatomic,strong) NSString *table_no;
@property(nonatomic,strong) NSString *total_chair;
@property(nonatomic,strong) NSString *table_area;
@property(nonatomic,strong) NSString *table_shape;
@property(nonatomic,strong) NSString *table_x;
@property(nonatomic,strong) NSString *table_y;
@property(nonatomic,strong) NSString *table_color;
@property(nonatomic,strong) NSString *order_id;
@property(nonatomic,strong) NSString *no_of_pax;
@property(nonatomic,strong) NSString *merged_into;
@property(nonatomic,strong) NSString *total_amount;

- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
