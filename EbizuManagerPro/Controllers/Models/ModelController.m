//
//  ModelController.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 25/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "ModelController.h"

@implementation ModelController

// Lazy instantiation
- (CompleteOrder *)currentCompleteOrder
{
    if (!_currentCompleteOrder)
    {
        _currentCompleteOrder = [[CompleteOrder alloc] init];
    }
    return _currentCompleteOrder;
}

- (Customer *)currentCustomer
{
    if (!_currentCustomer)
    {
        _currentCustomer = [[Customer alloc] init];
    }
    return _currentCustomer;
}

+ (instancetype)sharedModelController
{
    static ModelController *controller = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
                  {
                      controller = [[ModelController alloc] init];
                  });
    
    return controller;
}



@end
