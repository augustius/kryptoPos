//
//  Table.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 14/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "Table.h"
#import <objc/runtime.h>

@implementation Table

@synthesize table_id    = _table_id;
@synthesize table_no    = _table_no;
@synthesize total_chair = _total_chair;
@synthesize table_area  = _table_area;
@synthesize table_shape = _table_shape;
@synthesize table_x     = _table_x;
@synthesize table_y     = _table_y;
@synthesize table_color = _table_color;
@synthesize order_id    = _order_id;
@synthesize no_of_pax   = _no_of_pax;
@synthesize merged_into = _merged_into;
@synthesize total_amount = _total_amount;

- (id)copyWithZone: (NSZone *)zone
{
    Table *copy = [[Table allocWithZone:zone] init];
    
    [copy setTable_id    : self.table_id];
    [copy setTable_no    : self.table_no];
    [copy setTotal_chair : self.total_chair];
    [copy setTable_area  : self.table_area];
    [copy setTable_shape : self.table_shape];
    [copy setTable_x     : self.table_x];
    [copy setTable_y     : self.table_y];
    [copy setTable_color : self.table_color];
    [copy setOrder_id    : self.order_id];
    [copy setNo_of_pax   : self.no_of_pax];
    [copy setMerged_into : self.merged_into];
    [copy setTotal_amount : self.total_amount];
    
    return copy;
}

- (NSDictionary *)dictionaryRepresentation
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++)
    {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
    }
    
    free(properties);
    
    return dictionary;
}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.table_id    = [NSString stringWithFormat:@"%@",dictionary[@"table_id"]];
        self.table_no    = [NSString stringWithFormat:@"%@",dictionary[@"table_no"]];
        self.total_chair = [NSString stringWithFormat:@"%@",dictionary[@"total_chair"]];
        self.table_area  = [NSString stringWithFormat:@"%@",dictionary[@"table_area"]];
        self.table_shape = [NSString stringWithFormat:@"%@",dictionary[@"table_shape"]];
        self.table_x     = [NSString stringWithFormat:@"%@",dictionary[@"table_x"]];
        self.table_y     = [NSString stringWithFormat:@"%@",dictionary[@"table_y"]];
        self.table_color = [NSString stringWithFormat:@"%@",dictionary[@"table_color"]];
        self.order_id    = [NSString stringWithFormat:@"%@",dictionary[@"order_id"]];
        self.no_of_pax   = [NSString stringWithFormat:@"%@",dictionary[@"no_of_pax"] ?: @""];
        self.merged_into = [NSString stringWithFormat:@"%@",dictionary[@"merged_into"] ?: @""];
        self.total_amount = [NSString stringWithFormat:@"%@",dictionary[@"total_amount"] ?: @""];
    }
    return self;
}


@end
