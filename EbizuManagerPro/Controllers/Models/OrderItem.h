//
//  Order.h
//  Ebizu Manager
//
//  Created by Swati on 11/22/13.
//  Copyright (c) 2013 EBIZU. All rights reserved.
//   Modified By Jamal on Aug-14
//

#import <Foundation/Foundation.h>

@interface OrderItem : NSObject <NSCopying>

@property(nonatomic,strong) NSString    *hold_item_id;
@property(nonatomic,strong) NSString    *itemID;
@property(nonatomic,strong) NSString    *itemCatID;
@property(nonatomic,strong) NSString    *itemImage;
@property(nonatomic,strong) NSString    *itemName;
@property(nonatomic,strong) NSString    *is_edited_item_price;
@property(nonatomic,strong) NSDecimalNumber    *itemQuantity;
@property(nonatomic,strong) NSDecimalNumber    *itemQuantityPrinted;
@property(nonatomic,strong) NSDecimalNumber    *split_item_qty;

@property(nonatomic,strong) NSDecimalNumber *itemPrice;
@property(nonatomic,strong) NSDecimalNumber *totalPrice;
@property(nonatomic,strong) NSDecimalNumber *totalPrice_bfr_bill_disc;
@property(nonatomic,strong) NSDecimalNumber *totalPrice_bfr_bill_disc_gst;
@property(nonatomic,strong) NSNumber *stock_left;
@property(nonatomic,strong) NSDecimalNumber *cost_price;
@property(nonatomic,strong) NSDecimalNumber *selling_price;

@property(nonatomic,strong) NSArray     *selectedChoices;
@property(nonatomic,strong) NSArray     *selectedAdditions;
@property(nonatomic,strong) NSString    *remarks;
@property(nonatomic,strong) NSDecimalNumber    *discount;
@property(nonatomic,strong) NSDictionary *allModifiersDict;
@property(nonatomic,strong) NSNumber    *item_checked;
@property(nonatomic,strong) NSString    *predefined_discount;
@property(nonatomic,strong) NSNumber    *stock_monitoring;
@property(nonatomic,strong) NSNumber    *category_id;

@property(nonatomic,strong) NSString    *gst_code;
@property(nonatomic,strong) NSDecimalNumber    *gst_rate;
@property(nonatomic,strong) NSDecimalNumber    *gst_value;
@property(nonatomic,strong) NSDecimalNumber    *gst_value_bfr_bill_disc;
@property(nonatomic,strong) NSDecimalNumber    *refund_item_qty;
@property(nonatomic,strong) NSDecimalNumber    *remain_item_qty;
@property(nonatomic,strong) NSDecimalNumber    *refund_item_price;

@property(nonatomic,strong) NSString    *type_of_price;
@property(nonatomic,strong) NSString    *type_of_unit;
@property(nonatomic,strong) NSDecimalNumber    *uom_qty;

@property(nonatomic,strong) NSDecimalNumber    *itemPrice_n_uomQty_n_modifier_perItem;

@property (NS_NONATOMIC_IOSONLY, getter=getModifierLabel, readonly, copy) NSString *modifierLabel;
@property (NS_NONATOMIC_IOSONLY, getter=getModifierLabelAndNumberOfLine, readonly, copy) NSDictionary<NSString *,NSString *> *ModifierLabelAndNumberOfLine;


- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;
- (OrderItem *)calculate_with_bill_disc:(NSDecimalNumber *)bill_disc_perc_item;
- (OrderItem *)new_calculate_with_bill_disc:(NSDecimalNumber *)bill_disc_perc_item;


@end
