//
//  Customer.h
//  Ebizu Manager
//
//  Created by Swati on 11/26/13.
//  Copyright (c) 2013 EBIZU. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Customer : NSObject <NSCopying>

@property (nonatomic,strong) NSString *user_id;
@property (nonatomic,strong) NSString *checkin_id;
@property (nonatomic,strong) NSString *member_id;
@property (nonatomic,strong) NSString *customer_id;

@property (nonatomic,strong) NSString *profilePic;
@property (nonatomic,strong) UIImage  *justAddedProfilePicUIImage;
@property (nonatomic,strong) NSString *first_name;
@property (nonatomic,strong) NSString *last_name;
@property (nonatomic,strong) NSString *full_name;
@property (nonatomic,strong) NSString *email;
@property (nonatomic,strong) NSString *mobile;
@property (nonatomic,strong) NSString *address;
@property (nonatomic,strong) NSString *postCode;
@property (nonatomic,strong) NSString *city;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSString *birthDate;

//below model probably for manis
@property (nonatomic,strong) NSString *level;
@property (nonatomic,strong) NSString *lastVisit;
@property (nonatomic,strong) NSNumber *manisPts;
@property (nonatomic,strong) NSString *mostPurchasedCategory;
@property (nonatomic,strong) NSString *mostPurchasedCategoryItem;
@property (nonatomic,strong) NSNumber *totalSpent;

@property (NS_NONATOMIC_IOSONLY, getter=getProfilePicUIImage, readonly ,copy) UIImage *storedProfilePicUIImage;
@property (NS_NONATOMIC_IOSONLY, getter=getCustomerFullName, readonly ,copy) NSString *customerFullName;


- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
