//
//  Discount.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 09/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Discount : NSObject<NSCopying>

@property(nonatomic,strong) NSString *discount_id;
@property(nonatomic,strong) NSString *discount_desc;
@property(nonatomic,strong) NSString *amount;
@property(nonatomic,strong) NSString *percentage;


- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
