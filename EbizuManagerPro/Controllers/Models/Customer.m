//
//  Customer.m
//  Ebizu Manager
//
//  Created by Swati on 11/26/13.
//  Copyright (c) 2013 EBIZU. All rights reserved.
//

#import "Customer.h"
#import <objc/runtime.h>

@implementation Customer

@synthesize user_id     = _user_id;
@synthesize checkin_id  = _checkin_id;
@synthesize member_id   = _member_id;
@synthesize customer_id = _customer_id;

@synthesize profilePic  = _profilePic;
@synthesize justAddedProfilePicUIImage = _justAddedProfilePicUIImage;
@synthesize first_name  = _first_name;
@synthesize last_name   = _last_name;
@synthesize full_name   = _full_name;
@synthesize email       = _email;
@synthesize mobile      = _mobile;
@synthesize address     = _address;
@synthesize postCode    = _postCode;
@synthesize city        = _city;
@synthesize gender      = _gender;
@synthesize birthDate   = _birthDate;

@synthesize level       = _level;
@synthesize lastVisit   = _lastVisit;
@synthesize manisPts    = _manisPts;
@synthesize mostPurchasedCategory       = _mostPurchasedCategory;
@synthesize mostPurchasedCategoryItem   = _mostPurchasedCategoryItem;
@synthesize totalSpent  = _totalSpent;


-(id) copyWithZone: (NSZone *) zone
{
    Customer *copy = [[Customer allocWithZone: zone] init];
    
    copy.user_id         = self.user_id;
    copy.checkin_id      = self.checkin_id;
    copy.member_id       = self.member_id;
    copy.customer_id     = self.customer_id;
    
    copy.profilePic      = self.profilePic;
    copy.justAddedProfilePicUIImage = self.justAddedProfilePicUIImage;
    copy.first_name      = self.first_name;
    copy.last_name       = self.last_name;
    copy.email           = self.email;
    copy.mobile          = self.mobile;
    copy.address         = self.address;
    copy.postCode        = self.postCode;
    copy.city            = self.city;
    copy.gender          = self.gender;
    copy.birthDate       = self.birthDate;
    
    copy.level           = self.level;
    copy.lastVisit       = self.lastVisit;
    copy.manisPts        = self.manisPts;
    copy.mostPurchasedCategory      = self.mostPurchasedCategory;
    copy.mostPurchasedCategoryItem  = self.mostPurchasedCategoryItem;
    copy.totalSpent      = self.totalSpent;

    return copy;
}

- (NSDictionary *)dictionaryRepresentation
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++)
    {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
    }
    
    free(properties);
    
    return dictionary;
}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.user_id     = [NSString stringWithFormat:@"%@",dictionary[@"user_id"]];
        self.checkin_id  = [NSString stringWithFormat:@"%@",dictionary[@"checkin_id"]];
        self.member_id   = [NSString stringWithFormat:@"%@",dictionary[@"member_id"]];
        self.customer_id = [NSString stringWithFormat:@"%@",dictionary[@"customer_id"]];
        
        self.profilePic  = [NSString stringWithFormat:@"%@",dictionary[@"profilePic"]];
        self.justAddedProfilePicUIImage = dictionary[@"justAddedProfilePicUIImage"];
        self.first_name  = [NSString stringWithFormat:@"%@",dictionary[@"first_name"]];
        self.last_name   = [NSString stringWithFormat:@"%@",dictionary[@"last_name"]];
        self.email       = [NSString stringWithFormat:@"%@",dictionary[@"email"]];
        self.mobile      = [NSString stringWithFormat:@"%@",dictionary[@"mobile"]];
        self.address     = [NSString stringWithFormat:@"%@",dictionary[@"address"]];
        self.postCode    = [NSString stringWithFormat:@"%@",dictionary[@"postCode"]];
        self.city        = [NSString stringWithFormat:@"%@",dictionary[@"city"]];
        self.gender      = [NSString stringWithFormat:@"%@",dictionary[@"gender"]];
        self.birthDate   = [NSString stringWithFormat:@"%@",dictionary[@"birthDate"]];
        
        self.level       = [NSString stringWithFormat:@"%@",dictionary[@"level"]];
        self.lastVisit   = [NSString stringWithFormat:@"%@",dictionary[@"lastVisit"]];
        self.manisPts    = dictionary[@"manisPts"];
        self.mostPurchasedCategory       = [NSString stringWithFormat:@"%@",dictionary[@"mostPurchasedCategory"]];
        self.mostPurchasedCategoryItem   = [NSString stringWithFormat:@"%@",dictionary[@"mostPurchasedCategoryItem"]];
        self.totalSpent  = dictionary[@"totalSpent"];
    }
    return self;
}

- (UIImage *)getProfilePicUIImage
{
    UIImage *custImage = [UIImage imageNamed:CUSTOMER_DEFAULT_PIC]; //default image for customer
    
    if (self.profilePic)
    {
        NSRange nameRange  = [self.profilePic rangeOfString:@"/" options:NSBackwardsSearch];
        
        if (!(nameRange.length > 0)) //if image dont have the '/' then proceed
        {
            nameRange = [self.profilePic rangeOfString:@"."];
            
            if (nameRange.length > 0) //if image have the '.' then proceed
            {
                NSData *imgData = [FileUtils getDataFromDocDirWithPath:[NSString stringWithFormat:@"%@/%@",POSImages,IMG_CUSTOMERS]
                                                              fileName:[self.profilePic substringToIndex:nameRange.location]
                                                               andExtn:@"jpg"];
                
                if (imgData)
                {
                    custImage = [UIImage imageWithData:imgData];
                }
            }
        }
    }
    
    return custImage;
}

- (NSString *)getCustomerFullName
{
    NSString *custFullName; // default name for customer
    
    if (self.first_name)
    {
        custFullName = [NSString stringWithFormat:@"%@",self.first_name];
        if (self.last_name)
        {
            custFullName = [NSString stringWithFormat:@"%@ %@",self.first_name, self.last_name];
        }
    }
    
    return custFullName;
}

@end
