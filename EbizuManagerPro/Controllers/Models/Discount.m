//
//  Discount.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 09/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "Discount.h"
#import <objc/runtime.h>

@implementation Discount

@synthesize discount_id     = _discount_id;
@synthesize discount_desc   = _discount_desc;
@synthesize amount          = _amount;
@synthesize percentage      = _percentage;

- (id)copyWithZone: (NSZone *) zone
{
    Discount *copy = [[Discount allocWithZone:zone] init];
    
    [copy setDiscount_id    : self.discount_id];
    [copy setDiscount_desc  : self.discount_desc];
    [copy setAmount         : self.amount];
    [copy setPercentage     : self.percentage];
    
    return copy;
}

- (NSDictionary *)dictionaryRepresentation
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++)
    {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
    }
    
    free(properties);
    
    return dictionary;
}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.discount_id    = [NSString stringWithFormat:@"%@",dictionary[@"discount_id"]];
        self.discount_desc  = [NSString stringWithFormat:@"%@",dictionary[@"discount_desc"]];
        self.amount         = [NSString stringWithFormat:@"%@",dictionary[@"amount"]];
        self.percentage     = [NSString stringWithFormat:@"%@",dictionary[@"percentage"]];
    }
    return self;
}


@end
