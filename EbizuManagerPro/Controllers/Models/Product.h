//
//  Product.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 27/10/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject<NSCopying>

@property(nonatomic,strong) NSString *cost_price;
@property(nonatomic,strong) NSString *desc;
@property(nonatomic,strong) NSString *gst_code;
@property(nonatomic,strong) NSString *gst_rate;
@property(nonatomic,strong) NSString *item_cat_id;
@property(nonatomic,strong) NSString *item_id;
@property(nonatomic,strong) NSString *item_image;
@property(nonatomic,strong) NSString *item_name;
@property(nonatomic,strong) NSString *online_order;
@property(nonatomic,strong) NSString *price;
@property(nonatomic,strong) NSString *selling_price;
@property(nonatomic,strong) NSString *stock_left;
@property(nonatomic,strong) NSString *stock_monitoring;
@property(nonatomic,strong) NSString *type_of_price;
@property(nonatomic,strong) NSString *type_of_unit;

@property (NS_NONATOMIC_IOSONLY, getter=getItemUIImage, readonly, copy) UIImage *itemUIImage;

- (NSDictionary *)dictionaryRepresentation;
- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
