//
//  Categories.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 27/10/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "Categories.h"
#import <objc/runtime.h>

@implementation Categories

@synthesize ctg_id  = _ctg_id;
@synthesize image   = _image;
@synthesize title   = _title;


-(id) copyWithZone: (NSZone *) zone
{
    Categories *copy = [[Categories allocWithZone: zone] init];
    
    [copy setCtg_id     : self.ctg_id];
    [copy setImage      : self.image];
    [copy setTitle      : self.title];

    return copy;
}

- (NSDictionary *)dictionaryRepresentation
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++)
    {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
    }
    
    free(properties);
    
    return dictionary;
}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.ctg_id     = [NSString stringWithFormat:@"%@",dictionary[@"ctg_id"]];
        self.image      = [NSString stringWithFormat:@"%@",dictionary[@"image"]];
        self.title      = [NSString stringWithFormat:@"%@",dictionary[@"title"]];
    }
    return self;
}


@end
