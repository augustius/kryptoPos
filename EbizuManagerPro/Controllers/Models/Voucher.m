//
//  Voucher.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 10/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "Voucher.h"
#import <objc/runtime.h>

@implementation Voucher

@synthesize voucher_id               = _voucher_id;
@synthesize voucher_name             = _voucher_name;
@synthesize voucher_amount           = _voucher_amount;
@synthesize voucher_type_id          = _voucher_type_id;
@synthesize voucher_type_name        = _voucher_type_name;
@synthesize voucher_member_number    = _voucher_member_number;
@synthesize voucher_ref_number       = _voucher_ref_number;
@synthesize voucher_bought_detail_id = _voucher_bought_detail_id;
@synthesize redeemed                 = _redeemed;

- (id)copyWithZone: (NSZone *)zone
{
    Voucher *copy = [[Voucher allocWithZone:zone] init];
    
    [copy setVoucher_id               : self.voucher_id];
    [copy setVoucher_name             : self.voucher_name];
    [copy setVoucher_amount           : self.voucher_amount];
    [copy setVoucher_type_id          : self.voucher_type_id];
    [copy setVoucher_type_name        : self.voucher_type_name];
    [copy setVoucher_member_number    : self.voucher_member_number];
    [copy setVoucher_ref_number       : self.voucher_ref_number];
    [copy setVoucher_bought_detail_id : self.voucher_bought_detail_id];
    [copy setRedeemed                 : self.redeemed];
    
    return copy;
}

- (NSDictionary *)dictionaryRepresentation
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++)
    {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
    }
    
    free(properties);
    
    return dictionary;
}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.voucher_id               = [NSString stringWithFormat:@"%@",[dictionary[@"voucher_id"] isKindOfClass:[NSString class]] ? dictionary[@"voucher_id"] : @""];
        self.voucher_name             = [NSString stringWithFormat:@"%@",[dictionary[@"voucher_name"] isKindOfClass:[NSString class]] ? dictionary[@"voucher_name"] : @""];
        self.voucher_amount           = [NSString stringWithFormat:@"%@",[dictionary[@"voucher_amount"] isKindOfClass:[NSString class]] ? dictionary[@"voucher_amount"] : @"0.00"];
        self.voucher_type_id          = [NSString stringWithFormat:@"%@",[dictionary[@"voucher_type_id"] isKindOfClass:[NSString class]] ? dictionary[@"voucher_type_id"] :@""];
        self.voucher_type_name        = [NSString stringWithFormat:@"%@",[dictionary[@"voucher_type_name"] isKindOfClass:[NSString class]] ? dictionary[@"voucher_type_name"] : @""];
        self.voucher_member_number    = [NSString stringWithFormat:@"%@",[dictionary[@"voucher_member_number"] isKindOfClass:[NSString class]] ? dictionary[@"voucher_member_number"] : @""];
        self.voucher_ref_number       = [NSString stringWithFormat:@"%@",[dictionary[@"voucher_ref_number"] isKindOfClass:[NSString class]] ? dictionary[@"voucher_ref_number"] : @""];
        self.voucher_bought_detail_id = [NSString stringWithFormat:@"%@",[dictionary[@"voucher_bought_detail_id"] isKindOfClass:[NSString class]] ? dictionary[@"voucher_bought_detail_id"] : @""];
    }
    return self;
}


@end
