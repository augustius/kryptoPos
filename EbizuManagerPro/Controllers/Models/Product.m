//
//  Product.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 27/10/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "Product.h"
#import <objc/runtime.h>

@implementation Product

@synthesize cost_price      = _cost_price;
@synthesize desc            = _desc;
@synthesize gst_code        = _gst_code;
@synthesize gst_rate        = _gst_rate;
@synthesize item_cat_id     = _item_cat_id;
@synthesize item_id         = _item_id;
@synthesize item_image      = _item_image;
@synthesize item_name       = _item_name;
@synthesize online_order    = _online_order;
@synthesize price           = _price;
@synthesize selling_price   = _selling_price;
@synthesize stock_left      = _stock_left;
@synthesize stock_monitoring = _stock_monitoring;
@synthesize type_of_price   = _type_of_price;
@synthesize type_of_unit    = _type_of_unit;

-(id) copyWithZone: (NSZone *) zone
{
    Product *copy = [[Product allocWithZone: zone] init];
    
    [copy setCost_price      : self.cost_price];
    [copy setDesc            : self.desc];
    [copy setGst_code        : self.gst_code];
    [copy setGst_rate        : self.gst_rate];
    [copy setItem_cat_id     : self.item_cat_id];
    [copy setItem_id         : self.item_id];
    [copy setItem_image      : self.item_image];
    [copy setItem_name       : self.item_name];
    [copy setOnline_order    : self.online_order];
    [copy setPrice           : self.price];
    [copy setSelling_price   : self.selling_price];
    [copy setStock_left      : self.stock_left];
    [copy setStock_monitoring : self.stock_monitoring];
    [copy setType_of_price   : self.type_of_price];
    [copy setType_of_unit    : self.type_of_unit];
    
    return copy;
}

- (NSDictionary *)dictionaryRepresentation
{
    unsigned int count = 0;
    // Get a list of all properties in the class.
    objc_property_t *properties = class_copyPropertyList([self class], &count);
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithCapacity:count];
    
    for (int i = 0; i < count; i++)
    {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        NSString *value = [self valueForKey:key];
        
        // Only add to the NSDictionary if it's not nil.
        if (value)
            [dictionary setObject:value forKey:key];
    }
    
    free(properties);
    
    return dictionary;
}

- (instancetype)initWithDictionary:(NSDictionary*)dictionary
{
    if (self = [super init])
    {
        self.cost_price      = [NSString stringWithFormat:@"%@",dictionary[@"cost_price"]];
        self.desc            = [NSString stringWithFormat:@"%@",dictionary[@"desc"]];
        self.gst_code        = [NSString stringWithFormat:@"%@",dictionary[@"gst_code"]];
        self.gst_rate        = [NSString stringWithFormat:@"%@",dictionary[@"gst_rate"]];
        self.item_cat_id     = [NSString stringWithFormat:@"%@",dictionary[@"item_cat_id"]];
        self.item_id         = [NSString stringWithFormat:@"%@",dictionary[@"item_id"]];
        self.item_image      = [NSString stringWithFormat:@"%@",dictionary[@"item_image"]];
        self.item_name       = [NSString stringWithFormat:@"%@",dictionary[@"item_name"]];
        self.online_order    = [NSString stringWithFormat:@"%@",dictionary[@"online_order"]];
        self.price           = [NSString stringWithFormat:@"%@",dictionary[@"price"]];
        self.selling_price   = [NSString stringWithFormat:@"%@",dictionary[@"selling_price"]];
        self.stock_left      = [NSString stringWithFormat:@"%@",dictionary[@"stock_left"]];
        self.stock_monitoring = [NSString stringWithFormat:@"%@",dictionary[@"stock_monitoring"]];
        self.type_of_price   = [NSString stringWithFormat:@"%@",dictionary[@"type_of_price"]];
        self.type_of_unit    = [NSString stringWithFormat:@"%@",dictionary[@"type_of_unit"]];
        
    }
    return self;
}

- (UIImage *)getItemUIImage
{
    UIImage *image;
    
    if (![self.item_image isEqualToString:@"default_product.png"])
    {
        NSRange nameRange = [self.item_image rangeOfString:@"."];
        NSData *imgData = [FileUtils getDataFromDocDirWithPath:[NSString stringWithFormat:@"%@/%@",POSImages,IMG_ITEMS] fileName:[self.item_image substringToIndex:nameRange.location] andExtn:@"jpg"];
        if (imgData)
        {
            image = [UIImage imageWithData:imgData];
        }
    }
    
    return image;
}


@end
