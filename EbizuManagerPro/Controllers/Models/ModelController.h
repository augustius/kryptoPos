//
//  ModelController.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 25/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Customer.h"
#import "CompleteOrder.h"

@interface ModelController : NSObject

@property (nonatomic, strong) CompleteOrder *currentCompleteOrder;
@property (nonatomic, strong) Customer      *currentCustomer;

+ (instancetype)sharedModelController;

@end


