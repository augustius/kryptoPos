//
//  shareddb+shiftDetail.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+shiftDetail.h"

@implementation shareddb (shiftDetail)

- (NSMutableDictionary *)get_last_shift_time_n_date
{
    @try
    {
        
        NSString    *shift_open_time    = @"";
        NSString    *shift_closed_time  = @"";
        NSString    *last_shift         = @"";
        NSMutableDictionary *shift_dict = [[NSMutableDictionary alloc] init];
        
        NSString *query = @"SELECT datetime(shift_open_time,'unixepoch','localtime') AS shift_open_time,"
        "CASE WHEN shift_closed_time = '0' "
        "THEN '' "
        "ELSE datetime(shift_closed_time,'unixepoch','localtime') END AS shift_closed_time "
        "FROM SHIFT_DETAILS ORDER BY created_date DESC LIMIT 1";
        
        NSArray *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            shift_open_time   = dict[@"shift_open_time"] ;
            shift_closed_time = dict[@"shift_closed_time"] ;
        }
        last_shift = [shift_closed_time isEqualToString:@""] ? shift_open_time : shift_closed_time;
        
        shift_dict[@"shift_open_time"]   = shift_open_time;
        shift_dict[@"shift_closed_time"] = shift_closed_time;
        shift_dict[@"last_shift"]        = last_shift;
        
        return shift_dict;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSDictionary *)getLastShiftRecord
{
    NSArray *allShift = [self getAllShiftRecord];
    NSDictionary *lastShift;
    
    if (allShift.count > 0)
    {
        lastShift = allShift.firstObject;
    }
    
    return lastShift;
}

- (NSArray *)getAllShiftRecord
{
    @try
    {
        NSString *query = @"SELECT SHIFT_DETAILS.id, "
                        "datetime(shift_open_time, 'unixepoch','localtime') AS open_time, "
                        "CASE shift_closed_time "
                            "WHEN '0' THEN datetime('now','localtime') "
                            "ELSE datetime(shift_closed_time, 'unixepoch','localtime') END close_time, "
                        "shift_open_time AS open_time_epoch, "
                        "shift_closed_time AS close_time_epoch, "
                        "CASE shift_closed_time "
                            "WHEN '0' THEN strftime('%Y-%m',datetime('now','localtime')) "
                            "ELSE strftime('%Y-%m',datetime(shift_closed_time, 'unixepoch','localtime')) END month_year, "
                        "CASE (CASE shift_closed_time "
                                "WHEN '0' THEN strftime('%m',datetime('now','localtime')) "
                                "ELSE strftime('%m',datetime(shift_closed_time, 'unixepoch','localtime')) END) "
                            "WHEN '01' THEN 'January' "
                            "WHEN '02' THEN 'Febuary' "
                            "WHEN '03' THEN 'March' "
                            "WHEN '04' THEN 'April' "
                            "WHEN '05' THEN 'May' "
                            "WHEN '06' THEN 'June' "
                            "WHEN '07' THEN 'July' "
                            "WHEN '08' THEN 'August' "
                            "WHEN '09' THEN 'September' "
                            "WHEN '10' THEN 'October' "
                            "WHEN '11' THEN 'November' "
                            "ELSE 'December' END month, "
                        "CASE shift_closed_time "
                            "WHEN '0' THEN strftime('%Y',datetime('now','localtime')) "
                            "ELSE strftime('%Y',datetime(shift_closed_time, 'unixepoch','localtime')) END year "
                        "FROM SHIFT_DETAILS "
                        "ORDER BY close_time DESC ";
        
        NSArray *resultSet   = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray *)get_all_shift_record:(NSString *)dates
{
    @try
    {
        NSMutableDictionary *shift_data;
        NSMutableArray      *shiftArray  = [[NSMutableArray alloc] init];
        
        /** getting whole range of time to show all */
        shift_data = [[NSMutableDictionary alloc] init];
        shift_data[@"open_time"]        = @"show";
        shift_data[@"close_time"]       = @"all";
        [shiftArray addObject:shift_data];
        /*******************************************/
        
        NSString *query = [NSString stringWithFormat:
                           @"SELECT datetime(shift_open_time, 'unixepoch','localtime') AS open_time,"
                           "datetime(shift_closed_time, 'unixepoch','localtime') AS close_time,"
                           "shift_open_time AS open_time_epoch,"
                           "shift_closed_time AS close_time_epoch,"
                           "date(shift_open_time, 'unixepoch','localtime') AS selected_date FROM shift_details "
                           "WHERE selected_date = '%@'",dates];
        
        NSArray *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            shift_data = [[NSMutableDictionary alloc] init];
            shift_data[@"open_time"]        = dict[@"open_time"];
            shift_data[@"open_time_epoch"]  = dict[@"open_time_epoch"];
            
            if (dict[ @"close_time"] && ![dict[@"close_time_epoch"] isEqualToString:@"0"])
            {
                shift_data[@"close_time"]       = dict[@"close_time"];
                shift_data[@"close_time_epoch"] = dict[@"close_time_epoch"];
            }
            else
            {
                shift_data[@"close_time"] = @"now";
            }
            
            [shiftArray addObject:shift_data];
        }
        
        return shiftArray;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}


@end
