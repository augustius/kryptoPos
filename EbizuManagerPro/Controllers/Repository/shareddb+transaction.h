//
//  shareddb+transaction.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 28/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (transaction)

- (void)insert_trx_details:(NSDictionary*)dict;
- (CompleteOrder *)get_transaction_detail_by_transaction_number:(NSString *)transaction_number;
- (NSMutableDictionary *)get_all_transaction_modifiers:(NSString *)category_id withModItemID:(NSString *)mod_item_id;
- (NSArray *)getSalesReportFrom:(NSDate *)startShift to:(NSDate *)endShift withPaymentId:(NSString *)paymentId;

#pragma mark - Mainly used for daily sales report Section
#pragma mark Total of Bill Discount
- (NSArray *)get_total_discount_today;
- (NSArray *)get_total_discount_date:(NSDate *)date;
- (NSArray *)get_total_discount_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Breakdown of Bill Discount
- (NSArray *)get_discount_given_on_bill_today;
- (NSArray *)get_discount_given_on_bill_date:(NSDate*)date;
- (NSArray *)get_discount_given_on_bill_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Total of Item Discount
- (NSArray *)get_total_discount_item_today;
- (NSArray *)get_total_discount_item_date:(NSDate *)date;
- (NSArray *)get_total_discount_item_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Breakdown of Item Discount
- (NSArray *)get_discount_given_on_item_today;
- (NSArray *)get_discount_given_on_item_date:(NSDate *)date;
- (NSArray *)get_discount_given_on_item_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Breakdown of Voucher
- (NSArray*)get_total_voucher_today;
- (NSArray*)get_total_voucher_date:(NSDate *)date;
- (NSArray*)get_total_voucher_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Breakdown of Categories & Item Sales
- (NSArray *)get_category_group_sales_today;
- (NSArray *)get_category_group_sales_date:(NSDate *)date;
- (NSArray *)get_category_group_sales_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Total of Tax
- (NSString *)get_total_tax_today;
- (NSString *)get_total_tax_date:(NSDate *)date;
- (NSString *)get_total_tax_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Total of GST n SST
- (NSString *)get_total_gst_sst_today;
- (NSString *)get_total_gst_sst_date:(NSDate *)date;
- (NSString *)get_total_gst_sst_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Total of SST
- (NSString *)get_total_sst_today;
- (NSString *)get_total_sst_date:(NSDate *)date;
- (NSString *)get_total_sst_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Total of Cash & Card Sales
- (NSString *)get_cash_sales_today:(BOOL)cash;
- (NSString *)get_cash_sales:(BOOL)cash with_date:(NSDate *)date;
- (NSString *)get_cash_sales:(BOOL)cash from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark BreakDown of Payment Type
- (NSArray*)get_total_payment_today;
- (NSArray*)get_total_payment_date:(NSDate *)date;
- (NSArray*)get_total_payment_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark - For Customer transaction data
- (NSArray *)getTransactionBasedOnCustId:(NSString *)custId;

#pragma mark - Update Customer feedback
- (void)update_customer_feedback:(NSString *)trx_id rating:(NSString *)rate;

@end
