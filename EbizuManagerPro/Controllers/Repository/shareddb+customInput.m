//
//  shareddb+customInput.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+customInput.h"

@implementation shareddb (customInput)

- (NSArray *)get_custom_field_name
{
    @try
    {
        NSString    *query      = @"SELECT custom_field_name FROM CUSTOM_INPUT";
        NSArray     *resultSet  = [self get_from_queue:query];

        return (resultSet.count == 0) ? @[@""] : resultSet;
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

@end
