//
//  shareddb+userrights.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+userrights.h"

@implementation shareddb (userrights)

- (BOOL)supervisor_pin_check:(NSString *)pin_code
{
    @try
    {
        BOOL result         = NO;
        NSString *query     = [NSString stringWithFormat:
                               @"SELECT super_user FROM USERRIGHTS "
                               "WHERE name = '%@' AND password = '%@' AND active_status = '1'",
                               [StringUtils get_user_detail:@"name"],pin_code];
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            result = ([dict[@"super_user"]intValue] != 3);
        }
        return result;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (BOOL)validate_user_pin:(NSString *)password
{
    @try
    {
        BOOL result        = NO;
        NSString *query    = [NSString stringWithFormat:
                              @"SELECT name, email, user_id, last_login, super_user "
                              "FROM USERRIGHTS WHERE password = '%@' AND active_status = '1'",password];
        NSArray *resultSet = [self get_from_queue:query];
        
        if(resultSet.count > 0)
        {
            result = YES;
            [[NSUserDefaults standardUserDefaults] setObject:resultSet.firstObject forKey:@"user_data"];
        }
        
        return result;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)update_last_login:(NSString *)user_id
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE USERRIGHTS SET"
                           " last_login    = '%@',"
                           " sync_id       = '%@',"
                           " updated_by    = '%@',"
                           " updated_date  = '%@'"
                           " WHERE user_id = '%@'",
                           CURRENT_TIMESTAMP,
                           @"",
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP,
                           user_id
                           ];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray *)get_all_sp_data
{
    @try
    {
        NSString *query = @"SELECT user_id AS sales_person_id,"
        "name AS sales_person_name FROM USERRIGHTS WHERE is_sales_person = 1 AND active_status = '1'";
        
        NSArray *resultSet = [self get_from_queue:query];
            
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray *)get_all_employee
{
    @try
    {
        NSString *query = @"SELECT user_id,name FROM USERRIGHTS WHERE active_status = '1'";
        
        NSArray *resultSet = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
