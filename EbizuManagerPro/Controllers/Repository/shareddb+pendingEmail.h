//
//  shareddb+pendingEmail.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (pendingEmail)

- (void)add_pending_email:(NSString *)email withTrxId:(NSString *)trx_id;

@end
