//
//  shareddb+pendingEmail.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+pendingEmail.h"

@implementation shareddb (pendingEmail)

- (void)add_pending_email:(NSString *)email withTrxId:(NSString *)trx_id
{
    @try
    {
        NSDate *today               = [NSDate date];
        NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"dd-MM-yyyy hh:mm";
        NSString *current_date      = [formatter stringFromDate:today];
        
        NSString *query = [NSString stringWithFormat:
                           @"INSERT INTO PENDING_EMAILS (email_add,trx_id,last_try,created_by) VALUES ('%@','%@','%@','%@')",
                           email,
                           trx_id,
                           current_date,
                           [StringUtils get_business_detail:@"business_userid"]
                           ];
        
        [self add_to_queue:query];        
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}


@end
