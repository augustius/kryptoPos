//
//  shareddb+refund.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 27/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (refund)

#pragma mark - method use for 'Refund Report'
- (NSArray *)getRefundReportFrom:(NSDate *)shiftStart to:(NSDate *)shiftEnd withPaymentId:(NSString *)paymentId;

#pragma mark - method use for 'Insert Refund Data'
- (void)refund_transaction:(CompleteOrder *)data;

#pragma mark - method use for 'Daily Sales Report Print Out'
#pragma mark Total of Refund
- (NSString *)get_total_refund_today;
- (NSString *)get_total_refund_date:(NSDate *)date;
- (NSString *)get_total_refund_date_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Total of Refund GST n SST
- (NSString *)get_refund_gst_sst_today;
- (NSString *)get_refund_gst_sst_date:(NSDate *)date;
- (NSString *)get_refund_gst_sst_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Total of Refund SST
- (NSString *)get_refund_sst_today;
- (NSString *)get_refund_sst_date:(NSDate *)date;
- (NSString *)get_refund_sst_today_from:(NSDate *)shift_start to:(NSDate *)shift_end;

#pragma mark Breakdown of Categories & Item Refund
- (NSDictionary *)get_total_refund_cat_today:(NSString *)trn_cat_id
                                by_date_from:(NSDate *)start_date
                                       until:(NSDate *)end_date;
- (NSDictionary *)get_total_refund_item_today:(NSString *)trn_item_id
                                 by_date_from:(NSDate *)start_date
                                        until:(NSDate *)end_date;
- (NSDictionary *)get_total_refund_item_choice_today:(NSString *)mod_id
                                        by_date_from:(NSDate *)start_date
                                               until:(NSDate *)end_date;

@end
