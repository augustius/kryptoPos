//
//  shareddb+transactionNumber.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (transactionNumber)

- (void)increase_transaction_number;
- (void)get_transaction_number;

@end
