//
//  shareddb+printer.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+printer.h"

@implementation shareddb (printer)

- (NSArray *)get_list_of_printers
{
    @try
    {
        NSString *query = @"SELECT id,ip_address,printer_name,printer_type,category_id,default_printer,print_copies "
        "FROM PRINTERS WHERE active_status = 1";
        
        NSMutableDictionary *printers;
        NSMutableArray      *printer_array = [[NSMutableArray alloc] init];
        NSArray             *resultSet     = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            printers = [[NSMutableDictionary alloc] init];
            printers[@"id"]              = dict[@"id"];
            printers[@"ip_address"]      = dict[@"ip_address"];
            printers[@"printer_name"]    = dict[@"printer_name"];
            printers[@"printer_type"]    = dict[@"printer_type"];
            printers[@"category_id"]     = dict[@"category_id"];
            printers[@"default_printer"] = dict[@"default_printer"];
            printers[@"print_copies"]    = dict[@"print_copies"];
            printers[@"item_checked"]    = [dict[@"default_printer"] isEqualToString:@"1"] ? @"1" : @"0";

            [printer_array addObject:printers];
        }
        return printer_array;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSString *)get_categories_for_printer:(NSString *)printer_name
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT category_id "
                           "FROM PRINTERS "
                           "WHERE printer_name = '%@'",printer_name
                           ];
        NSLog(@"%@",query);
        
        NSString *categories;
        NSArray  *resultSet = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            categories = dict[@"category_id"];
        }
        
        return categories;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSString *)addPrinter:(NSDictionary *)data
{
    @try
    {
        NSString *printerId = [self get_unique_id_from_db];
        NSString *query = [NSString stringWithFormat:
                           @"INSERT INTO PRINTERS"
                           "(id,"
                           "ip_address,"
                           "printer_name,"
                           "printer_type,"
                           "category_id,"
                           "print_copies,"
                           "default_printer,"
                           "company_id,"
                           "created_by,"
                           "active_status)"
                           @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           printerId,
                           [data valueForKey:@"ip_address"],
                           [data valueForKey:@"printer_name"],
                           [data valueForKey:@"printer_type"],
                           [data valueForKey:@"category_id"],
                           [data valueForKey:@"print_copies"],
                           [data valueForKey:@"default_printer"],
                           [StringUtils get_business_detail:@"business_id"],
                           [StringUtils get_user_detail:@"user_id"],
                           @"1"
                           ];
        [self add_to_queue:query];
        return printerId;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)add_printer:(NSArray *)data
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"INSERT INTO PRINTERS"
                           "(id,"
                           "ip_address,"
                           "printer_name,"
                           "printer_type,"
                           "category_id,"
                           "print_copies,"
                           "default_printer,"
                           "company_id,"
                           "created_by,"
                           "active_status)"
                           @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [self get_unique_id_from_db],
                           [data[0] valueForKey:@"ip_address"],
                           [data[0] valueForKey:@"printer_name"],
                           [data[0] valueForKey:@"printer_type"],
                           [data[0] valueForKey:@"category_id"],
                           [data[0] valueForKey:@"print_copies"],
                           @"1",
                           [StringUtils get_business_detail:@"business_id"],
                           [StringUtils get_user_detail:@"user_id"],
                           @"1"
                           ];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)editPrinter:(NSDictionary *)data
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE PRINTERS SET "
                           "ip_address = '%@', "
                           "printer_name = '%@', "
                           "printer_type = '%@', "
                           "category_id = '%@', "
                           "print_copies = '%@', "
                           "default_printer = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@', "
                           "sync_id = null "
                           "WHERE id = '%@'",
                           [data valueForKey:@"ip_address"],
                           [data valueForKey:@"printer_name"],
                           [data valueForKey:@"printer_type"],
                           [data valueForKey:@"category_id"],
                           [data valueForKey:@"print_copies"],
                           [data valueForKey:@"default_printer"],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP,
                           [data valueForKey:@"printer_id"]
                           ];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)edit_printer_print_copies:(NSString *)printer_id withPrintCopies:(NSString *)print_copies
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE PRINTERS SET "
                           "print_copies = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@', "
                           "sync_id = null "
                           "WHERE id = '%@'",
                           print_copies,
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP,
                           printer_id];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)edit_printer_default:(NSArray *)printer_data
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE PRINTERS SET "
                           "default_printer = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@', "
                           "sync_id = null "
                           "WHERE id = '%@'",
                           [printer_data valueForKey:@"default_printer"],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP,
                           [printer_data valueForKey:@"id"]];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)delete_printer_from_pos:(NSString *)printer_id
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE PRINTERS SET active_status = 0, sync_id = null WHERE id = '%@'",printer_id];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray *)get_all_default_printers_from_bill_only:(BOOL)bill
{
    @try
    {
        NSString *bill_only_statement = bill ? @"AND printer_type='Bill'" : @"";
        
        NSString *query = [NSString stringWithFormat:
                           @"SELECT ip_address,printer_name,printer_type,print_copies,category_id "
                           "FROM PRINTERS WHERE default_printer = 1 %@ "
                           "AND active_status = 1 ",bill_only_statement];
        
        NSArray *resultSet = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)update_printer_category_id:(NSString *)category_id printer_name:(NSString *)printer_name
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE PRINTERS SET "
                           "category_id   = '%@', "
                           "updated_by    = '%@', "
                           "updated_date  = '%@', "
                           "sync_id       = null "
                           "WHERE printer_name = '%@'",
                           category_id,
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP,
                           printer_name];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
