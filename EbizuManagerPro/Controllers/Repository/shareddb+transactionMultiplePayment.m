//
//  shareddb+transactionMultiplePayment.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 31/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+transactionMultiplePayment.h"

@implementation shareddb (transactionMultiplePayment)

- (void)insert_multiple_payment_details:(NSArray *) data
{
    @try
    {
        NSMutableArray *array_of_query = [[NSMutableArray alloc]init];
        
        if(data.count > 0)
        {
            for (NSDictionary *payment_data in data)
            {
                NSString *unique_id = [self get_unique_id_from_db];
                
                NSString *query = [NSString stringWithFormat:
                                   @"INSERT INTO TRANSACTION_MULTIPLE_PAYMENTS"
                                   "(id,"
                                   "phm_pay_method_id,"
                                   "phm_poh_sn,"
                                   "phm_current_paid_amount,"
                                   "phm_total_amount,"
                                   "phm_payment_type_id,"
                                   "phm_pay_name,"
                                   "phm_payment_type_name,"
                                   "phm_ref_no,"
                                   "phm_com_id)"
                                   @" VALUES('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                                   unique_id,
                                   payment_data[@"id"],
                                   payment_data[@"trx_id"],
                                   payment_data[@"paid_amount"],
                                   [NSString stringWithFormat:@"%@",payment_data[@"total_amount"]],
                                   [NSString stringWithFormat:@"%@",payment_data[@"type_id"]],
                                   [NSString stringWithFormat:@"%@",payment_data[@"pay_name"]],
                                   [NSString stringWithFormat:@"%@",payment_data[@"type_name"]],
                                   payment_data[@"credit_card_no"],
                                   [NSString stringWithFormat:@"%@",[StringUtils get_business_detail:@"business_id"]]
                                   ];
                
                [array_of_query addObject:query];
            }
            
            [self add_to_queue:array_of_query with_priority:low];
        }
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

@end
