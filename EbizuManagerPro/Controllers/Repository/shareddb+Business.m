//
//  shareddb+Business.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 10/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+Business.h"

@implementation shareddb (Business)

- (NSString *)get_business_type
{
    @try
    {
        NSString *business_type = @"FNB";
        NSString *query         = [NSString stringWithFormat:@"SELECT business_type FROM BUSINESS"];
        NSArray *resultSet      = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            business_type = dict[@"business_type"];
        }
        
        return  business_type;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (BOOL)business_details_exist
{
    @try
    {
        NSString *query         = @"SELECT * FROM BUSINESS";
        NSArray *resultSet      = [self get_from_queue:query];
        
        return resultSet.count > 0;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (BOOL)check_for_username:(NSString *)user n_password:(NSString *)pass
{
    @try
    {
        NSString *query     = [NSString stringWithFormat:
                               @"SELECT * FROM BUSINESS "
                               "WHERE BUSINESS.business_email = '%@' "
                               "AND BUSINESS.business_pswd = '%@' ",
                               user,pass];
        NSArray *resultSet  = [self get_from_queue:query];
        
        return resultSet.count > 0;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}


@end
