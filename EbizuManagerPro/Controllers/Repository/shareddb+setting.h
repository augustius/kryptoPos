//
//  shareddb+setting.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 10/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (setting)

#pragma mark - Barcode Scanner
- (BOOL)get_barcode;
- (void)set_barcode:(BOOL)barcode;

#pragma mark - QR Validation
- (BOOL)get_qr_validation;
- (void)set_qr_validation:(BOOL)qr_code;

#pragma mark - Dine Type
- (BOOL)get_dine_type;
- (void)set_dine_type:(BOOL)dine_type;

#pragma mark - Image Mode
- (BOOL)get_image_mode;
- (void)set_image_mode:(BOOL)image_mode;

#pragma mark - GST
- (BOOL)get_gst;

#pragma mark - Custom Sale
- (BOOL)get_custom_sales;
- (void)set_custom_sales:(int)custom_sale;

#pragma mark - Mqtt Mode
- (BOOL)get_mqtt_mode;
- (void)set_mqtt_mode:(int)mqtt_mode;
- (void)configure_mqtt_details_with_ip:(NSString*)mqtt_ip mqtt_model:(NSString *)model_name;

#pragma mark - Multi Devices
- (BOOL)get_multi_device;
- (void)set_multi_device:(int)multi_device;

#pragma mark - Printer Mode
- (BOOL)get_printer_mode;
- (void)set_printer_mode:(BOOL)printer_mode;

#pragma mark - Printer Merchant Address , Name , Phone Mode
- (void)set_merchant_address_mode:(BOOL)address_mode name_mode:(BOOL)name_mode phone_mode:(BOOL)phone_mode;

#pragma mark Printer Merchant Address Mode
- (BOOL)get_merchant_address_mode;
- (void)set_merchant_address_mode:(BOOL)merchant_address;

#pragma mark Printer Merchant Name Mode
- (BOOL)get_merchant_name_mode;
- (void)set_merchant_name_mode:(BOOL)merchant_name;

#pragma mark Printer Merchant Phone Mode
- (BOOL)get_merchant_phone_mode;
- (BOOL)set_merchant_phone_mode:(BOOL)merchant_phone;

#pragma mark - Printer Merchant Additional Data
- (void)get_header_footer;
- (void)update_header_footer:(NSDictionary *)data;

#pragma mark - Hold Order Number & Counter Number
- (void)get_hold_order_number_n_order_count;
- (void)increase_hold_order_number;
- (void)increase_order_counter_number;
- (void)reset_order_count_to_one;

#pragma mark - Current balance
- (void)get_current_balance;
- (void)update_current_balance:(double)amount with_plus:(BOOL)plus;


@end
