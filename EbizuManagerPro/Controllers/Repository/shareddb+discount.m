//
//  shareddb+discount.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+discount.h"

@implementation shareddb (discount)

- (NSArray *)get_predefined_discount
{
    @try
    {
        NSString *query     = @"SELECT id as discount_id,discount_desc,amount,percentage FROM DISCOUNT WHERE active_status = 1";
        NSArray *resultSet  = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSString *)get_predefined_discount_by_id:(NSString *)discount_id
{
    @try
    {
        NSString *discount_desc = @"Discount Type";
        NSString *query         = [NSString stringWithFormat:
                                   @"SELECT discount_desc FROM DISCOUNT where id='%@'",discount_id];
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            discount_desc = dict[ @"discount_desc"];
        }
        
        return discount_desc;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
