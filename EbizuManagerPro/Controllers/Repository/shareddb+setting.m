//
//  shareddb+setting.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 10/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+setting.h"

@implementation shareddb (setting)

#pragma mark - Barcode Scanner
- (BOOL)get_barcode
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"SELECT enable_barcode FROM SETTING"];
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"enable_barcode"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_barcode:(BOOL)barcode
{
    @try
    {
        int value_to_store = barcode ? 1 : 0;
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set enable_barcode = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@';",
                           [NSString stringWithFormat:@"%d",value_to_store],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.barcode_enable = barcode;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - QR Validation
- (BOOL)get_qr_validation
{
    @try
    {
        NSString *query     = @"SELECT enable_qr_validation FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"enable_qr_validation"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_qr_validation:(BOOL)qr_code
{
    @try
    {
        int value_to_store = qr_code ? 1 : 0;
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set enable_qr_validation = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",value_to_store],[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],CURRENT_TIMESTAMP
                           ];
        [self add_to_queue:query];
        
        SESSION_DATA.qr_validation_enable = qr_code;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Dine Type
- (BOOL)get_dine_type
{
    @try
    {
        NSString *query     = @"SELECT enable_dine_type FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[ @"enable_dine_type"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_dine_type:(BOOL)dine_type
{
    @try
    {
        int value_to_store = dine_type ? 1 : 0;
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set enable_dine_type = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",value_to_store],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.dine_in_enable = dine_type;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Image Mode
- (BOOL)get_image_mode
{
    @try
    {
        NSString *query     = @"SELECT image_OnOff_type FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"image_OnOff_type"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_image_mode:(BOOL)image_mode
{
    @try
    {
        int value_to_store = image_mode ? 1 : 0;
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set image_OnOff_type = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",value_to_store],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.image_on_off_enable = image_mode;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - GST
- (BOOL)get_gst
{
    @try
    {
        NSString *query     = @"SELECT enable_gst FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"enable_gst"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Custom Sale
- (BOOL)get_custom_sales
{
    @try
    {
        NSString *query     = @"SELECT custom_sales FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[ @"custom_sales"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_custom_sales:(int)custom_sale
{
    NSString *query   = [NSString stringWithFormat:
                         @"UPDATE SETTING set custom_sales = '%@', "
                         "updated_by = '%@', "
                         "updated_date = '%@'",
                         [NSString stringWithFormat:@"%d",custom_sale],
                         [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                         CURRENT_TIMESTAMP];
    [self add_to_queue:query];
    
    SESSION_DATA.custom_sales_enable = custom_sale;
}

#pragma mark - Mqtt Mode
- (BOOL)get_mqtt_mode
{
    @try
    {
        NSString *query     = @"SELECT is_mqtt_enabled FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"is_mqtt_enabled"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_mqtt_mode:(int)mqtt_mode
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set is_mqtt_enabled = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",mqtt_mode],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.enable_mqtt = mqtt_mode;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)configure_mqtt_details_with_ip:(NSString*)mqtt_ip mqtt_model:(NSString *)model_name
//TODO: put to queue using high priority if expect return
{
    //    @try
    //    {
    //        NSString *query;
    //        BOOL result = NO;
    //
    //        query   = [NSString stringWithFormat:@"UPDATE SETTING set mqtt_server_ip = ?, "
    //                   "mqtt_model_name = ?, "
    //                   "updated_by      = ?, "
    //                   "updated_date    = ?;"];
    //
    //        result  = [db executeUpdate:query,mqtt_ip,model_name,[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID], CURRENT_TIMESTAMP];
    //
    //        if ([db hadError])
    //        {
    //            NSLog(@"DB ERROR - %s %@",__PRETTY_FUNCTION__,[db lastErrorMessage]);
    //            return NO;
    //        }
    //
    //        return result;
    //    }
    //    @catch(NSException *e)
    //    {
    //        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    //    }
}

#pragma mark - Multi Devices
- (BOOL)get_multi_device
{
    @try
    {
        NSString *query     = @"SELECT is_multi_device FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"is_multi_device"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_multi_device:(int)multi_device
{
    @try
    {
        NSString *query   = [NSString stringWithFormat:
                             @"UPDATE SETTING set is_multi_device = '%@', "
                             "updated_by = '%@', "
                             "updated_date = '%@'",
                             [NSString stringWithFormat:@"%d",multi_device],
                             [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                             CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.enable_multi_devices = multi_device;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Printer Mode
- (BOOL)get_printer_mode
{
    @try
    {
        NSString *query     = @"SELECT printer_mode FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"printer_mode"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_printer_mode:(BOOL)printer_mode
{
    @try
    {
        int value_to_store = printer_mode ? 1 : 0;
        
        NSString *query = [NSString stringWithFormat:@"UPDATE SETTING set printer_mode = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",value_to_store],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.force_print_mode_enable = printer_mode;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Printer Merchant Address , Name , Phone Mode
- (void)set_merchant_address_mode:(BOOL)address_mode name_mode:(BOOL)name_mode phone_mode:(BOOL)phone_mode
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set "
                           "enable_merchant_address = '%@', "
                           "enable_merchant_name = '%@', "
                           "enable_merchant_phone = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",address_mode ? 1 : 0],
                           [NSString stringWithFormat:@"%d",name_mode ? 1 : 0],
                           [NSString stringWithFormat:@"%d",phone_mode ? 1 : 0],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.enable_merchant_address = address_mode;
        SESSION_DATA.enable_merchant_name = name_mode;
        SESSION_DATA.enable_merchant_phone = phone_mode;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Printer Merchant Address Mode
- (BOOL)get_merchant_address_mode
{
    @try
    {
        NSString *query     = @"SELECT enable_merchant_address FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"enable_merchant_address"] boolValue];
        }
        
        return NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_merchant_address_mode:(BOOL)merchant_address
{
    @try
    {
        int value_to_store = merchant_address ? 1 : 0;
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set enable_merchant_address = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",value_to_store],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.enable_merchant_address = merchant_address;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Printer Merchant Name Mode
- (BOOL)get_merchant_name_mode
{
    @try
    {
        NSString *query;
        query               = @"SELECT enable_merchant_name FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[ @"enable_merchant_name"] boolValue];
        }
        
        return  NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)set_merchant_name_mode:(BOOL)merchant_name
{
    @try
    {
        int value_to_store = merchant_name ? 1 : 0;
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set enable_merchant_name = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",value_to_store],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.enable_merchant_name = merchant_name;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Printer Merchant Phone Mode
- (BOOL)get_merchant_phone_mode
{
    @try
    {
        NSString *query     = @"SELECT enable_merchant_phone FROM SETTING";
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            return [dict[@"enable_merchant_phone"] boolValue];
        }

        return  NO;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (BOOL)set_merchant_phone_mode:(BOOL)merchant_phone
{
    @try
    {
        int value_to_store = merchant_phone ? 1 : 0;
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING set enable_merchant_phone = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@'",
                           [NSString stringWithFormat:@"%d",value_to_store],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        SESSION_DATA.enable_merchant_phone = merchant_phone;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Printer Merchant Additional Data
- (void)get_header_footer
{
    @try
    {
        NSString *query = @"SELECT merchant_name,merchant_address,merchant_phone,footer_message,wifi_password,wifi_name FROM SETTING";
        
        NSArray *resultSet = [self get_from_queue:query];
        
        if(resultSet.count > 0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:resultSet.firstObject forKey:HEADER_FOOTER_DATA];
        }
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)update_header_footer:(NSDictionary *)data
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING SET "
                           "merchant_name      = '%@',"
                           "merchant_address   = '%@',"
                           "merchant_phone     = '%@',"
                           "footer_message     = '%@',"
                           "wifi_name          = '%@',"
                           "wifi_password      = '%@',"
                           "updated_by         = '%@'",
                           data[MERCHANT_NAME],
                           data[MERCHANT_ADDRESS],
                           data[MERCHANT_PHONE],
                           data[FOOTER_MESSAGE],
                           data[WIFI_NAME],
                           data[WIFI_PASS],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID]
                           ];
        [self add_to_queue:query];
        
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:HEADER_FOOTER_DATA];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Hold Order Number & Counter Number
- (void)get_hold_order_number_n_order_count
{
    @try
    {
        int hold_order_number = 1;
        int counter_number    = 1;
        NSString *query       = @"SELECT hold_order_number,counter_number FROM SETTING";
        
        NSArray *resultSet = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            hold_order_number   = [dict[@"hold_order_number"]intValue];
            counter_number      = [dict[@"counter_number"]intValue];
        }
        
        [[NSUserDefaults standardUserDefaults] setInteger:counter_number forKey:ORDER_COUNT];
        [[NSUserDefaults standardUserDefaults] setInteger:hold_order_number forKey:HOLD_ORDER_NO];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (void)increase_hold_order_number
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING SET "
                           "hold_order_number = hold_order_number + 1, "
                           "updated_by    = '%@', "
                           "updated_date  = '%@' ",
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
        
        [self add_to_queue:query];
        
        [[NSUserDefaults standardUserDefaults] setInteger:[[NSUserDefaults standardUserDefaults] integerForKey:HOLD_ORDER_NO] + 1
                                                   forKey:HOLD_ORDER_NO];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (void)increase_order_counter_number
{
    @try
    {
        NSString *main_query = [NSString stringWithFormat:
                                @"UPDATE SETTING SET "
                                "counter_number = counter_number + 1, "
                                "updated_by = '%@', "
                                "updated_date = '%@' ",
                                [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                                CURRENT_TIMESTAMP];
        
        [self add_to_queue:main_query];
        
        [[NSUserDefaults standardUserDefaults] setInteger:[[NSUserDefaults standardUserDefaults] integerForKey:ORDER_COUNT] + 1
                                                   forKey:ORDER_COUNT];
        
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        
    }
}

- (void)reset_order_count_to_one
{
    @try
    {
        NSString *query  = [NSString stringWithFormat:
                            @"UPDATE SETTING SET "
                            "counter_number = 1, "
                            "updated_by = '%@' , "
                            "updated_date = '%@' ",
                            [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                            CURRENT_TIMESTAMP];
        
        [self add_to_queue:query];
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:ORDER_COUNT];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Current balance
- (void)get_current_balance //TODO: remove this method after all user had migrate to 1.6
{
    @try
    {
        double current_balance = 0.0;
        
        NSString *query = @"SELECT SETTING.current_cash_balance FROM SETTING LIMIT 1";
        NSArray *resultSet   = [self get_from_queue:query];
        for(NSDictionary *dict in resultSet)
        {
            current_balance = [dict[@"current_cash_balance"]doubleValue];
        }
        
        [[NSUserDefaults standardUserDefaults] setDouble:current_balance forKey:CURRENT_BALANCE];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)update_current_balance:(double)amount with_plus:(BOOL)plus
{
    @try
    {
        double current_balance = [[NSUserDefaults standardUserDefaults] doubleForKey:CURRENT_BALANCE];
        double new_balance = plus ? (current_balance + amount) : (current_balance - amount);
        
        NSLog(@"new balance: %.2f , current balance: %.2f , amount: %.2f",new_balance,current_balance,amount);
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE SETTING SET current_cash_balance = %.2f",
                           new_balance];
        [self add_to_queue:query];
        
        [[NSUserDefaults standardUserDefaults] setDouble:new_balance forKey:CURRENT_BALANCE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}


@end
