//
//  shareddb+modifier.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+modifier.h"

@implementation shareddb (modifier)

- (NSMutableDictionary *)get_all_modifiers:(NSString *)hold_order_item_id withModItemID:(NSString *)mod_item_id
{
    @try
    {
        NSMutableDictionary *modifiers_array    = [[NSMutableDictionary alloc] init];

        // Choice
        NSString *parent_choice_query = [NSString stringWithFormat:
                                         @"select mod_parent_id,mod_parent_name,mod_type "
                                         "FROM MODIFIERS "
                                         "WHERE mod_item_id = %@ "
                                         "AND active_status = 1 "
                                         "AND mod_type = 'CHOICE' "
                                         "GROUP BY mod_parent_id ", mod_item_id];
        NSMutableArray      *final_choice_array = [[NSMutableArray alloc] init];
        
        NSArray *return_choice_data  = [self get_from_queue:parent_choice_query];
        for (NSDictionary *choice_data in return_choice_data)
        {
            NSMutableString *mod_id = [[NSMutableString alloc] init];
            NSMutableDictionary *choice_parent_modifier = [[NSMutableDictionary alloc] init];
            choice_parent_modifier[@"mod_parent_id"]    = choice_data[@"mod_parent_id"];
            choice_parent_modifier[@"mod_parent_name"]  = choice_data[@"mod_parent_name"];
            choice_parent_modifier[@"mod_type"]         = choice_data[@"mod_type"];
            
            NSString *choice_query = [NSString stringWithFormat:
                                      @"SELECT mod_id,mod_type,mod_name,mod_price,mod_parent_id,mod_parent_name,mod_item_id "
                                      "FROM MODIFIERS "
                                      "WHERE mod_parent_id = %@ "
                                      "AND mod_item_id = %@ "
                                      "AND active_status = 1 ",
                                      choice_data[@"mod_parent_id"], mod_item_id
                                      ];
            NSMutableArray *choice_array = [[NSMutableArray alloc] initWithArray:[self get_from_queue:choice_query]];
            for (NSDictionary *dict in choice_array)
            {
                [mod_id appendFormat:@",%@", dict[@"mod_id"]];
            }
            choice_parent_modifier[@"child_modifier"] = choice_array;
            
            if(mod_id.length > 2)
            {
                NSString *selected_choice_query = [NSString stringWithFormat:
                                                   @"SELECT mod_id, mod_type, mod_name, mod_price, mod_parent_id,"
                                                   "mod_parent_name, mod_item_id, mod_qty, selected_index "
                                                   "FROM HOLD_ORDER_MODIFIERS "
                                                   "WHERE mod_id in(%@) "
                                                   "AND hold_order_item_id = '%@' "
                                                   "AND mod_item_id= '%@' ",
                                                   [mod_id substringFromIndex:1],
                                                   hold_order_item_id,
                                                   mod_item_id
                                                   ];
                NSArray *selected_choice_array = [self get_from_queue:selected_choice_query];
                if (selected_choice_array.count > 0)
                {
                    choice_parent_modifier[@"selected_choice"] = selected_choice_array.firstObject;
                }
                else
                {
                    choice_parent_modifier[@"selected_choice"] = choice_array.firstObject;
                }
            }
            
            [final_choice_array addObject:choice_parent_modifier];
            [mod_id setString:@""];
        }
        
        // Addition
        NSString *addition_query = [NSString stringWithFormat:
                                    @"SELECT mod_id,mod_type,mod_name,mod_price,mod_parent_id,mod_parent_name,mod_item_id,"
                                    "IFNULL((select mod_qty from HOLD_ORDER_MODIFIERS where MODIFIERS.mod_id = HOLD_ORDER_MODIFIERS.mod_id AND HOLD_ORDER_MODIFIERS.hold_order_item_id = '%@'),0) as mod_qty "
                                    "FROM MODIFIERS "
                                    "WHERE mod_item_id = '%@' "
                                    "AND active_status = 1 "
                                    "AND mod_type = 'ADDITION' "
                                    ,hold_order_item_id, mod_item_id];
        NSArray *return_addition_data = [self get_from_queue:addition_query];
        
        modifiers_array[CHOICE]   = final_choice_array;
        modifiers_array[ADDITION] = return_addition_data;
        
        return modifiers_array;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (BOOL)is_modifier_exist:(NSString *)mod_item_id
{
    @try
    {
        BOOL result             = NO;
        NSString *query         = [NSString stringWithFormat:
                                   @"SELECT count(*) as modifier_count FROM MODIFIERS "
                                   "WHERE mod_item_id = %@ "
                                   "AND active_status = 1 "
                                   "AND (mod_type = 'CHOICE' OR mod_type = 'ADDITION')",
                                   mod_item_id];
        NSArray *resultSet         = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            result = ([dict[@"modifier_count"] intValue] > 0);
        }
        
        return result;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
