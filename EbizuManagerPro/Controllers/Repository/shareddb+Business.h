//
//  shareddb+Business.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 10/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (Business)

- (NSString *)get_business_type;

- (BOOL)business_details_exist;
- (BOOL)check_for_username:(NSString *)user n_password:(NSString *)pass;

@end
