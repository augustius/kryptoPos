//
//  shareddb.m
//  Ebizu Manager
//
//  Created by Jamal on 9/9/14.
//  Copyright (c) 2014 EBIZU. All rights reserved.
//
// TODO: P.S. PLEASE CATEGORISE YOUR NEW METHOD(S) UNDER A 'PRAGMA MARK' BASED ON THE TABLE NAME(S)

#import "shareddb.h"

@implementation shareddb

+ (instancetype)sharedShareddb
{
    static shareddb *controller = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
      controller = [[shareddb alloc] init];
    });
    
    return controller;
}

- (NSDecimalNumber *)gst_perc_in_decimal
{
    NSDecimalNumber *one_hundred    = [NSDecimalNumber decimalNumberWithString:@"100.0"];
    NSDecimalNumber *gst_percentage = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",[[NSUserDefaults standardUserDefaults] doubleForKey:GST_PERCENTAGE]]];

    // return 6/100 = 0.06
    return [gst_percentage decimalNumberByDividingBy:one_hundred];
}

- (NSDecimalNumber *)nongst_perc_in_decimal
{
    NSDecimalNumber *one = [NSDecimalNumber decimalNumberWithString:@"1"];
    
    // return 1-0.06 = 0.94
    return [one decimalNumberBySubtracting:[self gst_perc_in_decimal]];
}

#pragma mark - Shared Global Method

#pragma mark - BUSINESS

#pragma mark - USER_LOGIN 

#pragma mark - SETTINGS

#pragma mark - PAYMENT_METHOD

#pragma mark - CASH_REGISTER

#pragma mark - DISCOUNT

#pragma mark - VOUCHERS

#pragma mark - PRINTERS

#pragma mark - CATEGORIES

#pragma mark - ITEMS

#pragma mark - MODIFIERS

#pragma mark - HOLD ORDER

#pragma mark - PURCHASE_ORDER

#pragma mark - TRANSACTIONS_NUMBER

#pragma mark - USERRIGHTS

#pragma mark - MEMBERS

#pragma mark - AREA

#pragma mark - TABLES

#pragma mark - TRANSACTION_MULTIPLE_PAYMENTS

#pragma mark - CUSTOM_INPUT

#pragma mark - EMPLOYEE_TIMECARD

#pragma mark - SHIFT_DETAILS

#pragma mark - TRANSACTIONS

#pragma mark - REFUND

#pragma mark - PENDING_EMAILS

# pragma mark - ERRORS

// TODO: P.S. PLEASE CATEGORISE YOUR NEW METHOD(S) UNDER A 'PRAGMA MARK' BASED ON THE TABLE NAME(S)

@end
