//
//  AbstractRepository.h
//  Ebizu Manager Pro
//
//  Created by Jamal on 6/18/15.
//  Copyright (c) 2015 Ebizu Sdn Bhd. All rights reserved.
//

#import "AbstractRepository.h"
#import "FMDatabaseAdditions.h"
@implementation AbstractRepository

+(NSString *) databaseFilename
{
	return @"posapp_db.sqlite";
}

+(NSString *) databasePath
{
	NSString *filename              = [AbstractRepository databaseFilename];
	NSArray  *paths                 = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory    = paths[0];
	NSString * dbfilePath           = [documentsDirectory stringByAppendingPathComponent:filename];
	NSLog(@"THE DB PATH IS --- %@"  , dbfilePath);
	return dbfilePath;
}

-(instancetype)init
{
	if (self = [super init])
    {
		NSString *dbPath    = [AbstractRepository databasePath];
        queue               = [FMDatabaseQueue databaseQueueWithPath:dbPath];
		
        writeQueue = [NSOperationQueue new];
        [writeQueue setMaxConcurrentOperationCount:1];
        writeQueueLock = [NSRecursiveLock new];
	}
	
	return self;
}

# pragma mark - shared method for shareddb and data_sync
- (id)object:(id)object or:(alternative_type)type
{
    switch (type)
    {
        case object_null:
            return object ?: [NSNull null];
            break;
        case string_null:
            return object ?: @"NULL";
        case empty_string:
            return object ?: @"";
            break;
    }
}

- (NSString *)get_unique_syncid
{
    return [NSString stringWithFormat:@"%@-%@", [[NSUserDefaults standardUserDefaults] objectForKey:UDID],CURRENT_TIMESTAMP];
}

- (NSString *)get_unique_id_from_db
{
    NSString *unique_key;
    NSString *query = @" SELECT hex( randomblob(4)) || '-' || hex( randomblob(2)) "
    "|| '-' || '4' || substr( hex( randomblob(2)), 2) || '-' "
    "|| substr('AB89', 1 + (abs(random()) % 4) , 1)  || "
    "substr(hex(randomblob(2)), 2) || '-' || hex(randomblob(6))  as unique_key";
    
    NSArray *resultSet = [self get_from_queue:query];
    
    if(resultSet.count > 0)
    {
        unique_key  =  resultSet.firstObject[@"unique_key"];
    }
    
    return unique_key;
}

- (BOOL)record_already_exists:(NSString *)table_name with_filter:(NSString *)filter
{
    @try
    {
        NSString *query         = [NSString stringWithFormat:@"SELECT count(*) as id_count FROM %@",table_name];
        
        if (![filter isEqualToString:@""])
        {
            query = [query stringByAppendingString:[NSString stringWithFormat:@" WHERE %@",filter]];
        }
        
        NSArray *resultSet  = [self get_from_queue:query];
    
        return ([resultSet.firstObject[@"id_count"] intValue] > 0);
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

-(void)add_to_queue:(NSString*)sql
{
    NSArray *array_of_sql = [NSArray arrayWithObject:sql];
    
    [self add_to_queue:array_of_sql with_priority:low];
}

-(void)add_to_queue:(NSArray*)array_of_sql with_priority:(queue_priority)priority
{
    if (array_of_sql.count > 0)
    {
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:
       ^{
             @try
             {
                 [writeQueueLock lock];
                 [queue inTransaction:^(FMDatabase *database, BOOL *rollback)
//                 [queue inDatabase:^(FMDatabase *database)
                  {
                      @try
                      {
                          [database logsErrors];
                          
                          NSString *sql = [array_of_sql componentsJoinedByString:@";"];
                          BOOL success = [database executeStatements:sql];
                          if (!success) // if any one statement wrong
                          {
                              NSString *errorMessage = [database lastErrorMessage];
                              NSLog(@"DB ERROR - %s %@",__PRETTY_FUNCTION__,errorMessage);
                              NSLog(@"DB ERROR SQL - %@",sql);
                              if (priority == high)
                              {
                                  [[NSOperationQueue currentQueue] addOperationWithBlock:
                                   ^{
                                      [[NSNotificationCenter defaultCenter] postNotificationName:DATABASE_STATUS object:@[DATABASE_STATUS_FAIL,[NSString stringWithFormat:@"Regret to inform that there is a glitch in the system,\n Please restart the application.\n ERROR MESSAGE : %@.",errorMessage]]];
                                   }];
                              }
                              *rollback = YES;
                              return;
                          }
                        
                          NSLog(@"success commit");
                          
                          if (priority == high)
                          {
                              [[NSOperationQueue currentQueue] addOperationWithBlock:
                               ^{
                                  [[NSNotificationCenter defaultCenter] postNotificationName:DATABASE_STATUS object:@[DATABASE_STATUS_SUCCESS,@"DB SUCCESS"]];
                               }];
                          }
                      }
                      @catch (NSException *exception)
                      {
                          [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@|%@",exception.reason,exception.description]
                                              withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
                          NSLog(@"hit exception");
                      }
                      @finally
                      {
                          NSLog(@"finally done");
                      }
                  }];
             }
             @catch (NSException *exception)
             {
                 [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@|%@",exception.reason,exception.description]
                                     withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
                 //            NSLog(@"Error while inserting data saveLocation inside operation queue. %@", exception.description);
             }
             @finally
             {
                 [writeQueueLock unlock];
             }
         }];
        
        switch (priority)
        {
            case high:
                operation.queuePriority = NSOperationQueuePriorityVeryHigh;
                operation.qualityOfService = NSOperationQualityOfServiceUserInitiated;
                break;
            case medium:
                operation.queuePriority = NSOperationQueuePriorityNormal;
                operation.qualityOfService = NSOperationQualityOfServiceUtility;
                break;
            case low:
                operation.queuePriority = NSOperationQueuePriorityLow;
                operation.qualityOfService = NSOperationQualityOfServiceBackground;
                break;
        }
        
        [writeQueue addOperation:operation];
    }
}


-(NSArray *)get_from_queue:(NSString *)sql
{

    @try
    {
        [writeQueueLock lock];
        [queue inDatabase:^(FMDatabase *database)
         {
             queueResult = [self convert_sql_to_array:[database executeQuery:sql]];
         }];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@|%@",exception.reason,exception.description]
                            withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        //            NSLog(@"Error while inserting data saveLocation inside operation queue. %@", exception.description);
    }
    @finally
    {
        [writeQueueLock unlock];
        return queueResult;
    }
}

- (BOOL)operation_still_running
{
    return (writeQueue.operations.count > 0);
}

- (NSString *)get_query_for_replace:(NSArray *)data into:(NSString *)table_name
{
    NSString *query = @"";
    
    for (NSDictionary *dict_data in data)
    {
        NSArray *arr_column = dict_data.allKeys;
        NSString *column = [arr_column componentsJoinedByString:@", "];
        
        NSArray *arr_value = dict_data.allValues;
        NSString *values = [arr_value componentsJoinedByString:@"','"];
        
        query = [query stringByAppendingString:[NSString stringWithFormat:
                                                @"REPLACE INTO %@ (%@) VALUES ('%@') ; ",
                                                table_name, column, values]];
    }
    
    return query;
}

-(NSMutableArray *)convert_sql_to_array:(FMResultSet *)rs
{
    NSMutableArray* rawData = [[NSMutableArray alloc] init];
    
    while ([rs next])
    {
        NSMutableDictionary *saveDict = [[NSMutableDictionary alloc]init];
        NSArray *allKeys = rs.resultDictionary.allKeys;
        NSArray *allValue = rs.resultDictionary.allValues;
        int x = 0;
        for(NSString *key in allKeys)
        {
            saveDict[key] = [NSString stringWithFormat:@"%@",[allValue[x] isKindOfClass:[NSNull class]] ? @"" : allValue[x]];
            x++;
        }
        
        [rawData addObject:saveDict];
    }
    [rs close];
    
    return rawData;
}



@end
