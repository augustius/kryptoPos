//
//  shareddb+category.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+category.h"

@implementation shareddb (category)

- (NSArray *)get_product_categories
{
    @try
    {
        NSString *query = @"SELECT category_id AS ctg_id, "
        "category_name AS title ,"
        "category_image AS image "
        "FROM CATEGORIES WHERE active_status = 1 ORDER BY sort_index ASC";
        NSArray *resultSet = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    
}

@end
