//
//  data_sync.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/11/2015.
//  Copyright © 2015 Ebizu Sdn Bhd. All rights reserved.
//


#import "AbstractRepository.h"

@interface data_sync : AbstractRepository

+ (instancetype)sharedDataSync;

- (void)sync_reading_response:(NSDictionary *)response;

- (NSDictionary *)get_transactions_for_sync;
- (NSDictionary *)get_transaction_data_to_check_sync_id;
@property (NS_NONATOMIC_IOSONLY, getter=get_pending_emails, readonly, copy) NSArray *_pending_emails;
@property (NS_NONATOMIC_IOSONLY, getter=get_po_receive_for_sync, readonly, copy) NSArray *_po_receive_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_po_receive_item_for_sync, readonly, copy) NSArray *_po_receive_item_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_voucher_redeem_for_sync, readonly, copy) NSArray *_voucher_redeem_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_settings_for_sync, readonly, copy) NSArray *_settings_for_sync;
- (NSArray *)get_cash_register_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_multiple_payments_for_sync, readonly, copy) NSDictionary *_multiple_payments_for_sync;
//- (NSArray *)get_record_using_sync_master:(NSArray *)data;
@property (NS_NONATOMIC_IOSONLY, getter=get_error_data_for_sync, readonly, copy) NSArray *_error_data_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_new_items_for_sync, readonly, copy) NSArray *_new_items_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_new_category_for_sync, readonly, copy) NSArray *_new_category_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_new_table_for_sync, readonly, copy) NSArray *_new_table_for_sync;


    
- (void)add_table:(NSArray *)data;
- (void)add_paymethod:(NSArray *)data;
- (void)add_discount:(NSArray *)data;
- (void)add_voucher:(NSArray*)data;
- (void)add_edit_po_receive:(NSArray *)data;
- (void)add_voucher_bougth_details:(NSArray*)data;
- (void)add_voucher_type:(NSArray*)data;
//- (void)add_edit_transactions:(NSArray *)data;

//- (void)edit_transctions_from_sync:(NSArray *)data;
- (void)edit_sync_error_transaction:(NSArray *)data;
- (void)edit_voucher_redeemed_from_sync:(NSArray *)data;
- (BOOL)edit_settings_sync_details:(NSArray *)data;
- (void)edit_cash_register_from_sync:(NSArray *)data;
- (void)edit_multiple_payment_from_sync:(NSArray *)data;
- (void)edit_refund_from_sync:(NSArray *)data;
- (void)edit_pending_emails:(NSArray *)data;
- (void)update_synced_items:(NSArray *)data;
- (void)update_synced_categories:(NSArray *)data;
- (void)update_new_customers:(NSArray *)data;
- (void)update_new_printers:(NSArray *)data;

#pragma mark - Shift based methods
- (void)edit_emp_timecard_from_sync:(NSArray *)data;
- (void)edit_shift_details_from_sync:(NSArray *)data;



- (void)update_custom_input_from_sync:(NSArray *)data;

//TODO: update new table (JUST ADDED), need to find out what this is related to.

- (void)delete_pending_emails:(NSArray *)data;
@property (NS_NONATOMIC_IOSONLY, readonly) BOOL delete_sync_master_record;


- (void)sync_customer_details:(NSArray *) data;
- (void)sync_menus:(NSArray*)data;
- (void)sync_voucher_bought:(NSArray*)data;
- (void)sync_printers:(NSArray *) data;
- (void)sync_multi_payment:(NSArray *)data;
- (void)sync_cash_register:(NSArray *)data;
- (NSArray *)get_userright_for_sync;

#pragma mark - Recently Created 27 jan 2016

@property (NS_NONATOMIC_IOSONLY, getter=get_customers_data_for_sync, readonly, copy) NSArray *_customers_data_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_hold_order_data_for_sync, readonly, copy) NSArray *_hold_order_data_for_sync;
- (NSArray *)get_hold_order_item_data_for_sync:(NSString *)trx_id;
@property (NS_NONATOMIC_IOSONLY, getter=get_hold_order_number_for_sync, readonly, copy) NSArray *_hold_order_number_for_sync;

#pragma mark - Recently Created and possibly not used

@property (NS_NONATOMIC_IOSONLY, getter=get_custom_field_data_for_sync, readonly, copy) NSArray *_custom_field_data_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_printer_data_for_sync, readonly, copy) NSArray *_printer_data_for_sync;

@property (NS_NONATOMIC_IOSONLY, getter=get_shift_details_for_sync, readonly, copy) NSArray *_shift_details_for_sync;
@property (NS_NONATOMIC_IOSONLY, getter=get_employee_timecard_for_sync, readonly, copy) NSArray *_employee_timecard_for_sync;

- (void)update_user_right_details:(NSArray *)data;
- (NSDictionary *)get_all_unsync_data;
- (void)set_all_unsync_data;
- (void)update_sync_response:(NSDictionary *)response;

#pragma mark - BUSINESS
- (void)insert_business_details:(NSDictionary *)business withToken:(NSString *)token;

#pragma mark - SETTING
- (void)insert_hardware_code:(NSString *)hardware_code and_taxes:(NSString *)service_charge;
- (void)insert_settings:(NSArray *)setting;

#pragma mark - TRANSACTION_NO
- (void)insert_transaction_number:(NSString *)trans_no;

#pragma mark - USERRIGHTS
- (void)sync_user_rights_details:(NSArray *) data;

#pragma mark - SHIFT_DETAIL
- (void)sync_shift_details:(NSArray *)data;

#pragma mark - EMPLOYEE_TIMECARD
- (void)sync_employee_time_card_details:(NSArray *)data;

#pragma mark - REFUND
- (NSDictionary *)get_refund_for_sync;

#pragma mark - CUSTOM INPUT
- (void)sync_modules_details:(NSArray *) data;
- (void)set_custom_field_name:(NSArray *)data;
    
#pragma mark - AREA
- (void)add_area:(NSArray *)data;
- (NSArray *)getAreaForSync;
- (void)updateAreaSyncId:(NSArray *)data;
    
#pragma mark - TABLE
- (void)update_new_tables:(NSArray *)data;


@end
