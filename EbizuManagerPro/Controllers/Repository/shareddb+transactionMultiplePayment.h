//
//  shareddb+transactionMultiplePayment.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 31/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (transactionMultiplePayment)

- (void)insert_multiple_payment_details:(NSArray *) data;

@end
