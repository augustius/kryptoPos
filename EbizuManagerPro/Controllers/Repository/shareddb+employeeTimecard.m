//
//  shareddb+employeeTimecard.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+employeeTimecard.h"
#import "KryptoPOS-Swift.h"

@implementation shareddb (employeeTimecard)

- (BOOL)employee_clocked_in
{
    BOOL result = NO;
    NSString *clock_in_time  = @"";
    NSString *clock_out_time = @"";
    
    NSString *query = [NSString stringWithFormat:
                       @"SELECT clock_in_time , clock_out_time , MAX(created_date) AS latest_date "
                       "FROM EMPLOYEE_TIMECARD "
                       "WHERE user_id = '%@' ",
                       [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID]];
    NSArray *resultSet = [self get_from_queue:query];
    
    for(NSDictionary *dict in resultSet)
    {
        clock_in_time           = dict[@"clock_in_time"];
        clock_out_time          = dict[@"clock_out_time"];
    }
    
    if(![clock_in_time isEqualToString:@""])
    {
        if ([clock_out_time isEqualToString:@"0"])
        {
            result = YES;
        }
    }
    
    return result;
}

- (void)insert_new_employee_timecard
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"INSERT INTO EMPLOYEE_TIMECARD "
                           "(id,user_id,clock_in_time,company_id,created_by) "
                           "VALUES ('%@','%@','%@', %@ ,'%@')",
                           [self get_unique_syncid],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP,
                           [StringUtils get_business_detail:@"business_id"],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID]];

        [self add_to_queue:query];
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
    
}

- (void)update_employee_timecard
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE EMPLOYEE_TIMECARD set "
                           "clock_out_time = '%@',"
                           "updated_by = '%@',"
                           "updated_date = '%@',"
                           "sync_id = null "
                           "WHERE created_date = "
                           "(SELECT MAX(created_date) FROM EMPLOYEE_TIMECARD WHERE user_id = '%@')",
                           CURRENT_TIMESTAMP,
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP,
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID]];
        
        [self add_to_queue:query];
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray *)get_employee_clock_in_out_record:(NSDate *)shift_start to:(NSDate *)shift_end user_id:(NSString *)user_id
{
    @try
    {
        NSString *userIdStatement = [user_id isEqualToString:@""] ? @"" : [NSString stringWithFormat: @"AND EMPLOYEE_TIMECARD.user_id = '%@'",user_id];
        
        NSString *query = [NSString stringWithFormat:
                           @"SELECT EMPLOYEE_TIMECARD.user_id, "
                           "EMPLOYEE_TIMECARD.clock_in_time, "
                           "CASE WHEN EMPLOYEE_TIMECARD.clock_out_time = '0' "
                           "THEN 'Active' "
                           "ELSE EMPLOYEE_TIMECARD.clock_out_time END AS clock_out_time , "
                           "USERRIGHTS.name "
                           "FROM EMPLOYEE_TIMECARD INNER JOIN USERRIGHTS ON EMPLOYEE_TIMECARD.user_id = USERRIGHTS.user_id "
                           "WHERE EMPLOYEE_TIMECARD.company_id = '%@' "
                           "AND strftime(datetime(EMPLOYEE_TIMECARD.created_date,'unixepoch','localtime')) between '%@' AND '%@' %@ "
                           "ORDER BY EMPLOYEE_TIMECARD.created_date desc",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,shift_end.sql_string,
                           userIdStatement];
        
        NSLog(@"%@",query);
        
        NSArray *resultSet  = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end










