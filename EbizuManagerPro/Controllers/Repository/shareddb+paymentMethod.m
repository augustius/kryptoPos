//
//  shareddb+paymentMethod.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+paymentMethod.h"

@implementation shareddb (paymentMethod)

- (void)get_all_paymethod
{
    @try
    {
        //type_id- 99 is Multiple Payment
        NSString *query = @"SELECT id,pay_name,action,type_id,type_name,gateway_id,gateway_name "
                           "FROM PAYMENT_METHOD ORDER BY type_id ASC";
        
        NSMutableDictionary *pay_methods;
        NSMutableArray      *pay_array  = [[NSMutableArray alloc] init];
        NSArray             *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            pay_methods = [[NSMutableDictionary alloc] init];
            pay_methods[@"id"]          = dict[@"id"];
            pay_methods[@"pay_name"]    = dict[@"pay_name"];
            pay_methods[@"action"]      = dict[@"action"];
            pay_methods[@"type_id"]     = dict[@"type_id"];
            pay_methods[@"type_name"]   = dict[@"type_name"];
            
            pay_methods[@"gateway_id"]   = dict[@"gateway_id"];
            pay_methods[@"gateway_name"] = dict[@"gateway_name"];
            
            pay_methods[@"paid_amount"]     = @"0.00";  // added for multi payment
            pay_methods[@"credit_card_no"]  = @"";      // added for multi payment
            pay_methods[@"total_amount"]    = @"0.00";  // added for multi payment
            
            [pay_array addObject:pay_methods];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:pay_array forKey:ALL_PAYMENT_METHOD];
        
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

// FIXME: need to sort out the payment_method_id (not unique issue)
- (NSArray *)get_total_by_payment_type:(NSString *)start_date witEndDate:(NSString *)end_date //created to get all payment type in report chart and its total amount
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT pay_name,"
                           "("
                           " SELECT IFNULL(ROUND(SUM(TRANSACTIONS.total)  - "
                           " (SELECT IFNULL(SUM(REFUND.refund_amount),0) FROM REFUND WHERE REFUND.trx_id = TRANSACTIONS.trx_id) ,2),0) "
                           " FROM TRANSACTIONS "
                           " WHERE PAYMENT_METHOD.type_id = TRANSACTIONS.payment "
                           " AND TRANSACTIONS.company_id='%@' AND datetime(TRANSACTIONS.create_time, 'unixepoch','localtime') between '%@' AND '%@' "
                           ") as total "
                           "FROM PAYMENT_METHOD "
                           "UNION "
                           "SELECT 'Others', "
                           "( "
                           " SELECT IFNULL(ROUND(SUM(TRANSACTIONS.total)  - "
                           " (SELECT IFNULL(SUM(REFUND.refund_amount),0) FROM REFUND WHERE REFUND.trx_id = TRANSACTIONS.trx_id) ,2),0) "
                           " FROM TRANSACTIONS "
                           " WHERE TRANSACTIONS.payment = 0 "
                           " AND TRANSACTIONS.company_id='%@' AND datetime(TRANSACTIONS.create_time, 'unixepoch','localtime') between '%@' AND '%@' "
                           " ) as total "
                           "FROM PAYMENT_METHOD ",
                           [StringUtils get_business_detail:@"business_id"],start_date,end_date ,
                           [StringUtils get_business_detail:@"business_id"],start_date,end_date];
        
        NSArray *payment_type_data = [self get_from_queue:query];
        return payment_type_data;
        
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
    
}


@end
