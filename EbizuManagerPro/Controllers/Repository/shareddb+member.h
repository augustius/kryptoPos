//
//  shareddb+member.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"
#import "Customer.h"

@interface shareddb (member)

- (NSArray *)get_all_cust;
- (Customer *)insert_new_customer_details:(Customer *)cust;
- (void)updateCutomerDetail:(Customer *)cust;


@end
