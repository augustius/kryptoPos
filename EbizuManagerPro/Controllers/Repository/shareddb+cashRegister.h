//
//  shareddb+cashRegister.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (cashRegister)

- (void)add_cash_in:(NSArray *)data;

#pragma mark - Method use for Cash Register Report
- (NSArray *)get_cash_register_history_from:(NSDate *)startDate to:(NSDate *)endDate;

#pragma mark - Method use for 'Daily Sales Report Print Out'
- (NSString *)get_cash_in_today;
- (NSString *)get_cash_in_date:(NSDate *)date;
- (NSString *)get_cash_in_from:(NSDate *)shift_start to:(NSDate *)shift_end;

- (NSString *)get_cash_out_today;
- (NSString *)get_cash_out_date:(NSDate *)date;
- (NSString *)get_cash_out_from:(NSDate *)shift_start to:(NSDate *)shift_end;


@end
