//
//  Hubdb.m
//  EbizuManagerPro
//
//  Created by Vinoth on 3/2/16.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import "Hubdb.h"

@implementation Hubdb


- (NSString *)get_query_by_function_name:(NSString*)function_name;
{
    NSString *query = @"";

    if([function_name isEqualToString:@"GET_CASH_REGISTER"])  // Get cash register balance
    {
        query = [NSString stringWithFormat:@"SELECT IFNULL(sum(amount),0) + "
                           "IFNULL((SELECT sum(total) FROM TRANSACTIONS WHERE TRANSACTIONS.company_id='%@' AND TRANSACTIONS.payment = 1),0) as total_cash "
                           "FROM CASH_REGISTER "
                           "WHERE CASH_REGISTER.cash_type = 0 AND CASH_REGISTER.company_id='%@'",
                           [StringUtils get_business_detail:@"business_id"],
                           [StringUtils get_business_detail:@"business_id"]];
    }
    
    return query;
}
@end
