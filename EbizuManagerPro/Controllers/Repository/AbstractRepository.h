//
//  AbstractRepository.h
//  Ebizu Manager Pro
//
//  Created by Jamal on 6/18/15.
//  Copyright (c) 2015 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@import FMDB;

@interface AbstractRepository : NSObject
{
    FMDatabaseQueue *queue;
    NSOperationQueue *writeQueue;
    NSRecursiveLock *writeQueueLock;
    NSArray *queueResult;
}

typedef NS_ENUM(NSInteger, queue_priority)
{
    high = 0,
    medium,
    low
};

typedef NS_ENUM(NSInteger, alternative_type)
{
    object_null = 0,
    string_null,
    empty_string
};


+ (NSString *)   databaseFilename;
+ (NSString *)   databasePath;
- (id)           object:(id)object or:(alternative_type)type;
- (NSString *)   get_unique_syncid;
- (NSString *)   get_unique_id_from_db;
- (BOOL)         record_already_exists:(NSString *)table_name with_filter:(NSString *)filter;
- (void)         add_to_queue:(NSString*)sql;
- (void)         add_to_queue:(NSArray*)array_of_sql with_priority:(queue_priority)priority;
- (NSArray *)    get_from_queue:(NSString *)sql;
- (BOOL)         operation_still_running;
- (NSString *)   get_query_for_replace:(NSArray *)data into:(NSString *)table_name;
- (NSMutableArray *)convert_sql_to_array:(FMResultSet *)rs;

@end
