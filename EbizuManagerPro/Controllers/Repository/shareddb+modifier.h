//
//  shareddb+modifier.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (modifier)

- (NSMutableDictionary *)get_all_modifiers:(NSString *)hold_order_item_id withModItemID:(NSString *)mod_item_id;
- (BOOL)is_modifier_exist:(NSString *)mod_item_id;

@end
