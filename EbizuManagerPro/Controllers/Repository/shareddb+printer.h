//
//  shareddb+printer.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (printer)

- (NSArray *)get_list_of_printers;
- (NSString *)get_categories_for_printer:(NSString *)printer_name;
- (NSString *)addPrinter:(NSDictionary *)data;
- (void)add_printer:(NSArray *)data;
- (void)editPrinter:(NSDictionary *)data;
- (void)edit_printer_print_copies:(NSString *)printer_id withPrintCopies:(NSString *)print_copies;
- (void)edit_printer_default:(NSArray *)printer_data;
- (void)delete_printer_from_pos:(NSString *)printer_id;
- (NSArray *)get_all_default_printers_from_bill_only:(BOOL)bill;
- (void)update_printer_category_id:(NSString *)category_id printer_name:(NSString *)printer_name;





@end
