//
//  shareddb+member.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+member.h"

@implementation shareddb (member)

- (NSArray *)get_all_cust
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM MEMBERS WHERE active_status = '1' ORDER BY cust_firstName"];
        NSMutableArray *customer_arry   = [[NSMutableArray alloc] init];
        
        Customer *cust      = [[Customer alloc] init];      // add dummy record to show Walk in customer
        cust.user_id        = @"0";
        cust.first_name     = @"Walk In";
        cust.full_name      = @"Walk In";
        cust.last_name      = @"";
        cust.profilePic     = @"icon_walkin.png";
        cust.customer_id    = @"0";
        cust.checkin_id     = @"";
        cust.member_id      = @"0";
        cust.level          = @"";
        cust.lastVisit      = @"";
        cust.manisPts       = 0;
        cust.totalSpent     = 0;
        cust.email          = @"";
        cust.mobile         = @"";
        cust.mostPurchasedCategory      = @"";
        cust.mostPurchasedCategoryItem  = @"";
        
        [customer_arry addObject :cust];
        
        NSArray *customer_data = [self get_from_queue:query];
        
        for (NSDictionary *customer_list in customer_data)
        {
            Customer *cust      = [[Customer alloc] init];
            cust.customer_id    = customer_list[@"cust_id"];
            cust.first_name     = customer_list[@"cust_firstName"];
            cust.user_id        = customer_list[@"user_id"];
            cust.last_name      = customer_list[@"cust_lastName"];
            cust.full_name      = [NSString stringWithFormat:@"%@ %@",cust.first_name,cust.last_name];
            cust.profilePic     = customer_list[@"cust_image"];
            cust.checkin_id     = customer_list[@"cust_checkin_id"];
            cust.member_id      = customer_list[@"cust_member_id"];
            cust.level          = customer_list[@"cust_level"];
            cust.totalSpent     = customer_list[@"cust_total_spent"];
            cust.email          = customer_list[@"cust_email"];
            cust.mobile         = customer_list[@"cust_mobile"];
            cust.mostPurchasedCategory      = customer_list[@"cust_fav_cat"];
            cust.mostPurchasedCategoryItem  = customer_list[@"cust_fav_item"];
            cust.lastVisit      = customer_list[@"cust_last_visit"];
            cust.manisPts       = customer_list[@"cust_manis_pts"];
            cust.address        = customer_list[@"cust_address"];
            cust.city           = customer_list[@"cust_city"];
            cust.postCode       = customer_list[@"cust_postcode"];
            cust.birthDate      = customer_list[@"cust_birthdate"];
            cust.gender         = customer_list[@"cust_gender"];
            [customer_arry addObject:cust];
        }
        
        return customer_arry;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (Customer *)insert_new_customer_details:(Customer *)cust
{
    @try
    {
        cust.user_id        = [NSString stringWithFormat:@"%@%@",CURRENT_TIMESTAMP,[StringUtils get_business_detail:@"business_id"]];
        cust.customer_id    = [self get_unique_id_from_db];
        cust.profilePic     = cust.justAddedProfilePicUIImage ? [NSString stringWithFormat:@"%@_customerImage.jpg",cust.user_id] : CUSTOMER_DEFAULT_PIC;
        
        NSString *query = [NSString stringWithFormat:@"INSERT INTO MEMBERS "
                           "(user_id,"
                           "cust_id,"
                           "cust_firstName,"
                           "cust_lastName,"
                           "cust_image,"
                           "cust_checkin_id,"
                           "cust_member_id,"
                           "cust_level,"
                           "cust_last_visit,"
                           "cust_manis_pts,"
                           "cust_fav_cat,"
                           "cust_fav_item,"
                           "cust_total_spent,"
                           "cust_email,"
                           "cust_mobile,"
                           "cust_address,"
                           "cust_birthdate,"
                           "cust_gender,"
                           "cust_city,"
                           "cust_postcode,"
                           "company_id,"
                           "created_by,"
                           "cust_image_changed) "
                           "VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@', '%@','%@','%@','%@','%@','1') "
                           ,cust.user_id, //TODO: need to change to VARCHAR, for now use timestamp
                           [self object:cust.customer_id or:empty_string],
                           cust.first_name,
                           cust.last_name,
                           cust.profilePic,
                           [self object:cust.checkin_id or:empty_string],
                           [self object:cust.member_id or:empty_string],
                           [self object:cust.level or:empty_string],
                           [self object:cust.lastVisit or:empty_string],
                           [self object:cust.manisPts or:empty_string],
                           [self object:cust.mostPurchasedCategory or:empty_string],
                           [self object:cust.mostPurchasedCategoryItem or:empty_string],
                           [self object:cust.totalSpent or:empty_string],
                           cust.email,
                           cust.mobile,
                           cust.address,
                           [self object:cust.birthDate or:empty_string],
                           [self object:cust.gender or:empty_string],
                           cust.city,
                           cust.postCode,
                           [StringUtils get_business_detail:@"business_id"],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID]];
        
        [self add_to_queue:query];
        
        NSData *image_data = UIImagePNGRepresentation(cust.justAddedProfilePicUIImage);
        [FileUtils writeToDocuemntsDirectoryAtPath: [NSString stringWithFormat:@"%@/%@",POSImages,IMG_CUSTOMERS]
                                          fileData: image_data withPrefix:[NSString stringWithFormat:@"%@_customerImage",cust.user_id]
                                      andExtension: @"jpg"];
        
        return cust;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)updateCutomerDetail:(Customer *)cust
{
    @try
    {
        BOOL image_changed = NO;
        
        if (cust.justAddedProfilePicUIImage)
        {
            UIImage *defaultPic = [UIImage imageNamed:CUSTOMER_DEFAULT_PIC];
            if ([cust.justAddedProfilePicUIImage isEqual:defaultPic])
            {
                cust.profilePic = CUSTOMER_DEFAULT_PIC;
            }
            else
            {
                cust.profilePic = [NSString stringWithFormat:@"%@_customerImage.jpg",cust.user_id];
                NSData *image_data = UIImagePNGRepresentation(cust.justAddedProfilePicUIImage);
                [FileUtils writeToDocuemntsDirectoryAtPath: [NSString stringWithFormat:@"%@/%@",POSImages,IMG_CUSTOMERS]
                                                  fileData: image_data withPrefix:[NSString stringWithFormat:@"%@_customerImage",cust.user_id]
                                              andExtension: @"jpg"];
            }

            image_changed = YES;
        }
        
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE MEMBERS set "
                           "cust_firstName = '%@', "
                           "cust_lastName = '%@', "
                           "cust_image = '%@', "
                           "cust_email = '%@', "
                           "cust_mobile = '%@', "
                           "cust_address = '%@', "
                           "cust_birthdate = '%@', "
                           "cust_gender = '%@', "
                           "cust_city = '%@', "
                           "cust_postcode = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@', "
                           "sync_id = null, "
                           "cust_image_changed = '%@' "
                           "WHERE user_id = '%@'",
                           cust.first_name,
                           cust.last_name,
                           cust.profilePic,
                           cust.email,
                           cust.mobile,
                           cust.address,
                           [self object:cust.birthDate or:empty_string],
                           [self object:cust.gender or:empty_string],
                           cust.city,
                           cust.postCode,
                           [StringUtils get_business_detail:@"business_id"],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           image_changed ? @"1" : @"0",
                           cust.user_id];
        
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
