//
//  shareddb+discount.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (discount)

- (NSArray *)get_predefined_discount;
- (NSString *)get_predefined_discount_by_id:(NSString *)discount_id;

@end
