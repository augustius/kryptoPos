//
//  shareddb+table.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (table)

- (NSArray *)getAllTablesWithAreaId:(NSString *)areaId;
- (void)updateTableDetails:(NSArray *)data;
- (void)updateTableOrderId:(NSString *)orderId fromTableId:(NSString *)previousTableId intoTableId:(NSString *)newTableId;
- (void)update_table_amount_orderid:(NSDictionary *)data;
- (void)remove_merge_into_column_with_id:(NSString *)table_id;
- (void)deleteTableWithId:(NSString *)tableId;
- (NSString *)addNewTable:(NSDictionary *)dict;
- (void)merge_into:(NSString *)merge_table_id from:(NSString *)table_id;
- (NSArray *)getAllTableNameThatMergeInto:(NSString *)merge_id;
- (NSString *)getTableIdWithOrderId:(NSString *)OrderId;

@end
