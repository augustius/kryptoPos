//
//  shareddb+area.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+area.h"

@implementation shareddb (area)

- (NSArray *)get_list_of_areas
{
    @try
    {
        NSString *query     = @"SELECT area_name,id,tba_sequence_no FROM AREA WHERE active_status = 1 ORDER BY tba_sequence_no DESC";
        NSArray *resultSet  = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}
    
- (void)updateArea:(NSArray *)data
{
    @try
    {
        NSString *query = @"";
        NSMutableArray *arrayOfQuery = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dict in data)
        {
            query = [NSString stringWithFormat:
                     @"UPDATE AREA SET "
                     "tba_sequence_no = %@, "
                     "sync_id = null "
                     "WHERE id = '%@'"
                     , dict[@"tba_sequence_no"]
                     , dict[@"id"]] ;
            [arrayOfQuery addObject:query];
        }
        
        [self add_to_queue:arrayOfQuery with_priority:low];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
    

}

@end
