//
//  shareddb+item.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+item.h"

@implementation shareddb (item)

- (NSString *)get_item_image_name:(NSString *)item_id
{
    @try
    {
        NSString *image_name = @"";
        NSString *query      = [NSString stringWithFormat:@"SELECT item_image FROM ITEMS where item_id = '%@'",item_id];
        NSArray *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            image_name = dict[ @"item_image"];
        }
        return image_name;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (NSArray *)get_product_categories_items:(NSString *)input with:(item_search_method)method
{
    @try
    {
        NSString *main_query = @"SELECT "
        "item_cat_id,item_id,item_name,item_image,price,cost_price,selling_price,"
        "description,online_order,timestamp,gst_rate,gst_code,stock_monitoring,stock_left,"
        "type_of_price,type_of_unit FROM ITEMS ";
        
        NSString *query = @"";
        
        switch (method)
        {
            case barcode:
                query = [NSString stringWithFormat:
                         @"%@ INNER JOIN CATEGORIES ON CATEGORIES.category_id = ITEMS.item_cat_id "
                         "WHERE ITEMS.barcode = '%@' AND ITEMS.active_status=1"
                         ,main_query,input];
                break;
            case cat_id:
                query = [NSString stringWithFormat:@"%@ WHERE item_cat_id = %@ AND active_status = 1 order by sort_index"
                         ,main_query,input];
                break;
            case item_name:
                query = [NSString stringWithFormat:
                         @"%@ INNER JOIN CATEGORIES ON CATEGORIES.category_id = ITEMS.item_cat_id "
                         "WHERE ITEMS.item_name like '%@%@%@' AND ITEMS.active_status=1"
                         ,main_query,@"%",input,@"%"];
                break;
        }
        
        NSArray *resultSet = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (void)update_stock_quantity:(NSArray*)item_details
{
    @try
    {
        NSString *stock_left = @"", *updated_by = @"", *updated_date = @"", *item_id = @"";
        
        for(int i = 0; i <  item_details.count; i++)
        {
            stock_left     = [stock_left  stringByAppendingString  : [NSString stringWithFormat:@" when item_id = '%@' then '%@'",[item_details valueForKey:@"item_id"][i],[item_details valueForKey:@"stock_left"][i]]];
            updated_by     = [updated_by  stringByAppendingString  : [NSString stringWithFormat:@" when item_id = '%@' then '%@'",[item_details valueForKey:@"item_id"][i],[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID]]];
            updated_date   = [updated_date  stringByAppendingString  : [NSString stringWithFormat:@" when item_id = '%@' then '%@'",[item_details valueForKey:@"item_id"][i],CURRENT_TIMESTAMP]];
            item_id        = [item_id           stringByAppendingString  :  [NSString stringWithFormat:@"'%@',",[item_details valueForKey:@"item_id"][i]]];
        }
        
        NSString *query = [NSString stringWithFormat:@"UPDATE ITEMS SET "
                           "stock_left      = (case%@ end), "
                           "updated_by      = (case%@ end), "
                           "updated_date    = (case%@ end) "
                           "WHERE item_id in (%@)",
                           stock_left,updated_by,updated_date,[StringUtils remove_last_char:item_id]];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (int)get_stock_left:(NSString*)item_id
{
    @try
    {
        double stock_left;
        NSString *query     = [NSString stringWithFormat:@"SELECT stock_left FROM ITEMS where item_id = '%@'",item_id];
        NSArray *resultSet  = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            stock_left = [dict[@"stock_left"]doubleValue];
        }
        return stock_left;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}


@end
