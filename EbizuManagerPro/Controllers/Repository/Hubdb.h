//
//  Hubdb.h
//  EbizuManagerPro
//
//  Created by Vinoth on 3/2/16.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hubdb : NSObject

- (NSString *)get_query_by_function_name:(NSString*)function_name;

@end
