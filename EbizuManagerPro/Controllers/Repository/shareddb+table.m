//
//  shareddb+table.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+table.h"

@implementation shareddb (table)
- (NSArray *)getAllTablesWithAreaId:(NSString *)areaId
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT TABLES.id,"
                           "TABLES.table_no,"
                           "TABLES.area_id,"
                           "TABLES.table_area,"
                           "TABLES.tbl_sequence_no,"
                           "TABLES.merged_into,"
                           "HOLD_ORDER.no_of_pax,"
                           "HOLD_ORDER.trx_id as order_id,"
                           "HOLD_ORDER.total as total_amount,"
                           "HOLD_ORDER.voucher_amount as voucher_amount,"
                           "HOLD_ORDER.discount_amt as discount_amount "
                           "FROM TABLES LEFT JOIN HOLD_ORDER on TABLES.order_id = HOLD_ORDER.trx_id "
                           "WHERE TABLES.area_id = '%@' "
                           "AND TABLES.active_status = 1 "
                           "ORDER BY tbl_sequence_no DESC",areaId];
        
        NSMutableDictionary *tables_dict;
        NSMutableArray      *tables_array = [[NSMutableArray alloc] init];
        
        NSArray *resultSet = [self get_from_queue:query];
        
        for (NSDictionary *transaction_data in resultSet)
        {
            tables_dict = [[NSMutableDictionary alloc] init];
            tables_dict[@"table_id"]    = transaction_data[@"id"];
            tables_dict[@"table_no"]    = transaction_data[@"table_no"];
            tables_dict[@"table_area_id"] = transaction_data[@"area_id"];
            tables_dict[@"table_area"]  = transaction_data[@"table_area"];
            tables_dict[@"tbl_sequence_no"] = transaction_data[@"tbl_sequence_no"];
            tables_dict[@"merged_into"] = [self object:transaction_data[@"merged_into"] or:empty_string];
            tables_dict[@"no_of_pax"]   = [[self object:transaction_data[@"no_of_pax"] or:empty_string] isEqualToString:@""] ? @"0" : transaction_data[@"no_of_pax"];
            tables_dict[@"order_id"]    = [self object:transaction_data[@"order_id"] or:empty_string];
            tables_dict[@"total_amount"] = [self object:transaction_data[@"total_amount"] or:empty_string];
            tables_dict[@"voucher_amount"] = [self object:transaction_data[@"voucher_amount"] or:empty_string];
            tables_dict[@"discount_amount"] = [self object:transaction_data[@"discount_amount"] or:empty_string];
            
            [tables_array addObject:tables_dict];
        }
        return tables_array;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}
    
- (void)updateTableDetails:(NSArray *)data
    {
        @try
        {
            NSString *query = @"";
            NSMutableArray *arrayQuery = [[NSMutableArray alloc] init];
            for (NSDictionary *dict in data)
            {
                query = [NSString stringWithFormat:
                         @"UPDATE TABLES SET "
                         "table_no = '%@',"
                         "area_id = '%@',"
                         "table_area = '%@',"
                         "tbl_sequence_no = '%@',"
                         "merged_into = '%@',"
                         "order_id = '%@', "
                         "updated_by = '%@',"
                         "updated_date = '%@',"
                         "sync_id = null "
                         "WHERE id = '%@'",
                         dict[@"table_no"],
                         dict[@"table_area_id"],
                         dict[@"table_area"],
                         dict[@"tbl_sequence_no"],
                         dict[@"merged_into"],
                         dict[@"order_id"],
                         [self object:[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID] or:empty_string],
                         [self object:CURRENT_TIMESTAMP or:empty_string],
                         dict[@"table_id"]
                       ];
                [arrayQuery addObject:query];
            }
            
            [self add_to_queue:arrayQuery with_priority:low];
        }
        @catch (NSException *exception) 
        {
            [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
            NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
        }
    }

- (void)updateTableOrderId:(NSString *)orderId fromTableId:(NSString *)previousTableId intoTableId:(NSString *)newTableId
{
    @try
    {
        NSString *query = @"";
        NSMutableArray *arrayQuery = [[NSMutableArray alloc] init];
        
        if (![previousTableId isEqualToString:@""])
        {
            // move all merged table into new selected table
            query = [NSString stringWithFormat:
                     @"UPDATE TABLES SET "
                     "merged_into = '%@' "
                     "WHERE merged_into = '%@'",newTableId,previousTableId];
            [arrayQuery addObject:query];
            
            // remove previous selected table order id value
            query = [NSString stringWithFormat:
                     @"UPDATE TABLES SET "
                     "total_amount = '', "
                     "order_id = '' "
                     "WHERE order_id = '%@'",orderId];
            [arrayQuery addObject:query];
        }

        // update order id value into new selected table
        query = [NSString stringWithFormat:
                 @"UPDATE TABLES SET "
                 "order_id = '%@' "
                 "WHERE id = '%@'",orderId,newTableId];
        [arrayQuery addObject:query];
        
        [self add_to_queue:arrayQuery with_priority:low];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)remove_previous_selected_table:(NSString *)data
{
    NSString *empty = @"";
    
    NSString *query         = [NSString stringWithFormat:@"UPDATE TABLES SET"
                               " total_amount       = '%@',"
                               " order_id           = '%@'"
                               " WHERE order_id     = '%@'",empty,empty,data];
    
    [self add_to_queue:query];
}

- (void)update_table_amount_orderid:(NSDictionary *)data
{
    @try
    {
        [self remove_previous_selected_table:[data valueForKey:@"order_id"]];
        
        NSString *query = [NSString stringWithFormat:@"UPDATE TABLES SET"
                           " total_amount      = '%@',"
                           " order_id          = '%@'"
                           " WHERE table_no    = '%@'",
                           [self object:[data valueForKey:@"total_amount"] or:empty_string],
                           [self object:[data valueForKey:@"order_id"] or:empty_string],
                           [data valueForKey:@"table_no"]
                           ];
        
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)remove_merge_into_column_with_id:(NSString *)table_id
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE TABLES SET "
                           "merged_into        = null "
                           "WHERE merged_into  = '%@' ",
                           table_id
                           ];
    
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)deleteTableWithId:(NSString *)tableId
{
    @try
    {
        NSString *query  = [NSString stringWithFormat:
                            @"UPDATE TABLES SET "
                            "active_status = 0, "
                            "sync_id = null "
                            "WHERE id = '%@'",tableId];
        
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSString *)addNewTable:(NSDictionary *)dict
{
    @try
    {
        NSString *table_id = [self get_unique_id_from_db];
        
        if ([self record_already_exists:@"TABLES"
                            with_filter: [NSString stringWithFormat:@" table_no = '%@' AND active_status = 1",[dict valueForKey:@"table_no"]]])
        {
            table_id = @"";
        }
        else
        {
            NSString *query = [NSString stringWithFormat:
                               @"INSERT INTO TABLES"
                               "(id,"
                               "table_no,"
                               "area_id,"
                               "table_area,"
                               "tbl_sequence_no,"
                               "merged_into,"
                               "order_id,"
                               "created_by,"
                               "created_date,"
                               "total_chair,"
                               "table_shape) "
                               @"VALUES('%@','%@','%@','%@','%@','%@','%@','%@','%@','1','Square')",
                               table_id,
                               dict[@"table_no"],
                               dict[@"table_area_id"],
                               dict[@"table_area"],
                               dict[@"tbl_sequence_no"],
                               dict[@"merged_into"],
                               dict[@"order_id"],
                               [self object:[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID] or:empty_string],
                               [self object:CURRENT_TIMESTAMP or:empty_string]
                               ];
            [self add_to_queue:query];
        }
        
        return table_id;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)merge_into:(NSString *)merge_table_id from:(NSString *)table_id
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE TABLES SET "
                           "order_id = '', "
                           "total_amount = '', "
                           "merged_into = '%@' "
                           "WHERE id    = '%@' ",
                           merge_table_id,
                           table_id ];
        
        [self add_to_queue:query];
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray *)getAllTableNameThatMergeInto:(NSString *)merge_id
{
    @try
    {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        NSString *query = [NSString stringWithFormat:@" SELECT table_no FROM TABLES WHERE merged_into = '%@' ",merge_id];
        
        NSArray         *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            [result addObject:dict[@"table_no"]];
        }
        
        return result;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSString *)getTableIdWithOrderId:(NSString *)OrderId
{
    @try
    {
        NSString *result = @"";
        NSString *query = [NSString stringWithFormat:@"SELECT id FROM TABLES WHERE order_id = '%@'",OrderId];
        
        NSArray *resultSet   = [self get_from_queue:query];
        for(NSDictionary *dict in resultSet)
        {
            result = dict[@"id"];
        }
        
        return result;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
