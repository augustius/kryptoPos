//
//  shareddb+holdOrder.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+holdOrder.h"
#import "shareddb+modifier.h"

@implementation shareddb (holdOrder)

- (CompleteOrder *)get_hold_order_with_id:(NSString *)hold_order_id andSplitBill:(BOOL)is_split_bill
{
    @try
    {
        NSString *split_filter   = @"";
        NSString *hold_item_query;
        NSString *hold_query = [NSString stringWithFormat:@"SELECT "
                                "trx_id,"
                                "member,"
                                "customer,"
                                "customer_name,"
                                "customer_gstid,"
                                "customer_phone,"
                                "customer_pincode,"
                                "customer_address1,"
                                "customer_address2,"
                                "customer_email,"
                                "customer_city,"
                                "customer_profile_pic,"
                                "checkin_id,"
                                "create_time,"
                                "total_gst_value,"
                                "sub_total,"
                                "total,"
                                "currency,"
                                "discount_prcnt,"
                                "discount_amt,"
                                "tax_amt,"
                                "table_no,"
                                "order_type,"
                                "custom_discount_id,"
                                "voucher_id,"
                                "voucher_name,"
                                "voucher_amount,"
                                "voucher_type_id,"
                                "voucher_type_name,"
                                "voucher_member_number,"
                                "voucher_ref_number,"
                                "counter_number,"
                                "sales_person_id,"
                                "sales_person_name,"
                                "tbl_primary_id,"
                                "no_of_pax,"
                                "user_id,"
                                "voucher_bought_detail_id"
                                " FROM HOLD_ORDER"
                                " WHERE trx_id       = '%@'"
                                " AND company_id     = '%@'",
                                hold_order_id,[StringUtils get_business_detail:@"business_id"]];
        
        NSString *item_query = [NSString stringWithFormat:@"SELECT "
                                "id,"
                                "item_id,"
                                "item_name,"
                                "item_qty,"
                                "split_item_qty,"
                                "active_split_qty,"
                                "item_price,"
                                "cost_price,"
                                "selling_price,"
                                "item_remarks,"
                                "item_totalPrice,"
                                "item_discount,"
                                "item_checked,"
                                "custom_discount_id,"
                                "gst_code,"
                                "gst_rate,"
                                "gst_value,"
                                "type_of_price,"
                                "type_of_unit,"
                                "uom_qty,"
                                "stock_monitoring,"
                                "item_category_id,"
                                "printed_qty"
                                " FROM HOLD_ORDER_ITEMS"
                                " WHERE order_id = '%@' ",
                                hold_order_id];
        
        if(is_split_bill)
        {
            split_filter                    = @" AND item_checked = '1' order by item_category_id";
            hold_item_query                 = [NSString stringWithFormat:@"%@%@", item_query, split_filter];
        }
        else
        {
            hold_item_query                 = [NSString stringWithFormat:@"%@%@", item_query, @" order by item_category_id"];
        }
        
        CompleteOrder   *hold_order         = [[CompleteOrder alloc]  init];
        NSMutableArray *hold_order_array    = [[NSMutableArray alloc] init];
        
        NSArray *hold_order_data = [self get_from_queue:hold_query];
        
        for (NSDictionary *hold_data in hold_order_data)
        {
            hold_order.hold_order_id         = hold_data[@"trx_id"];
            hold_order.member_id             = hold_data[@"member"];
            hold_order.customer_id           = hold_data[@"customer"];
            hold_order.customer_name         = hold_data[@"customer_name"];
            hold_order.customer_gstid        = [hold_data[@"customer_gstid"] isKindOfClass:[NSString class]] ? hold_data[@"customer_gstid"] : @"";
            hold_order.customer_phone        = [hold_data[@"customer_phone"] isKindOfClass:[NSString class]] ? hold_data[@"customer_phone"] : @"";
            hold_order.customer_pincode      = [hold_data[@"customer_pincode"] isKindOfClass:[NSString class]] ? hold_data[@"customer_pincode"] : @"";
            hold_order.customer_address1     = [hold_data[@"customer_address1"] isKindOfClass:[NSString class]] ? hold_data[@"customer_address1"] : @"";
            hold_order.customer_address2     = [hold_data[@"customer_address2"] isKindOfClass:[NSString class]] ? hold_data[@"customer_address2"] : @"";
            hold_order.customer_email        = [hold_data[@"customer_email"] isKindOfClass:[NSString class]] ? hold_data[@"customer_email"] : @"";
            hold_order.customer_city         = [hold_data[@"customer_city"] isKindOfClass:[NSString class]] ? hold_data[@"customer_city"] : @"";
            hold_order.customer_profile_pic  = hold_data[@"customer_profile_pic"];
            hold_order.checkin_id            = hold_data[@"checkin_id"];
            hold_order.create_time           = hold_data[@"create_time"];
            hold_order.currency              = hold_data[@"currency"];
            hold_order.discount_amount       = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",hold_data[@"discount_amt"]]];
            hold_order.discount_percentage   = hold_data[@"discount_prcnt"];
            hold_order.tax_amount            = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",hold_data[@"tax_amt"]]];
            hold_order.table_no              = hold_data[@"table_no"];
            hold_order.order_type            = [hold_data[@"order_type"]intValue];
            hold_order.isOrderOnHold         = YES;
            hold_order.predefined_discount   = hold_data[@"custom_discount_id"];
            hold_order.counter_number        = [hold_data[@"counter_number"]intValue];
            hold_order.sales_person_id       = hold_data[@"sales_person_id"];
            hold_order.sales_person_name     = hold_data[@"sales_person_name"];
            hold_order.tbl_primary_id        = hold_data[@"tbl_primary_id"];
            hold_order.no_of_pax             = [hold_data[@"no_of_pax"]intValue];
            hold_order.voucher_id            = hold_data[@"voucher_id"];
            hold_order.voucher_name          = hold_data[@"voucher_name"];
            hold_order.voucher_amount        = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",hold_data[@"voucher_amount"]]];
            hold_order.voucher_type_id       = hold_data[@"voucher_type_id"];
            hold_order.voucher_type_name     = hold_data[@"voucher_type_name"];
            hold_order.voucher_member_number = hold_data[@"voucher_member_number"];
            hold_order.voucher_ref_number    = hold_data[@"voucher_ref_number"];
            hold_order.voucher_bought_detail_id = hold_data[@"voucher_bought_detail_id"];
            hold_order.user_id               = hold_data[@"user_id"];
            hold_order.subtotal              = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",hold_data[@"sub_total"]]];
            hold_order.total_gst_value       = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",hold_data[@"total_gst_value"]]];
            
            //TODO: find another way to get the total_gst_value_bfr_bill_disc, subtotal_bfr_bill_disc -- for now this is a quick fix
            if (hold_order.total_gst_value.doubleValue > 0) // has GST
            {
                hold_order.subtotal_bfr_bill_disc = [hold_order.subtotal decimalNumberByAdding:
                                                     [hold_order.discount_amount decimalNumberByMultiplyingBy:[self nongst_perc_in_decimal]]];
                hold_order.total_gst_value_bfr_bill_disc = [[hold_order.total_gst_value decimalNumberBySubtracting:
                                                             [hold_order.tax_amount decimalNumberByMultiplyingBy:[self gst_perc_in_decimal]]]
                                                            decimalNumberByAdding: [hold_order.discount_amount decimalNumberByMultiplyingBy:[self gst_perc_in_decimal]]];
            }
            else
            {
                hold_order.subtotal_bfr_bill_disc = [hold_order.subtotal decimalNumberByAdding: hold_order.discount_amount];
                hold_order.total_gst_value_bfr_bill_disc = hold_order.total_gst_value;
            }
            hold_order.from_split_bill = is_split_bill;
            
            NSArray *return_item_data = [self get_from_queue:hold_item_query];
            
            for(NSDictionary *item_data in return_item_data)
            {
                OrderItem *order_item           = [[OrderItem alloc] init];
                order_item.hold_item_id         = item_data[@"id"];
                order_item.itemID               = item_data[@"item_id"];
                order_item.itemName             = [NSString stringWithFormat:@"%@",item_data[@"item_name"]];
                order_item.split_item_qty       = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"split_item_qty"]]];
                order_item.itemPrice            = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"item_price"]]];
                order_item.cost_price           = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"cost_price"]]];
                order_item.selling_price        = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"selling_price"]]];
                order_item.remarks              = item_data[@"item_remarks"];
                order_item.totalPrice           = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"item_totalPrice"]]];
                order_item.discount             = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"item_discount"]]];
                order_item.item_checked         = item_data[@"item_checked"];
                order_item.type_of_price        = item_data[@"type_of_price"];
                order_item.type_of_unit         = item_data[@"type_of_unit"];
                order_item.uom_qty              = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"uom_qty"]]];
                order_item.predefined_discount  = item_data[@"custom_discount_id"];
                order_item.gst_code             = item_data[@"gst_code"];
                order_item.gst_rate             = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"gst_rate"]]];
                order_item.gst_value            = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"gst_value"]]];
                order_item.stock_monitoring     = item_data[@"stock_monitoring"];
                order_item.category_id          = [NSNumber numberWithInt:[item_data[@"item_category_id"] intValue]];
                order_item.itemQuantityPrinted  = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"printed_qty"]]];
                
                order_item.allModifiersDict     = [self is_modifier_exist:item_data[@"item_id"]] ? [self get_all_modifiers:(NSString *)order_item.hold_item_id withModItemID:order_item.itemID] : nil;
                
                order_item.itemQuantity         = (is_split_bill) ?
                [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"active_split_qty"]]] :
                [[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"item_qty"]]] decimalNumberBySubtracting:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",item_data[@"split_item_qty"]]]];
                
                [hold_order_array addObject:order_item];
            }
            hold_order.orderItemsArray          = [NSArray arrayWithArray:hold_order_array];
            
            return hold_order.newCalculate;
        }
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (NSArray *)get_all_on_hold_orders
{
    @try
    {
        NSString *query = @"SELECT trx_id,customer_name,tbl_primary_id,table_no,order_type,"
        "is_locked,no_of_pax,voucher_amount,discount_amt,is_order_merged,total,created_date FROM HOLD_ORDER";
        
        NSArray *resultSet   = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (void)updateOrderOnHold:(CompleteOrder *)holdOrder
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"UPDATE HOLD_ORDER SET "
                           "sub_total               = '%@',"
                           "total                   = '%@',"
                           "total_gst_value         = '%@',"
                           "currency                = '%@',"
                           "discount_prcnt          = '%@',"
                           "discount_amt            = '%@',"
                           "tax_amt                 = '%@',"
                           "table_no                = '%@',"
                           "sales_person_id         = '%@',"
                           "sales_person_name       = '%@',"
                           "tbl_primary_id          = '%@',"
                           "order_type              = '%@',"
                           "voucher_id              = '%@',"
                           "voucher_name            = '%@',"
                           "voucher_amount          = '%@',"
                           "voucher_type_id         = '%@',"
                           "voucher_type_name       = '%@',"
                           "voucher_member_number   = '%@',"
                           "voucher_ref_number      = '%@',"
                           "no_of_pax               = '%d',"
                           
                           "member                  = '%@',"
                           "customer                = '%@',"
                           "customer_name           = '%@',"
                           "customer_profile_pic    = '%@',"
                           "checkin_id              = '%@',"
                           
                           "counter_number          = '%@',"
                           "updated_by              = '%@',"
                           "updated_date            = '%@' "
                           "WHERE trx_id            = '%@' "
                           "AND company_ID          = '%@' ",
                           holdOrder.subtotal.stringValue,
                           holdOrder.total.stringValue, //float changes
                           holdOrder.total_gst_value       ?: @"",
                           holdOrder.currency              ?: @"",
                           holdOrder.discount_percentage   ?: @"",
                           holdOrder.discount_amount.stringValue, //float changes
                           holdOrder.tax_amount.stringValue, //float changes
                           holdOrder.table_no              ?: @"",
                           holdOrder.sales_person_id       ?: @"",
                           holdOrder.sales_person_name     ?: @"",
                           holdOrder.tbl_primary_id        ?: @"",
                           [NSString stringWithFormat:@"%d", holdOrder.order_type],
                           holdOrder.voucher_id            ?: @"",
                           holdOrder.voucher_name          ?: @"",
                           holdOrder.voucher_amount.stringValue,
                           holdOrder.voucher_type_id       ?: @"",
                           holdOrder.voucher_type_name     ?: @"",
                           holdOrder.voucher_member_number ?: @"",
                           holdOrder.voucher_ref_number    ?: @"",
                           holdOrder.no_of_pax             ?: 0,
                           
                           holdOrder.member_id             ?:@"",
                           holdOrder.customer_id           ?:@"",
                           holdOrder.customer_name         ?: @"Walk In",
                           holdOrder.customer_profile_pic  ?:@"",
                           holdOrder.checkin_id            ?:@"",
                           
                           [NSString stringWithFormat:@"%d", holdOrder.counter_number],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP,
                           holdOrder.hold_order_id,
                           [StringUtils get_business_detail:@"business_id"]
                           ];
        
        [self add_to_queue:query];
        
        [self updateOrderItemsOnHold:holdOrder.orderItemsArray ofOrderID:holdOrder.hold_order_id];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (void)updateOrderItemsOnHold:(NSArray*)orderItems ofOrderID:(NSString *)hold_order_id
{
    @try
    {
        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
        
        for (OrderItem *order in orderItems)
        {
            NSArray *choice_array      = order.allModifiersDict[CHOICE];
            NSArray *addition_array    = order.allModifiersDict[ADDITION];
            order.is_edited_item_price = @"";
            NSString *remarks  = order.remarks ? [order.remarks stringByReplacingOccurrencesOfString:@"'" withString:@"\""] : @"";
            
            if([self record_already_exists:@"HOLD_ORDER_ITEMS" with_filter:[NSString stringWithFormat:@" id = '%@' ",order.hold_item_id]])
            {
                NSString *query = [NSString stringWithFormat:
                                   @"UPDATE HOLD_ORDER_ITEMS SET "
                                   "item_qty               = '%@', "
                                   "split_item_qty         = '%@', "
                                   "item_totalPrice        = '%@', "
                                   "cost_price             = '%@', "
                                   "selling_price          = '%@', "
								   "item_price             = '%@', "
                                   "is_edited_item_price   = '%@', "
                                   "item_remarks           = '%@', "
                                   "item_discount          = '%@', "
                                   "custom_discount_id     = '%@', "
                                   "gst_code               = '%@', "
                                   "gst_rate               = '%@', "
                                   "gst_value              = '%@', "
                                   "type_of_price          = '%@', "
                                   "type_of_unit           = '%@', "
                                   "uom_qty                = '%@', "
                                   "stock_monitoring       = '%@', "
                                   "item_category_id       = '%@', "
                                   "updated_by             = '%@', "
                                   "updated_date           = '%@', "
                                   "printed_qty            = '%@'  "
                                   "WHERE id = '%@'",
                                   order.itemQuantity,
                                   @"0",
                                   order.totalPrice,
                                   order.cost_price,
                                   order.selling_price,
								   order.itemPrice,
                                   order.is_edited_item_price,
                                   remarks,
                                   order.discount ?: @"",
                                   order.predefined_discount,
                                   order.gst_code,
                                   order.gst_rate,
                                   order.gst_value,
                                   order.type_of_price,
                                   order.type_of_unit,
                                   order.uom_qty,
                                   order.stock_monitoring,
                                   order.category_id,
                                   [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                                   CURRENT_TIMESTAMP,
                                   order.itemQuantityPrinted,
                                   order.hold_item_id
                                   ];
                
                [array_of_query addObject:query];
                
                [self deleteOrderModifierOnHold:order.hold_item_id];
                
                if(order.allModifiersDict != nil &&
                   (choice_array.count > 0 || addition_array.count > 0 ))
                {
                    [self insertOrderModifierOnHold:order.allModifiersDict    withHoldOrderItemID:[NSString stringWithFormat:@"%@",order.hold_item_id]];
                }
            }
            else
            {
                [self insertOrderItemsOnHold:@[order] ofOrderID:hold_order_id];
            }
        }
        
        [self add_to_queue:array_of_query with_priority:low];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (NSString *)insertOrderOnHold:(CompleteOrder *)holdOrder
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"INSERT INTO HOLD_ORDER"
                           "(trx_id,"
                           "user_id,"
                           "company_id,"
                           "member,"
                           "customer,"
                           "customer_name,"
                           "customer_profile_pic,"
                           "checkin_id,"
                           "create_time,"
                           "sub_total,"
                           "total,"
                           "total_gst_value,"
                           "currency,"
                           "discount_prcnt,"
                           "discount_amt,"
                           "sales_person_id,"
                           "sales_person_name,"
                           "tbl_primary_id,"
                           "no_of_pax,"
                           "tax_amt,table_no,order_type,custom_discount_id,"
                           "voucher_id,voucher_name,voucher_amount,voucher_type_id,"
                           "voucher_type_name,voucher_member_number,voucher_ref_number,"
                           "counter_number,voucher_bought_detail_id)"
                           @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%d','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           holdOrder.hold_order_id,
                           holdOrder.user_id,
                           [StringUtils get_business_detail:@"business_id"],
                           holdOrder.member_id             ? holdOrder.member_id             : @"",
                           holdOrder.customer_id           ? holdOrder.customer_id           : @"",
                           holdOrder.customer_name         ? holdOrder.customer_name         : @"",
                           holdOrder.customer_profile_pic  ? holdOrder.customer_profile_pic  : @"",
                           holdOrder.checkin_id            ? holdOrder.checkin_id            : @"",
                           holdOrder.create_time           ? holdOrder.create_time           : @"",
                           holdOrder.subtotal.stringValue,
                           holdOrder.total.stringValue, //float changes
                           holdOrder.total_gst_value       ? holdOrder.total_gst_value       : @"",
                           holdOrder.currency              ? holdOrder.currency              : @"",
                           holdOrder.discount_percentage   ? holdOrder.discount_percentage   : @"",
                           holdOrder.discount_amount.stringValue, //float changes
                           holdOrder.sales_person_id       ? holdOrder.sales_person_id       : @"",
                           holdOrder.sales_person_name     ? holdOrder.sales_person_name     : @"",
                           holdOrder.tbl_primary_id     ? holdOrder.tbl_primary_id           : @"",
                           holdOrder.no_of_pax             ? holdOrder.no_of_pax             : 0,
                           holdOrder.tax_amount.stringValue, //float changes
                           holdOrder.table_no              ? holdOrder.table_no              : @"",
                           [NSString stringWithFormat:@"%d", holdOrder.order_type],
                           holdOrder.predefined_discount   ? holdOrder.predefined_discount   : @"",
                           holdOrder.voucher_id            ? holdOrder.voucher_id            : @"",
                           holdOrder.voucher_name          ? holdOrder.voucher_name          : @"",
                           holdOrder.voucher_amount.stringValue,
                           holdOrder.voucher_type_id       ? holdOrder.voucher_type_id       : @"",
                           holdOrder.voucher_type_name     ? holdOrder.voucher_type_name     : @"",
                           holdOrder.voucher_member_number ? holdOrder.voucher_member_number : @"",
                           holdOrder.voucher_ref_number    ? holdOrder.voucher_ref_number    : @"",
                           [NSString stringWithFormat:@"%d", holdOrder.counter_number],
                           holdOrder.voucher_bought_detail_id    ? holdOrder.voucher_bought_detail_id : @""
                           ];
        
        NSString *hold_order_id = holdOrder.hold_order_id;
        
        [self add_to_queue:query];
        
        [self insertOrderItemsOnHold:holdOrder.orderItemsArray ofOrderID:holdOrder.hold_order_id];
        
        return hold_order_id;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)insertOrderItemsOnHold:(NSArray*)orderItems ofOrderID:(NSString *)hold_order_id
{
    @try
    {
        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
        
        for (OrderItem *order in orderItems)
        {
            NSString *unique_id     = [self get_unique_id_from_db];
            NSArray *choice_array   = (order.allModifiersDict)[CHOICE];
            NSArray *addition_array = (order.allModifiersDict)[ADDITION];
            NSString *remarks  = order.remarks ? [order.remarks stringByReplacingOccurrencesOfString:@"'" withString:@"\""] : @"";
            
            NSString *query = [NSString stringWithFormat:
                               @"INSERT INTO HOLD_ORDER_ITEMS "
                               "(item_id,"
                               "id,"
                               "item_name,"
                               "item_qty,"
                               "split_item_qty,"
                               "item_price,"
                               "cost_price,"
                               "selling_price,"
                               "item_remarks,"
                               "item_discount,"
                               "item_totalPrice,"
                               "order_id,"
                               "custom_discount_id,"
                               "gst_code,"
                               "gst_rate,"
                               "gst_value,"
                               "uom_qty,"
                               "printed_qty,"
                               "type_of_price,"
                               "type_of_unit,"
                               "stock_monitoring,"
                               "item_category_id) "
                               "VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                               order.itemID,
                               unique_id,
                               order.itemName,
                               order.itemQuantity,
                               @"0",
                               order.itemPrice,
                               order.cost_price,
                               order.selling_price,
                               remarks,
                               order.discount,
                               order.totalPrice,
                               hold_order_id,
                               order.predefined_discount,
                               order.gst_code,                     //GST version
                               order.gst_rate,
                               order.gst_value,
                               order.uom_qty,
                               order.itemQuantityPrinted,//uncomment when db able to migrate
                               order.type_of_price,
                               order.type_of_unit,
                               order.stock_monitoring,
                               order.category_id];
            
            [array_of_query addObject:query];
            
            if(order.allModifiersDict != nil &&
               (choice_array.count > 0 || addition_array.count > 0 ))
            {
                [self insertOrderModifierOnHold:order.allModifiersDict withHoldOrderItemID:unique_id];
            }
        }
        
        [self add_to_queue:array_of_query with_priority:low];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (BOOL)insertOrderModifierOnHold:(NSDictionary *)modifiers withHoldOrderItemID:(NSString*)hold_order_item_id
{
    @try
    {
        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
        
        NSString *main_query = @"INSERT INTO HOLD_ORDER_MODIFIERS"
        "(id,"
        "hold_order_item_id,"
        "mod_type,"
        "mod_parent_id,"
        "mod_parent_name,"
        "mod_id,"
        "mod_name,"
        "mod_price,"
        "mod_item_id,"
        "mod_qty,"
        "selected_index)"
        "VALUES ";
        
        
        if (![modifiers isKindOfClass:[NSNull class]])
        {
            NSArray *choice_array           = modifiers[CHOICE];
            NSArray *addition_array         = modifiers[ADDITION];
            
            for (NSDictionary *dict in choice_array)
            {
                NSDictionary *selectedDict = dict[@"selected_choice"];
                
                NSString *query = [NSString stringWithFormat:
                                   @"%@('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                                   main_query,
                                   [self get_unique_id_from_db],
                                   hold_order_item_id,
                                   selectedDict[@"mod_type"],
                                   selectedDict[@"mod_parent_id"],
                                   selectedDict[@"mod_parent_name"],
                                   selectedDict[@"mod_id"],
                                   selectedDict[@"mod_name"],
                                   selectedDict[@"mod_price"],
                                   selectedDict[@"mod_item_id"],
                                   @"1",
                                   selectedDict[@"selected_index"]];
                
                [array_of_query addObject:query];
            }
            
            for (NSDictionary *dict in addition_array)
            {
                if([dict[@"mod_qty"] integerValue] > 0)
                {
                    NSString *query = [NSString stringWithFormat:
                                       @"%@('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                                       main_query,
                                       [self get_unique_id_from_db],
                                       hold_order_item_id,
                                       dict[@"mod_type"],
                                       dict[@"mod_parent_id"],
                                       dict[@"mod_parent_name"],
                                       dict[@"mod_id"],
                                       dict[@"mod_name"],
                                       dict[@"mod_price"],
                                       dict[@"mod_item_id"],
                                       dict[@"mod_qty"],
                                       @"0"];
                    
                    [array_of_query addObject:query];
                }
            }
            
            [self add_to_queue:array_of_query with_priority:low];
        }
    }
    
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)deleteOrderOnHold:(CompleteOrder*)compOrder andSplitBill:(BOOL)split_bill
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"DELETE FROM HOLD_ORDER WHERE trx_id = '%@' AND company_id = %@",
                           compOrder.hold_order_id,
                           [StringUtils get_business_detail:@"business_id"]];
        
        if (split_bill)         // check if split_bill
        {
            if ([self hold_order_has_remaining_item:compOrder.hold_order_id])
            {
                [self remove_item_with_empty_qty];
                [self updateHoldOrderTotalFromSplitBill:compOrder];
                return;
            }
        }
        
        [self add_to_queue:query];
        
        for (OrderItem *item_onHold in compOrder.orderItemsArray)
        {
            [self deleteOrderItemOnHold:item_onHold.hold_item_id];
        }
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)deleteOrderItemOnHold:(NSString*)orderItemId
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"DELETE FROM HOLD_ORDER_ITEMS WHERE id = '%@'",
                           orderItemId];
        [self add_to_queue:query];
        
        [self deleteOrderModifierOnHold:orderItemId];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)deleteOrderModifierOnHold:(NSString*)hold_order_item_id
{
    NSString *query = [NSString stringWithFormat:
                       @"DELETE FROM HOLD_ORDER_MODIFIERS WHERE hold_order_item_id = '%@'",
                       hold_order_item_id];
    [self add_to_queue:query];
}

- (void)set_hold_order_locked:(int)status trx_id:(NSString*)trx_id
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"UPDATE HOLD_ORDER set is_locked = '%@' "
                           "WHERE trx_id = '%@'",
                           [NSString stringWithFormat:@"%d",status],trx_id];
        
        [self add_to_queue:query];
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)mergeHoldOrderFrom:(NSArray *)arrHoldOrder into:(NSDictionary *)holdOrder //here
{
    @try
    {
        NSMutableArray *arrayOfQuery = [[NSMutableArray alloc] init];
        NSString *query;
        
        // update data on selected holdOrder
        query = [NSString stringWithFormat:
                 @"UPDATE HOLD_ORDER SET "
                 "total = '%@', "
                 "is_order_merged = '%@', "
                 "no_of_pax = '%@', "
                 "table_no = '%@', "
                 "tbl_primary_id = '%@' "
                 "WHERE trx_id = '%@'",
                 holdOrder[@"total_amount"],
                 [arrHoldOrder count] > 0 ? @"1" : @"0",
                 holdOrder[@"no_of_pax"],
                 holdOrder[@"table_no"],
                 holdOrder[@"table_id"],
                 holdOrder[@"order_id"]];
        [arrayOfQuery addObject:query];
        
        for(NSDictionary *dict in arrHoldOrder)
        {
            if (![dict[@"order_id"] isEqualToString:holdOrder[@"order_id"]])
            {
                // move item from arrHoldOrder into selected holdOrder
                query = [NSString stringWithFormat:
                         @"UPDATE HOLD_ORDER_ITEMS SET "
                         "order_id = '%@' "
                         "WHERE order_id = '%@'",
                         holdOrder[@"order_id"],
                         dict[@"order_id"]];
                [arrayOfQuery addObject:query];

                // delete order from arrHoldOrder
                query = [NSString stringWithFormat:
                         @"DELETE FROM HOLD_ORDER WHERE trx_id = '%@'",
                         dict[@"order_id"]];
                [arrayOfQuery addObject:query];
            }
        }
        
        [self add_to_queue:arrayOfQuery with_priority:low];
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)update_hold_order_merged_status:(NSDictionary *)updated_data //here
{
    @try
    {
        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
        NSString *query;
        
        query   = [NSString stringWithFormat:
                   @"UPDATE HOLD_ORDER SET "
                   "total = '%@', "
                   "is_order_merged = '%@', "
                   "no_of_pax = '%@', "
                   "table_no = '%@', "
                   "tbl_primary_id = '%@' "
                   "WHERE trx_id = '%@'",
                   updated_data[@"total_amount"],
                   updated_data[@"is_order_merged"],
                   updated_data[@"no_of_pax"],
                   updated_data[@"table_no"],
                   updated_data[@"table_primary_id"],
                   updated_data[@"order_id"]];
        [array_of_query addObject:query];
        
        // update all item from second order to point to primary order
        query   = [NSString stringWithFormat:
                   @"UPDATE HOLD_ORDER_ITEMS SET "
                   "order_id = '%@' "
                   "WHERE order_id = '%@'",
                   updated_data[@"order_id"],
                   updated_data[@"second_order_id"]];
        [array_of_query addObject:query];
        
        // then delete the second order
        query   = [NSString stringWithFormat:
                   @"DELETE FROM HOLD_ORDER WHERE trx_id = '%@'",
                   updated_data[@"second_order_id"]];
        [array_of_query addObject:query];
        
        [self add_to_queue:array_of_query with_priority:low];
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Split Bill section
- (NSArray *)get_orders_item_for_split_bill:(NSString*)orderID
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT "
                           "id,"
                           "item_id,"
                           "item_name,"
                           "(item_qty - split_item_qty) as item_qty,"
                           "split_item_qty,"
                           "(item_totalPrice / (item_qty - split_item_qty)) as item_price,"
                           "uom_qty,"
                           "item_discount,"
                           "item_totalPrice,"
                           "type_of_price,"
                           "type_of_unit,"
                           "item_checked,"
                           "stock_monitoring "
                           " FROM HOLD_ORDER HO "
                           " INNER JOIN HOLD_ORDER_ITEMS ON HO.trx_id = order_id "
                           " WHERE HO.trx_id    = '%@'"
                           " AND HO.company_id  = '%@'",
                           orderID,[StringUtils get_business_detail:@"business_id"]];
        
        NSArray *orders_array = [self get_from_queue:query]; // local DB
        
        return orders_array;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)update_order_item:(NSArray*)orderItems ofOrderID:(int)orderId
{
    @try
    {
        for (NSDictionary *dict in orderItems)
        {
            int active_split_item_qty = [dict[@"item_checked"] isEqualToString:@"1"] ? active_split_item_qty = [dict[@"item_qty"] intValue] : 0;
            
            NSString *query = [NSString stringWithFormat:
                               @"UPDATE HOLD_ORDER_ITEMS SET "
                               "item_checked         = '%@', "
                               "active_split_qty     = '%d' "
                               "WHERE id             = '%@' ",
                               dict[@"item_checked"],
                               active_split_item_qty,
                               dict[@"id"]];
            
            [self add_to_queue:query];
        }
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)reupdate_confirmed_order_item
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"UPDATE HOLD_ORDER_ITEMS SET "
                           " split_item_qty            = split_item_qty + active_split_qty,"
                           " item_totalPrice           = item_totalPrice - (item_price * active_split_qty),"
                           " updated_by                = '%@',"
                           " updated_date              = '%@',"
                           " item_checked              = 0,"
                           " active_split_qty          = 0 "
                           " WHERE active_split_qty > 0 ",
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],CURRENT_TIMESTAMP];
        
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (BOOL)hold_order_has_remaining_item:(NSString*)orderID
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT * "
                           "FROM HOLD_ORDER_ITEMS "
                           "WHERE order_id = '%@' "
                           "AND item_qty > split_item_qty ",orderID];
        
        NSArray *resultSet = [self get_from_queue:query];
        
        return resultSet.count > 0;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)remove_item_with_empty_qty
{
    @try
    {
        NSString *query = @"SELECT id from HOLD_ORDER_ITEMS WHERE split_item_qty = item_qty ";
        NSArray *resultSet = [self get_from_queue:query];
        for(NSDictionary *dict in resultSet)
        {
            [self deleteOrderItemOnHold:[NSString stringWithFormat:@"%@",dict[@"id"]]];
        }
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)updateHoldOrderTotalFromSplitBill:(CompleteOrder *)order
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"UPDATE HOLD_ORDER SET total = total - %@ WHERE trx_id = '%@' ",order.total,order.hold_order_id];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
