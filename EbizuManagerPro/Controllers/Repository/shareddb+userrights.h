//
//  shareddb+userrights.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (userrights)

- (BOOL)supervisor_pin_check:(NSString *)pin_code;
- (BOOL)validate_user_pin:(NSString *)password;
- (void)update_last_login:(NSString *)user_id;
- (NSArray *)get_all_sp_data;
- (NSArray *)get_all_employee;

@end
