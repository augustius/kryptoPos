//
//  shareddb+employeeTimecard.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (employeeTimecard)

- (BOOL)employee_clocked_in;
- (void)insert_new_employee_timecard;
- (void)update_employee_timecard;
- (NSArray *)get_employee_clock_in_out_record:(NSDate *)shift_start to:(NSDate *)shift_end user_id:(NSString*)user_id;

@end
