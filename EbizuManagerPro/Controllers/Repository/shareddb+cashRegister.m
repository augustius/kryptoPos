//
//  shareddb+cashRegister.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+cashRegister.h"
#import "KryptoPOS-Swift.h"

@implementation shareddb (cashRegister)

- (void)add_cash_in:(NSArray *)data
{
    @try
    {
        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
        NSString *query = [NSString stringWithFormat:
                           @"INSERT INTO CASH_REGISTER"
                           "(cash_reg_id,cash_type,amount,member_id,company_id,create_time,user_id,trx_id,notes)"
                           "VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                           [self get_unique_id_from_db],
                           data[0][@"cash_type"],
                           data[0][@"amount"],
                           data[0][@"member_id"],
                           [StringUtils get_business_detail:@"business_id"],
                           CURRENT_TIMESTAMP,
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           data[0][@"trx_id"],
                           data[0][@"notes"]
                           ];
        [array_of_query addObject:query];
        
        // insert shift below
        if (![data[0][@"shift_action"] isEqualToString:@""])
        {
            if([[NSUserDefaults standardUserDefaults] boolForKey:SHIFT_OPENED])
            {
                query   = [NSString stringWithFormat:
                           @"UPDATE SHIFT_DETAILS set shift_status = '0',"
                           "shift_closed_time = '%@',"
                           "sync_id = '',"
                           "closed_drawer_amount = '%@', "
                           "updated_by = '%@', "
                           "updated_date = '%@' "
                           "WHERE shift_status = 1",
                           CURRENT_TIMESTAMP,
                           data[0][@"amount"],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           CURRENT_TIMESTAMP];
            }
            else
            {
                
                query = [NSString stringWithFormat:
                         @"INSERT INTO SHIFT_DETAILS"
                         "(id,user_id,shift_status,shift_open_time,"
                         "shift_closed_time,open_drawer_amount,closed_drawer_amount,"
                         "company_id,created_by,created_date)"
                         " VALUES ('%@','%@','1','%@','NULL','%@','0','%@','%@','%@')",
                         [self get_unique_id_from_db],
                         [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                         CURRENT_TIMESTAMP,
                         data[0][@"amount"],
                         [StringUtils get_business_detail:@"business_id"],
                         [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                         CURRENT_TIMESTAMP
                         ];
            }
            
            [array_of_query addObject:query];
        }
        
        [self add_to_queue:array_of_query with_priority:high];
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Method use for Cash Register Report
- (NSArray *)get_cash_register_history_from:(NSDate *)startDate to:(NSDate *)endDate
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT cash.*, "
                           "strftime('%@', datetime(cash.create_time,'unixepoch','localtime')) AS createtime,"
                           "(SELECT name FROM USERRIGHTS WHERE user_id = cash.user_id) AS userright_name, "
                           "CASE WHEN cash_type = '0' THEN 'cash in' ELSE 'cash out' END AS cash_type_name "
                           "FROM CASH_REGISTER cash "
                           "WHERE datetime(cash.create_time,'unixepoch','localtime') BETWEEN '%@' AND '%@' "
                           "ORDER BY create_time DESC "
                           ,@"%d-%m-%Y %H:%M:%S"
                           ,startDate.sql_string
                           ,endDate.sql_string];
        
        NSArray *result = [self get_from_queue:query];
        
        return result;
    }
    @catch (NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Method use for 'Daily Sales Report Print Out'
- (NSString *)get_cash_in_today
{
    return [self get_cash_in_date:[NSDate date]];
}

- (NSString *)get_cash_in_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;
    return [self get_cash_in_from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_cash_in_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *cash_amount = @"0.0";
        
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(sum(amount),0) as total_cash "
                           "FROM CASH_REGISTER "
                           "WHERE CASH_REGISTER.cash_type = 0 "
                           "AND CASH_REGISTER.company_id='%@' "
                           "AND strftime(datetime(CASH_REGISTER.create_time, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        NSLog(@"%@",query);
        
        NSArray *resultSet = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            cash_amount = dict[@"total_cash"];
        }
        
        return cash_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSString *)get_cash_out_today
{
    return [self get_cash_out_date:[NSDate date]];
}

- (NSString *)get_cash_out_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;
    return [self get_cash_out_from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_cash_out_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *cash_amount = @"0.0";
        
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(sum(amount),0) as total_cash "
                           "FROM CASH_REGISTER "
                           "WHERE CASH_REGISTER.cash_type = 1 "
                           "AND CASH_REGISTER.company_id='%@' "
                           "AND strftime(datetime(CASH_REGISTER.create_time, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        NSLog(@"%@",query);
        
        NSArray *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            cash_amount = dict[@"total_cash"];
        }
        
        return cash_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
