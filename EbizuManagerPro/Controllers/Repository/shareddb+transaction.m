//
//  shareddb+transaction.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 28/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+transaction.h"
#import "KryptoPOS-Swift.h"

@implementation shareddb (transaction)

//FIXME: pass completeOrder as modal rather than dict (refer to paymentForm complete_transaction_dict)
- (void)insert_trx_details:(NSDictionary*)dict
{
    @try
    {
        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
        NSString *temp_query    = @"";
        
        if([dict count] > 0)
        {
            temp_query = [NSString stringWithFormat:@"INSERT INTO TRANSACTIONS "
                          "(id,"
                          "poh_order_id,"
                          "poh_total,"
                          "poh_lpt_id,"
                          "poh_mem_id,"
                          "poh_usr_id,"
                          "poh_transaction_type,"
                          "poh_close_datetime,"
                          "poh_mem_screen_name,"
                          "poh_customer_gstid,"
                          "poh_customer_email,"
                          "poh_customer_phone,"
                          "poh_customer_city,"
                          "poh_customer_address1,"
                          "poh_customer_address2,"
                          "poh_customer_pincode,"
                          "poh_sales_person_id,"
                          "poh_sales_person_name,"
                          "poh_gst_mode,"
                          "poh_offer_sn,"
                          "poh_subtotal,"
                          "poh_cus_id,"
                          "poh_discount_amount,"
                          "poh_currency,"
                          "poh_tax_amount,"
                          "poh_com_id,"
                          "poh_create_datetime,"
                          "poh_discount_percentage,"
                          "poh_notes,"
                          "poh_transaction_status,"
                          "poh_payment_method_id,"
                          "sync_id,"
                          "poh_table_number,"
                          "poh_discount_id,"
                          "poh_custom_field,"
                          "poh_voucher_id,"
                          "poh_voucher_name,"
                          "poh_voucher_amount,"
                          "poh_voucher_type_id,"
                          "poh_voucher_type_name,"
                          "poh_voucher_member_number,"
                          "poh_voucher_ref_number,"
                          "poh_counter_number,"
                          "poh_rounding_adj,"
                          "poh_trans_number,"
                          "poh_total_gst_value,"
                          "poh_payment_amount,"
                          "poh_payment_changes,"
                          "poh_pyt_name,"
                          "poh_pvd_id,"
                          "poh_no_of_pax,"
                          "poh_order_type,"
                          "poh_card_number"
                          ") "
                          @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@');",
                          dict[@"trx_id"],
                          dict[@"hold_order_id"],
                          dict[@"total"],
                          dict[@"checkin_id"],
                          dict[@"member_id"],
                          dict[@"user"],
                          dict[@"payment"],
                          dict[@"close_time"],
                          dict[@"customer_name"],
                          dict[@"customer_gstid"],
                          dict[@"customer_email"],
                          dict[@"customer_phone"],
                          dict[@"customer_city"],
                          dict[@"customer_address1"],
                          dict[@"customer_address2"],
                          dict[@"customer_pincode"],
                          dict[@"sales_person_id"],
                          dict[@"sales_person_name"],
                          dict[@"trx_gst_mode"],
                          dict[@"offer_sn"],
                          dict[@"subtotal"],
                          dict[@"customer"],
                          dict[@"discount_amount"],
                          dict[@"currency"],
                          dict[@"tax_amount"],
                          dict[@"company"],
                          dict[@"create_time"],
                          dict[@"discount_percentage"],
                          dict[@"notes"],
                          dict[@"status"],
                          dict[@"payment_method_id"],
                          @"",// insert empty string for sync_id
                          dict[@"table_no"],
                          dict[@"predefined_discount"],
                          dict[@"custom_field_name"],
                          dict[@"voucher_id"],
                          dict[@"voucher_name"],
                          dict[@"voucher_amount"],
                          dict[@"voucher_type_id"],
                          dict[@"voucher_type_name"],
                          dict[@"voucher_member_number"],
                          dict[@"voucher_ref_number"],
                          dict[@"counter_number"],
                          dict[@"rounding_amount"],
                          dict[@"trans_number"],
                          dict[@"total_gst_value"],
                          dict[@"payment_amount"],
                          dict[@"payment_changes"],
                          dict[@"pay_type_name"],
                          dict[@"voucher_bought_detail_id"],
                          dict[@"no_of_pax"],
                          dict[@"order_type"],
                          dict[@"credit_card_no"]
                          // FIXME: Check if updated_by is needed as it is missing from current statements.
                          ];
            
            [array_of_query addObject:temp_query];
            
            if(![[dict objectForKey:@"voucher_bought_detail_id"] isEqualToString:@""])
            {
                temp_query = [NSString stringWithFormat:@"UPDATE VOUCHERS_BOUGHT_DETAIL SET updated_by = '%@', updated_date = strftime('%@', 'now'),"
                              "redeemed = 1 WHERE id = '%@' ;",
                              [StringUtils get_user_detail:@"user_id"],@"%s",[dict objectForKey:@"voucher_bought_detail_id"]];
                
                [array_of_query addObject:temp_query];
            }
            
            NSArray *itemArr = [dict objectForKey:@"items"];
            
            for (int i = 0; i < [itemArr count]; i++)
            {
                NSString *unique_id_for_each_item = [self get_unique_id_from_db];
                
                //GST version for gst_code & gst_rate
                temp_query = [NSString stringWithFormat:@"INSERT INTO TRN_ITEMS "
                              "("
                              "id,"
                              "pod_poh_sn,"
                              "pod_item_id,"
                              "pod_item,"
                              "pod_qty,"
                              "pod_price,"
                              "pod_price_goods,"
                              "pod_selling_price,"
                              "pod_gst_code,"
                              "pod_gst_rate,"
                              "pod_gst_value,"
                              "pod_uom_qty,"
                              "pod_type_of_price,"
                              "pod_type_of_unit,"
                              "pod_remarks,"
                              "pod_discount_amount,"
                              "pod_discount_id,"
                              "pod_stock_monitoring,"
                              "pod_com_id"
                              ")"
                              @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@') ;",
                              unique_id_for_each_item,
                              dict[@"trx_id"],
                              itemArr[i][@"itemID"],
                              itemArr[i][@"itemName"],
                              itemArr[i][@"itemQuantity"],
                              itemArr[i][@"totalPrice"],
                              itemArr[i][@"cost_price"],
                              itemArr[i][@"selling_price"],
                              itemArr[i][@"gst_code"],
                              itemArr[i][@"gst_rate"],
                              itemArr[i][@"gst_value"],
                              itemArr[i][@"uom_qty"],
                              itemArr[i][@"type_of_price"],
                              itemArr[i][@"type_of_unit"],
                              itemArr[i][@"remarks"],
                              itemArr[i][@"discount"],
                              itemArr[i][@"predefined_discount"],
                              itemArr[i][@"stock_monitoring"],
                              dict[@"company"]
                              ];
                
                [array_of_query addObject:temp_query];
                
                
                NSArray *adModArr = itemArr[i][@"modifiers"][0][@"addition"];
                NSArray *chModArr = itemArr[i][@"modifiers"][0][@"choice"];
                
                for (int ad = 0; ad < [adModArr count]; ad++)
                {
                    CGFloat mod_amount = [[[adModArr objectAtIndex:ad] objectForKey:@"mod_price"] floatValue] * [[[adModArr objectAtIndex:ad] objectForKey:@"child_qty"] intValue];
                    
                    temp_query = [NSString stringWithFormat:@"INSERT INTO TRN_MODIFIERS "
                                  "(id,"
                                  "pom_pod_sn,"
                                  "pom_poh_sn,"
                                  "pom_type,"
                                  "pom_mod_id,"
                                  "pom_name,"
                                  "pom_price,"
                                  "pom_mod_parent_id,"
                                  "pom_mod_parent_name,"
                                  "pom_mod_amount,"
                                  "pom_mod_qty,"
                                  "pom_mod_item_id,"
                                  "pom_com_id"
                                  ") "
                                  @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@') ;",
                                  [self get_unique_id_from_db],
                                  unique_id_for_each_item,
                                  dict[@"trx_id"],
                                  ADDITION,
                                  adModArr[ad][@"mod_id"],
                                  adModArr[ad][@"mod_name"],
                                  adModArr[ad][@"mod_price"],
                                  adModArr[ad][@"mod_parent_id"],
                                  adModArr[ad][@"mod_parent_name"],
                                  [NSString stringWithFormat:@"%.2f",mod_amount], // since it was empty, now use for storing total
                                  adModArr[ad][@"mod_qty"],
                                  adModArr[ad][@"mod_item_id"],
                                  dict[@"company"]
                                  ];
                    
                    [array_of_query addObject:temp_query];
                }
                
                for (int ch = 0; ch < [chModArr count]; ch++)
                {
                    temp_query = [NSString stringWithFormat:@"INSERT INTO TRN_MODIFIERS "
                                  "(id,"
                                  "pom_pod_sn,"
                                  "pom_poh_sn,"
                                  "pom_type,"
                                  "pom_mod_id,"
                                  "pom_name,"
                                  "pom_price,"
                                  "pom_mod_parent_id,"
                                  "pom_mod_parent_name,"
                                  "pom_mod_amount,"
                                  "pom_mod_qty,"
                                  "pom_mod_item_id,"
                                  "pom_com_id"
                                  ") "
                                  @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@') ;",
                                  [self get_unique_id_from_db],
                                  unique_id_for_each_item,
                                  dict[@"trx_id"],
                                  CHOICE,
                                  chModArr[ch][@"mod_id"],
                                  chModArr[ch][@"mod_name"],
                                  chModArr[ch][@"mod_price"],
                                  chModArr[ch][@"mod_parent_id"],
                                  chModArr[ch][@"mod_parent_name"],
                                  chModArr[ch][@"mod_price"], // since choice will always be 1 qty
                                  chModArr[ch][@"mod_qty"] ? chModArr[ch][@"mod_qty"] : @"1",// since choice doesnt has qty put it as 1
                                  chModArr[ch][@"mod_item_id"],
                                  dict[@"company"]
                                  ];
                    
                    [array_of_query addObject:temp_query];
                }
            }
            
            [self add_to_queue:array_of_query with_priority:high];                
        }
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",[e reason]] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (CompleteOrder *)get_transaction_detail_by_transaction_number:(NSString *)transaction_number
{
    @try
    {
        NSString *main_query = [NSString stringWithFormat:@"SELECT "
                                "id,"
                                "poh_order_id,"
                                "poh_transaction_type,"
                                "poh_payment_method_id,"
                                "poh_pyt_name,"
                                "datetime(poh_close_datetime, 'unixepoch','localtime') as poh_close_datetime,"
                                "poh_gst_mode,"
                                "poh_total_gst_value,"
                                "poh_mem_screen_name,"
                                "poh_customer_gstid,"
                                "poh_customer_phone,"
                                "poh_customer_pincode,"
                                "poh_customer_address1,"
                                "poh_customer_address2,"
                                "poh_customer_email,"
                                "poh_customer_city,"
                                "poh_subtotal,"
                                "poh_total,"
                                "IFNULL((select sum(phr_amount) from REFUND where TRANSACTIONS.id = REFUND.phr_poh_sn),0) as total_refunded_amount,"
                                "poh_rounding_adj,"
                                "poh_currency,"
                                "poh_discount_percentage,"
                                "poh_discount_amount,"
                                "poh_tax_amount,"
                                "poh_table_number,"
                                "poh_discount_id,"
                                "poh_voucher_id,"
                                "poh_voucher_name,"
                                "poh_voucher_amount,"
                                "poh_voucher_type_id,"
                                "poh_voucher_type_name,"
                                "poh_voucher_member_number,"
                                "poh_voucher_ref_number,"
                                "poh_counter_number,"
                                "poh_trans_number,"
                                "poh_no_of_pax,"
                                "poh_sales_person_id,"
                                "poh_sales_person_name,"
                                "poh_order_type,"
                                "poh_card_number,"
                                "poh_payment_amount,"
                                "poh_payment_changes,"
                                "IFNULL((Select name from USERRIGHTS where USERRIGHTS.user_id = TRANSACTIONS.poh_usr_id),'') as created_by "
                                "FROM TRANSACTIONS "
                                "WHERE id       = '%@' "
                                "AND poh_com_id     = '%@' ",transaction_number,[StringUtils get_business_detail:@"business_id"]];
        
        NSString *item_query = [NSString stringWithFormat:@"SELECT "
                                "id,"
                                "pod_poh_sn,"
                                "pod_item_id,"
                                "pod_item, "
                                "pod_qty,"
                                "pod_uom_qty,"
                                "pod_selling_price,"
                                "pod_gst_value,"
                                "pod_remarks,"
                                "pod_discount_amount,"
                                "pod_discount_id,"
                                "pod_type_of_price,"
                                "pod_type_of_unit,"
                                "pod_gst_code,"
                                "pod_gst_rate,"
                                
                                "IFNULL("
                                "(select sum(phd_refund_item_qty) from REFUND_TRN_ITEMS  where REFUND_TRN_ITEMS.phd_poh_sn = TRN_ITEMS.pod_poh_sn and REFUND_TRN_ITEMS.phd_pod_sn = TRN_ITEMS.id), 0"
                                ") as refunded_item_qty,"
                                
                                "IFNULL("
                                "(select sum(phd_refund_item_price * phd_refund_item_qty) from REFUND_TRN_ITEMS  where REFUND_TRN_ITEMS.phd_poh_sn = TRN_ITEMS.pod_poh_sn and REFUND_TRN_ITEMS.phd_pod_sn = TRN_ITEMS.id), 0"
                                ") as refunded_item_price,"
                                
                                "pod_stock_monitoring"
                                " FROM TRN_ITEMS "
                                " WHERE pod_poh_sn = '%@' ",transaction_number];
        
        
        NSString *query                     = [NSString stringWithFormat:@"%@%@", item_query, @" ORDER BY pod_item_id"];
        
        CompleteOrder   *transaction_order  = [[CompleteOrder alloc]  init];
        NSMutableArray  *hold_order_array   = [[NSMutableArray alloc] init];
        
        NSArray *transaction_order_data = [self get_from_queue:main_query];
        
        NSLog(@" reprint data : %@ ", transaction_order_data);
        
        for (NSDictionary *transaction_data in transaction_order_data)
        {
            transaction_order.trans_number          = [NSString stringWithFormat:@"%@",transaction_data[@"poh_trans_number"]];
            transaction_order.trx_id                = [NSString stringWithFormat:@"%@",transaction_data[@"id"]];
            transaction_order.trx_order_id          = [NSString stringWithFormat:@"%@",transaction_data[@"poh_order_id"]];
            transaction_order.payment               = [NSString stringWithFormat:@"%@",transaction_data[@"poh_transaction_type"]];
            transaction_order.pay_type_name         = [NSString stringWithFormat:@"%@",transaction_data[@"poh_pyt_name"]];
            transaction_order.payment_method_id     = [NSString stringWithFormat:@"%@",transaction_data[@"poh_payment_method_id"]];
            transaction_order.create_time           = [NSString stringWithFormat:@"%@",transaction_data[@"poh_close_datetime"]];
            transaction_order.trx_gst_mode          = [NSString stringWithFormat:@"%@",transaction_data[@"poh_gst_mode"]];
            transaction_order.total_gst_value       = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_total_gst_value"]]];
            transaction_order.customer_name         = [NSString stringWithFormat:@"%@",transaction_data[@"poh_mem_screen_name"]];
            transaction_order.customer_gstid        = [NSString stringWithFormat:@"%@",transaction_data[@"poh_customer_gstid"]];
            transaction_order.customer_phone        = [NSString stringWithFormat:@"%@",transaction_data[@"poh_customer_phone"]];
            transaction_order.customer_pincode      = [NSString stringWithFormat:@"%@",transaction_data[@"poh_customer_pincode"]];
            transaction_order.customer_address1     = [NSString stringWithFormat:@"%@",transaction_data[@"poh_customer_address1"]];
            transaction_order.customer_address2     = [NSString stringWithFormat:@"%@",transaction_data[@"poh_customer_address2"]];
            transaction_order.customer_email        = [NSString stringWithFormat:@"%@",transaction_data[@"poh_customer_email"]];
            transaction_order.customer_city         = [NSString stringWithFormat:@"%@",transaction_data[@"poh_customer_city"]];
            transaction_order.total                 = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_total"]]];
            transaction_order.subtotal              = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_subtotal"]]];
            transaction_order.refund_total_amount   = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"total_refunded_amount"]]];
            transaction_order.currency              = [NSString stringWithFormat:@"%@",transaction_data[@"poh_currency"]];
            transaction_order.discount_amount       = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_discount_amount"]]];
            transaction_order.discount_percentage   = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_discount_percentage"]]];
            transaction_order.tax_amount            = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_tax_amount"]]];
            transaction_order.rounding_amount       = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_rounding_adj"]]];
            transaction_order.table_no              = [NSString stringWithFormat:@"%@",transaction_data[@"poh_table_number"]];
            transaction_order.isOrderOnHold         = YES;
            transaction_order.predefined_discount   = [NSString stringWithFormat:@"%@",transaction_data[@"poh_discount_id"]];
            transaction_order.counter_number        = [transaction_data[@"poh_counter_number"]intValue];
            transaction_order.voucher_id            = [NSString stringWithFormat:@"%@",transaction_data[@"poh_voucher_id"]];
            transaction_order.voucher_name          = [NSString stringWithFormat:@"%@",transaction_data[@"poh_voucher_name"]];
            transaction_order.voucher_amount        = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_voucher_amount"]]];
            transaction_order.voucher_type_id       = [NSString stringWithFormat:@"%@",transaction_data[@"poh_voucher_type_id"]];
            transaction_order.voucher_type_name     = [NSString stringWithFormat:@"%@",transaction_data[@"poh_voucher_type_name"]];
            transaction_order.voucher_member_number = [NSString stringWithFormat:@"%@",transaction_data[@"poh_voucher_member_number"]];
            transaction_order.voucher_ref_number    = [NSString stringWithFormat:@"%@",transaction_data[@"poh_voucher_ref_number"]];
            transaction_order.no_of_pax             = [transaction_data[@"poh_no_of_pax"] intValue];
            transaction_order.user_name             = [NSString stringWithFormat:@"%@",transaction_data[@"created_by"]];
            transaction_order.sales_person_id       = [NSString stringWithFormat:@"%@",transaction_data[@"poh_sales_person_id"]];
            transaction_order.sales_person_name     = [NSString stringWithFormat:@"%@",transaction_data[@"poh_sales_person_name"]];
            transaction_order.credit_card_no        = [NSString stringWithFormat:@"%@",transaction_data[@"poh_card_number"]];
            transaction_order.order_type            = [transaction_data[@"poh_order_type"] intValue];
            transaction_order.payment_amount        = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_payment_amount"]]];
            transaction_order.payment_changes       = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_data[@"poh_payment_changes"]]];
            
            transaction_order.from_sales_report = YES;
            
            //TODO: find another way to get the total_gst_value_bfr_bill_disc, subtotal_bfr_bill_disc -- for now this is a quick fix
            if (transaction_order.total_gst_value.doubleValue > 0) // has GST
            {
                transaction_order.subtotal_bfr_bill_disc = [transaction_order.subtotal decimalNumberByAdding:
                                                            [transaction_order.discount_amount decimalNumberByMultiplyingBy:[self nongst_perc_in_decimal]]];
                transaction_order.total_gst_value_bfr_bill_disc = [[transaction_order.total_gst_value decimalNumberBySubtracting:
                                                                    [transaction_order.tax_amount decimalNumberByMultiplyingBy:[self gst_perc_in_decimal]]]
                                                                   decimalNumberByAdding: [transaction_order.discount_amount decimalNumberByMultiplyingBy:[self gst_perc_in_decimal]]];
            }
            else
            {
                transaction_order.subtotal_bfr_bill_disc = [transaction_order.subtotal decimalNumberByAdding: transaction_order.discount_amount];
                transaction_order.total_gst_value_bfr_bill_disc = transaction_order.total_gst_value;
            }
            
            // need to get billDiscountPercentage to calculate and to re-print later
            NSDecimalNumber *billDiscountPercentage = [NSDecimalNumber decimalNumberWithString:@"0.0"];
            NSDecimalNumber *total_bfr_disc = [transaction_order.subtotal_bfr_bill_disc decimalNumberByAdding: transaction_order.total_gst_value_bfr_bill_disc];
            if (total_bfr_disc == NSDecimalNumber.zero) {
                billDiscountPercentage = [[NSDecimalNumber decimalNumberWithString:@"1.0"] decimalNumberBySubtracting:
                                          [transaction_order.discount_amount decimalNumberByDividingBy:
                                           total_bfr_disc]];
            }
            
            NSArray *return_item_data = [self get_from_queue:query];
            
            for (NSDictionary *transaction_item_data in return_item_data)
            {
                OrderItem *order_item           = [[OrderItem alloc] init];
                order_item.hold_item_id         = transaction_item_data[@"id"];
                order_item.itemID               = [NSString stringWithFormat:@"%@",transaction_item_data[@"pod_item_id"]];
                order_item.itemName             = [NSString stringWithFormat:@"%@",transaction_item_data[@"pod_item"]];
                order_item.itemQuantity         = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_item_data[@"pod_qty"]]];
                order_item.uom_qty              = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_item_data[@"pod_uom_qty"]]];
                order_item.refund_item_qty      = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_item_data[@"refunded_item_qty"]]];
                order_item.refund_item_price    = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_item_data[@"refunded_item_price"]]];
                order_item.itemPrice            = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_item_data[@"pod_selling_price"]]];
                order_item.remarks              = [NSString stringWithFormat:@"%@",transaction_item_data[@"pod_remarks"]];
                order_item.discount             = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_item_data[@"pod_discount_amount"]]];
                order_item.item_checked         = 0;
                order_item.predefined_discount  = [NSString stringWithFormat:@"%@",transaction_item_data[@"pod_discount_id"]];
                order_item.type_of_price        = [NSString stringWithFormat:@"%@",transaction_item_data[@"pod_type_of_price"]];
                order_item.type_of_unit         = [NSString stringWithFormat:@"%@",transaction_item_data[@"pod_type_of_unit"]];
                order_item.gst_code             = [NSString stringWithFormat:@"%@",transaction_item_data[@"pod_gst_code"]];
                order_item.gst_rate             = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_item_data[@"pod_gst_rate"]]];
                order_item.gst_value            = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%@",transaction_item_data[@"pod_gst_value"]]];
                order_item.stock_monitoring     = transaction_item_data[@"pod_stock_monitoring"];
                order_item.allModifiersDict     = [self get_all_transaction_modifiers:order_item.hold_item_id withModItemID:order_item.itemID];
                
                [hold_order_array               addObject:[order_item calculate_with_bill_disc:billDiscountPercentage]];
            }
            transaction_order.orderItemsArray          = [NSArray arrayWithArray:hold_order_array];
            
        }
        return transaction_order;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (NSMutableDictionary *)get_all_transaction_modifiers:(NSString *)trx_item_id withModItemID:(NSString *)mod_item_id
{
    @try
    {
        // Choice
        NSString *choice_query = [NSString stringWithFormat:@"select "
                                  "mod_parent_id,mod_parent_name, mod_type,mod_name,mod_price,mod_id,mod_item_id "
                                  " FROM MODIFIERS "
                                  " where mod_item_id = '%@'  AND active_status = 1 AND mod_type = 'CHOICE' "
                                  " group by mod_parent_id ", mod_item_id];
        
        NSMutableDictionary *choice_parent_modifier;
        NSMutableDictionary *child_modifier;
        NSMutableDictionary *modifiers_array    = [[NSMutableDictionary alloc] init];
        NSMutableArray      *choice_array       = [[NSMutableArray alloc] init];
        NSMutableArray      *addition_array     = [[NSMutableArray alloc] init];
        NSMutableArray      *child_array;
        NSString            *child_query;
        NSMutableString     *mod_id             = [[NSMutableString alloc] init];
        
        NSArray *return_choice_data = [self get_from_queue:choice_query];
        
        for (NSDictionary *choice_modifier_data in return_choice_data)
        {
            choice_parent_modifier = [[NSMutableDictionary alloc] init];
            choice_parent_modifier[@"mod_parent_id"] = choice_modifier_data[@"mod_parent_id"];
            choice_parent_modifier[@"mod_parent_name"] = choice_modifier_data[@"mod_parent_name"];
            choice_parent_modifier[@"mod_type"] = choice_modifier_data[@"mod_type"];
            
            child_array =   [[NSMutableArray alloc] init];
            child_query =   [
                             NSString stringWithFormat:@"SELECT "
                             "mod_id,mod_type,mod_name,mod_price,mod_parent_id,"
                             " mod_parent_name,mod_item_id "
                             " FROM MODIFIERS "
                             " where mod_parent_id = '%@' AND mod_item_id = '%@' AND active_status = 1 ",
                             choice_modifier_data[@"mod_parent_id"], mod_item_id
                             ];
            
            NSArray *return_child_data = [self get_from_queue:child_query];
            
            
            for (NSDictionary *child_modifier_data in return_child_data)
            {
                child_modifier = [[NSMutableDictionary alloc] init];
                child_modifier[@"mod_id"] = child_modifier_data[@"mod_id"];
                child_modifier[@"mod_name"] = child_modifier_data[@"mod_name"];
                child_modifier[@"mod_price"] = child_modifier_data[@"mod_price"];
                child_modifier[@"mod_item_id"] = child_modifier_data[@"mod_item_id"];
                child_modifier[@"mod_parent_id"] = child_modifier_data[@"mod_parent_id"];
                child_modifier[@"mod_parent_name"] = child_modifier_data[@"mod_parent_name"];
                child_modifier[@"mod_type"] = child_modifier_data[@"mod_type"];
                [mod_id appendFormat:@",%@", child_modifier_data[@"mod_id"]];                         // append modifier id to be used below
                [child_array addObject:child_modifier];
            }
            choice_parent_modifier[@"child_modifier"] = child_array;
            
            if(mod_id.length > 2)
            {
                NSDictionary *selected_choice = [self get_transaction_order_selected_choice   : [mod_id substringFromIndex:1]
                                                                                withItemId    : mod_item_id
                                                                              andTrx_item_id  : trx_item_id];
                
                if(selected_choice != nil)
                {
                    choice_parent_modifier[@"selected_choice"] = selected_choice;
                }
            }
            
            [choice_array addObject:choice_parent_modifier];
            [mod_id setString:@""];
        }
        
        // Addition
        NSString *addition_query = [NSString stringWithFormat:@"SELECT "
                                    "pom_mod_id, pom_name,pom_price, pom_mod_parent_id,"
                                    "pom_mod_parent_name, pom_mod_item_id, pom_mod_qty, pom_type "
                                    "FROM TRN_MODIFIERS "
                                    "WHERE TRN_MODIFIERS.pom_pod_sn = '%@' AND pom_mod_item_id = '%@' AND pom_type = 'ADDITION' "
                                    ,trx_item_id,mod_item_id];
        
        NSMutableDictionary *addition_parent_modifier;
        
        NSArray *return_addition_data = [self get_from_queue:addition_query];
        
        for (NSDictionary *addition_modifier_data in return_addition_data)
        {
            addition_parent_modifier = [[NSMutableDictionary alloc] init];
            addition_parent_modifier[@"mod_id"]         = addition_modifier_data[@"pom_mod_id"];
            addition_parent_modifier[@"mod_name"]       = addition_modifier_data[@"pom_name"];
            addition_parent_modifier[@"mod_price"]      = addition_modifier_data[@"pom_price"];
            addition_parent_modifier[@"mod_item_id"]    = addition_modifier_data[@"pom_mod_item_id"];
            addition_parent_modifier[@"mod_parent_id"]  = addition_modifier_data[@"pom_mod_parent_id"];
            addition_parent_modifier[@"mod_parent_name"] = addition_modifier_data[@"pom_mod_parent_name"];
            addition_parent_modifier[@"mod_type"]       = addition_modifier_data[@"pom_type"];
            addition_parent_modifier[@"mod_qty"]        = addition_modifier_data[@"pom_mod_qty"];
            [addition_array addObject:addition_parent_modifier];
        }
        
        if (choice_array.count > 0)
        {
            modifiers_array[CHOICE] = choice_array;
        }
        
        if (addition_array.count > 0)
        {
            modifiers_array[ADDITION] = addition_array;
        }
        
        return modifiers_array.count > 0 ? modifiers_array : nil;
        
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (NSMutableDictionary *)get_transaction_order_selected_choice:(NSString *)modifier_id withItemId:(NSString *)mod_item_id andTrx_item_id:(NSString *)trx_item_id
{
    @try
    {
        NSMutableArray      *selected_array;
        NSMutableDictionary *selected_modifier;
        selected_array      =   [[NSMutableArray alloc] init];
        NSString *query     =   [
                                 NSString stringWithFormat:@"SELECT "
                                 "pom_mod_id, pom_type, pom_name, pom_price, pom_mod_parent_id,"
                                 "pom_mod_parent_name, pom_mod_item_id, pom_mod_qty "
                                 "FROM TRN_MODIFIERS "
                                 "WHERE pom_mod_id in(%@) AND pom_mod_item_id = '%@' AND pom_pod_sn = '%@'",
                                 modifier_id,mod_item_id,trx_item_id
                                 ];
        
        NSArray *return_hub_data = [self get_from_queue:query];
        
        
        for (NSDictionary *modifier_data in return_hub_data)
        {
            selected_modifier = [[NSMutableDictionary alloc] init];
            selected_modifier[@"mod_id"] = modifier_data[@"pom_mod_id"];
            selected_modifier[@"mod_name"] = modifier_data[@"pom_name"];
            selected_modifier[@"mod_price"] = modifier_data[@"pom_price"];
            selected_modifier[@"mod_item_id"] = modifier_data[@"pom_mod_item_id"];
            selected_modifier[@"mod_parent_id"] = modifier_data[@"pom_mod_parent_id"];
            selected_modifier[@"mod_parent_name"] = modifier_data[@"pom_mod_parent_name"];
            selected_modifier[@"mod_type"] = modifier_data[@"pom_type"];
            selected_modifier[@"mod_qty"] = modifier_data[@"pom_mod_qty"] ? modifier_data[@"pom_mod_qty"] : @"0";
            
        }
        
        return selected_modifier;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

// TODO: deal with method below at the end, must have same dict key as refund one
- (NSArray *)getSalesReportFrom:(NSDate *)startShift to:(NSDate *)endShift withPaymentId:(NSString *)paymentId
{
    @try
    {
        NSString *paymentMethodStatement = [paymentId isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"AND TRANSACTIONS.poh_payment_method_id = '%@' ", paymentId];
        
        NSString *query = [NSString stringWithFormat:
                           @"SELECT TRANSACTIONS.id AS poh_sn , "
                           "TRANSACTIONS.poh_total - IFNULL((SELECT SUM(REFUND.phr_amount) FROM REFUND WHERE REFUND.phr_poh_sn = TRANSACTIONS.id),0) AS total ,"
                           "TRANSACTIONS.poh_trans_number AS trans_no ,"
                           "USERRIGHTS.name AS created_by , "
                           "datetime(TRANSACTIONS.poh_close_datetime,'unixepoch','localtime') AS created_datetime ,"
                           "PAYMENT_METHOD.pay_name AS payment_method, "
                           "CASE IFNULL(TRANSACTIONS.sync_id,'') WHEN '' THEN 'not_synced' ELSE 'synced' END sync_status "
                           "FROM TRANSACTIONS "
                           "LEFT JOIN USERRIGHTS ON USERRIGHTS.user_id = TRANSACTIONS.poh_usr_id "
                           "LEFT JOIN PAYMENT_METHOD ON PAYMENT_METHOD.id = TRANSACTIONS.poh_payment_method_id "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' %@ "
                           "AND datetime(TRANSACTIONS.poh_close_datetime,'unixepoch','localtime') BETWEEN '%@' AND '%@' "
                           "ORDER BY poh_close_datetime desc",
                           [StringUtils get_business_detail:@"business_id"],
                           paymentMethodStatement,
                           startShift.sql_string,
                           endShift.sql_string];
        
        NSLog(@"%@",query);
        
        NSArray *report_data = [self get_from_queue:query];
        
        return report_data;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Method use for 'Daily Sales Report Print Out'
#pragma mark Total of Bill Discount
- (NSArray *)get_total_discount_today
{
    return [self get_total_discount_date:[NSDate date]];
}

- (NSArray *)get_total_discount_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_total_discount_today_from:dates.firstObject to:dates.lastObject];
}

- (NSArray *)get_total_discount_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(sum(poh_discount_amount),0) - IFNULL((SELECT sum(phr_discount_amount) FROM REFUND WHERE strftime(datetime(REFUND.phr_create_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ),0) AS discount,"
                           "COUNT(*) as qty "
                           "FROM TRANSACTIONS WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "AND TRANSACTIONS.poh_discount_amount > 0",
                           shift_start.sql_string,
                           shift_end.sql_string,
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSMutableDictionary *discounts;
        NSMutableArray      *disc_array  = [[NSMutableArray alloc] init];
        NSArray         *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            discounts = [[NSMutableDictionary alloc] init];
            discounts[@"discount"] = dict[@"discount"];
            discounts[@"qty"] = dict[@"qty"];
            [disc_array addObject:discounts];
        }
        
        return disc_array;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Breakdown of Bill Discount
- (NSArray *)get_discount_given_on_bill_today
{
    return [self get_discount_given_on_bill_date:[NSDate date]];
}

- (NSArray *)get_discount_given_on_bill_date:(NSDate*)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_discount_given_on_bill_today_from:dates.firstObject to:dates.lastObject];
}

- (NSArray *)get_discount_given_on_bill_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(DISCOUNT.discount_desc,'Others') as discount_desc,"
                           "COUNT(*) as discount_qty,"
                           "IFNULL(sum(poh_discount_amount),0) - IFNULL(sum(REFUND.phr_discount_amount),0) as total_discount "
                           "FROM TRANSACTIONS "
                           "LEFT JOIN DISCOUNT ON TRANSACTIONS.poh_discount_id = DISCOUNT.id "
                           "LEFT JOIN REFUND ON REFUND.phr_poh_sn = TRANSACTIONS.id "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "AND TRANSACTIONS.poh_discount_amount > 0 "
                           "group by DISCOUNT.discount_desc ",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string
                           ];
        
        NSLog(@"%@",query);
        NSMutableDictionary *discounts;
        NSMutableArray      *disc_array  = [[NSMutableArray alloc] init];
        NSArray         *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            discounts = [[NSMutableDictionary alloc] init];
            if ([dict[@"total_discount"] doubleValue] > 0.0)
            {
                discounts[@"discount_desc"] = dict[@"discount_desc"];
                discounts[@"total_discount"] = dict[@"total_discount"];
                discounts[@"discount_qty"] = dict[@"discount_qty"];
                [disc_array addObject:discounts];
            }
        }
        
        return disc_array;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Total of Item Discount
- (NSArray *)get_total_discount_item_today
{
    return [self get_total_discount_item_date:[NSDate date]];
}

- (NSArray *)get_total_discount_item_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_total_discount_item_today_from:dates.firstObject to:dates.lastObject];
}

- (NSArray *)get_total_discount_item_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(sum(pod_discount_amount * pod_qty),0) - IFNULL((SELECT sum(REFUND_TRN_ITEMS.phd_item_discount * REFUND_TRN_ITEMS.phd_refund_item_qty) FROM REFUND "
                           "INNER JOIN REFUND_TRN_ITEMS ON REFUND.phr_poh_sn = REFUND_TRN_ITEMS.phd_pod_sn "
                           "WHERE strftime(datetime(REFUND.phr_create_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ),0) "
                           "AS discount , "
                           "COUNT(*) as qty "
                           "FROM TRANSACTIONS INNER JOIN TRN_ITEMS on TRANSACTIONS.id = TRN_ITEMS.pod_poh_sn "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "AND TRN_ITEMS.pod_discount_amount > 0 ",
                           shift_start.sql_string,
                           shift_end.sql_string,
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSMutableDictionary *discounts;
        NSMutableArray      *disc_array  = [[NSMutableArray alloc] init];
        NSArray         *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            discounts = [[NSMutableDictionary alloc] init];
            discounts[@"discount"] = dict[@"discount"];
            discounts[@"qty"] = dict[@"qty"];
            [disc_array addObject:discounts];
        }
        
        return disc_array;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Breakdown of Item Discount
- (NSArray *)get_discount_given_on_item_today
{
    return [self get_discount_given_on_item_date:[NSDate date]];
}

- (NSArray *)get_discount_given_on_item_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_discount_given_on_item_today_from:dates.firstObject to:dates.lastObject];
}

- (NSArray *)get_discount_given_on_item_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(DISCOUNT.discount_desc,'Others') as discount_desc,"
                           "COUNT(*) as discount_qty,"
                           "(IFNULL(sum(TRN_ITEMS.pod_discount_amount * TRN_ITEMS.pod_qty),0) - IFNULL(sum(REFUND_TRN_ITEMS.phd_item_discount * REFUND_TRN_ITEMS.phd_refund_item_qty),0))  as total_discount "
                           "FROM TRN_ITEMS "
                           "LEFT JOIN DISCOUNT on TRN_ITEMS.pod_discount_id = DISCOUNT.id "
                           "LEFT JOIN TRANSACTIONS ON TRN_ITEMS.pod_poh_sn = TRANSACTIONS.id "
                           "LEFT JOIN REFUND_TRN_ITEMS ON REFUND_TRN_ITEMS.phd_poh_sn = TRN_ITEMS.pod_poh_sn "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "AND TRN_ITEMS.pod_discount_amount > 0 "
                           "group by DISCOUNT.discount_desc ",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string
                           ];
        
        NSLog(@"%@",query);
        NSMutableDictionary *discounts;
        NSMutableArray      *disc_array  = [[NSMutableArray alloc] init];
        NSArray         *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            discounts = [[NSMutableDictionary alloc] init];
            discounts[@"discount_desc"] = dict[@"discount_desc"];
            discounts[@"total_discount"] = dict[@"total_discount"];
            discounts[@"discount_qty"] = dict[@"discount_qty"];
            [disc_array addObject:discounts];
        }
        
        return disc_array;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Breakdown of Voucher
- (NSArray*)get_total_voucher_today
{
    return [self get_total_voucher_date:[NSDate date]];
}

- (NSArray*)get_total_voucher_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_total_voucher_today_from:dates.firstObject to:dates.lastObject];
}

- (NSArray*)get_total_voucher_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(SUM(poh_voucher_amount),0) - IFNULL((SELECT SUM(phr_voucher_amount) FROM REFUND WHERE strftime(datetime(REFUND.phr_create_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@'),0) AS total_amount , "
                           "COUNT(*) as qty "
                           "FROM TRANSACTIONS WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND TRANSACTIONS.poh_voucher_amount != '' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ",
                           shift_start.sql_string,
                           shift_end.sql_string,
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSLog(@"%@",query);
        
        NSMutableDictionary *voucher;
        NSMutableArray      *vouc_array  = [[NSMutableArray alloc] init];
        NSArray         *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            voucher = [[NSMutableDictionary alloc] init];
            voucher[@"total_amount"] = dict[@"total_amount"];
            voucher[@"qty"] = dict[@"qty"];
            voucher[@"vouchers"] = [self get_voucher_group_today_from:shift_start to:shift_end ];
            [vouc_array addObject:voucher];
        }
        
        return vouc_array;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        
    }
}

- (NSArray*)get_voucher_group_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT poh_voucher_name, "
                           "IFNULL(SUM(poh_voucher_amount),0) AS amount, "
                           "COUNT(*) as qty "
                           "FROM TRANSACTIONS "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND TRANSACTIONS.poh_voucher_id != '' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "GROUP BY TRANSACTIONS.poh_voucher_id ",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSLog(@"%@",query);
        
        NSMutableDictionary *voucher;
        NSMutableArray      *vouc_array  = [[NSMutableArray alloc] init];
        NSArray         *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            voucher = [[NSMutableDictionary alloc] init];
            voucher[@"voucher_name"] = dict[@"poh_voucher_name"];
            voucher[@"amount"] = dict[@"amount"];
            voucher[@"qty"] = dict[@"qty"];
            [vouc_array addObject:voucher];
        }
        
        return vouc_array;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        
    }
}

#pragma mark Breakdown of Categories & Item Sales
- (NSArray *)get_category_group_sales_today
{
    return [self get_category_group_sales_date:[NSDate date]];
}

- (NSArray *)get_category_group_sales_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_category_group_sales_today_from:dates.firstObject to:dates.lastObject];
}

- (NSArray *)get_category_group_sales_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try{
        NSString *query = [NSString stringWithFormat:
                           @"SELECT CATEGORIES.category_name,"
                           "CATEGORIES.category_id,"
                           "sum(TRN_ITEMS.pod_qty) as qty, "
                           "sum(TRN_ITEMS.pod_price) as total "
                           "FROM TRANSACTIONS "
                           "INNER JOIN TRN_ITEMS on TRANSACTIONS.id = TRN_ITEMS.pod_poh_sn "
                           "INNER JOIN ITEMS ON TRN_ITEMS.pod_item_id = ITEMS.item_id "
                           "INNER JOIN CATEGORIES ON ITEMS.item_cat_id = CATEGORIES.category_id "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "GROUP BY CATEGORIES.category_name ORDER BY qty DESC",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSMutableDictionary *categories;
        NSMutableArray      *ctg_array  = [[NSMutableArray alloc] init];
        
        NSArray *category_item_data = [self get_from_queue:query];
        
        for (NSDictionary *data in category_item_data)
        {
            categories = [[NSMutableDictionary alloc] init];
            NSDictionary *refund_data = [self get_total_refund_cat_today:data[@"category_id"] by_date_from:shift_start until:shift_end];
            categories[@"category_name"] = data[@"category_name"];
            categories[@"qty"] = data[@"qty"];
            categories[@"total"] = data[@"total"];
            categories[@"refund_qty"] = refund_data[@"refund_qty"];
            categories[@"refund_total"] = refund_data[@"refund_total"];
            categories[@"items"] = [self get_category_item_sales_with_catId:data[@"category_id"] from:shift_start to:shift_end];
            [ctg_array addObject:categories];
        }
        
        return ctg_array;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray *)get_category_item_sales_with_catId:(NSString *)cat_id from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT ITEMS.item_id,"
                           "ITEMS.item_name,"
                           "SUM(TRN_ITEMS.pod_qty) as qty, "
                           "SUM(TRN_ITEMS.pod_price) as total "
                           "FROM TRANSACTIONS "
                           "INNER JOIN TRN_ITEMS on TRANSACTIONS.id = TRN_ITEMS.pod_poh_sn "
                           "INNER JOIN ITEMS ON TRN_ITEMS.pod_item_id = ITEMS.item_id "
                           "INNER JOIN CATEGORIES ON ITEMS.item_cat_id = CATEGORIES.category_id "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND CATEGORIES.category_id = '%@' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "GROUP BY ITEMS.item_name ORDER BY qty DESC "
                           ,[StringUtils get_business_detail:@"business_id"],cat_id,
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSLog(@"%@",query);
        NSMutableDictionary *categories;
        NSMutableArray      *ctg_array  = [[NSMutableArray alloc] init];
        NSArray *category_item_data = [self get_from_queue:query];
        
        for (NSDictionary *data in category_item_data)
        {
            categories = [[NSMutableDictionary alloc] init];
            NSDictionary *refund_data = [self get_total_refund_item_today:data[@"item_id"] by_date_from:shift_start until:shift_end];
            categories[@"item_name"] = data[@"item_name"];
            categories[@"qty"] = data[@"qty"];
            categories[@"total"] = data[@"total"];
            categories[@"refund_qty"] = refund_data[@"refund_qty"];
            categories[@"refund_total"] = refund_data[@"refund_total"];
            categories[@"choice"] = [self get_category_item_choice_with_itemId:data[@"item_id"] from:shift_start to:shift_end];
            [ctg_array addObject:categories];
        }
        
        NSLog(@"trans ac : %@",ctg_array);
        
        return ctg_array;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray *)get_category_item_choice_with_itemId:(NSString *)item_id from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT TRN_MODIFIERS.pom_mod_id,"
                           "TRN_MODIFIERS.pom_name as item_name,"
                           "sum(TRN_ITEMS.pod_qty) as qty,"
                           "sum(TRN_ITEMS.pod_price) as total "
                           "FROM TRANSACTIONS "
                           "INNER JOIN TRN_ITEMS on TRANSACTIONS.id = TRN_ITEMS.pod_poh_sn "
                           "INNER JOIN TRN_MODIFIERS on TRN_MODIFIERS.pom_pod_sn = TRN_ITEMS.id "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND TRN_MODIFIERS.pom_mod_item_id = '%@' "
                           "AND TRN_MODIFIERS.pom_type = '%@' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "GROUP BY TRN_MODIFIERS.pom_mod_id "
                           "ORDER BY qty DESC "
                           ,[StringUtils get_business_detail:@"business_id"],
                           item_id,CHOICE,
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSLog(@"%@",query);
        NSMutableDictionary *categories;
        NSMutableArray      *ctg_array  = [[NSMutableArray alloc] init];
        NSArray *category_item_data = [self get_from_queue:query];
        
        for (NSDictionary *data in category_item_data)
        {
            categories = [[NSMutableDictionary alloc] init];
            NSDictionary *refund_data = [self get_total_refund_item_choice_today:data[@"pom_mod_id"] by_date_from:shift_start until:shift_end];
            categories[@"item_name"] = data[@"item_name"];
            categories[@"qty"] = data[@"qty"];
            categories[@"total"] = data[@"total"];
            categories[@"refund_qty"] = refund_data[@"refund_qty"];
            categories[@"refund_total"] = refund_data[@"refund_total"];
            [ctg_array addObject:categories];
        }
        
        NSLog(@"trans ch : %@",ctg_array);
        return ctg_array;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Total of Tax
- (NSString *)get_total_tax_today
{
    return [self get_total_tax_date:[NSDate date]];
}

- (NSString *)get_total_tax_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_total_tax_today_from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_total_tax_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *cash_amount = @"0.0";
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(sum(poh_tax_amount),0) AS total_tax "
                           "FROM TRANSACTIONS "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        
        NSLog(@"%@",query);
        NSArray *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            cash_amount = dict[@"total_tax"];
        }
        
        return cash_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Total of GST and SST
- (NSString *)get_total_gst_sst_today
{
    return [self get_total_gst_sst_date:[NSDate date]];
}

- (NSString *)get_total_gst_sst_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_total_gst_sst_today_from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_total_gst_sst_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *gst_amount = @"0.0";
        
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(sum(poh_total_gst_value),0) as total_gst "
                           "FROM TRANSACTIONS "
                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSLog(@"%@",query);
        NSArray *resultSet   = [self get_from_queue:query];
        
        for(NSDictionary *dict in resultSet)
        {
            gst_amount = dict[@"total_gst"];
        }
        
        return gst_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Total of SST
- (NSString *)get_total_sst_today
{
    return [self get_total_sst_date:[NSDate date]];
}

- (NSString *)get_total_sst_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_total_sst_today_from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_total_sst_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *gst_amount = @"0.0";

        // recheck condition if needed
//        NSString *query = [NSString stringWithFormat:
//                           @"SELECT IFNULL(sum(poh_total_gst_value),0) as total_gst "
//                           "FROM TRANSACTIONS "
//                           "WHERE TRANSACTIONS.poh_com_id = '%@' "
//                           "AND poh_gst_mode = 'trx_sst_en' "
//                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ",
//                           [StringUtils get_business_detail:@"business_id"],
//                           shift_start.sql_string,
//                           shift_end.sql_string];
//
//        NSLog(@"%@",query);
//        NSArray *resultSet   = [self get_from_queue:query];
//
//        for(NSDictionary *dict in resultSet)
//        {
//            gst_amount = dict[@"total_gst"];
//        }

        return gst_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Total of Cash & Card Sales
- (NSString *)get_cash_sales_today:(BOOL)cash
{
    return [self get_cash_sales:cash with_date:[NSDate date]];
}

- (NSString *)get_cash_sales:(BOOL)cash with_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_cash_sales:cash from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_cash_sales:(BOOL)cash from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *cash_amount = @"0.0";
        NSString *main_query = @"SELECT IFNULL(sum(poh_total),0) as total_cash from TRANSACTIONS ";
        NSString *filter = cash ? @"WHERE TRANSACTIONS.poh_transaction_type = 1" : @"WHERE TRANSACTIONS.poh_transaction_type in(2,3,4)";
        
        NSString *query = [NSString stringWithFormat:
                           @"%@ %@ "
                           "AND strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch', 'localtime')) BETWEEN '%@' AND '%@' "
                           "AND TRANSACTIONS.poh_com_id = '%@' ",
                           main_query, filter,
                           shift_start.sql_string,
                           shift_end.sql_string,
                           [StringUtils get_business_detail:@"business_id"]];
        
        NSLog(@"%@",query);
        
        NSArray *resultSet   = [self get_from_queue:query];
        for(NSDictionary *dict in resultSet)
        {
            cash_amount = dict[@"total_cash"];
        }
        
        cash_amount = [NSString stringWithFormat:@"%f",(cash_amount.doubleValue + [self get_multi_cash_sales:cash from:shift_start to:shift_end].doubleValue) ];
        
        return cash_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSString *)get_multi_cash_sales:(BOOL)cash from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *cash_amount = @"0.0";
        NSString *main_query = @"SELECT IFNULL(sum(phm_current_paid_amount),0) as total_cash from TRANSACTION_MULTIPLE_PAYMENTS ";
        NSString *filter = cash ? @"WHERE TRANSACTION_MULTIPLE_PAYMENTS.phm_payment_type_id = 1" : @"WHERE TRANSACTION_MULTIPLE_PAYMENTS.phm_payment_type_id in(2,3,4)";
        
        NSString *query = [NSString stringWithFormat:
                           @"%@ %@ "
                           "AND strftime(datetime(TRANSACTION_MULTIPLE_PAYMENTS.phm_created_date, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' "
                           "AND TRANSACTION_MULTIPLE_PAYMENTS.phm_com_id = '%@'",
                           main_query, filter,
                           shift_start.sql_string,
                           shift_end.sql_string,
                           [StringUtils get_business_detail:@"business_id"]];
        
        NSLog(@"%@",query);
        
        NSArray *resultSet   = [self get_from_queue:query];
        for(NSDictionary *dict in resultSet)
        {
            cash_amount = dict[@"total_cash"];
        }
        
        return cash_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark BreakDown of Payment Type
- (NSArray*)get_total_payment_today
{
    return [self get_total_payment_date:[NSDate date]];
}

- (NSArray*)get_total_payment_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_total_payment_today_from:dates.firstObject to:dates.lastObject];
}

- (NSArray*)get_total_payment_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *total_query = [NSString stringWithFormat:
                                 @"SELECT IFNULL(SUM(TRANSACTIONS.poh_total),0) - IFNULL((SELECT SUM(REFUND.phr_amount) FROM REFUND WHERE strftime(datetime(REFUND.phr_create_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@'),0) AS total_amount "
                                 "FROM TRANSACTIONS "
                                 "WHERE TRANSACTIONS.poh_com_id = '%@' AND "
                                 "strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch', 'localtime')) BETWEEN '%@' AND '%@' ",
                                 shift_start.sql_string,
                                 shift_end.sql_string,
                                 [StringUtils get_business_detail:@"business_id"],
                                 shift_start.sql_string,
                                 shift_end.sql_string];
        
        NSMutableDictionary *payment;
        NSMutableArray      *payment_arr = [[NSMutableArray alloc] init];
        NSArray             *resultSet   = [self get_from_queue:total_query];
        
        for(NSDictionary *dict in resultSet) // will only loop 1 time
        {
            payment = [[NSMutableDictionary alloc] init];
            payment[@"total_amount"] = dict[@"total_amount"];
            payment[@"payments"] = [self get_payment_group_today_from:shift_start to:shift_end ];
            [payment_arr addObject:payment];
        }
        
        return payment_arr;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSArray*)get_payment_group_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        // all transaction
        NSString *payment_query = [NSString stringWithFormat:
                                   @"SELECT PAYMENT_METHOD.id, "
                                   "PAYMENT_METHOD.pay_name, "
                                   "IFNULL(sum(TRANSACTIONS.poh_total),0) as total "
                                   "FROM TRANSACTIONS INNER JOIN PAYMENT_METHOD ON "
                                   "TRANSACTIONS.poh_payment_method_id = PAYMENT_METHOD.id "
                                   "WHERE strftime(datetime(TRANSACTIONS.poh_close_datetime, 'unixepoch', 'localtime')) BETWEEN '%@' AND '%@' "
                                   "AND TRANSACTIONS.poh_com_id = '%@' "
                                   //"AND TRANSACTIONS.poh_transaction_type != 99 " //dont select multiple payment
                                   "GROUP BY TRANSACTIONS.poh_payment_method_id ",
                                   shift_start.sql_string,
                                   shift_end.sql_string,
                                   [StringUtils get_business_detail:@"business_id"]];
        
        NSArray *payment_result = [self get_from_queue:payment_query];

        return payment_result;
        
//        // all multiple transaction
//        NSString *multi_payment_query = [NSString stringWithFormat:
//                                         @"SELECT "
//                                         "(SELECT PAYMENT_METHOD.id FROM PAYMENT_METHOD WHERE PAYMENT_METHOD.id = TRANSACTION_MULTIPLE_PAYMENTS.phm_pay_method_id ) AS id, "
//                                         "(SELECT PAYMENT_METHOD.pay_name FROM PAYMENT_METHOD WHERE PAYMENT_METHOD.id = TRANSACTION_MULTIPLE_PAYMENTS.phm_pay_method_id ) AS pay_name,"
//                                         "IFNULL(sum(TRANSACTION_MULTIPLE_PAYMENTS.phm_current_paid_amount),0) as total "
//                                         "FROM TRANSACTION_MULTIPLE_PAYMENTS "
//                                         "INNER JOIN TRANSACTIONS ON TRANSACTIONS.id = TRANSACTION_MULTIPLE_PAYMENTS.phm_poh_sn "
//                                         "WHERE strftime(datetime(TRANSACTIONS.poh_create_datetime, 'unixepoch', 'localtime')) BETWEEN '%@' AND '%@' "
//                                         "AND TRANSACTIONS.poh_com_id = '%@' "
//                                         "GROUP BY TRANSACTION_MULTIPLE_PAYMENTS.phm_pay_method_id ",
//                                         shift_start.sql_string,
//                                         shift_end.sql_string,
//                                         [StringUtils get_business_detail:@"business_id"]];
//
//        NSArray *multi_payment_result = [self get_from_queue:multi_payment_query];
//
//
//        NSMutableDictionary *temp_dict;
//        NSMutableArray      *payment_arr = [NSMutableArray arrayWithArray:payment_result];
//        NSArray *all_pay_id_in_payment_result = [payment_result valueForKeyPath:@"id"];
//
//        for(NSDictionary *multi_dict in multi_payment_result)
//        {
//            if([all_pay_id_in_payment_result containsObject:multi_dict[@"id"]])
//            {
//                NSInteger index = [all_pay_id_in_payment_result indexOfObject:multi_dict[@"id"]];
//                NSDictionary *dict = payment_result[index];
//
//                //update
//                temp_dict = [[NSMutableDictionary alloc] init];
//                temp_dict[@"id"] = dict[@"id"];
//                temp_dict[@"pay_name"] = dict[@"pay_name"];
//                temp_dict[@"total"] = [NSString stringWithFormat:@"%.2f",[dict[@"total"] doubleValue] + [multi_dict[@"total"] doubleValue]];
//
//                payment_arr[index] = temp_dict; //replace
//            }
//            else
//            {
//                [payment_arr addObject:multi_dict]; //append
//            }
//        }
//        return payment_arr;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}


#pragma mark - For Customer transaction data
- (NSArray *)getTransactionBasedOnCustId:(NSString *)custId
{
    @try
    {
        // extra statement is for walk in 
        NSString *extraStatement = ([custId isEqualToString:@"0"]) ? @"OR poh_cus_id = '' OR poh_cus_id = '(null)'" : @"";
        NSString *query = [NSString stringWithFormat:
                           @"SELECT id, "
                           "datetime(TRANSACTIONS.poh_close_datetime,'unixepoch','localtime') AS created_datetime,"
                           "TRANSACTIONS.poh_total AS total,"
                           "TRANSACTIONS.poh_trans_number AS trans_no "
                           "FROM TRANSACTIONS "
                           "WHERE poh_cus_id = '%@' %@ "
                           "ORDER BY datetime(TRANSACTIONS.poh_close_datetime,'unixepoch','localtime') DESC "
                           "LIMIT 5 ",
                           custId,extraStatement];
        
        NSArray *dataArr = [self get_from_queue:query];
        return dataArr;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - Update Customer feedback
- (void)update_customer_feedback:(NSString *)trx_id rating:(NSString *)rate
{
    NSString *query   = [NSString stringWithFormat:
                         @"UPDATE TRANSACTIONS set "
                         "poh_customer_review = '%@', "
                         "sync_id = null "
                         "WHERE id = '%@'",
                         rate,trx_id];
    
    [self add_to_queue:query];
    
    NSLog(@"Latest trx_id : %@", trx_id);
}

@end
