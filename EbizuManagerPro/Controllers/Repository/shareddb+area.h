//
//  shareddb+area.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (area)

- (NSArray *)get_list_of_areas;
- (void)updateArea:(NSArray *)data;

@end
