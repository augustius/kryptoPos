//
//  shareddb+voucher.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (voucher)

- (NSArray *)get_all_voucher;
- (void)edit_manis_voucher:(NSArray *)data;

@end
