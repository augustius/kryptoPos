//
//  shareddb+refund.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 27/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+refund.h"
#import "KryptoPOS-Swift.h"

@implementation shareddb (refund)

#pragma mark - method use for 'Refund Report'
- (NSArray *)getRefundReportFrom:(NSDate *)shiftStart to:(NSDate *)shiftEnd withPaymentId:(NSString *)paymentId
{
    @try
    {
        NSString *paymentMethodStatement = [paymentId isEqualToString:@""] ? @"" : [NSString stringWithFormat:@"AND poh_payment_method_id = '%@'", paymentId];
        
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(TRANSACTIONS.id,'') AS poh_sn,"
                           "REFUND.phr_amount AS total , "
                           "TRANSACTIONS.poh_trans_number AS trans_no ,"
                           "USERRIGHTS.name AS created_by ,"
                           "datetime(REFUND.phr_create_datetime,'unixepoch','localtime') AS created_datetime , "
                           "PAYMENT_METHOD.pay_name AS payment_method, "
                           "CASE IFNULL(REFUND.sync_id,'') WHEN '' THEN 'not_synced' ELSE 'synced' END sync_status "
                           "FROM REFUND "
                           "LEFT JOIN TRANSACTIONS ON TRANSACTIONS.id = REFUND.phr_poh_sn "
                           "LEFT JOIN USERRIGHTS ON USERRIGHTS.user_id = REFUND.phr_updated_by "
                           "LEFT JOIN PAYMENT_METHOD ON TRANSACTIONS.poh_payment_method_id = PAYMENT_METHOD.id "
                           "WHERE phr_com_id = '%@' %@ "
                           "AND datetime(REFUND.phr_create_datetime,'unixepoch','localtime') between '%@' AND '%@' "
                           "ORDER BY created_datetime desc ",
                           [StringUtils get_business_detail:@"business_id"],
                           paymentMethodStatement,
                           shiftStart.sql_string,
                           shiftEnd.sql_string];
        
        NSArray *report_data  = [self get_from_queue:query];
        
        return report_data;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark - method use for 'Insert Refund Data'
- (void)refund_transaction:(CompleteOrder *)data
{
    @try
    {
        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
        NSString *execute_query= @"" ;
        NSString *refund_id = [self get_unique_id_from_db];
        
        
        execute_query = [NSString stringWithFormat:@"INSERT INTO REFUND "
                         "(id,"
                         "phr_amount,"
                         "phr_total_amount,"
                         "phr_rounding_amount,"
                         "phr_tax_amount,"
                         "phr_gst_amount,"
                         "phr_discount_amount,"
                         "phr_custom_discount_id,"
                         "phr_voucher_amount,"
                         "phr_voucher_id,"
                         "phr_no_of_pax,"
                         "phr_poh_sn,"
                         "phr_order_id,"
                         "phr_com_id,"
                         "phr_create_datetime,"
                         "phr_updated_by,"
                         "phr_created_by"
                         ") "
                         @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                         refund_id,
                         data.total,
                         data.total,
                         data.rounding_amount,
                         data.tax_amount,
                         data.total_gst_value,
                         data.discount_amount,
                         data.predefined_discount,
                         data.voucher_amount,
                         data.voucher_id,
                         [NSString stringWithFormat:@"%i",data.no_of_pax],
                         data.trx_id,
                         data.trans_number,
                         [StringUtils get_business_detail:@"business_id"],
                         CURRENT_TIMESTAMP,
                         [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                         [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID]
                         ];
        
        [array_of_query addObject:execute_query];
        
        
        if(data.orderItemsArray.count > 0)
        {
            for(OrderItem *item in data.orderItemsArray)
            {
                NSString *refund_trn_id = [self get_unique_id_from_db];
                execute_query = [NSString stringWithFormat:@"INSERT INTO REFUND_TRN_ITEMS "
                                 "(id,"
                                 "phd_phr_id,"
                                 "phd_poh_sn,"
                                 "phd_item_id,"
                                 "phd_pod_sn,"
                                 "phd_item_name,"
                                 "phd_item_qty,"
                                 "phd_refund_item_qty,"
                                 "phd_item_price,"
                                 "phd_refund_item_price,"
                                 "phd_gst_code,"
                                 "phd_gst_rate,"
                                 "phd_gst_value,"
                                 "phd_refund_gst_value,"
                                 "phd_created_date,"
                                 "phd_item_remarks,"
                                 "phd_item_discount,"
                                 "phd_stock_monitoring,"
                                 "phd_updated_by,"
                                 "phd_com_id"
                                 ")"
                                 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                                 refund_trn_id,
                                 refund_id,
                                 data.trx_id,
                                 item.itemID, //[data.orderItemsArray[i] valueForKey:@"itemID"],
                                 item.hold_item_id, //[data.orderItemsArray[i] valueForKey:@"hold_item_id"],
                                 item.itemName, //[data.orderItemsArray[i] valueForKey:@"itemName"],
                                 item.itemQuantity,//[data.orderItemsArray[i] valueForKey:@"itemQuantity"],
                                 item.itemQuantity, //[NSString stringWithFormat:@"%.2f",refund_item_qty],
                                 item.itemPrice, //[data.orderItemsArray[i] valueForKey:@"itemPrice"],
                                 item.itemPrice, //[data.orderItemsArray[i] valueForKey:@"refund_item_price"],
                                 item.gst_code, //[data.orderItemsArray[i] valueForKey:@"gst_code"],
                                 item.gst_rate, //[data.orderItemsArray[i] valueForKey:@"gst_rate"],
                                 item.gst_value, //[data.orderItemsArray[i] valueForKey:@"gst_value"],
                                 item.gst_value, //[data.orderItemsArray[i] valueForKey:@"refund_gst_value"],
                                 CURRENT_TIMESTAMP,
                                 item.remarks, //[data.orderItemsArray[i] valueForKey:@"remarks"],
                                 item.discount, //[data.orderItemsArray[i] valueForKey:@"discount"],
                                 item.stock_monitoring, //[data.orderItemsArray[i] valueForKey:@"stock_monitoring"],
                                 [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                                 [StringUtils get_business_detail:@"business_id"]];
                
                [array_of_query addObject:execute_query];
                
                NSArray *addition_array = item.allModifiersDict[@"ADDITION"];
                NSArray *choice_array   = item.allModifiersDict[@"CHOICE"];
                
                for (int x = 0; x < addition_array.count; x++)
                {
                    float mod_amount = [addition_array[x][@"mod_qty"] intValue] * [addition_array[x][@"mod_price"] floatValue];
                    
                    /**** Append REFUND Modifiers - ADDITION VALUES as a query ****/
                    
                    execute_query = [NSString stringWithFormat:
                                     @"INSERT INTO REFUND_TRN_MODIFIERS "
                                     "(id,"
                                     "prm_phr_id,"
                                     "prm_phd_id,"
                                     "prm_poh_sn,"
                                     "prm_pod_sn,"
                                     "prm_mod_type,"
                                     "prm_mod_id,"
                                     "prm_mod_name,"
                                     "prm_mod_price,"
                                     "prm_mod_parent_id,"
                                     "prm_parent_name,"
                                     "prm_mod_amount,"
                                     "prm_mod_item_id,"
                                     "prm_mod_qty,"
                                     "prm_created_datetime,"
                                     "prm_updated_by,"
                                     "prm_com_id)"
                                     @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                                     [self get_unique_id_from_db],
                                     refund_id,
                                     refund_trn_id,
                                     data.trx_id,
                                     item.hold_item_id, //[data.orderItemsArray[i] valueForKey:@"hold_item_id"],
                                     [addition_array[x] valueForKey:@"mod_type"],
                                     [addition_array[x] valueForKey:@"mod_id"],
                                     [addition_array[x] valueForKey:@"mod_name"],
                                     [addition_array[x] valueForKey:@"mod_price"],
                                     [addition_array[x] valueForKey:@"mod_parent_id"],
                                     [addition_array[x] valueForKey:@"mod_parent_name"],
                                     [NSString stringWithFormat:@"%.2f",mod_amount],
                                     [addition_array[x] valueForKey:@"mod_item_id"],
                                     [addition_array[x] valueForKey:@"mod_qty"],
                                     CURRENT_TIMESTAMP,
                                     [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                                     [StringUtils get_business_detail:@"business_id"]];
                    
                    [array_of_query addObject:execute_query];
                }
                
                for (int y = 0; y < choice_array.count; y++)
                {
                    /**** Append REFUND Modifiers - CHOICES VALUES as a query ****/
                    
                    execute_query = [NSString stringWithFormat:
                                     @"INSERT INTO REFUND_TRN_MODIFIERS "
                                     "(id,"
                                     "prm_phr_id,"
                                     "prm_phd_id,"
                                     "prm_poh_sn,"
                                     "prm_pod_sn,"
                                     "prm_mod_type,"
                                     "prm_mod_id,"
                                     "prm_mod_name,"
                                     "prm_mod_price,"
                                     "prm_mod_parent_id,"
                                     "prm_parent_name,"
                                     "prm_mod_amount,"
                                     "prm_mod_item_id,"
                                     "prm_mod_qty,"
                                     "prm_created_datetime,"
                                     "prm_updated_by,"
                                     "prm_com_id)"
                                     @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                                     [self get_unique_id_from_db],
                                     refund_id,
                                     refund_trn_id,
                                     data.trx_id,
                                     item.hold_item_id, //[data.orderItemsArray[i] valueForKey:@"hold_item_id"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_type"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_id"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_name"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_price"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_parent_id"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_parent_name"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_price"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_item_id"],
                                     [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_qty"] ? [[choice_array[y] valueForKey:@"selected_choice"] valueForKey:@"mod_qty"] : @"1",
                                     CURRENT_TIMESTAMP,
                                     [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                                     [StringUtils get_business_detail:@"business_id"]];
                    
                    [array_of_query addObject:execute_query];
                }
            }
        }
        
        [self add_to_queue:array_of_query with_priority:high];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

#pragma mark - Method use for 'Daily Sales Report Print Out'
#pragma mark Total of Refund
- (NSString *)get_total_refund_today
{
    return [self get_total_refund_date:[NSDate date]];
}

- (NSString *)get_total_refund_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_total_refund_date_from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_total_refund_date_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *cash_amount = @"0.0";
        
        NSString *query = [NSString stringWithFormat:
                           @"select IFNULL(sum(phr_amount),0) as total_refund FROM REFUND WHERE "
                           " REFUND.phr_com_id='%@' AND datetime(REFUND.phr_create_datetime, 'unixepoch','localtime') BETWEEN '%@' AND '%@' ",
                           [StringUtils get_business_detail:@"business_id"],
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSLog(@"%@",query);
        NSArray *resultSet   = [self get_from_queue:query];
        
        for (NSDictionary *dict in resultSet)
        {
            cash_amount = dict[@"total_refund"];
        }
        
        
        return cash_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Total of Refund GST n SST
- (NSString *)get_refund_gst_sst_today
{
    return [self get_refund_gst_sst_date:[NSDate date]];
}

- (NSString *)get_refund_gst_sst_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_refund_gst_sst_today_from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_refund_gst_sst_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *refund_gst_amount = @"0.0";
        
        NSString *query = [NSString stringWithFormat:@"select IFNULL(sum(phr_gst_amount),0) as total_refund_gst from REFUND where "
                           " strftime(datetime(REFUND.phr_create_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ",
                           shift_start.sql_string,
                           shift_end.sql_string];
        
        NSLog(@"%@",query);
        NSArray *resultSet   = [self get_from_queue:query];
        
        for (NSDictionary *dict in resultSet)
        {
            refund_gst_amount = dict[@"total_refund_gst"];
        }
        
        
        return refund_gst_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Total of Refund SST
- (NSString *)get_refund_sst_today
{
    return [self get_refund_sst_date:[NSDate date]];
}

- (NSString *)get_refund_sst_date:(NSDate *)date
{
    NSArray *dates = date.start_n_end_date;;
    return [self get_refund_sst_today_from:dates.firstObject to:dates.lastObject];
}

- (NSString *)get_refund_sst_today_from:(NSDate *)shift_start to:(NSDate *)shift_end
{
    @try
    {
        NSString *refund_gst_amount = @"0.0";

// recheck condition if needed
//        NSString *query = [NSString stringWithFormat:@"SELECT IFNULL(sum(phr_gst_amount),0) as total_refund_gst from REFUND "
//                           "LEFT JOIN TRANSACTIONS ON TRANSACTIONS.id = REFUND.phr_poh_sn "
//                           "WHERE poh_gst_mode = 'trx_sst_en' "
//                           "AND strftime(datetime(REFUND.phr_create_datetime, 'unixepoch','localtime')) BETWEEN '%@' AND '%@' ",
//                           shift_start.sql_string,
//                           shift_end.sql_string];
//
//        NSLog(@"%@",query);
//        NSArray *resultSet   = [self get_from_queue:query];
//
//        for (NSDictionary *dict in resultSet)
//        {
//            refund_gst_amount = dict[@"total_refund_gst"];
//        }


        return refund_gst_amount;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

#pragma mark Breakdown of Categories & Item Refund
- (NSDictionary *)get_total_refund_cat_today:(NSString *)trn_cat_id by_date_from:(NSDate *)start_date until:(NSDate *)end_date
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(SUM(REFUND_TRN_ITEMS.phd_refund_item_qty),0) as qty, "
                           "IFNULL(SUM((REFUND_TRN_ITEMS.phd_refund_item_price) - REFUND_TRN_ITEMS.phd_item_discount),0) as total "
                           "FROM REFUND_TRN_ITEMS "
                           "INNER JOIN ITEMS ON REFUND_TRN_ITEMS.phd_item_id = ITEMS.item_id "
                           "INNER JOIN CATEGORIES ON ITEMS.item_cat_id = CATEGORIES.category_id "
                           "WHERE CATEGORIES.category_id = '%@' "
						   "AND REFUND_TRN_ITEMS.phd_refund_item_qty > 0 "
                           "AND datetime(REFUND_TRN_ITEMS.phd_created_date, 'unixepoch','localtime') BETWEEN '%@' AND '%@' "
                           ,trn_cat_id,
                           start_date.sql_string,
                           end_date.sql_string];
        
        NSDictionary    *refund_item_array;
        NSArray     *resultSet                = [self get_from_queue:query];
        
        if(resultSet.count > 0)
        {
            for (NSDictionary *dict in resultSet)
            {
                refund_item_array =
                @{
                  @"refund_qty"     : dict[@"qty"],
                  @"refund_total"   : dict[@"total"]
                  };
            }
        }
        else
        {
            refund_item_array =
            @{
              @"refund_qty"     : @"0",
              @"refund_total"   : @"0.0"
              };
        }
        
        
        return refund_item_array;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSDictionary *)get_total_refund_item_today:(NSString *)trn_item_id by_date_from:(NSDate *)start_date until:(NSDate *)end_date
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(SUM(REFUND_TRN_ITEMS.phd_refund_item_qty),0) as qty, "
                           "IFNULL(SUM((REFUND_TRN_ITEMS.phd_refund_item_price) - REFUND_TRN_ITEMS.phd_item_discount),0) as total "
                           "FROM REFUND_TRN_ITEMS "
                           "WHERE phd_item_id = '%@' "
						   "AND REFUND_TRN_ITEMS.phd_refund_item_qty > 0 "
                           "AND datetime(REFUND_TRN_ITEMS.phd_created_date, 'unixepoch','localtime') BETWEEN '%@' AND '%@' "
                           ,trn_item_id,
                           start_date.sql_string,
                           end_date.sql_string];
        
        NSDictionary *refund_item_array;
        NSArray  *resultSet = [self get_from_queue:query];
        
        if(resultSet.count > 0)
        {
            for (NSDictionary *dict in resultSet)
            {
                refund_item_array =
                @{
                  @"refund_qty"     : dict[@"qty"],
                  @"refund_total"   : dict[@"total"]
                  };
            }
        }
        else
        {
            refund_item_array =
            @{
              @"refund_qty"     : @"0",
              @"refund_total"   : @"0.0"
              };
        }
        
        
        return refund_item_array;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (NSDictionary *)get_total_refund_item_choice_today:(NSString *)mod_id by_date_from:(NSDate *)start_date until:(NSDate *)end_date
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"SELECT IFNULL(SUM(REFUND_TRN_ITEMS.phd_refund_item_qty),0) as qty,"
                           "IFNULL(SUM((REFUND_TRN_ITEMS.phd_refund_item_price) - REFUND_TRN_ITEMS.phd_item_discount),0) as total "
                           "FROM REFUND_TRN_ITEMS "
                           "INNER JOIN REFUND_TRN_MODIFIERS ON REFUND_TRN_ITEMS.phd_pod_sn = REFUND_TRN_MODIFIERS.prm_pod_sn "
                           "WHERE REFUND_TRN_MODIFIERS.prm_mod_id = '%@' "
                           "AND REFUND_TRN_MODIFIERS.prm_mod_type = '%@' "
						   "AND REFUND_TRN_ITEMS.phd_refund_item_qty > 0 "
                           "AND datetime(REFUND_TRN_ITEMS.phd_created_date, 'unixepoch','localtime') "
                           "BETWEEN '%@' AND '%@' GROUP BY REFUND_TRN_MODIFIERS.prm_mod_id ORDER BY qty DESC "
                           ,mod_id,CHOICE,
                           start_date.sql_string,
                           end_date.sql_string];
        
        
        NSDictionary *refund_item_array;
        NSArray  *resultSet = [self get_from_queue:query];
        
        if(resultSet.count > 0)
        {
            for (NSDictionary *dict in resultSet)
            {
                refund_item_array =
                @{
                  @"refund_qty"     : dict[@"qty"],
                  @"refund_total"   : dict[@"total"]
                  };
            }
        }
        else
        {
            refund_item_array =
            @{
              @"refund_qty"     : @"0",
              @"refund_total"   : @"0.0"
              };
        }
        
        
        return refund_item_array;
    }
    @catch(NSException *e)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
