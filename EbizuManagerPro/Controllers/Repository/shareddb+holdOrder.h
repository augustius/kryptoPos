//
//  shareddb+holdOrder.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (holdOrder)

- (CompleteOrder *)get_hold_order_with_id:(NSString *)hold_order_id andSplitBill:(BOOL)is_split_bill;
- (NSArray *)get_all_on_hold_orders;
- (void)updateOrderOnHold:(CompleteOrder *)holdOrder;
- (NSString *)insertOrderOnHold:(CompleteOrder *)holdOrder;
- (void)deleteOrderOnHold:(CompleteOrder*)compOrder andSplitBill:(BOOL)split_bill;
- (void)deleteOrderItemOnHold:(NSString*)orderItemId;
- (void)set_hold_order_locked:(int)status trx_id:(NSString*)trx_id;
- (void)mergeHoldOrderFrom:(NSArray *)arrHoldOrder into:(NSDictionary *)holdOrder;
- (void)update_hold_order_merged_status:(NSDictionary *)updated_data;



#pragma mark - Split Bill section
- (NSArray *)get_orders_item_for_split_bill:(NSString*)orderID;
- (void)update_order_item:(NSArray*)orderItems ofOrderID:(int)orderId;
- (void)reupdate_confirmed_order_item;







@end
