//
//  shareddb.h
//  Ebizu Manager
//
//  Created by Jamal on 9/9/14.
//  Copyright (c) 2014 EBIZU. All rights reserved.
//

#import "AbstractRepository.h"
#import "CompleteOrder.h"
#import "OrderItem.h"

@interface shareddb : AbstractRepository

typedef NS_ENUM(NSInteger, item_search_method)
{
    cat_id = 0,
    item_name,
    barcode
};

+ (instancetype)sharedShareddb;

- (NSDecimalNumber *)gst_perc_in_decimal;
- (NSDecimalNumber *)nongst_perc_in_decimal;

@end

