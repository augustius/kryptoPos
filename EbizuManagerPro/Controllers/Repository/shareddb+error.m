//
//  shareddb+error.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+error.h"

@implementation shareddb (error)

- (void)insert_error_details:(NSArray *)error_data
{
    @try
    {
        NSString *query = [NSString stringWithFormat:
                           @"INSERT INTO ERRORS"
                           "(error_id,"
                           "error_desc,"
                           "error_module,"
                           "updated_by,"
                           "created_by,"
                           "company_id,"
                           "error_function_name)"
                           @" VALUES ('%@','%@','%@','%@','%@','%@','%@')",
                           [self get_unique_id_from_db],
                           [error_data valueForKey:@"error_desc"],
                           [error_data valueForKey:@"error_module"],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
                           [StringUtils get_business_detail:@"business_id"],
                           [error_data valueForKey:@"error_function_name"]
                           ];
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

@end
