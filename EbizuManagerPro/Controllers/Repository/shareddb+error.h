//
//  shareddb+error.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (error)

- (void)insert_error_details:(NSArray *)error_data;

@end
