//
//  shareddb+item.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (item)

- (NSString *)get_item_image_name:(NSString *)item_id;
- (NSArray *)get_product_categories_items:(NSString *)input with:(item_search_method)method;
- (void)update_stock_quantity:(NSArray*)item_details;
- (int)get_stock_left:(NSString*)item_id;

@end
