//
//  shareddb+voucher.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+voucher.h"

@implementation shareddb (voucher)

- (NSArray *)get_all_voucher //TODO: table 'VOUCHER_TYPE' is empty, should we take from 'VOUCHERS' table ?
{
    @try
    {
        NSString *now   = @"strftime('%s', 'now','localtime')";
        NSString *query = [NSString stringWithFormat:
                           @"SELECT vo.*, vt.voucher_type_id, vt.voucher_type_name "
                           "FROM VOUCHERS vo "
                           "LEFT JOIN VOUCHER_TYPE vt ON vt.voucher_type_id = vo.voucher_type_id "
                           "WHERE vo.active_status ='1' AND valid_end > %@", now
                           ];
        
        NSArray *resultSet   = [self get_from_queue:query];
        
        return resultSet;
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)edit_manis_voucher:(NSArray *)data
{
    @try
    {
        NSString *query = @"";
        
        if ([self record_already_exists:@"VOUCHERS_BOUGHT_DETAIL"
                            with_filter: [NSString stringWithFormat:@" id='%@'",[data valueForKey:@"voucher_bought_detail_id"]]])
        {
            query = [NSString stringWithFormat:
                     @"UPDATE VOUCHERS_BOUGHT_DETAIL SET"
                     " voucher_name  = '%@',"
                     " member_id     = '%@',"
                     " is_manis      = '%@',"
                     " sn            = '%@',"
                     " code          = '%@',"
                     " expired       = '%@'"
                     " WHERE id      = '%@'",
                     @"MANIS_QRCODE",
                     [data valueForKey:@"member_id"],
                     @"1",
                     [data valueForKey:@"sn"],
                     [data valueForKey:@"code"],
                     [data valueForKey:@"expired"],
                     [data valueForKey:@"voucher_bought_detail_id"]
                     ];
        }
        else
        {
            query = [NSString stringWithFormat:
                     @"INSERT INTO VOUCHERS_BOUGHT_DETAIL"
                     "(id,"
                     "voucher_name,"
                     "voucher_bought_id,"
                     "sn,"
                     "code,"
                     "redeemed,"
                     "expired,"
                     "member_id,"
                     "is_manis,"
                     "company_id)"
                     @" VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
                     [data valueForKey:@"voucher_bought_detail_id"],
                     @"MANIS_QRCODE",
                     [data valueForKey:@"voucher_bought_id"],
                     [data valueForKey:@"sn"],
                     [data valueForKey:@"code"],
                     [data valueForKey:@"redeemed"],
                     [data valueForKey:@"expired"],
                     [data valueForKey:@"member_id"],
                     @"1",
                     [data valueForKey:@"company_id"]
                     ];
        }
        [self add_to_queue:query];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

@end
