//
//  shareddb+transactionNumber.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb+transactionNumber.h"

@implementation shareddb (transactionNumber)

- (void)increase_transaction_number
{
    @try
    {
        NSString *query = [NSString stringWithFormat:@"UPDATE TRANSACTIONS_NUMBER SET trans_number = trans_number + 1 ,"
                           " updated_by    = '%@',"
                           " updated_date  = '%@'",[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],CURRENT_TIMESTAMP];
        [self add_to_queue:query];
        
        [[NSUserDefaults standardUserDefaults] setInteger:[[NSUserDefaults standardUserDefaults] integerForKey:TRANSACTION_NO] + 1
                                                   forKey:TRANSACTION_NO];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

- (void)get_transaction_number
{
    @try
    {
        int trans_number   = 0;
        NSString *query    = [NSString stringWithFormat:@"SELECT trans_number FROM TRANSACTIONS_NUMBER"];
        NSArray *resultSet = [self get_from_queue:query];
        for(NSDictionary *dict in resultSet)
        {
            trans_number = [dict[@"trans_number"] intValue];
        }
        trans_number++;
        
        [[NSUserDefaults standardUserDefaults] setInteger:trans_number forKey:TRANSACTION_NO];
    }
    @catch (NSException *exception)
    {
        [APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
    }
}

@end
