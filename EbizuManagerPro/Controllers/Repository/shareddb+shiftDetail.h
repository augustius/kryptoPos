//
//  shareddb+shiftDetail.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 11/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "shareddb.h"

@interface shareddb (shiftDetail)

- (NSMutableDictionary *)get_last_shift_time_n_date;
- (NSDictionary *)getLastShiftRecord;
- (NSArray *)getAllShiftRecord;
- (NSArray *)get_all_shift_record:(NSString *)dates;

@end
