	//
	//  data_sync.m
	//  EbizuManagerPro
	//
	//  Created by augustius cokroe on 11/11/2015.
	//  Copyright © 2015 Ebizu Sdn Bhd. All rights reserved.
	//

#import "data_sync.h"
#import "StringUtils.h"
#import "FileUtils.h"
#import "shareddb.h"
@interface data_sync ()

@end

@implementation data_sync

+ (instancetype)sharedDataSync
{
	static data_sync *controller = nil;
	static dispatch_once_t onceToken;

	dispatch_once(&onceToken, ^
								{
									controller = [[data_sync alloc] init];
								});

	return controller;
}

- (NSArray *)get_pk_from:(NSString *)table_name with:(NSArray *)extraColumns
{
	NSString *extraQuery = @"";
	if ([extraColumns count] > 0)
	{
		extraQuery = [NSString stringWithFormat:@",%@ ",[extraColumns componentsJoinedByString:@","]];
	}

	NSString *query = [NSString stringWithFormat:
										 @"SELECT id %@ FROM %@ WHERE sync_id is null OR sync_id = '' LIMIT 10",
										 extraQuery,
										 table_name];

	return [self get_from_queue:query];
}

- (NSArray *)get_unsync_data_from:(NSString *)table_name
{
	return [self get_unsync_data_from:table_name withWhereStatement:@"WHERE sync_id is null OR sync_id = '' LIMIT 10"];
}

- (NSArray *)get_unsync_data_from:(NSString *)table_name withWhereStatement:(NSString *)whereStatement
{
	NSString *query = [NSString stringWithFormat:
										 @"SELECT * FROM %@ %@",
										 table_name, whereStatement];

	return [self get_from_queue:query];
}

- (NSDictionary *)get_all_unsync_data
{
	NSMutableDictionary *table_dict = [[NSMutableDictionary alloc] init];

		// get all table name
	NSString *query = @"SELECT tbl_name FROM sqlite_master where type='table'";
	NSArray *rs = [self get_from_queue:query];

	for(NSDictionary *dict in rs)
	{
		NSString *table_name = dict[@"tbl_name"];

		table_dict[table_name] = [self get_unsync_data_from:table_name];
	}

	return table_dict;
}

- (void)set_all_unsync_data // this code is to ease test only
{
		// get all table name
	NSString *query = @"SELECT tbl_name FROM sqlite_master where type='table'";
	NSArray*resultSet = [self get_from_queue:query];

	for(NSDictionary *dict in resultSet)
	{
		NSString *table_name = dict[@"tbl_name"];

		query = [NSString stringWithFormat:
						 @"UPDATE %@ set sync_id = null ",
						 table_name];
	}

}

	// Handle all reading response
- (void)sync_reading_response:(NSDictionary *)response
{
	@try
	{
		if ([response isKindOfClass:[NSDictionary class]])
		{
			for (NSString *table_name in response.allKeys)
			{
				NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

				if ([response[table_name] isKindOfClass:[NSArray class]])
				{
					if([response[table_name] count] > 0)
					{
						NSString *query = [self get_query_for_replace:response[table_name] into:table_name];
						[array_of_query addObject:query];
					}
				}
				NSLog(@"%@ : %@ ",table_name,array_of_query);

				[self add_to_queue:array_of_query with_priority:low];
			}
		}
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)update_sync_response:(NSDictionary *)response
{
	@try
	{
		if ([response isKindOfClass:[NSDictionary class]])
		{
			for (NSString *table_name in response.allKeys)
			{
				if ([response[table_name][@"d"] isKindOfClass:[NSArray class]])
				{
					for (NSDictionary *dict in response[table_name][@"d"])
					{
						NSString *query = [NSString stringWithFormat:
															 @"UPDATE %@ set sync_id = '%@', last_sync_time = '%@' WHERE id = '%@' ",
															 table_name, dict[@"sync_id"] , dict[@"last_sync_time"],
															 dict[@"id"] ];

						[self add_to_queue:query];
					}
				}
			}
		}
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - BUSINESS
- (void)insert_business_details:(NSDictionary *)business withToken:(NSString *)token
{
	@try
	{
		if ([self record_already_exists:@"BUSINESS" with_filter:@""])
		{
			[self edit_business_details:business withToken:token];
		}
		else
		{
			NSString *execute_query = [NSString stringWithFormat:
																 @"INSERT INTO BUSINESS"
																 "(business_id,"
																 "business_name,"
																 "business_token,"
																 "business_category,"
																 "business_userid,"
																 "business_username,"
																 "business_type,"
																 "business_type_id,"
																 "business_address,"
																 "business_email,"
																 "business_photo,"
																 "business_banner,"
																 "business_descrip,"
																 "business_pswd)"
																 @" VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
																 business[@"id"],
																 business[@"name"],
																 token,
																 business[@"category"],
																 business[@"user_id"],
																 [self object:business[@"username"] or:empty_string],
																 business[@"business_name"] ? business[@"business_name"] : @"FNB" ,
																 business[@"business_type_id"] ? business[@"business_type_id"] : @"1",
																 business[@"address"],
																 [self object:business[@"email"] or:empty_string],
																 [self object:business[@"photo"] or:empty_string],
																 [self object:business[@"banner"] or:empty_string],
																 [self object:business[@"description"] or:empty_string],
																 business[@"password"]];

			[self add_to_queue:execute_query];
		}

		[FileUtils download_image:business[@"photo"] withStoringPath:IMG_BUSINESS];

		dispatch_async(dispatch_get_main_queue(),^{
			SESSION_DATA.business_type = business[@"business_name"] ? business[@"business_name"] : @"FNB";
		});

			// rename and put all business data to array to be accessed through NSUserDefault
		NSArray *all_business_data  =
		@[
			@{
				@"business_id"              : business[@"id"]           ? business[@"id"]             : @"",
				@"business_name"            : business[@"name"]         ? business[@"name"]           : @"" ,
				@"business_photo"           : business[@"photo"]        ? business[@"photo"]          : @"",
				@"business_token"           : token                     ? token                       : @"",
				@"business_email"           : business[@"email"]        ? business[@"email"]          : @"",
				@"business_userid"          : business[@"user_id"]      ? business[@"user_id"]        : @"",
				@"business_descrip"         : business[@"description"]  ? business[@"description"]    : @"",
				@"business_username"        : business[@"username"]     ? business[@"username"]       : @"",
				@"business_pswd"            : business[@"password"]     ? business[@"password"]       : @"",
				@"business_banner"          : business[@"banner"]       ? business[@"banner"]         : @"",
				@"business_category"        : business[@"category"]     ? business[@"category"]       : @"",
				@"business_address"         : business[@"address"]      ? business[@"address"]        : @"",
				@"business_phone"           : business[@"phone"]        ? business[@"phone"]          : @"",
				@"footer_message"           : @"Thank you! Please come Again.\nPowered by Ebizu"
				}
			];

		[[NSUserDefaults standardUserDefaults] setObject:all_business_data forKey:BUSINESS_DATA];
		[[NSUserDefaults standardUserDefaults] setObject:token forKey:APP_TOKEN];
		[[NSUserDefaults standardUserDefaults] setObject:business[@"id"] forKey:COMPANY_ID];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}


-(void)edit_business_details:(NSDictionary *)business withToken:(NSString *)token
{
	@try
	{
		NSString *execute_query = [NSString stringWithFormat:
															 @"UPDATE BUSINESS SET "
															 "business_name      = '%@',"
															 "business_token     = '%@',"
															 "business_category  = '%@',"
															 "business_userid    = '%@',"
															 "business_username  = '%@',"
															 "business_type      = '%@',"
															 "business_type_id   = '%@',"
															 "business_address   = '%@',"
															 "business_email     = '%@',"
															 "business_photo     = '%@',"
															 "business_banner    = '%@',"
															 "business_descrip   = '%@',"
															 "business_pswd      = '%@',"
															 "updated_by         = '%@',"
															 "updated_date       = '%@'"
															 " WHERE business_id = '%@'",
															 business[@"name"],
															 token,
															 business[@"category"],
															 business[@"user_id"],
															 [self object:business[@"username"] or:empty_string],
															 business[@"business_name"] ? business[@"business_name"] : @"FNB" ,
															 business[@"business_type_id"] ? business[@"business_type_id"] :@"1",
															 business[@"address"],
															 [self object:business[@"email"] or:empty_string],
															 [self object:business[@"photo"] or:empty_string],
															 [self object:business[@"banner"] or:empty_string],
															 [self object:business[@"description"] or:empty_string],
															 business[@"password"],
															 [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
															 CURRENT_TIMESTAMP,
															 business[@"id"]
															 ];

		[self add_to_queue:execute_query];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - SETTING
- (NSArray *)get_settings_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "hardware_code,enable_barcode,enable_qr_validation,enable_dine_type,printer_mode,image_OnOff_type,"
											 "multi_sync, enable_gst, merchant_name, merchant_address, merchant_phone, footer_message, wifi_name,"
											 "wifi_password, enable_merchant_name, enable_merchant_address, enable_merchant_phone, company_id,"
											 "updated_by, updated_date,custom_sales,is_multi_device,is_mqtt_enabled,last_sync_time,created_by,created_date,active_status,"
											 "current_cash_balance,hold_order_number,counter_number,currency_symbol,currency,gst_id,gst_percentage"
											 " FROM SETTING"];

		NSMutableDictionary *settings;
		NSMutableArray      *setting_array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			settings = [[NSMutableDictionary alloc] init];
			settings[@"hardware_code"]          = dict[@"hardware_code"];
			settings[@"enable_barcode"]         = [self object:dict[@"enable_barcode"] or:empty_string];
			settings[@"enable_qr_validation"]   = [self object:dict[@"enable_qr_validation"] or:empty_string];
			settings[@"enable_dine_type"]       = [self object:dict[@"enable_dine_type"] or:empty_string];
			settings[@"printer_mode"]           = [self object:dict[@"printer_mode"] or:empty_string];
			settings[@"image_OnOff_type"]       = [self object:dict[@"image_OnOff_type"] or:empty_string];
			settings[@"multi_sync"]             = [self object:dict[@"multi_sync"] or:empty_string];
			settings[@"enable_gst"]             = [self object:dict[@"enable_gst"] or:empty_string];
			settings[@"merchant_name"]          = [self object:dict[@"merchant_name"] or:empty_string];
			settings[@"merchant_address"]       = [UIUtils add_encoded_value:[self object:dict[@"merchant_address"] or:empty_string]];
			settings[@"merchant_phone"]         = [self object:dict[@"merchant_phone"] or:empty_string];
			settings[@"footer_message"]         = [UIUtils add_encoded_value:[self object:dict[@"footer_message"] or:empty_string]];
			settings[@"wifi_name"]              = [self object:dict[@"wifi_name"] or:empty_string];
			settings[@"wifi_password"]          = [self object:dict[@"wifi_password"] or:empty_string];
			settings[@"enable_merchant_name"]   = [self object:dict[@"enable_merchant_name"] or:empty_string];
			settings[@"enable_merchant_address"] = [self object:dict[@"enable_merchant_address"] or:empty_string];
			settings[@"enable_merchant_phone"]  = [self object:dict[@"enable_merchant_phone"] or:empty_string];
			settings[@"company_id"]             = [self object:dict[@"company_id"] or:empty_string];
			settings[@"updated_by"]             = [self object:dict[@"updated_by"] or:empty_string];
			settings[@"updated_time"]           = [self object:dict[@"updated_date"] or:empty_string];

			settings[@"custom_sales"]           = [self object:dict[@"custom_sales"] or:empty_string];
			settings[@"is_multi_device"]        = [self object:dict[@"is_multi_device"] or:empty_string];
			settings[@"is_mqtt_enabled"]        = [self object:dict[@"is_mqtt_enabled"] or:empty_string];
			settings[@"last_sync_time"]         = [self object:dict[@"last_sync_time"] or:empty_string];
			settings[@"created_by"]             = [self object:dict[@"created_by"] or:empty_string];
			settings[@"created_date"]           = [self object:dict[@"created_date"] or:empty_string];

			settings[@"active_status"]          = dict[@"active_status"];
			settings[@"company_id"]             = [StringUtils get_business_detail:@"business_id"];

			settings[@"current_cash_balance"]   = [self object:dict[@"current_cash_balance"] or:empty_string];
			settings[@"hold_order_number"]      = [self object:dict[@"hold_order_number"] or:empty_string];
			settings[@"counter_number"]         = [self object:dict[@"counter_number"] or:empty_string];
			settings[@"currency_symbol"]        = [self object:dict[@"currency_symbol"] or:empty_string];
			settings[@"currency"]               = [self object:dict[@"currency"] or:empty_string];
			settings[@"gst_id"]                 = [self object:dict[@"gst_id"] or:empty_string];
			settings[@"gst_percentage"]         = [self object:dict[@"gst_percentage"] or:empty_string];

			[setting_array addObject:settings];
		}


		NSLog(@"%@",setting_array);

		return setting_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

-(void)insert_hardware_code:(NSString *)hardware_code and_taxes:(NSString *)service_charge
{
	@try
	{
		NSString *execute_query = [NSString stringWithFormat:
															 @"INSERT INTO SETTING (hardware_code,service_charge) VALUES ('%@','%@')",hardware_code,service_charge
															 ];

		[self add_to_queue:execute_query];

		[[NSUserDefaults standardUserDefaults] setObject:hardware_code forKey:HARDWARE_CODE];
		[[NSUserDefaults standardUserDefaults] setDouble:[service_charge doubleValue] forKey:TAXES];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

-(void)insert_settings:(NSArray *)setting
{
	@try
	{
		NSString *execute_query   = @"";
		NSString *statement_query =
		@"REPLACE INTO SETTING "
		"(hardware_code,enable_barcode,enable_qr_validation,enable_dine_type,printer_mode,"
		"image_OnOff_type,custom_sales,multi_sync,enable_gst,merchant_name,"
		"merchant_address,merchant_phone,footer_message,wifi_name,wifi_password,"
		"enable_merchant_name,enable_merchant_address,enable_merchant_phone,company_id,sync_id,"
		"last_sync_time,created_by,created_date,updated_by,updated_date,"
		"active_status,mqtt_model_name,mqtt_server_ip,is_multi_device,is_mqtt_enabled,"
		"currency,currency_symbol,gst_id,service_charge,gst_percentage,"
		"counter_number,hold_order_number,current_cash_balance) "
		"VALUES "
		"("
		"'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',"
		"'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',"
		"'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',"
		"'%@','%@','%@','%@','%@','%@','%@','%@'"
		")";

		for (NSDictionary *sett in setting)
		{
			execute_query = [NSString stringWithFormat:statement_query,
											 [[NSUserDefaults standardUserDefaults] stringForKey:HARDWARE_CODE], //sett[@"pos_hardware_code"],
											 sett[@"pos_enable_barcode"],
											 sett[@"pos_enable_qr_validation"],
											 sett[@"pos_enable_dine_type"],
											 sett[@"pos_printer_mode"],

											 sett[@"pos_image_on_off_type"],
											 sett[@"pos_custom_sales"],
											 sett[@"pos_multi_sync"],
											 sett[@"pos_enable_gst"],
											 sett[@"pos_merchant_name"],

											 [UIUtils remove_encoded_value:sett[@"pos_merchant_address"]],
											 sett[@"pos_merchant_phone"],
											 [UIUtils remove_encoded_value:sett[@"pos_footer_message"]],
											 sett[@"pos_wifi_name"],
											 sett[@"pos_wifi_password"],

											 sett[@"pos_enable_merchant_name"],
											 sett[@"pos_enable_merchant_address"],
											 sett[@"pos_enable_merchant_phone"],
											 sett[@"company_id"],
											 sett[@"sync_id"],

											 sett[@"last_sync_time"],
											 sett[@"created_by"],
											 sett[@"created_date"],
											 sett[@"updated_by"],
											 sett[@"update_date"],

											 sett[@"active_status"],
											 sett[@"pos_mqtt_model_name"],
											 sett[@"pos_mqtt_server_ip"],
											 sett[@"pos_is_multi_device"],
											 sett[@"pos_is_mqtt_enabled"],

											 sett[@"pos_currency"],
											 sett[@"pos_currency_symbol"],
											 sett[@"pos_sst_id"],
											 sett[@"pos_service_charge"],
											 sett[@"pos_gst_percentage"],

											 sett[@"pos_counter_number"],
											 sett[@"pos_hold_order_number"],
											 sett[@"pos_current_cash_balance"]
											 ];

			[self add_to_queue:execute_query];

				//            [[NSUserDefaults standardUserDefaults] setObject:sett[@"pos_hardware_code"]     forKey:HARDWARE_CODE];
			[[NSUserDefaults standardUserDefaults] setObject:sett[@"pos_currency_symbol"]   forKey:CURRENCY];
			[[NSUserDefaults standardUserDefaults] setObject:sett[@"pos_currency"]          forKey:CURRENCY_CODE];
			[[NSUserDefaults standardUserDefaults] setObject:sett[@"pos_sst_id"]            forKey:GST_ID];
			[[NSUserDefaults standardUserDefaults] setDouble:([sett[@"pos_gst_percentage"] isEqualToString:@""]) ? 0.0 : [sett[@"pos_gst_percentage"] doubleValue]
																								forKey:GST_PERCENTAGE];
			[[NSUserDefaults standardUserDefaults] setInteger:[sett[@"pos_counter_number"] integerValue]    forKey:ORDER_COUNT];
			[[NSUserDefaults standardUserDefaults] setInteger:[sett[@"pos_hold_order_number"] integerValue] forKey:HOLD_ORDER_NO];
			[[NSUserDefaults standardUserDefaults] setDouble:[sett[@"pos_service_charge"] doubleValue] forKey:TAXES];

			SESSION_DATA.barcode_enable         = [sett[@"pos_enable_barcode"] intValue];
			SESSION_DATA.qr_validation_enable   = [sett[@"pos_enable_qr_validation"] intValue];
			SESSION_DATA.dine_in_enable         = [sett[@"pos_enable_dine_type"] intValue];
			SESSION_DATA.image_on_off_enable    = [sett[@"pos_image_on_off_type"] intValue];
			SESSION_DATA.force_print_mode_enable= [sett[@"pos_printer_mode"] intValue];
			SESSION_DATA.gst_enable             = [sett[@"pos_enable_gst"] intValue];
			SESSION_DATA.enable_merchant_name   = [sett[@"pos_enable_merchant_name"] intValue];
			SESSION_DATA.enable_merchant_address= [sett[@"pos_enable_merchant_address"] intValue];
			SESSION_DATA.enable_merchant_phone  = [sett[@"pos_enable_merchant_phone"] intValue];
			SESSION_DATA.custom_sales_enable    = [sett[@"pos_custom_sales"] intValue];
			SESSION_DATA.enable_multi_devices   = [sett[@"pos_is_multi_device"] intValue];
			SESSION_DATA.enable_mqtt            = [sett[@"pos_is_mqtt_enabled"] intValue];

			SESSION_DATA.mqtt_ip_address        = sett[@"pos_mqtt_server_ip"];
		}
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

-(BOOL)edit_settings_sync_details:(NSArray *)data
{
	@try
	{
		NSString *execute_query = [NSString stringWithFormat:
															 @"UPDATE SETTING SET"
															 " sync_id               = '%@',"
															 " last_sync_time        = '%@'"
															 " WHERE hardware_code   = '%@'",
															 [data valueForKey:@"sync_id"],
															 [data valueForKey:@"last_sync_time"],
															 [data valueForKey:@"hardware_code"]
															 ];

		[self add_to_queue:execute_query];

		[UIUtils store_last_sync_time:CURRENT_TIMESTAMP];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - TRANSACTION_NUMBER
- (void)insert_transaction_number:(NSString *)trans_no
{
	@try
	{
		NSString *execute_query = [NSString stringWithFormat:
															 @"INSERT INTO TRANSACTIONS_NUMBER"
															 "(trans_number,"
															 "company_id,"
															 "sync_id,"
															 "last_sync_time,"
															 "created_by,"
															 "updated_by,"
															 "updated_date,"
															 "active_status)"
															 @" VALUES ('%@','%@','%@','%@','%@','%@','%@','%@')",
															 trans_no,
															 [[NSUserDefaults standardUserDefaults] objectForKey:COMPANY_ID],
															 @"",// manually input '' for sync_id
															 CURRENT_TIMESTAMP,
															 [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
															 [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
															 CURRENT_TIMESTAMP,
															 @"1"];

		[self add_to_queue:execute_query];

		[[NSUserDefaults standardUserDefaults] setObject:trans_no forKey:TRANSACTION_NO];

	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - USERRIGHTS
- (void)sync_user_rights_details:(NSArray *) data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		NSString *resetActiveStatusStatement = @"UPDATE USERRIGHTS SET active_status = '0'";
		[array_of_query addObject:resetActiveStatusStatement];

		for (NSDictionary *user_rights in data)
		{
			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO USERRIGHTS"
																 "(user_id,"
																 "email,"
																 "name,"
																 "password,"
																 "super_user,"
																 "is_sales_person,"
																 "company_id,"
																 "sync_id,"
																 "last_sync_time,"
																 "active_status,"
																 "last_login)"
																 " VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
																 user_rights[@"id"],
																 user_rights[@"email"],
																 user_rights[@"name"],
																 user_rights[@"password"],
																 user_rights[@"superuser"],
																 user_rights[@"is_sales_person"],
																 user_rights[@"company_id"],
																 user_rights[@"sync_id"],
																 user_rights[@"last_sync_time"],
																 @"1" ,
																 user_rights[@"last_login"]
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];

			// TODO: do we even user userrightmodules ? if not drop the table
			//            NSArray *user_right_data;
			//            // To avoid crash issue : Sometime API returns wrong format, So app gets crashed
			//            if([data[i][@"user_rights"] isKindOfClass:[NSArray class]])
			//                user_right_data = data[i][@"user_rights"];
			//
			//            for (int x = 0; x < user_right_data.count; x++)
			//            {
			//                int module_id = (int)[user_right_data[x][@"module"] integerValue];
			//                if (module_id > 0)
			//                {
			//                    if ([Shareddb record_already_exists:@"USERRIGHTSMODULES" with_filter:[NSString stringWithFormat:@"user_id = %@",data[i][@"id"]]])
			//                    {
			//                        NSString *query = @"UPDATE USERRIGHTSMODULES SET"
			//                        " module_id     = ?,"
			//                        "role_id        = ?,"
			//                        "role_name      = ?"
			//                        " WHERE user_id = ?";
			//
			//                        result = [db executeUpdate:query,
			//                                  user_right_data[x][@"module"],
			//                                  [user_right_data[x][@"role"] valueForKey:@"id"],
			//                                  [user_right_data[x][@"role"] valueForKey:@"name"],
			//                                  data[i][@"id"]
			//                                  ];
			//
			//                        if ([db hadError])
			//                        {
			//                            [APP_DELEGATE handle_exception:[db lastErrorMessage] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
			//                            NSLog(@"DB ERROR - %s %@",__PRETTY_FUNCTION__,[db lastErrorMessage]);
			//                            return NO;
			//                        }
			//                    }
			//                    else
			//                    {
			//                        NSString *sql_query = @"INSERT INTO USERRIGHTSMODULES (user_id,module_id,role_id,role_name)"
			//                        @" VALUES (?,?,?,?)";
			//
			//                        result = [db executeUpdate:sql_query,
			//                                  data[i][@"id"],
			//                                  user_right_data[x][@"module"],
			//                                  [user_right_data[x][@"role"] valueForKey:@"id"],
			//                                  [user_right_data[x][@"role"] valueForKey:@"name"]
			//                                  ];
			//
			//                        NSLog(@"here : %@",sql_query);
			//
			//                        if ([db hadError])
			//                        {
			//                            NSLog(@"DB ERROR - %s %@",__PRETTY_FUNCTION__,[db lastErrorMessage]);
			//                            return NO;
			//                        }
			//                    }
			//                }
			//            }

	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

-(void)update_user_right_details:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE USERRIGHTS SET sync_id = '%@', last_sync_time = '%@' WHERE user_id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - SHIFT_DETAIL
- (NSArray *)get_shift_details_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "id,user_id,shift_status,shift_open_time,shift_closed_time,"
											 "open_drawer_amount,closed_drawer_amount,"
											 "updated_by, updated_date,company_id,last_sync_time,created_by,created_date, active_status"
											 " FROM SHIFT_DETAILS where sync_id is null or sync_id is \"\""];


		NSMutableDictionary *shift_data;
		NSMutableArray      *shift_detail_array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			shift_data = [[NSMutableDictionary alloc] init];
			shift_data[@"id"] = dict[@"id"];
			shift_data[@"user_id"] = dict[@"user_id"];
			shift_data[@"shift_status"] = [self object:dict[@"shift_status"] or:empty_string];
			shift_data[@"shift_open_time"] = [self object:dict[@"shift_open_time"] or:empty_string];
			shift_data[@"shift_closed_time"] = [self object:dict[@"shift_closed_time"] or:empty_string];
			shift_data[@"open_drawer_amount"] = [self object:dict[@"open_drawer_amount"] or:empty_string];
			shift_data[@"closed_drawer_amount"] = [self object:dict[@"closed_drawer_amount"] or:empty_string];
			shift_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			shift_data[@"updated_time"] = [self object:dict[@"updated_date"] or:empty_string];
			shift_data[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			shift_data[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			shift_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			shift_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			shift_data[@"active_status"] = dict[@"active_status"];
			[shift_detail_array addObject:shift_data];
		}


		NSLog(@"%@",shift_detail_array);

		return shift_detail_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)sync_shift_details:(NSArray *) data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (NSDictionary *shift_data in data)
		{
			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO SHIFT_DETAILS"
																 "(id,"
																 "user_id,"
																 "shift_status,"
																 "shift_open_time,"
																 "shift_closed_time,"
																 "open_drawer_amount,"
																 "closed_drawer_amount,"
																 "company_id,"
																 "sync_id,"
																 "last_sync_time,"
																 "created_by,"
																 "created_date,"
																 "updated_by,"
																 "updated_date,"
																 "active_status) "
																 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
																 shift_data[@"id"],
																 shift_data[@"user_id"],
																 [self object:shift_data[@"shift_status"] or:empty_string],
																 [self object:shift_data[@"shift_open_time"] or:empty_string],
																 [self object:shift_data[@"shift_closed_time"] or:empty_string],
																 [self object:shift_data[@"open_drawer_amount"] or:empty_string],
																 [self object:shift_data[@"closed_drawer_amount"] or:empty_string],
																 [self object:shift_data[@"company_id"] or:empty_string],
																 [self object:shift_data[@"sync_id"] or:empty_string],
																 [self object:shift_data[@"last_sync_time"] or:empty_string],
																 [self object:shift_data[@"created_by"] or:empty_string],
																 [self object:shift_data[@"created_date"] or:empty_string],
																 [self object:shift_data[@"updated_by"] or:empty_string],
																 [self object:shift_data[@"updated_date"] or:empty_string],
																 [self object:shift_data[@"active_status"] or:empty_string]
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];

			// check for any open shift status without touching on the database
			// use nspredicate to check any 'shift_status' with value 1 in this array of dictionary
		NSPredicate *open_shift_predicate = [NSPredicate predicateWithFormat:@"shift_status = '1'"];
		NSArray *open_shift_data  = [data filteredArrayUsingPredicate:open_shift_predicate];
		[[NSUserDefaults standardUserDefaults] setBool:(open_shift_data.count > 0) forKey:SHIFT_OPENED];

	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)edit_shift_details_from_sync:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"shift_details_id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE SHIFT_DETAILS SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"shift_details_id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - EMPLOYEE_TIMECARD
- (NSArray *)get_employee_timecard_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "id,user_id,clock_in_time,clock_out_time,"
											 "updated_by, updated_date,company_id,last_sync_time,created_by,created_date, active_status"
											 " FROM EMPLOYEE_TIMECARD where sync_id is null or sync_id is \"\""];

		NSMutableDictionary *emp_timecard_data;
		NSMutableArray      *emp_timecard_array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			emp_timecard_data = [[NSMutableDictionary alloc] init];
			emp_timecard_data[@"id"] = dict[@"id"];
			emp_timecard_data[@"user_id"] = dict[@"user_id"];
			emp_timecard_data[@"clock_in_time"] = [self object:dict[@"clock_in_time"] or:empty_string];
			emp_timecard_data[@"clock_out_time"] = [self object:dict[@"clock_out_time"] or:empty_string];
			emp_timecard_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			emp_timecard_data[@"updated_time"] = [self object:dict[@"updated_date"] or:empty_string];
			emp_timecard_data[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			emp_timecard_data[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			emp_timecard_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			emp_timecard_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			emp_timecard_data[@"active_status"] = dict[@"active_status"];
			[emp_timecard_array addObject:emp_timecard_data];
		}


		NSLog(@"%@",emp_timecard_array);

		return emp_timecard_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)sync_employee_time_card_details:(NSArray *) data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (NSDictionary *emp_timecard_data in data)
		{
			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO EMPLOYEE_TIMECARD"
																 "(id,"
																 "user_id,"
																 "clock_in_time,"
																 "clock_out_time,"
																 "company_id,"
																 "sync_id,"
																 "last_sync_time,"
																 "created_by,"
																 "created_date,"
																 "updated_by,"
																 "updated_date,"
																 "active_status)"
																 @" VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
																 emp_timecard_data[@"id"],
																 emp_timecard_data[@"user_id"],
																 [self object:emp_timecard_data[@"clock_in_time"] or:empty_string],
																 [self object:emp_timecard_data[@"clock_out_time"] or:empty_string],
																 [self object:emp_timecard_data[@"company_id"] or:empty_string],
																 [self object:emp_timecard_data[@"sync_id"] or:empty_string],
																 [self object:emp_timecard_data[@"last_sync_time"] or:empty_string],
																 [self object:emp_timecard_data[@"created_by"] or:empty_string],
																 [self object:emp_timecard_data[@"created_date"] or:empty_string],
																 [self object:emp_timecard_data[@"updated_by"] or:empty_string],
																 [self object:emp_timecard_data[@"updated_date"] or:empty_string],
																 [self object:emp_timecard_data[@"active_status"] or:empty_string]
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)edit_emp_timecard_from_sync:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE EMPLOYEE_TIMECARD SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - TRANSACTION

- (NSDictionary *)get_transactions_for_sync
{
	return [self get_transactions_for_sync:10];
}

- (NSDictionary *)get_transactions_for_sync:(int)limit
{
	@try
	{
		NSMutableDictionary *table_dict = [[NSMutableDictionary alloc] init];
		NSMutableArray *allHeaderPk = [[NSMutableArray alloc] init];

			// 1. get transaction id from details
		NSString *query = [NSString stringWithFormat:@"SELECT pod_poh_sn, count(*) - count(id) as null_count FROM TRN_ITEMS WHERE sync_id is null GROUP BY pod_poh_sn LIMIT %d", limit];
		NSArray *selectedPK = [self get_from_queue: query];

			// 2. only use transaction id from non null detail id
		for (NSDictionary *dict in selectedPK) {
			if ([dict[@"null_count"] isEqualToString: @"0"]) {
				[allHeaderPk addObject:dict[@"pod_poh_sn"]];
			}
		}

		if (allHeaderPk.count > 0)
		{
				// 3. get transaction by using transaction id from detail in database
			NSString *allPkStatement = [allHeaderPk componentsJoinedByString:@"','"];

			NSString *table_name = @"TRANSACTIONS";
			NSArray *allHeaderData = [self get_unsync_data_from:table_name withWhereStatement:[NSString stringWithFormat:@"WHERE id IN('%@')",allPkStatement]];
			table_dict[table_name] = allHeaderData;

				// 4. make sure only get detail and the rest using non null transaction id in database
			allHeaderPk = [allHeaderData valueForKey:@"id"];
			allPkStatement = [allHeaderPk componentsJoinedByString:@"','"];

			table_name = @"TRN_ITEMS";
			NSArray *details = [self get_unsync_data_from:table_name withWhereStatement:[NSString stringWithFormat:@"WHERE pod_poh_sn IN('%@')",allPkStatement]];

			// 5. make sure details is not too big unless it is the only transaction, else call function again with smaller limit
			if ([details count] < 100 || [allHeaderData count] == 1) {
				table_dict[table_name] = details;

				table_name = @"TRN_MODIFIERS";
				table_dict[table_name] = [self get_unsync_data_from:table_name withWhereStatement:[NSString stringWithFormat:@"WHERE pom_poh_sn IN('%@')",allPkStatement]];

				table_name = @"TRANSACTION_MULTIPLE_PAYMENTS";
				table_dict[table_name] = [self get_unsync_data_from:table_name withWhereStatement:[NSString stringWithFormat:@"WHERE phm_poh_sn IN('%@')",allPkStatement]];
			} else {
				limit--;
				return [self get_transactions_for_sync:limit];
			}

		} else {
			return @{};
		}

		return table_dict;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (NSDictionary *)get_transaction_data_to_check_sync_id
{
	@try
	{
		NSString *table_name = @"TRANSACTIONS";
		NSMutableDictionary *table_dict = [[NSMutableDictionary alloc] init];
		table_dict[table_name] = [self get_pk_from:table_name with:@[@"poh_com_id"]];

			//        table_name = @"TRN_ITEMS";
			//        table_dict[table_name] = [self get_pk_from:table_name with:@[@"pod_com_id"]];
			//
			//        table_name = @"TRN_MODIFIERS";
			//        table_dict[table_name] = [self get_pk_from:table_name with:@[@"pom_com_id",@"pom_type"]];

		return table_dict;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

	//- (void)add_edit_transactions:(NSArray *)data
	//{
	//    @try
	//    {
	//        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
	//        NSString *execute_query = @"";
	//        data = [UIUtils remove_encoded_values:data];
	//
	//        for (id data_item in data)
	//        {
	//            //testing temporay
	//            if(data_item == data.lastObject)
	//            {
	//                NSLog(@"Last data %@", data_item);
	//            }
	//
	//            if ([self record_already_exists : @"TRANSACTIONS"
	//                          with_filter           : [NSString stringWithFormat:@" trx_id = \"%@\"", [data_item valueForKey:@"transaction_sn"]]
	//                 ])
	//            {
	//
	//            }
	//            else
	//            {
	//                execute_query = [NSString stringWithFormat:
	//                @"INSERT INTO TRANSACTIONS"
	//                "("
	//                "trx_id,"
	//                "total,"
	//                "checkin_id,"
	//                "member_id,"
	//                "payment,"
	//
	//                "close_time,"
	//                "customer_name,"
	//                "offer_sn,"
	//                "subtotal,"
	//                "customer_id,"
	//
	//                "disc_amt,"
	//                "currency,"
	//                "tax_amt,"
	//                "company_id,"
	//                "create_time,"
	//
	//                "disc_percentage,"
	//                "user_id,"
	//                "trx_order_id,"
	//
	//                "notes,"
	//                "status,"
	//                "payment_method_id,"
	//                "custom_discount_id, "
	//                "voucher_id,"
	//
	//                "voucher_name,"
	//                "voucher_amount,"
	//                "voucher_type_id,"
	//                "voucher_type_name,"
	//                "voucher_member_number,"
	//
	//                "voucher_ref_number,"
	//                "counter_number,"
	//                "rounding_amount,"
	//                "customer_gstid,"
	//                "customer_email,"
	//
	//                "customer_phone,"
	//                "customer_city,"
	//                "customer_address1,"
	//                "customer_address2,"
	//                "customer_pincode,"
	//
	//                "trx_gst_mode,"
	//                "total_gst_value,"
	//                "company_id,"
	//
	//                "table_no,"
	//                "trans_number,"
	//                "last_sync_time,"
	//                "sync_id,"
	//
	//                "pay_type_name"
	//                ") "
	//                @"VALUES ('%@','%@','%@','%@','%@',"
	//                "'%@','%@','%@','%@','%@',"
	//                "'%@','%@','%@','%@','%@',"
	//                "'%@','%@','%@','%@','%@',"
	//                "'%@','%@','%@','%@','%@',"
	//                "'%@','%@','%@','%@','%@',"
	//                "'%@','%@','%@','%@','%@',"
	//                "'%@','%@','%@','%@','%@',"
	//                "'%@','%@','%@','%@','%@','%@')",
	//                 [data_item valueForKey:@"transaction_sn"],
	//                 [data_item valueForKey:@"total"],
	//                 [data_item valueForKey:@"checkin_id"],
	//                 [data_item valueForKey:@"member"],
	//                 [data_item valueForKey:@"payment"],
	//
	//                 [data_item valueForKey:@"close_time"],
	//                 [data_item valueForKey:@"customer_name"],
	//                 [data_item valueForKey:@"offer_sn"],
	//                 [data_item valueForKey:@"subtotal"],
	//                 [data_item valueForKey:@"customer"],
	//
	//                 [data_item valueForKey:@"discount_amount"],
	//                 [data_item valueForKey:@"currency"],
	//                 [data_item valueForKey:@"tax_amount"],
	//                 [data_item valueForKey:@"company_id"],
	//                 [data_item valueForKey:@"create_time"],
	//
	//                 [data_item valueForKey:@"discount_percentage"],
	//                 [data_item valueForKey:@"user_id"],
	//                 [data_item valueForKey:@"transaction_id"],
	//
	//                 [data_item valueForKey:@"notes"],
	//                 [data_item valueForKey:@"status"],
	//                 [data_item valueForKey:@"payment_method_id"],
	//                 [data_item valueForKey:@"discount_id"],
	//                 [data_item valueForKey:@"voucher_id"],
	//
	//                 [data_item valueForKey:@"voucher_name"],
	//                 [data_item valueForKey:@"voucher_amount"],
	//                 [data_item valueForKey:@"voucher_type_id"],
	//                 [data_item valueForKey:@"voucher_type_name"],
	//                 [data_item valueForKey:@"voucher_member_number"],
	//
	//                 [data_item valueForKey:@"voucher_ref_number"],
	//                 [data_item valueForKey:@"counter_number"],
	//                 [data_item valueForKey:@"rounding_amount"],
	//                 [data_item valueForKey:@"customer_gstid"],
	//                 [data_item valueForKey:@"customer_email"],
	//
	//                 [data_item valueForKey:@"customer_phone"],
	//                 [data_item valueForKey:@"customer_city"],
	//                 [data_item valueForKey:@"customer_address1"],
	//                 [data_item valueForKey:@"customer_address2"],
	//                 [data_item valueForKey:@"customer_pincode"],
	//
	//                 [data_item valueForKey:@"gst_mode"],
	//                 [data_item valueForKey:@"total_gst_value"],
	//                 [data_item valueForKey:@"company_id"],
	//
	//                 [data_item valueForKey:@"table_number"],
	//                 [data_item valueForKey:@"trans_number"],
	//                 [data_item valueForKey:@"sync_datetime"],
	//                 [data_item valueForKey:@"transaction_sync_id"],
	//
	//                 [data_item valueForKey:@"pay_type_name"]
	//                 ];
	//
	//                [self add_edit_transactions_item:[data_item valueForKey:@"items"] with_trans_number:[data_item valueForKey:@"transaction_sn"]];
	//
	//                [array_of_query addObject:execute_query];
	//            }
	//        }
	//
	//        [self add_to_queue:array_of_query with_priority:low];
	//    }
	//    @catch (NSException *exception)
	//    {
	//        DebugLog(@"Exception %@",exception);
	//    }
	//}
	//
	//- (void)edit_transctions_from_sync:(NSArray *)data
	//{
	//    @try
	//    {
	//        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
	//
	//        for (int i = 0; i < data.count; i++)
	//        {
	//            if(data[i][@"trx_sn"] != nil)
	//            {
	//                NSString *execute_query = [NSString stringWithFormat:
	//                  @"UPDATE TRANSACTIONS SET "
	//                   " sync_id       = '%@',"
	//                   " updated_by    = '%@',"
	//                   " updated_date  = '%@'"
	//                   " WHERE trx_id  = '%@'",
	//                   data[i][@"transaction_sync_id"],
	//                   [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
	//                   CURRENT_TIMESTAMP,
	//                   data[i][@"trx_sn"]];
	//
	//                [array_of_query addObject:execute_query];
	//            }
	//        }
	//
	//        [self add_to_queue:array_of_query with_priority:low];
	//
	//        [UIUtils store_last_sync_time:CURRENT_TIMESTAMP];
	//    }
	//    @catch (NSException *exception)
	//    {
	//        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	//    }
	//}

- (void)edit_sync_error_transaction:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (NSDictionary *dict in data)
		{
			if(dict[@"message"] != nil && dict[@"trx_sn"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE TRANSACTIONS SET sync_error_message = '%@', "
																	 " updated_by    = '%@',"
																	 " updated_date  = '%@'"
																	 " WHERE trx_id = '%@'",
																	 dict[@"message"],
																	 [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
																	 CURRENT_TIMESTAMP,
																	 dict[@"trx_sn"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - TRN_ITEMS
- (NSArray *)get_transactions_item_for_sync:(NSString *)trans_id
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT * FROM TRN_ITEMS WHERE trx_id = '%@'",trans_id];

		NSMutableDictionary *items;
		NSMutableArray      *items_array    = [[NSMutableArray alloc] init];
		NSArray        *resultSet             = [self get_from_queue:query]; //[self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			items = [[NSMutableDictionary alloc] init];

			NSString *item_name     = [UIUtils remove_encoded_value:dict[@"item_name"]];
			NSString *item_remarks  = [UIUtils remove_encoded_value:dict[@"item_remarks"]];

			items[@"name"] = [item_name stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
			items[@"sn"] = dict[@"trx_item_id"];
			items[@"trx_id"] = dict[@"trx_id"];
			items[@"id"] = dict[@"item_id"];
			items[@"remarks"] = [item_remarks stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
			items[@"qty"] = dict[@"item_qty"];
			items[@"price"] = dict[@"item_price"];
			items[@"cost_price"] = dict[@"cost_price"];
			items[@"selling_price"] = dict[@"selling_price"];
			items[@"stock_monitoring"] = [self object:dict[@"stock_monitoring"] or:empty_string];
			items[@"gst_code"] = dict[@"gst_code"];
			items[@"gst_rate"] = dict[@"gst_rate"];
			items[@"gst_value"] = dict[@"gst_value"];
			items[@"discount_amount"] = dict[@"item_discount"];
			items[@"discount_id"] = dict[@"custom_discount_id"];


			items[@"uom_qty"] = [self object:dict[@"uom_qty"] or:empty_string];
			items[@"type_of_price"] = [self object:dict[@"type_of_price"] or:empty_string];
			items[@"type_of_unit"] = [self object:dict[@"type_of_unit"] or:empty_string];

			items[@"is_edited_item_price"] = [self object:dict[@"is_edited_item_price"] or:empty_string];
			items[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			items[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			items[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			items[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			items[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			items[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			items[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			items[@"modifiers"] = [self get_modifier_item_for_sync:dict[@"trx_item_id"] andItemId:dict[@"item_id"]];
			[items_array addObject:items];

		}


		NSLog(@"%@",items_array);
		return [UIUtils add_encoded_values:items_array];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)add_edit_transactions_item:(NSArray *)data with_trans_number:(NSString *)trans_number
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *execute_query = @"";

		data = [UIUtils remove_encoded_values:data];

		for (id data_item in data)
		{
			if ([self record_already_exists : @"TRN_ITEMS"
								with_filter           : [NSString stringWithFormat:@" trx_id = \"%@\" AND trx_item_id=\"%@\"",
																				 trans_number,
																				 [data_item valueForKey:@"id"]]
					 ])

			{

			}
			else
			{
				execute_query = [NSString stringWithFormat:
												 @"INSERT INTO TRN_ITEMS"
												 "("
												 "trx_item_id,"
												 "trx_id,"
												 "item_id,"
												 "item_name,"
												 "item_qty,"
												 "item_price,"
												 "item_remarks,"
												 "item_discount,"
												 "custom_discount_id,"
												 "stock_monitoring,"
												 "sync_id,"
												 "gst_code,"
												 "gst_rate,"
												 "gst_value,"
												 "cost_price,"
												 "selling_price,"
												 "type_of_unit,"
												 "type_of_price,"
												 "uom_qty"
												 ")"
												 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
												 [data_item valueForKey:@"sn"],
												 trans_number,
												 [data_item valueForKey:@"item_id"],
												 [data_item valueForKey:@"name"],
												 [data_item valueForKey:@"qty"],
												 [data_item valueForKey:@"price"],
												 [data_item valueForKey:@"remarks"],
												 [data_item valueForKey:@"discount_amount"],
												 [data_item valueForKey:@"discount_id"],
												 [data_item valueForKey:@"stock_monitoring"],
												 @"", // sync_id not in server
												 [data_item valueForKey:@"gst_code"],
												 [data_item valueForKey:@"gst_rate"],
												 [data_item valueForKey:@"gst_value"],
												 [data_item valueForKey:@"cost_price"],
												 [data_item valueForKey:@"selling_price"],
												 [data_item valueForKey:@"type_of_unit"],
												 [data_item valueForKey:@"type_of_price"],
												 [data_item valueForKey:@"uom_qty"]
												 ];

				[array_of_query addObject:execute_query];

				NSArray *addition_array = [[data_item valueForKey:@"modifiers"] valueForKey:@"addition"];
				NSArray *choice_array   = [[data_item valueForKey:@"modifiers"] valueForKey:@"choice"];

					// should get mod_qty

				for (id addtion in addition_array)
				{
					execute_query = [NSString stringWithFormat:
													 @"INSERT INTO TRN_MODIFIERS "
													 "("
													 "trx_item_id,"
													 "mod_item_id,"
													 "mod_type,"
													 "mod_id,"
													 "mod_name,"
													 "mod_price,"
													 "mod_parent_id,"
													 "mod_parent_name,"
													 "mod_amount,"
													 "mod_qty"
													 ") "
													 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
													 [data_item valueForKey:@"sn"],//trans_number,
													 [data_item valueForKey:@"item_id"],
													 ADDITION,
													 [addtion valueForKey:@"childid"],
													 [addtion valueForKey:@"childname"],
													 [addtion valueForKey:@"childprice"],
													 [addtion valueForKey:@"parentid"],
													 [addtion valueForKey:@"parentname"],
													 @"0",//[[addition_array objectAtIndex:x] valueForKey:@"childamount"], //FIXME : this is not precise amount
													 [addtion valueForKey:@"mod_qty"]
													 ];

					[array_of_query addObject:execute_query];
				}

				for (id choice in choice_array)
				{
					execute_query = [NSString stringWithFormat:
													 @"INSERT INTO TRN_MODIFIERS "
													 "("
													 "trx_item_id,"
													 "mod_item_id,"
													 "mod_type,"
													 "mod_id,"
													 "mod_name,"
													 "mod_price,"
													 "mod_parent_id,"
													 "mod_parent_name,"
													 "mod_amount,"
													 "mod_qty"
													 ")"
													 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
													 [data_item valueForKey:@"sn"],//trans_number,
													 [data_item valueForKey:@"item_id"],
													 CHOICE,
													 [choice valueForKey:@"childid"],
													 [choice valueForKey:@"childname"],
													 [choice valueForKey:@"childprice"],
													 [choice valueForKey:@"parentid"],
													 [choice valueForKey:@"parentname"],
													 @"0",//[[addition_array objectAtIndex:x] valueForKey:@"childamount"] ? [[addition_array objectAtIndex:x] valueForKey:@"childamount"] : @"0",
													 @"1" //[[addition_array objectAtIndex:x] valueForKey:@"mod_qty"] ? [[addition_array objectAtIndex:x] valueForKey:@"mod_qty"] : @"1"
													 ];

					[array_of_query addObject:execute_query];
				}
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - TRN_MODIFIER
- (NSArray *)get_modifier_item_for_sync:(NSString *)trx_item_id andItemId:(NSString *)mod_item_id
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT * FROM TRN_MODIFIERS WHERE trx_item_id = '%@' AND mod_item_id = '%@'",trx_item_id,mod_item_id];

		NSMutableDictionary *items;
		NSMutableArray      *addition_array = [[NSMutableArray alloc]init];
		NSMutableArray      *choice_array   = [[NSMutableArray alloc]init];
		NSArray        *resultSet             = [self get_from_queue:query]; //[self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			items               = [[NSMutableDictionary alloc] init];
			NSString *item_name = [UIUtils add_encoded_value:dict[@"mod_name"]];

			items[@"childname"] = [item_name stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
			items[@"childid"] = dict[@"mod_id"];
			items[@"childprice"] = dict[@"mod_price"];
			items[@"parentname"] = dict[@"mod_parent_name"];
			items[@"parentid"] = dict[@"mod_parent_id"];
			items[@"childamount"] = dict[@"mod_amount"];

			items[@"mod_qty"] = dict[@"mod_qty"];
			items[@"mod_cat_id"] = dict[@"mod_item_id"];

			items[@"id"] = [self object:dict[@"id"] or:empty_string];
			items[@"trx_item_id"] = [self object:dict[@"trx_item_id"] or:empty_string];
			items[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			items[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			items[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			items[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			items[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			items[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			items[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			if ([dict[@"mod_type"] isEqualToString:ADDITION])
			{
				[addition_array addObject:items];
			}
			else if ([dict[@"mod_type"] isEqualToString:CHOICE])
			{
				[choice_array addObject:items];
			}

		}


		NSArray *modifier_array = @[@{@"addition": addition_array},
																@{@"choice": choice_array}];

		return modifier_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - TRANSACTION_MULTIPLE_PAYMENTS
- (NSDictionary *)get_multiple_payments_for_sync
{
	@try
	{
			//        NSString *query = [NSString stringWithFormat:@"SELECT "
			//                           "id,trx_id,current_paid_amount,total_amount,payment_type_id,pay_name,"
			//                           "payment_type_name,payment_method_id,"
			//                           "updated_by, updated_date,company_id,last_sync_time,created_by,created_date, active_status"
			//                           " FROM TRANSACTION_MULTIPLE_PAYMENTS where sync_id is null"];

			//        NSMutableDictionary *payment_data;
			//        NSMutableArray      *mp_array   = [[NSMutableArray alloc] init];
			//        NSArray         *resultSet  = [db executeQuery:query];

			//        while([resultSet next])
			//        {
			//            payment_data = [[NSMutableDictionary alloc] init];
			//            payment_data[@"id"]                     = [resultSet stringForColumn:@"id"];
			//            payment_data[@"trx_id"]                 = [resultSet stringForColumn:@"trx_id"];
			//            payment_data[@"current_paid_amount"]    = [self object:[resultSet stringForColumn:@"current_paid_amount"] or:empty_string];
			//            payment_data[@"total_amount"]           = [self object:[resultSet stringForColumn:@"total_amount"] or:empty_string];
			//            payment_data[@"payment_type_id"]        = [self object:[resultSet stringForColumn:@"payment_type_id"] or:empty_string];
			//            payment_data[@"pay_name"]               = [self object:[resultSet stringForColumn:@"pay_name"] or:empty_string];
			//            payment_data[@"payment_type_name"]      = [self object:[resultSet stringForColumn:@"payment_type_name"] or:empty_string];
			//            payment_data[@"payment_method_id"]      = [self object:[resultSet stringForColumn:@"payment_method_id"] or:empty_string];
			//            payment_data[@"updated_by"]             = [self object:[resultSet stringForColumn:@"updated_by"] or:empty_string];
			//            payment_data[@"updated_date"]           = [self object:[resultSet stringForColumn:@"updated_date"] or:empty_string];
			//            payment_data[@"company_id"]             = [self object:[resultSet stringForColumn:@"company_id"] or:empty_string];
			//
			//            payment_data[@"last_sync_time"]         = [self object:[resultSet stringForColumn:@"last_sync_time"] or:empty_string];
			//            payment_data[@"created_by"]             = [self object:[resultSet stringForColumn:@"created_by"] or:empty_string];
			//            payment_data[@"created_date"]           = [self object:[resultSet stringForColumn:@"created_date"] or:empty_string];
			//
			//            payment_data[@"active_status"]          = [resultSet stringForColumn:@"active_status"];
			//            [mp_array addObject:payment_data];
			//        }
			//        [resultSet close];

		NSString *table_name = @"TRANSACTION_MULTIPLE_PAYMENTS";
		NSMutableDictionary *table_dict = [[NSMutableDictionary alloc] init];
		table_dict[table_name] = [self get_unsync_data_from:table_name];

		return table_dict;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)sync_multi_payment:(NSArray *)data
{
	@try
	{
			//        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
			//
			//        for (NSDictionary *payment_data in data)
			//        {
			//            NSString *execute_query = [NSString stringWithFormat:
			//            @"REPLACE INTO TRANSACTION_MULTIPLE_PAYMENTS"
			//            "(id,"
			//            "trx_id,"
			//            "current_paid_amount,"
			//            "total_amount,"
			//            "pay_name,"
			//            "payment_type_id,"
			//            "payment_type_name,"
			//            "payment_method_id,"
			//            "company_id,"
			//            "sync_id,"
			//            "last_sync_time,"
			//            "created_by,"
			//            "created_date,"
			//            "updated_by,"
			//            "updated_date,"
			//            "active_status)"
			//            @" VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
			//              payment_data [@"id"],
			//              payment_data [@"trx_id"],
			//              payment_data [@"current_paid_amount"],
			//              payment_data [@"total_amount"],
			//              payment_data [@"pay_name"],
			//              payment_data [@"payment_type_id"],
			//              payment_data [@"payment_type_name"],
			//              payment_data [@"payment_method_id"],
			//              payment_data [@"company_id"],
			//              payment_data [@"sync_id"],
			//              payment_data [@"last_sync_time"],
			//              payment_data [@"created_by"],
			//              payment_data [@"created_date"],
			//              payment_data [@"updated_by"],
			//              payment_data [@"updated_date"],
			//              payment_data [@"active_status"]];
			//
			//            [array_of_query addObject:execute_query];
			//        }
			//
			//        [self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)edit_multiple_payment_from_sync:(NSArray *)data // TODO: remove when new response came
{
		//    @try
		//    {
		//        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		//
		//        for (int i = 0; i < data.count; i++)
		//        {
		//            if(data[i][@"transaction_multiple_payment_id"] != nil)
		//            {
		//                NSString *execute_query = [NSString stringWithFormat:
		//                @"UPDATE TRANSACTION_MULTIPLE_PAYMENTS SET sync_id = '%@', last_sync_time = '%@' "
		//                " WHERE id = '%@'",
		//                data[i][@"sync_id"],
		//                data[i][@"last_sync_time"],
		//                data[i][@"transaction_multiple_payment_id"]
		//                ];
		//
		//                [array_of_query addObject:execute_query];
		//            }
		//        }
		//
		//        [self add_to_queue:array_of_query with_priority:low];
		//    }
		//    @catch (NSException *exception)
		//    {
		//        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
		//    }
}

#pragma mark - PENDING_EMAILS
- (NSArray *)get_pending_emails;
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT email_add,trx_id,last_try,remarks,email_type,company_id,last_sync_time,created_by,created_date,updated_by,updated_date,active_status FROM PENDING_EMAILS"];

		NSMutableDictionary *emails;
		NSMutableArray      *email_array  = [[NSMutableArray alloc] init];
		NSArray        *resultSet   = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			emails = [[NSMutableDictionary alloc] init];
			emails[@"email"] = dict[@"email_add"];
			emails[@"trx_sn"] = dict[@"trx_id"];

			emails[@"last_try"] = [self object:dict[@"last_try"] or:empty_string];
			emails[@"remarks"] = [self object:dict[@"remarks"] or:empty_string];
			emails[@"email_type"] = [self object:dict[@"email_type"] or:empty_string];
			emails[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			emails[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			emails[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			emails[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			emails[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			emails[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			emails[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			[email_array addObject:emails];
		}


		return email_array;
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

- (void)edit_pending_emails:(NSArray *)data
{
	@try
	{
		NSDate *today   = [NSDate date];
		NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
		formatter.dateFormat = @"dd-MM-yyyy hh:mm";
		NSString *current_date      = [formatter stringFromDate:today];

		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"trx_sn"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE PENDING_EMAILS SET"
																	 " last_try = '%@', remarks='%@',"
																	 " updated_by = '%@', "
																	 " updated_date = '%@' "
																	 " WHERE email_add = '%@' AND trx_id='%@'",
																	 current_date,
																	 data[i][@"m"],
																	 [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
																	 CURRENT_TIMESTAMP,
																	 data[i][@"email"],
																	 data[i][@"trx_sn"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];

	}
}

- (void)delete_pending_emails:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			NSString *execute_query = [NSString stringWithFormat:
																 @"DELETE FROM PENDING_EMAILS "
																 " WHERE email_add = '%@' AND trx_id= '%@'",
																 [data[i] valueForKey:@"email"],
																 [data[i] valueForKey:@"trx_sn"]
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];

	}
}

#pragma mark - REFUND
- (NSDictionary *)get_refund_for_sync
{
	@try
	{
		NSString *table_name = @"REFUND";
		NSMutableDictionary *table_dict = [[NSMutableDictionary alloc] init];
		NSArray *allHeaderData = [self get_unsync_data_from:table_name withWhereStatement:@""];
		table_dict[table_name] = allHeaderData;

		NSArray *allHeaderPk = [allHeaderData valueForKey:@"id"];
		if (allHeaderPk.count > 0)
		{
			NSString *allPkStatement = [allHeaderPk componentsJoinedByString:@"','"];

			table_name = @"REFUND_TRN_ITEMS";
			table_dict[table_name] = [self get_unsync_data_from:table_name withWhereStatement:[NSString stringWithFormat:@"WHERE phd_phr_id IN('%@') OR sync_id is null OR sync_id = '' ",allPkStatement]];

			table_name = @"REFUND_TRN_MODIFIERS";
			table_dict[table_name] = [self get_unsync_data_from:table_name withWhereStatement:[NSString stringWithFormat:@"WHERE prm_phr_id IN('%@') OR sync_id is null OR sync_id = '' ",allPkStatement]];
		}
		else
		{
			return @{};
		}

		return table_dict;
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

- (void)edit_refund_from_sync:(NSArray *)data
{
		//    @try
		//    {
		//        NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		//
		//        for (int i = 0; i < data.count; i++)
		//        {
		//            if(data[i][@"id"] != nil)
		//            {
		//                NSString *execute_query = [NSString stringWithFormat:
		//               @"UPDATE REFUND SET sync_id = '%@', last_sync_time = '%@', "
		//               "updated_by = '%@', "
		//               "updated_date = '%@' "
		//               "WHERE refund_id = '%@'",
		//                  data[i][@"sync_id"],
		//                  data[i][@"sync_time"],
		//                  [[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_USERID],
		//                  CURRENT_TIMESTAMP,
		//                  data[i][@"id"]
		//                ];
		//
		//                [array_of_query addObject:execute_query];
		//            }
		//        }
		//
		//        [self add_to_queue:array_of_query with_priority:low];
		//    }
		//    @catch (NSException *exception)
		//    {
		//        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
		//    }
}

#pragma mark - PURCHASE_ORDER
- (NSArray *)get_po_receive_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "po_id, po_no, supplier_code, supplier_name, outright_consignment, status_id, company_id,"
											 "sync_id, last_sync_time, created_by, created_date, updated_by,updated_date, active_status"
											 " FROM PURCHASE_ORDER where sync_id is null"];

		NSMutableDictionary *po;
		NSMutableArray      *po_array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			po = [[NSMutableDictionary alloc] init];
			po[@"po_id"] = dict[@"po_id"];
			po[@"po_no"] = dict[@"po_no"];
			po[@"supplier_code"] = dict[@"supplier_code"];
			po[@"supplier_name"] = dict[@"supplier_name"];
			po[@"outright_consignment"] = dict[@"outright_consignment"];
			po[@"status_id"] = dict[@"status_id"];
			po[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			po[@"sync_id"] = [self object:dict[@"sync_id"] or:empty_string];
			po[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			po[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			po[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			po[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			po[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			po[@"active_status"] = dict[@"active_status"];
			[po_array addObject:po];
		}


		NSLog(@"%@",po_array);

		return po_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)add_edit_po_receive:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *execute_query = @"";

		NSArray *po_list        = [data valueForKey:@"list"];
		NSArray *po_item_list   = [data valueForKey:@"item_list"];

		for (id po in po_list)
		{
			if ([self record_already_exists : @"PURCHASE_ORDER"
								with_filter           : [NSString stringWithFormat:@" po_id = \"%@\"", [po valueForKey:@"po_id"]]
					 ])
			{
				execute_query = [NSString stringWithFormat:
												 @"UPDATE PURCHASE_ORDER"
												 " SET "
												 " po_no                 = '%@',"
												 " supplier_code         = '%@',"
												 " supplier_name         = '%@',"
												 " outright_consignment  = '%@',"
												 " status_id             = '%@',"
												 " company_id            = '%@',"
												 " sync_id               = '%@',"
												 " last_sync_time        = '%@',"
												 " created_by            = '%@',"
												 " created_date          = '%@',"
												 " updated_by            = '%@',"
												 " updated_date          = '%@',"
												 " active_status         = '%@'"
												 " WHERE po_id = '%@'",
												 [po valueForKey:@"po_no"],
												 [po valueForKey:@"supplier_code"],
												 [po valueForKey:@"supplier_name"],
												 [po valueForKey:@"po_type"],
												 [po valueForKey:@"status_id"],
												 [po valueForKey:@"company_id"],
												 [po valueForKey:@"sync_id"],
												 [po valueForKey:@"last_sync_time"],
												 [po valueForKey:@"created_by"],
												 [po valueForKey:@"created_date"],
												 [po valueForKey:@"updated_by"],
												 [po valueForKey:@"updated_date"],
												 [po valueForKey:@"active_status"],
												 [po valueForKey:@"po_id"]
												 ];
			}
			else
			{
				execute_query = [NSString stringWithFormat:
												 @"INSERT INTO PURCHASE_ORDER"
												 "("
												 "po_id,"
												 "po_no,"
												 "supplier_code,"
												 "supplier_name,"
												 "outright_consignment,"
												 "status_id,"
												 "company_id,"
												 "sync_id,"
												 "last_sync_time,"
												 "created_by,"
												 "created_date,"
												 "updated_by,"
												 "updated_date,"
												 "active_status"
												 ") "
												 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
												 [po valueForKey:@"po_id"],
												 [po valueForKey:@"po_no"],
												 [po valueForKey:@"supplier_code"],
												 [po valueForKey:@"supplier_name"],
												 [po valueForKey:@"po_type"],
												 [po valueForKey:@"status_id"],
												 [po valueForKey:@"company_id"],
												 [po valueForKey:@"sync_id"],
												 [po valueForKey:@"last_sync_time"],
												 [po valueForKey:@"created_by"],
												 [po valueForKey:@"created_date"],
												 [po valueForKey:@"updated_by"],
												 [po valueForKey:@"updated_date"],
												 [po valueForKey:@"active_status"]
												 ];
			}

			[array_of_query addObject:execute_query];
		}

		[self add_edit_po_receive_item:po_item_list];

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - PURCHASE_ORDER_ITEM
- (NSArray *)get_po_receive_item_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "po_item_no, po_id, quantity, item_id, remarks,is_temp,company_id,"
											 "sync_id, last_sync_time, created_by, created_date, updated_by,updated_date, active_status"
											 " FROM PURCHASE_ORDER_ITEM where sync_id is null"];

		NSMutableDictionary *po_item;
		NSMutableArray      *po_item_array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet      = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			po_item = [[NSMutableDictionary alloc] init];
			po_item[@"po_item_no"] = dict[@"po_item_no"];
			po_item[@"po_id"] = dict[@"po_id"];
			po_item[@"quantity"] = dict[@"quantity"];
			po_item[@"item_id"] = dict[@"item_id"];
			po_item[@"remarks"] = [self object:dict[@"remarks"] or:empty_string];
			po_item[@"is_temp"] = dict[@"is_temp"];
			po_item[@"company_id"] = dict[@"company_id"];
			po_item[@"sync_id"] = [self object:dict[@"sync_id"] or:empty_string];
			po_item[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			po_item[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			po_item[@"created_date"] = dict[@"created_date"];
			po_item[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			po_item[@"updated_date"] = dict[@"updated_date"];
			po_item[@"active_status"] = dict[@"active_status"];
			[po_item_array addObject:po_item];
		}


		return po_item_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)add_edit_po_receive_item:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *execute_query = @"";

		for (id data_item in data)
		{
			if ([self record_already_exists : @"PURCHASE_ORDER_ITEM"
								with_filter           : [NSString stringWithFormat:@" po_item_no = \"%@\"", [data_item valueForKey:@"item_no"]]
					 ])
			{
				execute_query = [NSString stringWithFormat:
												 @"UPDATE PURCHASE_ORDER_ITEM"
												 " SET "
												 " po_id             = '%@',"
												 " quantity          = '%@',"
												 " item_id           = '%@',"
												 " remarks           = '%@',"
												 " is_temp           = '%@',"
												 " company_id        = '%@',"
												 " sync_id           = '%@',"
												 " last_sync_time    = '%@',"
												 " created_by        = '%@',"
												 " created_date      = '%@',"
												 " updated_by        = '%@',"
												 " updated_date      = '%@',"
												 " active_status     = '%@'"
												 " WHERE po_item_no = '%@' ",
												 [data_item valueForKey:@"item_po_id"],
												 [data_item valueForKey:@"item_quantity"],
												 [data_item valueForKey:@"item_id"],
												 [data_item valueForKey:@"item_remarks"],
												 [data_item valueForKey:@"item_is_temp"],
												 [data_item valueForKey:@"item_company_id"],
												 [data_item valueForKey:@"item_sync_id"],
												 [data_item valueForKey:@"item_last_sync_time"],
												 [data_item valueForKey:@"item_created_by"],
												 [data_item valueForKey:@"item_created_date"],
												 [data_item valueForKey:@"item_updated_by"],
												 [data_item valueForKey:@"item_updated_date"],
												 [data_item valueForKey:@"item_active_status"],
												 [data_item valueForKey:@"item_no"]
												 ];
			}
			else
			{
				execute_query = [NSString stringWithFormat:
												 @"INSERT INTO PURCHASE_ORDER_ITEM"
												 "("
												 "po_item_no,"
												 "po_id,"
												 "quantity,"
												 "item_id,"
												 "remarks,"
												 "is_temp,"
												 "company_id,"
												 "sync_id,"
												 "last_sync_time,"
												 "created_by,"
												 "created_date,"
												 "updated_by,"
												 "updated_date,"
												 "active_status"
												 ") "
												 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
												 [data_item valueForKey:@"item_no"],
												 [data_item valueForKey:@"item_po_id"],
												 [data_item valueForKey:@"item_quantity"],
												 [data_item valueForKey:@"item_id"],
												 [data_item valueForKey:@"item_remarks"],
												 [data_item valueForKey:@"item_is_temp"],
												 [data_item valueForKey:@"item_company_id"],
												 [data_item valueForKey:@"item_sync_id"],
												 [data_item valueForKey:@"item_last_sync_time"],
												 [data_item valueForKey:@"item_created_by"],
												 [data_item valueForKey:@"item_created_date"],
												 [data_item valueForKey:@"item_updated_by"],
												 [data_item valueForKey:@"item_updated_date"],
												 [data_item valueForKey:@"item_active_status"]
												 ];
			}

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - CASH_REGISTER
- (NSArray *)get_cash_register_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "cash_reg_id,cash_type,amount,member_id,create_time,tran_sync_id,"
											 "user_id, trx_id, notes, company_id,"
											 "updated_by, updated_date,tran_sync_id,last_sync_time,created_by,created_date, active_status"
											 " FROM CASH_REGISTER where sync_id is null"];
		NSArray *resultSet  = [self get_from_queue:query];

		NSMutableDictionary *cs;
		NSMutableArray      *cs_array   = [[NSMutableArray alloc] init];

		for(NSDictionary *dict in resultSet)
		{
			cs = [[NSMutableDictionary alloc] init];
			cs[@"cash_reg_id"]  = dict[@"cash_reg_id"];
			cs[@"cash_type"]    = [self object:dict[@"cash_type"] or:empty_string];
			cs[@"amount"]       = [self object:dict[@"amount"] or:empty_string];
			cs[@"member_id"]    = [self object:dict[@"member_id"] or:empty_string];
			cs[@"create_time"]  = [self object:dict[@"create_time"] or:empty_string];
			cs[@"user_id"]      = [self object:dict[@"user_id"] or:empty_string];
			cs[@"trx_id"]       = [self object:dict[@"trx_id"] or:empty_string];
			cs[@"notes"]        = [self object:dict[@"notes"] or:empty_string];
			cs[@"company_id"]   = [self object:dict[@"company_id"] or:empty_string];
			cs[@"updated_by"]   = [self object:dict[@"updated_by"] or:empty_string];
			cs[@"updated_time"] = [self object:dict[@"updated_date"] or:empty_string];

			cs[@"tran_sync_id"] = [self object:dict[@"tran_sync_id"] or:empty_string];
			cs[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			cs[@"created_by"]   = [self object:dict[@"created_by"] or:empty_string];
			cs[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];

			cs[@"active_status"] = dict[@"active_status"];
			[cs_array addObject:cs];
		}


		NSLog(@"%@",cs_array);

		return cs_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)sync_cash_register:(NSArray *)data
{
	NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

	for (NSDictionary *cash_reg_data in data)
	{
		NSString *execute_query = [NSString stringWithFormat:
															 @"REPLACE INTO CASH_REGISTER"
															 "(cash_reg_id,"
															 "cash_type,"
															 "amount,"
															 "member_id,"
															 "create_time,"
															 "tran_sync_id,"
															 "user_id,"
															 "trx_id,"
															 "notes,"
															 "company_id,"
															 "sync_id,"
															 "last_sync_time,"
															 "created_by,"
															 "created_date,"
															 "updated_by,"
															 "updated_date,"
															 "active_status)"
															 @" VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
															 cash_reg_data[@"id"],
															 cash_reg_data[@"cash_type"],
															 cash_reg_data[@"amount"],
															 cash_reg_data[@"member_id"],
															 cash_reg_data[@"created_date"],
															 cash_reg_data[@"sync_id"],
															 cash_reg_data[@"user_id"],
															 cash_reg_data[@"trx_id"],
															 cash_reg_data[@"notes"],
															 cash_reg_data[@"company_id"],
															 cash_reg_data[@"sync_id"],
															 cash_reg_data[@"last_sync_time"],
															 cash_reg_data[@"created_date"],
															 cash_reg_data[@"created_by"],
															 cash_reg_data[@"updated_by"],
															 cash_reg_data[@"updated_date"],
															 cash_reg_data[@"active_status"]];

		[array_of_query addObject:execute_query];
	}

	[self add_to_queue:array_of_query with_priority:low];
}

- (void)edit_cash_register_from_sync:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"cash_reg_id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE CASH_REGISTER SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE cash_reg_id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"cash_reg_id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - CATEGORIES
- (NSArray *)get_new_category_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "category_id,category_name,category_image,"
											 "updated_date,company_id,datetime,sort_index,last_sync_time,created_by,created_date,updated_by,active_status"
											 " FROM CATEGORIES where sync_id is null"];

		NSMutableDictionary *category_data;
		NSMutableArray      *category_array   = [[NSMutableArray alloc] init];

		NSArray        *resultSet  = [self get_from_queue:query];


		for(NSDictionary *dict in resultSet)
		{
			category_data = [[NSMutableDictionary alloc] init];
			category_data[@"category_id"] = dict[@"category_id"];
			category_data[@"category_name"] = dict[@"category_name"];
			category_data[@"category_image"] = [self object:dict[@"category_image"] or:empty_string];
			category_data[@"updated_time"] = [self object:dict[@"updated_date"] or:empty_string];
			category_data[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];

			category_data[@"datetime"] = [self object:dict[@"datetime"] or:empty_string];
			category_data[@"sort_index"] = [self object:dict[@"sort_index"] or:empty_string];
			category_data[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			category_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			category_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			category_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];

			category_data[@"active_status"] = dict[@"active_status"];

			NSData *image_data = [FileUtils getDataFromDocDirWithPath:[NSString stringWithFormat:@"%@/%@",POSImages,IMG_CATEGORIES] fileName:[NSString stringWithFormat:@"%@_cat_image",dict[@"category_name"]] andExtn:@"jpg"];

			if (image_data)
			{
				category_data[@"image"] = [UIUtils convert_base_64:[UIImage imageWithData:image_data]];
			}
			else
			{
				category_data[@"image"] = @"";
			}

			[category_array addObject:category_data];
		}

		return category_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)sync_menus:(NSArray*)data
{
	NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

	for (int i = 0; i < data.count; i++)
	{
		NSArray *item = data[i];

		NSString *execute_query = [NSString stringWithFormat:
															 @"REPLACE INTO CATEGORIES "
															 "(category_id,"
															 "category_name,"
															 "category_image,"
															 "datetime,"
															 "sort_index,"
															 "company_id,"
															 "sync_id,"
															 "created_by,"
															 "created_date,"
															 "updated_by,"
															 "updated_date,"
															 "active_status)"
															 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
															 [item valueForKey:@"id"],
															 [UIUtils remove_encoded_value:[item valueForKey:@"category"]],
															 [item valueForKey:@"image"],
															 [item valueForKey:@"datetime"],
															 [item valueForKey:@"sort_index"],
															 [StringUtils get_business_detail:@"business_id"],
															 @"",
															 @"",
															 @"",
															 @"",
															 @"",
															 [item valueForKey:@"active_status"]
															 ];

		[array_of_query addObject:execute_query];

		[self sync_items:data[i][@"items"] withCatId:[item valueForKey:@"id"]];
	}

	[self add_to_queue:array_of_query with_priority:low];
}

- (void)update_synced_categories:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"category_id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE CATEGORIES SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE category_id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"category_id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - ITEM
- (NSArray *)get_new_items_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "item_id,"
											 "item_name,"
											 "description,"
											 "item_image,"
											 "price,"
											 "cost_price,"
											 "selling_price,"
											 "gst_code,"
											 "gst_rate,"
											 "item_cat_id,"
											 "company_id,"
											 "updated_date,"
											 "active_status,"

											 "online_order,"
											 "image_last_updated,"
											 "inclusive_price,"
											 "stock_monitoring,"
											 "stock_left,"
											 "stock_min,"
											 "stock_ideal,"
											 "timestamp,"
											 "sort_index,"
											 "customfield1,"
											 "customfield2,"
											 "sku,"
											 "barcode,"
											 "parent,"
											 "source,"
											 "brand,"
											 "supplier_id,"
											 "term,"
											 "margin,"
											 "item_code,"
											 "size,"
											 "color,"
											 "season,"
											 "remarks1,"
											 "remarks2,"
											 "currency_type,"
											 "category_tags,"
											 "type_of_price,"
											 "type_of_unit,"
											 "last_sync_time,"
											 "created_by,"
											 "created_date,"
											 "updated_by"

											 " FROM ITEMS where sync_id is null"];

		NSMutableDictionary *item_data;
		NSMutableArray      *item_array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			item_data = [[NSMutableDictionary alloc] init];
			item_data[@"item_id"] = dict[@"item_id"];
			item_data[@"item_name"] = dict[@"item_name"];
			item_data[@"description"] = [self object:dict[@"description"] or:empty_string];
			item_data[@"item_image"] = [self object:dict[@"item_image"] or:empty_string];
			item_data[@"price"] = [self object:dict[@"price"] or:empty_string];
			item_data[@"cost_price"] = [self object:dict[@"cost_price"] or:empty_string];
			item_data[@"selling_price"] = [self object:dict[@"selling_price"] or:empty_string];
			item_data[@"gst_code"] = [self object:dict[@"gst_code"] or:empty_string];
			item_data[@"gst_rate"] = [self object:dict[@"gst_rate"] or:empty_string];
			item_data[@"item_cat_id"] = [self object:dict[@"item_cat_id"] or:empty_string];
			item_data[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			item_data[@"updated_time"] = [self object:dict[@"updated_date"] or:empty_string];
			item_data[@"active_status"] = dict[@"active_status"];

			item_data[@"online_order"] = [self object:dict[@"online_order"] or:empty_string];
			item_data[@"image_last_updated"] = [self object:dict[@"image_last_updated"] or:empty_string];
			item_data[@"inclusive_price"] = [self object:dict[@"inclusive_price"] or:empty_string];
			item_data[@"stock_monitoring"] = [self object:dict[@"stock_monitoring"] or:empty_string];
			item_data[@"stock_left"] = [self object:dict[@"stock_left"] or:empty_string];
			item_data[@"stock_min"] = [self object:dict[@"stock_min"] or:empty_string];
			item_data[@"stock_ideal"] = [self object:dict[@"stock_ideal"] or:empty_string];
			item_data[@"timestamp"] = [self object:dict[@"timestamp"] or:empty_string];
			item_data[@"sort_index"] = [self object:dict[@"sort_index"] or:empty_string];
			item_data[@"customfield1"] = [self object:dict[@"customfield1"] or:empty_string];
			item_data[@"customfield2"] = [self object:dict[@"customfield2"] or:empty_string];
			item_data[@"sku"] = [self object:dict[@"sku"] or:empty_string];
			item_data[@"barcode"] = [self object:dict[@"barcode"] or:empty_string];
			item_data[@"parent"] = [self object:dict[@"parent"] or:empty_string];
			item_data[@"source"] = [self object:dict[@"source"] or:empty_string];
			item_data[@"brand"] = [self object:dict[@"brand"] or:empty_string];
			item_data[@"supplier_id"] = [self object:dict[@"supplier_id"] or:empty_string];
			item_data[@"term"] = [self object:dict[@"term"] or:empty_string];
			item_data[@"margin"] = [self object:dict[@"margin"] or:empty_string];
			item_data[@"item_code"] = [self object:dict[@"item_code"] or:empty_string];
			item_data[@"size"] = [self object:dict[@"size"] or:empty_string];
			item_data[@"color"] = [self object:dict[@"color"] or:empty_string];
			item_data[@"season"] = [self object:dict[@"season"] or:empty_string];
			item_data[@"remarks1"] = [self object:dict[@"remarks1"] or:empty_string];
			item_data[@"remarks2"] = [self object:dict[@"remarks2"] or:empty_string];
			item_data[@"currency_type"] = [self object:dict[@"currency_type"] or:empty_string];
			item_data[@"category_tags"] = [self object:dict[@"category_tags"] or:empty_string];
			item_data[@"type_of_price"] = [self object:dict[@"type_of_price"] or:empty_string];
			item_data[@"type_of_unit"] = [self object:dict[@"type_of_unit"] or:empty_string];
			item_data[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			item_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			item_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			item_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];


			NSData *image_data = [FileUtils getDataFromDocDirWithPath:[NSString stringWithFormat:@"%@/%@",POSImages,IMG_ITEMS] fileName:[NSString stringWithFormat:@"%@_%@itemimage",dict[@"item_cat_id"],dict[@"item_name"]] andExtn:@"jpg"];

			if (image_data)
			{
				item_data[@"image"] = [UIUtils convert_base_64:[UIImage imageWithData:image_data]];
			}
			else
			{
				item_data[@"image"] = @"";

			}
			[item_array addObject:item_data];
		}

		return item_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)sync_items:(NSArray*)data withCatId:(NSString *)category_id
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			NSArray *item = data[i];
			NSString *image_name = [item valueForKey:@"image"];

			if (image_name)
			{
				if([FileUtils contains_string:image_name])
				{
					NSRange name_range = [image_name rangeOfString:@"/" options:NSBackwardsSearch];
					image_name = [image_name substringFromIndex:name_range.location+1];
				}
			}

			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO ITEMS "
																 "(item_id,"
																 "item_name,"
																 "description,"
																 "online_order,"
																 "item_image,"
																 "image_last_updated,"
																 "inclusive_price,"
																 "price,"
																 "cost_price,"
																 "selling_price,"
																 "gst_code,"
																 "gst_rate,"

																 "stock_monitoring,"
																 "stock_left,"
																 "stock_min,"
																 "stock_ideal,"
																 "timestamp,"
																 "sort_index,"
																 "customfield1,"
																 "customfield2,"
																 "sku,"
																 "barcode,"

																 "parent,"
																 "source,"
																 "brand,"
																 "supplier_id,"
																 "term,"
																 "margin,"
																 "item_code,"
																 "size,"
																 "color,"
																 "season,"
																 "type_of_price,"
																 "type_of_unit,"

																 "remarks1,"
																 "remarks2,"
																 "currency_type,"
																 "category_tags,"
																 "item_cat_id,"
																 "company_id,"
																 "sync_id,"
																 "created_by,"
																 "created_date,"
																 "updated_by,"

																 "updated_date,"
																 "active_status)"
																 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',"
																 "'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@', "
																 "'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@',"
																 "'%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
																 [item valueForKey:@"id"],
																 [[item valueForKey:@"name"] stringByReplacingOccurrencesOfString: @"\"" withString: @"``"],
																 [[item valueForKey:@"description"] stringByReplacingOccurrencesOfString: @"\"" withString: @"``"],
																 [item valueForKey:@"online_order"],
																 image_name ? image_name: @"",
																 [item valueForKey:@"image_last_updated"],
																 [item valueForKey:@"inclusive_price"],
																 [item valueForKey:@"selling_price"],
																 [item valueForKey:@"cost_price"],
																 [item valueForKey:@"selling_price"],
																 SESSION_DATA.gst_enable ? [item valueForKey:@"gst_code"] : @" ",
																 SESSION_DATA.gst_enable ? [item valueForKey:@"gst_rate"] : @"0",

																 [item valueForKey:@"stock_monitoring"],
																 [item valueForKey:@"stock_left"],
																 [item valueForKey:@"stock_min"],
																 [item valueForKey:@"stock_ideal"],
																 [item valueForKey:@"timestamp"],
																 [item valueForKey:@"sort_index"],
																 [item valueForKey:@"customfield1"],
																 [item valueForKey:@"customfield2"],
																 [item valueForKey:@"sku"],
																 [item valueForKey:@"barcode"],

																 [item valueForKey:@"parent"],
																 [item valueForKey:@"source"],
																 [item valueForKey:@"brand"],
																 [item valueForKey:@"supplier_id"],
																 [item valueForKey:@"term"],
																 [item valueForKey:@"margin"],
																 [item valueForKey:@"item_code"],
																 [item valueForKey:@"size"],
																 [item valueForKey:@"color"],
																 [item valueForKey:@"season"],
																 [item valueForKey:@"type_of_price"] ? [item valueForKey:@"type_of_price"]:@"Fixed Price",
																 [item valueForKey:@"type_of_unit"] ? [item valueForKey:@"type_of_unit"] : @"",

																 [item valueForKey:@"remark1"],
																 [item valueForKey:@"remark2"],
																 [item valueForKey:@"currency_type"],
																 [item valueForKey:@"category_tags"],
																 category_id,
																 [StringUtils get_business_detail:@"business_id"],
																 @"",
																 @"",
																 @"",
																 @"",
																 @"",
																 [item valueForKey:@"active_status"]
																 ];

			[array_of_query addObject:execute_query];

			if ([item valueForKey:@"image"])
			{
				[FileUtils download_image:[item valueForKey:@"image"] withStoringPath:IMG_ITEMS];
			}

				// sort out data to be insert to modifier field
			int child_cnt                       = 0;
			NSArray *child_item;
			NSMutableArray *addition_array      = [[NSMutableArray alloc] initWithArray:[item valueForKey:@"addition"]];
			NSMutableArray *choice_array        = [[NSMutableArray alloc] initWithArray:[item valueForKey:@"choice"]];
			NSMutableDictionary *item_choice;
			NSMutableArray *modifiers_array     = [[NSMutableArray alloc] init];

			for (int choice_cnt = 0; choice_cnt < choice_array.count; choice_cnt++)
			{
				child_item = [choice_array[choice_cnt] valueForKey:@"child"];

				for (child_cnt = 0; child_cnt < child_item.count; child_cnt++)
				{
					item_choice = [[NSMutableDictionary alloc] init];
					item_choice[@"childid"] = [child_item[child_cnt]    valueForKey:@"childid"];
					item_choice[@"childname"] = [child_item[child_cnt]    valueForKey:@"childname"];
					item_choice[@"childprice"] = [child_item[child_cnt]    valueForKey:@"child_inclusive_price"];
					item_choice[@"parentid"] = [choice_array[choice_cnt] valueForKey:@"parentid"];
					item_choice[@"parentname"] = [choice_array[choice_cnt] valueForKey:@"parentname"];
					item_choice[@"mod_item_id"] = [item valueForKey:@"id"];
					item_choice[@"mod_type"] = CHOICE;
					item_choice[@"active_status"] = [child_item[child_cnt]    valueForKey:@"active_status"];

					[modifiers_array addObject:item_choice];
				}
			}

			for (int add_cnt = 0; add_cnt < addition_array.count; add_cnt++)
			{
				child_item = [addition_array[add_cnt] valueForKey:@"child"];

				for (child_cnt = 0; child_cnt < child_item.count; child_cnt++)
				{
					item_choice = [[NSMutableDictionary alloc] init];
					item_choice[@"childid"] = [child_item[child_cnt]    valueForKey:@"childid"];
					item_choice[@"childname"] = [child_item[child_cnt]    valueForKey:@"childname"];
					item_choice[@"childprice"] = [child_item[child_cnt]    valueForKey:@"child_inclusive_price"];
					item_choice[@"parentid"] = [addition_array[add_cnt]  valueForKey:@"parentid"];
					item_choice[@"parentname"] = [addition_array[add_cnt]  valueForKey:@"parentname"];
					item_choice[@"mod_item_id"] = [item valueForKey:@"id"];
					item_choice[@"mod_type"] = ADDITION;
					item_choice[@"active_status"] = [child_item[child_cnt]    valueForKey:@"active_status"];
					[modifiers_array addObject:item_choice];
				}
			}

			[self sync_modifiers:modifiers_array];

		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)update_synced_items:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"item_id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE ITEMS SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE item_id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"item_id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - MODIFIERS
- (void)sync_modifiers:(NSArray*)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *mod_item_id;

		for (int i = 0; i < data.count; i++)
		{
			NSArray *item = data[i];

			mod_item_id = [item valueForKey:@"mod_item_id"];

			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO MODIFIERS "
																 "(mod_id,"
																 "mod_type,"
																 "mod_name,"
																 "mod_price,"
																 "mod_parent_id,"
																 "mod_parent_name,"
																 "mod_item_id,"

																 "company_id,"
																 "sync_id,"
																 "created_by,"
																 "created_date,"
																 "updated_by,"
																 "updated_date,"
																 "active_status)"
																 @"VALUES ('%@','%@','%@','%@','%@','%@','%@', '%@','%@','%@','%@','%@','%@','%@')",
																 [item valueForKey:@"childid"],
																 [item valueForKey:@"mod_type"],
																 [item valueForKey:@"childname"],
																 [item valueForKey:@"childprice"],
																 [item valueForKey:@"parentid"],
																 [item valueForKey:@"parentname"],
																 [item valueForKey:@"mod_item_id"],

																 [StringUtils get_business_detail:@"business_id"],
																 @"",
																 @"",
																 @"",
																 @"",
																 @"",
																 [item valueForKey:@"active_status"]
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];

		NSMutableArray *all_modifier = [self get_all_modifiers:mod_item_id];

		for (NSArray *child_item in all_modifier)
		{
			Boolean found = false;

			for(NSArray *item in data)
			{
				if([[child_item valueForKey:@"mod_id"] isEqualToString:[item valueForKey:@"childid"]])
				{
					found = YES;
					break;
				}
			}

			if(!found)
			{
				[self set_modifier_inactive:[child_item valueForKey:@"mod_id"]];
			}
		}
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

-(void)set_modifier_inactive:(NSString *) mod_id
{
	@try
	{
		NSString *execute_query = [NSString stringWithFormat:
															 @"UPDATE MODIFIERS SET "
															 "active_status  = '%@' "
															 "WHERE mod_id   = '%@' ",
															 @"0",
															 mod_id];

		[self add_to_queue:execute_query];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (NSMutableArray *)get_all_modifiers:(NSString *)mod_item_id
{
	@try
	{
			// Choice
		NSString *query = [NSString stringWithFormat:@"select mod_id, mod_type"
											 " FROM MODIFIERS where mod_item_id = %@", mod_item_id];

		NSMutableArray      *modifiers_array    = [[NSMutableArray alloc] init];
		NSMutableDictionary *child_modifier;

		NSArray        *resultSet          = [self get_from_queue:query];


		for(NSDictionary *dict in resultSet)
		{
			child_modifier = [[NSMutableDictionary alloc] init];
			child_modifier[@"mod_id"] = dict[@"mod_id"];
			child_modifier[@"mod_type"] = dict[@"mod_type"];

			[modifiers_array addObject:child_modifier];
		}



		return modifiers_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - AREA
- (void)add_area:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (NSDictionary *dat in data)
		{
			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO AREA"
																 "(id,"
																 "area_name,"
																 "tba_sequence_no,"
																 "sync_id)"
																 @"VALUES ('%@','%@','%@','manual-sync-id')",
																 dat[@"id"],
																 dat[@"name"],
																 dat[@"sequence_no"] ?: @"1"
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch(NSException *e)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

- (NSArray *)getAreaForSync
{
	@try
	{
		NSString *query = @"SELECT id,tba_sequence_no FROM AREA WHERE sync_id is null";

		NSMutableArray  *array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			NSMutableDictionary *finalDict = [[NSMutableDictionary alloc] init];
			finalDict[@"id"] = dict[@"id"];
			finalDict[@"sequence_no"] = dict[@"tba_sequence_no"];
			finalDict[@"company_id"] = [StringUtils get_business_detail:@"business_id"];

			[array addObject:finalDict];
		}


		NSLog(@"%@",array);

		return array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)updateAreaSyncId:(NSArray *)data
{
	NSLog(@"new area sync id responese : %@",data);
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE AREA SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - TABLES
- (NSArray *)get_new_table_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "id,"
											 "table_area,"
											 "IFNULL(area_id,'') as area_id,"
											 "table_color,"
											 "table_no,"
											 "table_shape,"
											 "table_x,"
											 "table_y,"
											 "total_chair,"
											 "IFNULL(company_id,'') as company_id,"
											 "created_date,"
											 "IFNULL(updated_by,'') as updated_by,"
											 "updated_date,"
											 "last_sync_time,"

											 "IFNULL(created_by,'') as created_by,"
											 "active_status,"
											 "order_id,"
											 "total_amount,"
											 "tbl_sequence_no"

											 " FROM TABLES where sync_id is null or sync_id is \"\""];

		NSMutableDictionary *table_data;
		NSMutableArray      *table_array   = [[NSMutableArray alloc] init];

		NSArray        *resultSet  = [self get_from_queue:query];


		for(NSDictionary *dict in resultSet)
		{
			table_data                      = [[NSMutableDictionary alloc] init];
			table_data[@"id"]               = dict[@"id"];
			table_data[@"area"]             = dict[@"table_area"];
			table_data[@"area_id"]          = dict[@"area_id"];
			table_data[@"table_color"]      = dict[@"table_color"];
			table_data[@"number"]           = dict[@"table_no"];
			table_data[@"shape"]            = dict[@"table_shape"];
			table_data[@"table_x"]          = dict[@"table_x"];
			table_data[@"table_y"]          = dict[@"table_y"];
			table_data[@"total_chair"]      = dict[@"total_chair"];
			table_data[@"company_id"]       = [StringUtils get_business_detail:@"business_id"];
			table_data[@"create_time"]      = dict[@"created_date"];
			table_data[@"updated_by"]       = dict[@"updated_by"];
			table_data[@"updated_time"]     = dict[@"updated_date"];
			table_data[@"last_sync_time"]   = dict[@"last_sync_time"];

			table_data[@"created_by"]       = dict[@"created_by"];
			table_data[@"active_status"]    = dict[@"active_status"];
			table_data[@"order_id"]         = dict[@"order_id"];
			table_data[@"total_amount"]     = dict[@"total_amount"];
			table_data[@"sequence_no"]      = dict[@"tbl_sequence_no"];

			NSLog(@"tab data : %@",table_data);

			[table_array addObject:table_data];
		}


		return table_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)add_table:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (NSDictionary *dat in data)
		{
			NSString *areaId = dat[@"area_id"] ?: @"";

			if (![areaId isEqualToString:@""])
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"REPLACE INTO TABLES"
																	 "(id,"
																	 "table_no,"
																	 "total_chair,"
																	 "table_area,"
																	 "order_id,"
																	 "total_amount,"
																	 "table_shape,"
																	 "table_x,"
																	 "table_y,"
																	 "active_status,"
																	 "sync_id,"
																	 "tbl_sequence_no,"
																	 "area_id)"
																	 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
																	 dat[@"id"],
																	 dat[@"number"],
																	 dat[@"total_chair"],
																	 dat[@"area"],
																	 @"",
																	 @"",
																	 dat[@"shape"],
																	 dat[@"table_x"],
																	 dat[@"table_y"],
																	 dat[@"active_status"],
																	 @"manual-sync-id",
																	 dat[@"sequence_no"] ?: @"1",
																	 areaId
																	 ];
				[array_of_query addObject:execute_query];
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch(NSException *e)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

- (void)update_new_tables:(NSArray *)data
{
	NSLog(@"new table responese : %@",data);
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE TABLES SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - MEMBERS
- (NSArray *)get_customers_data_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "user_id,"
											 "cust_id,"
											 "cust_firstName,"
											 "cust_lastName,"
											 "cust_image,"
											 "cust_checkin_id,"
											 "cust_member_id,"
											 "cust_level,"
											 "cust_last_visit,"
											 "cust_manis_pts,"
											 "cust_fav_cat,"
											 "cust_fav_item,"
											 "cust_total_spent,"
											 "cust_email,"
											 "cust_mobile,"
											 "cust_address,"
											 "cust_birthdate,"
											 "cust_gender,"
											 "cust_city,"
											 "cust_postcode,"
											 "company_id,"
											 "last_sync_time,"
											 "created_by,"
											 "created_date,"
											 "updated_by,"
											 "updated_date,"
											 "active_status,"
											 "cust_image_changed"
											 " FROM MEMBERS where sync_id is null"];

		NSMutableDictionary *cust_data;
		NSMutableArray      *cust_array   = [[NSMutableArray alloc] init];

		NSArray        *resultSet  = [self get_from_queue:query];


		for(NSDictionary *dict in resultSet)
		{
			cust_data = [[NSMutableDictionary alloc] init];
			cust_data[@"user_id"] = dict[@"user_id"];
			cust_data[@"cust_id"] = [self object:dict[@"cust_id"] or:empty_string];
			cust_data[@"cust_firstName"] = [self object:dict[@"cust_firstName"] or:empty_string];
			cust_data[@"cust_lastName"] = [self object:dict[@"cust_lastName"] or:empty_string];
			cust_data[@"cust_checkin_id"] = [self object:dict[@"cust_checkin_id"] or:empty_string];
			cust_data[@"cust_member_id"] = [self object:dict[@"cust_member_id"] or:empty_string];
			cust_data[@"cust_level"] = [self object:dict[@"cust_level"] or:empty_string];
			cust_data[@"cust_last_visit"] = [self object:dict[@"cust_last_visit"] or:empty_string];
			cust_data[@"cust_manis_pts"] = [self object:dict[@"cust_manis_pts"] or:empty_string];
			cust_data[@"cust_fav_cat"] = [self object:dict[@"cust_fav_cat"] or:empty_string];
			cust_data[@"cust_fav_item"] = [self object:dict[@"cust_fav_item"] or:empty_string];
			cust_data[@"cust_total_spent"] = [self object:dict[@"cust_total_spent"] or:empty_string];
			cust_data[@"cust_email"] = [self object:dict[@"cust_email"] or:empty_string];
			cust_data[@"cust_mobile"] = [self object:dict[@"cust_mobile"] or:empty_string];
			cust_data[@"cust_address"] = [self object:dict[@"cust_address"] or:empty_string];
			cust_data[@"cust_birthdate"] = [self object:dict[@"cust_birthdate"] or:empty_string];
			cust_data[@"cust_gender"] = [self object:dict[@"cust_gender"] or:empty_string];
			cust_data[@"cust_city"] = [self object:dict[@"cust_city"] or:empty_string];
			cust_data[@"cust_postcode"] = [self object:dict[@"cust_postcode"] or:empty_string];
			cust_data[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			cust_data[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			cust_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			cust_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			cust_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			cust_data[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			cust_data[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			cust_data[@"cust_image"] = @"";
			if ([dict[@"cust_image_changed"] isEqualToString:@"1"])
			{
				if ([dict[@"cust_image"] isEqualToString:CUSTOMER_DEFAULT_PIC])
				{
					cust_data[@"cust_image"] = @"default";
				}
				else
				{
					NSData *image_data =
					[FileUtils getDataFromDocDirWithPath:[NSString stringWithFormat:@"%@/%@",POSImages,IMG_CUSTOMERS]
																			fileName:[NSString stringWithFormat:@"%@_customerImage",dict[@"user_id"]]
																			 andExtn:@"jpg"];
					if (image_data)
					{
						NSLog(@"Size of Image(bytes):%lu",(unsigned long)[image_data length]);

						if ([image_data length] < 200000) //only accept image smaller than 200KB
						{
							cust_data[@"cust_image"] = [UIUtils convert_base_64:[UIImage imageWithData:image_data]];
						}
					}
				}
			}

			[cust_array addObject:cust_data];
		}


		NSLog(@"%@",cust_array);

		return cust_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)sync_customer_details:(NSArray *) data //TODO: use high priority in queue like insert transaction later
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *execute_query = @"";
		int counter = 0;

		if ([data count] > 0)
		{
			NSString *extraQuery = @"UPDATE MEMBERS SET active_status = '0'";
			[array_of_query addObject:extraQuery];
		}

		for (NSDictionary *customer in data)
		{
			counter++;
			NSString *photo = [customer valueForKey:@"photo"];
			NSString *image_name = CUSTOMER_DEFAULT_PIC;

			if (photo)
			{
				if([FileUtils contains_string:photo] && ([photo length] < 200) )
				{
					NSRange name_range = [photo rangeOfString:@"/" options:NSBackwardsSearch];
					NSString *photo_name = [photo substringFromIndex:name_range.location+1];
					if (![photo_name isEqualToString:@"default_people.jpg"])
					{
						image_name = photo_name;
					}
				}
			}

			NSString *cust_id = [NSString stringWithFormat:@"%@",[customer valueForKey:@"cust_id"]];
			NSString *sync_id = [NSString stringWithFormat:@"%@",[customer valueForKey:@"sync_id"]];
			NSString *customer_member_id = [NSString stringWithFormat:@"%@",[customer valueForKey:@"member_id"]];
			NSString *user_id = [NSString stringWithFormat:@"%@%d",CURRENT_TIMESTAMP,counter];

			NSString *validate_query = [NSString stringWithFormat:@"SELECT cust_id from MEMBERS where cust_id='%@'", cust_id];
			NSArray*resultSet      = [self get_from_queue:validate_query];

			if(resultSet.count > 0)
			{
				execute_query = [NSString stringWithFormat:
												 @"UPDATE MEMBERS SET "
												 "user_id            = '%@',"
												 "cust_firstName     = '%@',"
												 "cust_lastName      = '%@',"
												 "cust_image         = '%@',"
												 "cust_checkin_id    = '%@',"
												 "cust_member_id     = '%@',"
												 "cust_level         = '%@',"
												 "cust_total_spent   = '%@',"
												 "cust_email         = '%@',"
												 "cust_mobile        = '%@',"
												 "cust_address       = '%@',"
												 "cust_birthdate     = '%@',"
												 "cust_gender        = '%@',"
												 "cust_city          = '%@',"
												 "cust_postcode      = '%@',"
												 "sync_id            = '%@',"
												 "active_status      = '%@',"
												 "company_id     = '%@' WHERE cust_id = '%@'",
												 user_id,
												 [self object:[customer valueForKey:@"first_name"] or:empty_string],
												 [self object:[customer valueForKey:@"last_name"] or:empty_string],
												 image_name ? image_name: @"",
												 [self object:[customer valueForKey:@"checkin_id"] or:empty_string],
												 customer_member_id,
												 [self object:[customer valueForKey:@"level"] or:empty_string],
												 [self object:[customer valueForKey:@"total_spent"] or:empty_string],
												 [self object:[customer valueForKey:@"member_email"] or:empty_string],
												 [self object:[customer valueForKey:@"member_mobile"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_address"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_birthdate"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_gender"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_city"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_postcode"] or:empty_string],
												 sync_id,
												 [self object:[customer valueForKey:@"active_status"] or:empty_string],
												 [StringUtils get_business_detail:@"business_id"],
												 cust_id
												 ];
			}
			else
			{
				execute_query = [NSString stringWithFormat:
												 @"INSERT INTO MEMBERS"
												 "(user_id,"
												 "cust_id,"
												 "cust_firstName,"
												 "cust_lastName,"
												 "cust_image,"
												 "cust_checkin_id,"
												 "cust_member_id,"
												 "cust_level,"
												 "cust_total_spent,"
												 "cust_email,"
												 "cust_mobile,"
												 "cust_address,"
												 "cust_birthdate,"
												 "cust_gender,"
												 "cust_city,"
												 "cust_postcode,"
												 "sync_id,"
												 "active_status,"
												 "company_id)"
												 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
												 user_id,
												 cust_id, //customer_id
												 [self object:[customer valueForKey:@"first_name"] or:empty_string],
												 [self object:[customer valueForKey:@"last_name"] or:empty_string],
												 image_name ? image_name: @"",
												 [self object:[customer valueForKey:@"checkin_id"] or:empty_string],
												 customer_member_id,
												 [self object:[customer valueForKey:@"level"] or:empty_string],
												 [self object:[customer valueForKey:@"total_spent"] or:empty_string],
												 [self object:[customer valueForKey:@"member_email"] or:empty_string],
												 [self object:[customer valueForKey:@"member_mobile"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_address"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_birthdate"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_gender"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_city"] or:empty_string],
												 [self object:[customer valueForKey:@"cust_postcode"] or:empty_string],
												 sync_id,
												 [self object:[customer valueForKey:@"active_status"] or:empty_string],
												 [StringUtils get_business_detail:@"business_id"]
												 ];
			}

			[array_of_query addObject:execute_query];

			if (![image_name isEqualToString:CUSTOMER_DEFAULT_PIC])
			{
				[FileUtils download_image:[customer valueForKey:@"photo"] withStoringPath:IMG_CUSTOMERS];
			}
		}

		NSLog(@"we are here : %@",array_of_query);

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)update_new_customers:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"cust_id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE MEMBERS SET sync_id = '%@', last_sync_time = '%@', cust_image_changed = '0' "
																	 " WHERE cust_id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"cust_id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - USERRIGHTS
- (NSArray *)get_userright_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT * from USERRIGHTS where sync_id is null or sync_id =''"];
		NSArray  *resultSet  = [self get_from_queue:query];

		return  resultSet;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - HOLD_ORDER_NUMBER
- (NSArray *)get_hold_order_number_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "hold_order_number,"
											 "last_sync_time,"
											 "created_by,"
											 "created_date,"
											 "updated_by,"
											 "updated_date,"
											 "active_status"
											 " FROM HOLD_ORDER_NUMBER"];

		NSMutableDictionary *hold_data;
		NSMutableArray      *hold_array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			hold_data = [[NSMutableDictionary alloc] init];
			hold_data[@"hold_order_number"] = dict[@"hold_order_number"];
			hold_data[@"last_sync_time"] = dict[@"last_sync_time"];
			hold_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			hold_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			hold_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			hold_data[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			hold_data[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			[hold_array addObject:hold_data];
		}


		NSLog(@"%@",hold_array);

		return hold_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - HOLD_ORDER
- (NSArray *)get_hold_order_data_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "trx_id,"
											 "member,"
											 "customer,"
											 "customer_name,"
											 "checkin_id,"
											 "create_time,"
											 "sub_total,"
											 "total,"
											 "total_gst_value,"
											 "currency,"
											 "discount_prcnt,"
											 "discount_amt,"
											 "tax_amt,"
											 "customer_profile_pic,"
											 "user_id,"
											 "table_no,"
											 "order_type,"
											 "custom_discount_id,"
											 "counter_number,"
											 "voucher_id,"
											 "voucher_name,"
											 "voucher_amount,"
											 "voucher_type_id,"
											 "voucher_type_name,"
											 "voucher_member_number,"
											 "voucher_ref_number,"
											 "rounding_amount,"
											 "customer_gstid,"
											 "customer_email,"
											 "customer_phone,"
											 "customer_pincode,"
											 "customer_city,"
											 "customer_address1,"
											 "customer_address2,"
											 "voucher_bought_detail_id,"
											 "last_sync_time,"
											 "created_by,"
											 "created_date,"
											 "updated_by,"
											 "updated_date,"
											 "active_status"
											 " FROM HOLD_ORDER where sync_id is null"];

		NSMutableDictionary *hold_data;
		NSMutableArray      *hold_array   = [[NSMutableArray alloc] init];
		NSArray        *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			hold_data = [[NSMutableDictionary alloc] init];
			hold_data[@"trx_id"] = dict[@"trx_id"];
			hold_data[@"member"] = [self object:dict[@"member"] or:empty_string];
			hold_data[@"customer"] = [self object:dict[@"customer"] or:empty_string];
			hold_data[@"customer_name"] = [self object:dict[@"customer_name"] or:empty_string];
			hold_data[@"checkin_id"] = [self object:dict[@"checkin_id"] or:empty_string];
			hold_data[@"create_time"] = [self object:dict[@"create_time"] or:empty_string];
			hold_data[@"sub_total"] = dict[@"sub_total"];
			hold_data[@"total"] = dict[@"total"];
			hold_data[@"total_gst_value"] = dict[@"total_gst_value"];
			hold_data[@"currency"] = dict[@"currency"];
			hold_data[@"discount_prcnt"] = dict[@"discount_prcnt"];
			hold_data[@"discount_amt"] = dict[@"discount_amt"];
			hold_data[@"tax_amt"] = dict[@"tax_amt"];
			hold_data[@"customer_profile_pic"] = [self object:dict[@"customer_profile_pic"] or:empty_string];
			hold_data[@"user_id"] = dict[@"user_id"];
			hold_data[@"table_no"] = [self object:dict[@"table_no"] or:empty_string];
			hold_data[@"order_type"] = dict[@"order_type"];
			hold_data[@"custom_discount_id"] = [self object:dict[@"custom_discount_id"] or:empty_string];
			hold_data[@"counter_number"] = dict[@"counter_number"];
			hold_data[@"voucher_id"] = [self object:dict[@"voucher_id"] or:empty_string];
			hold_data[@"voucher_name"] = [self object:dict[@"voucher_name"] or:empty_string];
			hold_data[@"voucher_amount"] = [self object:dict[@"voucher_amount"] or:empty_string];
			hold_data[@"voucher_type_id"] = [self object:dict[@"voucher_type_id"] or:empty_string];
			hold_data[@"voucher_type_name"] = [self object:dict[@"voucher_type_name"] or:empty_string];
			hold_data[@"voucher_member_number"] = [self object:dict[@"voucher_member_number"] or:empty_string];
			hold_data[@"voucher_ref_number"] = [self object:dict[@"voucher_ref_number"] or:empty_string];
			hold_data[@"rounding_amount"] = dict[@"rounding_amount"];
			hold_data[@"customer_gstid"] = [self object:dict[@"customer_gstid"] or:empty_string];
			hold_data[@"customer_email"] = [self object:dict[@"customer_email"] or:empty_string];
			hold_data[@"customer_phone"] = [self object:dict[@"customer_phone"] or:empty_string];
			hold_data[@"customer_pincode"] = [self object:dict[@"customer_pincode"] or:empty_string];
			hold_data[@"customer_city"] = [self object:dict[@"customer_city"] or:empty_string];
			hold_data[@"customer_address1"] = [self object:dict[@"customer_address1"] or:empty_string];
			hold_data[@"customer_address2"] = [self object:dict[@"customer_address2"] or:empty_string];
			hold_data[@"voucher_bought_detail_id"] = [self object:dict[@"voucher_bought_detail_id"] or:empty_string];
			hold_data[@"last_sync_time"] = dict[@"last_sync_time"];
			hold_data[@"rounding_amount"] = dict[@"rounding_amount"];
			hold_data[@"rounding_amount"] = dict[@"rounding_amount"];
			hold_data[@"customer_address1"] = [self object:dict[@"customer_address1"] or:empty_string];
			hold_data[@"customer_address1"] = [self object:dict[@"customer_address1"] or:empty_string];
			hold_data[@"customer_address1"] = [self object:dict[@"customer_address1"] or:empty_string];
			hold_data[@"customer_address1"] = [self object:dict[@"customer_address1"] or:empty_string];
			hold_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			hold_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			hold_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			hold_data[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			hold_data[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			hold_data[@"hold_order_items"] = [self get_hold_order_item_data_for_sync:dict[@"trx_id"] ];


			[hold_array addObject:hold_data];
		}


		NSLog(@"%@",hold_array);

		return hold_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - HOLD_ORDER_ITEMS
- (NSArray *)get_hold_order_item_data_for_sync:(NSString *)trx_id
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "id,"
											 "order_id,"
											 "item_name,"
											 "item_id,"
											 "item_qty,"
											 "split_item_qty,"
											 "item_price,"
											 "cost_price,"
											 "selling_price,"
											 "item_remarks,"
											 "item_totalPrice,"
											 "item_discount,"
											 "item_checked,"
											 "kitchen_printer_checked,"
											 "custom_discount_id,"
											 "stock_monitoring,"
											 "item_category_id,"
											 "gst_code,"
											 "gst_rate,"
											 "gst_value,"
											 "is_edited_item_price,"
											 "last_sync_time,"
											 "created_by,"
											 "created_date,"
											 "updated_by,"
											 "updated_date,"
											 "active_status"
											 " FROM HOLD_ORDER_ITEMS where order_id = '%@'",trx_id];

		NSMutableDictionary *hold_data;
		NSMutableArray      *hold_array   = [[NSMutableArray alloc] init];

		NSArray         *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			hold_data = [[NSMutableDictionary alloc] init];
			hold_data[@"id"] = dict[@"id"];
			hold_data[@"order_id"] = dict[@"order_id"];
			hold_data[@"item_name"] = dict[@"item_name"];
			hold_data[@"item_id"] = dict[@"item_id"];
			hold_data[@"item_qty"] = dict[@"item_qty"];
			hold_data[@"split_item_qty"] = dict[@"split_item_qty"];
			hold_data[@"item_price"] = dict[@"item_price"];
			hold_data[@"cost_price"] = dict[@"cost_price"];
			hold_data[@"selling_price"] = dict[@"selling_price"];
			hold_data[@"item_remarks"] = [self object:dict[@"item_remarks"] or:empty_string];
			hold_data[@"item_totalPrice"] = dict[@"item_totalPrice"];
			hold_data[@"item_discount"] = [self object:dict[@"item_discount"] or:empty_string];
			hold_data[@"item_checked"] = dict[@"item_checked"];
			hold_data[@"kitchen_printer_checked"] = dict[@"kitchen_printer_checked"];
			hold_data[@"custom_discount_id"] = [self object:dict[@"custom_discount_id"] or:empty_string];
			hold_data[@"stock_monitoring"] = dict[@"stock_monitoring"];
			hold_data[@"item_category_id"] = dict[@"item_category_id"];
			hold_data[@"gst_code"] = dict[@"gst_code"];
			hold_data[@"gst_rate"] = dict[@"gst_rate"];
			hold_data[@"gst_value"] = [self object:dict[@"gst_value"] or:empty_string];
			hold_data[@"is_edited_item_price"] = [self object:dict[@"is_edited_item_price"] or:empty_string];
			hold_data[@"last_sync_time"] = dict[@"last_sync_time"];
			hold_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			hold_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			hold_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			hold_data[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			hold_data[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			hold_data[@"hold_order_modifiers"] = [self get_hold_order_modifier_data_for_sync:dict[@"id"]];


			[hold_array addObject:hold_data];
		}


		NSLog(@"%@",hold_array);

		return hold_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - HOLD_ORDER_MODIFIER
- (NSArray *)get_hold_order_modifier_data_for_sync:(NSString *)hold_order_item_id
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "id,"
											 "hold_order_item_id,"
											 "mod_id,"
											 "mod_name,"
											 "mod_price,"
											 "mod_type,"
											 "mod_parent_name,"
											 "mod_selected,"
											 "mod_parent_id,"
											 "mod_item_id,"
											 "mod_qty,"
											 "last_sync_time,"
											 "created_by,"
											 "created_date,"
											 "updated_by,"
											 "updated_date,"
											 "active_status"
											 " FROM HOLD_ORDER_MODIFIERS where hold_order_item_id = '%@'",
											 hold_order_item_id];

		NSMutableDictionary *hold_data;
		NSMutableArray      *hold_array   = [[NSMutableArray alloc] init];

		NSArray         *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			hold_data = [[NSMutableDictionary alloc] init];
			hold_data[@"id"] = dict[@"id"];
			hold_data[@"item_id"] = dict[@"hold_order_item_id"];
			hold_data[@"mod_id"] = dict[@"mod_id"];
			hold_data[@"mod_name"] = dict[@"mod_name"];
			hold_data[@"mod_price"] = dict[@"mod_price"];
			hold_data[@"mod_type"] = dict[@"mod_type"];
			hold_data[@"mod_parent_name"] = dict[@"mod_parent_name"];
			hold_data[@"mod_selected"] = [self object:dict[@"mod_selected"] or:empty_string];
			hold_data[@"mod_parent_id"] = dict[@"mod_parent_id"];
			hold_data[@"mod_cat_id"] = dict[@"mod_item_id"];
			hold_data[@"mod_qty"] = dict[@"mod_qty"];
			hold_data[@"last_sync_time"] = dict[@"last_sync_time"];
			hold_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			hold_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			hold_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			hold_data[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			hold_data[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			[hold_array addObject:hold_data];
		}


		NSLog(@"%@",hold_array);

		return hold_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - PAYMENT_METHOD
- (void)add_paymethod:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

			/// only delete when data is not empty
		if ([data count] > 0) {
			[array_of_query addObject:@"DELETE FROM PAYMENT_METHOD"];
		}

		for (NSDictionary *dat in data)
		{
			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO PAYMENT_METHOD"
																 "(id,"
																 "pay_name,"
																 "action,"
																 "type_id,"
																 "type_name,"
																 "gateway_id,"
																 "gateway_name)"
																 @"VALUES ('%@','%@','%@','%@','%@','%@','%@')",
																 dat[@"id"],
																 dat[@"name"],
																 dat[@"action"],
																 dat[@"type_id"],
																 dat[@"type_name"],
																 dat[@"gateway_id"],
																 dat[@"gateway_name"]
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch(NSException *e)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

#pragma mark - DISCOUNT
- (void)add_discount:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *execute_query = @"";

		for (id discount_item in data)
		{
			if ([self record_already_exists : @"DISCOUNT"
								with_filter           : [NSString stringWithFormat:@" id= \"%@\"",[discount_item valueForKey:@"id"]]])
			{
				execute_query = [NSString stringWithFormat:
												 @"UPDATE DISCOUNT SET "
												 "discount_desc  = '%@',"
												 "amount         = '%@',"
												 "percentage     = '%@',"
												 "sync_id        = '%@',"
												 "created_by     = '%@',"
												 "created_date   = '%@',"
												 "updated_by     = '%@',"
												 "updated_date   = '%@',"
												 "active_status  = '%@'"
												 " WHERE id      = '%@'",
												 [data valueForKey:@"description"],
												 [data valueForKey:@"amount"],
												 [data valueForKey:@"percentage"],
												 [data valueForKey:@"sync_id"],
												 [data valueForKey:@"created_by"],
												 [data valueForKey:@"created_date"],
												 [data valueForKey:@"updated_by"],
												 [data valueForKey:@"updated_date"],
												 [data valueForKey:@"active_status"],
												 [data valueForKey:@"id"]
												 ];
			}
			else
			{
				execute_query = [NSString stringWithFormat:
												 @"INSERT INTO DISCOUNT"
												 "(id,"
												 "discount_desc,"
												 "amount,"
												 "percentage,"
												 "sync_id,"
												 "created_by,"
												 "created_date,"
												 "updated_by,"
												 "updated_date,"
												 "active_status)"
												 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
												 discount_item[@"id"],
												 discount_item[@"description"],
												 discount_item[@"amount"],
												 discount_item[@"percentage"],
												 discount_item[@"sync_id"],
												 discount_item[@"created_by"],
												 discount_item[@"created_date"],
												 discount_item[@"updated_by"],
												 discount_item[@"updated_date"],
												 discount_item[@"active_status"]
												 ];
			}

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch(NSException *e)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",e.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

#pragma mark - VOUCHER
- (void)add_voucher:(NSArray*)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *execute_query = @"";

		for (int i = 0; i < data.count; i++)
		{
			if ([self record_already_exists : @"VOUCHERS"
								with_filter           : [NSString stringWithFormat:@" voucher_id= \"%@\"",[data[i] valueForKey:@"id"]]])
			{
				execute_query = [NSString stringWithFormat:
												 @"UPDATE VOUCHERS SET "
												 "voucher_name   = '%@',"
												 "voucher_type_id = '%@',"
												 "voucher_amount = '%@',"
												 "sync_id        = '%@',"
												 "created_by     = '%@',"
												 "created_date   = '%@',"
												 "updated_by     = '%@',"
												 "updated_date   = '%@',"
												 "active_status  = '%@' "
												 "WHERE voucher_id = '%@' ",
												 [data valueForKey:@"name"],
												 [data valueForKey:@"type_id"],
												 [data valueForKey:@"amount"],
												 [data valueForKey:@"sync_id"],
												 [data valueForKey:@"created_by"],
												 [data valueForKey:@"created_date"],
												 [data valueForKey:@"updated_by"],
												 [data valueForKey:@"updated_date"],
												 [data valueForKey:@"active_status"],
												 [data valueForKey:@"id"]
												 ];
			}
			else
			{
				execute_query = [NSString stringWithFormat:
												 @"INSERT INTO VOUCHERS"
												 "(voucher_id,"
												 "voucher_name,"
												 "voucher_type_id,"
												 "voucher_amount,"
												 "voucher_qrcode,"
												 "description,"
												 "picture,"
												 "stock_left,"
												 "stock_minimum,"
												 "type,"
												 "valid_end,"
												 "valid_start,"
												 "sync_id,"
												 "created_by,"
												 "created_date,"
												 "updated_by,"
												 "updated_date,"
												 "active_status)"
												 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
												 data[i][@"id"],
												 data[i][@"name"],
												 data[i][@"type_id"],
												 data[i][@"amount"],
												 data[i][@"qrcode"],
												 data[i][@"description"],
												 data[i][@"picture"],
												 data[i][@"stock_left"],
												 data[i][@"stock_minimum"],
												 data[i][@"type"],
												 data[i][@"valid_end"],
												 data[i][@"valid_start"],
												 data[i][@"sync_id"],
												 data[i][@"created_by"],
												 data[i][@"created_date"],
												 data[i][@"updated_by"],
												 data[i][@"updated_date"],
												 data[i][@"active_status"]
												 ];
			}
			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

#pragma mark - VOUCHER_BOUGHT
- (void)sync_voucher_bought:(NSArray*)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			NSArray *item = data[i];

			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO VOUCHERS_BOUGHT "
																 "(id,"
																 "voucher_id,"
																 "qty,"
																 "price,"
																 "bought_time,"
																 "company_id,"
																 "sync_id,"
																 "created_by,"
																 "created_date,"
																 "updated_by,"
																 "updated_date,"
																 "active_status)"
																 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
																 [item valueForKey:@"id"],
																 [item valueForKey:@"voucher_id"],
																 [item valueForKey:@"qty"],
																 [item valueForKey:@"price"],
																 [item valueForKey:@"bought_time"],
																 [StringUtils get_business_detail:@"business_id"],
																 [item valueForKey:@"sync_id"],
																 [item valueForKey:@"created_by"],
																 [item valueForKey:@"create_time"],
																 [item valueForKey:@"updated_by"],
																 [item valueForKey:@"update_time"],
																 [item valueForKey:@"active_status"]
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - VOUCHERS_BOUGHT_DETAIL
- (NSArray *)get_voucher_redeem_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "id, redeemed, member_id, is_manis, updated_by, updated_date,voucher_name,voucher_id,voucher_bought_id,sn,code,expired,company_id,last_sync_time,created_by,created_date, active_status"
											 " FROM VOUCHERS_BOUGHT_DETAIL where (sync_id is null or sync_id = '') and redeemed = 1"];

		NSMutableDictionary *vo;
		NSMutableArray      *vo_array   = [[NSMutableArray alloc] init];

		NSArray         *resultSet  = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			vo = [[NSMutableDictionary alloc] init];
			vo[@"id"] = dict[@"id"];
			vo[@"redeemed"] = dict[@"redeemed"];
			vo[@"member_id"] = dict[@"member_id"] ? dict[@"member_id"] : @"";
			vo[@"is_manis"] = dict[@"is_manis"];
			vo[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			vo[@"updated_time"] = [self object:dict[@"updated_date"] or:empty_string];

			vo[@"voucher_name"] = [self object:dict[@"voucher_name"] or:empty_string];
			vo[@"voucher_id"] = [self object:dict[@"voucher_id"] or:empty_string];
			vo[@"voucher_bought_id"] = [self object:dict[@"voucher_bought_id"] or:empty_string];
			vo[@"sn"] = [self object:dict[@"sn"] or:empty_string];
			vo[@"code"] = [self object:dict[@"code"] or:empty_string];
			vo[@"expired"] = [self object:dict[@"expired"] or:empty_string];

			vo[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			vo[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			vo[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			vo[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			vo[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];
			[vo_array addObject:vo];
		}


		NSLog(@"%@",vo_array);

		return vo_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)add_voucher_bougth_details:(NSArray*)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *execute_query = @"";

		for (NSArray* item in data)
		{
			if ([self record_already_exists: @"VOUCHERS_BOUGHT_DETAIL"
													with_filter: [NSString stringWithFormat:@" id=\"%@\"",[item valueForKey:@"voucher_bought_detail_id"]]])
			{
				execute_query = [NSString stringWithFormat:
												 @"UPDATE VOUCHERS_BOUGHT_DETAIL SET"
												 " sn        = '%@',"
												 " code      = '%@',"
												 " expired   = '%@'"
												 " WHERE id  = '%@'",
												 [data valueForKey:@"sn"],
												 [data valueForKey:@"code"],
												 [data valueForKey:@"expired"],
												 [data valueForKey:@"voucher_bought_detail_id"]
												 ];
			}
			else
			{
				execute_query = [NSString stringWithFormat:
												 @"REPLACE INTO VOUCHERS_BOUGHT_DETAIL "
												 "(id,"
												 "voucher_id,"
												 "voucher_bought_id,"
												 "sn,"
												 "code,"
												 "redeemed,"
												 "expired,"
												 "company_id,"
												 "sync_id,"
												 "last_sync_time,"
												 "created_by,"
												 "created_date,"
												 "updated_by,"
												 "updated_date,"
												 "active_status)"
												 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
												 [item valueForKey:@"id"],
												 [item valueForKey:@"voucher_id"],
												 [item valueForKey:@"voucher_bought_id"],
												 [item valueForKey:@"sn"],
												 [item valueForKey:@"code"],
												 [item valueForKey:@"redeemed"],
												 [item valueForKey:@"expired"],
												 [StringUtils get_business_detail:@"business_id"],
												 [item valueForKey:@"sync_id"],
												 [item valueForKey:@"last_sync_time"],
												 [item valueForKey:@"created_by"],
												 [item valueForKey:@"create_time"],
												 [item valueForKey:@"updated_by"],
												 [item valueForKey:@"update_time"],
												 [item valueForKey:@"active_status"]
												 ];
			}
			[array_of_query addObject:execute_query];
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason]
												withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

- (void)edit_voucher_redeemed_from_sync:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE VOUCHERS_BOUGHT_DETAIL SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"sync_time"],
																	 data[i][@"id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - VOUCHER_TYPE
- (void)add_voucher_type:(NSArray*)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		NSString *execute_query = @"";

		for (int i = 0; i < data.count; i++)
		{
			if ([self record_already_exists : @"VOUCHER_TYPE"
								with_filter           : [NSString stringWithFormat:@" voucher_type_id= \"%@\"",[data[i] valueForKey:@"id"]]])
			{
				execute_query = [NSString stringWithFormat:
												 @"UPDATE VOUCHER_TYPE SET "
												 "voucher_type_name   = '%@',"
												 "sync_id        = '%@',"
												 "created_by     = '%@',"
												 "created_date   = '%@',"
												 "updated_by     = '%@',"
												 "updated_date   = '%@',"
												 "active_status  = '%@' "
												 "WHERE voucher_type_id = '%@' ",
												 [data valueForKey:@"name"],
												 [data valueForKey:@"sync_id"],
												 [data valueForKey:@"created_by"],
												 [data valueForKey:@"created_date"],
												 [data valueForKey:@"updated_by"],
												 [data valueForKey:@"updated_date"],
												 [data valueForKey:@"active_status"],
												 [data valueForKey:@"id"]
												 ];
			}
			else
			{
				execute_query = [NSString stringWithFormat:
												 @"INSERT INTO VOUCHER_TYPE "
												 "(voucher_type_id,"
												 "voucher_type_name,"
												 "sync_id,"
												 "created_by,"
												 "created_date,"
												 "updated_by,"
												 "updated_date,"
												 "active_status)"
												 @"VALUES ('%@','%@','%@','%@','%@','%@','%@','%@')",
												 data[i][@"id"],
												 data[i][@"name"],
												 data[i][@"sync_id"],
												 data[i][@"created_by"],
												 data[i][@"created_date"],
												 data[i][@"updated_by"],
												 data[i][@"updated_date"],
												 data[i][@"active_status"]
												 ];
			}
			[array_of_query addObject:execute_query];
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		[APP_DELEGATE handle_exception:[NSString stringWithFormat:@"%@",exception.reason] withmodule:@"" withfunc:[NSString stringWithFormat:@"%s",__PRETTY_FUNCTION__]];
	}
}

#pragma mark - PRINTER
- (NSArray *)get_printer_data_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "id,"
											 "ip_address,"
											 "printer_name,"
											 "printer_type,"
											 "category_id,"
											 "default_printer,"
											 "print_copies,"
											 "company_id,"
											 "last_sync_time,"
											 "created_by,"
											 "created_date,"
											 "updated_by,"
											 "updated_date,"
											 "active_status"
											 " FROM PRINTERS where sync_id is null or sync_id is \"\""];

		NSMutableDictionary *custom_data;
		NSMutableArray      *custom_array   = [[NSMutableArray alloc] init];

		NSArray         *resultSet      = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			custom_data = [[NSMutableDictionary alloc] init];
			custom_data[@"id"] = [self object:dict[@"id"] or:empty_string];
			custom_data[@"ip_address"] = [self object:dict[@"ip_address"] or:empty_string];
			custom_data[@"printer_name"] = [self object:dict[@"printer_name"] or:empty_string];
			custom_data[@"printer_type"] = [self object:dict[@"printer_type"] or:empty_string];
			custom_data[@"category_id"] = [self object:dict[@"category_id"] or:empty_string];
			custom_data[@"default_printer"] = [self object:dict[@"default_printer"] or:empty_string];
			custom_data[@"print_copies"] = [self object:dict[@"print_copies"] or:empty_string];
			custom_data[@"company_id"] = [self object:dict[@"company_id"] or:empty_string];
			custom_data[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			custom_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			custom_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			custom_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			custom_data[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			custom_data[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			[custom_array addObject:custom_data];
		}


		NSLog(@"%@",custom_array);

		return custom_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)sync_printers:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (NSArray* printer in data)
		{
			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO PRINTERS"
																 "(id,"
																 "ip_address,"
																 "printer_name,"
																 "printer_type,"
																 "category_id,"
																 "default_printer,"
																 "print_copies,"
																 "company_id,"
																 "sync_id,"
																 "last_sync_time,"
																 "created_by,"
																 "created_date,"
																 "updated_by,"
																 "updated_date,"
																 "active_status) "
																 "VALUES ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",
																 [printer valueForKey:@"id"],
																 [printer valueForKey:@"ip_address"],
																 [printer valueForKey:@"printer_name"],
																 [printer valueForKey:@"printer_type"],
																 [printer valueForKey:@"category_id"],
																 [printer valueForKey:@"default_printer"],
																 [printer valueForKey:@"print_copies"],
																 [printer valueForKey:@"company_id"],
																 [printer valueForKey:@"sync_id"],
																 [printer valueForKey:@"last_sync_time"],
																 [printer valueForKey:@"created_by"],
																 [printer valueForKey:@"created_date"],
																 [printer valueForKey:@"updated_by"],
																 [printer valueForKey:@"updated_date"],
																 [printer valueForKey:@"active_status"]
																 ];

			[array_of_query addObject:execute_query];
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)update_new_printers:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"id"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE PRINTERS SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE id = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"id"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - CUSTOM INPUT
- (NSArray *)get_custom_field_data_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "custom_field_name,"
											 "last_sync_time,"
											 "created_by,"
											 "created_date,"
											 "updated_by,"
											 "updated_date,"
											 "active_status"
											 " FROM CUSTOM_INPUT where sync_id is null"];

		NSMutableDictionary *custom_data;
		NSMutableArray      *custom_array   = [[NSMutableArray alloc] init];

		NSArray         *resultSet      = [self get_from_queue:query];

		for(NSDictionary *dict in resultSet)
		{
			custom_data = [[NSMutableDictionary alloc] init];
			custom_data[@"custom_field_name"] = [self object:dict[@"custom_field_name"] or:empty_string];
			custom_data[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			custom_data[@"created_by"] = [self object:dict[@"created_by"] or:empty_string];
			custom_data[@"created_date"] = [self object:dict[@"created_date"] or:empty_string];
			custom_data[@"updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			custom_data[@"updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			custom_data[@"active_status"] = [self object:dict[@"active_status"] or:empty_string];

			[custom_array addObject:custom_data];
		}


		NSLog(@"%@",custom_array);

		return custom_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)update_custom_input_from_sync:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (int i = 0; i < data.count; i++)
		{
			if(data[i][@"custom_field_name"] != nil)
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"UPDATE CUSTOM_INPUT SET sync_id = '%@', last_sync_time = '%@' "
																	 " WHERE custom_field_name = '%@'",
																	 data[i][@"sync_id"],
																	 data[i][@"last_sync_time"],
																	 data[i][@"custom_field_name"]
																	 ];

				[array_of_query addObject:execute_query];
			}
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

#pragma mark - MODULES
- (void)sync_modules_details:(NSArray *) data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];
		for (int i = 0; i < data.count; i++)
		{
			NSArray *modules  = data[i];
			NSString *execute_query = [NSString stringWithFormat:
																 @"REPLACE INTO MODULES"
																 "(module_id,"
																 "module_name,"
																 "module_desc,"
																 "module_icon)"
																 @" VALUES ('%@','%@','%@','%@')",
																 [modules valueForKey:@"id"],
																 [modules valueForKey:@"name"],
																 [self object:[modules valueForKey:@"description"] or:empty_string],
																 [self object:[modules valueForKey:@"icon"] or:empty_string]
																 ];

			[array_of_query addObject:execute_query];
		}
		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}

- (void)set_custom_field_name:(NSArray *)data
{
	@try
	{
		NSMutableArray *array_of_query = [[NSMutableArray alloc] init];

		for (NSDictionary *data_field in data)
		{
			if (![self record_already_exists:@"CUSTOM_INPUT"
													 with_filter: [NSString stringWithFormat:@" custom_field_name='%@'",[data_field valueForKey:@"custom_field_name"]]])
			{
				NSString *execute_query = [NSString stringWithFormat:
																	 @"INSERT INTO CUSTOM_INPUT"
																	 "(custom_field_name,"
																	 "company_id,"
																	 "sync_id,"
																	 "last_sync_time,"
																	 "active_status,"
																	 "created_by,"
																	 "created_date)"
																	 @" VALUES ('%@','%@','%@','%@','%@','%@','%@')",
																	 [data_field valueForKey:@"custom_field_name"],
																	 [data_field valueForKey:@"company_id"],
																	 [data_field valueForKey:@"sync_id"],
																	 [data_field valueForKey:@"last_sync_time"],
																	 [data_field valueForKey:@"active_status"],
																	 [StringUtils get_user_detail:@"user_id"],
																	 CURRENT_TIMESTAMP];
				[array_of_query addObject:execute_query];
			}
		}

		[self add_to_queue:array_of_query with_priority:low];
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}


#pragma mark - ERROR
- (NSArray *)get_error_data_for_sync
{
	@try
	{
		NSString *query = [NSString stringWithFormat:@"SELECT "
											 "error_id,error_desc,error_module,error_function_name,created_by,"
											 "created_date,notified,sync_id,last_sync_time,"
											 "updated_by, updated_date,company_id, active_status"
											 " FROM ERRORS WHERE notified = 0"];

		NSMutableDictionary *error_data;
		NSMutableArray      *error_array   = [[NSMutableArray alloc] init];

		NSArray         *resultSet  = [self get_from_queue:query];

		NSMutableArray     *final_error_array   = [[NSMutableArray alloc] init];

		NSMutableDictionary *company_name_error_data = [[NSMutableDictionary alloc] init];
		company_name_error_data[@"company_name"] = [NSString stringWithFormat:@"%@-%@",[StringUtils get_business_detail:@"business_name"],[StringUtils get_business_detail:@"business_email"]];

		for(NSDictionary *dict in resultSet)
		{
			error_data = [[NSMutableDictionary alloc] init];
			error_data[@"poe_id"] = dict[@"error_id"];
			error_data[@"poe_description"] = [self object:dict[@"error_desc"] or:empty_string];
			error_data[@"poe_module"] = [self object:dict[@"error_module"] or:empty_string];
			error_data[@"poe_function_name"] = [self object:dict[@"error_function_name"] or:empty_string];
			error_data[@"poe_notified"] = [self object:dict[@"notified"] or:empty_string];
			error_data[@"poe_com_id"] = [self object:dict[@"company_id"] or:empty_string];
			error_data[@"sync_id"] = [self object:dict[@"sync_id"] or:empty_string];
			error_data[@"last_sync_time"] = [self object:dict[@"last_sync_time"] or:empty_string];
			error_data[@"poe_created_by"] = [self object:dict[@"created_by"] or:empty_string];
			error_data[@"poe_created_date"] = [self object:dict[@"created_date"] or:empty_string];
			error_data[@"poe_updated_by"] = [self object:dict[@"updated_by"] or:empty_string];
			error_data[@"poe_updated_date"] = [self object:dict[@"updated_date"] or:empty_string];
			error_data[@"poe_active_status"] = [self object:dict[@"active_status"] or:empty_string];
			[error_array addObject:error_data];
		}


		[self add_to_queue:@"UPDATE ERRORS SET notified = 1"];

		company_name_error_data[@"error_data"] = error_array;
		[final_error_array addObject:company_name_error_data];
		NSLog(@"%@",error_array);

		return final_error_array;
	}
	@catch (NSException *exception)
	{
		NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
	}
}


#pragma mark - UNKOWN
- (BOOL)delete_sync_master_record
{
	BOOL result     = NO;
		//    NSString *query = @"DELETE FROM SYNC_MASTER";
		//    result          = [db executeUpdate:query];
		//
		//    if ([db hadError] || !result)
		//    {
		//        NSLog(@"DB ERROR - %s %@",__PRETTY_FUNCTION__,[db lastErrorMessage]);
		//        return NO;
		//    }
	return result;
}

	//- (NSArray *)get_record_using_sync_master:(NSArray *)data
	//{
	//    NSMutableArray *sync_master_data = [[NSMutableArray alloc]init];
	//
	//    for(int i = 0; i< data.count; i++)
	//    {
	//        if([[data[i]valueForKey:@"table_name"] isEqualToString:@"CASH_REGISTER"])
	//        {
	//            NSMutableDictionary *cash_data = [[NSMutableDictionary alloc]init];
	//            cash_data[@"CASH_REGISTER"] = [self get_cash_register_for_sync:[data[i]valueForKey:@"record_id"]];
	//            [sync_master_data addObject:cash_data];
	//
	//        }
	//
	//        if([[data[i]valueForKey:@"table_name"] isEqualToString:@"TRANSACTIONS"])
	//        {
	//            NSMutableDictionary *trx_data = [[NSMutableDictionary alloc]init];
	//            trx_data[@"TRANSACTIONS"] = [self get_transactions_for_sync:[data[i]valueForKey:@"record_id"]];
	//            [sync_master_data addObject:trx_data];
	//        }
	//
	//    }
	//    return sync_master_data;
	//}

@end
