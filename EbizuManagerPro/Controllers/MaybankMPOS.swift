//
//  MaybankMPOS.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 30/11/2016.
//  Copyright © 2016 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

@objc class MaybankMPOS: NSObject,ExternalPaymentProtocol
{
    let MaybankUrlSchemeRequest = "sspay-maybankmpos://nativePayment?url=%@&id=3rdPartyVendor&currency=%@&amount=%@&dscp=%@&cust_email=%@&cust_mobile=%@&mpos_auto_logout=NO&isHTTPS=NO&referenceNo=%@"
    
    // MARK: Singleton Declaration
    // swiftSharedExPayment is not accessible from ObjC
    class var swiftSharedExPayment: MaybankMPOS
    {
        struct Singleton
        {
            static let SharedExPayment = MaybankMPOS()
        }
        return Singleton.SharedExPayment
    }
    
    // the sharedInstance class method can be reached from ObjC
    class func SharedExPayment() -> MaybankMPOS
    {
        return MaybankMPOS.swiftSharedExPayment
    }
    
    func sendPaymentAmount(amount: String?) -> Bool
    {
        let bundleIdentifier = Bundle.main.bundleIdentifier ?? ""

        let request = String(format: MaybankUrlSchemeRequest,
                           bundleIdentifier,                                                            // url
                           ModelController.shared().currentCompleteOrder.currency,                      // currency
                           amount!,                                                                     // amount
                           UIUtils.add_encoded_value(StringUtils.get_business_detail("business_name"))!, // description
                           ModelController.shared().currentCompleteOrder.customer_email,                // cust email
                           ModelController.shared().currentCompleteOrder.customer_phone,                // cust phone
                           ModelController.shared().currentCompleteOrder.hold_order_id)                 // hold order no
        
        print("maybank request : \(request)")
        
        if let url = URL(string: request)
        {
            print("success maybank request : \(url)")
            return UIApplication.shared.openURL(url)
        }
    
        return false;
    }
    
    func checkPaymentResponse(response: AnyObject) -> [String:String]
    {
        var maybankResponse = [String:String]()
        
        if let url = response as? NSURL
        {
            if let response = url.query?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
            {
                print("maybank original response : \(response)")
                
                guard let dictResponse = response.getResponseDict() else
                {
                    print("Maybank reponse error")
                    return maybankResponse;
                }
                
                maybankResponse[RESPONSECARDCODE] = dictResponse["status"]
                maybankResponse[RESPONSECARDMSG]  = dictResponse["status_msg"]
                maybankResponse[RESPONSECARDTYPE] = dictResponse["card_type"]
                maybankResponse[RESPONSECARDNO]   = dictResponse["card_no"]
                maybankResponse[RESPONSECARDAPPROVALCODE] = dictResponse["approval_code"]
                
                print("maybank stored response: \(maybankResponse)")
                
                // check did we have this status ?
                if let maybankCardCode = maybankResponse[RESPONSECARDCODE]
                {
                    guard let status = MaybankMPOSStatus(rawValue: maybankCardCode) else
                    {
                        print ("Maybank unknown status response")
                        maybankResponse[RESPONSECARDSTAT] = UNKNOWNSCHEME
                        return maybankResponse;
                    }
                    
                    var responseStatus = "";
                    switch status
                    {
                    case .successStatus:
                        responseStatus = SUCCESSSCHEME
                    case .cancelStatus:
                        responseStatus = CANCELSCHEME
                    case .failStatus:
                        responseStatus = FAILURESCHEME
                    }
                    maybankResponse[RESPONSECARDSTAT] = responseStatus
                }
            }
        }
        return maybankResponse
    }
}
