//
//  UploadAPI.swift
//  KryptoPOS
//
//  Created by augustius cokroe on 09/03/2019.
//  Copyright © 2019 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

struct UploadAPI {

    static func uploadAttachment(_ data: Data, fileName: String, progessHandler: ((_ progress: Double) -> Void)? = nil, completion: @escaping (_ result: KRNetworkResult<String>) -> Void) {

        KRNetworkRequest.postMultiPart(.uploadDb, attachmentData: data, fileName: fileName, progressHandler: { (progress) in
            progessHandler?(progress.fractionCompleted)
        }) { (result) in
            switch result {
            case .success(let dict):
                print(dict)
                completion(.success(""))
            case .failure(let error):
                print(error)
                completion(.failure(error))
            }
        }
    }
}

