//
//  KRNetworkEndpoint.swift
//  KryptoPOS
//
//  Created by augustius cokroe on 09/03/2019.
//  Copyright © 2019 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum KRNetworkEndpoint: String {

    case uploadDb = "/pointofsale/upload_db_file"
}
