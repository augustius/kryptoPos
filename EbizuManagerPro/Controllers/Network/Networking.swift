//
//  Networking.swift
//  Manager
//
//  Created by augustius cokroe on 29/09/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum networkNotification: String{
    case transactionSyncForm = "TRANSACTION_SYNC_FORM"
    case resyncTransaction = "RESYNC_TRANSACTIONS"
    case verifyLoginSuccess = "LOGIN_SUCCESS"
    case verifyLoginFail = "LOGIN_FAIL"
    case getManisSuccess = "MANIS_SUCCESS"
    case getManisFail = "MANIS_FAIL"
    case isEbizuMemberSuccess = "EBIZU_MEMBER_EXIST"
    case isEbizuMemberFail = "EBIZU_MEMBER_NONEXIST"
    case sendLocalDbSuccess = "SEND_LOCAL_DB_SUCCESS"
    case sendLocalDbFail = "SEND_LOCAL_DB_FAIL"
    case AccountStatusUpdated = "ACCOUNT_STATUS_UPDATED"
    case getMaybankQrPaySuccess = "GET_MAYBANK_QRCODE_SUCCESS"
    case getMaybankQrPayFail = "GET_MAYBANK_QRCODE_FAIL"
    case getQrPaySuccess = "GET_QRPAY_SUCCESS"
    case getQrPayFail = "GET_QRPAY_FAIL"

    var notification : Notification.Name  {
        return Notification.Name(rawValue: self.rawValue)
    }
}

enum responseStatus:String{
    case onProgress = "on progress"
    case fail = "fail"
    case Success = "success"
    case noDataToSend = "no data to send"
}

enum readingApi: String{
    case readingSetting = "setting"
    case readingTableArea = "tableareas"
    case readingTable = "tables"
    case readingPaymentMethod = "paymentmethods"
    case readingDiscount = "discounts"
    case readingVoucher = "vouchers"
    case readingVoucherBought = "voucherboughts"
    case readingVoucherBoughtDetail = "voucherboughtdetails"
    case readingVoucherType = "vouchertypes"
    case readingTransaction = "transactions"
    case readingPurchaseOrder = "poreceive"
    case readingMenu = "menu"
    case readingCustomer = "customer"
    case readingPrinter = "printers"
    case readingRefund = "transactionrefunds"
    case readingShift = "shiftdetails"
    case readingTimecard = "employeetimecards"
    case readingUserright = "get_user_rights"
    case readingModule = "modules"
    case readingCustomInput = "custom_input"
    case readingCashRegister = "cashregister"
    case readingMultiPayment = "multiplepayment"
    case readingHoldOrderNo = "hold_order_number"
    case readingTransactionNo = "transaction_number"
    case readingTransactionSyncId = "get_sync_id"
    case readingAccountSubscriptionStatus = "check_subscription_status"
}

enum writingApi: String{
    case writingTransaction = "sync_all"
    case writingEmail = "emailreceiptbatch"
    case writingRefund = "sync_all_refund" //"sync_all" use sync_all also
    case writingPurchaseOrder = "pobatch"
    case writingVoucherBoughtDetail = "voucherboughtdetailbatch"
    case writingSetting = "settingbatch"
    case writingCashRegister = "cashregisterbatch"
    case writingCategory = "categorybatch"
    case writingItem  = "itembatch"
    case writingTable = "tablebatch"
    case writingArea = "areabatch"
    case writingPushToken = "push_id"
    case writingShift  = "shiftdetailsbatch"
    case writingTimecard = "employeetimecardbatch"
    case writingCustomer = "customerbatch"
    case writingUserright = "edit_employee_login"
    case writingErrorReport = "send_error_report"
    case writingHoldOrderNo = "holdordernumberbatch"
    case writingCustomInput = "custominputbatch"
    case writingPrinter = "printerbatch"
}

class Networking:NSObject
{
    // MARK: - Singelton declaration
    private static var sharedNetworking: Networking = {
        let networkManager = Networking()
        // Configuration
        // ...
        return networkManager
    }()
    
    
    class func shared() -> Networking {
        return sharedNetworking
    }

    internal let currentApiVersion = "2.0"

    // MARK: - private main function
    private func convertToJSONData(data:Any) -> Data
    {
        do
        {
            let value = "r="
            var finalData = value.data(using: .utf8)!
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            finalData.append(jsonData)
            return finalData
        }
        catch
        {
            print("fail")
            return Data.init()
        }
    }
    
    private func convertToDictionary(str:String) -> [String:AnyObject]
    {
        do
        {
            guard
                let data = str.data(using: .utf8),
                let dict = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                else { return [:] }
            
            return dict
        }
        catch
        {
            return [:]
        }
    }
    
    fileprivate func post(data:Any,withUrl urlString:String, withRequestName requestName:String)
    {
        let jsonData = convertToJSONData(data: data)
        finishResponse(withMessage: String(data: jsonData, encoding: .utf8) ?? "", fromRequest: requestName, withStatus: .onProgress)
        
        var request = URLRequest(url:URL(string:urlString)!)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue(requestName, forHTTPHeaderField: "Request-Name")
        request.httpBody = jsonData
        request.timeoutInterval = 240
        
        let session = URLSession.shared
        //Post
        session.dataTask(with: request){data, response, err in
            
            let requestName = request.value(forHTTPHeaderField: "Request-Name") ?? ""
            
            //Guard: ws there error ?
            guard(err == nil) else {
                self.finishResponse(withMessage: err?.localizedDescription ?? "", fromRequest: requestName, withStatus: .fail)
                return
            }
            
            //Guard: check was any data returned?
            guard let data = data else{
                self.finishResponse(withMessage: "no data return", fromRequest: requestName, withStatus: .fail)
                return
            }
            
            //Convert data to string and then dictionary
            if let parseResult =  String(data: data, encoding: .utf8)
            {
                let convertedDict = self.convertToDictionary(str: parseResult.filterSpecialChar())
                if convertedDict.count > 0
                {
                    self.handleResponse(from: requestName, withDict: convertedDict)
                }
                else
                {
                    self.finishResponse(withMessage: "Empty response from server. \nPlease contact Krypto support team", fromRequest: requestName, withStatus: .fail)
                }
                return
            }
            else
            {
                self.finishResponse(withMessage: "Could not parse data as string. \nData : \(data)", fromRequest: requestName, withStatus: .fail)
                return
            }
        }.resume()
    }
    
    fileprivate func finishResponse(withMessage message:String, fromRequest requestName:String, withStatus status:responseStatus)
    {
        let nothing:AnyObject? = nil
        finishResponse(withMessage: message, fromRequest: requestName, withStatus: status, withAdditionalData: nothing)
    }
    
    fileprivate func finishResponse(withMessage message:String, fromRequest requestName:String, withStatus status:responseStatus, withAdditionalData additionalData:AnyObject?)
    {
        let messageDict = ["message":message] as [String:AnyObject]
        
        switch status
        {
        case .onProgress:
            print("on progress for \(requestName) with message : \(message)")
        case .noDataToSend:
            print("no data to send from \(requestName) with message : \(message)")
        case .Success:
            switch requestName
            {
            case "Login":
                notification.post(name: networkNotification.verifyLoginSuccess.notification, object: additionalData, userInfo:messageDict)
            case "Manis":
                notification.post(name: networkNotification.getManisSuccess.notification, object: additionalData, userInfo:messageDict)
            case "EbizuMember":
                notification.post(name: networkNotification.isEbizuMemberSuccess.notification, object: additionalData, userInfo:messageDict)
            case "upload_db_file":
                notification.post(name: networkNotification.sendLocalDbSuccess.notification, object: additionalData, userInfo: messageDict)
            case "MaybankQr":
                notification.post(name: networkNotification.getMaybankQrPaySuccess.notification, object: additionalData, userInfo:messageDict)
            case "MaybankQrStatus":
                notification.post(name: networkNotification.getQrPaySuccess.notification, object: additionalData, userInfo:messageDict)
            case readingApi.readingAccountSubscriptionStatus.rawValue:
                notification.post(name: networkNotification.AccountStatusUpdated.notification, object: additionalData, userInfo: messageDict)
            default:
                break
            }
            print("success response from \(requestName) with message : \(message)")
        case .fail:
            switch requestName
            {
            case "Login":
                notification.post(name: networkNotification.verifyLoginFail.notification, object: additionalData, userInfo:messageDict)
            case "Manis":
                notification.post(name: networkNotification.getManisFail.notification, object: additionalData, userInfo:messageDict)
            case "EbizuMember":
                notification.post(name: networkNotification.isEbizuMemberFail.notification, object: additionalData, userInfo:messageDict)
            case "upload_db_file":
                notification.post(name: networkNotification.sendLocalDbFail.notification, object: additionalData, userInfo: messageDict)
            case "MaybankQr":
                notification.post(name: networkNotification.getMaybankQrPayFail.notification, object: additionalData, userInfo:messageDict)
            case "MaybankQrStatus":
                notification.post(name: networkNotification.getQrPayFail.notification, object: additionalData, userInfo:messageDict)
            case writingApi.writingTransaction.rawValue,
                 readingApi.readingTransactionSyncId.rawValue:
                notification.removeObserver(self, name: networkNotification.resyncTransaction.notification, object: nil)
            default:
                break
            }
            print("fail response from \(requestName) with message : \(message)")
        }
        
        //post notification to transactionSyncForm
        let responseDict = syncResponse.init(name: requestName, message: message, status: status)
        notification.post(name: networkNotification.transactionSyncForm.notification, object: responseDict, userInfo: nil)
    }
    
    fileprivate func handleResponse(from requestName:String, withDict dict:[String:AnyObject])
    {
        print("response from \(requestName) : \(dict)")
        
        switch requestName
        {
        //READING API RESPONSE
        case "Login":
            parseLoginResponse(dictResponse: dict)
        case "Manis":
            parseManisResponse(dictResponse: dict)
        case "EbizuMember":
            parseEbizuMemberResponse(dictResponse: dict)
        case "MaybankQr":
            parseMaybankQrResponse(dictResponse: dict)
        case "MaybankQrStatus":
            parseMaybankQrStatusResponse(dictResponse: dict)
        case readingApi.readingShift.rawValue:
            parseReadingShiftResponse(dictResponse: dict)
        case readingApi.readingTimecard.rawValue:
            parseReadingTimecardResponse(dictResponse: dict)
        case readingApi.readingSetting.rawValue:
            parseReadingSettingResponse(dictResponse: dict)
        case readingApi.readingMenu.rawValue:
            parseReadingMenuResponse(dictResponse: dict)
        case readingApi.readingTableArea.rawValue:
            parseReadingAreaResponse(dictResponse: dict)
        case readingApi.readingTable.rawValue:
            parseReadingTableResponse(dictResponse: dict)
        case readingApi.readingPaymentMethod.rawValue:
            parseReadingPaymentMethodResponse(dictResponse: dict)
        case readingApi.readingDiscount.rawValue:
            parseReadingDiscountResponse(dictResponse: dict)
        case readingApi.readingCustomer.rawValue:
            parseReadingCustomerResponse(dictResponse: dict)
        case readingApi.readingPrinter.rawValue:
            parseReadingPrinterResponse(dictResponse: dict)
        case readingApi.readingVoucher.rawValue:
            parseReadingVoucherResponse(dictResponse: dict)
        case readingApi.readingVoucherBought.rawValue:
            parseReadingVoucherBoughtResponse(dictResponse: dict)
        case readingApi.readingVoucherBoughtDetail.rawValue:
            parseReadingVoucherBoughtDetailResponse(dictResponse: dict)
        case readingApi.readingVoucherType.rawValue:
            parseReadingVoucherTypeResponse(dictResponse: dict)
        case readingApi.readingTransaction.rawValue,
             readingApi.readingRefund.rawValue,
             readingApi.readingMultiPayment.rawValue:
            parseReadingSyncAllResponse(dictResponse: dict, fromRequestName: requestName)
        case readingApi.readingModule.rawValue:
            parseReadingModulesResponse(dictResponse: dict)
        case readingApi.readingCustomInput.rawValue:
            parseReadingCustomInputResponse(dictResponse: dict)
        case readingApi.readingCashRegister.rawValue:
            parseReadingCashRegisterResponse(dictResponse: dict)
        case readingApi.readingPurchaseOrder.rawValue:
            parseReadingPurchaseOrderResponse(dictResponse: dict)
        case readingApi.readingUserright.rawValue:
            parseReadingUserrightResponse(dictResponse: dict)
        case readingApi.readingAccountSubscriptionStatus.rawValue:
            parseReadingAccountSubsriptionStatusResponse(dictResponse: dict)
        //WRITING API RESPONSE
        case "upload_db_file":
            parseWritingLocalDb(dictResponse: dict)
        case writingApi.writingTransaction.rawValue,
             readingApi.readingTransactionSyncId.rawValue:
            parseWritingSyncAllResponse(dictResponse: dict, fromRequestName: requestName)
            userDefaults.set(false, forKey: CURRENTLY_SYNCING)
            userDefaults.synchronize()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                notification.post(name: networkNotification.resyncTransaction.notification, object: nil)
            }
        case writingApi.writingRefund.rawValue:
            parseWritingSyncAllResponse(dictResponse: dict, fromRequestName: requestName)
        case writingApi.writingEmail.rawValue:
            parseWritingEmailResponse(dictResponse: dict)
        case writingApi.writingUserright.rawValue:
            parseWritingUserrightResponse(dictResponse: dict)
        case writingApi.writingCashRegister.rawValue:
            parseWritingCashRegisterResponse(dictResponse: dict)
        case writingApi.writingSetting.rawValue:
            parseWritingSettingResponse(dictResponse: dict)
        case writingApi.writingTable.rawValue:
            parseWritingTableResponse(dictResponse: dict)
        case writingApi.writingArea.rawValue:
            parseWritingAreaResponse(dictResponse: dict)
        case writingApi.writingPushToken.rawValue:
            parseWritingPushTokenResponse(dictResponse: dict)
        case writingApi.writingCustomer.rawValue:
            parseWritingCustomerResponse(dictResponse: dict)
        case writingApi.writingPrinter.rawValue:
            parseWritingPrinterResponse(dictResponse: dict)
        case writingApi.writingShift.rawValue:
            parseWritingShiftResponse(dictResponse: dict)
        case writingApi.writingTimecard.rawValue:
            parseWritingTimecardResponse(dictResponse: dict)
        case writingApi.writingVoucherBoughtDetail.rawValue:
            parseWritingVoucherBoughtDetailResponse(dictResponse: dict)
        default:
            break
        }
    }
}

// MARK: - Reading API
extension Networking
{
    // MARK: - Reading API function
    func verifyLogin(username:String, password:String)
    {
        DispatchQueue.global().async {
            let postString = ["d" : ["username":username,"password":password]]
            let urlStr = BASE_URL + "/business/login"
            self.post(data: postString, withUrl: urlStr, withRequestName: "Login")
        }
    }
    
    func getManisData(fromQrcode qrcode:String)
    {
        DispatchQueue.global().async {
            let postString =
                [
                    "t" : UserDefaults.standard.string(forKey: APP_TOKEN) ?? "",
                    "d" : ["qrcode":qrcode]
                ] as [String : Any]
            let urlStr = BASE_URL + "/pointofsale/manisvouchers"
            self.post(data: postString, withUrl: urlStr, withRequestName: "Manis")
        }
    }
    
    func isEbizuMember(fromMobileNumber mobileNumber:String)
    {
        DispatchQueue.global().async {
            let postString =
                [
                    "t" : UserDefaults.standard.string(forKey: APP_TOKEN) ?? "",
                    "d" : mobileNumber
                ]
            let urlStr = BASE_URL + "/pointofsale/is_ebizu_member"
            self.post(data: postString, withUrl: urlStr, withRequestName: "EbizuMember")
        }
    }

    func getMaybankQrCode(paymentMethodId: String, transRefId: String, amountStr: String) {
        DispatchQueue.global().async {
            let postString =
                [
                    "t" : UserDefaults.standard.string(forKey: APP_TOKEN) ?? "",
                    "d" : [
                        "payment_method_id" : paymentMethodId,
                        "transaction_ref_id" : transRefId,
                        "sales_amount": amountStr
                        ] as [String : String]
                    ] as [String : Any]
            let urlStr = BASE_URL + "/pointofsale/get_qrpay_data"
            self.post(data: postString, withUrl: urlStr, withRequestName: "MaybankQr")
        }
    }

    func getQrCodeStatus(paymentMethodId: String, qrRefId: String) {
        DispatchQueue.global().async {
            let postString =
                [
                    "t" : UserDefaults.standard.string(forKey: APP_TOKEN) ?? "",
                    "d" : [
                        "payment_method_id" : paymentMethodId,
                        "qr_ref_id" : qrRefId
                        ] as [String : String]
                    ] as [String : Any]
            let urlStr = BASE_URL + "/pointofsale/get_qrpay_transaction_status"
            self.post(data: postString, withUrl: urlStr, withRequestName: "MaybankQrStatus")
        }
    }
    
    func getData(forReading api:readingApi)
    {
        DispatchQueue.global().async {
            var postString = [String : Any]()
            switch api
            {
            case .readingTransactionSyncId:
                let data = (data_sync.sharedData().get_transaction_data_to_check_id() as? [String : AnyObject]) ?? [:]
                postString = ["t" : UserDefaults.standard.string(forKey: APP_TOKEN) ?? "",
                              "d" : data]
            default:
                postString = ["t" : UserDefaults.standard.string(forKey: APP_TOKEN) ?? "" ,
                              "p" : ["version" : self.currentApiVersion]]
            }
            let urlStr = BASE_URL + "/pointofsale/" + api.rawValue
            self.post(data: postString, withUrl: urlStr, withRequestName: api.rawValue)
        }
    }
    
    // MARK: - ONTIME Reading API response
    fileprivate func parseLoginResponse(dictResponse:[String:AnyObject])
    {
        let requestName = "Login"
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                // App Token validation
                guard
                    let loginDict = dictResponse["d"] as? [String:AnyObject],
                    let token = loginDict["token"] as? String
                    else {
                        finishResponse(withMessage: "Token is missing.\n Please contact Krypto support team", fromRequest: requestName, withStatus: .fail)
                        return
                }
                
                // Setting Data Validation
                guard
                    let settingArr = loginDict["settings"] as? [[String:AnyObject]],
                    let settingDict = settingArr.first
                    else {
                        finishResponse(withMessage: "Settings data is missing.\n Please contact Krypto support team", fromRequest: requestName, withStatus: .fail)
                        return
                }

                // Transaction Number Validation
                var transactionNo = ""
                if let safeInt = settingDict["trans_number"] as? Int {
                    transactionNo = "\(safeInt)"
                } else if let safeStr = settingDict["trans_number"] as? String {
                    transactionNo = safeStr
                } else {
                    finishResponse(withMessage: "Settings data is missing.\n Please contact Krypto support team", fromRequest: requestName, withStatus: .fail)
                    return
                }

                // Hardware Code Validation
                guard
                    let hardwareCode = settingDict["code"] as? String
                    else {
                        finishResponse(withMessage: "Hardware Code is missing.\n Please contact Krypto support team", fromRequest: requestName, withStatus: .fail)
                        return
                }

                // Business Name & Business Id Validation
                guard
                    let businessDict = loginDict["business"] as? [String:AnyObject],
                    let _ = businessDict["business_name"] as? String,
                    let _ = businessDict["business_type_id"] as? String
                    else {
                        finishResponse(withMessage: "Business name or Business type id is missing.\n Please contact Krypto support team", fromRequest: requestName, withStatus: .fail)
                        return
                }
                
                // Employees Validation
                guard
                    let userrightData = loginDict["user_rights"] as? [String:AnyObject],
                    let userrightUserData = userrightData["users"] as? [String:AnyObject],
                    let userrightUserDataList = userrightUserData["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "At least one Employee must be created.\n Please add employee on the EBC panel", fromRequest: requestName, withStatus: .fail)
                        return
                }
                
                // exception validation, data below are inconsistent from response
                let shiftData = (loginDict["shift_detail"] as? [String:AnyObject]) ?? [:]
                let shiftDataList = (shiftData["list"] as? [AnyObject]) ?? []
                let timecardData = (loginDict["emp_timecard"] as? [String:AnyObject]) ?? [:]
                let timecardDataList = (timecardData["list"] as? [AnyObject]) ?? []
                let taxData = (loginDict["tax"] as? [[String:AnyObject]]) ?? [[:]]
                let taxDataList = taxData.first ?? [:]
                let serviceCharge = (taxDataList["percentage"] as? String) ?? "0.00"
                
                // Insert Data to Local DB
                data_sync.sharedData().insert_business_details(businessDict, withToken: token)
                
                if let currentHardwareCode = userDefaults.string(forKey: HARDWARE_CODE)
                {
                    if currentHardwareCode == ""
                    {
                        data_sync.sharedData().insert_hardware_code(hardwareCode, and_taxes: serviceCharge)
                    }
                }
                else
                {
                     data_sync.sharedData().insert_hardware_code(hardwareCode, and_taxes: serviceCharge)
                }
                
                if let currentTransactionNo = userDefaults.string(forKey: TRANSACTION_NO)
                {
                    if currentTransactionNo == ""
                    {
                        data_sync.sharedData().insert_transaction_number(transactionNo)
                    }
                }
                else
                {
                    data_sync.sharedData().insert_transaction_number(transactionNo)
                }
                
                data_sync.sharedData().sync_user_rights_details(userrightUserDataList)
                data_sync.sharedData().sync_shift_details(shiftDataList)
                data_sync.sharedData().sync_employee_time_card_details(timecardDataList)
                
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseManisResponse(dictResponse:[String:AnyObject])
    {
        let requestName = "Manis"
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let voucherBoughtId = data["voucher_bought_detail_id"] as? String
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                if voucherBoughtId != ""
                {
                    finishResponse(withMessage: "Voucher Redeemed", fromRequest: requestName, withStatus: .Success, withAdditionalData: data as AnyObject)
                }
                else
                {
                    finishResponse(withMessage: "Voucher doesn't exist", fromRequest: requestName, withStatus: .fail)
                }
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseEbizuMemberResponse(dictResponse:[String:AnyObject])
    {
        let requestName = "EbizuMember"
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                finishResponse(withMessage: "Customer Exist", fromRequest: requestName, withStatus: .Success, withAdditionalData: data as AnyObject)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }

    fileprivate func parseMaybankQrResponse(dictResponse:[String:AnyObject]) {
        let requestName = "MaybankQr"

        guard
            let data = dictResponse["d"] as? [String : AnyObject],
            let status = data["status"] as? String
            else {
                finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                return
        }

        if status == "OK" {
            finishResponse(withMessage: "QrCode Received", fromRequest: requestName, withStatus: .Success, withAdditionalData: data as AnyObject)
        } else if status == "FAIL" {
            let errCode = (data["error_code"] as? String) ?? ""
            let errMsg = (data["error_msg"] as? String) ?? ""
            finishResponse(withMessage: "\(errCode) : \(errMsg)", fromRequest: requestName, withStatus: .fail)
        } else {
            finishResponse(withMessage: "invalid status", fromRequest: requestName, withStatus: .fail)
        }
    }

    fileprivate func parseMaybankQrStatusResponse(dictResponse:[String:AnyObject]) {
        let requestName = "MaybankQrStatus"

        guard
            let data = dictResponse["d"] as? [String : AnyObject],
            let status = data["status"] as? String
            else {
                finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                return
        }

        if status == "OK" || status == "PENDING" {
            finishResponse(withMessage: "QrPay Status Received", fromRequest: requestName, withStatus: .Success, withAdditionalData: data as AnyObject)
        } else if status == "FAIL" {
            let errCode = (data["error_code"] as? String) ?? ""
            let errMsg = (data["error_msg"] as? String) ?? ""
            finishResponse(withMessage: "\(errCode) : \(errMsg)", fromRequest: requestName, withStatus: .fail)
        } else {
            finishResponse(withMessage: "invalid status", fromRequest: requestName, withStatus: .fail)
        }
    }
    
    // MARK: - Reading API response
    // to my future successor, how i wish i can standardize and centralize all the reading function below, but as life never always goes your ways, so does the response, appearently the response back is not standardize, so we might receive "d" with array or dictionary and "e" with string or array or dictionary. so with heavy sigh.., each response must have their own function to handle the data. (August ,Oct 2017)
    fileprivate func parseReadingSyncAllResponse(dictResponse:[String:AnyObject], fromRequestName requestName:String)
    {
        // this function is abit different then the rest below it,
        // this function was shared by all reading response that used the new format
        // new format example :
        // d : "TRANSACTION" : [[*perColumnData*],[*perColumnData*]] ,
        //     "TRN_ITEMS" : [[*perColumnData*],[*perColumnData*]]
        // for now, this new format only used in transaction, refund & transaction multi payment
        // the future plan is of course for all response to follow the same format so that it can standardize.
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_reading_response(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingShiftResponse(dictResponse:[String:AnyObject])
    {
        let requestName = readingApi.readingShift.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_shift_details(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingTimecardResponse(dictResponse:[String:AnyObject])
    {
        let requestName = readingApi.readingTimecard.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_employee_time_card_details(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingSettingResponse(dictResponse:[String:AnyObject])
    {        
        let requestName = readingApi.readingSetting.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().insert_settings(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingMenuResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingMenu.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [[String:AnyObject]],
                    let dataList = data.first,
                    let dataListChild = dataList["child"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_menus(dataListChild)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingAreaResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingTableArea.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().add_area(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingTableResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingTable.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().add_table(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingPaymentMethodResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingPaymentMethod.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().add_paymethod(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingDiscountResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingDiscount.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().add_discount(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingCustomerResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingCustomer.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_customer_details(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingPrinterResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingPrinter.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_printers(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingVoucherResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingVoucher.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().add_voucher(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingVoucherBoughtResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingVoucherBought.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_voucher_bought(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingVoucherBoughtDetailResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingVoucherBoughtDetail.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().add_voucher_bougth_details(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingVoucherTypeResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingVoucherType.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().add_voucher_type(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingModulesResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingModule.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_modules_details(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingCustomInputResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingCustomInput.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().set_custom_field_name(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingCashRegisterResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingCashRegister.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_cash_register(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingPurchaseOrderResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingPurchaseOrder.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().add_edit_po_receive(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingUserrightResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingUserright.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let dataList = data["list"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().sync_user_rights_details(dataList)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseReadingAccountSubsriptionStatusResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  readingApi.readingAccountSubscriptionStatus.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [String:AnyObject],
                    let status = data["status"] as? String
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                userDefaults.set(NSDate().timeIntervalSince1970, forKey: LAST_ACC_STATUS_CHECK)
                userDefaults.set(status, forKey: ACC_STATUS)
                userDefaults.synchronize()
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    //MARK: - Call Bulk api
    func executePartialReadingApi() -> [String]
    {
        getData(forReading: .readingCashRegister)
        getData(forReading: .readingSetting)
        getData(forReading: .readingModule)
        getData(forReading: .readingTableArea)
        getData(forReading: .readingTable)
        getData(forReading: .readingPaymentMethod)
        getData(forReading: .readingDiscount)
        getData(forReading: .readingVoucher)
        getData(forReading: .readingVoucherType)
        getData(forReading: .readingVoucherBought)
        getData(forReading: .readingVoucherBoughtDetail)
        getData(forReading: .readingMenu)
        getData(forReading: .readingCustomer)
        getData(forReading: .readingPrinter)
        getData(forReading: .readingCustomInput)
        
//        // Unused Reading API below
//        getData(forReading: .readingPurchaseOrder)
        
        // return requestname so that transactionSyncForm know which response to wait
        return getPartialReadingApiResponseName()
    }
    
    func getPartialReadingApiResponseName() -> [String]
    {
        return
            getStoreInformationReadingApiResponseName() +
            getItemInformationReadingApiResponseName() +
            getCustomerInformationReadingApiResponseName() +
            getSettingInformationReadingApiResponseName()
    }
    
    // Store Information > Item Information > Customer Information > Setting > Transaction Information > Employee Information
    // for clasification purpose only so transactionSyncForm know what to expect
    func getStoreInformationReadingApiResponseName() -> [String]
    {
        return
            [
                readingApi.readingCashRegister.rawValue,
                readingApi.readingModule.rawValue,
                readingApi.readingTableArea.rawValue,
                readingApi.readingTable.rawValue,
                readingApi.readingPaymentMethod.rawValue,
                readingApi.readingDiscount.rawValue,
                readingApi.readingVoucher.rawValue,
                readingApi.readingVoucherType.rawValue,
                readingApi.readingVoucherBought.rawValue,
                readingApi.readingVoucherBoughtDetail.rawValue,
                readingApi.readingCustomInput.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getItemInformationReadingApiResponseName() -> [String]
    {
        return
            [
                readingApi.readingMenu.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getCustomerInformationReadingApiResponseName() -> [String]
    {
        return
            [
                readingApi.readingCustomer.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getSettingInformationReadingApiResponseName() -> [String]
    {
        return
            [
                readingApi.readingSetting.rawValue,
                readingApi.readingPrinter.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func executeAllReadingApi() -> [String]
    {
        _ = executePartialReadingApi()
        
        getData(forReading: .readingMultiPayment)
        getData(forReading: .readingTransaction)
        getData(forReading: .readingRefund)
        getData(forReading: .readingUserright)
        getData(forReading: .readingShift)
        getData(forReading: .readingTimecard)
        
        // return requestname so that transactionSyncForm know which response to wait
        return getAllReadingApiResponseName()
    }
    
    func getAllReadingApiResponseName() -> [String]
    {
			var responseNames: [String] = []
			responseNames += getPartialReadingApiResponseName()
			responseNames += getTransactionInformationReadingApiResponseName()
			responseNames += getEmployeeInformationReadingApiResponseName()

			return responseNames
    }
    
    // Store Information > Item Information > Customer Information > Setting > Transaction Information > Employee Information
    // for clasification purpose only so transactionSyncForm know what to expect
    func getTransactionInformationReadingApiResponseName() -> [String]
    {
        return
            [
                readingApi.readingMultiPayment.rawValue,
                readingApi.readingTransaction.rawValue,
                readingApi.readingRefund.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getEmployeeInformationReadingApiResponseName() -> [String]
    {
        return
            [
                readingApi.readingUserright.rawValue,
                readingApi.readingShift.rawValue,
                readingApi.readingTimecard.rawValue
            ]
    }
    
}

//MARK: - Writing API
extension Networking
{
    // MARK: - Writing API function
    func putLocalDb(localDbData:[String:String])
    {
        DispatchQueue.global().async {
            let postString =
                [
                    "t" : UserDefaults.standard.string(forKey: APP_TOKEN) ?? "" ,
                    "d" : localDbData
                ] as [String : Any]
        
            let requestName = "upload_db_file"
            let urlStr = BASE_URL + "/pointofsale/" + requestName
            self.post(data: postString, withUrl: urlStr, withRequestName: requestName)
        }
    }
    
    fileprivate func putDataForTransactionManualSync()
    {
        notification.addObserver(self, selector: #selector(resyncTransaction), name: networkNotification.resyncTransaction.notification, object: nil)
        putData(forWriting: .writingTransaction)
    }
    
    @objc fileprivate func resyncTransaction()
    {
        putData(forWriting: .writingTransaction)
    }
    
    func putData(forWriting api:writingApi)
    {
        DispatchQueue.global().async {
            var dataExist = false
            var data:Any!
            
            switch api
            {
            case .writingTransaction:
                if !userDefaults.bool(forKey: CURRENTLY_SYNCING)
                {
                    let tempData = (data_sync.sharedData().get_transactions_for_sync() as? [String:AnyObject]) ?? [:]
                    dataExist = tempData.count > 0
                    data = tempData
                    if !dataExist
                    {
                        notification.removeObserver(self, name: networkNotification.resyncTransaction.notification, object: nil)
                        self.finishResponse(withMessage: "", fromRequest: api.rawValue, withStatus: .Success)
                        self.putData(forWriting: .writingRefund)
                    }
                }
                else
                {
                    self.getData(forReading: .readingTransactionSyncId)
                }
            case .writingEmail:
                let tempData = (data_sync.sharedData()._pending_emails as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingRefund:
                let tempData = (data_sync.sharedData().get_refund_for_sync() as? [String:AnyObject]) ?? [:]
                dataExist = tempData.count > 0
                data = tempData
            case .writingPurchaseOrder:
                let poData = data_sync.sharedData()._po_receive_for_sync as [AnyObject]
                let poItem = data_sync.sharedData()._po_receive_item_for_sync as [AnyObject]
                if poData.count > 0
                {
                    dataExist = true
                    data = ["detail" : poData, "item" : poItem]
                }
            case .writingVoucherBoughtDetail:
                let tempData = (data_sync.sharedData()._voucher_redeem_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingSetting:
                let tempData = (data_sync.sharedData()._settings_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingCashRegister:
                let tempData = (data_sync.sharedData().get_cash_register_for_sync() as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingCategory:
                let tempData = (data_sync.sharedData()._new_category_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingItem:
                let tempData = (data_sync.sharedData()._new_items_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingTable:
                let tempData = (data_sync.sharedData()._new_table_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingArea:
                let tempData = (data_sync.sharedData().getAreaForSync() as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingPushToken:
                let tempData = [
                    "company_id"    : StringUtils.get_business_detail("business_id"),
                    "hardware_code" : userDefaults.string(forKey: HARDWARE_CODE),
                    "push_id"       : appDelegate?.push_device_token,
                    "app_version"   : Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString"),
                    "build"         : Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")
                ]
                dataExist = true
                data = tempData
            case .writingShift:
                let tempData = (data_sync.sharedData()._shift_details_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingTimecard:
                let tempData = (data_sync.sharedData()._employee_timecard_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingCustomer:
                let tempData = (data_sync.sharedData()._customers_data_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingUserright:
                let tempData = (data_sync.sharedData().get_userright_for_sync() as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingErrorReport:
                let tempData = (data_sync.sharedData()._error_data_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingHoldOrderNo:
                let tempData = (data_sync.sharedData()._hold_order_number_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingCustomInput:
                let tempData = (data_sync.sharedData()._customers_data_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            case .writingPrinter:
                let tempData = (data_sync.sharedData()._printer_data_for_sync as [AnyObject])
                dataExist = tempData.count > 0
                data = tempData
            }
            
            // send data to server
            if dataExist
            {
                // only for refund we modify the raw value because it share the same url with transaction
                let urlStr = BASE_URL + "/pointofsale/" + (api == .writingRefund ? "sync_all" : api.rawValue)
                let postString = ["t" : UserDefaults.standard.string(forKey: APP_TOKEN) ?? "" ,
                                  "d" : data ,
                                  "p" : ["version" : self.currentApiVersion]] as [String : Any]
                self.post(data: postString, withUrl: urlStr, withRequestName: api.rawValue)
            }
            else
            {
                self.finishResponse(withMessage: "no data to send to server", fromRequest: api.rawValue, withStatus: .noDataToSend)
                print("no data to send to server for \(api.rawValue)")
            }
        }
    }
    
    //MARK: - Writing API response
    fileprivate func parseWritingSyncAllResponse(dictResponse:[String:AnyObject], fromRequestName requestName:String)
    {
        // this function is abit different then the rest below it,
        // this function was shared by all writing response that used the new format
        // new format example :
        // d : "TRANSACTION" : [[*perColumnData*],[*perColumnData*]] ,
        //     "TRN_ITEMS" : [[*perColumnData*],[*perColumnData*]]
        // for now, this new format only used in transaction, refund & transaction multi payment
        // also use on check_sync_id_from_transaction for reading
        // the future plan is of course for all response to follow the same format so that it can standardize.
        data_sync.sharedData().update_response(dictResponse)
        
        switch requestName {
        case writingApi.writingTransaction.rawValue:
            finishResponse(withMessage: "", fromRequest: requestName, withStatus: .onProgress)
        default:
            finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
        }
        
        // TODO: need to capture the e to display the error
    }
    
    fileprivate func parseWritingEmailResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingEmail.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().delete_pending_emails(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                guard
                    let data = dictResponse["e"] as? [AnyObject]
                    else { return }
                data_sync.sharedData().edit_pending_emails(data)
                finishResponse(withMessage: "Fail to send email", fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingUserrightResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingUserright.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().update_user_right_details(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingCashRegisterResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingCashRegister.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().edit_cash_register_(from_sync:data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingSettingResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingSetting.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().edit_settings_sync_details(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingTableResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingTable.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().update_new_tables(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingAreaResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingArea.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().updateAreaSyncId(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingPushTokenResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingPushToken.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let _ = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingCustomerResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingCustomer.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().update_new_customers(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingPrinterResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingPrinter.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().update_new_printers(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingShiftResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingShift.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().edit_shift_details_(from_sync:data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingTimecardResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingTimecard.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().edit_emp_timecard_(from_sync:data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingVoucherBoughtDetailResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingVoucherBoughtDetail.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().edit_voucher_redeemed_(from_sync:data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingLocalDb(dictResponse:[String:AnyObject])
    {
        let requestName = "upload_db_file"
        
        if dictResponse.count > 0
        {
            finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
        }
        else
        {
            finishResponse(withMessage: "", fromRequest: requestName, withStatus: .fail)
        }
    }
    
    // MARK: NOT USED WRITING API RESPONSE
    fileprivate func parseWritingCategoryResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingCategory.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().update_synced_categories(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingItemResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingItem.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().update_synced_items(data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    fileprivate func parseWritingCustomInputResponse(dictResponse:[String:AnyObject])
    {
        let requestName =  writingApi.writingCustomInput.rawValue
        
        for key in dictResponse.keys
        {
            if key == "d"
            {
                guard
                    let data = dictResponse["d"] as? [AnyObject]
                    else {
                        finishResponse(withMessage: "no data found", fromRequest: requestName, withStatus: .fail)
                        return
                }
                data_sync.sharedData().update_custom_input_(from_sync:data)
                finishResponse(withMessage: "", fromRequest: requestName, withStatus: .Success)
            }
            else if key == "e"
            {
                let errorMessage = (dictResponse["e"] as? String) ?? ""
                finishResponse(withMessage: errorMessage, fromRequest: requestName, withStatus: .fail)
            }
        }
    }
    
    //MARK: - Call Bulk api
    func executeAllWritingApi() -> [String]
    {
        putDataForTransactionManualSync()
        putData(forWriting: .writingEmail)
        putData(forWriting: .writingVoucherBoughtDetail)
        putData(forWriting: .writingSetting)
        putData(forWriting: .writingCashRegister)
        putData(forWriting: .writingTable)
        putData(forWriting: .writingArea)
        putData(forWriting: .writingCustomer)
        putData(forWriting: .writingPrinter)
        putData(forWriting: .writingPushToken)
        putData(forWriting: .writingShift)
        putData(forWriting: .writingTimecard)
        putData(forWriting: .writingUserright)
        
        //        //Unused Writing API below
        //        putData(forWriting: .writingPurchaseOrder)
        //        putData(forWriting: .writingHoldOrderNo)
        //        putData(forWriting: .writingErrorReport)
        //        putData(forWriting: .writingCustomInput)
        //        putData(forWriting: .writingCategory)
        //        putData(forWriting: .writingItem)
        
        // return requestname so that transactionSyncForm know which response to wait
        return getAllWritingApiResponseName()
    }
    
    func getAllWritingApiResponseName() -> [String] {
			var responseNames: [String] = []
			responseNames += getStoreInformationWritingApiResponseName()
      responseNames += getItemInformationWritingApiResponseName()
			responseNames += getCustomerInformationWritingApiResponseName()
			responseNames += getSettingInformationWritingApiResponseName()
			responseNames += getTransactionInformationWritingApiResponseName()
			responseNames += getEmployeeInformationWritingApiResponseName()
			return responseNames
    }
    
    // Store Information > Item Information > Customer Information > Setting > Transaction Information > Employee Information
    // for clasification purpose only so transactionSyncForm know what to expect
    func getStoreInformationWritingApiResponseName() -> [String]
    {
        return
            [
                writingApi.writingVoucherBoughtDetail.rawValue,
                writingApi.writingCashRegister.rawValue,
                writingApi.writingTable.rawValue,
                writingApi.writingArea.rawValue,
                writingApi.writingPushToken.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getItemInformationWritingApiResponseName() -> [String]
    {
        return []
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getCustomerInformationWritingApiResponseName() -> [String]
    {
        return
            [
                writingApi.writingCustomer.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getSettingInformationWritingApiResponseName() -> [String]
    {
        return
            [
                writingApi.writingPrinter.rawValue,
                writingApi.writingSetting.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getTransactionInformationWritingApiResponseName() -> [String]
    {
        return
            [
                writingApi.writingTransaction.rawValue,
                writingApi.writingRefund.rawValue, // writingRefund was called upon call writingTransaction
                writingApi.writingEmail.rawValue
            ]
    }
    
    // for clasification purpose only so transactionSyncForm know what to expect
    func getEmployeeInformationWritingApiResponseName() -> [String]
    {
        return
            [
                writingApi.writingShift.rawValue,
                writingApi.writingTimecard.rawValue,
                writingApi.writingUserright.rawValue
            ]
    }

}
