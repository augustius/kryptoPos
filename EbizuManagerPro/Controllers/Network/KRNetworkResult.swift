//
//  KRNetworkResult.swift
//  KryptoPOS
//
//  Created by augustius cokroe on 09/03/2019.
//  Copyright © 2019 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation
/// Result type for API calling with 2 types of response
///
/// - success: `T` of generic object.
/// - failure: `ErrorType` of any objects that conform to `ErrorType` protocol.
enum KRNetworkResult<T> {
    case success(T)
    case failure(Any)
}
