//
//  KRNetworkRequest.swift
//  KryptoPOS
//
//  Created by augustius cokroe on 09/03/2019.
//  Copyright © 2019 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation
import Alamofire

struct KRNetworkRequest {

    static func request(_ httpMethod: HTTPMethod, endpoint: KRNetworkEndpoint, needsAuth: Bool, parameters: [String: Any]?, completion: @escaping (_ result: KRNetworkResult<Any>) -> Void) {
        request(httpMethod, customEndpoint: endpoint.rawValue, needsAuth: needsAuth, parameters: parameters, completion: completion)
    }

    static func request(_ httpMethod: HTTPMethod, customEndpoint: String, needsAuth: Bool, parameters: [String: Any]?, completion: @escaping (_ result: KRNetworkResult<Any>) -> Void) {

        // baseUrl
        let url: String = BASE_URL + customEndpoint
        // parameters
        var parameters: [String: Any]? = parameters == nil ? [:] : parameters
        if needsAuth, let authToken = UserDefaults.standard.string(forKey: APP_TOKEN), !authToken.isEmpty {
            parameters?["t"] = authToken
        }

        Alamofire.request(url, method: httpMethod, parameters: parameters, headers: nil)
            .validate()
            .responseJSON(){ (response) in
                switch response.result {
                case .success(let json):
                    print("🎉 \(customEndpoint):\n \(json)")
                    completion(.success(json))
                case .failure(let error):
                    print("💩 \(url): \(error.localizedDescription)")
                    completion(.failure(error))
                }
        }
    }
}

// MARK: - Networking call request for file uploader
extension KRNetworkRequest {

    /// Send POST Multipart network call request
    ///
    /// - Parameters:
    ///   - endpoint: attachment upload endpoint
    ///   - attachmentData: atatchment data
    ///   - fileName: atatchment file name
    ///   - progressHandler: progress handler
    ///   - completion: success(NetworkResult<Data>) / failure(String)
    static func postMultiPart(_ endpoint: KRNetworkEndpoint, attachmentData: Data, fileName: String, progressHandler: @escaping (_ progress: Progress) -> Void, completion: @escaping (_ result: KRNetworkResult<Any>) -> Void) {

        // baseUrl
        let url: String = BASE_URL + endpoint.rawValue

        Alamofire.upload(multipartFormData: { (multipartData) in
            self.multipartDataHandler(endpoint, formData: multipartData, data: attachmentData, fileName: fileName)
        }, to: url, method: .post, headers: nil, encodingCompletion: { (encodingResult) in
            switch encodingResult {
            case .success(let request, _, _):
                request.uploadProgress(closure: progressHandler)
                self.uploadFile(dataRequest: request, completion: { (result) in
                    print("🎉 \(endpoint.rawValue):\n \(result)")
                    completion(result)
                })
            case .failure(let error):
                print("💩 \(url): \(error.localizedDescription)")
                completion(.failure(error))
            }
        })
    }

    /// Handle multipart data encoding
    ///
    /// - Parameters:
    ///   - formData: multipart data
    ///   - data: file data
    ///   - fileName: file name
    private static func multipartDataHandler(_ endpoint: KRNetworkEndpoint ,formData: MultipartFormData, data: Data, fileName: String) {

        let fileNameString = fileName as NSString
        let fileExtension = fileNameString.pathExtension.lowercased()
        let mimeType: String

        switch fileExtension {
        case "jpg", "jpeg":
            mimeType = "image/jpeg"
        case "png":
            mimeType = "image/png"
        case "pdf":
            mimeType = "application/pdf"
        default:
            mimeType = "text/plain"
        }

        let token = UserDefaults.standard.string(forKey: APP_TOKEN) ?? ""
        formData.append(token.data(using: String.Encoding.utf8)!, withName: "t")
        formData.append(data, withName: "d", fileName: fileName, mimeType: mimeType)
    }

    /// Upload encoded data to server
    ///
    /// - Parameters:
    ///   - request: upload request
    ///   - completion: success(NetworkResult<Data>) / failure(String)
    private static func uploadFile(dataRequest: DataRequest, completion: @escaping (_ result: KRNetworkResult<Any>) -> Void) {

        dataRequest.validate()
            .responseData(completionHandler: { (response) in
                switch response.result {
                case .success(let json):
                    completion(.success(json))
                case .failure(let error):
                    completion(.failure(error))
                }
            })
    }
}

