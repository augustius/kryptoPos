//
//  NSData+StringConversion.m
//  EADemo
//
//  Created by Daliso Ngoma on 01/03/2017.
//  Copyright © 2017 DTS. All rights reserved.
//

#import "NSData+StringConversion.h"

@implementation NSData (StringConversion)

#pragma mark - String Conversion

- (NSString *)hexString
{
    NSUInteger bytesCount = self.length;
    if (bytesCount) {
        const char *hexChars = "0123456789ABCDEF";
        const unsigned char *dataBuffer = self.bytes;
        char *chars = malloc(sizeof(char) * (bytesCount * 2 + 1));
        char *s = chars;
        for (unsigned i = 0; i < bytesCount; ++i) {
            *s++ = hexChars[((*dataBuffer & 0xF0) >> 4)];
            *s++ = hexChars[(*dataBuffer & 0x0F)];
            dataBuffer++;
        }
        *s = '\0';
        NSString *hexString = [NSString stringWithUTF8String:chars];
        free(chars);
        return hexString;
    }
    return @"";
}

@end
