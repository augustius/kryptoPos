//
//  StaticFunctions.m
//  cardbiz
//
//  Created by Daliso Ngoma on 2017/04/11.
//  Copyright © 2017 Daliso Ngoma. All rights reserved.
//

#import "StaticFunctions.h"
#import "EADSessionController.h"

@implementation StaticFunctions


+ (char)calculateLRC:(NSString *) text
{
    
    NSData * data = [self hexToBytes:text];
    
    NSUInteger size = [data length] / sizeof(const char);
    const char * array = (const char*) [data bytes];
    
    char checksum = 0;
    
    for( uint32_t i = 0 ; i < size; i++)
    {
        
        checksum ^= * array++;
    }
    
    NSLog(@"Checksum = %c", checksum);
    
    return checksum;
}

+ (NSString *)hexStringOfLRC:(NSString *)text
{
    
    NSString *tempString = [NSString stringWithFormat:@"%02x", [self calculateLRC:text]];
    
    return tempString;

}

+ (NSData*)hexToBytes:(NSString *)hexaStr
{
    
    NSMutableData* data = [NSMutableData data];
    int idx;
    for (idx = 0; idx+2 <= hexaStr.length; idx+=2)
    {
        NSRange range = NSMakeRange(idx, 2);
        NSString * hexStrTmp = [hexaStr substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStrTmp];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
        [data appendBytes:&intValue length:1];
    }
    return data;
}


+ (void)sendToDevice:(NSString *)withString
{
    
    const char *buf = [withString UTF8String];
    
    NSMutableData *data = [NSMutableData data];
    if (buf)
    {
        uint32_t len = (uint32_t)strlen(buf);
        
        char singleNumberString[3] = {'\0', '\0', '\0'};
        uint32_t singleNumber = 0;
        for(uint32_t i = 0 ; i < len; i+=2)
        {
            if ( ((i+1) < len) && isxdigit(buf[i]) && (isxdigit(buf[i+1])) )
            {
                singleNumberString[0] = buf[i];
                singleNumberString[1] = buf[i + 1];
                sscanf(singleNumberString, "%x", &singleNumber);
                uint8_t tmp = (uint8_t)(singleNumber & 0x000000FF);
                [data appendBytes:(void *)(&tmp) length:1];
            }
            else
            {
                break;
            }
        }
        
        [[EADSessionController sharedController] writeData:data];
    }
}

@end
