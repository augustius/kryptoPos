//
//  StaticFunctions.h
//  cardbiz
//
//  Created by Daliso Ngoma on 2017/04/11.
//  Copyright © 2017 Daliso Ngoma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StaticFunctions : NSObject

+ (char)calculateLRC:(NSString *) text;
+ (NSString *)hexStringOfLRC:(NSString *) text;
+ (void)sendToDevice:(NSString *)withString;

@end
