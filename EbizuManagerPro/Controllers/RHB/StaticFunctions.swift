//
//  StaticFunctions.swift
//  cardbiz
//
//  Created by Daliso Ngoma on 2017/04/11.
//  Copyright © 2017 Daliso Ngoma. All rights reserved.
//

import Foundation

extension StaticFunctions
{
    static func stringHexForMPOS() -> String
    {
        let amount = "0000 0000 0850"
        return stringHexForMPOS(withAmount: amount)
    }

    static func stringHexForMPOS(withAmount amount: String ) -> String
    {
        let transactionId = "0000 0000 0000 0000 0123"
        return stringHexForMPOS(withTransactionId: transactionId, andAmount: amount)
    }
    
    // TODO: Put parameters for sending data
    static func stringHexForMPOS(withTransactionId transactionId: String, andAmount amount: String ) -> String
    {
        var customValueHexString = "02010836303030303030303030313032303030301C30300020"
        
        // Pay Account ID - For now 20 digits of zeros - DON'T CHANGE
        let payAccountId = "0000 0000 0000 0000 0000"
        
        customValueHexString += changeToHex(withNum: payAccountId)
        
        // TODO: Name this section properly
        customValueHexString += "1C36360020"
        
        // Transaction ID - The value is 20 digits long
        // Example: 0000 0000 0000 0000 0123
        
        customValueHexString += changeToHex(withNum: transactionId)
        
        // TODO: Name this section properly
        customValueHexString += "1C34300012"
        
        // Amount - The value is 12 digits long
        // Example: 0000 0000 0850
        
        customValueHexString += changeToHex(withNum: amount)
        
        // TODO: Name this section properly
        customValueHexString += "1C343200123030303030303030303030301C30390001301C03"
        
        let index = customValueHexString.index(customValueHexString.startIndex, offsetBy: 2)
        
        let stringForLRC = String(customValueHexString.substring(from: index).characters)
        
        let hexStringOfLRC: String = StaticFunctions.hexString(ofLRC: stringForLRC)
        
        customValueHexString += hexStringOfLRC
        
        print("Hex to be sent: \(customValueHexString)")
        
        return customValueHexString
    }
    
    static func changeToHex(withNum string: String) -> String
    {
        var tempString = ""
        for i in string.removeWhitespace()
        {
            tempString += "3"+String(i)
        }
        return tempString
    }
}
