//
//  TagStructure.swift
//  cardbiz
//
//  Created by Daliso Ngoma on 2017/04/13.
//  Copyright © 2017 Daliso Ngoma. All rights reserved.
//

import Foundation

enum TagType: String {
    case payAccountId       = "3030"
    case approvalCode       = "3031"
    case responseText       = "3032"
    case transactionDate    = "3033"
    case transactionTime    = "3034"
    case terminalId         = "3136"
    case receiptData        = "3139"
    case cardNo             = "3330"
    case expiryDate         = "3331"
    case memberExpiry       = "3333"
    case batchNo            = "3530"
    case transactionId      = "3636"
    case invoiceNumber      = "3635"
    case merchantName       = "4430"
    case merchantNo         = "4431"
    case cardIssuerName     = "4432"
    case cardholderName     = "4435"
    case unknownType        = "unknown"
    
    var name : String {
        get {
            switch(self) {
            case .payAccountId:
                return "Pay Account ID"
            case .responseText:
                return "Response Text"
            case .approvalCode:
                return "Approval Code"
            case .invoiceNumber:
                return "Invoice Number"
            case .cardNo:
                return "Card Number"
            case .cardholderName:
                return "Card Holder Name"
            case .expiryDate:
                return "Expiry Date"
            case .memberExpiry:
                return "Member Expiry"
            case .terminalId:
                return "Terminal ID"
            case .merchantNo:
                return "Merchant Number"
            case .merchantName:
                return "Merchant Name"
            case .batchNo:
                return "Batch Number"
            case .transactionDate:
                return "Transaction Date"
            case .transactionTime:
                return "Transaction Time"
            case .cardIssuerName:
                return "Card Issuer Name"
            case .receiptData:
                return "Receipt Data"
            case .transactionId:
                return "Transaction ID"
            default:
                return "Unknown Type"
            }
        }
    }
}

struct Tag {
    let name: String
    let id: String
    let hexValue: String
    let stringValue: String
}

enum TagError: Error {
    case SomeError
}
