//
//  NSData+StringConversion.h
//  EADemo
//
//  Created by Daliso Ngoma on 01/03/2017.
//  Copyright © 2017 DTS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (StringConversion)

#pragma mark - String Conversion
- (NSString *)hexString;

@end
