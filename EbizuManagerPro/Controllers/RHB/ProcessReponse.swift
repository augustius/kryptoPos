//
//  ProcessReponse.swift
//  cardbiz
//
//  Created by Daliso Ngoma on 2017/04/13.
//  Copyright © 2017 Daliso Ngoma. All rights reserved.
//

import Foundation

func processResponse(withTextArray texts: [String])  -> [TagType:Tag]
{
    
    var tagArray: [TagType:Tag] = [:]
    
    // This is just testing the happy path of things, should put error handing for unhappy path
    
    let text: String = texts.reduce("", +)
    
    let parts: [String] = text.components(separatedBy: "1C")
    
    for j in 1..<parts.count-1
    {
        let part: String = parts[j] // Some Part of String - Works from 1 to 7
        
        let typeIndex: String.Index = part.index(part.startIndex, offsetBy: 4)
        
        let type: String = part.substring(to: typeIndex)
        
        var someType: TagType = .unknownType
        if let tempSomeType: TagType = TagType(rawValue: type)
        {
            someType = tempSomeType
        }
        
        let lengthIndexStart:   String.Index = part.index(part.startIndex, offsetBy: 4)
        let lengthIndexEnd:     String.Index = part.index(part.startIndex, offsetBy: 8)
        let lengthIndexRange:   Range<String.Index> = lengthIndexStart..<lengthIndexEnd
        
        let length:     String = part.substring(with: lengthIndexRange)
        let lengthInt:  Int? = Int(length)
        
        let valueIndexStart:    String.Index = part.index(part.startIndex, offsetBy: 8)
        let valueIndexEnd:      String.Index = part.index(part.startIndex, offsetBy: 8+(lengthInt!*2))
        let valueRange:         Range<String.Index> = valueIndexStart..<valueIndexEnd
        
        let value: String = part.substring(with: valueRange)
        
        tagArray[someType] = Tag(name: someType.name,
                                 id: someType.rawValue == "unknown" ? "\(type)" : someType.rawValue,
                                 hexValue: value,
                                 stringValue: value.hexToString()!)
    }
    return tagArray
}
