//
//  RHB.swift
//  EbizuManagerPro
//
//  Created by Daliso Ngoma on 2017/04/15.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

@objc class RHB: NSObject,ExternalPaymentProtocol
{
    
    // MARK: Singleton Declaration
    // swiftSharedExPayment is not accessible from ObjC
    class var swiftSharedExPayment: RHB
    {
        struct Singleton
        {
            static let SharedExPayment = RHB()
        }
        return Singleton.SharedExPayment
    }
    
    // the sharedInstance class method can be reached from ObjC
    class func SharedExPayment() -> RHB
    {
        return RHB.swiftSharedExPayment
    }
    
    func sendPaymentAmount(amount: String?) -> Bool
    {
        
        let amountDouble = Double(amount!)!
        
        let amountString = amountDouble == 0 ? "0" : String("\(Int(amountDouble * 100))")
        
        var zerosRequired = ""
        for _ in 0..<12-amountString.count
        {
            zerosRequired += "0"
        }
        let newAmountString = zerosRequired + amountString
        print(newAmountString)
        StaticFunctions.send(toDevice: StaticFunctions.stringHexForMPOS(withAmount: newAmountString))
        return true
    }
    
    func checkPaymentResponse(response: AnyObject) -> [String : String]
    {
        let tagArray = processResponse(withTextArray: EADSessionController.shared().readDataArray as! [String])

        var rhbResponse: [String:String] = [:]
                
        rhbResponse[RESPONSECARDCODE] = tagArray[.responseText]?.stringValue
        rhbResponse[RESPONSECARDMSG]  = tagArray[.responseText]?.stringValue
        rhbResponse[RESPONSECARDTYPE] = tagArray[.cardIssuerName]?.stringValue ?? ""
        rhbResponse[RESPONSECARDNO]   = tagArray[.cardNo]?.stringValue ?? ""
        rhbResponse[RESPONSECARDAPPROVALCODE] = tagArray[.approvalCode]?.stringValue ?? ""
        
        // TODO: Check on the performance of the replacing occurrences method as it seems to delay compilation. Method just strips any postfix spaces
        guard let status = RHBStatus(rawValue: (tagArray[.responseText]?.stringValue)!.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)) else
        {
            print("ERROR: rhb unknown status response")
            rhbResponse[RESPONSECARDSTAT] = UNKNOWNSCHEME
            return rhbResponse
        }
        switch status
        {
        case .successStatus:
            rhbResponse[RESPONSECARDSTAT] = SUCCESSSCHEME
        case .cancelStatus:
            rhbResponse[RESPONSECARDSTAT] = CANCELSCHEME
        case .failStatus:
            rhbResponse[RESPONSECARDSTAT] = FAILURESCHEME
        }

        return rhbResponse
    }
    
}
