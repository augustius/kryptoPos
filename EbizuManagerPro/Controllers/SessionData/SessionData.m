
//  SessionData.m
//
//
//
//  Copyright (c) 2013 MIPL. All rights reserved.
//  Modified by Jamal on Sept-14
//

#import "SessionData.h"
#import "UIUtils.h"
#import "shareddb.h"

#import "KryptoPOS-Swift.h"

@interface SessionData ()

@end

@implementation SessionData

@synthesize isReachable             = _isReachable;
@synthesize barcode_enable          = _barcode_enable;
@synthesize qr_validation_enable    = _qr_validation_enable;
@synthesize dine_in_enable          = _dine_in_enable;
@synthesize image_on_off_enable     = _image_on_off_enable;
@synthesize force_print_mode_enable = _force_print_mode_enable;
@synthesize enable_merchant_name    = _enable_merchant_name;
@synthesize enable_merchant_phone   = _enable_merchant_phone;
@synthesize enable_merchant_address = _enable_merchant_address;
@synthesize custom_sales_enable     = _custom_sales_enable;
@synthesize enable_mqtt             = _enable_mqtt;
@synthesize enable_multi_devices    = _enable_multi_devices;

#pragma mark - Public Methods -

+ (instancetype)sharedSessionData
{
    static SessionData *controller = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
                  {
                      controller = [[SessionData alloc] init];
                  });
    
    return controller;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _barcode_enable             = [Shareddb get_barcode];
        _qr_validation_enable       = [Shareddb get_qr_validation];
        _dine_in_enable             = [Shareddb get_dine_type];
        _gst_enable                 = [Shareddb get_gst];
        _business_type              = [Shareddb get_business_type];
        _image_on_off_enable        = [Shareddb get_image_mode];
        _force_print_mode_enable    = [Shareddb get_printer_mode];
        _custom_sales_enable        = [Shareddb get_custom_sales];
        _enable_merchant_name       = [Shareddb get_merchant_name_mode];
        _enable_merchant_phone      = [Shareddb get_merchant_phone_mode];
        _enable_merchant_address    = [Shareddb get_merchant_address_mode];
        _enable_mqtt                = [Shareddb get_mqtt_mode];
        _enable_multi_devices       = [Shareddb get_multi_device];
        
//        if(APP_DELEGATE.is_multi_device)
//        {
//            [APP_DELEGATE start_mqtt:YES];
//            [APP_DELEGATE listen_mqtt_incoming];
//        }
        
    }
    return  self;
}

#pragma mark - Manage Pay CONNECT RESPONSE
- (BOOL)handleURL:(NSURL *)url
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RESPONSES_RECEIVE" object:url];

    return YES;
}



@end
