//
//  SessionData.h
//  Copyright (c) 2013 MIPL. All rights reserved.
/**
 SessionData is the singleton object with life span of the app.
 Its only object is created when the app is launched.
 
 It stores variables for
 - kitchen printer IP
 - receipt printer IP
 - order number
 - Current Server(person taking the food orders)
 - POSPrinter object
 - Business
 - Settings
 **/

#import <Foundation/Foundation.h>
@class MultiplePaymentForm;

@interface SessionData : NSObject

+ (instancetype)sharedSessionData;

@property (nonatomic, assign) BOOL                barcode_enable;
@property (nonatomic, assign) BOOL                qr_validation_enable;
@property (nonatomic, assign) BOOL                dine_in_enable;
@property (nonatomic, assign) BOOL                image_on_off_enable;
@property (nonatomic, assign) BOOL                force_print_mode_enable;
@property (nonatomic, assign) BOOL                gst_enable;
@property (nonatomic, assign) BOOL                enable_merchant_name;
@property (nonatomic, assign) BOOL                enable_merchant_address;
@property (nonatomic, assign) BOOL                enable_merchant_phone;
@property (nonatomic, assign) BOOL                custom_sales_enable;
@property (strong, nonatomic) NSString            *business_type;
@property (nonatomic, assign) BOOL                 enable_multi_devices;
@property (nonatomic, assign) BOOL                 enable_mqtt;
@property (nonatomic, strong) NSString            *mqtt_ip_address;
@property (nonatomic, assign) BOOL                 isReachable;

/*MANAGE PAY Response from app*/
- (BOOL)handleURL:(NSURL *)url;

@end
