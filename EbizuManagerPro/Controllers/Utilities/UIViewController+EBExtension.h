//
//  UIViewController+EBExtension.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 16/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
@import SCLAlertView_Objective_C;

@interface UIViewController (EBExtension)

#pragma mark - call SCLAlert controller
- (void)present_sclalert:(nullable NSString *)title
                 message:(nullable NSString *)message
             alert_style:(SCLAlertViewStyle)style;

#pragma mark - call IOS alert controller
- (void)present_alert:(nullable NSString *)title message:(nullable NSString *)message;
- (void)present_alert:(nullable NSString *)title message:(nullable NSString *)message okTitle:(nullable NSString *)ok;

- (void)present_alert:(nullable NSString *)title
              message:(nullable NSString *)message
yesAlertActionCompletionHandler:(void(^ _Nullable)(UIAlertAction * _Nonnull action))yesCompletionHandler
noAlertActionCompletionHandler:(void(^ _Nullable)(UIAlertAction * _Nonnull action))noCompletionHandler;

- (void)present_alert:(nullable NSString *)title
              message:(nullable NSString *)message
             yesTitle:(nullable NSString *)yes
              noTitle:(nullable NSString *)no
yesAlertActionCompletionHandler:(void(^ _Nullable)(UIAlertAction * _Nonnull action))yesCompletionHandler
noAlertActionCompletionHandler:(void(^ _Nullable)(UIAlertAction * _Nonnull action))noCompletionHandler;

#pragma mark - google analytic method
- (void)sendFormToGoogleAnalytic;

#pragma mark - loading screen method
- (void)showLoadingScreen;
- (void)hideLoadingScreen;

@end
