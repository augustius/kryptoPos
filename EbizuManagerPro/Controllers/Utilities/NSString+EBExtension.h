//
//  NSString+EBExtension.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 16/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EBExtension)

- (NSString *)translate;
- (NSString *)MD5;
- (NSString *)filterSpecialChar;
- (NSString *)initialChar;
- (NSString *)getCurrency;
- (NSString *)addCurrency;
- (NSString *)getCurrencyCode;
- (NSString *)addCurrencyCode;
- (NSString *)maskedCreditCardNo;
- (BOOL)isValidEmail;


@end
