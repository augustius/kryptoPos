
#import "StringUtils.h"

#import <Foundation/NSString.h>
#import <CommonCrypto/CommonDigest.h>
#import "shareddb.h"
#import "UIUtils.h"

@implementation StringUtils

+ (NSString *)get_language_from_view:(NSString *)text
{
    // Method required by public API defined in StringUtils.h
    return @"";
}

+ (BOOL)isStringPresent:(NSString *)string
{
    
	if ([StringUtils isBlank:string] || (string == nil) || ([self isKindOfClass:[NSNull class]])) {
		return NO;
	}
	return YES;
}

+ (NSString *)stringByStrippingWhitespace:(NSString *)string
{
    
	return [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
}

+ (BOOL)isBlank:(NSString *)string
{
    
    if (string != nil)
    {
        
        if([StringUtils stringByStrippingWhitespace:string].length == 0)
            return YES;
        
    }
	
	return NO;
    
}

+ (BOOL)string:(NSString *)string containsSubstring:(NSString *)subString
{
    
	NSRange range = [string rangeOfString:subString];
    return (range.location != NSNotFound);
    
}

+ (NSArray *)string:(NSString *)string
 splitWithCharacter:(char) ch
{
    
	NSMutableArray *results = [[NSMutableArray alloc] init];
    
	int start = 0;
    
	for(int i=0; i < string.length; i++)
    {
		
		BOOL isAtSplitChar = [string characterAtIndex:i] == ch;
		BOOL isAtEnd = i == string.length - 1;
        
		if(isAtSplitChar || isAtEnd)
        {
            
			//take the substring & add it to the array
			NSRange range;
			range.location = start;
			range.length = i - start + 1;
			
			if(isAtSplitChar)
				range.length -= 1;
			
			[results addObject:[string substringWithRange:range]];
			start = i + 1;
            
		}
		
		if(isAtEnd && isAtSplitChar)
			[results addObject:@""];
        
	}
	
	return results ;
}

+ (NSString *)string:(NSString *)string
       substringFrom:(NSInteger)from
                  to:(NSInteger)to
{
    
	NSString *rightPart = [string substringFromIndex:from];
	return [rightPart substringToIndex:to-from];
    
}

+ (int)string:(NSString *)string indexOfString:(NSString *)str
{
    
	NSRange range = [string rangeOfString:str];
	return (int)range.location;
    
}

+ (int)string:(NSString *)string indexOfString:(NSString *)str
    fromIndex:(int)fromIndex
{
    
    NSRange rangeToSearch = NSMakeRange(fromIndex, string.length - fromIndex);
    NSRange range = [string rangeOfString:string options:NSLiteralSearch range:rangeToSearch];
    
    return (int)range.location;
}

+ (int)string:(NSString *)string lastIndexOfString:(NSString *) str
{
    
	NSRange range = [string rangeOfString:str options:NSBackwardsSearch];
    
	return (int)range.location;
    
}

+ (BOOL)string:(NSString *)string equalsIgnoreCase:(NSString *) str
{
    
	if( [string caseInsensitiveCompare:str] == NSOrderedSame )
    {
		return YES;
        
	} else
    {
        
		return NO;
	}
    
}

+ (NSString *)isStringNil:(NSString *)inputString
{
	if (inputString == nil)
    {
		return @"";
	}
	
	return inputString;
}


+ (NSString *)stringByTrimmingLeadingCharactersInSet:(NSCharacterSet *)characterSet inString:(NSString*)string
{
    NSRange rangeOfFirstWantedCharacter = [string rangeOfCharacterFromSet:characterSet.invertedSet];
    if (rangeOfFirstWantedCharacter.location == NSNotFound)
    {
        return @"";
    }
    return [string substringFromIndex:rangeOfFirstWantedCharacter.location];
}


+ (NSString *)stringByTrimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet
                                             inString:(NSString*)string
{
    NSRange rangeOfLastWantedCharacter = [string rangeOfCharacterFromSet:characterSet.invertedSet
                                                               options:NSBackwardsSearch];
    if (rangeOfLastWantedCharacter.location == NSNotFound)
    {
        return @"";
    }
    return [string substringToIndex:rangeOfLastWantedCharacter.location+1]; // non-inclusive
}

+ (NSString *)stringByTrimmingTrailingAndLeadingWhitespaceAndNewlineCharactersInString:(NSString*)string
{
    
    NSString *str = [self stringByTrimmingTrailingCharactersInSet:
                     [NSCharacterSet whitespaceAndNewlineCharacterSet]  inString:string];
    
    str = [self stringByTrimmingLeadingCharactersInSet:
           [NSCharacterSet whitespaceAndNewlineCharacterSet] inString:str];
    
    return str;
}

+ (NSData *)get_json_string:(NSArray *)json_array
{
    NSError *error;
    NSMutableData *json_mutable_data;
    NSData *json_data = [NSJSONSerialization dataWithJSONObject:json_array
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!json_data)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,error);
    }
    else
    {
        json_mutable_data = [NSMutableData dataWithBytes:(const void *)"r=" length:2];
        [json_mutable_data appendData:json_data];
    }
    
    return  json_mutable_data;
}

+ (NSData *)get_json_string_exclude_r:(NSArray *)json_array
{
    
    NSError *error;
    NSMutableData *json_string;
    NSData *json_data = [NSJSONSerialization dataWithJSONObject:json_array
                                                        options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                          error:&error];
    if (!json_data)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,error);
    }
    else
    {
        json_string = [NSMutableData dataWithBytes:(const void *)"" length:0];
        [json_string appendData:json_data];
    }
    return  json_string;
}

+(NSString *)get_user_detail:(NSString *)field_name
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"user_data"])
        return [[NSUserDefaults standardUserDefaults] valueForKey:@"user_data"][field_name];
    else
        return @"";
}

+(NSString *)get_business_detail:(NSString *)field_name
{
    if([[NSUserDefaults standardUserDefaults] valueForKey:BUSINESS_DATA])
        return [[[NSUserDefaults standardUserDefaults] valueForKey:BUSINESS_DATA][0] valueForKey:field_name];
    else
        return @"";
}

+ (NSString *)get_language_label:(NSString *)field_name //done
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = languages[0];

    if (APP_DELEGATE.language_bundle == nil)
    {
        if ([currentLanguage isEqualToString:@"ms"])
        {
            APP_DELEGATE.language_bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ms" ofType:@"lproj"]];
        }
        else if ([currentLanguage isEqualToString:@"id"])
        {
            APP_DELEGATE.language_bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"id" ofType:@"lproj"]];
        }
        else
        {
            APP_DELEGATE.language_bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Base" ofType:@"lproj"]];
        }
    }
    
    NSString* str = [APP_DELEGATE.language_bundle localizedStringForKey:field_name value:@"" table:nil];
    return str;
}

+ (NSString *)remove_last_char:(NSString *)word
{
    if ([word length] > 0) {
        word = [word substringToIndex:[word length] - 1];
    }
    
    return word;
}


@end
