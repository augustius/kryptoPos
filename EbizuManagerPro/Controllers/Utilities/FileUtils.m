//
//  FileUtils.m
//  
//


#import "FileUtils.h"
#import "StringUtils.h"
//#import "Macros.h"

@implementation FileUtils


+(NSString *) documentsDirectoryPath
{
    NSArray *paths               = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
	return documentsDirectory;
}

+(NSString *) documentsDirectoryPathForResource:(NSString *)fileName
{
    NSString *documentsDirectoryPath = [FileUtils documentsDirectoryPath];
    NSString *finalPath              = [documentsDirectoryPath stringByAppendingPathComponent:fileName];
	return finalPath;
}

+(BOOL) fileExistsAtPath:(NSString *)path
                fileName:(NSString *)fileName
{
	NSString *combinedPath = [path stringByAppendingPathComponent:fileName];
	
	NSFileManager * fileManager = [NSFileManager defaultManager];
	BOOL exists = [fileManager fileExistsAtPath:combinedPath]; 
	return exists;
}

+(NSString *) nextSequentialFileName:(NSString *)fileName
                      fileExtenstion:(NSString *)extenstion
                       directoryPath:(NSString *) path
{
	NSFileManager * fileManager = [NSFileManager defaultManager];
	NSArray *contents = [fileManager contentsOfDirectoryAtPath:path error:nil];
	
	if(contents)
    {
		NSString *formattedFileName = [[NSString alloc] initWithFormat:@"%@.%@", fileName, extenstion];
		int counter = 1;
		while([contents indexOfObject:formattedFileName] != NSNotFound) {
			NSString *newFileName = [[NSString alloc] initWithFormat:@"%@%d.%@", fileName, counter, extenstion];
			formattedFileName = newFileName;
			counter++;
		}
		DebugLog(@"Formated file name %@",formattedFileName );
		return formattedFileName;
		
	} else
    {
		return nil;
	}		
}

+(BOOL) writeToDocuemntsDirectoryAtPath:(NSString*)path
                               fileData:(NSData *)fileData
                             withPrefix:(NSString *)prefix
                           andExtension:(NSString *)extenstion
{
    [self deleteFileFromDocumentsDirectory:[NSString stringWithFormat:@"%@/%@.%@",path,prefix,extenstion]];
    
    NSString *combined_path      = [NSString stringWithFormat:@"%@/%@",[self documentsDirectoryPath],path];

    NSString *file_name_with_ext = [[NSString alloc] initWithFormat:@"%@.%@", prefix, extenstion];

    NSString *file_path          = [combined_path stringByAppendingPathComponent:file_name_with_ext];
    BOOL is_img_exists           = [[NSFileManager defaultManager] fileExistsAtPath:file_path];

    if(is_img_exists == NO)
    {
        BOOL success= [fileData writeToFile:file_path options:NSDataWritingAtomic error:nil];
        return success;
    }
    else
        return NO;
}


+(BOOL) writeFileToDocuemntsDirectory:(NSData *)fileData
                           withPrefix:(NSString *)prefix
                        withExtension:(NSString *)extenstion
{
	NSString *documentsDirectoryPath = [FileUtils documentsDirectoryPath];
	NSString *nextSequentialFileName = [FileUtils nextSequentialFileName:prefix
                                                          fileExtenstion:extenstion
                                                           directoryPath:documentsDirectoryPath];
	NSString *fileNameWithExtension = [[NSString alloc] initWithString: nextSequentialFileName];
	NSString *finalFilePath = [documentsDirectoryPath stringByAppendingPathComponent:fileNameWithExtension];
	BOOL success = [fileData writeToFile:finalFilePath atomically:YES];
	return success;
}

+(void) copyResourceFileToDocumentsDirectory:(NSString *)fileName
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    NSString *writablePath = [documentsDirectory stringByAppendingPathComponent:fileName];
	
	NSFileManager * fileManager = [NSFileManager defaultManager];
	BOOL succeeded = [fileManager fileExistsAtPath:writablePath]; 
	NSError *error;
	
	if(!succeeded) //If file is not in the documents directory then only write
    {
		NSString *newPath = [[NSBundle mainBundle].resourcePath stringByAppendingPathComponent:fileName];
		DebugLog(@"newPath : %@ ", newPath);
		
		succeeded = [fileManager copyItemAtPath:newPath
                                         toPath:writablePath
                                          error:&error];
		
		if(succeeded == FALSE)
		{
			DebugLog(@"%@ : copy failed", fileName);
			
		} else
        {
			DebugLog(@"%@ : copy success", fileName);
		}
	} else
    {
		DebugLog(@"%@ : already exists", fileName);
	}
}

+(void) copyResourcesToDocumentsDirectory:(NSArray *)resources
{
	int fileCount = (int)resources.count;
	for(int i = 0; i < fileCount; i++)
    {
		NSString *filename = resources[i];
		[self copyResourceFileToDocumentsDirectory:filename];
	}
}

+(BOOL) deleteFileFromDocumentsDirectory:(NSString *)fileName{
    
	NSString *path = [self documentsDirectoryPath];
	NSString *finalpath = [self documentsDirectoryPathForResource:fileName];
	BOOL fileExits = [self fileExistsAtPath:path fileName:fileName];
	if(fileExits)
    {
		NSFileManager *fileManager = [NSFileManager defaultManager];
		[fileManager removeItemAtPath:finalpath error:NULL];
		return YES;
	} else
    {
		return NO;
	}
	
}

/// temp directory:
+(NSString *) temporaryDirectoryPath
{
	NSString *temporaryDirectory = NSTemporaryDirectory();
	return temporaryDirectory;
}

+(BOOL) writeFileToTemporaryDirectory:(NSData *)fileData
                           withPrefix:(NSString *)prefix
                        withExtension:(NSString *)extenstion
{
	NSString *temporaryDirectoryPath = [FileUtils temporaryDirectoryPath];
	NSString *nextSequentialFileName = [FileUtils nextSequentialFileName:prefix fileExtenstion:extenstion directoryPath:temporaryDirectoryPath];
	DebugLog(@"nextSequentialFileName %@",nextSequentialFileName);
	NSString *fileNameWithExtension = [[NSString alloc] initWithString: nextSequentialFileName];
	NSString *finalFilePath = [temporaryDirectoryPath stringByAppendingPathComponent:fileNameWithExtension];
	DebugLog(@"final path name %@",finalFilePath);
	BOOL success = [fileData writeToFile:finalFilePath atomically:YES];	
	return success;
}

+(NSString *) temporaryDirectoryPathForResource:(NSString *) fileName
{
	NSString *temporaryDirectoryPath = [FileUtils temporaryDirectoryPath];
	NSString *finalPath = [temporaryDirectoryPath stringByAppendingPathComponent:fileName];
	return finalPath;
}

+(BOOL) createNewDirectoryAtPath:(NSString *)path andName:(NSString *)name
{
	NSFileManager * fileManager = [NSFileManager defaultManager];
	NSString *newDirectoryWithPath=[[NSString alloc]initWithFormat:@"%@/%@",path,name];
	BOOL created = [fileManager	createDirectoryAtPath:newDirectoryWithPath
                          withIntermediateDirectories:YES
                                           attributes:nil
                                                error:nil];
    return created;
}

+(BOOL) writeFileToAnyDirectory:(NSData *)fileData
                     withPrefix:(NSString *)prefix
                  withExtension:(NSString *)extenstion
                        andPath:(NSString *)path
{
	DebugLog(@"\n  path : %@",prefix);
	DebugLog(@"\n  path : %@",extenstion);
		DebugLog(@"\n  path : %@",path);

	NSString *nextSequentialFileName = [FileUtils nextSequentialFileName:prefix
                                                          fileExtenstion:extenstion directoryPath:path];
	
	DebugLog(@"nextSequentialFileName %@",nextSequentialFileName);
	NSString *fileNameWithExtension = [[NSString alloc] initWithString: nextSequentialFileName];
	NSString *finalFilePath = [path stringByAppendingPathComponent:fileNameWithExtension];
	DebugLog(@"final path name %@",finalFilePath);
	BOOL success = [fileData writeToFile:finalFilePath atomically:YES];	
	return success;
}

+(NSString *) writeFileToAnyDirectoryandReturnPath:(NSData *)fileData
                                        withPrefix:(NSString *)prefix
                                     withExtension:(NSString *)extenstion
                                           andPath:(NSString *)path
{
	DebugLog(@"\n  path : %@",prefix);
	DebugLog(@"\n  path : %@",extenstion);
	DebugLog(@"\n  path : %@",path);
	
	NSString *nextSequentialFileName = [FileUtils nextSequentialFileName:prefix
                                                          fileExtenstion:extenstion directoryPath:path];
	
	DebugLog(@"nextSequentialFileName %@",nextSequentialFileName);
	NSString *fileNameWithExtension = [[NSString alloc] initWithString: nextSequentialFileName];
	NSString *finalFilePath = [path stringByAppendingPathComponent:fileNameWithExtension];
	DebugLog(@"final path name %@",finalFilePath);
	[fileData writeToFile:finalFilePath atomically:YES];	
	return finalFilePath;
}

+(NSData *) getDataFromPath:(NSString *) path
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	
	NSData *data = nil;
	NSString *directoryPath = [FileUtils temporaryDirectoryPath];
	NSString *finalPath = [directoryPath stringByAppendingPathComponent:path];
	DebugLog(@"\n data at path : %@",finalPath);
	data=[fileManager contentsAtPath:finalPath];
	
    return data;

}



+(NSData *) getDataFromDocDirWithPath:(NSString *)path
                             fileName:(NSString*)fileName
                              andExtn:(NSString*)extn
{
    NSFileManager *fileManager      = [NSFileManager defaultManager];

    NSData *data                    = nil;
    NSString *combinedPath          = [NSString stringWithFormat:@"%@/%@",[self documentsDirectoryPath],path];

    NSString *fileNameWithExtension = [[NSString alloc] initWithFormat:@"%@.%@", fileName, extn];

    NSString *finalFilePath         = [combinedPath stringByAppendingPathComponent:fileNameWithExtension];

    data                            = [fileManager contentsAtPath:finalFilePath];

    return data;
    
}


+(NSArray*)getContentsofDirectory:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *result            = nil;
	result =[fileManager contentsOfDirectoryAtPath:path error:nil];
	DebugLog(@"result count :%d",(int)[result count]);
	return result;
	
}

+(BOOL) removeFileFromPath:(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
	BOOL isRemoved             = [fileManager removeItemAtPath:path error:NULL];
	return isRemoved;
}

+ (BOOL)contains_string:(NSString*)string
{
    NSRange range        = [string rangeOfString:@"/"];
    return range.length != 0;
}

+(void) download_image:(NSString *)image_url
       withStoringPath:(NSString *)storing_path
{
    NSString *image_name = image_url;
	
    if (image_name)
    {
        if([self contains_string:image_name])
        {
            NSRange name_range = [image_name rangeOfString:@"/" options:NSBackwardsSearch];
            image_name         = [image_name substringFromIndex:name_range.location+1];
        }
    }
    
    
    NSRange name_range      = [image_name rangeOfString:@"."];
    
    if(name_range.location)
    {
        NSArray *paths          = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *doc_directory = paths[0];
        
        //get file path
        NSString *file_path     = [doc_directory stringByAppendingPathComponent: [NSString stringWithFormat:@"%@/%@/%@.jpg",
                                                                                  POSImages,storing_path,[image_name substringToIndex:name_range.location]]];
        BOOL is_img_exists      = [[NSFileManager defaultManager] fileExistsAtPath:file_path];
        
        //check the file already in directory
        if(is_img_exists == NO && (![image_name isEqualToString:@"default_product.png"]|| ![image_name isEqualToString:@"default_people.jpg"]))
        {
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:image_url]];
            
            //write file to local directory
            [FileUtils writeToDocuemntsDirectoryAtPath: [NSString stringWithFormat:@"%@/%@",POSImages,storing_path]
                                              fileData: data withPrefix:[image_name substringToIndex:name_range.location]
                                          andExtension: @"jpg"];
        }
    }
}

@end
