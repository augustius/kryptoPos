//
//  google_analytics.h
//  Ebizu Manager
//
//  Created by Jamal on 2/11/15.
//  Copyright (c) 2015 EBIZU. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "GAI.h"
//#import "GAIFields.h"
//#import "GAITracker.h"
//#import "GAIDictionaryBuilder.h"

@interface google_analytics : NSObject
+ (void)send_data_to_google:(NSString *)form_name;
@end
