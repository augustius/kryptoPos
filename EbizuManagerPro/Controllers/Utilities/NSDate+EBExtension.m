//
//  NSDate+EBExtension.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "NSDate+EBExtension.h"

@implementation NSDate (EBExtension)

- (NSArray *)start_n_end_date
{
    NSDate *startDate    = [APP_DELEGATE.current_calender startOfDayForDate:self];
    NSDate *endDate      = [APP_DELEGATE.current_calender dateBySettingHour:23 minute:59 second:59 ofDate:self options:NSCalendarMatchFirst];
    return @[startDate, endDate];
}

//- (NSArray<NSDate *> *)start_n_end_date
//{
//    NSDate *startDate    = [APP_DELEGATE.current_calender dateBySettingHour:APP_DELEGATE.dailySalesHour minute:APP_DELEGATE.dailySalesMin second:00 ofDate:self options:NSCalendarMatchFirst];
//    
//    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
//    dayComponent.day = 1;
//    dayComponent.second = -1;
//    NSDate *endDate = [APP_DELEGATE.current_calender dateByAddingComponents:dayComponent toDate:startDate options:NSCalendarMatchFirst];
//    
//    return @[startDate, endDate];
//}

- (NSString *)sql_string
{
    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *current_date      = [formatter stringFromDate:self];
    return current_date;
}


@end
