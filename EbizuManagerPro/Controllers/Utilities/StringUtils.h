

#import <Foundation/Foundation.h>

@interface StringUtils : NSObject


+ (BOOL)isStringPresent:(NSString *)string;

+ (NSString *)stringByStrippingWhitespace:(NSString *)string;

+ (BOOL)isBlank:(NSString *)string;

+ (BOOL)string:(NSString *)string containsSubstring:(NSString *)subString;

+ (NSArray *)string:(NSString *)string
 splitWithCharacter:(char) ch;

+ (NSString *)string:(NSString *)string
       substringFrom:(NSInteger)from
                  to:(NSInteger)to;

+ (int)string:(NSString *)string
indexOfString: (NSString *) str;

+ (int)string:(NSString *)string
indexOfString:(NSString *)str
    fromIndex:(int)fromIndex;

+ (int)string:(NSString *)string
lastIndexOfString:(NSString *) str;

+ (BOOL)string:(NSString *)string equalsIgnoreCase:(NSString *) str;

+ (NSString *)isStringNil:(NSString *)inputString;

+ (NSString *)stringByTrimmingLeadingCharactersInSet:(NSCharacterSet *)characterSet
                                            inString:(NSString*)string;

+ (NSString *)stringByTrimmingTrailingCharactersInSet:(NSCharacterSet *)characterSet
                                             inString:(NSString*)string;

+ (NSString *)stringByTrimmingTrailingAndLeadingWhitespaceAndNewlineCharactersInString:(NSString*)string;

+ (NSData *) get_json_string:(NSArray *)json_array;

+ (NSData *)get_json_string_exclude_r:(NSArray *)json_array;

+ (NSString *) get_user_detail:(NSString *)field_name;

+ (NSString *) get_business_detail:(NSString *)field_name;

+ (NSString *)get_language_label:(NSString *)field_name;

+ (NSString *)remove_last_char:(NSString *)word;

@end


