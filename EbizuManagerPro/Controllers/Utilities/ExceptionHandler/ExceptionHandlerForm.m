//
//  ExceptionHandlerViewController.m
//  Ebizu Manager
//
//  Created by Jamal on 9/28/14.
//  Copyright (c) 2014 EBIZU. All rights reserved.
//

#import "ExceptionHandlerForm.h"

@interface ExceptionHandlerForm ()

@end

@implementation ExceptionHandlerForm

- (void) viewWillAppear:(BOOL)animated  //this function is where the screensize is set and centered
{
    [super viewWillAppear:animated];
    self.view.superview.backgroundColor = [UIColor clearColor];
    CGRect screen   = self.view.superview.bounds;
    CGRect frame    = CGRectMake(0, 0, 800, 400);
    float x         = (screen.size.width - frame.size.width) * .5f;
    float y         = (screen.size.height - frame.size.height) * .5f;
    frame           = CGRectMake(x, y, frame.size.width, frame.size.height);
    self.view.frame = frame;
}


- (IBAction)cmd_cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cmd_done:(id)sender
{
}
@end
