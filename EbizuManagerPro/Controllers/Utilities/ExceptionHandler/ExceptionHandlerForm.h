//
//  ExceptionHandlerViewController.h
//  Ebizu Manager
//
//  Created by Jamal on 9/28/14.
//  Copyright (c) 2014 EBIZU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExceptionHandlerForm : UIViewController


- (IBAction)cmd_cancel:(id)sender;
- (IBAction)cmd_done:(id)sender;
@end
