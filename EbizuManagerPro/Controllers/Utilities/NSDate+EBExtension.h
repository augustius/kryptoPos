//
//  NSDate+EBExtension.h
//  EbizuManagerPro
//
//  Created by augustius cokroe on 12/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (EBExtension)

- (NSArray<NSDate *> *)start_n_end_date;
- (NSString *)sql_string;

@end
