//
//  NoAutorotateUIImagePickerController.swift
//  EbizuManagerPro
//
//  Created by Daliso Ngoma on 15/02/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import UIKit

// TODO: Remove this class once the support for all orientations has been implemented.
class NoAutorotateUIImagePickerController: UIImagePickerController {
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask { return .landscape }
    override var shouldAutorotate: Bool { return false }
}
