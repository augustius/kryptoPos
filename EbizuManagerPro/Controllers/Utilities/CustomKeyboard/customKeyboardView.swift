//
//  customKeyboardView.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 23/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

@objc protocol customKeyboardViewDelegate: class
{
    @objc optional func keyPressed(key:String, textField:UITextField) -> Bool
    @objc optional func pinStatus(success:Bool, textField:UITextField) -> Bool
}

enum keyboardType
{
    case number,amount,pin,intNumber,ipAddress,creditCard
}

class customKeyboardView: UIView
{
    @IBOutlet var dotButton: UIButton!
    @IBOutlet var zeroWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var centerXConstraint: NSLayoutConstraint!
    
    @IBOutlet var deleteWidthConstraint: NSLayoutConstraint!
    @IBOutlet var deleteHeightConstraint: NSLayoutConstraint!
    @IBOutlet var deleteLabel: UILabel!
    @IBOutlet var spaceBetweenDoneDeleteConstraint: NSLayoutConstraint!
    @IBOutlet var doneWitdhConstraint: NSLayoutConstraint!
    @IBOutlet var doneHeightConstraint: NSLayoutConstraint!
    @IBOutlet var doneLabel: UILabel!
    
    @IBOutlet var pinView: UIView!
    @IBOutlet var firstPin: UIButton!
    @IBOutlet var secondPin: UIButton!
    @IBOutlet var thirdPin: UIButton!
    @IBOutlet var fourthPin: UIButton!
    
    weak var delegate: customKeyboardViewDelegate?
    var textInput: UITextInput?
    
    var textfield = UITextField()
    {
        didSet{
            textfield.inputView = self
            textInput = textfield
        }
    }
    
    var customKeyboardType:keyboardType = .number
    {
        didSet{
            switch customKeyboardType
            {
            case .number, .amount:
                dotButton.isHidden = false
                zeroWidthConstraint.constant = 186
                centerXConstraint.constant = -44
                deleteHeightConstraint.constant = 122
                doneWitdhConstraint.constant = 0
                doneHeightConstraint.constant = 122
                spaceBetweenDoneDeleteConstraint.constant = 10
                deleteLabel.text = "DELETE"
                doneLabel.text = "DONE"
                pinView.isHidden = true
                break
            case .pin:
                dotButton.isHidden = true
                zeroWidthConstraint.constant = 88
                centerXConstraint.constant = 0
                deleteHeightConstraint.constant = 0 //57
                doneWitdhConstraint.constant = -88
                doneHeightConstraint.constant = 0
                spaceBetweenDoneDeleteConstraint.constant = 0
                deleteLabel.text = ""//"CANCEL"
                doneLabel.text = ""
                pinView.isHidden = false
                resetPin()
                break
            case .intNumber, .ipAddress, .creditCard:
                dotButton.isHidden = true
                zeroWidthConstraint.constant = 186
                centerXConstraint.constant = -44
                deleteHeightConstraint.constant = 122
                doneWitdhConstraint.constant = 0
                doneHeightConstraint.constant = 122
                spaceBetweenDoneDeleteConstraint.constant = 10
                deleteLabel.text = "DELETE"
                doneLabel.text = "DONE"
                pinView.isHidden = true
                break
            }
        }
    }

    @IBAction func keyPressed(_ sender: UIButton)
    {
        if let safeKey = sender.titleLabel?.text
        {
            switch customKeyboardType
            {
            case .pin:
                textInput?.insertText(safeKey)
                handlePin()
            case .number, .amount, .ipAddress, .intNumber:
                textInput?.insertText(safeKey)
                // auto decimal for amount type
                if customKeyboardType == .amount {
                    let currentText = textfield.text?.replace(".", replacement: "") ?? ""
                    if currentText.count > 2 {
                        let endIndex = currentText.index(currentText.endIndex, offsetBy: -2)
                        let range = currentText.startIndex..<endIndex
                        let startText = currentText.substring(with: range)
                        let endText = currentText.substring(from: endIndex)
                        let finaltext = startText + "." + endText
                        textfield.text = String(format: "%.2f", Double(finaltext) ?? 0.0)
                    }

                }
                if let delegateExist = delegate?.keyPressed?(key: safeKey, textField: textfield)
                {
                    print(delegateExist ? "delegateExist" : "delegateNotExist")
                }
            case .creditCard:
                if let currentLength = textfield.text?.length
                {
                    if currentLength < 23 // cardnum = 19 , dash = 3
                    {
                        if ((currentLength + 1) % 5) == 0
                        {
                            textInput?.insertText("-")
                        }
                        textInput?.insertText(safeKey)
                    }
                }
            }
        }
    }
    
    @IBAction func deletePressed(_ sender: UIButton)
    {
        textInput?.deleteBackward()
    }
    
    @IBAction func donePressed(_ sender: UIButton)
    {
        guard
            let safeText = textfield.text,
            let safeDouble = Double(safeText)
            else { textfield.resignFirstResponder(); return }
   
        switch customKeyboardType
        {
        case .amount:
            textfield.text = String(format: "%.2f", safeDouble)
            break
        case .number:
            textfield.text = String(format: "%d", Int(safeDouble))
            break
        default:
            break
        }
        
        textfield.resignFirstResponder()
    }

    @IBAction func cancelPressed(_ sender: UIBarButtonItem) {
        textfield.resignFirstResponder()
    }

    
    func handlePin()
    {
        if let passCode = textfield.text
        {
            if passCode.count <= 4
            {
                switch passCode.count
                {
                case 1:
                    firstPin.backgroundColor = UIColor(netHex:"0x14406B")
                    break
                case 2:
                    secondPin.backgroundColor = UIColor(netHex:"0x14406B")
                    break
                case 3:
                    thirdPin.backgroundColor = UIColor(netHex:"0x14406B")
                    break
                case 4:
                    fourthPin.backgroundColor = UIColor(netHex:"0x14406B")
                    textfield.text = ""
                    textfield.resignFirstResponder()
                    
                    if let delegateExist = delegate?.pinStatus?(success: shareddb.shared().supervisor_pin_check(passCode.MD5()),
                                                                 textField: textfield)
                    {
                        print(delegateExist ? "delegateExist" : "delegateNotExist")
                    }
                    break
                default:
                    break
                }
            }
        }
    }
    
    func resetPin()
    {
        textfield.text = ""
        firstPin.backgroundColor = UIColor.white
        secondPin.backgroundColor = UIColor.white
        thirdPin.backgroundColor = UIColor.white
        fourthPin.backgroundColor = UIColor.white
    }
}
