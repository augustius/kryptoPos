//
//  customCalendarKeyboardView.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 04/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

class customCalendarKeyboardView: UIView
{
    @IBOutlet var datePicker: UIDatePicker!
    var dateFormatter = DateFormatter()
    var textfield = UITextField()
    {
        didSet{
            textfield.inputView = self
        }
    }
    
    @IBAction func dateChanged(_ sender: UIDatePicker)
    {
        textfield.text = dateFormatter.string(from: sender.date)
    }
    
    @IBAction func donePressed(_ sender: UIBarButtonItem)
    {
        textfield.resignFirstResponder()
    }
    
}
