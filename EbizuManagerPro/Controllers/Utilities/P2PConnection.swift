//
//  P2PConnection.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 18/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class P2PConnection: NSObject,MCSessionDelegate,MCNearbyServiceBrowserDelegate,MCNearbyServiceAdvertiserDelegate
{
    static let P2PCommand = "P2PCommand" // use as dictionary key
    @objc enum P2PCommandList: Int // use as dictionary value
    {
        case APPEND_CD,CLEAR_CD,PAYMENT_CD,CD_FEEDBACK
    }
    
    static let sharedInstance: P2PConnection = P2PConnection()
    
    // the sharedInstance class method can be reached from ObjC
    class func sharedP2PConnection() -> P2PConnection
    {
        return P2PConnection.sharedInstance
    }
    
    fileprivate override init() // Prevent use of initialisation of class
    {
        peerID          = MCPeerID.init(displayName:"temporary-name")
        session         = MCSession.init(peer: peerID)
        serviceType     = "chat-files"
        advertiser      = MCNearbyServiceAdvertiser.init(peer: peerID, discoveryInfo: nil, serviceType: serviceType)
        browser         = MCNearbyServiceBrowser.init(peer: peerID, serviceType: serviceType)
        invitedPeers    = [MCPeerID]()
        discoveredPeers = [MCPeerID]()
        connectedPeers  = [MCPeerID]()
    }
    
    // MARK: - Variable Declaration
    var peerID: MCPeerID
    var session: MCSession
    var advertiser: MCNearbyServiceAdvertiser
    var serviceType: String
    var browser: MCNearbyServiceBrowser
    var invitedPeers: [MCPeerID]
    var discoveredPeers: [MCPeerID]
    var connectedPeers: [MCPeerID]
    
    
    
    // MARK: - Public Method implementation
    func setupMasterDevice()
    {
        setupsetupPeerAndSessionWith(displayName: UIDevice.current.name)
        advertiseSelf(shouldAdvertise: true)
        setupMCNearbyServiceBrowser()
    }
    
    func setupCustomerDisplay()
    {
        setupsetupPeerAndSessionWith(displayName: userDefaults.string(forKey: CUSTOMER_DISPLAY_NAME)!)
        advertiseSelf(shouldAdvertise: true)
        setupMCNearbyServiceBrowser()
    }
    
    func setupsetupPeerAndSessionWith(displayName: String)
    {
        if let recyledPeerIDData = userDefaults.data(forKey: "RECYCLED_PEERID")
        {
            if let recyledPeerID = NSKeyedUnarchiver.unarchiveObject(with: recyledPeerIDData) as? MCPeerID
            {
                peerID = recyledPeerID
            }
        }
        else
        {
            peerID = MCPeerID.init(displayName: displayName)
            let recyledPeerID = NSKeyedArchiver.archivedData(withRootObject: peerID)
            userDefaults.set(recyledPeerID, forKey: "RECYCLED_PEERID")
        }
        
        session = MCSession.init(peer: peerID)
        session.delegate = self
        serviceType = "chat-files"
    }

    func setupMCNearbyServiceBrowser()
    {
        browser = MCNearbyServiceBrowser.init(peer: peerID, serviceType: serviceType)
        browser.delegate = self
        browser.startBrowsingForPeers()
    }
    
    func advertiseSelf(shouldAdvertise: Bool)
    {
        if shouldAdvertise
        {
            advertiser = MCNearbyServiceAdvertiser.init(peer: peerID, discoveryInfo: nil, serviceType: serviceType)
            advertiser.delegate = self
            advertiser.startAdvertisingPeer()
        }
        else
        {
            advertiser.stopAdvertisingPeer()
        }
    }
    
    func choose(peerID: MCPeerID, toInvite: Bool)
    {
        if toInvite
        {
            if !invitedPeers.contains(peerID)
            {
                browser.invitePeer(peerID, to: session, withContext: nil, timeout: 0)
                invitedPeers.append(peerID)
                appDelegate?.notify_(with_right_toast: "Connecting to \(peerID.displayName.replace("[CD]", replacement: ""))", title: nil)
            }
        }
        else
        {
            if connectedPeers.count > 0
            {
                if let index = connectedPeers.index(of:peerID)
                {
                    connectedPeers.remove(at: index)
                    appDelegate?.notify_(with_right_toast: "Disconnecting from \(peerID.displayName.replace("[CD]", replacement: ""))", title: nil)
                }
            }
            if invitedPeers.count > 0
            {
                if let index = invitedPeers.index(of:peerID)
                {
                    invitedPeers.remove(at: index)
                }
            }
            
            setupMasterDevice()
        }
    }
    
    func sendDataToPeers(data:AnyObject)
    {
        let dataToSend          = NSKeyedArchiver.archivedData(withRootObject: data) //here august
        let allConnectedPeers   = session.connectedPeers
        
        if allConnectedPeers.count > 0
        {
            do
            {
                try session .send(dataToSend, toPeers: allConnectedPeers, with: .reliable )
            }
            catch let error as NSError
            {
                print("error on \(#function) , \(error.localizedDescription)")
            }
        }
    }
    
    // MARK: - MCSession Delegate method
    func session(_ session: MCSession, didReceive data: Data, fromPeer peerID: MCPeerID)
    {
        let dict = ["data": data, "peerID": peerID] as [String : Any]
        
        NotificationCenter.default.post(name: NSNotification.Name("MCDidReceiveDataNotification"), object: nil, userInfo: dict)
    }
    
    func session(_ session: MCSession, peer peerID: MCPeerID, didChange state: MCSessionState)
    {
        if !userDefaults.bool(forKey: CUSTOMER_DISPLAY)
        {
            let dict = ["peerID": peerID,"state" : state] as [String : Any]
            
            switch state
            {
            case .connected:
                connectedPeers.append(peerID)
                appDelegate?.notify_(with_right_toast: "'\(peerID.displayName.replace("[CD]", replacement: ""))' is connected", title: nil)
                break
            case .notConnected:
                if connectedPeers.count > 0
                {
                    if let index = connectedPeers.index(of:peerID)
                    {
                        connectedPeers.remove(at: index)
                    }
                }
                
                if invitedPeers.count > 0
                {
                    if invitedPeers.contains(peerID)
                    {
                        browser.invitePeer(peerID, to: session, withContext: nil, timeout: 0)
                    }
                }
                
                appDelegate?.notify_(with_right_toast: "'\(peerID.displayName.replace("[CD]", replacement: ""))' is disconnected", title: nil)
                break
            case .connecting:
                appDelegate?.notify_(with_right_toast: "'\(peerID.displayName.replace("[CD]", replacement: ""))' is still connecting", title: nil)
                break
            }
            
            NotificationCenter.default.post(name: NSNotification.Name("MCDidChangeStateNotification"), object: nil, userInfo: dict)
        }
    }
    
    func session(_ session: MCSession, didReceive stream: InputStream, withName streamName: String, fromPeer peerID: MCPeerID)
    {
        
    }
    
    func session(_ session: MCSession, didStartReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, with progress: Progress)
    {
        
    }
    
//    func session(_ session: MCSession, didReceiveCertificate certificate: [Any]?, fromPeer peerID: MCPeerID, certificateHandler: @escaping (Bool) -> Void)
//    {
//        certificateHandler(true)
//    }
    
    func session(_ session: MCSession, didFinishReceivingResourceWithName resourceName: String, fromPeer peerID: MCPeerID, at localURL: URL?, withError error: Error?)
    {
        
    }
    
    // MARK: - MCNearbyServiceBrowserDelegate method
    func browser(_ browser: MCNearbyServiceBrowser, lostPeer peerID: MCPeerID)
    {
        print("\(peerID) connection lost (find me")
        
        if discoveredPeers.count > 0
        {
            if let index = discoveredPeers.index(of:peerID)
            {
                discoveredPeers.remove(at: index)
            }
        }
        
        NotificationCenter.default.post(name: NSNotification.Name("DiscoveredNewPeersNotification"), object: nil, userInfo: nil)

    }

    func browser(_ browser: MCNearbyServiceBrowser, foundPeer peerID: MCPeerID, withDiscoveryInfo info: [String : String]?)
    {
        let ownDevice = MCPeerID.init(displayName: UIDevice.current.name)
        
        if peerID != ownDevice
        {
            print(discoveredPeers)
            
            if !discoveredPeers.contains(peerID)
            {
                discoveredPeers.append(peerID)
            }
            
            if invitedPeers.contains(peerID)
            {
                if !connectedPeers.contains(peerID)
                {
                    browser.invitePeer(peerID, to: session, withContext: nil, timeout: 0)
                }
            }
            
            NotificationCenter.default.post(name: NSNotification.Name("DiscoveredNewPeersNotification"), object: nil, userInfo: nil)
        }
    }

    // MARK: - MCNearbyServiceAdvertiserDelegate method
    func advertiser(_ advertiser: MCNearbyServiceAdvertiser, didReceiveInvitationFromPeer peerID: MCPeerID, withContext context: Data?, invitationHandler: @escaping (Bool, MCSession?) -> Void)
    {
        print("invitation from : \(peerID.displayName)")

        if userDefaults.bool(forKey: CUSTOMER_DISPLAY)
        {
            invitationHandler(true,session)
            browser.invitePeer(peerID, to: session, withContext: nil, timeout: 0)
            print("Accept invitation from Master : \(peerID.displayName)")
        }
        else
        {
            if invitedPeers.contains(peerID)
            {
                invitationHandler(true,session)
                print("Accept invitation from Slave : \(peerID.displayName)")
            }
        }
    }
}

