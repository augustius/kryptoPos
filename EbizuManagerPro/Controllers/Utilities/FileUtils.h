//
//  FileUtils.h
//  
//


#import <Foundation/Foundation.h>


@interface FileUtils : NSObject 

+(NSString *) documentsDirectoryPath;

+(NSString *) documentsDirectoryPathForResource:(NSString *)fileName;

+(BOOL) fileExistsAtPath:(NSString *) path
                fileName:(NSString *)fileName;

+(NSString *) nextSequentialFileName:(NSString *)fileName
                      fileExtenstion:(NSString *)extenstion
                       directoryPath:(NSString *)path;

+(BOOL) writeFileToDocuemntsDirectory:(NSData *)fileData
                           withPrefix:(NSString *)prefix
                        withExtension:(NSString *)extenstion;

+(BOOL) writeToDocuemntsDirectoryAtPath:(NSString*)path
                               fileData:(NSData *)fileData
                             withPrefix:(NSString *)prefix
                           andExtension:(NSString *)extenstion;

+(void) copyResourceFileToDocumentsDirectory:(NSString *)fileName;

+(void) copyResourcesToDocumentsDirectory:(NSArray *)resources;

+(BOOL) deleteFileFromDocumentsDirectory:(NSString *)fileName;

+(NSString *) temporaryDirectoryPath;

+(BOOL) writeFileToTemporaryDirectory:(NSData *)fileData
                           withPrefix:(NSString *)prefix
                        withExtension:(NSString *)extenstion;

+(NSString *) temporaryDirectoryPathForResource:(NSString *)fileName;

+(BOOL) createNewDirectoryAtPath:(NSString *)path
                         andName:(NSString *)name;

+(BOOL) writeFileToAnyDirectory:(NSData *)fileData
                     withPrefix:(NSString *)prefix
                  withExtension:(NSString *)extenstion
                        andPath:(NSString *)path;

+(NSData *)getDataFromPath:(NSString*)path;

+(NSData *) getDataFromDocDirWithPath:(NSString *)path
                             fileName:(NSString*)fileName
                              andExtn:(NSString*)extn;

+(NSArray *)getContentsofDirectory:(NSString *)path;

+(BOOL) removeFileFromPath:(NSString *)path; 

+(NSString *) writeFileToAnyDirectoryandReturnPath:(NSData *)fileData
                                        withPrefix:(NSString *)prefix
                                     withExtension:(NSString *)extenstion
                                           andPath:(NSString *)path ;

+(void) download_image:(NSString *)image_url
       withStoringPath:(NSString *)storing_path;

+ (BOOL)contains_string:(NSString*)string;

@end
