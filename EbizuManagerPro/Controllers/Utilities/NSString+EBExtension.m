//
//  NSString+EBExtension.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 16/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "NSString+EBExtension.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (EBExtension)


/*!
 @brief transalate your string to choosen language.
 */
- (NSString *)translate
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = languages[0];
    
    if (APP_DELEGATE.language_bundle == nil)
    {
        if ([currentLanguage isEqualToString:@"ms"])
        {
            APP_DELEGATE.language_bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ms" ofType:@"lproj"]];
        }
        else if ([currentLanguage isEqualToString:@"id"])
        {
            APP_DELEGATE.language_bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"id" ofType:@"lproj"]];
        }
        else
        {
            APP_DELEGATE.language_bundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Base" ofType:@"lproj"]];
        }
    }
    
    NSString* str = [APP_DELEGATE.language_bundle localizedStringForKey:self value:@"" table:nil];
    return str;
}

/*!
 @brief decrypt your string into md5 string.
 */
- (NSString *)MD5
{
    const char *ptr = self.UTF8String;              // Create pointer to the string as UTF8
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];  // Create byte array of unsigned chars
    CC_MD5(ptr, (int)strlen(ptr), md5Buffer);       // Create 16 bytes MD5 hash value, store in buffer
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];    // Convert unsigned char buffer to NSString of hex values
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}

/*!
 @brief replace special char so it didn't disrupt database query execution.
 */
- (NSString *)filterSpecialChar
{
    NSString *filteredString = [self stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    
    return filteredString;
}

/*!
 @brief get String Initial, Example: Capucinno = CA , Hot Coffee = HC.
 */
- (NSString *)initialChar
{
    NSString *title_str     = [self isEqualToString:@""] ? @"No Name" : self;
    title_str = [title_str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSArray *array_value    = [title_str componentsSeparatedByString:@" "];
    if(array_value.count > 1)
    {
        NSString *spaceStr_1        = array_value[0];
        spaceStr_1                  = [spaceStr_1 substringToIndex:1];
        NSString *spaceStr_2        = array_value[1];
        /* Check the String contains Numeric value */
        NSCharacterSet *alphaNums   = [NSCharacterSet decimalDigitCharacterSet];
        NSCharacterSet *inStringSet = [NSCharacterSet characterSetWithCharactersInString:spaceStr_2];
        
        spaceStr_2                  = [alphaNums isSupersetOfSet:inStringSet] ? spaceStr_2 : [spaceStr_2 substringToIndex:1];
        
        title_str = [NSString stringWithFormat:@"%@%@",spaceStr_1,spaceStr_2];
    }
    else if ([title_str length] > 2)
    {
        title_str = [title_str substringToIndex:2];
    }
    
    return title_str;
}

/*!
 @brief get currency, input must contain "" to prevent any override on current string
 */
- (NSString *)getCurrency
{
    NSString *value = [self isEqualToString:@""] ? [[NSUserDefaults standardUserDefaults] stringForKey:CURRENCY] : self;
    return value;
}

/*!
 @brief append currency in front of string, Example: "89" to "RM 89"
 */
- (NSString *)addCurrencyCode
{
    NSString *currency = @"".getCurrencyCode;
    NSString *amountWithCurrency = [NSString stringWithFormat:@"%@ %@",currency,self];
    
    return amountWithCurrency;
}

/*!
 @brief get currency, input must contain "" to prevent any override on current string
 */
- (NSString *)getCurrencyCode
{
    NSString *value = [self isEqualToString:@""] ? [[NSUserDefaults standardUserDefaults] stringForKey:CURRENCY_CODE] : self;
    return value;
}

/*!
 @brief append currency in front of string, Example: "89" to "RM 89"
 */
- (NSString *)addCurrency
{
    NSString *currency = @"".getCurrency;
    NSString *amountWithCurrency = [NSString stringWithFormat:@"%@ %@",currency,self];

    return amountWithCurrency;
}


/*!
 @brief replace all char to * ,except last 4 char, Example: "1234567890" to "******7890"
 */
- (NSString *)maskedCreditCardNo
{
    // remove all non-numeric char first
    NSString *creditCardNum = [[self componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
    
    NSString *last4digit  = [creditCardNum substringFromIndex:MAX((int)[creditCardNum length]-4, 0)]; //in case string is less than 4 characters long.
    NSString *frontNum    = [@"" stringByPaddingToLength:MAX((int)[creditCardNum length]-4, 0) withString: @"*" startingAtIndex:0];
    
    return [NSString stringWithFormat:@"%@%@",frontNum,last4digit];
}

/*!
 @brief return YES when string is a valid email.
 */
- (BOOL)isValidEmail
{
    BOOL isValid = NO;
    if (self.length != 0)
    {
        NSString *emailRegex   = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        isValid                = [emailTest evaluateWithObject:self];
    }
    else
    {
        isValid = YES;
    }
    
    return isValid;
}


@end
