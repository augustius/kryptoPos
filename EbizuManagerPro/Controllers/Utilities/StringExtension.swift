//
//  StringExtension.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 14/04/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension String
{
    func translate() -> String
    {
        return NSString(string: self).translate() as String
    }
    
    func MD5() -> String
    {
        return NSString(string: self).md5() as String
    }
    
    func filterSpecialChar() -> String
    {
        return NSString(string: self).filterSpecialChar() as String
    }

    func initialChar() -> String
    {
        return NSString(string: self).initialChar() as String
    }
    
    func getCurrency() -> String
    {
        return NSString(string: self).getCurrency() as String
    }
    
    func addCurrency() -> String
    {
        return NSString(string: self).addCurrency() as String
    }

    func getCurrencyCode() -> String
    {
        return NSString(string: self).getCurrencyCode() as String
    }

    func addCurrencyCode() -> String
    {
        return NSString(string: self).addCurrencyCode() as String
    }
    
    func maskedCreditCardNo() -> String
    {
        return NSString(string: self).maskedCreditCardNo() as String
    }
    
    func isValidEmail() -> Bool
    {
        return NSString(string: self).isValidEmail()
    }
    
}
