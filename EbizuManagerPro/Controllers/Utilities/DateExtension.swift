//
//  DateExtension.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 03/07/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

extension Date
{
    func endOfToday() -> Date
    {
        let cal = Calendar(identifier: .gregorian)
        
        if let end = cal.date(bySettingHour: 23, minute: 59, second: 59, of: self)
        {
            return end
        }
        else
        {
            return self
        }
    }
    
    func startOfToday() -> Date
    {
        let cal = Calendar(identifier: .gregorian)
        
        if let start = cal.date(bySettingHour: 0, minute: 0, second: 0, of: self)
        {
            return start
        }
        else
        {
            return self
        }
    }
    
}
