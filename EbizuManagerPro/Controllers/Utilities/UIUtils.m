//
//  UIUtils.m
//  
//
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIUtils.h"

@implementation UIUtils

+ (UIColor *)getBorderColor
{
    return [UIColor colorWithRed:0.698 green:0.698 blue:0.698 alpha:1];
}

+ (void)show_keyboard:(UITextField *)textbox withUIView:(UIView *)inputAccessoryView
{
    @try
    {        
        if (!inputAccessoryView)
        {
            inputAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        }
        textbox.inputAccessoryView          = inputAccessoryView;
        inputAccessoryView.superview.frame  = CGRectMake(0, 460, 1024, 265);
        [textbox becomeFirstResponder];
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

+ (NSDictionary *)replaceObjectWithKey:(id)key inDictionary:(NSMutableDictionary *)dict withString:(NSString *)object
{
    NSMutableDictionary *mutableDict   = [dict mutableCopy];
    mutableDict[key]                   = object;
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

+ (BOOL) validate_email: (NSString *) candidate
{
    
    BOOL isvalid = NO;
    
    if (candidate.length != 0)
    {
        NSString *emailRegex   = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        isvalid                = [emailTest evaluateWithObject:candidate];
    }
    else
    {
        isvalid = YES;
    }
    
    return isvalid;
}

+ (dispatch_queue_t)shared_queue
{
    static dispatch_queue_t shared_Queue;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,
    ^{
        shared_Queue = dispatch_queue_create("com.ebizu.pos_manager", NULL);
    });
    return shared_Queue;
}

+ (NSMutableArray *)sorting_array:(NSArray *)unsorted_array key:(NSString*)key_name ascending:(BOOL)asc
{
    NSMutableArray *sorted_array;
    
    NSSortDescriptor *sort_descriptor = [NSSortDescriptor sortDescriptorWithKey:key_name ascending:asc comparator:^(id obj_1 , id obj_2)
    {
        if ([obj_1 floatValue] > [obj_2 floatValue])
        {
            return (NSComparisonResult)NSOrderedDescending;
        }
        else if ([obj_1 floatValue] < [obj_2 floatValue])
        {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    sorted_array = [NSMutableArray arrayWithArray:[unsorted_array sortedArrayUsingDescriptors:@[sort_descriptor]]];
    
    return sorted_array;
}

+(NSString*)different_bw_two_dates_start:(NSString*)start_date end:(NSString*)end_date
{
    NSString *difference_str = @"";
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    f.dateFormat = @"yyyy-MM-dd hh:mm:ss";
    NSDate *startDate = [f dateFromString:start_date];
    NSDate *endDate = [f dateFromString:end_date];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:NSCalendarWrapComponents];
    if(components.day !=0)
    {
        difference_str = [NSString stringWithFormat:@"%ldday",(long)components.day];
    }
    
    difference_str = [difference_str stringByAppendingString:[NSString stringWithFormat:@" %ldhr %ldm",(long)components.hour,(long)components.minute]];
    
    
    return difference_str;
}

#pragma mark - UIImage conversion
+(NSString*)convert_base_64:(UIImage *)image_file
{
    NSString *base_64_string = @"";
    NSData *image_date       = UIImageJPEGRepresentation(image_file, 1.0);
    base_64_string           = [image_date base64EncodedStringWithOptions:0];
    return base_64_string;
}

+(UIImage*)get_employee_image:(NSString *)user_id
{
    UIImage *empImage = [UIImage imageNamed:@"UserAvathar.png"]; //default image for employee
    
    NSData *imgData = [FileUtils getDataFromDocDirWithPath:[NSString stringWithFormat:@"%@/%@",POSImages,IMG_USERS]
                                                  fileName:[NSString stringWithFormat:@"%@_userimage",user_id]
                                                   andExtn:@"jpg"];
    
    if (imgData)
    {
        empImage = [UIImage imageWithData:imgData];
    }
    
    return empImage;
}

+ (UIImage *)convertImageToGrayScale:(UIImage *)image
{
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage;
}

#pragma mark - UIButton conversion
+(UIButton *)get_circle:(UIButton *)button
{
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = button.bounds.size.width/2;
    
    return button;
}


#pragma mark - method for html string conversion
+(NSMutableArray*)add_encoded_values:(NSArray*)arrays
{
    NSMutableArray *replaced_array = [[NSMutableArray alloc]init];
    
    for(NSMutableDictionary *dict in arrays)
    {
        NSMutableDictionary *replacing_dict = [[NSMutableDictionary alloc]init];
        
        for (NSString* key in dict)
        {
            id value = dict[key];
            
            if([value isKindOfClass:[NSString class]])
            {
                NSString *dict_value = dict[key];
                dict_value  = [self add_encoded_value:dict_value];
                replacing_dict[key] = dict_value;
            }
            else
            {
                replacing_dict[key] = value;
            }
            
        }
        [replaced_array addObject:replacing_dict];
    }
    return replaced_array;
}

+ (NSString *) add_encoded_value: (NSString *) stringToEncode
{
    NSString *encodedString = [stringToEncode stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return encodedString;
}

+(NSMutableArray*)remove_encoded_values:(NSArray*)arrays
{
    NSMutableArray *replaced_array = [[NSMutableArray alloc]init];
    
    for(NSMutableDictionary *dict in arrays)
    {
        NSMutableDictionary *replacing_dict = [[NSMutableDictionary alloc]init];
        
        for (NSString* key in dict)
        {
            id value = dict[key];
            
            if([value isKindOfClass:[NSString class]])
            {
                NSString *dict_value = dict[key];
                dict_value  = [self remove_encoded_value:dict_value];
                replacing_dict[key] = dict_value;
            }
            else
            {
                replacing_dict[key] = value;
            }
            
        }
        [replaced_array addObject:replacing_dict];
    }
    return replaced_array;
}

+ (NSString *)remove_encoded_value: (NSString *) stringToEncode
{
    NSString *encodedString = stringToEncode;

    while ([encodedString rangeOfString:@"%"].location != NSNotFound) // to remove double encoded value
    {
        encodedString = [encodedString stringByRemovingPercentEncoding];
        
        if (encodedString == nil)
        {
            return stringToEncode;
        }
    }
    return encodedString;
}

#pragma mark - Syncing status check

+ (void)store_last_sync_time :(NSString *)sync_time
{
    @try
    {
        // To check last sync time is more than 7 days
        NSDate *last_sync_time = [NSDate dateWithTimeIntervalSince1970:[sync_time integerValue]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
        NSString *current_time = [dateFormatter stringFromDate:last_sync_time];
        NSLog(@"%@",current_time);
        [[NSUserDefaults standardUserDefaults]setObject:current_time forKey:@"last_sync_time"];
    }
    @catch (NSException *exception){}
    @finally{}
}

+(BOOL) is_data_not_syned_seven_days
{
    BOOL is_not_synced = NO;
    
    NSString *last_sync_time = [[NSUserDefaults standardUserDefaults] objectForKey:@"last_sync_time"];
    
    if(![last_sync_time isEqualToString:@""] && last_sync_time != nil && last_sync_time != (id)[NSNull null])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm:ss"];
        NSDate *last_detect_time = [dateFormatter dateFromString:last_sync_time];
        NSArray *day_mins = [[self get_days_minutes_different:last_detect_time] componentsSeparatedByString:@"-"];
        int total_days = [[day_mins objectAtIndex:0] intValue];
        
        if(total_days >= 7)
        {
            is_not_synced = YES;
        }
    }
    return is_not_synced;
    
}

+ (NSString *)get_days_minutes_different :(NSDate*)validating_time
{
    NSString *days_mins_str = @"";
    
    unsigned int unitFlags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth;
    
    NSDateComponents *conversionInfo = [APP_DELEGATE.current_calender components:unitFlags fromDate:validating_time   toDate:[NSDate date]  options:0];
    
    int months = (int)[conversionInfo month];
    int days =  (int)[conversionInfo day];
    int hours = (int)[conversionInfo hour];
    int minutes = (int)[conversionInfo minute];
    NSLog(@"months %d, days %d, hours %d, minutes %d",months,days,hours,minutes);
    days_mins_str = [NSString stringWithFormat:@"%d-%d",days,minutes];
    return days_mins_str;
}

#pragma mark - Table/Collection view empty handler
+ (void)show_message_to:(UICollectionView *)collection tbl:(UITableView*)tblView message:(NSString*)msg
{
    UILabel *noDataLabel;
    if(collection != nil)
        noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, collection.bounds.size.width, collection.bounds.size.height)];
    else
        noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tblView.bounds.size.width, tblView.bounds.size.height)];
    noDataLabel.text                = msg;
    noDataLabel.textColor           = [UIColor blackColor];
    noDataLabel.textAlignment       = NSTextAlignmentCenter;
    if(collection != nil)
        collection.backgroundView   = noDataLabel;
    else
        tblView.backgroundView      = noDataLabel;
}

@end
