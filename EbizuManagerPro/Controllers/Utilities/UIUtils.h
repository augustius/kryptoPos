//
//  UIUtils.h
//  
//
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StringUtils.h"

@interface UIUtils : NSObject

+ (nullable UIColor *)getBorderColor;

+ (void)show_keyboard:(nullable UITextField *)textbox
           withUIView:(nullable UIView *)inputAccessoryView;

+ (nullable NSDictionary *)replaceObjectWithKey:(nullable id)key
                          inDictionary:(nullable NSMutableDictionary *)dict
                            withString:(nullable NSString *)object;

+ (nullable NSMutableArray *)sorting_array:(nullable NSArray *)unsorted_array
                              key:(nullable NSString*)key_name
                        ascending:(BOOL)asc;

+ (nullable dispatch_queue_t)shared_queue;


+ (nullable NSString*)different_bw_two_dates_start:(nullable NSString*)start_date end:(nullable NSString*)end_date;

#pragma mark - UIImage conversion
+ (nullable NSString*)convert_base_64:(nullable UIImage *)image_file;
+ (nullable UIImage*)get_employee_image:(nullable NSString *)user_id;
+ (nullable UIImage *)convertImageToGrayScale:(nullable UIImage *)image;

#pragma mark - UIButton conversion
+ (nullable UIButton *)get_circle:(nullable UIButton *)button;

#pragma mark - method for html string conversion
+ (nullable NSString *) add_encoded_value:(nullable NSString *)stringToEncode;
+ (nullable NSString *) remove_encoded_value:(nullable NSString *)stringToEncode;
+ (nullable NSMutableArray*)add_encoded_values:(nullable NSArray*)arrays;
+ (nullable NSMutableArray*)remove_encoded_values:(nullable NSArray*)arrays;

#pragma mark - Syncing staus check
+ (BOOL)        is_data_not_syned_seven_days;
+ (void)        store_last_sync_time :(nullable NSString *)sync_time;
+ (nullable NSString *)  get_days_minutes_different :(nullable NSDate*)validating_time;


#pragma mark - Table/Collection view empty handler
+ (void)show_message_to:(nullable UICollectionView *)collection tbl:(nullable UITableView*)tblView message:(nullable NSString*)msg;

@end
