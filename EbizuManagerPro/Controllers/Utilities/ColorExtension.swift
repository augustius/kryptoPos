//
//  ColorExtension.swift
//  EbizuManagerPro
//
//  Created by augustius cokroe on 05/05/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import Foundation

enum ebizuColor{
    case ebizuBlue, ebizuGreen, ebizuDarkGreen, ebizuDarkBlue, ebizuYellow, ebizuRed
}

extension UIColor
{
    convenience init(red: Int, green: Int, blue: Int)
    {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:String) // use digital color meter -> display in sRGB
    {
        var arrayEx = [String]()
        var firstVar = ""
        for (i, str) in netHex.enumerated()
        {
            if i % 2 != 0 // separated into
            {
                firstVar += String(str)
                arrayEx.append(firstVar)
                firstVar = ""
            }
            firstVar = String(str)
        }
        
        self.init(red:Int((UInt8(strtoul(arrayEx[1], nil, 16)))),
                  green:Int((UInt8(strtoul(arrayEx[2], nil, 16)))),
                  blue:Int((UInt8(strtoul(arrayEx[3], nil, 16)))))
    }
    
    convenience init(color: ebizuColor)
    {
        var colorHex = ""
        
        switch color
        {
        case .ebizuBlue:
            colorHex = "0x478BCA"
            break
        case .ebizuGreen:
            colorHex = "0x2ECC71"
            break
        case .ebizuYellow:
            colorHex = "0xFFFF00"
            break
        case .ebizuDarkBlue:
            colorHex = "0x213042"
            break
        case .ebizuDarkGreen:
            colorHex = "0x4EBD94"
            break
        case .ebizuRed:
            colorHex = "0xEC5351"
            break
        }
        
        self.init(netHex: colorHex)
    }
    
    func hexString() -> String
    {
        let components = self.cgColor.components
        let r: Float = Float(components![0])
        let g: Float = Float(components![1])
        let b: Float = Float(components![2])
        
        return String(format:"0x%02lX%02lX%02lX", lroundf(r * 255),lroundf(g * 255),lroundf(b * 255))
    }
    
}
