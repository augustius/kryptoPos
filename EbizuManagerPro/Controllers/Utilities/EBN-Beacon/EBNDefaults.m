//
//  EBNDefaults.m
//  EBNManagerFramwork
//
//  Created by VinothV on 3/30/15.
//  Copyright (c) 2015 Ebizu. All rights reserved.
//

#import "EBNDefaults.h"
#import "StringUtils.h"

NSString *beacon_identifier = @"com.ebizu.Ebizu-Manager";

@implementation EBNDefaults

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        NSUUID      *beacon_id          =   [[NSUUID alloc] initWithUUIDString:@"404C4F55-532B-204C-494E-454152444D53"];
        _supportedProximityUUIDs        =   @[beacon_id];
        
        _defaultProximityUUIDMajorValue = [NSNumber numberWithShort:[StringUtils get_business_detail:@"business_id"].integerValue];
        _defaultProximityUUIDMinorValue = [NSNumber numberWithShort:[StringUtils get_business_detail:@"business_id"].integerValue];
    }
    return self;
}

+(EBNDefaults *)sharedDefaults
{
    static id sharedDefaults = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      sharedDefaults = [[self alloc]init];
                  });
    return sharedDefaults;
}

-(NSUUID *)defaultProximityUUID
{
    return _supportedProximityUUIDs[0];
}

@end
