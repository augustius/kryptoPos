//
//  EBNDefaults.h
//  EBNManagerFramwork
//
//  Created by VinothV on 3/30/15.
//  Copyright (c) 2015 Ebizu. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *beacon_identifier;

@interface EBNDefaults : NSObject

+ (EBNDefaults *)sharedDefaults;

/*
 *  supportedProximityUUIDs
 *
 *  Discussion:
 *    To store supported UUIDS.
 */

@property (nonatomic, copy, readonly) NSArray *supportedProximityUUIDs;


/*
 *  defaultProximityUUID
 *
 *  Discussion:
 *    To have the device specific UUID
 */

@property (nonatomic, copy, readonly) NSUUID *defaultProximityUUID;


/*
 *  deafault Major & MinorValues
 *
 *  Discussion:
 *    To have the beacon Major & Minor Values
 */

@property (nonatomic, copy, readonly) NSNumber *defaultProximityUUIDMajorValue;

@property (nonatomic, copy, readonly) NSNumber *defaultProximityUUIDMinorValue;



/*
 *  deafault beacon Power
 *
 *  Discussion:
 *   To have default power for Beacon Transmitter
 */
@property (nonatomic, copy, readonly) NSNumber *defaultPower;

@end
