//
//  UIViewController+EBExtension.m
//  EbizuManagerPro
//
//  Created by augustius cokroe on 16/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

#import "UIViewController+EBExtension.h"
#import "KryptoPOS-Swift.h"

@implementation UIViewController (EBExtension)
#pragma mark - call SCLAlert controller
- (void)present_sclalert:(NSString *)title message:(NSString *)message alert_style:(SCLAlertViewStyle)style
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showTitle:self
                   title:title.translate
                subTitle:message.translate
                   style:style closeButtonTitle:@"Dismiss".translate
                duration:0.0f];
    });
}

#pragma mark - call IOS alert controller
- (void)present_alert:(NSString *)title message:(NSString *)message
{
    [self present_alert:title message:message okTitle:@"Dismiss"];
}

- (void)present_alert:(NSString *)title message:(NSString *)message okTitle:(NSString *)ok
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title.translate
                                                                                 message:message.translate
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:ok.translate
                                                           style:UIAlertActionStyleDefault
                                                         handler:nil];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)present_alert:(NSString *)title message:(NSString *)message yesAlertActionCompletionHandler:(void(^ _Nullable)(UIAlertAction * _Nonnull action))yesCompletionHandler noAlertActionCompletionHandler:(void(^ _Nullable)(UIAlertAction * _Nonnull action))noCompletionHandler
{
    [self present_alert:title
                message:message
               yesTitle:@"Yes"
                noTitle:@"No" yesAlertActionCompletionHandler:yesCompletionHandler noAlertActionCompletionHandler:noCompletionHandler];
}

- (void)present_alert:(NSString *)title message:(NSString *)message yesTitle:(NSString*)yes noTitle:(NSString*)no yesAlertActionCompletionHandler:(void(^ _Nullable)(UIAlertAction * _Nonnull action))yesCompletionHandler noAlertActionCompletionHandler:(void(^ _Nullable)(UIAlertAction * _Nonnull action))noCompletionHandler
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title.translate
                                                                                 message:message.translate
                                                                          preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAlertAction = [UIAlertAction actionWithTitle:yes.translate
                                                                style:UIAlertActionStyleDefault
                                                              handler:yesCompletionHandler];
        UIAlertAction *cancelAlertAction = [UIAlertAction actionWithTitle:no.translate
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:noCompletionHandler];
        [alertController addAction:okAlertAction];
        [alertController addAction:cancelAlertAction];
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

#pragma mark - google analytic method
- (void)sendFormToGoogleAnalytic
{
//    @try
//    {
//        NSString *className = NSStringFromClass([self class]);
//        
//        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
//        [tracker set    :kGAIScreenName value: className];
//        [tracker setAllowIDFACollection:NO];
//        [tracker send   :[[GAIDictionaryBuilder createScreenView] build]];
//    }
//    @catch (NSException *exception)
//    {
//        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
//    }
}

#pragma mark - loading screen method
- (void)showLoadingScreen
{
    @try
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                           [self.view showLoading];
                       });
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}

- (void)hideLoadingScreen
{
    @try
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                           [self.view hideLoading];
                       });
    }
    @catch (NSException *exception)
    {
        NSLog(@"%s %@",__PRETTY_FUNCTION__,exception);
    }
}


@end
