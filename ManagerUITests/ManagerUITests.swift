//
//  ManagerUITests.swift
//  ManagerUITests
//
//  Created by augustius cokroe on 08/03/2017.
//  Copyright © 2017 Ebizu Sdn Bhd. All rights reserved.
//

import XCTest

class ManagerUITests: XCTestCase {
    
    let app = XCUIApplication()
    let predict = NSPredicate(format: "exists == 1")
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testkeyInPin()
    {
        
        XCUIDevice.shared.orientation = .landscapeLeft
        
        app.buttons["4"].tap()
        app.buttons["2"].tap()
        app.buttons["9"].tap()
        app.buttons["9"].tap()
        app.buttons["ENTER"].tap()
        
        let collectionViewsQuery = app.scrollViews.otherElements.collectionViews
        collectionViewsQuery.staticTexts["Coffee"].tap()
        
        for _ in 0...100
        {
            testkeyInItem()
        }
    }
    
    func testkeyInItem()
    {
        let collectionViewsQuery = app.scrollViews.otherElements.collectionViews
        collectionViewsQuery.staticTexts["Cafe Latte"].tap()
        let tablesQuery = app.tables
        let addOnEspressoPriceRm106Switch = tablesQuery.cells.containing(.staticText, identifier:"Add On : Espresso").children(matching: .switch).matching(identifier: "Add On : Espresso, Price : RM 1.06").element(boundBy: 0)
        addOnEspressoPriceRm106Switch.tap()
        let doneButton = app.buttons["Done"]
        doneButton.tap()
        
        
        collectionViewsQuery.staticTexts["Cappucino"].tap()
        tablesQuery.cells.containing(.staticText, identifier:"Toping : add").children(matching: .switch).matching(identifier: "Toping : add, Price : RM 2.12").element(boundBy: 0).tap()
        doneButton.tap()
        
        
        app.buttons["Pay Now"].tap()
        app.collectionViews.buttons["cash icon"].tap()
        app.buttons["MYR 33.00"].tap()
        app.buttons["SUBMIT"].tap()
        app.buttons["Close"].tap()
    }
}
