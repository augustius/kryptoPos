# CHANGE LOG:

## (1.3.1)
**New:**
* Remove Customer support cell
* Update kitchen receipt item line

## (1.3)
**New:**
* Update epson sdk from 1.3.4 to 2.7.0
* initiate gitlab